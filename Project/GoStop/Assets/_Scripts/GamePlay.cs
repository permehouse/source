﻿using UnityEngine;
using System.Collections;

public class GamePlay : MonoBehaviour {
	public enum PLAY_STATE {
		READY = 0,
		DITSRIBUTE_CARD,
		SELECT_HAND_CARD,
		OPEN_DECK_CARD,
		DECIDE_GO_OR_STOP,
		CHANGE_TURN,
		END
	};

	private PLAY_STATE nPlayState = PLAY_STATE.READY;

	private int PLAYER_HAND_CARD_COUNT = 10;
	private int GROUND_CARD_COUNT = 8;

	private GameManager iGameManager;
	private CardDeck iCardDeck;
	private GroundCards iGroundCards;
	private Player iMe;
	private Player iYou;
	private UI iUI;

	private bool bMyTurn = false; 

	private static GamePlay _this;
	public static GamePlay GetInstance() {
		return _this;
	}
	
	void Awake() {
		_this = this;
	}

	public void Init() {
		iGameManager = GameManager.GetInstance ();
		iCardDeck = transform.Find ("CardDeck").GetComponent<CardDeck> ();
		iGroundCards = transform.Find ("GroundCards").GetComponent<GroundCards> ();
		iMe = transform.Find ("Player/Me").GetComponent<Player> ();
		iYou = transform.Find ("Player/You").GetComponent<Player> ();
		iUI = transform.Find ("UI").GetComponent<UI> ();
	}

	public void Clear() {
		iCardDeck.Clear ();
		iMe.Clear ();
		iYou.Clear ();
		iGroundCards.Clear ();

		iCardDeck.UpdateLayout ();
		iUI.UpdateScore ();
	}

	void Start() {
		Init ();
		SetPlayState (PLAY_STATE.READY);
	}

	void Update() {
		if (!CheckGameState ()) {
			return;
		}
	}

	public void StartPlay() {
		Clear ();

		bMyTurn = true;
		SetPlayState (PLAY_STATE.DITSRIBUTE_CARD);
	}

	bool CheckGameState() {
		GameManager.GAME_STATE nGameState = iGameManager.GetGameState ();
		if (nGameState == GameManager.GAME_STATE.PLAY) {
			return true;
		}

		return false;
	}

	public void SetPlayState (PLAY_STATE nPS) {
		nPlayState = nPS;

	   switch (nPlayState) {
		case PLAY_STATE.READY:
			PlayState_Ready ();
			break;
		case PLAY_STATE.DITSRIBUTE_CARD:
			PlayState_DistributeCard ();
			break;
		case PLAY_STATE.SELECT_HAND_CARD: 
			PlayState_SelectHandCard ();
			break;
		case PLAY_STATE.OPEN_DECK_CARD:
			PlayState_OpenDeckCard ();
			break;
		case PLAY_STATE.DECIDE_GO_OR_STOP:
			PlayState_DecideGoOrStop ();
			break;
		case PLAY_STATE.CHANGE_TURN:
			PlayState_ChangeTurn ();
			break;
		case 	PLAY_STATE.END:
			PlayState_End ();
			break;
		}
	}
	
	public PLAY_STATE GetPlayState () {
		return nPlayState;
	}

	void PlayState_Ready() {
		Clear ();
	}

	void PlayState_DistributeCard() {
		iCardDeck.ShuffleCards ();
		iCardDeck.DistributeCards (iMe, PLAYER_HAND_CARD_COUNT);
		iCardDeck.DistributeCards (iYou, PLAYER_HAND_CARD_COUNT);
		iCardDeck.DistributeCards (iGroundCards, iMe, GROUND_CARD_COUNT);

		iGroundCards.CheckBombCards ();

		iMe.SortHandCards ();
		iYou.SortHandCards ();

		iMe.UpdateLayout ();
		iYou.UpdateLayout ();
		iGroundCards.UpdateLayout ();
		iCardDeck.UpdateLayout ();
		
		SetPlayState (PLAY_STATE.SELECT_HAND_CARD);
	}
	void PlayState_SelectHandCard() {
		Player iPlayer = bMyTurn ? iMe : iYou;

		iPlayer.CheckBombHandCards (iGroundCards);

		if (bMyTurn) {
			iPlayer.CheckMatchedGroundCard (iGroundCards);
			iPlayer.UpdateHandCardsLayout ();
		} else {
			iPlayer.SelectOneCard ();
		}
	}

	void PlayState_OpenDeckCard() {
		iCardDeck.OpenTopCard ();
	}

	void PlayState_DecideGoOrStop() {
		Player iPlayer = bMyTurn ? iMe : iYou;

		iPlayer.CalculateScore ();
		iUI.UpdateScore ();

		if (iCardDeck.isEmpty () || iPlayer.isEmptyHandCards()) {
			SetPlayState(PLAY_STATE.END);
			return;
		}

		if (iPlayer.bCanGoOrStop) {
			if(bMyTurn) {
				iUI.ShowGoOrStopBox();
			}
			else {
				SetPlayState (PLAY_STATE.END);
			}
		} else {
			SetPlayState (PLAY_STATE.CHANGE_TURN);
		}
	}
	void PlayState_ChangeTurn() {
		bMyTurn = !bMyTurn;
		SetPlayState(PLAY_STATE.SELECT_HAND_CARD);
	}
	void PlayState_End() {
		iGameManager.SetGameState (GameManager.GAME_STATE.END);
	}

	public void PostEvent_SelectCard(Card iCard) {
		if (nPlayState != PLAY_STATE.SELECT_HAND_CARD) {
			return;
		}

		Player iPlayer = bMyTurn ? iMe : iYou;
		if (!iPlayer.RemoveHandCard(iCard) ) {
			return;
		}

		if (iCard.bGhost) {
			iCardDeck.DestroyGhostCard(iCard);

			SetPlayState (PLAY_STATE.OPEN_DECK_CARD);
			return;
		}

		if (iCard.bJoker) {
			iPlayer.AddOwnCard(iCard);
			Card iNewCard = iCardDeck.GetNextCard();
			iPlayer.AddHandCard(iNewCard);

			iPlayer.UpdateLayout();
			SetPlayState(PLAY_STATE.SELECT_HAND_CARD);
			return;
		}

		Card iMatchedCard = iGroundCards.FindMatchedCard (iCard);
		if (iMatchedCard) {
			if(iCard.bBomb) {
				iGroundCards.AddMatchedCards (iMatchedCard, iPlayer.GetBombHandCards(iCard));
				// Add two ghost cards
				iPlayer.AddHandCard (iCardDeck.CreateGhostCard(bMyTurn));
				iPlayer.AddHandCard (iCardDeck.CreateGhostCard(bMyTurn));
			}
			else {
				iGroundCards.AddMatchedCard (iMatchedCard, iCard);
			}
		} else {
			iGroundCards.Add (iCard);
		}

		iPlayer.UpdateOwnCardsLayout ();
		iGroundCards.UpdateLayout ();
		SetPlayState (PLAY_STATE.OPEN_DECK_CARD);
	}

	public void PostEvent_OpenCard(Card iCard) {
		if (nPlayState == PLAY_STATE.OPEN_DECK_CARD) {
			StartCoroutine (PostOpenDeckCard (iCard));
		}
		else if (nPlayState == PLAY_STATE.SELECT_HAND_CARD) {
			if(!bMyTurn) {
				PostEvent_SelectCard (iCard);
			}
		}
	}

	IEnumerator PostOpenDeckCard(Card iCard) {
		Player iPlayer = bMyTurn ? iMe : iYou;
		Player iOppenent = bMyTurn ? iYou : iMe;
		if (iCard.bJoker) {
			Card iMatchedCard = iGroundCards.GetMatchedCard ();
			if(iMatchedCard) {
				iGroundCards.AddMatchedCard (iMatchedCard, iCard);
				iGroundCards.UpdateLayout();
			}
			else {
				iPlayer.AddOwnCard(iCard);
				iPlayer.UpdateOwnCardsLayout ();
			}

			SetPlayState(PLAY_STATE.OPEN_DECK_CARD);
		} else {
			Card iMatchedCard = iGroundCards.FindMatchedCard (iCard);
			if (iMatchedCard) {
				iGroundCards.AddMatchedCard (iMatchedCard, iCard);
			} else {
				iGroundCards.Add (iCard);
			}

			iGroundCards.UpdateLayout ();

			yield return new WaitForSeconds (1);

			int nCountBonusPI = iGroundCards.CheckBonusPICount();
			if(nCountBonusPI > 0) iOppenent.GiveBonusPI(iPlayer, nCountBonusPI);
			iGroundCards.MoveMatchedCardToPlayer (iPlayer);
			
			iPlayer.CheckBombHandCards (iGroundCards);
			iPlayer.CheckMatchedGroundCard (iGroundCards);
			iPlayer.UpdateLayout ();
			iGroundCards.UpdateLayout ();
			
			SetPlayState (PLAY_STATE.DECIDE_GO_OR_STOP);
		}
	}

	public void PostEvent_DecideGoOrStop(bool bGo) {
		if (!bMyTurn) {
			return;
		}

		if (bGo) {
			iMe.DecidedGo ();
			SetPlayState(PLAY_STATE.CHANGE_TURN);
		} else {
			SetPlayState(PLAY_STATE.END);
		}
	}
}
