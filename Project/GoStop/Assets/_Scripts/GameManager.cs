﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
	public enum GAME_STATE {
		READY = 0,
		START,
		PLAY,
		PAUSE,
		RESUME,
		END
	};
	private GAME_STATE nGameState = GAME_STATE.READY;

	private GamePlay iGamePlay;

	private static GameManager _this;
	public static GameManager GetInstance() {
		return _this;
	}

	void Awake() {
		_this = this;
	}

	void Init () {
		iGamePlay = GamePlay.GetInstance ();
	}

	// Use this for initialization
	void Start () {
		Init ();
		SetGameState (GAME_STATE.READY);
	}
	
	// Update is called once per frame
	void Update () {
		if (!CheckGameState ()) {
			return;
		}
	}

	bool CheckGameState () {
		return true;
	}

	public void SetGameState (GAME_STATE nGS) {
		nGameState = nGS;

		switch (nGameState) {
		case GAME_STATE.READY:
			GameState_Ready ();
			break;
		case GAME_STATE.START: 
			GameState_Start ();
			break;
		case GAME_STATE.PLAY: 
			GameState_Play ();
			break;
		case GAME_STATE.PAUSE:
			GameState_Pause ();
			break;
		case GAME_STATE.RESUME:
			GameState_Resume ();
			break;
		case 	GAME_STATE.END:
			GameState_End ();
			break;
		}
	}

	public GAME_STATE GetGameState () {
		return nGameState;
	}

	void GameState_Ready() {
	}
	
	void GameState_Start() {
		SetGameState (GAME_STATE.PLAY);
	}
	
	void GameState_Play() {
		iGamePlay.StartPlay ();
	}
	
	void GameState_Pause() {
		
	}
	
	void GameState_Resume() {
		
	}
	
	void GameState_End() {
		SetGameState (GAME_STATE.READY);
	}
}
