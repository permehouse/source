﻿using UnityEngine;
using System.Collections;

public class GroundCards : MonoBehaviour {
	public class Slot {
		public ArrayList iCards = new ArrayList();
		public bool bMatched = false;
//		public bool bPuck = false;

		public Slot() {
			Clear ();
		}

		public void Clear() {
			iCards.Clear ();
			bMatched = false;	
//			bPuck = false;
		}

		public void Add(Card iCard) {
			iCards.Add (iCard);
		}

		public Card GetFirstCard() {
			if (iCards.Count == 0) {
				return null;
			}

			return (Card)iCards [0];
		}
	}
	public ArrayList iCardSlots = new ArrayList();

	private int DEFAULT_DISPLAY_CARD_COUNT = 8;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Clear() {
		foreach (Slot iSlot in iCardSlots) {
			iSlot.Clear();
		}
		iCardSlots.Clear ();
	}

	public void Add(Card iCard) {
		Slot iSlot =new Slot ();
		iSlot.Add (iCard);

		iCardSlots.Add (iSlot);
	}

	public Slot Add(Card iFirstCard, Card iCard) {
		Slot iSlot = FindSlot (iFirstCard);
		if (iSlot == null) {
			return null;
		}

		iSlot.Add(iCard);
		return iSlot;
	}

	public void AddMatchedCard(Card iMatchedCard, Card iCard) {
		Slot iSlot = Add (iMatchedCard, iCard);
		if (iSlot != null) {
//			iSlot.bPuck = CheckPuck (iSlot);
			iSlot.bMatched = !CheckPuck(iSlot);
		}
	}

	public void AddMatchedCards(Card iMatchedCard, ArrayList iBombCards) {
		Slot iSlot = null;
		for (int i = 0; i< iBombCards.Count; i++) {
			Card iCard = (Card)iBombCards [i];
			if (iSlot == null) {
				iSlot = Add (iMatchedCard, iCard);
			} else {
				iSlot.Add (iCard);
			}
		}

		if (iSlot != null) {
			iSlot.bMatched = true;
		}
	}

	public bool CheckPuck (Slot iSlot)	{
		int nCount = 0;
		foreach (Card iCard in iSlot.iCards) {
			if(!iCard.bJoker && !iCard.bBomb) {
				nCount++;
			}
		}

		if (nCount == 3) {
			return true;
		}

		return false;
	}

	public int CheckBonusPICount() {
		// Case 1) Check Piled Cards by Bomb or Puck
		int nCountBonusPI = 0;
		int nRestCardCount = 0;
		Card.SHAPE nPrevMatchedShape = Card.SHAPE.NONE;
		foreach (Slot iSlot in iCardSlots) {
			if (iSlot.bMatched) {
				// Case 2) Match all 4 cards of a shape at one time
				Card.SHAPE nShape = iSlot.GetFirstCard().nShape;
				if(nPrevMatchedShape == nShape) {
					nPrevMatchedShape = nShape;
					nCountBonusPI++;
				}
				if(iSlot.iCards.Count >= 4) {
					nCountBonusPI++;
				}
			}
			else {
				nRestCardCount++;
			}
		}

		// Case 3) No more Ground Cards
		if (nRestCardCount == 0) {
			nCountBonusPI++;
		}

		return nCountBonusPI;
	}

	public void MoveMatchedCardToPlayer(Player iPlayer) {
		ArrayList iRemovedSlots = new ArrayList();

		foreach (Slot iSlot in iCardSlots) {
//			if (iSlot.bMatched && !iSlot.bPuck) {
			if (iSlot.bMatched) {
				foreach (Card iCard in iSlot.iCards) {
					iPlayer.AddOwnCard (iCard);
				}

				iRemovedSlots.Add (iSlot);
			}
		}

		foreach (Slot iSlot in iRemovedSlots) {
			iCardSlots.Remove (iSlot);
		}
	}

//	public void Sort() {
//		IComparer iComparer = new CardComparer();
//		iCards.Sort (iComparer);
//	}

	public Slot FindSlot(Card iCard) {
		foreach (Slot iSlot in iCardSlots) {
			Card iFirstCard = iSlot.GetFirstCard ();
			if (iFirstCard == iCard) {
				return iSlot;
			}
		}

		return null;
	}

	public Card FindMatchedCard(Card iCard) {
		foreach (Slot iSlot in iCardSlots) {
			Card iFirstCard = iSlot.GetFirstCard ();
			if (iFirstCard.nShape == iCard.nShape) {
				return iFirstCard;
			}
		}

		return null;
	}

	public Card GetMatchedCard() {
		foreach (Slot iSlot in iCardSlots) {
			if (iSlot.bMatched) {
				return iSlot.GetFirstCard ();
			}
		}

		return null;
	}

	public void RemoveCard(Card iCard) {
		iCardSlots.Remove (iCard);
	}

	public void UpdateLayout() {
		Transform trPos = transform.Find ("Position");

		Vector2 dLeftBottom = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
		Vector2 dRightTop = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
		dLeftBottom.x = trPos.position.x;
		dRightTop.x = -trPos.position.x;
		int nCount = (iCardSlots.Count <= DEFAULT_DISPLAY_CARD_COUNT) ? DEFAULT_DISPLAY_CARD_COUNT : iCardSlots.Count;
		float nGapX = (dRightTop.x - dLeftBottom.x) / nCount;

		float nOffsetX = ((dRightTop.x - dLeftBottom.x) / Screen.width) * 10;
		float nOffsetY = ((dRightTop.y - dLeftBottom.y) / Screen.height) * 10;
		float nOffsetZ = 0.01f;

		Card iCard = null;
		for (int i = 0; i < iCardSlots.Count; i++) {
			Slot iSlot = (Slot) iCardSlots[i];
			for(int j = 0; j < iSlot.iCards.Count; j++) {
				iCard = (Card)iSlot.iCards[j];
				iCard.transform.parent = transform;

				iCard.transform.position = new Vector3(trPos.position.x + i * nGapX + j * nOffsetX, trPos.transform.position.y + j * nOffsetY, trPos.transform.position.z - j * nOffsetZ);

				iCard.Show ();
				iCard.Open();
			}
		}
	}

	public void CheckBombCards ()	{
		ArrayList dBombSlots = new ArrayList ();
		ArrayList dIndex = new ArrayList ();
		for (int i = 0; i < iCardSlots.Count - 1; i++) {
			Slot iSlot = (Slot)iCardSlots [i];
			Card iCard = (Card)iSlot.GetFirstCard ();
			if (iCard != null) {
				dIndex.Clear();
				for (int j = i + 1; j < iCardSlots.Count; j++) {
					Slot iSlot2 = (Slot)iCardSlots [j];
					Card iCard2 = (Card)iSlot2.GetFirstCard ();
					if (iCard2 != null) {
						if (iCard.nShape == iCard2.nShape) {
							dIndex.Add (j);

							if (dIndex.Count == 2) {
								iCard.bBomb = true;

								foreach (int nIndex in dIndex) {
									Slot iSlot3 = (Slot)iCardSlots [nIndex];
									Card iCard3 = (Card)iSlot3.GetFirstCard ();

									iCard3.bBomb = true;
									iSlot.Add (iCard3);
									iSlot3.Clear ();

									dBombSlots.Add (iSlot3);
								}

								break;
							}
						}
					}
				}
			}
		}

		foreach (Slot iBombSlot in dBombSlots) {
			iCardSlots.Remove(iBombSlot);
		}
	}
}
