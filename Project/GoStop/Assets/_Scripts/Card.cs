﻿using UnityEngine;
using System.Collections;

public class CardComparer : IComparer  {
	int IComparer.Compare( System.Object x, System.Object y )  {
		Card iCard1 = (Card)x;
		Card iCard2 = (Card)y;

		int nPoint1 = ((int)iCard1.nShape) * 10 + (int)iCard1.nIndex;
		int nPoint2 = ((int)iCard2.nShape) * 10 + (int)iCard2.nIndex;

		return nPoint1 - nPoint2;
	}
}

public class CardShapeComparer : IComparer  {
	int IComparer.Compare( System.Object x, System.Object y )  {
		Card iCard1 = (Card)x;
		Card iCard2 = (Card)y;
		
		int nPoint1 = (int)iCard1.nShape;
		int nPoint2 = (int)iCard2.nShape;
		
		return nPoint1 - nPoint2;
	}
}

public class CardRankComparer : IComparer  {
	int IComparer.Compare( System.Object x, System.Object y )  {
		Card iCard1 = (Card)x;
		Card iCard2 = (Card)y;
		
		int nPoint1 = ((int)iCard1.nRank) * 1000 + (int)iCard1.nShape * 10 + (int)iCard1.nIndex;
		int nPoint2 = ((int)iCard2.nRank) * 1000 + (int)iCard2.nShape * 10 + (int)iCard2.nIndex;;
		
		return nPoint1 - nPoint2;
	}
}


public class Card : MonoBehaviour {
	public enum SHAPE {
		NONE = -1,
		_1 = 0,
		_2 = 1,
		_3 = 2,
		_4 = 3,
		_5 = 4,
		_6 = 5,
		_7 = 6,
		_8 = 7,
		_9 = 8,
		_10 = 9,
		_11 = 10,
		_12 = 11,
		JOKER = 12,
		GHOST = 13
	};

	public enum INDEX {
		NONE = -1,
		_1 = 0,
		_2 = 1,
		_3 = 2,
		_4 = 3,
	};

	public enum RANK {
		NONE = -1,
		KWANG = 0,
		SHAPE = 1,
		STRIPE = 2,
		THREEPI = 3,
		TWOPI = 4,
		PI = 5,
	}

	public enum SPECIAL {
		NONE = -1,
		KWANG = 0,
		GODORI = 1,
		HONGDAN = 2,
		CHUNGDAN = 3,
		CHODAN = 4,
		KUKHWA = 5,
		PIG = 6
	}

	public SHAPE nShape = SHAPE.NONE;
	public INDEX nIndex = INDEX.NONE;
	public RANK nRank = RANK.NONE;
	public SPECIAL nSpecial = SPECIAL.NONE;
	public bool bGhost = false;
	public bool bJoker = false;
	public bool bInHand = false;
	public bool bBomb = false;
	public bool bShake = false;
	public bool bHasMatchedGroundCard = false;
	private bool bSendEvent = false;
	private Animator ani = null;

	static public RANK [,] dRankMap = new RANK[12, 4] {
		{RANK.KWANG, RANK.STRIPE, RANK.PI, RANK.PI},		// 1
		{RANK.SHAPE, RANK.STRIPE, RANK.PI, RANK.PI},		// 2
		{RANK.KWANG, RANK.STRIPE, RANK.PI, RANK.PI},		// 3
		{RANK.SHAPE, RANK.STRIPE, RANK.PI, RANK.PI},		// 4
		{RANK.SHAPE, RANK.STRIPE, RANK.PI, RANK.PI},		// 5
		{RANK.SHAPE, RANK.STRIPE, RANK.PI, RANK.PI},		// 6
		{RANK.SHAPE, RANK.STRIPE, RANK.PI, RANK.PI},		// 7
		{RANK.KWANG, RANK.SHAPE, RANK.PI, RANK.PI},		// 8
		{RANK.SHAPE, RANK.STRIPE, RANK.PI, RANK.PI},		// 9
		{RANK.SHAPE, RANK.STRIPE, RANK.PI, RANK.PI},		// 10
		{RANK.KWANG, RANK.TWOPI, RANK.PI, RANK.PI},		// 11
		{RANK.KWANG, RANK.SHAPE, RANK.STRIPE, RANK.TWOPI}		// 12
	};

	static public SPECIAL [,] dSpecialMap = new SPECIAL[12, 4] {
		{SPECIAL.KWANG, SPECIAL.HONGDAN, SPECIAL.NONE, SPECIAL.NONE},		// 1
		{SPECIAL.GODORI, SPECIAL.HONGDAN, SPECIAL.NONE, SPECIAL.NONE},		// 2
		{SPECIAL.KWANG, SPECIAL.HONGDAN, SPECIAL.NONE, SPECIAL.NONE},		// 3
		{SPECIAL.GODORI, SPECIAL.CHODAN, SPECIAL.NONE, SPECIAL.NONE},		// 4
		{SPECIAL.NONE, SPECIAL.CHODAN, SPECIAL.NONE, SPECIAL.NONE},			// 5
		{SPECIAL.NONE, SPECIAL.CHUNGDAN, SPECIAL.NONE, SPECIAL.NONE},		// 6
		{SPECIAL.PIG, SPECIAL.CHODAN, SPECIAL.NONE, SPECIAL.NONE},				// 7
		{SPECIAL.KWANG, SPECIAL.GODORI, SPECIAL.NONE, SPECIAL.NONE},			// 8
		{SPECIAL.KUKHWA, SPECIAL.CHUNGDAN, SPECIAL.NONE, SPECIAL.NONE},	// 9
		{SPECIAL.NONE, SPECIAL.CHUNGDAN, SPECIAL.NONE, SPECIAL.NONE},		// 10
		{SPECIAL.KWANG, SPECIAL.NONE, SPECIAL.NONE, SPECIAL.NONE},				// 11
		{SPECIAL.KWANG, SPECIAL.NONE, SPECIAL.NONE, SPECIAL.NONE}				// 12
	};

	void Init() {
		ani = GetComponent<Animator> ();
	}

	public void Clear() {
		bInHand = false;
		bBomb = false;
		bShake = false;
		bHasMatchedGroundCard = false;
		bSendEvent = false;
	}

	void Awake() {
		Init ();
	}

	void Start () {
	}
	
	public void Show(bool bShow = true) {
		gameObject.SetActive (bShow);
	}

	public bool IsShow() {
		return gameObject.activeSelf;
	}

	public void Open(bool bOpen = true, bool bEvent = true) {
		if (IsShow()) {
			ani.SetBool ("IsCardOpen", bOpen);

			bSendEvent = bEvent;
		}
	}

	public void OnFinishOpen() {
		if (bSendEvent) {
			GamePlay.GetInstance ().PostEvent_OpenCard (this);
		}
	}

	// Update is called once per frame
	void Update() {
	}

	void OnMouseDown () {
		OnSelected ();
	}

	void OnSelected() {
		GamePlay.GetInstance ().PostEvent_SelectCard (this);
	}
}
