﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI : MonoBehaviour {
	GameManager iGameManager;
	public Player iMe;
	public Player iYou;

	// Use this for initialization
	void Awake() {
		iGameManager = GameManager.GetInstance ();
	}

	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		UpdateUI ();
	}

	void UpdateUI () {
		GameManager.GAME_STATE nGameStatus = iGameManager.GetGameState ();
		Transform trButton_Start = transform.FindChild ("Button_Start");
		if (trButton_Start) {
			trButton_Start.gameObject.SetActive (false);
			if (nGameStatus == GameManager.GAME_STATE.READY) {
				trButton_Start.gameObject.SetActive (true);
			}
		}
	}

	public void OnClickButton_Start() {
		iGameManager.SetGameState (GameManager.GAME_STATE.START);
	}

	public void UpdateScore() {
		Transform trPlayer = transform.Find ("Score/Me");
		if (iMe.iScore.nScore > 0) {
			trPlayer.gameObject.SetActive (true);
			Text iText = trPlayer.Find ("Text").GetComponent<Text>();
			iText.text = "Score : " + iMe.iScore.nScore;
		} else {
			trPlayer.gameObject.SetActive (false);
		}

		trPlayer = transform.Find ("Score/You");
		if (iYou.iScore.nScore > 0) {
			trPlayer.gameObject.SetActive (true);
			Text iText = trPlayer.Find ("Text").GetComponent<Text>();
			iText.text = "Score : " + iYou.iScore.nScore;
		} else {
			trPlayer.gameObject.SetActive (false);
		}
	}

	public void ShowGoOrStopBox(bool bShow = true) {
		Transform trBox = transform.Find ("GoStop");
		trBox.gameObject.SetActive (bShow);
	}

	public void OnClickButton_GO() {
		ShowGoOrStopBox (false);
		GamePlay.GetInstance ().PostEvent_DecideGoOrStop (this);
	}

	public void OnClickButton_Stop() {
		ShowGoOrStopBox (false);
		GamePlay.GetInstance ().PostEvent_DecideGoOrStop (false);
	}
}
