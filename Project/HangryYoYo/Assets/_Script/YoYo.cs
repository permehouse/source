﻿using UnityEngine;
using System.Collections;

public class YoYo : MonoBehaviour {
    public float nRotateSpeed = 1000;
    public float nSpeed = 5;
    float nCurRotateSpeed = 0;
    Quaternion rotNew;
    public GameObject obAttached = null;
    public float nMaxDistance = 100;

	// Use this for initialization
	void Start () {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {

        transform.Rotate(Vector3.forward * nCurRotateSpeed * Time.deltaTime);
        nCurRotateSpeed -= Time.deltaTime;

/*
        rotNew.eulerAngles += transform.forward * nRotateSpeed * Time.deltaTime;
        if ((rotNew.eulerAngles - transform.rotation.eulerAngles).magnitude < 0.01)
        {
            transform.rotation = rotNew;
        }
        else
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, rotNew, nRotateSpeed * Time.deltaTime);
        }
*/


/*
        if (obAttached != null)
        {
            float nDistance = Vector3.Distance(transform.position, obAttached.transform.position);
//            Debug.Log("Distance : " + nDistance);
            if (nDistance > nMaxDistance)
            {
                Vector3 dir = (transform.position - obAttached.transform.position).normalized;
                transform.position = obAttached.transform.position + dir * nMaxDistance;

                Rigidbody2D rb = GetComponent<Rigidbody2D>();
                //      rb.angularVelocity = 100000;
            }
        }*/

	}

    public void SpeedUp()
    {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
//        rb.AddRelativeForce(rb.velocity.normalized * nSpeed); 


  //      rb.angularVelocity = 100000;
        rb.velocity = nSpeed * rb.velocity;
        nCurRotateSpeed = nRotateSpeed;
        //Debug.Log("Vel : " + rb.velocity + ", Mag : " + rb.velocity.magnitude + ", Dist : " + Vector3.Distance(transform.position, obAttached.transform.position));
    }
}
