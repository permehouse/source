----
title: 인게임 캐릭터 아트워크 파이프라인
author: 이태우(oiehot@gmail.com)
date: 2014-04-03
attention: 예제로 쓰인 그림의 퀄리티는 신경쓰지 말아주세요.
----

# 1장. 애니메이션 초안 잡기

## 1) 플래시 실행

<img src="flash_logo.png" />

## 2) 문서 열기

<img src="flash_open_document.png" />

**/sagdoc/design/flash** 에서 해당되는 게임 문서를 열도록 한다.

## 3) 심볼 생성

<img src="flash_create_symbol.png" />

새 동작을 위해 심볼을 만든다.

## 4) 심볼 편집

<img src="flash_edit_symbol.png" />

Library 창에서 심볼을 더블클릭해서 편집모드로 들어간다.

<img src="flash_setup_layer.png" />

레이어의 이름을 세팅한다.

## 5) 러프한 애니메이션 그리기

* 드로잉에 사용하는 도구는 Pencil Tool, Brush Tool.
* Pencil Tool:
  * Pencil Mode 는 Smooth
  * Properties
    * Stroke: 2.0 ~ 2.5 (TODO: 미확정
    * Style: Solid
    * Scale: Normal
    * Hinting: Enable (Paint Bucket 때문)
    * Cap: Round
* Brush Tool:
	* Zoom 100% 에서는 Brush Size 1단계를 사용한다.
	* Zoom 200% 에서는 Brush Size 2단계를 사용한다.
* Object Drawing (J) 은 Disable

## 6) 애니메이션 초안 완성 

<img src="step_1.png" />




# 2장. 게임 리소스화 작업

## 1) 플래시에서 스프라이트 시트 추출

<img src="flash_generate_sprite_sheet.png" />

Library 윈도우에서 추출할 Symbol 위에서 우클릭을 하고 **Generate Sprite Sheet**를 실행한다.

<img src="step_2.png" />

추출에 성공하면 위와 같이 **스프라이트 시트 이미지**와 **프레임 정보**가 담긴 XML(or JSON) 문서를 얻을 수 있다.

## 2) 스프라이트 선화 정리

<img src="step_3.png" />

* 포토샵에서 **스프라이트 시트 이미지**를 열고 그 위에 선을 그려 정리한다.
* **TODO:** 선을 정리할 때 쓰이는 Brush는 -브러시를 이용하고 Size는 -px
* **TODO:** 캐릭터 선 색: *#000000*

## 3) 스프라이트 완성

<img src="step_4.png" />

1. Line: 선 레이어
	* Solid Layer를 사용
	* Normal Blending
	* 100% Opacity
2. Mask
	* 페인트가 캐릭터 영역을 벗어나지 않도록 하는 목적
	* 페인트 레이어들을 그룹으로 묶고 이 그룹 레이어에 마스크를 추가해서 생성.
3. Paint
	* Normal Blending
	* 100% Opacity
	* 캐릭터의 색으로 머리, 피부, 옷 등이 레이어로 나뉘어져 있어야 함
4. Shadow
	* **Multiply** Blending
	* 100% Opacity
	* Solid Color 레이어를 사용.
5. Highlight
	* Normal Blending
	* 100% Opacity
	* Solid Color 레이러를 사용.

## 4) 스프라이트 잘라내기

<img src="step_5.png" />

* **XML, JSON** 데이터를 활용해서 스크립트로 잘라내거나 수작업으로 포토샵에서 한 장 한장 **CUT PASTE SAVE AS PNG**.
* **TODO:** Unity에서 파일 하나로된 스프라이트 시티를 읽어들일 수 있는지 여부 확인 필요. 가능하다면 그림을 자를 필요가 없어지기 때문에 관리소요가 줌.