# 스프라이터(Spriter) 사용 가이드

# 1. 주의사항

1. 모든 String 필드에 한글을 입력하지 않는다.
2. Viewport에 놓여진 Sprite의 중앙 축은 수정하지 않는다.
3. 원본 리소스의 중앙 축 또한 수정하지 않는다.

# 2. 설정하기

1. 타임라인의 ... 버튼을 눌러서 Timeline Snapping Options 창을 띄우고 다음과 같이 설정한다.

	Snapping Interval: 100ms (~10.00fps)
	Enable Snapping: True
	Lock PlaybackFrameRate to Snapping Interval: False
	Ruler Units: Milliseconds

# 3. 추출한 프레임을 정하기

1. 추출할 프레임으로 시간을 옮긴다.
2. 타임라인의 meta data을 더블클릭하여 Edit MetaData 창을 띄운다.
3. Find/Create Variable에서 var_export를 선택하고 active in key 를 켠 뒤 값을 1로 설정한다. (추출할 것이라는 표기)
4. 만약에 var_export 변수가 없다면, Create new Variable 버튼을 선택해 Create New Variable 창을 띄운다.
6. name에 var_export 를 type은 integer를 default value는 0으로 써놓고 Create Variable 버튼을 누른다.

# 4. 애니메이션을 PNG 시퀀스로 저장하기

1. Menu-File-Save to PNG
2. 저장 경로를 선택한다.
3. 파일이름을 ex) alpha_walk, bravo_idle, <캐릭터이름>_<액션명>

4. 다음처럼 설정이 되어있는지를 확인한다.

	source frames:
	  keyframes only: off
	  interval: 100ms

	source rectangle: "time rect to animation"

	output file options: "separate numbered image file"

	output scale:
	  constraint proportions: off
	  width: 100%
	  height: 100%

5. 파일이 정상적으로 저장되었는지를 확인.

	brabo_idle__000.png
	brabo_idle__001.png
	brabo_idle__002.png
	brabo_idle__003.png
	brabo_idle__004.png
	brabo_idle__005.png
	brabo_idle__006.png
	brabo_idle__007.png
	brabo_idle__008.png
	brabo_idle__009.png

# 4. 게임에 쓸 수 있도록 PNG 시퀀스를 정리

1. 명령창에서 다음과 같은 식으로 스크립트를 실행한다.

	> ruby /sag/doc/bin/ruby/scopy.rb