<작성중. 정리안됨>
20140503 OIEHOT

* 캐릭터: Black 외곽선

* 캐릭터가 올라가거나 잡는 소품, 탈것은 외곽선이 있다. 외곽선의 색은 배경인 경우 블랙을 쓰면 안된다.
원경 배경은 외곽선을 넣지 않는다.

* 명암은 부드러운 그라데이션으로 처리한다.

* 솔리드 면의 디테일은 텍스쳐를 통해서 보여준다

1. 영역

영역을 칠할 때는 플랫한 브러시인 Hard Round Pressure Size 을 사용

2. 명암

부드러운 명암을 표현하고 싶을 때는 Soft Round Pressure Size 브러시가 좋으며, 디테일을 표현할 때 사용하는 텍스쳐 브러시를 약한 오퍼시티로 중첩하여 각진 형태를 표현하는것도 괜찮다.

2. 디테일

틈새 명암
표면 디테일을 올릴 때는 적절한 텍스쳐의 브러시를 사용

Brush Tip Shape

  Oil Pastel Large
  Size: 30px
  Spacing: 5%

Shape Dynamics
  Size Jitter: 0%
    Control: Pen Pressure

Texture
  Surface: Oil Paster Light
  Scale: 50%
  Brightness: 100
  Contrast: 50
  Texture Each Tip: On
  Mode: Color Burn
  Depth: 60%
  Depth Jitter: 40%

Dual Brush
  Shape: Sampled Tip 90
  Size: 90px
  Spacing: 30%
  Scatter: 0%
  Count: 1

Noise: Off

Smoothing: On
