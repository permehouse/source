#!/usr/bin/ruby

require 'rubygems'
require 'json'
require 'fileutils'

Debug = false
Fps = 30
def log(s)
  puts s if Debug
end

class Float
  def to_frame
    (self * Fps).round
  end
end

class Hash
  def method_missing m
    self[m.to_s]
  end
end

class SpineProject
  attr_accessor :data, :project_path

  def to_export_filename(skeleton, skin, anim, frame)
    "#{skeleton}-#{skin}-#{anim}#{frame}.png"
  end

  def initialize(path)
    @data = {}
    @project_path = path
    Dir.foreach(path) do |filename|
      if filename =~ /([a-z]+).json/
        importJson(path+'/'+filename)
      end
    end
  end

  # JSON 쇱 遺댁 data瑜 異異⑸.
  def importJson(path)
    puts "importJson(#{path})"
    f = File.new(path,'r')
    json = JSON.parse(f.read)

    # 쇰쇰 遺 Skeleton 紐 살듬.
    skeleton_name = File.basename(path, ".json")
    @data[skeleton_name] = {}

    #########################
    # ㅽ 蹂대 紐⑥. #
    #########################
    @data[skeleton_name][:skins] = []

    json["skins"].each_key do |skin_name|
      next if skin_name == "default"
      @data[skeleton_name][:skins] << skin_name
    end

    ###############################
    # 硫댁 蹂대 紐⑥. #
    ###############################
    @data[skeleton_name][:animations] = {}

    json["animations"].each do |ani_name, anim|

      # 硫댁 湲몄대 .
      anim_length = 0.0
      anim["bones"].each do |bone_name, bone|
        bone.each do |action_name, action|
          action.each do |key|
            time = key["time"].to_f
            anim_length = time if time > anim_length
          end
        end
      end

      @data[skeleton_name][:animations][ani_name] = {
        :length_ms => anim_length,
        :length_frame => anim_length.to_frame,
        :render_frames => []
      }

      # 異異  踰몃 살대.
      for n in 0...json["animations"][ani_name]["events"].length
        event = json["animations"][ani_name]["events"][n]
        if event["name"] == "render"
          time = event["time"].to_f
          @data[skeleton_name][:animations][ani_name][:render_frames] << {
            :order => n,
            :time_ms => time,
            :time_frame => time.to_frame
          }
        end
      end
    end

    # 硫댁  媛 怨고⑸.
    @data[skeleton_name][:animations].each do |ani_name, anim|
      rf = anim[:render_frames]
      for n in 0...(rf.length)
        delay = 0.0
        if n+1 >= rf.length
          delay = anim[:length_ms] - rf[n][:time_ms]
        else
          delay = rf[n+1][:time_ms] - rf[n][:time_ms]
        end
        rf[n][:delay_ms] = delay
        rf[n][:delay_frame] = delay.to_frame
      end
    end
  end

  def to_json
    JSON.pretty_generate(@data)
  end

  def to_gamejson
    json = {}
    @data.each do |skeleton_name, skeleton|
      skeleton[:animations].each do |animation_name, animation|
        if animation_name =~ /([a-z]+)_([a-z]+)/
          game_name, action_name = $1, $2
          keyword = "#{game_name.capitalize}_#{skeleton_name.capitalize}_All_#{action_name.capitalize}".to_sym
          json[keyword] = []
          animation[:render_frames].each do |rf|
            json[keyword] << rf
          end
        end
      end
    end
    JSON.pretty_generate(json)
  end

  def export_timing(export_path)
    f = File.new(export_path + '/AnimationTiming.json', "w")
    f.syswrite(to_gamejson)
  end

  def export_sequences(export_path)
    @data.each do |skeleton_name, skeleton|
      skeleton[:skins].each do |skin_name|
        skeleton[:animations].each do |anim_name, anim|
          anim[:render_frames].each do |frame|
            if anim_name =~ /([a-z]+)_([a-z]+)/
              game_name, action_name = $1, $2
              src_path = "#{@project_path}/#{skeleton_name}-#{skin_name}-#{anim_name}#{frame[:time_frame].to_s}.png"
              dst_path = "#{export_path}/#{game_name.capitalize}/#{skeleton_name.capitalize}/#{skin_name.capitalize}/#{skin_name.capitalize}_#{action_name.capitalize}_#{frame[:order].to_s}.png"
              if File.exists?(src_path)
                FileUtils.mkdir_p(File.dirname(dst_path))
                FileUtils.cp(src_path, dst_path)
              end
            end
          end
        end
      end
    end
  end

  def export(path)
    FileUtils.mkdir_p(path)
    export_sequences(path)
    export_timing(path)
  end
end

project = SpineProject.new('d:/sagdoc/design/spine/export')
project.export 'd:/sagdoc/design/export'