using UnityEngine;
using System.Collections;

public class UICharactorPet
{
	static public ArrayList charactorInfoList = new ArrayList();
	static public ArrayList petInfoList = new ArrayList();
	
	static public ArrayList userCharactorInfoList = new ArrayList();
	
	// 처음 가지고 있는 캐릭터와 물개 세팅. 차후에는 파일로드로 구현해야 함.
	static public string selectedCharacterCode = "CH_DEFAULT_C";
	static public string selectedRelayCharacterCode = "";
	static public string selectedPetCode = "PET_SEAL";			// 물개.
	static public string selectedCharacterExpressionName = "그냥 고래";			// yj edit.
	static public string selectedExpressionPet_1_Name = "";
	
	
	public enum CHARACTOR_TYPE
	{
		CHARACTOR_DEFAULT = 0,
		CHARACTOR_SALABLE,
		CHARACTOR_UPGRADED,
	}	
	
	public struct CharactorInfo
	{
//		public string charactorID;
		public string CharacterCode;		
		public string charactorName;
		public CHARACTOR_TYPE charactorType;
		public string charactorIconName;
		public int animationFramesPerSecond;
		public string animationNamePrefix;
		public int priceJewel;
		public int priceFish;
		public char grade;
		public string charactorExpressionName;
		public string charactorDescription;
		
		public int nPower;
		public int nSpeed;
		public int nSkill;

		public CharactorInfo(string _CharacterCode, string _charactorName, CHARACTOR_TYPE _charactorType, string _charactorIconName, int _animationFramesPerSecond, string _animationNamePrefix, int _priceJewel, int _priceFish, char _grade, string _charactorExpressionName, string _charactorDescription,int _power,int _speed,int _skill)
		{
//			charactorID = _charactorID;
			CharacterCode = _CharacterCode;
	
			charactorName = _charactorName;
			charactorType = _charactorType;
			charactorIconName = _charactorIconName;
			
			animationFramesPerSecond = _animationFramesPerSecond;
			animationNamePrefix = _animationNamePrefix;
			
			priceJewel = _priceJewel;
			priceFish = _priceFish;
			grade = _grade;
			charactorExpressionName = _charactorExpressionName;
			charactorDescription = _charactorDescription;
			
			nPower = _power;
			nSpeed = _speed;
			nSkill = _skill;
			
			
		}
	};
	
	public struct PetInfo
	{
//		public string petID;
		public string petCode;
		public string petName;
		public int animationFramesPerSecond;
		public string animationNamePrefix;
		public int price;
		public char grade;
		public string petExpressionName;
		public string petDescription;

		public PetInfo(string _petCode, string _petName, int _animationFramesPerSecond, string _animationNamePrefix,  int _price, char _grade, string _petExpressionName, string _petDescription)
		{
//			petID = _petID;
			petCode = _petCode;
			petName = _petName;
			animationFramesPerSecond = _animationFramesPerSecond;
			animationNamePrefix = _animationNamePrefix;
			price = _price;
			grade = _grade;
			petExpressionName = _petExpressionName;
			petDescription = _petDescription;
		}
	};	
	
	//유저가 보유한 캐릭터 리스트.
	static public void InitUIUserCharactor()
	{
		Debug.Log ("init 1");
		userCharactorInfoList.Clear();
		
		//userCharactorInfoList.Add(new CharactorInfo("CH_POLAR_B", "Yudangtang", CHARACTOR_TYPE.CHARACTOR_DEFAULT, "CharactorPolarBearIcon", 5, "CharactorPolarBearAni_", 0, 45, 'C', "우당탕", "힘30 속30  기30\n그냥 원시소년"));
		//userCharactorInfoList.Add(new CharactorInfo("CH_DEFAULT_C", "Karr", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorWhaleIcon", 5, "CharactorWhaleAni_", 0, 0, 'B', "까르르", "힘50 속30  기30\n무식한 원시소녀"));		
		//userCharactorInfoList.Add(new CharactorInfo("CH_PORORO_A", "Warr", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorPororoIcon", 5, "CharactorPororoAni_", 0, 60, 'B', "와르르","힘30 속30  기50\n똑똑한 원시소년"));
		
		
		//userCharactorInfoList.Clear();
		
		//userCharactorInfoList.Add(new CharactorInfo("CH_POLAR_B", "Yudangtang", CHARACTOR_TYPE.CHARACTOR_DEFAULT, "CharactorPolarBearIcon", 5, "CharactorPolarBearAni_", 0, 45, 'C', "우당탕", "힘30 속30  기30\n그냥 원시소년"));
		//userCharactorInfoList.Add(new CharactorInfo("CH_DEFAULT_C", "Karr", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorWhaleIcon", 5, "CharactorWhaleAni_", 0, 0, 'B', "까르르", "힘50 속30  기30\n무식한 원시소녀"));		
		//userCharactorInfoList.Add(new CharactorInfo("CH_PORORO_A", "Warr", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorPororoIcon", 5, "CharactorPororoAni_", 0, 60, 'B', "와르르","힘30 속30  기50\n똑똑한 원시소년"));

		
	}
	
	static public void InitUIUserCharactor2()
	{
		Debug.Log ("init 2");
		userCharactorInfoList.Clear();
		
		//userCharactorInfoList.Add(new CharactorInfo("CH_POLAR_B", "Yudangtang", CHARACTOR_TYPE.CHARACTOR_DEFAULT, "CharactorPolarBearIcon", 5, "CharactorPolarBearAni_", 0, 45, 'C', "우당탕", "힘30 속30  기30\n그냥 원시소년"));
		//userCharactorInfoList.Add(new CharactorInfo("CH_DEFAULT_C", "Karr", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorWhaleIcon", 5, "CharactorWhaleAni_", 0, 0, 'B', "까르르", "힘50 속30  기30\n무식한 원시소녀"));		
		//userCharactorInfoList.Add(new CharactorInfo("CH_PORORO_A", "Warr", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorPororoIcon", 5, "CharactorPororoAni_", 0, 60, 'B', "와르르","힘30 속30  기50\n똑똑한 원시소년"));
	}
	
	//HOME 버튼 누르면 실행됨 : 현재 소유하고 있는 유저 캐릭터를 보여준다.
	static public void InitTotalUserCharactor()
	{		
		userCharactorInfoList.Clear();
				
		/*
		for (int i=0; i< GlobalValues.userTotalCharactor.count ;i++)
		{
			switch (GlobalValues.userTotalCharactor.characterCode[i])
			{
				case "CH_DEFAULT_C":
					userCharactorInfoList.Add(new CharactorInfo("CH_DEFAULT_C", "Karr", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorWhaleIcon", 5, "CharactorWhaleAni_", 0, 0, 'B', "까르르", "힘50 속30  기30\n무식한 원시소녀"));
					break;				
				case "CH_POLAR_A":
					userCharactorInfoList.Add(new CharactorInfo("CH_POLAR_B", "Yudangtang", CHARACTOR_TYPE.CHARACTOR_DEFAULT, "CharactorPolarBearIcon", 5, "Wa_Stand_", 0, 45, 'C', "우당탕", "힘30 속30  기30\n그냥 원시소년"));
					break;				
				case "CH_PORORO_A":
					userCharactorInfoList.Add(new CharactorInfo("CH_PORORO_A", "Warr", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorPororoIcon", 5, "CharactorPororoAni_", 0, 60, 'B', "와르르","힘30 속30  기50\n똑똑한 원시소년"));
					break;
				case "CH_MOBYDICK_B":
					userCharactorInfoList.Add(new CharactorInfo("CH_MOBYDICK_B", "Baing", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorMobydickIcon", 5, "CharactorMobydickAni", 0, 50, 'B', "빠잉","힘30 속50  기30\n재빠른 원시소녀"));
					break;
				case "CH_TURTLE_B":
					userCharactorInfoList.Add(new CharactorInfo("CH_TURTLE_B", "Yamyam", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorTurtleIcon", 5, "CharactorTurtleAni", 0, 70, 'A', "얌얌","힘40 속40  기40\n만능 원시소년"));				
					break;
			}			
		}
		
		*/		
		//userCharactorInfoList.Add(new CharactorInfo("CH_POLAR_B", "Yudangtang", CHARACTOR_TYPE.CHARACTOR_DEFAULT, "CharactorPolarBearIcon", 5, "CharactorPolarBearAni_", 0, 45, 'C', "우당탕", "힘30 속30  기30\n그냥 원시소년"));
		//userCharactorInfoList.Add(new CharactorInfo("CH_PORORO_A", "Warr", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorPororoIcon", 5, "CharactorPororoAni_", 0, 60, 'B', "와르르","힘30 속30  기50\n똑똑한 원시소년"));
	}
	
	//유저가 보유한 펫 리스트.
	static public void InitUIUserPet()
	{
	}
		
	//상점에 전시되어 있는 캐릭터 리스트.
	static public void InitUICharactorPet()
	{
		//Debug.Log ("Init UI Charactor & pet");
		charactorInfoList.Clear();
		
		
		
		charactorInfoList.Add(new CharactorInfo("CH_DEFAULT_C", "Kya", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorWhaleIcon", 16, "Kya_Stand_", 0, 0, 'B', "까르르", "힘쎈 원시소녀",50,30,30));		
		charactorInfoList.Add(new CharactorInfo("CH_PORORO_A", "Wa", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorPororoIcon", 16, "Wa_Stand_", 0, 60, 'B', "와르르","똑똑한 원시소년",30,30,50));
		
		charactorInfoList.Add(new CharactorInfo("CH_POLAR_B", "Yoo", CHARACTOR_TYPE.CHARACTOR_DEFAULT, "CharactorPolarBearIcon", 16, "Woo_Stand_", 0, 45, 'B', "우당탕", "발빠른 원시소년",30,50,30));
		
		charactorInfoList.Add(new CharactorInfo("CH_MOBYDICK_B", "Ppa", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorWhaleIcon", 16, "Ppa_Stand_", 0, 50, 'B', "빠잉","만능 원시소녀",40,40,40));
		charactorInfoList.Add(new CharactorInfo("CH_TURTLE_B", "Yam", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorTurtleIcon", 16, "Yam_Stand_", 0, 70, 'A', "얌얌","만능 원시소년",40,40,40));				
				
		
		/*
		petInfoList.Clear();
		petInfoList.Add(new PetInfo("PET_SHARK_C", "PetShark", 5, "PetSharkAni_", 10,  'C', "아기 상어", "소형 적 사냥"));		
		petInfoList.Add(new PetInfo("PET_SEAL_C", "PetWalrus", 5, "PetWalrusAni_", 15, 'C', "작은 물개", "8초 마다 돌진하여\n모든 적 공격"));			//바다 코끼리.		
		petInfoList.Add(new PetInfo("PET_JELLYFISH_C", "PetJellyfish", 5, "PetJellyfishAni_", 15, 'C', "작은 해파리", "3초 마다 물고기\n3마리 증가"));
		petInfoList.Add(new PetInfo("PET_MERMAID_A", "PetPeoplefish", 5, "PetPeoplefishAni_", 20, 'C', "소녀 인어", "20초 마다\n작은 유혹"));
		petInfoList.Add(new PetInfo("PET_RAY_C", "PetRay", 5, "PetRayAni_", 15, 'C', "가오리", "거대화 시간 증가"));
		petInfoList.Add(new PetInfo("PET_PENGUIN_B", "PetPenguin", 5, "PetPenguinAni_", 10, 'C', "펭귄", "거대 펭귄 변신"));
		petInfoList.Add(new PetInfo("PET_SQUID_B", "PetSquid", 5, "PetSquidAni_", 15, 'C', "오징어", "수혈 에너지 증가"));
		petInfoList.Add(new PetInfo("PET_STARFISH_C", "PetStarfish", 5, "PetStarfishAni_", 15, 'C', "불가사리", "아이템 사용시간 증가"));
		petInfoList.Add(new PetInfo("PET_TOPSHELL_C", "PetTopshell", 5, "PetTopshellAni_", 20, 'C', "소라", "아이템 생성"));
		petInfoList.Add(new PetInfo("PET_CLAM_C", "PetClam", 5, "PetClamAni_", 15, 'C', "조개", "경험치 상승"));
		*/		
	}
	
	static public CharactorInfo GetCharacterInfoFromCode(string strCharacterCode)
	{
		CharactorInfo character = (CharactorInfo)charactorInfoList[0];
//		if("CH_DEFAULT" != strCharacterID && "NONE" != strCharacterID)
		if("NONE" != strCharacterCode)
		{
			for(int i = 0;i<charactorInfoList.Count;++i)
			{
				if(strCharacterCode == ((CharactorInfo)charactorInfoList[i]).CharacterCode)
				{
					character = (CharactorInfo)charactorInfoList[i];
					break;
				}
			}
		}

		return character;
	}
	
	static public PetInfo GetPetInfoFromCode(string strPetCode)
	{
		PetInfo pet = (PetInfo)petInfoList[0];
		for(int i = 0;i<petInfoList.Count;++i)
		{
			if(strPetCode == ((PetInfo)petInfoList[i]).petCode)
			{
				pet = (PetInfo)petInfoList[i];
				break;
			}
		}

		return pet;
	}
	
	/*
	static public Player.PlayerType GetCurrentInGameCharactor()
	{
		return GetGameCharactorType(selectedCharacterCode);
	}

	static public Player.PlayerType GetCurrentInGameRelayCharactor()
	{
		Debug.Log(">>> Get Relay Character Type");
		Player.PlayerType nType = GetGameCharactorType(selectedRelayCharacterCode);

		if(nType == Player.PlayerType.none) {
			GlobalValues.gameBoost.nCount_Relay = 0;
		}
		else {
			GlobalValues.gameBoost.nCount_Relay = 1;
		}
		
		return nType;
	}
	
	static public Player.PlayerType GetGameCharactorType(string strCharacterCode) 
	{
		if(strCharacterCode == "CH_DEFAULT_C") {
			Debug.Log("Player.PlayerType.whale");
			return Player.PlayerType.whale;
		}
		else if(strCharacterCode == "CH_POLAR_B") {
			Debug.Log("Player.PlayerType.polarbear");
			return Player.PlayerType.polarbear;
		}
		else if(strCharacterCode == "CH_PORORO_A") {
			Debug.Log("Player.PlayerType.pororo");
			return Player.PlayerType.pororo;
		}
		else if(strCharacterCode == "CH_MOBYDICK_B") {
			Debug.Log("Player.PlayerType.mobydick");
			return Player.PlayerType.mobydick;			
		}
		else if(strCharacterCode == "CH_TURTLE_B") {
			Debug.Log("Player.PlayerType.turtle");
			return Player.PlayerType.turtle;			
		}
		else if(strCharacterCode == "CH_OCTOPUS_B") {
			Debug.Log("Player.PlayerType.octopus");
			return Player.PlayerType.octopus;			
		}
		
		Debug.Log("Player.PlayerType.none");
		return Player.PlayerType.none;
	}

	static public Pet.PetType GetCurrentInGamePet()
	{
		if("" == selectedPetCode)
		{
			Debug.Log("Pet id is null");
			return Pet.PetType.none;
		}
		//차후 수정.
		
		if(selectedPetCode == "PET_SHARK_C") {
			Debug.Log("Pet.PetType.littleshark");
			return Pet.PetType.littleshark;
		}
		else if(selectedPetCode == "PET_SEAL_C") {
			Debug.Log("Pet.PetType.walrus");
			return Pet.PetType.walrus;			
		}
		else if(selectedPetCode == "PET_JELLYFISH_C") {
			Debug.Log("Pet.PetType.jellyfish");
			return Pet.PetType.jellyfish;			
		}
		else if(selectedPetCode == "PET_MERMAID_A") {
			Debug.Log("Pet.PetType.mermaid");
			return Pet.PetType.mermaid;			
		}		
		else if(selectedPetCode == "PET_TOPSHELL_C") {
			Debug.Log("Pet.PetType. topshell");
			return Pet.PetType.topshell;			
		}
		else if(selectedPetCode == "PET_STARFISH_C") {
			Debug.Log("Pet.PetType starfish");
			return Pet.PetType.starfish;			
		}
		else if(selectedPetCode == "PET_SQUID_B") {
			Debug.Log("Pet.PetType squid");
			return Pet.PetType.squid;			
		}
		else if(selectedPetCode == "PET_RAY_C") {
			Debug.Log("Pet.PetType ray");
			return Pet.PetType.ray;			
		}
		else if(selectedPetCode == "PET_PENGUIN_B") {
			Debug.Log("Pet.PetType penguin");
			return Pet.PetType.penguin;			
		}
		else if(selectedPetCode == "PET_CLAM_C") {
			Debug.Log("Pet.PetType clam");
			return Pet.PetType.clam;			
		}
		
		Debug.Log("Default - Pet.PetType.none");
		return Pet.PetType.none;		
		*/
/*		
		switch(selectedPetCode[0])
		{
		case '0':
			Debug.Log("Pet.PetType.none");
			return Pet.PetType.none;
		case '1':
			Debug.Log("Pet.PetType.littleshark");
			return Pet.PetType.littleshark;
		case '2':
			Debug.Log("Pet.PetType.walrus");
			return Pet.PetType.walrus;
		case '3':
			Debug.Log("Pet.PetType.jellyfish");
			return Pet.PetType.jellyfish;
//		case '4':
//			Debug.Log("Pet.PetType.PetConch");
//			return Pet.PetType.PetConch;
//		case '5':
//			Debug.Log("Pet.PetType.PetPenguin");
//			return Pet.PetType.PetPenguin;
//		case '6':
//			Debug.Log("Pet.PetType.PetStarfish");
//			return Pet.PetType.PetStarfish;
//		case '7':
//			Debug.Log("Pet.PetType.PetCoral");
//			return Pet.PetType.PetCoral;
		case '8':
			Debug.Log("Pet.PetType.mermaid");
			return Pet.PetType.mermaid;
		default:
			Debug.Log("Default - Pet.PetType.none");
			return Pet.PetType.none;
		}
*/		
//	}

}
