using UnityEngine;
using System.Collections;

public class GlobalValues
{
	static public UserValues userValues = new UserValues();
	
	static public UserCharactor userCharactor = new UserCharactor();
	
	static public UserTotalCharactor userTotalCharactor = new UserTotalCharactor();
	
	static public GameUser gameUser = new GameUser();
	static public ArrayList friendList = new ArrayList();
	public struct GameContact
	{
		public int usid;		
		public string name;
		public string nickname;
		public string character;
		public int record;
		//public int digestFishes;
		//public int weekDistance;
		//public int week;
		//public string imageUrl;
		//public int chanceReceive;
		//public int pushReceive;
		//public bool isDead;
	}
	
	public class GameUser
	{
		// 삭제함. Grade 포함된 ID 를 정식 식별자로 보고, 기존의 characterCode/petCode 으로 처리함.
//		public string characterGradeCode;				//1128
//		public string petGradeCode;
		
		//public string grade;						//yj edit//1128
		public int level;						//yj edit
		public string expressionName;			//yj edit
		public string expressionPet_1_Name;		//yj edit
		public string expressionPet_2_Name;		//yj edit
		
		public int totalEarnedFishes;			//환경설정 추가.
		public int totalInterceptFishes;		//환경설정 추가.
		public int totalLoseFishes;				//환경설정 추가.
		public int totalGameCount;				//환경설정 추가.
		
		public string name;
		public string email;
		public string characterCode;
		public string petCode;
		public int jewels;
		public int fishes;
		public int digestFishes;
		public int digestTimer;
		public System.DateTime digestUpdateTime;
		public int chance;
		public int chanceTimer;
		public System.DateTime chanceUpdateTime;
		public int bestScore;
		public int bestDistance;
		public int weekScore;
		public int weekDistance;
		public int week;
		public string imageUrl;
//		public int chanceReceive;
//		public int sound;
		public string lastLogin;
		public int loginCount;
		public string writeDate;
		public string modifyDate;
		public ArrayList gameCharacters;	//GameCharacter[]
		public ArrayList gamePets;			//GamePet[]
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public class UserValues		// 사용자 가 선택 등 게임 중에 하는행위. 편의를 위한 변수. 
	{
		
		public bool isLogin = false;	//0629
		
		public string id;		// 사용자의 현재 ID.
			
		public bool titleClick=false;		// false = 처 음시.작. true = 게 임플레이.중...
		
		
		
		public string chooseBtnPayment;		//보석,골드,기회 중에 선택한것.
		
		
		
		
		
		//캐릭터,펫,장비 중에 어떤 Tab을  선택하였는지 정보 "Pet","Charactor","Item". 
		
		
		
		
//		public string chooseBuyGicode;				//  더이상 안쓰임. 캐릭터,펫,아이템 중에서 구매하려고 선택한 특정 상품의 gicode.
		public string chooseBuyGigCode;				// 등급 값이 저장된 코드.
		
		public int chooseBuyGrade;					//등급.		0 = C , 1 = B, 2 = A, 3 = S.
		public int chooseBuyLevel;					//레벨.
		public string chooseCharactorDialogMode;	//캐릭터 선택 or 릴레이 선택 중 하나.
		public int chanceUpdated = 0;				// 0 = no update , 1= update 로 별이 max 로 변경.
		
	}
	/*
	public enum Character {
		DEFAULT=0,
		DEFAULT_FRIEND = 1,
		KYA=0,
		WA,
		WOO,
		PPA,
		YAM,
		COUNT
	}
	*/
	public class UserCharactor		
	{		
		public GameObject[] objCharactor = new GameObject[200];		//200 개의 캐릭터 보유가능. 수정.//home 캐릭터.		
		public GameObject[] objMenuCharactor = new GameObject[200];		//200 개의 캐릭터 보유가능. 메인메뉴와 각 종목 전시용 캐릭터.
		
		
		
		
		
		public bool menuOnEnable = false;
		
		
		public bool onEnable = false;
		
		
		
		public int mode;
		
		
		
		public int menuCH = 0;
		public int throwCH 	= 0;
		public int archeryCH = 0;
		public int boxCH  = 0;
		
		public int currentGAMEMODE =0;
		
		public int MODE_MENU =  0;
		public int MODE_SUBMENU =  1;
		
		
		
		
		
		
		public const int GAME_THROW = 0;
		public const int GAME_ARCHERY = 1;
		public const int GAME_BOXING = 2;
		public const int GAME_SHOOT = 3;
		public const int GAME_SWIM = 4;
		public const int GAME_5EVENT = 5;
		
		
		public int currentEvent = 0; //현재 종목.  
	
		
		//각 종목별 캐릭터.
		public int chMenu = 0;
		public int chThrow = 0;
		public int chArchery = 0;
		public int chBoxing = 0;
		public int chShoot = 0;
		public int chSwim = 0;
		public int ch5Event = 0;
		
		/*
		//각 종목별 캐릭터.
		public Character  chThrow = 0;
		public Character chArchery = 0;
		public Character chBoxing = 0;
		public Character chShoot = 0;
		public Character chSwim = 0;
		public Character ch5Event = 0;
		*/
		
		//각 캐릭터 번호.
		public int CH_KYA = 0;
		public int CH_WA = 1;
		public int CH_WOO = 2;
		public int CH_PPA = 3;
		public int CH_YAM = 4;
		
		
		
		
	}
	
	public class UserTotalCharactor		
	{		
		public int count;			//전체 캐릭터 개수.
		
		
		
		
		public int[] power = new int[1000];
		public int[] skill = new int[1000];
		public int[] speed = new int[1000];
		
		public string[] characterCode = new string[1000];
		
		
		
		
		
		
		
			
		
		
		
		
		// 힘, 기, 속, // 레벨 , 사용레벨. 
	}

	// Server Communication
	public static string strHostName = "218.54.31.44:8080";
	
	static public string sessionId = "";
	static public string tokenId = "";
	static public string errorCodeConnector ="";
	static public string errorMessageConnector ="";
}
