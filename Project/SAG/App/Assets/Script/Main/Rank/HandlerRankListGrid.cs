using UnityEngine;
using System.Collections;

public class HandlerRankListGrid : MonoBehaviour
{
	public GameObject prefabRankListItem;

	public HandlerRankListGrid()
	{
	}

	void Start ()
	{
		
		GetFriendList();	//수정. 임시.
		
	}
	
	void Update ()
	{
	
	}
	
	protected void GetFriendList()
	{
		
		//수정, 서버에서 값 가져와야 함. ServerConnector   QueryFriendList.
		// 친구의 정보. 
		
		/*
		
		GlobalValues.GameContact contact = new GlobalValues.GameContact();
				
		GlobalValues.friendList.Clear();
		
		contact.guid = 1;
		contact.email = "yjpark@funy";
		contact.name = "yjkim";
		contact.character = "CH_PORORO_A";			//캐릭터 코드. 중요함. 캐릭터 Icon 을 결정한다.
		contact.imageUrl = "www.funnysoft.co.kr";
		contact.chanceReceive = 5;
		contact.pushReceive = 1;
		contact.weekScore = 12345;
		contact.week = 240;		
		
		contact.digestFishes = 1;
		contact.weekDistance = 100;
		
		GlobalValues.friendList.Add(contact);
		
		*/
				
		Debug.Log ("Get Friend List");
					
		ServerConnector.CallbackResult cbAllResult = delegate(bool nResult) {	
			Debug.Log ("Allraingking okok = "+nResult);
			
			if (nResult)
				DrawFriendList();
			else
			{
				Debug.Log("Get friend list error. code:" + nResult);
			}
			
		};				
		
		ServerConnector connector = ServerConnector.GetInstance();
		connector.All_Ranking_Throw(cbAllResult);
				
		
		
	}

	protected void DrawFriendList()
	{
		Debug.Log ("draw friend list Count = "+GlobalValues.friendList.Count);
				
		RemoveAllChildren();
		int iInsertedIndex = 0;
		int i = 0;
		for (i = 0; i < GlobalValues.friendList.Count; ++i)
		{
			GlobalValues.GameContact contact = (GlobalValues.GameContact)GlobalValues.friendList[i];
			/*
			 if(contact.week != GlobalValues.gameUser.week)
				continue;
				*/

			GameObject obj = Instantiate(prefabRankListItem, new Vector3(0.0f, 0.0f, -0.004f), Quaternion.identity) as GameObject;
			
			obj.transform.parent = this.transform;
			obj.transform.localScale = new Vector3(1f, 1f, 1f);
			HandlerRankListItem itemHandler = obj.GetComponentInChildren<HandlerRankListItem>();
			itemHandler.SetItemInfo(iInsertedIndex, iInsertedIndex+1, iInsertedIndex);
			++iInsertedIndex;
		}
		GetComponent<UIGrid>().Reposition();
	}
	
	protected void RemoveAllChildren()
	{
	
		
	//	Debug.Log ("remove all chi");
		
		HandlerRankListItem[] children = GetComponentsInChildren<HandlerRankListItem>();
		foreach(HandlerRankListItem childObj in children)
		{
			Debug.Log ("destroy Immediat");
			DestroyImmediate(childObj.gameObject);
		}
		transform.DetachChildren();
		
	}


}
