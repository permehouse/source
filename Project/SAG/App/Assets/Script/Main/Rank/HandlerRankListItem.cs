using UnityEngine;
using System.Collections;

public class HandlerRankListItem : MonoBehaviour
{
	private int m_iKakaoID;
	private int m_iRank;
	private int m_iFriendListID;
	private bool m_bIsMe;
	
	public HandlerRankListItem()
	{
		InitData();
	}
	
	void Start ()
	{
	
	}

	void Update ()
	{
	
	}
	
	private void InitData()
	{
		m_iKakaoID = -1;
		m_iRank = -1;
		m_iFriendListID = -1;
		m_bIsMe = false;
	}
	
	//기회 선물 하기 버튼 누름. 
	public void OnClickPresentLife()
	{
//		HandlerRankListItem itemHandler = this.transform.parent.GetComponentInChildren<HandlerRankListItem>();

		//Debug.Log(string.Format("Hello? My number is {0}, {1}", m_iKakaoID, m_bIsMe));
	}
	
	public void SetItemInfo(int iKakaoID, int iRank, int iFriendListID)
	{
		//Debug.Log ("Set Rank Item Info");
		
		m_iKakaoID = iKakaoID;
		m_iFriendListID = iFriendListID;
		m_iRank = iRank;
		
		//Debug.Log ("m_iFriendListID = "+m_iFriendListID);
		GlobalValues.GameContact contact = (GlobalValues.GameContact)GlobalValues.friendList[m_iFriendListID];
		m_bIsMe = (contact.name == GlobalValues.gameUser.name);
		
		
		
		Debug.Log ("## contact.name = "+contact.name);
		Debug.Log ("## GlobalValues.gameUser.name = "+GlobalValues.gameUser.name);
		
		
//		m_bIsMe = (contact.email == GlobalValues.gameUser.email);		//0622.
		//m_bIsMe = false;
		//Profile image
		/*
		if(contact.imageUrl != "")
		{
			StartCoroutine(SetProfilePic(ServerConnector.GetInstance().strDomainName + contact.imageUrl));
		}*/

		//Item background, Rank number
		UISprite[] spriteChildren = GetComponentsInChildren<UISprite>();
		foreach(UISprite spriteItem in spriteChildren)
		{
			/*
			if(spriteItem.name == "RankNumber")
			{
				//Debug.Log ("m_iRank.ToString = "+m_iRank.ToString());
				spriteItem.spriteName = "CommonRankNo_" + m_iRank.ToString();
				
			}*/	
			
			//GlobalValues.friendList.Count
			
			if(spriteItem.name == "imgMedal")
			{
				// 1, 5,, 10 ,20 ,40 %에 따라 각각 메달 수여.
				if ( 100 * m_iRank / GlobalValues.friendList.Count <= 1)
					spriteItem.spriteName = "Medal_5";
				else if ( 100 * m_iRank / GlobalValues.friendList.Count <= 5)
					spriteItem.spriteName = "Medal_4";
				else if ( 100 * m_iRank / GlobalValues.friendList.Count <= 10)
					spriteItem.spriteName = "Medal_3";
				else if ( 100 * m_iRank / GlobalValues.friendList.Count <= 20)
					spriteItem.spriteName = "Medal_2";
				else if ( 100 * m_iRank / GlobalValues.friendList.Count <= 40)
					spriteItem.spriteName = "Medal_1";
				else
					spriteItem.spriteName = "Medal_0";
				
				if (m_iRank == 1) //1위인데 금메달 이상이 아닌 경우. 
				{
					if (spriteItem.spriteName == "Medal_2" || spriteItem.spriteName == "Medal_1" || spriteItem.spriteName == "Medal_0")
						spriteItem.spriteName = "Medal_3";
					
				}
				else if (m_iRank == 2) //2위인데 은메달 이상이 아닌 경우. 
				{
					if (spriteItem.spriteName == "Medal_1" || spriteItem.spriteName == "Medal_0")
						spriteItem.spriteName = "Medal_2";
				}
				else if (m_iRank == 3) //3위인데 동메달 이상이 아닌 경우. 
				{
					if (spriteItem.spriteName == "Medal_0")
						spriteItem.spriteName = "Medal_1";
				}
				
			}
			else if(spriteItem.name == "imgUserIcon")
			{
				//Debug.Log ("m_iRank.ToString = "+m_iRank.ToString());
				if (m_iRank <= 8)
					spriteItem.spriteName = "nail" + m_iRank.ToString();
				else
					spriteItem.spriteName = "nail" + (Random.Range(0,8)).ToString();
			}
			else if(spriteItem.name == "RankItemBGNormal")
			{
				spriteItem.gameObject.SetActive(!m_bIsMe);
			}
			else if(spriteItem.name == "RankItemBGMe")
			{
				spriteItem.gameObject.SetActive(m_bIsMe);
			}			
			else if(spriteItem.name == "imgCharactorIcon")
			{
				//TODO solmea, Always show now, because charactor id is allmost -1 yet.
				spriteItem.gameObject.SetActive(true);
				UICharactorPet.CharactorInfo charactor = UICharactorPet.GetCharacterInfoFromCode(contact.character);
				
				//UICharactorPet / InitUICharactorPet() 에 등록된 캐릭터-아이콘네임 에 따라 가져온다.
				//Debug.Log ("ch name = "+charactor.charactorIconName);
				//Debug.Log ("ch 2    = "+charactor.CharacterCode);
				
				if("" != charactor.charactorIconName)
				{
					spriteItem.gameObject.SetActive(true);
					spriteItem.spriteName = charactor.charactorIconName;
				}
			}
			
		}

		//Label text
		UILabel[] labelChildren = GetComponentsInChildren<UILabel>();
		foreach(UILabel labelItem in labelChildren)
		{
			if(labelItem.name == "lblName")
			{
				//Debug.Log ("name = " + contact.name);
				labelItem.text = contact.name;
			}
			else if(labelItem.name == "lblRankNumber")
			{
				//Debug.Log ("name = " + contact.name);
				labelItem.text = m_iRank.ToString()+GlobalStrings.txtRank;
			}			
			else if(labelItem.name == "lblTopScore")
			{
				bool bExistScore = true;

				if(bExistScore)
				{
					labelItem.text = contact.record.ToString("n0")+"m";
				}
				else
				{
					labelItem.text = "No Score";
					labelItem.color = new Color(19f, 87f, 117f);	//#135775
				}
				
				//Debug.Log ("labelItem.text"+labelItem.text);
/*
- "c" : 해당 컴퓨터에서 사용하는 통화 단위로 변환
- "f3" : 소수점 아래로 세 자리까지 표시
- "0" : 정수 형태로 변환
- "0%" : 정수 퍼센트 단위로 변환
- "n" : 천 단위마다 쉼표를 집어넣음(소수점 2자리 나옴)
- "n0" : 천 단위마다 쉼표를 집어넣음(소수점 안나옴)
- "yyyy-MM-dd HH:mm:ss" : 날짜 형식으로 표시
- "yyyy-MM-dd tt hh:mm:ss" : 날짜 형식으로 표시(오전/오후 표시)
- "yyyy-MM-dd tt hh:mm:ss ffffff" : 밀리세컨즈까지 표시
*/
			}
		}
	}

	public int GetKakaoID()
	{
		return m_iKakaoID;
	}
	
	protected IEnumerator SetProfilePic(string strProfilePicURL)
	{
		Debug.Log(">>>> SetProfilePic : " + strProfilePicURL);
		
		UITexture[] textureChildren = GetComponentsInChildren<UITexture>();
		foreach(UITexture textureItem in textureChildren)
		{
			if(textureItem.name == "imgProfilePic")
			{
				WWW www = new WWW(strProfilePicURL);
		     		yield return www;

				Material mat = new Material(textureItem.material);
				mat.mainTexture = www.texture;
		       		textureItem.material = mat;
				break;
			}
		}
	}
}
