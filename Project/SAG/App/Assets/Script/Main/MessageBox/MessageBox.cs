using UnityEngine;
using System.Collections;

public class MessageBox
{
	
	public UIPanel panelUserManagement;
	
	private MessageBox() { }
	
	public delegate void MessageBoxCallback(CommonDef.MESSAGE_BOX_RESULT result);
	
	//ID 삭제 처리하는 다이얼로그 박스.
	static public void DeleteDialogBox(string inputString)
	{		
			Debug.Log ("DeleteDialogBox####### dialogbox");
		
		MessageBox.MessageBoxCallback callback = delegate(CommonDef.MESSAGE_BOX_RESULT result)
		{
			if(result == CommonDef.MESSAGE_BOX_RESULT.MB_OK)
			{						
				Debug.Log ("DeleteDialogBox dialogbox");							
			
				GameObject[] allPanel = GameObject.FindGameObjectsWithTag("UIPanel");
				foreach(GameObject panel in allPanel)
				{
					if(panel.name == "16_panelUserManagement")
					{	//OK 버튼을 누른 후에 모든 유저 데이터를 가져온다.		
						panel.SendMessage("readAllUser");
						break;
					}
				}				
			}		
		};
		MessageBox.ShowMessageBox(inputString, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, callback);		
	}	
	//ID 등록 처리하는 다이얼로그 박스.
	static public void SignUPDialogBox(string inputString)
	{		
			Debug.Log ("SignUPDialogBox####### dialogbox");
		
		MessageBox.MessageBoxCallback callbackSignUP = delegate(CommonDef.MESSAGE_BOX_RESULT result)
		{
			if(result == CommonDef.MESSAGE_BOX_RESULT.MB_OK)
			{						
				Debug.Log ("successSignUPDialogBox dialogbox");							
			
				GameObject[] allPanel = GameObject.FindGameObjectsWithTag("UIPanel");
				foreach(GameObject panel in allPanel)
				{
					if(panel.name == "16_panelUserManagement")
					{	//OK 버튼을 누른 후에 모든 유저 데이터를 가져온다.		
						panel.SendMessage("readAllUser");
						break;
					}
				}				
			}		
		};
		MessageBox.ShowMessageBox(inputString, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, callbackSignUP);		
	}
	//ID 수정 처리하는 다이얼로그 박스.
	static public void UpdateUserDialogBox(string inputString)
	{		
			Debug.Log ("UpdateUserDialogBox####### dialogbox");
		
		MessageBox.MessageBoxCallback callbackSignUP = delegate(CommonDef.MESSAGE_BOX_RESULT result)
		{
			if(result == CommonDef.MESSAGE_BOX_RESULT.MB_OK)
			{						
				Debug.Log ("UpdateUserDialogBox dialogbox");							
			
				GameObject[] allPanel = GameObject.FindGameObjectsWithTag("UIPanel");
				foreach(GameObject panel in allPanel)
				{
					if(panel.name == "16_panelUserManagement")
					{	//OK 버튼을 누른 후에 모든 유저 데이터를 가져온다.		
						panel.SendMessage("readAllUser");
						break;
					}
				}				
			}		
		};
		MessageBox.ShowMessageBox(inputString, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, callbackSignUP);		
	}
		
	//간단히 "문구"만 통보해 주는 다이얼로그 박스.
	static public void SimpleOKDialogBox(string inputString)
	{
		
			MessageBox.MessageBoxCallback callbackSimple = delegate(CommonDef.MESSAGE_BOX_RESULT result)
			{
				if(result == CommonDef.MESSAGE_BOX_RESULT.MB_OK)
				{						
					Debug.Log ("simple dialogbox");					
				}		
			};
			MessageBox.ShowMessageBox(inputString, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, callbackSimple);				
	}
	
	// "메시지" 보여주고, Yes,No,OK 로 분기함.
	static public void ShowMessageBox(string strMessage, CommonDef.MESSAGE_BOX_TYPE mbType, MessageBoxCallback callback)
	{
		GameObject[] allPanel = GameObject.FindGameObjectsWithTag("UIPanel");
		foreach(GameObject panel in allPanel)
		{
			if(panel.name == "MessageBox")
			{
				Debug.Log ("simple dialogbox2");
				Debug.Log ("simple dialogbox2");	
				CommonDef.MessageBoxParam param = new CommonDef.MessageBoxParam();
				param.strMessage = strMessage;
				param.mbType = mbType;
				param.callback = callback;

				panel.SendMessage("ShowMessageBox", param);
				break;
			}
		}
	}
	
	//아이디 생성 메시지 박스.
	static public void ShowSignUPMessageBox(string strMessage, CommonDef.MESSAGE_BOX_TYPE mbType, MessageBoxCallback callback)
	{
		
		Debug.Log (" Messagebox ShowSignUPMessageBox = "+strMessage);
		GameObject[] allPanel = GameObject.FindGameObjectsWithTag("UIPanel");
		foreach(GameObject panel in allPanel)
		{
			if(panel.name == "MessageBox")
			{
				CommonDef.MessageBoxParam param = new CommonDef.MessageBoxParam();
				param.strMessage = strMessage;
				param.mbType = mbType;
				param.callback = callback;

				panel.SendMessage("ShowSignUPMessageBox", param);
				break;
			}
		}
	}
	//아이디 수정 업데이트 메시지 박스.
	static public void ShowUpdateMessageBox(string strMessage, CommonDef.MESSAGE_BOX_TYPE mbType, MessageBoxCallback callback)
	{
		
		Debug.Log (" Messagebox ShowUpdateMessageBox = "+strMessage);
		GameObject[] allPanel = GameObject.FindGameObjectsWithTag("UIPanel");
		foreach(GameObject panel in allPanel)
		{
			if(panel.name == "MessageBox")
			{
				CommonDef.MessageBoxParam param = new CommonDef.MessageBoxParam();
				param.strMessage = strMessage;
				param.mbType = mbType;
				param.callback = callback;

				panel.SendMessage("ShowUpdateMessageBox", param);
				break;
			}
		}
	}
}
