using UnityEngine;
using System.Collections;

public class MainMessageBox : MonoBehaviour {

	public UIPanel panelMessageBox;
	//public UIPanel panelSignUPMessageBox;
	//public UIPanel panelUpdateMessageBox;
	//public UIPanel panelLoading;

	// Use this for initialization
	void Start ()
	{
		panelMessageBox.gameObject.SetActive(false);
		//panelSignUPMessageBox.gameObject.SetActive(false);
		//panelUpdateMessageBox.gameObject.SetActive(false);
		//panelLoading.gameObject.SetActive(false);
	}

	// Update is called once per frame
	void Update ()
	{
		
	}
	

	
	protected void ShowMessageBox(CommonDef.MessageBoxParam param)
	{
		Debug.Log ("Main ShowMessageBox");
		panelMessageBox.gameObject.SetActive(true);
		panelMessageBox.SendMessage("ShowMessageBox", param);
	}
	
	/*
	//아이디 생성 메시지 박스.
	protected void ShowSignUPMessageBox(CommonDef.MessageBoxParam param)
	{
		Debug.Log ("Main sign ShowMessageBox");
		panelSignUPMessageBox.gameObject.SetActive(true);
		panelSignUPMessageBox.SendMessage("ShowSignUPMessageBox", param);	
	}
	
	//아이디 수정 메시지 박스.
	protected void ShowUpdateMessageBox(CommonDef.MessageBoxParam param)
	{
		Debug.Log ("ShowUpdateMessageBox");
		panelUpdateMessageBox.gameObject.SetActive(true);
		panelUpdateMessageBox.SendMessage("ShowUpdateMessageBox", param);	
	}
	*/
	
	/*
	protected void ShowLoadingScreen(bool bShow)
	{
		Debug.Log("active loading");
		panelLoading.gameObject.SetActive(bShow);
	}
	*/
}
/*
public class MessageBox
{
	private MessageBox() { }
	
	public delegate void MessageBoxCallback(CommonDef.MESSAGE_BOX_RESULT result);

	static public void ShowMessageBox(string strMessage, CommonDef.MESSAGE_BOX_TYPE mbType, MessageBoxCallback callback)
	{
		GameObject[] allPanel = GameObject.FindGameObjectsWithTag("GUICamera");
		foreach(GameObject panel in allPanel)
		{
			if(panel.name == "GUICamera")
			{
				CommonDef.MessageBoxParam param = new CommonDef.MessageBoxParam();
				param.strMessage = strMessage;
				param.mbType = mbType;
				param.callback = callback;

				panel.SendMessage("ShowMessageBox", param);
				break;
			}
		}
	}
}
*/