using UnityEngine;
using System.Collections;
using System;		//시간 가져오기.

public class Main : MonoBehaviour {
	
	
	
	
	//환경설정.
	public UIPanel panelPopupConfig;
	//결제.
	public UIPanel panelPopupPayment;
	
	//상점.
	public UIPanel panelPopupShop;
	//홈(Home).유저의 캐릭터 펫 등 보유물을 볼 수 있다.	
	public UIPanel panelPopupHome;
	
	//타이틀 페널.
	public UIPanel panelTitle;
	
	//종목 선택.
	public UIPanel panelPopupSub;
	
	
	// 메인메뉴의 모든 팝업을 닫는다.
	void Hide_All_Popup()
	{			
		//panelTitle.gameObject.SetActive(true);
		panelPopupConfig.gameObject.SetActive(false);		
		panelPopupPayment.gameObject.SetActive(false);
		panelPopupSub.gameObject.SetActive(false);
		panelPopupShop.gameObject.SetActive(false);
		panelPopupHome.gameObject.SetActive(false);		
	}
	
	
	DateTime remainTime;
	
	//수정.
	void Awake() {
		//	Debug.Log ("awake");
		
		
		
		
		
		
		//GlobalValues.userValues.titleClick = false;
		GlobalValues.gameUser.email = "yjpark";//임시.
		
		//서버에서 유저의 데이터를 가져오는 것으로 수정해야함.
		// 임시로 뽀로로 캐릭터를 가지고 있도록 작성.
		GlobalValues.userTotalCharactor.count = 4;
		
		GlobalValues.userTotalCharactor.power[0] = 50;
		GlobalValues.userTotalCharactor.skill[0] = 30;
		GlobalValues.userTotalCharactor.speed[0] = 30;
		GlobalValues.userTotalCharactor.characterCode[0] = "CH_PORORO_A";		
		GlobalValues.userTotalCharactor.characterCode[1] = "CH_TURTLE_B";
		GlobalValues.userTotalCharactor.characterCode[2] = "CH_PORORO_A";		
		GlobalValues.userTotalCharactor.characterCode[3] = "CH_PORORO_A";		
				
				
		// 화면에 보여줄 리스트를 초기화 한다.
		UIPayment.InitUIPayment();	
		// 상점의 캐릭터,펫 초기화. = 고정되어 보여진다.
		UICharactorPet.InitUICharactorPet();
		
		//유저가 보유한 캐릭터, 펫. = 변동(사용자에 따라 다름)되어 보여진다.
		//	UICharactorPet.InitTotalUserCharactor();
		
		//UICharactorPet.InitUIUserPet();
	}
	
	// Use this for initialization
	void Start () {		
		
		// 타이틀 화면.
		if (GlobalValues.userValues.titleClick == true)
			panelTitle.gameObject.SetActive(false);
		
		// 메인메뉴의 모든 팝업을 닫는다.
		Hide_All_Popup();	
		
		
		
		
		
		
		
		
	}	
	// Update is called once per frame
	void Update () {
		
		//remainTime =  DateTime.Now.AddHours (-12);
		//Debug.Log ("time = " + remainTime);
	
	}
	
	
	
	//타이틀화면 클릭. 메인메뉴로 이동.
	void OnClickTitle()		
	{
		Debug.Log ("on click title");
		GlobalValues.userValues.titleClick = true;
		panelTitle.gameObject.SetActive(false);
	}
	
	
	//sub button . 
	void OnClickConfig()
	{
		panelPopupConfig.gameObject.SetActive(true);
		Debug.Log ("on click OnClickConfig");		
	}
	
	/*
	MessageBox.MessageBoxCallback callbackWillYouBuy = delegate(CommonDef.MESSAGE_BOX_RESULT result)
	{
		if(result == CommonDef.MESSAGE_BOX_RESULT.MB_YES)
		{				
			// 캐릭터 구매 시도.
			buyChooseMainCharactor();				
		}		
	};
	MessageBox.ShowMessageBox(GlobalStrings.willYouBuy, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_YESNO, callbackWillYouBuy);
	*/
	
	
	//Home & Shop.
	void OnClickHome()
	{		
		Debug.Log ("On click home");		
		//UICharactorPet.InitUIUserCharactor2();
		UICharactorPet.InitTotalUserCharactor();
		panelPopupHome.gameObject.SetActive(true);
		
		Debug.Log ("On click home End ");				
	}	
	
	// 메인 메뉴에서 캐릭터 변경 선택.
	void OnClickShop()
	{		
		GlobalValues.userCharactor.currentEvent = 100; //종목 선택에서 정식 종목이 아니며 구별을 위해 100 으로 세팅함.
		GlobalValues.userCharactor.mode = GlobalValues.userCharactor.MODE_MENU;
		Debug.Log ("Onclick Shop = "+GlobalValues.userCharactor.mode);
		panelPopupShop.gameObject.SetActive(true);
		
	}
	
	//종목 선택 메뉴에서 캐릭터 변경 선택.
	void OnSubClickShop()
	{		
		GlobalValues.userCharactor.mode = GlobalValues.userCharactor.MODE_SUBMENU;
		Debug.Log ("Onclick SUB Shop = "+GlobalValues.userCharactor.mode);
		panelPopupShop.gameObject.SetActive(true);
		
	}
	
	

	
	
	//Payment.
	void OnClickChance()
	{
		GlobalValues.userValues.chooseBtnPayment = "Chance";
		panelPopupPayment.gameObject.SetActive(true);
		Debug.Log ("on click OnClickChance");		
	}
	void OnClickGold()
	{
		GlobalValues.userValues.chooseBtnPayment = "Gold";
		panelPopupPayment.gameObject.SetActive(true);
		Debug.Log ("on click OnClickGold");		
	}
	void OnClickJewel()
	{
		GlobalValues.userValues.chooseBtnPayment = "Jewel";
		panelPopupPayment.gameObject.SetActive(true);
		Debug.Log ("on click OnClickJewel");		
	}
	
	
	// 종목 선택화면.
	void OnClickSub()
	{
		panelPopupSub.gameObject.SetActive(true);
		Debug.Log ("OnClickSub");		
	}
	
	// 임시.
	void StartBoxing()
	{
		Application.LoadLevel("Boxing");
	}
	void StartThrow()
	{
		Application.LoadLevel("Throw");
	}
	void StartArchery()
	{
		Application.LoadLevel("Archery");
	}
}
