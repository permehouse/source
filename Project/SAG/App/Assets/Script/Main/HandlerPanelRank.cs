using UnityEngine;
using System.Collections;

public class HandlerPanelRank : MonoBehaviour {
	
	public UIPanel panelTotalRanking;	
	public UIPanel panelBoxing;	
	public UIPanel panelThrowing;	
	public UIPanel panelArchery;	
	public UIPanel panelSwim;	
	public UIPanel paneShoot;			
	
	private enum PANEL_INDEX {
		TOTAL_RANKING = 0,
		BOXING,
		THROWING,
		ARCHERY,
		SWIM,
		SHOOT,
	}
	
	// Use this for initialization
	void Start () {		
		SwitchPanel(PANEL_INDEX.THROWING);
	}
	
	
	void OnClickWorldRank()
	{
		Debug.Log ("rank");
		//userCharactorInfoList.Add(new CharactorInfo("CH_DEFAULT_C", "Karr", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorWhaleIcon", 5, "CharactorWhaleAni_", 0, 0, 'B', "까르르", "힘50 속30  기30\n무식한 원시소녀"));			
		
		UICharactorPet.InitUIUserCharactor2();
	}
		
		
		
	
	//순위 화면 버튼 선택.
	void BtnRankTotalRanking()
	{		
		Debug.Log ("btn rank total ranking");
		SwitchPanel(PANEL_INDEX.TOTAL_RANKING);
	}
	void BtnRankBoxingClick()
	{		
		SwitchPanel(PANEL_INDEX.BOXING);		
	}	
	void BtnRankThrowingClick()
	{
		
		
		
		
		SwitchPanel(PANEL_INDEX.THROWING);
	}	
	void BtnRankArchery()
	{		
		SwitchPanel(PANEL_INDEX.ARCHERY);
	}
	
	
	
	
	
	void BtnRankShoot()
	{
		SwitchPanel(PANEL_INDEX.SHOOT);
	}	
	void BtnRankSwim()
	{
		SwitchPanel(PANEL_INDEX.SWIM);
	}
	
	
	
	
	
	
	//순위 화면 전환.
	private void SwitchPanel(PANEL_INDEX indexPanel)
	{
		panelTotalRanking.gameObject.SetActive(false);				
		panelBoxing.gameObject.SetActive(false);
		panelThrowing.gameObject.SetActive(false);
		panelArchery.gameObject.SetActive(false);
		panelSwim.gameObject.SetActive(false);
		paneShoot.gameObject.SetActive(false);
		
		switch(indexPanel)
		{	
			case PANEL_INDEX.TOTAL_RANKING:			
				panelTotalRanking.gameObject.SetActive(true);			
				break;
			case PANEL_INDEX.ARCHERY:				
				panelArchery.gameObject.SetActive(true);				
				break;
			case PANEL_INDEX.BOXING:				
				panelBoxing.gameObject.SetActive(true);
				break;
			case PANEL_INDEX.SHOOT:				
				paneShoot.gameObject.SetActive(true);
				break;
			case PANEL_INDEX.SWIM:				
				panelSwim.gameObject.SetActive(true);
				break;
			case PANEL_INDEX.THROWING:				
				panelThrowing.gameObject.SetActive(true);
				break;
		}		
	}
	
}
