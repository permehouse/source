using UnityEngine;
using System.Collections;

public class Sound : MonoBehaviour {
	// Sound
	public AudioSource audioBGM;
	
	void Awake() {
		if(GameData.Settings.bSoundOn == false) {
			return;
		}
		
		audioBGM.Play();
	}	
}
