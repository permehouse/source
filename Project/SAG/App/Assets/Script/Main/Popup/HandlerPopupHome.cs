using UnityEngine;
using System.Collections;

public class HandlerPopupHome : MonoBehaviour {
	
	public GameObject pageCharactor;
	public GameObject pagePet;
	public GameObject pageItem;
	
	public UIImageButton tabBtnCharactor;
	public UIImageButton tabBtnPet;
	public UIImageButton tabBtnItem;
	
	public UISprite tabImgCharactor;
	public UISprite tabImgPet;
	public UISprite tabImgItem;
	
	public UILabel lblPet;
	public UILabel lblSapphire;	
	public int nCurrentPAGE;
	
	private enum PAGE_INDEX
	{
		PAGE_CHARACTOR = 0,
		PAGE_PET,
		PAGE_ITEM,	
	}
	
	//초기화.
	void OnEnable ()	
	{	
		SwitchPage(PAGE_INDEX.PAGE_CHARACTOR);				
	}
	
	//탭 선택.
	public void OnClickTabCharactor()
	{		
		SwitchPage(PAGE_INDEX.PAGE_CHARACTOR);
	}	
	public void OnClickTabPet()
	{		
		SwitchPage(PAGE_INDEX.PAGE_PET);
	}	
	public void OnClickTabItem()
	{		
		SwitchPage(PAGE_INDEX.PAGE_ITEM);
	}
		
		 
		   
	private void SwitchPage(PAGE_INDEX indexPage)
	{
		nCurrentPAGE = (int)indexPage;		
		
		pageCharactor.SetActive(false);
		pagePet.SetActive(false);
		pageItem.SetActive(false);			
		// 우선 캐릭터만 지원한다.
		//tabBtnCharactor.gameObject.SetActive(true);
		//tabBtnPet.gameObject.SetActive(true);
		//tabBtnItem.gameObject.SetActive(true);		
		tabImgCharactor.gameObject.SetActive(false);
		tabImgPet.gameObject.SetActive(false);
		tabImgItem.gameObject.SetActive(false);		
		
		switch(indexPage)
		{
			case PAGE_INDEX.PAGE_CHARACTOR:
				pageCharactor.SetActive(true);
				tabBtnCharactor.gameObject.SetActive(false);
				tabImgCharactor.gameObject.SetActive(true);							
				break;
			case PAGE_INDEX.PAGE_PET:
				pagePet.SetActive(true);			
				tabBtnPet.gameObject.SetActive(false);
				tabImgPet.gameObject.SetActive(true);				
				break;
			case PAGE_INDEX.PAGE_ITEM:
				pageItem.SetActive(true);
				tabBtnItem.gameObject.SetActive(false);
				tabImgItem.gameObject.SetActive(true);			
				break;		
		}		
	}
	
	
	
	// Update is called once per frame
	void Update () {
	
	}
	
	//확인키 눌러서 창 닫음.생성했던 캐릭터 없앰.
	void OnClickOK()
	{
		
		for (int i = 0; i < UICharactorPet.userCharactorInfoList.Count; ++i)
		{
			Destroy(GlobalValues.userCharactor.objCharactor[i]);
	
		}
		GlobalValues.userCharactor.onEnable = false;
		//UICharactorPet.userCharactorInfoList.Count = 0;
		
		GameObject currentPanel = GetComponent<UIPanel>().gameObject;
		currentPanel.gameObject.SetActive(false);
	}
}
