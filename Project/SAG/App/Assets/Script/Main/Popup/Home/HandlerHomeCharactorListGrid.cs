using UnityEngine;
using System.Collections;

public class HandlerHomeCharactorListGrid : MonoBehaviour
{
	public GameObject prefabCharactorListItem;


	void Start ()
	{
		Debug.Log ("init user star");
		//InitStarList();
	}
	
	void OnEnable()
	{
		Debug.Log ("On Enable !!!!! init  enable = "+UICharactorPet.userCharactorInfoList.Count+"  = "+GlobalValues.userCharactor.onEnable);
		
		// 수정. Enable 이 2번 불려지는 버그 때문에 flag 사용함. 
		if (GlobalValues.userCharactor.onEnable == false)
		{
			Debug.Log ("OK");
			InitCharactorList();
		}
		
		GlobalValues.userCharactor.onEnable = true;
		
		
	}
	
	void Update ()
	{
	
	}
	
	
	
	
	protected void InitCharactorList()
	{		
		Debug.Log("Init userCharactorInfoList List(), = " + UICharactorPet.userCharactorInfoList.Count.ToString());
		
		//for (int i = 0; i < UIPayment.starInfoList.Count; ++i)
		for (int i = 0; i < UICharactorPet.userCharactorInfoList.Count; ++i)
		{		
			GlobalValues.userCharactor.objCharactor[i] = Instantiate(prefabCharactorListItem, new Vector3(0.0f, 0.0f, -0.043f), Quaternion.identity) as GameObject;

			GlobalValues.userCharactor.objCharactor[i].transform.parent = this.transform;
			GlobalValues.userCharactor.objCharactor[i].transform.localScale = new Vector3(1f, 1f, 1f);
			HandlerHomeCharactorListItem itemHandler = GlobalValues.userCharactor.objCharactor[i].GetComponentInChildren<HandlerHomeCharactorListItem>();
			itemHandler.SetItemInfo(i);
		}
		GetComponent<UIGrid>().Reposition();
	}

	protected void OnSelectedItem()
	{
		for(int i = 0;i<this.transform.childCount;++i)
		{
			this.transform.GetChild(i).gameObject.SendMessage("OnSelectedItem");
		}
	}
}
