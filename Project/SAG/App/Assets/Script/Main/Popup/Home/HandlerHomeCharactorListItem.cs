using UnityEngine;
using System.Collections;

public class HandlerHomeCharactorListItem : MonoBehaviour
{
	
	private int chooseCharactor;
	

	
	void Start ()
	{	
	}

	void Update ()
	{	
	}
		
	//캐릭터 선택. (구입).
	public void OnClickChoose()
	{			
		Debug.Log ("Choose Charactor = "+chooseCharactor);
		
		MessageBox.MessageBoxCallback callback = delegate( CommonDef.MESSAGE_BOX_RESULT result )
		{
			if(result == CommonDef.MESSAGE_BOX_RESULT.MB_YES)
			{
				Debug.Log ("buy message"+result);
				//MessageBox.ShowMessageBox	(GlobalStrings.gsSuccessBuy,CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK,"");
				MessageBox.SimpleOKDialogBox("ok");
			}
			else
				Debug.Log ("buy message"+result);
		};				
		MessageBox.ShowMessageBox(GlobalStrings.gsTemp,CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_YESNO,callback);				
		
	}
	
	//캐릭터 레벨업. 
	public void OnClickLevelUp()
	{
		Debug.Log ("Btn Level UP"+chooseCharactor);	
	}
	
	
	public void SetItemInfo(int iCharactorIndex)
	{
		Debug.Log("...iCharactorIndex Set ItemInfo"+iCharactorIndex);
			
		
		if(iCharactorIndex < 0 || UICharactorPet.userCharactorInfoList.Count <= iCharactorIndex)
			return;
					
		
		chooseCharactor = iCharactorIndex;
		
		UICharactorPet.CharactorInfo charactor = (UICharactorPet.CharactorInfo)UICharactorPet.userCharactorInfoList[iCharactorIndex];
			
		
		//캐릭터 표시.
		UISpriteAnimation[] spriteAniChildren = GetComponentsInChildren<UISpriteAnimation>();
		foreach(UISpriteAnimation spriteAniItem in spriteAniChildren)
		{			
			if(spriteAniItem.name == "imgCharactor")
			{			
				spriteAniItem.framesPerSecond = charactor.animationFramesPerSecond;
				spriteAniItem.namePrefix = charactor.animationNamePrefix;
			}
		}
		
		//레벨 표시. 
		UISprite[] spriteChildren = GetComponentsInChildren<UISprite>();
		foreach(UISprite spriteItem in spriteChildren)
		{
			if(spriteItem.name == "imgGrade")
			{
				spriteItem.gameObject.SetActive(charactor.grade != 'N');
				spriteItem.spriteName = "PopupCharactorListItemGrade" + charactor.grade;
				spriteItem.MakePixelPerfect();
			}
		}
		
		
		//이름, 정보, 레벨, 가격 표시.
		UILabel[] labelChildren = GetComponentsInChildren<UILabel>();
		foreach(UILabel labelItem in labelChildren)
		{
			//charactorName
			if(labelItem.name == "lblName")			//이름.
			{
				labelItem.text = charactor.charactorExpressionName;
			}
			else if(labelItem.name == "lblLevel")
			{
				labelItem.text = "1";		//수정. 차후에 레벨 추가.
			}
			else if(labelItem.name == "lblDescription")
			{
				labelItem.text = charactor.charactorDescription;
			}
			else if(labelItem.name == "lblPrice") 
			{
				labelItem.text =   "price";
			}
			
			
						
			/*
			if(labelItem.name == "lblBonus")				//보너스 % 표시.
			{
				
				labelItem.text = (100*(star.nCount-(star.nPrice*star.nMultiply/star.nDivid)) /(star.nPrice*star.nMultiply/star.nDivid))+"%";
			}			
			else if(labelItem.name == "lblGoods")		
			{	
				labelItem.text = star.nCount.ToString();				
			}
			else if(labelItem.name == "lblPay")				//가격.
			{
				labelItem.text = star.nPrice.ToString();				
			}
			*/
		}		
	}	
}
