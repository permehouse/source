using UnityEngine;
using System.Collections;

public class HandlerPopupSubMain : MonoBehaviour {
	
	
	public GameObject subReady;
	
		
	EVENT_INDEX userChooseEvent = EVENT_INDEX.EVENT_THROW;
	public UISprite imgEvent0;
	public UISprite imgEvent1;
	public UISprite imgEvent2;
	public UISprite imgEvent3;
	public UISprite imgEvent4;
	public UISprite imgEvent5;
	
	private enum EVENT_INDEX {
		EVENT_THROW = 0,
		EVENT_ARCHERY,
		EVENT_BOXING,
		EVENT_SHOOT,
		EVENT_SWIM,
		EVENT_5EVENT,		
	}
	
	// Use this for initialization
	void Start () {		
		Debug.Log ("A::!!!!!! start");
		
		//userChooseEvent = EVENT_INDEX.EVENT_THROW;
		//ImgChoosenSelected();	
	}
	
	void OnEnable()
	{
		Debug.Log ("on enable!!!!");	
		
		/*
		
		switch (GlobalValues.userCharactor.mode)
		{
			case GlobalValues.userCharactor.MODE_THROW:
				break;
			case GlobalValues.userCharactor.MODE_ARCHERY:
				break;
			case GlobalValues.userCharactor.MODE_BOXING:
				break;			
		}
			
		*/	
		
		
		
			
		switch(GlobalValues.userCharactor.currentEvent) 
		{
			case 0:
				userChooseEvent = EVENT_INDEX.EVENT_THROW;
				break;
			case 1:
				userChooseEvent = EVENT_INDEX.EVENT_ARCHERY;
				break;
			case 2:
				userChooseEvent = EVENT_INDEX.EVENT_BOXING;
				break;
			case 3:
				userChooseEvent = EVENT_INDEX.EVENT_SHOOT;
				break;
			case 4:
				userChooseEvent = EVENT_INDEX.EVENT_SWIM;
				break;
			case 5:
				userChooseEvent = EVENT_INDEX.EVENT_5EVENT;
				break;			
		}
		
		Debug.Log ("GlobalValues.userCharactor.currentEvent = "+GlobalValues.userCharactor.currentEvent);
	//	userChooseEvent = EVENT_INDEX.EVENT_THROW;
		Debug.Log ("userChooseEvent = "+userChooseEvent);
		ImgChoosenSelected();
	//	GlobalValues.userCharactor.mode = GlobalValues.userCharactor.MODE_THROW;
		
	}
	
	//현재 선택한 종목에 테두리를 표시함.
	void ImgChoosenSelected()
	{
		imgEvent0.gameObject.SetActive(false);
		imgEvent1.gameObject.SetActive(false);
		imgEvent2.gameObject.SetActive(false);
		imgEvent3.gameObject.SetActive(false);
		imgEvent4.gameObject.SetActive(false);
		imgEvent5.gameObject.SetActive(false);			
		
		switch(userChooseEvent) 
		{
			case EVENT_INDEX.EVENT_THROW:
				imgEvent0.gameObject.SetActive(true);
				break;
			case EVENT_INDEX.EVENT_ARCHERY:
				imgEvent1.gameObject.SetActive(true);
				break;
			case EVENT_INDEX.EVENT_BOXING:
				imgEvent2.gameObject.SetActive(true);
				break;
			case EVENT_INDEX.EVENT_SHOOT:
				imgEvent3.gameObject.SetActive(true);
				break;
			case EVENT_INDEX.EVENT_SWIM:
				imgEvent4.gameObject.SetActive(true);
				break;
			case EVENT_INDEX.EVENT_5EVENT:
				imgEvent5.gameObject.SetActive(true);
				break;			
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	//캐릭터 선택.
	void OnClickSelectCharactor()
	{
	//	Ready.SendMessage("aniCharactor");
		
	}
	
	//확인키 눌러서 창 닫음.
	void OnClickBackToMain()
	{
		Debug.Log ("back to Main");
		GameObject currentPanel = GetComponent<UIPanel>().gameObject;
		currentPanel.gameObject.SetActive(false);
	}
	
	//종목별 시작하기.
	void OnClickStart()
	{
	
		switch(userChooseEvent) 
		{
			case EVENT_INDEX.EVENT_THROW:
				Application.LoadLevel("Throw");
				break;
			case EVENT_INDEX.EVENT_ARCHERY:
				Debug.Log ("Archery");
				Application.LoadLevel("Archery");
				break;
			case EVENT_INDEX.EVENT_BOXING:
				Application.LoadLevel("Boxing");
				break;
			case EVENT_INDEX.EVENT_SHOOT:
				break;
			case EVENT_INDEX.EVENT_SWIM:
				break;
			case EVENT_INDEX.EVENT_5EVENT:
				break;			
		}		
	}
	
	
	
	
	void RefreshCharactor()
	{			
		switch (GlobalValues.userCharactor.currentEvent)
		{			
			case 0://GlobalValues.userCharactor.GAME_THROW:	
				GameData.nPlayerCharacter = (GameData.Character)GlobalValues.userCharactor.chThrow;		
				break;
			case 1://GlobalValues.userCharactor.GAME_ARCHERY:	
				GameData.nPlayerCharacter = (GameData.Character)GlobalValues.userCharactor.chArchery;		
				break;
			case 2://GlobalValues.userCharactor.GAME_BOXING:	
				GameData.nPlayerCharacter = (GameData.Character)GlobalValues.userCharactor.chBoxing;		
				break;
			case 3://GlobalValues.userCharactor.GAME_SHOOT:	
				GameData.nPlayerCharacter = (GameData.Character)GlobalValues.userCharactor.chShoot;		
				break;
			case 4://GlobalValues.userCharactor.GAME_SWIM:	
				GameData.nPlayerCharacter = (GameData.Character)GlobalValues.userCharactor.chSwim;		
				break;
			case 5:// GlobalValues.userCharactor.GAME_5EVENT:	
				GameData.nPlayerCharacter = (GameData.Character)GlobalValues.userCharactor.ch5Event;		
				break;			
		}			
		subReady.SendMessage("aniCharactor"); 			
	}	
	
	//각 종목 선택.
	void OnClickGame0()
	{
		Debug.Log ("On click game 0");			
		userChooseEvent = EVENT_INDEX.EVENT_THROW;
		GlobalValues.userCharactor.currentEvent = 0;
		
		RefreshCharactor();
		ImgChoosenSelected();		
	}
	
	void OnClickGame1()
	{
		Debug.Log ("On click game 1");			
		userChooseEvent = EVENT_INDEX.EVENT_ARCHERY;		
		GlobalValues.userCharactor.currentEvent = 1;
		
		RefreshCharactor();
		ImgChoosenSelected();	
		
	}
	void OnClickGame2()
	{
		Debug.Log ("On click game 2");			
		userChooseEvent = EVENT_INDEX.EVENT_BOXING;
		GlobalValues.userCharactor.currentEvent = 2;
		
		RefreshCharactor();
		ImgChoosenSelected();
	}
	void OnClickGame3()
	{
		Debug.Log ("On click game 3");			
		userChooseEvent = EVENT_INDEX.EVENT_SHOOT;
		GlobalValues.userCharactor.currentEvent = 3;
		
		RefreshCharactor();
		ImgChoosenSelected();
	}
	void OnClickGame4()
	{
		Debug.Log ("On click game 4");			
		userChooseEvent = EVENT_INDEX.EVENT_SWIM;
		GlobalValues.userCharactor.currentEvent = 4;
		
		RefreshCharactor();
		ImgChoosenSelected();
	}
	void OnClickGame5()
	{
		Debug.Log ("On click game 5");			
		userChooseEvent = EVENT_INDEX.EVENT_5EVENT;
		GlobalValues.userCharactor.currentEvent = 5;
		
		RefreshCharactor();
		ImgChoosenSelected();
	}	
}
