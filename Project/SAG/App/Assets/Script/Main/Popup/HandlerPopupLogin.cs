using UnityEngine;
using System.Collections;

// config/로그아웃 후에 아이디 생성과 로그인.
public class HandlerPopupLogin : MonoBehaviour {

	public UIInput inputId;	
		
	//아이디 생성함. 기존에 DB에 같은 ID 가 있는지 알아봐야 하는데 이를 서버에서 api로 지원하지 않음. 412 오류와 함께 중복 아이디라고 와야 하는데 412 오류만 와서 이를 체크할 수 없음.
	// 그래서 수동적으로 id를 다른 것을 넣어서 해야함.
	void OnClickLeftOK()
	{
		// 아이디 생성.		
		string id = inputId.value;		
		
		ServerConnector.CallbackResult cbResult = delegate(bool bSuccess) {	
			if(!bSuccess) {
				Debug.Log ("User cresate error : "+ GlobalValues.errorCodeConnector + ", " + GlobalValues.errorMessageConnector);
			}
			
			GlobalValues.gameUser.name = id;
			Debug.Log ("User cresate okok = "+bSuccess);
		};				
		ServerConnector connector = ServerConnector.GetInstance();
		connector.CreateUser(id,id,id,cbResult);
	}
	
	// 로그인.
	void OnClickOK()
	{			
		// 로그인 한후에 창을 닫는다.	
		ServerConnector.CallbackResult cbResult = delegate(bool bSuccess) {	
			if(!bSuccess) {
				Debug.Log ("login error : "+ GlobalValues.errorCodeConnector + ", " + GlobalValues.errorMessageConnector);
			}
			Debug.Log ("login okok = "+bSuccess);
			//GlobalValues.gameUser.name = GlobalValues.userValues.id;		//수정.
			GameObject currentPanel = GetComponent<UIPanel>().gameObject;
			currentPanel.gameObject.SetActive(false);
		};		
		
		GlobalValues.gameUser.name = GlobalValues.userValues.id;//0623 . 수정.
		ServerConnector connector = ServerConnector.GetInstance();
		
		
		connector.Login(GlobalValues.userValues.id, GlobalValues.userValues.id, cbResult);	
	}
}
