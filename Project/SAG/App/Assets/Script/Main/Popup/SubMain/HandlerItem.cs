using UnityEngine;
using System.Collections;

public class HandlerItem : MonoBehaviour {
	
	ITEM_INDEX choosenItem;
	
	public UISprite imgUpgradeChoosen0;
	public UISprite imgUpgradeChoosen1;
	public UISprite imgUpgradeChoosen2;
	public UISprite imgBuyChoosen0;
	public UISprite imgBuyChoosen1;
	public UISprite imgBuyChoosen2;
	public UISprite imgBuyChoosen3;
	public UISprite imgBuyChoosen4;
	public UISprite imgBuyChoosen5;
	
	private enum ITEM_INDEX {
		UPGRADE_0 = 0,
		UPGRADE_1,
		UPGRADE_2,
		BUY_0,
		BUY_1,
		BUY_2,		
		BUY_3,
		BUY_4,
		BUY_5,
	}
	
	// Use this for initialization
	void Start () {
		ImgChoosenSelected(ITEM_INDEX.BUY_0);
	
	}
	
	//현재 선택한 종목에 테두리를 표시함.
	void ImgChoosenSelected(ITEM_INDEX inputINDEX)
	{
		choosenItem = inputINDEX;
			
		imgUpgradeChoosen0.gameObject.SetActive(false);
		imgUpgradeChoosen1.gameObject.SetActive(false);
		imgUpgradeChoosen2.gameObject.SetActive(false);
		imgBuyChoosen0.gameObject.SetActive(false);
		imgBuyChoosen1.gameObject.SetActive(false);
		imgBuyChoosen2.gameObject.SetActive(false);	
		imgBuyChoosen3.gameObject.SetActive(false);
		imgBuyChoosen4.gameObject.SetActive(false);
		imgBuyChoosen5.gameObject.SetActive(false);	
		
		switch(choosenItem) 
		{
			case ITEM_INDEX.UPGRADE_0:
				imgUpgradeChoosen0.gameObject.SetActive(true);
				break;
			case ITEM_INDEX.UPGRADE_1:
				imgUpgradeChoosen1.gameObject.SetActive(true);
				break;
			case ITEM_INDEX.UPGRADE_2:
				imgUpgradeChoosen2.gameObject.SetActive(true);
				break;
			case ITEM_INDEX.BUY_0:
				imgBuyChoosen0.gameObject.SetActive(true);
				break;
			case ITEM_INDEX.BUY_1:
				imgBuyChoosen1.gameObject.SetActive(true);
				break;
			case ITEM_INDEX.BUY_2:
				imgBuyChoosen2.gameObject.SetActive(true);
				break;	
			case ITEM_INDEX.BUY_3:
				imgBuyChoosen3.gameObject.SetActive(true);
				break;
			case ITEM_INDEX.BUY_4:
				imgBuyChoosen4.gameObject.SetActive(true);
				break;
			case ITEM_INDEX.BUY_5:
				imgBuyChoosen5.gameObject.SetActive(true);
				break;
		}
	}
	
	void OnClickBuyItemConfirm()
	{
		switch(choosenItem) 
		{
			case ITEM_INDEX.UPGRADE_0:
				Debug.Log ("0 u confirm");
				break;
			case ITEM_INDEX.UPGRADE_1:
				Debug.Log ("1 u confirm");
				break;
			case ITEM_INDEX.UPGRADE_2:
				Debug.Log ("2 u confirm");
				break;
			case ITEM_INDEX.BUY_0:
				Debug.Log ("0 b confirm");
				break;
			case ITEM_INDEX.BUY_1:
				Debug.Log ("1 b confirm");
				break;
			case ITEM_INDEX.BUY_2:
				Debug.Log ("2 b confirm");
				break;	
			case ITEM_INDEX.BUY_3:
				Debug.Log ("3 b confirm");
				break;
			case ITEM_INDEX.BUY_4:
				Debug.Log ("4 b confirm");
				break;
			case ITEM_INDEX.BUY_5:
				Debug.Log ("5 b confirm");
				break;
		}
		
		
	}
	
	
	
	
	
	// 업그레이 드 아이.템...
	void OnClickUpgradeItem0()
	{
		ImgChoosenSelected(ITEM_INDEX.UPGRADE_0);
		Debug.Log ("buy Upgrade 0");	
	}
	void OnClickUpgradeItem1()
	{
		ImgChoosenSelected(ITEM_INDEX.UPGRADE_1);
		Debug.Log ("buy Upgrade 1");	
	}
	void OnClickUpgradeItem2()
	{
		ImgChoosenSelected(ITEM_INDEX.UPGRADE_2);
		Debug.Log ("buy Upgrade 2");	
	}
	
	// 구매 형아이.템..
	void OnClickBuyItem0()
	{
		ImgChoosenSelected(ITEM_INDEX.BUY_0);
		Debug.Log ("buy item 0");	
	}
	void OnClickBuyItem1()
	{
		ImgChoosenSelected(ITEM_INDEX.BUY_1);
		Debug.Log ("buy item 1");	
	}
	void OnClickBuyItem2()
	{
		ImgChoosenSelected(ITEM_INDEX.BUY_2);
		Debug.Log ("buy item 2");	
	}
	void OnClickBuyItem3()
	{
		ImgChoosenSelected(ITEM_INDEX.BUY_3);
		Debug.Log ("buy item 3");	
	}
	void OnClickBuyItem4()
	{
		ImgChoosenSelected(ITEM_INDEX.BUY_4);
		Debug.Log ("buy item 4");	
	}
	void OnClickBuyItem5()
	{
		ImgChoosenSelected(ITEM_INDEX.BUY_5);
		Debug.Log ("buy item 5");	
	}
}
