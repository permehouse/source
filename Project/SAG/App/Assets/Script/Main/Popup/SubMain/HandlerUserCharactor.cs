using UnityEngine;
using System.Collections;

public class HandlerUserCharactor : MonoBehaviour {
	
	public UISprite aniKarr;
	public UISprite aniWa;
	public UISprite aniWoo;
	public UISprite aniPPa;
	public UISprite aniYam;
	
	public UILabel lblName;
	public UILabel lblDescription;
	
	// Use this for initialization
	void Start () {
	
	}
	
	void OnEnable() {
		
		aniCharactor();
		
	}
	
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	public void aniCharactor()
	{
	
		int indexCharacter = 0;
		
		Debug.Log ("SUb ani Char");
		aniKarr.gameObject.SetActive(false);
		aniWa.gameObject.SetActive(false);
		aniWoo.gameObject.SetActive(false);
		aniPPa.gameObject.SetActive(false);
		aniYam.gameObject.SetActive(false);
		
		switch (GameData.nPlayerCharacter)
		{
			case GameData.Character.KYA:
				indexCharacter = 0;	
				aniKarr.gameObject.SetActive(true);
				break;
			case GameData.Character.WA:
				indexCharacter = 1;	
				aniWa.gameObject.SetActive(true);
				break;
			case GameData.Character.WOO:
				indexCharacter = 2;	
				aniWoo.gameObject.SetActive(true);
				break;	
			case GameData.Character.PPA:
				indexCharacter = 3;	
				aniPPa.gameObject.SetActive(true);		
				break;
			case GameData.Character.YAM:
				indexCharacter = 4;	
				aniYam.gameObject.SetActive(true);
				break;
		}
		
				
		
		// 캐릭터 이름, 설명 세팅.
		UICharactorPet.CharactorInfo charactor = (UICharactorPet.CharactorInfo)UICharactorPet.charactorInfoList[indexCharacter];
		UILabel[] labelChildren = GetComponentsInChildren<UILabel>();
		foreach(UILabel labelItem in labelChildren)
		{				
				if(labelItem.name == "lblName")			
				{				
					lblName.text = charactor.charactorExpressionName;
				}
				else if(labelItem.name == "lblDescription")
				{
					lblDescription.text = charactor.charactorDescription;
				}
		}
		
		UISlider[] sliderChildren = GetComponentsInChildren<UISlider>();
		foreach(UISlider sliderItem in sliderChildren)
		{
			if(sliderItem.name == "sliderPower")
			{
				sliderItem.sliderValue = (float)charactor.nPower / 100f;
			}
			else if(sliderItem.name == "sliderSpeed")
			{
				sliderItem.sliderValue = (float)charactor.nSpeed / 100f;
			}
			else if(sliderItem.name == "sliderSkill")
			{
				sliderItem.sliderValue = (float)charactor.nSkill / 100f;
			}
			
		}
		
		
		
		
		
		
	}
}
