using UnityEngine;
using System.Collections;

public class HandlerPopupConfig : MonoBehaviour {
	
	public GameObject pageSetting;
	public GameObject pageMyRecord;
	public UIImageButton tabBtnSetting;
	public UIImageButton tabBtnMyRecord;
	public UISprite tabImgSetting;
	public UISprite tabImgMyRecord;
	
	public UIPanel panelLogin;
	
	private bool bSettingChanged;			//환경 설정에서 바뀐 부분이 있는 경우.

	private enum PAGE_INDEX
	{
		PAGE_SETTING = 0,
		PAGE_MY_RECORD,
	}
	
	//초기화.
	void OnEnable ()	
	{		
		Debug.Log ("config start");
		bSettingChanged = false;
		SwitchPage(PAGE_INDEX.PAGE_SETTING);	
	}
	
	//로그 아웃 버튼 선택.
	void OnClickLogOut()
	{
		Debug.Log ("OnClickLogOut = "+GlobalValues.sessionId);			
		
		ServerConnector.CallbackResult cbResult = delegate(bool bSuccess) {	
			if(!bSuccess) {
				Debug.Log ("log out error : "+ GlobalValues.errorCodeConnector + ", " + GlobalValues.errorMessageConnector);
			}
				
			OnClickOK();			
			//TestRankingThrow();
		};		
		
		ServerConnector connector = ServerConnector.GetInstance();
		connector.Logout(GlobalValues.sessionId, cbResult);
		
		// Config 닫기.
		OnClickOK();
		// Login 열기.
		panelLogin.gameObject.SetActive(true);		
	}	
	
	void OnClickTabMyRecord()
	{
		Debug.Log ("OnClickTabMyRecord");	
	}
	
	void OnClickTabSetting()
	{
		Debug.Log ("OnClickTabSetting");
	}
	
	
	
	private void SwitchPage(PAGE_INDEX indexPage)
	{
		Debug.Log ("switch page connfig");
		pageSetting.SetActive(false);
		pageMyRecord.SetActive(false);
		tabBtnSetting.gameObject.SetActive(true);
		tabBtnMyRecord.gameObject.SetActive(true);
		tabImgSetting.gameObject.SetActive(false);
		tabImgMyRecord.gameObject.SetActive(false);

		switch(indexPage)
		{
		case PAGE_INDEX.PAGE_SETTING:
			pageSetting.SetActive(true);
			tabBtnSetting.gameObject.SetActive(false);
			tabImgSetting.gameObject.SetActive(true);
			break;
		case PAGE_INDEX.PAGE_MY_RECORD:
			pageMyRecord.SetActive(true);
			//ApplyMyRecord();
			tabBtnMyRecord.gameObject.SetActive(false);
			tabImgMyRecord.gameObject.SetActive(true);
			break;
		}
	}
	
	//확인키 눌러서 창 닫음.
	void OnClickOK()
	{
		bSettingChanged = false;
		GameObject currentPanel = GetComponent<UIPanel>().gameObject;
		currentPanel.gameObject.SetActive(false);
	}
	
}
