using UnityEngine;
using System.Collections;

public class HandlerPopupShop : MonoBehaviour {
	
	public GameObject pageCharactor;
	public GameObject pagePet;
	public GameObject pageItem;
	
	public GameObject Ready;
	
	public GameObject subReady;
	
	
	public UIImageButton tabBtnCharactor;
	public UIImageButton tabBtnPet;
	public UIImageButton tabBtnItem;
	
	public UISprite tabImgCharactor;
	public UISprite tabImgPet;
	public UISprite tabImgItem;
	
	public UILabel lblPet;
	public UILabel lblSapphire;	
	public int nCurrentPAGE;
	
	private enum PAGE_INDEX
	{
		PAGE_CHARACTOR = 0,
		PAGE_PET,
		PAGE_ITEM,	
	}
	
	//초기화.
	void OnEnable ()	
	{	
		SwitchPage(PAGE_INDEX.PAGE_CHARACTOR);				
	}
	
	//탭 선택.
	public void OnClickTabCharactor()
	{		
		SwitchPage(PAGE_INDEX.PAGE_CHARACTOR);
	}	
	public void OnClickTabPet()
	{		
		SwitchPage(PAGE_INDEX.PAGE_PET);
	}	
	public void OnClickTabItem()
	{		
		SwitchPage(PAGE_INDEX.PAGE_ITEM);
	}
		
		 
		   
	private void SwitchPage(PAGE_INDEX indexPage)
	{
		nCurrentPAGE = (int)indexPage;		
		
		pageCharactor.SetActive(false);
		pagePet.SetActive(false);
		pageItem.SetActive(false);			
		// 우선 캐릭터만 지원한다.
		//tabBtnCharactor.gameObject.SetActive(true);
		//tabBtnPet.gameObject.SetActive(true);
		//tabBtnItem.gameObject.SetActive(true);		
		tabImgCharactor.gameObject.SetActive(false);
		tabImgPet.gameObject.SetActive(false);
		tabImgItem.gameObject.SetActive(false);		
		
		switch(indexPage)
		{
			case PAGE_INDEX.PAGE_CHARACTOR:
				pageCharactor.SetActive(true);
				tabBtnCharactor.gameObject.SetActive(false);
				tabImgCharactor.gameObject.SetActive(true);							
				break;
			case PAGE_INDEX.PAGE_PET:
				pagePet.SetActive(true);			
				tabBtnPet.gameObject.SetActive(false);
				tabImgPet.gameObject.SetActive(true);				
				break;
			case PAGE_INDEX.PAGE_ITEM:
				pageItem.SetActive(true);
				tabBtnItem.gameObject.SetActive(false);
				tabImgItem.gameObject.SetActive(true);			
				break;		
		}		
	}
	
	// Use this for initialization
	void Charactort () {
		
		
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	//확인키 눌러서 창 닫음.
	void OnClickOK()
	{		
		
		Debug.Log ("exit");
		
		//화면에 보여지는 캐릭터 교체.
		//메인메뉴에서 눌렀을때.
		if (GlobalValues.userCharactor.mode == GlobalValues.userCharactor.MODE_MENU)
		{
			Debug.Log ("main");
			Ready.SendMessage("aniCharactor");
		}
		else //서브 메뉴에서 눌렀을때.
		{
			Debug.Log ("sub");
			subReady.SendMessage("aniCharactor"); 
		}
		
		for (int i = 0; i < UICharactorPet.charactorInfoList.Count; ++i)
		{
			Destroy(GlobalValues.userCharactor.objMenuCharactor[i]);
	
		}
		GlobalValues.userCharactor.menuOnEnable = false;
		
		
		
		GameObject currentPanel = GetComponent<UIPanel>().gameObject;
		currentPanel.gameObject.SetActive(false);
	}
}
