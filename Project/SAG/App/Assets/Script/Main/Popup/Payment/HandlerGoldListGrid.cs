using UnityEngine;
using System.Collections;

public class HandlerGoldListGrid : MonoBehaviour
{
	public GameObject prefabGoldListItem;

	public HandlerGoldListGrid()
	{
	}

	void Start ()
	{
		InitGoldList();
	}
	
	void Update ()
	{
	
	}
	
	protected void InitGoldList()
	{		
		//Debug.Log("Init Gold List(), " + UIPayment.goldInfoList.Count.ToString());
		for (int i = 0; i < UIPayment.goldInfoList.Count; ++i)
		{		
			GameObject obj = Instantiate(prefabGoldListItem, new Vector3(0.0f, 0.0f, -0.043f), Quaternion.identity) as GameObject;

			obj.transform.parent = this.transform;
			obj.transform.localScale = new Vector3(1f, 1f, 1f);
			HandlerGoldListItem itemHandler = obj.GetComponentInChildren<HandlerGoldListItem>();
			itemHandler.SetItemInfo(i);
		}
		GetComponent<UIGrid>().Reposition();
	}

	protected void OnSelectedItem()
	{
		for(int i = 0;i<this.transform.childCount;++i)
		{
			this.transform.GetChild(i).gameObject.SendMessage("OnSelectedItem");
		}
	}
}
