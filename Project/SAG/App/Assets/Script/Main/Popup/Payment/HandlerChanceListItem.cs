using UnityEngine;
using System.Collections;

public class HandlerChanceListItem : MonoBehaviour
{
	
	private int chooseStar;
	

	
	void Start ()
	{	
	}

	void Update ()
	{	
	}
		
	public void OnClickChoose()
	{		
		//Debug.Log("btn Star Payment Click = "+chooseStar); //선택한 칸.		
	}

	public void SetItemInfo(int iStarIndex)
	{
		//Debug.Log("...Star Set ItemInfo"+iStarIndex);
		
		//For test
		if(iStarIndex < 0 || UIPayment.starInfoList.Count <= iStarIndex)
			return;
					
		
		chooseStar = iStarIndex;
		
		UIPayment.StarInfo star = (UIPayment.StarInfo)UIPayment.starInfoList[iStarIndex];
		
		UILabel[] labelChildren = GetComponentsInChildren<UILabel>();
		foreach(UILabel labelItem in labelChildren)
		{
			if(labelItem.name == "lblBonus")				//보너스 % 표시.
			{
				
				labelItem.text = (100*(star.nCount-(star.nPrice*star.nMultiply/star.nDivid)) /(star.nPrice*star.nMultiply/star.nDivid))+"%";
			}			
			else if(labelItem.name == "lblGoods")		
			{	
				labelItem.text = star.nCount.ToString();				
			}
			else if(labelItem.name == "lblPay")				//가격.
			{
				labelItem.text = star.nPrice.ToString();				
			}
		}		
	}	
}
