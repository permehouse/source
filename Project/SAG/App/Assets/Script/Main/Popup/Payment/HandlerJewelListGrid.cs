using UnityEngine;
using System.Collections;

public class HandlerJewelListGrid : MonoBehaviour {
	
	public GameObject prefabJewelListItem;
	
	
	
	
	// Use this for initialization
	void Start () {
		//Debug.Log ("start");
		InitJewelList();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	protected void InitJewelList()
	{		
		//Debug.Log("Init Jewel List(), " + UIPayment.diaInfoList.Count.ToString());
		for (int i = 0; i < UIPayment.diaInfoList.Count; ++i)
		{		
			GameObject obj = Instantiate(prefabJewelListItem, new Vector3(0.0f, 0.0f, -0.043f), Quaternion.identity) as GameObject;

			obj.transform.parent = this.transform;
			obj.transform.localScale = new Vector3(1f, 1f, 1f);
			HandlerJewelListItem itemHandler = obj.GetComponentInChildren<HandlerJewelListItem>();
			itemHandler.SetItemInfo(i);
		}
		GetComponent<UIGrid>().Reposition();
	}

	protected void OnSelectedItem()
	{
		//Debug.Log ("OnSelectedItem");
		for(int i = 0;i<this.transform.childCount;++i)
		{
			this.transform.GetChild(i).gameObject.SendMessage("OnSelectedItem");
		}
	}
}
