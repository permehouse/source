using UnityEngine;
using System.Collections;

public class HandlerChanceListGrid : MonoBehaviour
{
	public GameObject prefabChanceListItem;


	void Start ()
	{
		InitStarList();
	}
	
	void Update ()
	{
	
	}
	
	protected void InitStarList()
	{		
		//Debug.Log("Init Star List(), " + UIPayment.starInfoList.Count.ToString());
		for (int i = 0; i < UIPayment.starInfoList.Count; ++i)
		{		
			GameObject obj = Instantiate(prefabChanceListItem, new Vector3(0.0f, 0.0f, -0.043f), Quaternion.identity) as GameObject;

			obj.transform.parent = this.transform;
			obj.transform.localScale = new Vector3(1f, 1f, 1f);
			HandlerChanceListItem itemHandler = obj.GetComponentInChildren<HandlerChanceListItem>();
			itemHandler.SetItemInfo(i);
		}
		GetComponent<UIGrid>().Reposition();
	}

	protected void OnSelectedItem()
	{
		for(int i = 0;i<this.transform.childCount;++i)
		{
			this.transform.GetChild(i).gameObject.SendMessage("OnSelectedItem");
		}
	}
}
