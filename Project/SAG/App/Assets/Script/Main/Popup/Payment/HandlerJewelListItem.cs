using UnityEngine;
using System.Collections;

public class HandlerJewelListItem : MonoBehaviour
{
	
	private int chooseDia;
	

	void Update ()
	{	
	}
		
	public void OnClickChoose()
	{		
	//	Debug.Log("btn Dia Payment Click = "+chooseDia); //선택한 칸.		
	}

	public void SetItemInfo(int iDiaIndex)
	{
	//	Debug.Log("...Dia Set ItemInfo = "+iDiaIndex);
		
		//For test
		if(iDiaIndex < 0 || UIPayment.diaInfoList.Count <= iDiaIndex)		
			return;
			
		chooseDia = iDiaIndex;
		
		UIPayment.DiaInfo dia = (UIPayment.DiaInfo)UIPayment.diaInfoList[iDiaIndex];
		
		UILabel[] labelChildren = GetComponentsInChildren<UILabel>();
		foreach(UILabel labelItem in labelChildren)
		{
			if(labelItem.name == "lblBonus")				//보너스 % 표시.
			{
				
				labelItem.text = (100*(dia.nCount-(dia.nPrice*dia.nMultiply/dia.nDivid)) /(dia.nPrice*dia.nMultiply/dia.nDivid))+"%";
			}			
			else if(labelItem.name == "lblGoods")		
			{	
				labelItem.text = dia.nCount.ToString();				
			}
			else if(labelItem.name == "lblPay")				//가격.
			{
				labelItem.text = dia.nPrice.ToString();				
			}
		}		
	}	
}
