using UnityEngine;
using System.Collections;

public class HandlerCharactorListItem : MonoBehaviour
{
	public UISprite bgDefault;
	public UISprite bgSelected;
	public UISprite bgOwned;
	
	public UIImageButton btnChoose;
	public UIImageButton btnBuy;
	public UIImageButton btnSelected;
	public UIImageButton btnLevelUp;
	//public UISprite imgRelay;
	
	
	
	
	private int chooseCharactor;
	
	private string m_strCharacterCode;
	//private string m_strCharacterExpressionName;		//yj edit.
	private bool m_bOwned;
	private bool m_bSelected;
	//private bool m_bRelay;
	
	void Start ()
	{	
	}

	void Update ()
	{	
	}
		
	//캐릭터 선택. (구입).
	public void OnClickChoose()
	{			
		Debug.Log ("Choose Charactor = "+chooseCharactor);
		
		/*
		 외관 변경. 
		 
		  */
		UICharactorPet.selectedCharacterCode = m_strCharacterCode;			// 방금 선택한 캐릭터를 현재 선택 캐릭터로 등록한다. 		
		//UICharactorPet.selectedCharacterExpressionName = m_strCharacterExpressionName;		
		this.transform.parent.gameObject.SendMessage("OnSelectedItem");		// 전체 instance (캐릭터) 들에게 소유하고 있는지 아닌지에 대한 정보. 
		//서버에 변경 사항 등록 한 후에 메인화면 Update.
		//		SaveCharactorToServer();
		
		
		
		
		
		/*
		 내부 값 변경. 
		 
		  */
		
		//메인 메뉴에서 의 구입.
		if (GlobalValues.userCharactor.mode == GlobalValues.userCharactor.MODE_MENU)
		{
		
			switch (chooseCharactor)
			{
				case 0:
					GameData.mainCharacter =GameData.Character.KYA;
					break;
				case 1:
					GameData.mainCharacter =GameData.Character.WA;
					break;
				case 2:
					GameData.mainCharacter =GameData.Character.WOO;
					break;
				case 3:
					GameData.mainCharacter =GameData.Character.PPA;
					break;
				case 4:
					GameData.mainCharacter =GameData.Character.YAM;
					break;					
			}	
			
			Debug.Log ("$$## GlobalValues.userCharactor.currentEvent = "+GlobalValues.userCharactor.currentEvent);
			
			Debug.Log ("(int)GameData.nPlayerCharacter = "+(int)GameData.nPlayerCharacter);
			
			switch (GlobalValues.userCharactor.currentEvent)
			{			
					
				case 100:// 메인 메뉴 화면.
					GlobalValues.userCharactor.chMenu = chooseCharactor;//(int)GameData.nPlayerCharacter;	//수정..
					break;	
			}
			
		}
		else //서브 메뉴에서 구입.
		{
			//현재 게임 용 캐릭터 설정.
			switch (chooseCharactor)
			{
				case 0:
					GameData.nPlayerCharacter =GameData.Character.KYA;
					break;
				case 1:
					GameData.nPlayerCharacter =GameData.Character.WA;
					break;
				case 2:
					GameData.nPlayerCharacter =GameData.Character.WOO;
					break;
				case 3:
					GameData.nPlayerCharacter =GameData.Character.PPA;
					break;
				case 4:
					GameData.nPlayerCharacter =GameData.Character.YAM;
					break;					
			}				
			
			Debug.Log ("## GlobalValues.userCharactor.currentEvent = "+GlobalValues.userCharactor.currentEvent);
			//선택한 캐릭터를 각 종목에 저장. = 종목별로 유저 캐릭터 세팅.
			switch (GlobalValues.userCharactor.currentEvent)
			{			
				case 0://GlobalValues.userCharactor.GAME_THROW:	
					GlobalValues.userCharactor.chThrow = (int)GameData.nPlayerCharacter;
					break;
				case 1://GlobalValues.userCharactor.GAME_ARCHERY:	
					GlobalValues.userCharactor.chArchery = (int)GameData.nPlayerCharacter;
					break;
				case 2://GlobalValues.userCharactor.GAME_BOXING:	
					GlobalValues.userCharactor.chBoxing = (int)GameData.nPlayerCharacter;
					break;
				case 3://GlobalValues.userCharactor.GAME_SHOOT:	
					GlobalValues.userCharactor.chShoot = (int)GameData.nPlayerCharacter;
					break;
				case 4://GlobalValues.userCharactor.GAME_SWIM:	
					GlobalValues.userCharactor.chSwim = (int)GameData.nPlayerCharacter;
					break;
				case 5:// GlobalValues.userCharactor.GAME_5EVENT:	
					GlobalValues.userCharactor.ch5Event = (int)GameData.nPlayerCharacter;
					break;			
				case 100:// 메인 메뉴 화면.
					GlobalValues.userCharactor.chMenu = (int)GameData.nPlayerCharacter;
					break;	
			}			
					
			Debug.Log ("GameData.nCharacter = "+GameData.nPlayerCharacter);			
			/*
			MessageBox.MessageBoxCallback callback = delegate( CommonDef.MESSAGE_BOX_RESULT result )
			{
				if(result == CommonDef.MESSAGE_BOX_RESULT.MB_YES)
				{
					Debug.Log ("buy message"+result);
					//MessageBox.ShowMessageBox	(GlobalStrings.gsSuccessBuy,CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK,"");
					MessageBox.SimpleOKDialogBox("ok");
				}
				else
					Debug.Log ("buy message"+result);
			};				
			MessageBox.ShowMessageBox(GlobalStrings.gsQuestionBuy,CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_YESNO,callback);				
			*/
		}
		
		
		Debug.Log ("(int)GameData.nPlayerCharacter = "+(int)GameData.nPlayerCharacter);
		Debug.Log ("^^ GlobalValues.userCharactor.chMenu = "+GlobalValues.userCharactor.chMenu);
		
	}
	
	//캐릭터 레벨업. 
	public void OnClickLevelUp()
	{
		Debug.Log ("Btn Level UP"+chooseCharactor);	
	}
	protected void OnSelectedItem()
	{
		m_bSelected = (m_strCharacterCode == UICharactorPet.selectedCharacterCode);
		SetBGState();
		SetButtonState();
	}
	protected void SetBGState()
	{
		bgSelected.gameObject.SetActive(false);
		bgOwned.gameObject.SetActive(false);
		
	//	Debug.Log ("111 m_bSelected = "+m_bSelected);
		
		if(m_bSelected) {
			bgSelected.gameObject.SetActive(true);
		}
		else {
			bgDefault.gameObject.SetActive(true);
			if(m_bOwned) {
				bgOwned.gameObject.SetActive(true);
			}
		}
	}
	
	protected void SetButtonState()
	{
		btnSelected.gameObject.SetActive(false);
		btnChoose.gameObject.SetActive(false);
		btnBuy.gameObject.SetActive(false);
		btnLevelUp.gameObject.SetActive(false);
		
		
	//	Debug.Log ("222 m_bSelected = "+m_bSelected);
		
		if(m_bSelected) {
			
	//		Debug.Log ("3333333333");
			btnSelected.gameObject.SetActive(true);
		}
		else {
			
	//		Debug.Log ("444444444444");
			if(m_bOwned) {
				btnChoose.gameObject.SetActive(true);
			}
			else {
				btnBuy.gameObject.SetActive(true);
			}
			//btnLevelUp.gameObject.SetActive(!m_bSelected && m_bOwned);
		}
	}
	
	public void SetItemInfo(int iCharactorIndex)
	{
		//Debug.Log("...Star Set ItemInfo"+iStarIndex);
			
		
		if(iCharactorIndex < 0 || UICharactorPet.charactorInfoList.Count <= iCharactorIndex)
			return;
					
		
		chooseCharactor = iCharactorIndex;
		
		UICharactorPet.CharactorInfo charactor = (UICharactorPet.CharactorInfo)UICharactorPet.charactorInfoList[iCharactorIndex];
			
		
		m_strCharacterCode = charactor.CharacterCode;
		
	//	Debug.Log ("m_strCharacterCode = "+m_strCharacterCode);
	//	Debug.Log ("UICharactorPet.selectedCharacterCode = "+UICharactorPet.selectedCharacterCode);
		
		m_bOwned = true;//수정. CheckOwnedChar(m_strCharacterCode);
		
		
		
		
		
		
		
		
		
		//m_bSelected = (bool)(m_strCharacterCode == UICharactorPet.selectedCharacterCode);	//현재 선택된 캐릭터 코드와 비교.
		    
		
		Debug.Log ("m b selected");
		Debug.Log ("event = "+GlobalValues.userCharactor.currentEvent);
		Debug.Log ("GlobalValues.userCharactor.chMenu = "+GlobalValues.userCharactor.chMenu);
		
		// 해당 종목에 선택되어진 캐릭터와 현재의 iCharactorIndex 가 같으면 (즉 여러 캐릭터 중에 선택된 캐릭터 면) "선택됨"이라 표시한다.		
			switch (GlobalValues.userCharactor.currentEvent)
			{			
				case 0://GlobalValues.userCharactor.GAME_THROW:	
					Debug.Log (" *** 111");
					m_bSelected = (bool)(iCharactorIndex == GlobalValues.userCharactor.chThrow);
					//GlobalValues.userCharactor.chThrow = (int)GameData.nPlayerCharacter;
					break;
				case 1://GlobalValues.userCharactor.GAME_ARCHERY:	
					m_bSelected = (bool)(iCharactorIndex == GlobalValues.userCharactor.chArchery);
					Debug.Log (" *** 222");
					//GlobalValues.userCharactor.chArchery = (int)GameData.nPlayerCharacter;
					break;
				case 2://GlobalValues.userCharactor.GAME_BOXING:	
					m_bSelected = (bool)(iCharactorIndex == GlobalValues.userCharactor.chBoxing);
					Debug.Log (" *** 333");
					//GlobalValues.userCharactor.chBoxing = (int)GameData.nPlayerCharacter;
					break;
				case 3://GlobalValues.userCharactor.GAME_SHOOT:	
					Debug.Log (" *** 444");
					//GlobalValues.userCharactor.chShoot = (int)GameData.nPlayerCharacter;
					break;
				case 4://GlobalValues.userCharactor.GAME_SWIM:	
					Debug.Log (" *** 555");
					//GlobalValues.userCharactor.chSwim = (int)GameData.nPlayerCharacter;
					break;
				case 5:// GlobalValues.userCharactor.GAME_5EVENT:	
					Debug.Log (" *** 66");
					//GlobalValues.userCharactor.ch5Event = (int)GameData.nPlayerCharacter;
					break;			
				case 100:	//첫 화면에서. 
					m_bSelected = (bool)(iCharactorIndex == GlobalValues.userCharactor.chMenu);
					break;
			}	
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		Debug.Log ("m_bSelected = "+m_bSelected);
		SetBGState();
		SetButtonState();
		
		
		
		//캐릭터 표시.
		UISpriteAnimation[] spriteAniChildren = GetComponentsInChildren<UISpriteAnimation>();
		foreach(UISpriteAnimation spriteAniItem in spriteAniChildren)
		{			
			if(spriteAniItem.name == "imgCharactor")
			{			
				spriteAniItem.framesPerSecond = charactor.animationFramesPerSecond;
				spriteAniItem.namePrefix = charactor.animationNamePrefix;
			}
		}
		
		//레벨 표시. 
		UISprite[] spriteChildren = GetComponentsInChildren<UISprite>();
		foreach(UISprite spriteItem in spriteChildren)
		{
			if(spriteItem.name == "imgGrade")
			{
				spriteItem.gameObject.SetActive(charactor.grade != 'N');
				spriteItem.spriteName = "PopupCharactorListItemGrade" + charactor.grade;
				spriteItem.MakePixelPerfect();
			}
		}
		
		
		//이름, 정보, 레벨, 가격 표시.
		UILabel[] labelChildren = GetComponentsInChildren<UILabel>();
		foreach(UILabel labelItem in labelChildren)
		{
			//charactorName
			if(labelItem.name == "lblName")			//이름.
			{
				labelItem.text = charactor.charactorExpressionName;
			}
			else if(labelItem.name == "lblLevel")
			{
				labelItem.text = "1";		//수정. 차후에 레벨 추가.
			}
			else if(labelItem.name == "lblDescription")
			{
				labelItem.text = charactor.charactorDescription;
			}
			else if(labelItem.name == "lblPrice") 
			{
				labelItem.text =   "price";
			}
			else if(labelItem.name == "lblPower") 
			{				
				labelItem.text =   charactor.nPower.ToString();
			}
			else if(labelItem.name == "lblSpeed") 
			{
				labelItem.text =   charactor.nSpeed.ToString();
			}
			else if(labelItem.name == "lblSkill") 
			{
				labelItem.text =   charactor.nSkill.ToString();
			}
			
			
						
			/*
			if(labelItem.name == "lblBonus")				//보너스 % 표시.
			{
				
				labelItem.text = (100*(star.nCount-(star.nPrice*star.nMultiply/star.nDivid)) /(star.nPrice*star.nMultiply/star.nDivid))+"%";
			}			
			else if(labelItem.name == "lblGoods")		
			{	
				labelItem.text = star.nCount.ToString();				
			}
			else if(labelItem.name == "lblPay")				//가격.
			{
				labelItem.text = star.nPrice.ToString();				
			}
			*/
		}		
	}	
}
