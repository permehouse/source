using UnityEngine;
using System.Collections;

public class HandlerCharactorListGrid : MonoBehaviour
{
	public GameObject prefabCharactorListItem;


	void OnEnable ()
	{
		if (GlobalValues.userCharactor.menuOnEnable == false)
		{
			InitStarList();
		}
		GlobalValues.userCharactor.menuOnEnable = true;
	}
	
	void Update ()
	{
	
	}
	
	protected void InitStarList()
	{		
		Debug.Log("Init char List(), " + UICharactorPet.charactorInfoList.Count.ToString());
		
		//for (int i = 0; i < UIPayment.starInfoList.Count; ++i)
		for (int i = 0; i < UICharactorPet.charactorInfoList.Count; ++i)
		{		
			GlobalValues.userCharactor.objMenuCharactor[i] = Instantiate(prefabCharactorListItem, new Vector3(0.0f, 0.0f, -0.043f), Quaternion.identity) as GameObject;
			
			GlobalValues.userCharactor.objMenuCharactor[i].transform.parent = this.transform;
			GlobalValues.userCharactor.objMenuCharactor[i].transform.localScale = new Vector3(1f, 1f, 1f);
			HandlerCharactorListItem itemHandler = GlobalValues.userCharactor.objMenuCharactor[i].GetComponentInChildren<HandlerCharactorListItem>();
			itemHandler.SetItemInfo(i);
		}
		GetComponent<UIGrid>().Reposition();
	}

	protected void OnSelectedItem()
	{
		for(int i = 0;i<this.transform.childCount;++i)
		{
			this.transform.GetChild(i).gameObject.SendMessage("OnSelectedItem");
		}
	}
}
