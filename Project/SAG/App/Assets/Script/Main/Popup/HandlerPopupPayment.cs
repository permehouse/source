using UnityEngine;
using System.Collections;

public class HandlerPopupPayment : MonoBehaviour {
		
	public GameObject pageChance;
	public GameObject pageGold;
	public GameObject pageJewel;
	
	public UIImageButton tabBtnChance;
	public UIImageButton tabBtnGold;
	public UIImageButton tabBtnJewel;
	
	public UISprite tabImgChance;
	public UISprite tabImgGold;
	public UISprite tabImgJewel;
	
	public UILabel lblGold;
	public UILabel lblSapphire;	
	public int nCurrentPAGE;
	
	private enum PAGE_INDEX
	{
		PAGE_CHANCE = 0,
		PAGE_GOLD,
		PAGE_JEWEL,	
	}
	
	//초기화.
	void OnEnable ()	
	{			
		switch (GlobalValues.userValues.chooseBtnPayment) 
		{
			case "Chance":
					SwitchPage(PAGE_INDEX.PAGE_CHANCE);
					break;
			case "Gold":
					SwitchPage(PAGE_INDEX.PAGE_GOLD);
					break;
			case "Jewel":
					SwitchPage(PAGE_INDEX.PAGE_JEWEL);
					break;			
		}				
	}
	
	//탭 선택.
	public void OnClickTabChance()
	{
		SwitchPage(PAGE_INDEX.PAGE_CHANCE);
	}
	
	public void OnClickTabGold()
	{
		SwitchPage(PAGE_INDEX.PAGE_GOLD);
	}
	
	public void OnClickTabJewel()
	{
		SwitchPage(PAGE_INDEX.PAGE_JEWEL);
	}
	
	private void SwitchPage(PAGE_INDEX indexPage)
	{
		nCurrentPAGE = (int)indexPage;		
		
		pageChance.SetActive(false);
		pageGold.SetActive(false);
		pageJewel.SetActive(false);			
		tabBtnChance.gameObject.SetActive(true);
		tabBtnGold.gameObject.SetActive(true);
		tabBtnJewel.gameObject.SetActive(true);		
		tabImgChance.gameObject.SetActive(false);
		tabImgGold.gameObject.SetActive(false);
		tabImgJewel.gameObject.SetActive(false);		
		
		switch(indexPage)
		{
			case PAGE_INDEX.PAGE_CHANCE:
				pageChance.SetActive(true);
				tabBtnChance.gameObject.SetActive(false);
				tabImgChance.gameObject.SetActive(true);							
				break;
			case PAGE_INDEX.PAGE_GOLD:
				pageGold.SetActive(true);			
				tabBtnGold.gameObject.SetActive(false);
				tabImgGold.gameObject.SetActive(true);				
				break;
			case PAGE_INDEX.PAGE_JEWEL:
				pageJewel.SetActive(true);
				tabBtnJewel.gameObject.SetActive(false);
				tabImgJewel.gameObject.SetActive(true);			
				break;		
		}		
	}
	
	// Use this for initialization
	void Chancet () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	//확인키 눌러서 창 닫음.
	void OnClickOK()
	{
		GameObject currentPanel = GetComponent<UIPanel>().gameObject;
		currentPanel.gameObject.SetActive(false);
	}
	
}
