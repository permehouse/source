using UnityEngine;
using System.Collections;

public class GameData : MonoBehaviour {
//
// Common data for all game
// 
	public static float nScreenWidth = 1280;
	public static float nScreenHeight = 720;
	public static float nScreenCenterX = nScreenWidth/2f;
	public static float nScreenCenterY = nScreenHeight/2f;

	// Layer : Background, UI, Camera
	public static int LAYER_HIDE = 0;
	public static int LAYER_BG = -100;
	public static int LAYER_UI = -800;
	public static int LAYER_MENU = -900;
	public static int LAYER_LOADING = -950;
	public static int LAYER_CAMERA = -1000;
	
	// Layer : Player, Enemy, Objets, Effects ...
	public static int LAYER_OBJECT = -200;
	public static int LAYER_PLAYER = -300;
	public static int LAYER_ENEMY = -400;
	public static int LAYER_ITEM = -500;
	public static int LAYER_EFFECT = -600;

	public static int LAYER_BG_FAR = -50;
	public static int LAYER_EFFECT_BEHINE = -250;
	public static int LAYER_OBJECT_FRONT = -550;
	public static int LAYER_BG_NEAR = -750;
	
	public enum Status {
		START,							// for data loading from server
		PLAY,								// during playing
		PAUSE,							// at the moment of pause
		RESUME,							// for displaying resuming count down 
		END,								// for displaying game result
		QUIT,								// during not playing
		DEFAULT = QUIT
	}
	
	public static Status nStatus = Status.DEFAULT;

	public static string strFriendName = "Friend";
	
	public enum Character {
		NONE = -1,
		DEFAULT=0,
		DEFAULT_FRIEND = 1,
		KYA=0,
		WA,
		WOO,
		PPA,
		YAM,
		COUNT
	}
	public static string[] strCharacterList = new string[(int)Character.COUNT] { "Kya", "Wa", "Woo","Ppa","Yam" };
	public static Character nPlayerCharacter = Character.DEFAULT;
	public static Character mainCharacter = Character.DEFAULT;			//yj.
	public static Character nFriendCharacter = Character.DEFAULT_FRIEND;

	public enum Pet {
		DEFAULT=0,
		PIG=0,
		COUNT
	}
	public static string[] strPetList = new string[(int)Pet.COUNT] { "Pig"};
	public static Pet nPet = Pet.DEFAULT;
	
	public class Settings {
		public static bool bSoundOn = true;
	}

	public enum ControlResult {
		BAD,
		GOOD,
		PERFECT
	}	

	public enum Medal {
		NONE,
		BRONZE,							
		SILVER,							
		GOLD,					
		GOLD_2,
		GOLD_3,
		DEFAULT = NONE
	}
	
//
// Data for each Game
//	

// Game Data for Throw
	public class Throw {
		public static int nRound = 1;
		public static int nMaxRound = 3;
		
		public static float nSpeed = 0f;
		public static float nMinSpeed = 50f;//20f;	//for test.
		public static float nMaxSpeed = 100f;
		public static float nSpeedUpRatio = 1.1f;
		public static float nSpeedUpPerfectRatio = 1.3f;
		public static float nPower = 0f;
		public static float nMaxPower = 100f;
		public static float nPowerMovePerSec = 10f;
		public static float nAngle = 45f;
		public static float nTimeToFinishAfterLanding = 1f;
		
		public static bool bControlSpeedGaugeActive = false;
		public static float nControlSpeedGauge = 0f;
		public static float nControlSpeedMaxGauge = 100.0f;
		public static float nControlSpeedGaugeMovePerSec = 0f;
		public static float nControlSpeedGaugeStartMovePerSec = 100.0f;
		public static float nControlSpeedGaugeStartMovePerSecRatio = 1.1f;
		public static bool nControlSpeedGaugeUp = true;
		public static float nControlSpeedGaugeSize = 280f;		// you have to set this value with the width of gauge image
		public static float nControlSpeedTargetPos = 0f;
		public static float nControlSpeedTargetSize = 56f;			// you have to set this value with the width of target image
		public static float nControlSpeedTargetPerfectSize = 5f;
		public static float nControlSpeedTargetMoveSpeed = 200f;

		public static float nControlAngleGauge = 0f;
		public static float nControlAngleMaxGauge = 90.0f;
		public static float nControlAngleGaugeMovePerSec = 120.0f;
		
		public static float nDisplaySpeed = 5.0f;
		public static float dxDisplayMove = 0f;
		public static float dyDisplayMove = 0f;
		
		public static float nDisplaySlowEffectRatio = 1f;
		public static float nDisplaySlowEffectAdjustRatio = 0.01f;
		public static float nDisplaySlowEffectDuration = 1f;
		
		public static float nPlayerSpinFPS = 40f;
		public static float nThrowingObjectThrowingFPS = 30f;
		
		public static float nMeterPerPixel = 0.1f;		// 0.1 meter per one pixel, (100 meter = 1000 pixel)
		
		public static float nGravity = (10f / nMeterPerPixel);

		public static float nThrowingObjectReadyPosX = -100f;
		public static float nThrowingObjectReadyPosY = 123f;
		public static float nThrowingObjectSpinPosX = 0f;
		public static float nThrowingObjectSpinPosY = 182f;
		public static float nThrowingObjectLandingPosY = 170f;

		public static float nThrowingDistance = 0f;
		public static float nThrowingHeight = 0f;
		public static float nThrowingAngle = 0f;
		public static float nThrowingAmpSpeedX = 20.0f;
		public static float nThrowingAmpSpeedY = 10.0f;
		public static float nThrowingSpeedX = 0f;
		public static float nThrowingSpeedY = 0f;
		public static float nThrowingPosX = 0f;
		public static float nThrowingPosY = 0f;

		public static float nLandingHeight = nThrowingObjectReadyPosY;
		
		public static float nWindSpeed = 0f;
		public static float nWindMaxSpeed = 10f;
		public static float nWindSpeedFPS = 30f;
		public static float nGoodWindSpeed = 20f;
		public static float nGoodWindPossibility = 5f;
		public static float nWindTextPosZero = -35f;
		public static float nWindTextPosPlus = -40f;
		public static float nWindTextPosMinus = 35f;
		
		public static int nGroundHiddenBounceObjectCount = 1;
		public static float nGroundHiddenBounceObjectStartPosX = -2000f;
		public static float nGroundHiddenBounceObjectEndPosX = -500f;
		public static float nGroundHiddenBounceObjectMinAdderX = 2000f;					//밸런스 조절 변수.
		public static float nGroundHiddenBounceObjectMaxAdderX = 3000f;
		public static float nGroundHiddenBounceObjectMinAdderY = 200f;					//밸런스 조절 변수.
		public static float nGroundHiddenBounceObjectMaxAdderY = 300f;

		public static int nGroundBounceObjectCount = 5;
		public static float nGroundBounceObjectStartPosX = 3*nScreenWidth;
		public static float nGroundBounceObjectEndPosX = 40*nScreenWidth;
		public static float nGroundBounceObjectPosY = 170f;
		public static float nGroundBounceObjectMinAdderY = 100f;					//밸런스 조절 변수.
		public static float nGroundBounceObjectMaxAdderY = 500f;
		
		public static int nSkyBounceObjectCount = 5;
		public static float nSkyBounceObjectStartPosX = 2*nScreenWidth;
		public static float nSkyBounceObjectEndPosX = 40*nScreenWidth;
		public static float nSkyBounceObjectStartPosY = nScreenHeight;
		public static float nSkyBounceObjectEndPosY = 5*nScreenHeight;
		public static float nSkyBounceObjectMinAdderX = 100f;
		public static float nSkyBounceObjectMaxAdderX = 200f;
		public static float nSkyBounceObjectMinAdderY = 300f;						//밸런스 조절 변수.
		public static float nSkyBounceObjectMaxAdderY = 500f;

		public static int nSkyBlockObjectCount = 3;
		public static float nSkyBlockObjectStartPosX = 3*nScreenWidth;
		public static float nSkyBlockObjectEndPosX = 40*nScreenWidth;
		public static float nSkyBlockObjectStartPosY = nScreenHeight;
		public static float nSkyBlockObjectEndPosY = 5*nScreenHeight;
		public static float nSkyBlockObjectMinAmplifierX = 0.25f;
		public static float nSkyBlockObjectMaxAmplifierX = 0.4f;
		public static float nSkyBlockObjectMinAmplifierY = 0.25f;					//밸런스 조절 변수.
		public static float nSkyBlockObjectMaxAmplifierY = 0.4f;
		
		public class Result {
			public static bool bNoNextMedal = false;
			public static int nRecord = 0;
			public static int nBestRecord = 0;
			public static int nRank = 0;
			
			public static Medal nMedal = Medal.DEFAULT;
			public static int nMedalRecord = 0;
			public static Medal nNextMedal = Medal.DEFAULT;
			public static int nNextMedalRecord = 0;
			
			public static Medal TranslateMedal(string strMedal, int nCount) {
				Medal nMedalTemp = Medal.NONE; 
				if(strMedal == "GOLD") {
					if(nCount == 3) nMedalTemp = Medal.GOLD_3;
					else if(nCount == 2) nMedalTemp = Medal.GOLD_2;
					else nMedalTemp = Medal.GOLD;
				}
				else if(strMedal == "SILVER") nMedalTemp = Medal.SILVER;
				else if(strMedal == "BRONZE") nMedalTemp = Medal.BRONZE;
				
				return nMedalTemp;
			}
		}
		
		public enum PlayStatus {
			ROUND,
			SPIN,
			ANGLE,
			THROW,
			LANDING,
			FINISH,
			FOUL,
			DEFAULT
		}

		public static PlayStatus nPlayStatus = PlayStatus.DEFAULT;
	};
	
// Game Data for Archery
	public class Archery {
		public static int nRound = 1;
		public static int nMaxRound = 5;
		public static int nPerfectScore = 10;
		
		public static int nScore = 0;
		public static int nPlayerScore = 0;
		public static int []listPlayerRoundScore = new int[nMaxRound];
		public static int nFriendScore = 0;
		public static int []listFriendRoundScore = new int[nMaxRound];
		
		public static float nZoomScale = 1f/0.3f;
		public static float nZoomSpeed = 100f;
		public static float nZoomFastSpeed = 300f;
		public static float nZoomImmediateSpeed = 2000f;
		public static float nZoomPlayerSpeed = nZoomSpeed * 1.2f;
		public static float nZoomBGSpeed = nZoomSpeed * 1.1f;
		public static Vector2 posZoomCenter = new Vector2(GameData.nScreenWidth/2, GameData.nScreenHeight/2);

		public static float nPlayingFieldSpeed = 100f;
		public static float nPlayingFieldPlayerPosX = 0f;
		public static float nPlayingFieldFriendPosX = 1000f;

		public static float nAimPosX = 0f;
		public static float nAimPosY = 0f;
		public static float nAimMaxTargetDistance = 140f;
		public static float nAimMaxScoreDistance = 121f;			// Target Radius in taret image (pixel)
		public static float nAimWindScoreDistance = nAimMaxScoreDistance / 2f;			// Distance per a second that the arrrow can move when wind speed is maximum
		public static float nAimPerfectScoreDistance = 8f;			// Perfect zone Radius in taret image (pixel)
		public static float nAimIntervalScoreDistance = (nAimMaxScoreDistance - nAimPerfectScoreDistance) / (nPerfectScore - 1f);		// pixel size of each score from 1 to 9
		public static float nAimSpeed = 3f;

		public static bool bAimTime = false;
		public static float nAimTime = 0;
		public static float nAimMaxTime = 10f;
		public static float nAimFriendEndTime = 0;
		public static float nAimFriendMimWaitTime = 1f;
		
		public static float nAimControlDirX = 0f;
		public static float nAimControlDirY = 0f;
		
		public static float nWindDirX = 0f;
		public static float nWindDirY = 0f;
		public static float nMaxDir = 10f;
		
		public static float nWindSpeed = 0f;
		public static float nWindSpeedFPS = 30f;
		public static float nWindMinSpeed = 1f;
		public static float nWindMaxSpeed = 10f;

		public static float nArrowFlyingPosX = -215f;
		public static float nArrowFlyingPosY = 60f;
		public static float nArrowImageAdjustPosX = 11f;
		public static float nArrowImageAdjustPosY = -12f;

		public class Result {
			public static int nRecord = 0;
			public static int nPrevRecord = 0;
			public static int nRank = 0;
			public static int nFriendRecord = 0;
			public static Medal nMedal = Medal.DEFAULT;
			public static Medal nGoalMedal = Medal.DEFAULT;
			
			public static void SetMedal(string strMedal, int nCount) {
				if(strMedal == "GOLD") {
					if(nCount == 3) nMedal = Medal.GOLD_3;
					else if(nCount == 2) nMedal = Medal.GOLD_2;
					else nMedal = Medal.GOLD;
				}
				else if(strMedal == "SILVER") nMedal = Medal.SILVER;
				else if(strMedal == "BRONZE") nMedal = Medal.SILVER;
				else 	nMedal = Medal.NONE;
				
				nGoalMedal = nMedal+ 1;
				if(nGoalMedal > Medal.GOLD_3) {
					nGoalMedal = Medal.GOLD_3;
				}
			}
		}
		
		public enum PlayStatus {
			ROUND,
			READY,
			DRAW,
			AIM,
			SHOOT,
			MARK,
			FINISH,
			FRIEND_READY,
			FRIEND_DRAW,
			FRIEND_AIM,
			FRIEND_SHOOT,
			FRIEND_MARK,
			FRIEND_FINISH,
			DEFAULT = ROUND
		}

		public static PlayStatus nPlayStatus = PlayStatus.DEFAULT;
	};	
}
