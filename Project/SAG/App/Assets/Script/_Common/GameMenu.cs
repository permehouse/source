using UnityEngine;
using System.Collections;

public class GameMenu : MonoBehaviour {
	public GameObject obPause;
	public GameObject obBackground;
	public GameObject obResume;
	public GameObject obQuit;
	public GameObject obCountDown;
	
	void Start() {
		ShowMenu (false);
		ShowMenuBackground(false);
		
		obCountDown.SetActive(false);
	}

	public void ShowMenuBackground(bool bShow) {
		obBackground.SetActive(bShow);
	}
	
	public void ShowMenu(bool bShow) {
		obResume.SetActive(bShow);
		obQuit.SetActive(bShow);
	}

	public void OnPause() {
		if(GameData.nStatus == GameData.Status.PLAY) {
			GameData.nStatus = GameData.Status.PAUSE;
//			GamePlay.ShowController(false);
			obPause.SetActive(false);
			
			ShowMenu(true);	
			ShowMenuBackground(true);
		}
	}
	
	public void OnResume() {
		if(GameData.nStatus == GameData.Status.PAUSE) {
			ShowMenu(false);	
			ShowCountDown();
		}
	}
	
	public void OnQuit() {
		ShowMenu(false);	
		GameData.nStatus = GameData.Status.QUIT;
		
		Application.LoadLevel("Main");		
	}
	
	public void ShowCountDown() {
		obCountDown.SetActive(true);
		
		StartCoroutine(CR_CountDown());
	}
	
	IEnumerator CR_CountDown() {
		for(int nCountDown = 3; nCountDown >=1; nCountDown--) {
			_CountDown(nCountDown);
			yield return new WaitForSeconds(1f);
		}
		
		_RestartGame();
	}
	
	void _CountDown(int nCountDown) {
	 	tk2dSprite obSprite = obCountDown.GetComponent<tk2dSprite>();
		obSprite.spriteId = obSprite.GetSpriteIdByName("Menu_CountDown" + nCountDown);
			
		obCountDown.transform.localScale = new Vector3(0f, 0f, 0f);
			
		Vector3 scaleChageSize = new Vector3(1f, 1f, 1f);
		 iTween.EaseType easeType =  iTween.EaseType.easeOutBack;
		float nChageSizeTime = 0.6f;
		
		Hashtable tweenParam = new Hashtable();
		tweenParam.Add ("scale", scaleChageSize);
		tweenParam.Add ("easetype", easeType);
		tweenParam.Add ("time", nChageSizeTime);
		
		iTween.ScaleTo(obSprite.gameObject, tweenParam);
	}
	
	void _RestartGame() {
		obCountDown.SetActive(false);
		ShowMenuBackground(false);
		
		obPause.SetActive(true);
		GameData.nStatus = GameData.Status.PLAY;
	}
}