using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ServerConnector : MonoBehaviour
{
	public string strDomainName { get { return "http://" + GlobalValues.strHostName; } }
	protected string strURLLogin { get { return strDomainName + "/user/login"; } }
	
	protected string strURLLogout { get { return strDomainName + "/user/logout/"; } }
	protected string strURLCreateUser { get { return strDomainName + "/user"; } }
	
	protected string strURLRanking { get { return strDomainName + "/ranking"; } }
	protected string strURLRankingThrow { get { return strDomainName + "/game/throw/"; } }
	protected string strURLRankingThrowEndPointStart { get { return "/start"; } }
	protected string strURLRankingThrowEndPointEnd { get { return "/end"; } }
	
	
	protected string strURLAllRankingThrow { get { return strDomainName + "/ranking/throw/"; } }
	
	
	protected delegate void CallbackResponse(bool bSuccess, JSONObject jsonObjResponse);
	public delegate void CallbackResult(bool bSuccess);
	
	static public ServerConnector _instance = null;
	static public ServerConnector GetInstance()
	{
		return _instance;
	}
	
	public void Awake() {
		//Debug.Log ("Server connector Awake");
		_instance = this;
	}

	public ServerConnector()
	{
	}

	void Start ()
	{
	//	Debug.Log ("Server connector Start");
	}
	
	void Update ()
	{
	}
	
	public void All_Ranking_Throw(CallbackResult cbAllRanking)
	{
		
		CallbackResponse cbResponse = delegate(bool bSuccess, JSONObject jsonObjResponse) {
						
			if(bSuccess) {				//랭킹 받아오기 성공
				Debug.Log ("All ranking success "+jsonObjResponse);
								
				GlobalValues.GameContact contact = new GlobalValues.GameContact();		
		
				GlobalValues.friendList.Clear();
				
				
				//랭킹에 관련된 JSon만 추출.
				JSONObject jsonObjGameContace = jsonObjResponse["rankingList"];
				Debug.Log ("All jsonObjGameContace3 "+jsonObjGameContace);
				Debug.Log ("Detail = "+jsonObjGameContace[0]["name"].str);
				Debug.Log ("Detail2= "+jsonObjGameContace[0]["usid"].n);
				
				for(int i=0;i<jsonObjGameContace.Count;++i)
				{				
					contact.usid = (int)jsonObjGameContace[i]["usid"].n;					
					contact.name = jsonObjGameContace[i]["name"].str;
					contact.nickname = jsonObjGameContace[i]["nickname"].str;
					//contact.character = jsonObjGameContace[i]["character"].str;
					contact.character = "CH_PORORO_A";		//수정. 
					contact.record = (int)jsonObjGameContace[i]["record"].n;
					//contact.digestFishes = jsonObjGameContace[i]["digestFishes"].n;
					//contact.weekDistance = jsonObjGameContace[i]["weekDistance"].n;
					//contact.week = jsonObjGameContace[i]["week"].n;
					//contact.imageUrl = jsonObjGameContace[i]["imageUrl"].str;
					//contact.chanceReceive = jsonObjGameContace[i]["chanceReceive"].n;
					//contact.pushReceive = jsonObjGameContace[i]["pushReceive"].n;
					GlobalValues.friendList.Add(contact);
					
				//	Debug.Log(">>> FriendList " + (i+1) + "/" + jsonObjGameContace.count() + ", guid : " + contact.guid + ", name : " + contact.name + ",  week : " + contact.week + ",  weekScore : " + contact.weekScore);
				}
								
				contact.usid = 1;
				
				contact.name = "yjkim";
				contact.nickname = "yjpark@funy";
				//contact.character = "CH_PORORO_A";			//캐릭터 코드. 중요함. 캐릭터 Icon 을 결정한다.
				//contact.imageUrl = "www.funnysoft.co.kr";
				//contact.chanceReceive = 5;
				//contact.pushReceive = 1;
				contact.record = 12345;
				//contact.week = 240;		
				
				//contact.digestFishes = 1;
				//contact.weekDistance = 100;
								
			}
			
			if(cbAllRanking != null) {
				cbAllRanking(bSuccess);
			}
						
		};	
				
		string strURL = strURLAllRankingThrow;// + GlobalValues.sessionId;
		JSONObject objAllRanking = new JSONObject(JSONObject.Type.OBJECT);
		// nothing to set
		
		QueryServer(strURL, cbResponse);
	}
	
	
	public void GameStart_Throw(CallbackResult cbGameStart)
	{
		CallbackResponse cbResponse = delegate(bool bSuccess, JSONObject jsonObjResponse) {
			if(bSuccess) {
				GlobalValues.sessionId = jsonObjResponse["sessionId"].str;
				GlobalValues.tokenId = jsonObjResponse["token"].str;
				
				GameData.Throw.Result.bNoNextMedal = false;
				JSONObject jsonObjGameAction = jsonObjResponse["gameAction"];
				if(jsonObjGameAction != null) {
					GameData.Throw.Result.nRank = (int)jsonObjGameAction["currentRank"].n;
					GameData.Throw.Result.nBestRecord = (int)jsonObjGameAction["currentRecord"].n;
					GameData.Throw.Result.nMedal = GameData.Throw.Result.TranslateMedal(jsonObjGameAction["currentMedal"].str, (int)jsonObjGameAction["medalCount"].n);
					
					// current medal data
					JSONObject jsonObjMedalData = jsonObjGameAction["currentBoundary"];
					if(jsonObjMedalData != null && !jsonObjMedalData.IsNull) {
						GameData.Throw.Result.nMedalRecord = (int)jsonObjMedalData["record"].n;
					}
					else {
						GameData.Throw.Result.nMedalRecord = GameData.Throw.Result.nBestRecord;
					}
					
					// next medal data
					jsonObjMedalData = jsonObjGameAction["nextBoundary"];
					if(jsonObjMedalData != null && !jsonObjMedalData.IsNull) {
						GameData.Throw.Result.nNextMedal = GameData.Throw.Result.TranslateMedal(jsonObjMedalData["medal"].str, (int)jsonObjMedalData["medalCount"].n);
						GameData.Throw.Result.nNextMedalRecord = (int)jsonObjMedalData["record"].n;
					}
					else {
						GameData.Throw.Result.bNoNextMedal = true;
					}
				}
			}
			
			if(cbGameStart != null) {
				cbGameStart(bSuccess);
			}
		};
		
		string strURL = strURLRankingThrow + GlobalValues.sessionId + strURLRankingThrowEndPointStart;
		JSONObject objRanking = new JSONObject(JSONObject.Type.OBJECT);
		// nothing to set
		
		QueryServer(strURL, objRanking.ToString(), cbResponse);
	}
	
	public void GameEnd_Throw(CallbackResult cbGameEnd)
	{
		CallbackResponse cbResponse = delegate(bool bSuccess, JSONObject jsonObjResponse) {
			if(bSuccess) {
//				GlobalValues.sessionId = jsonObjResponse["sessionId"].str;
//				GlobalValues.tokenId = jsonObjResponse["token"].str;
				
				// Do nothing...
			}
			
			if(cbGameEnd != null) {
				cbGameEnd(bSuccess);
			}
		};
		
		string strURL = strURLRankingThrow + GlobalValues.sessionId + "/" + GlobalValues.tokenId + strURLRankingThrowEndPointEnd;
		JSONObject objRanking = new JSONObject(JSONObject.Type.OBJECT);
		objRanking.AddField("record", GameData.Throw.Result.nRecord);
		
		QueryServer(strURL, objRanking.ToString(), cbResponse);
	}
	
	//유저 생성.
	public void CreateUser(string id,string pw,string nickname,CallbackResult cbCreateUser)
	{
		CallbackResponse cbResponse = delegate(bool bSuccess, JSONObject jsonObjResponse) {
			if(bSuccess) {
				//유저 생성  성공.
				GlobalValues.userValues.id = id;
			}
			
			if(cbCreateUser != null) {
				cbCreateUser(bSuccess);
			}
		};	
		
		JSONObject objUser = new JSONObject(JSONObject.Type.OBJECT);
		
		objUser.AddField("id", id);
		objUser.AddField("password", pw);
		objUser.AddField("nickname", nickname);		
		
		Debug.Log("strURLCreateUser : " + objUser);
		
		QueryServer(strURLCreateUser, objUser.ToString(), cbResponse);		
	}
	
	public void Logout(string strSessionId, CallbackResult cbLogout)
	{
		CallbackResponse cbResponse = delegate(bool bSuccess, JSONObject jsonObjResponse) {
			if(cbLogout != null) {
				cbLogout(bSuccess);
			}
		};
		
		Debug.Log("Logout : " + strSessionId);
		string strURL = strURLLogout+strSessionId;
		
		
		JSONObject objLogout = new JSONObject(JSONObject.Type.OBJECT);
		objLogout.AddField("sessionId", strSessionId);
		
		QueryServer(strURL,objLogout.ToString(), cbResponse);
	}
	
	
	public void Login(string strID, string strPW, CallbackResult cbLogin) {
		CallbackResponse cbResponse = delegate(bool bSuccess, JSONObject jsonObjResponse) {
			if(bSuccess) {
				GlobalValues.sessionId = jsonObjResponse["sessionId"].str;
				
				Debug.Log ("sessid iD " + GlobalValues.sessionId);
				
				JSONObject jsonObjGameUser = jsonObjResponse["user"];
				if(jsonObjGameUser != null) {
					GlobalValues.gameUser.name =  jsonObjGameUser["name"].str;	//수정.0623.
				}
				
				//GlobalValues.gameUser.name = jsonObjResponse["name"].str;
/* 다음과 같이  UserData 가 넘어어옴.  아래 GlobalValues 의 User 정보에 맞게 넣으면 됨.
   "user":    {
      "usid": 1,
      "id": "test",
      "token": null,
      "password": "TESTPASSWORD",
      "passwordFailCount": 0,
      "name": "TEST_NICKNAME",
      "nickname": "TEST_NICKNAME",
      "sex": null,
      "email": null,
      "celphone": null,
      "birthday": null,
      "birthdayType": null,
      "loginCount": 18,
      "loginTime": "140614120140",
      "loginIp": "218.156.68.160",
      "activate": true,
      "unsubscribed": false,
      "writeDate": "140505000839",
      "modifyDate": "140614115805"
   }				
*/				
/*			
				JSONObject jsonObjGameUser = jsonObjResponse["gameUser"];

				GlobalValues.gameUser.SetGameUserData(jsonObjGameUser);
				GlobalValues.settings.chanceReceive = jsonObjGameUser["chanceReceive"].n;
				GlobalValues.settings.soundPlay = jsonObjGameUser["sound"].n;
				GlobalValues.settings.pushReceive = jsonObjGameUser["pushReceive"].n;
				GlobalValues.settings.dragController = jsonObjGameUser["control"].n;
				GlobalValues.settings.lowQuality = jsonObjGameUser["quality"].n;				
*/
			}
			
			if(cbLogin != null) {
				cbLogin(bSuccess);
			}
		};

		JSONObject objLogin = new JSONObject(JSONObject.Type.OBJECT);
		objLogin.AddField("id", strID);
		objLogin.AddField("password", strPW);
		
		Debug.Log("Logon : " + strURLLogin + ", " + strID + ", " + strPW);
		
		QueryServer(strURLLogin, objLogin.ToString(), cbResponse);
	}
	
	//Common connector
	protected void QueryServer(string strURL, CallbackResponse cbResponse) {			// GET
		StartCoroutine(ConnectServer(strURL, null, cbResponse));
	} 
	
	protected void QueryServer(string strURL, string strJsonData, CallbackResponse cbResponse) {		// POST
		StartCoroutine(ConnectServer(strURL, strJsonData, cbResponse));
	} 

	protected IEnumerator ConnectServer(string strURL, string strJsonData, CallbackResponse cbResponse)
	{
        yield return null;
/*
		if(strJsonData == null) {	// GET
			Debug.Log(" *** GET Request =" + strURL);
	
			WWW www = new WWW(strURL);
			yield return www;
			
			OnResponseConnectServer(www, cbResponse);
		}
		else {									// POST
			Debug.Log(" *** POST Request =" + strURL);
			Debug.Log(" *** POST Data =" + strJsonData);
			
            Dictionary<string,string> headers = new Dictionary<string,string>();
            headers.Add("Host", GlobalValues.strHostName);
            headers.Add("Content-Type", "application/json; charset=utf-8");

//            headers["Host"] = GlobalValues.strHostName;
//			headers["Content-Type"] = "application/json; charset=utf-8";
			byte[] data = System.Text.Encoding.UTF8.GetBytes(strJsonData);
	
			WWW www = new WWW(strURL, data, headers);
			yield return www;

			OnResponseConnectServer(www, cbResponse);
		}*/
	}
	
	void OnResponseConnectServer(WWW www, CallbackResponse cbResponse) {
		bool bSuccess = true;
		JSONObject jsonObjResponse = null;

		if(www.error != null && www.error != "") {	// error
			bSuccess = false;
			
			GlobalValues.errorCodeConnector = www.error.Substring(0, www.error.IndexOf(' '));
			GlobalValues.errorMessageConnector = www.error;
			
			Debug.Log(" *** Response Error, Request : " + www.url);
			Debug.Log(" *** Response Error : " + GlobalValues.errorMessageConnector);
		}		
		else {
			Debug.Log(" *** Response : " + www.text);
			jsonObjResponse = new JSONObject(www.text);
			if(jsonObjResponse["errorCode"] != null) {
				bSuccess = false;	
				GlobalValues.errorCodeConnector = jsonObjResponse["errorCode"].str;
				GlobalValues.errorMessageConnector = jsonObjResponse["errorMessage"].str;
				
				Debug.Log(" *** Response Server Error, Request : " + www.url);
				Debug.Log(" *** Response Server Error : " + GlobalValues.errorCodeConnector + ", "  + GlobalValues.errorMessageConnector);
			}
		}
			
		cbResponse(bSuccess, jsonObjResponse);
	}
}
