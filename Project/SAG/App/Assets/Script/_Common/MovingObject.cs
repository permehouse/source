using UnityEngine;
using System.Collections;

public class MovingObject : MonoBehaviour {
	public tk2dSpriteAnimator obAni = null;
	protected bool bPaused = false;
	
	Vector2 posMoveTo;
	float nSpeed;
	protected bool bMoving = false;

	Vector2 scaleTo;
	protected float nScaleSpeed;
	Vector2 posScaleCenter;
	protected bool bScaling = false;
	
	public virtual void Awake() {
		nSpeed = 500f;
		posMoveTo = new Vector2(transform.localPosition.x, transform.localPosition.y);
		
		nScaleSpeed = 50f;
		scaleTo = new Vector2(transform.localScale.x, transform.localScale.y);
		posScaleCenter = new Vector2(0f, 0f);
	}
	
	public virtual void Update() {
		CheckGamePaused();
		if(bPaused) {
			return;
		}
		
		UpdateMove();	
		UpdateScale();
	}
	
	void CheckGamePaused() {
		if(!bPaused && GameData.nStatus == GameData.Status.PAUSE) {
			bPaused = true;
			if(obAni) {
				obAni.Pause();
			}
		}
		else if(bPaused && GameData.nStatus == GameData.Status.PLAY) {
			bPaused = false;
			if(obAni) {
				obAni.Resume();
			}
		}
	}
	
	public void UpdateMove() {
		if(!bMoving) {
			return;
		}
		
		float nX = X();
		float nY = Y();

		if(posMoveTo.x == nX && posMoveTo.y == nY) {
			bMoving = false;
			return;
		}
		
		float nRate = (nSpeed / 10f) * Time.deltaTime;
		if(nRate>1f) nRate = 1f;
			
		float nNextX = nX + (posMoveTo.x - nX) * nRate;
		float nNextY = nY + (posMoveTo.y - nY) * nRate;
		if(Mathf.Abs(posMoveTo.x - nNextX) < 0.1f) {
			nNextX = posMoveTo.x;
		}
		
		if(Mathf.Abs(posMoveTo.y - nNextY) < 0.1f) {
			nNextY = posMoveTo.y;
		}

		transform.localPosition = new Vector3(nNextX, nNextY, transform.localPosition.z);
	}
	
	public void UpdateScale() {
		if(!bScaling) {
			return;
		}
		
		float nX = ScaleX();
		float nY = ScaleY();

		if(scaleTo.x == nX && scaleTo.y == nY) {
			bScaling = false;
			return;
		}
		
		float nRate = (nScaleSpeed / 10f) * Time.deltaTime;
		if(nRate>1f) nRate = 1f;
			
		float nNextX = nX + (scaleTo.x - nX) * nRate;
		float nNextY = nY + (scaleTo.y - nY) * nRate;
		if(Mathf.Abs(scaleTo.x - nNextX) < 0.001f) {
			nNextX = scaleTo.x;
		}
		
		if(Mathf.Abs(scaleTo.y - nNextY) < 0.001f) {
			nNextY = scaleTo.y;
		}
		
		transform.localScale = new Vector3(nNextX, nNextY, transform.localScale.z);
		transform.localPosition = new Vector3(posMoveTo.x + (posMoveTo.x-posScaleCenter.x)*(nNextX - 1f), posMoveTo.y + (posMoveTo.y-posScaleCenter.y)*(nNextY - 1f), transform.localPosition.z);
	}
	
	public void Pos(float nX, float nY) {
		posMoveTo = new Vector2(nX, nY);
		transform.localPosition = new Vector3(nX, nY, transform.localPosition.z);
	}
	
	public void Translate(float dx, float dy) {
		Pos(transform.localPosition.x + dx, transform.localPosition.y + dy);
	}
	
	public void MoveToPos(float nX, float nY) {
		bMoving = true;
		posMoveTo = new Vector2(nX, nY);
	}
	
	public void Move(float dx, float dy) {
		bMoving = true;
		posMoveTo = new Vector2(posMoveTo.x + dx,  posMoveTo.y + dy);	
	}
	
	public void Scale(float nX, float nY) {
		scaleTo = new Vector2(nX, nY);
		transform.localScale = new Vector3(nX, nY, transform.localScale.z);
	}
	
	public void ReScale(float nX, float nY) {
		bScaling = true;
		scaleTo = new Vector2(nX, nY);
	}
	
	public void Angle(float nAngle) {
		transform.localEulerAngles = new Vector3(0f, 0f, nAngle);
	}
	
	public void SetSpeed(float speed) {
		nSpeed = speed;
	}
	
	public void SetScaleSpeed(float speed) {
		nScaleSpeed = speed;
	}
	
	public void SetScaleCenterPos(Vector2 pos) {
		posScaleCenter	= new Vector2(pos.x, pos.y);
	}
	
	public float X() {
		return transform.localPosition.x;
	}
	
	public float Y() {
		return transform.localPosition.y;
	}
	
	public float ScaleX() {
		return transform.localScale.x;
	}
	
	public float ScaleY() {
		return transform.localScale.y;
	}	
}
