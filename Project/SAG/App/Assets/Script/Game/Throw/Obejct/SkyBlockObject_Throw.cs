using UnityEngine;
using System.Collections;

public class SkyBlockObject_Throw : Object_Throw {
	public void OnStartGame() {
		SetType(Object_Throw.Type.sky_block);			
	}		
	
	public override void OnRound() {
		base.OnRound();
		
		float nX = Random.Range(GameData.Throw.nSkyBlockObjectMinAmplifierX, GameData.Throw.nSkyBlockObjectMaxAmplifierX);
		float nY = Random.Range(GameData.Throw.nSkyBlockObjectMinAmplifierY, GameData.Throw.nSkyBlockObjectMaxAmplifierY);
		
		SetAmplifier(nX, nY);
	}
	
	public override void SetPos() {
		float nX = Random.Range(GameData.Throw.nSkyBlockObjectStartPosX, GameData.Throw.nSkyBlockObjectEndPosX);
		float nY = Random.Range(GameData.Throw.nSkyBlockObjectStartPosY, GameData.Throw.nSkyBlockObjectEndPosY);
		
		if(bTest) {
			nX = GameData.Throw.nSkyBlockObjectStartPosX;
			nY = GameData.Throw.nSkyBlockObjectStartPosY;
		}
		
		Pos(nX, nY);
	}		
}
