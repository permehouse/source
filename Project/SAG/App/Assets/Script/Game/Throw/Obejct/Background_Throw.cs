using UnityEngine;
using System.Collections;

public class Background_Throw : MonoBehaviour {
	// Use this for initialization
	public MovingObject obBG_Mid1;
	public MovingObject obBG_Mid2;
	public MovingObject obBG_Far1;
	public MovingObject obBG_Far2;

 	tk2dSprite obSpriteBG_Mid = null;
 	tk2dSprite obSpriteBG_Far = null;
	
	Vector2 sizeBG_Mid;
	Vector2 sizeBG_Far;
	
	float nFarBG_SpeedDownRatio = 0.05f;
	
	void Awake() {
	 	obSpriteBG_Mid = obBG_Mid1.GetComponent<tk2dSprite>();
	 	obSpriteBG_Far = obBG_Far1.GetComponent<tk2dSprite>();
	}
		
	// Use this for initialization
	void Start () {
		ResetPosition();
	}
	
	void ResetPosition() {
		sizeBG_Mid = new Vector2(obSpriteBG_Mid.GetBounds().size.x, obSpriteBG_Mid.GetBounds().size.y);
		sizeBG_Far = new Vector2(obSpriteBG_Far.GetBounds().size.x, obSpriteBG_Far.GetBounds().size.y);
		
		// Mid BG
		float nBG_Mid_X = 0;
		float nBG_Mid_Y = 0;
		obBG_Mid1.Pos(nBG_Mid_X, nBG_Mid_Y);		
		
		nBG_Mid_X += sizeBG_Mid.x;
		obBG_Mid2.Pos(nBG_Mid_X, nBG_Mid_Y);
		
		// Far BG
		float nBG_Far_X = 0;
		float nBG_Far_Y = 0;
		obBG_Far1.Pos(nBG_Far_X, nBG_Far_Y);		
		
		nBG_Far_X += sizeBG_Far.x;
		obBG_Far2.Pos(nBG_Far_X, nBG_Far_Y);
	}
	
	void UpdateThrowing() {
		float nCenterX = GameData.nScreenWidth/2f;
		float nThrowingObjectStartPosX = 100f;
		GameData.Throw.dxDisplayMove = 0;
		if(GameData.Throw.nThrowingPosX > nCenterX) {
			GameData.Throw.dxDisplayMove = nCenterX - GameData.Throw.nThrowingPosX;
		}
		else if(GameData.Throw.nThrowingPosX < nThrowingObjectStartPosX) {
			GameData.Throw.dxDisplayMove = nThrowingObjectStartPosX - GameData.Throw.nThrowingPosX;
		}
		
		float nCenterY = GameData.nScreenHeight/2f;
		GameData.Throw.dyDisplayMove = 0;
		if(GameData.Throw.nThrowingSpeedY >= 0) {
			if(GameData.Throw.nThrowingPosY > nCenterY) {
				GameData.Throw.dyDisplayMove = nCenterY - GameData.Throw.nThrowingPosY;
			}
		}
		else {
			if(GameData.Throw.nThrowingPosY < nCenterY) {
				GameData.Throw.dyDisplayMove = nCenterY - GameData.Throw.nThrowingPosY;
				// not to set the position of backgroiund over 0.
				if(obBG_Mid1.transform.position.y + GameData.Throw.dyDisplayMove > 0) {
					GameData.Throw.dyDisplayMove = -obBG_Mid1.transform.position.y;
				}
			}
		}
	}
	
	void OnUpdateThrowingPos() {
		float dxMove = GameData.Throw.dxDisplayMove;
		float dyMove = GameData.Throw.dyDisplayMove;
		float dxMoveFarBG = GameData.Throw.dxDisplayMove * nFarBG_SpeedDownRatio;
		float dyMoveFarBG = GameData.Throw.dyDisplayMove * nFarBG_SpeedDownRatio;
		
		Move(obBG_Mid1, dxMove, dyMove, sizeBG_Mid.x);
		Move(obBG_Mid2, dxMove, dyMove, sizeBG_Mid.x);
		Move(obBG_Far1, dxMoveFarBG, dyMoveFarBG, sizeBG_Far.x);
		Move(obBG_Far2, dxMoveFarBG, dyMoveFarBG, sizeBG_Far.x);
	}
	
	void Move(MovingObject obBG, float dxMove, float dyMove, float nBGWidth) {
		obBG.Translate (dxMove, dyMove);

		if(dxMove<0f) {
			if(obBG.transform.position.x <= - nBGWidth) {
				obBG.Translate(2*nBGWidth, 0);
			}
		}
		else {
			if(obBG.transform.position.x >= nBGWidth) {
				obBG.Translate(-2*nBGWidth, 0);
			}
		}
	}
	
	public void OnRound() {
		ResetPosition();
	}
}
