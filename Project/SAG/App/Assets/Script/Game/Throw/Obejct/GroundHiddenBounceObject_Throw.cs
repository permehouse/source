using UnityEngine;
using System.Collections;

public class GroundHiddenBounceObject_Throw : Object_Throw {
	public void OnStartGame() {
		SetType(Object_Throw.Type.ground_hidden_bounce);			
	}		
	
	public override void OnRound() {
		base.OnRound();
		
		float nX = 0f;
		float nY = 0f;
		SetAmplifier(-1f, 1f);
			
		nX = Random.Range(GameData.Throw.nGroundHiddenBounceObjectMinAdderX, GameData.Throw.nGroundHiddenBounceObjectMaxAdderX);
		nY = Random.Range(GameData.Throw.nGroundHiddenBounceObjectMinAdderY, GameData.Throw.nGroundHiddenBounceObjectMaxAdderY);
		
		SetAdder(nX, nY);
	}
	
	public override void SetPos() {
		float nX = Random.Range(GameData.Throw.nGroundHiddenBounceObjectStartPosX, GameData.Throw.nGroundHiddenBounceObjectEndPosX);
		float nY = GameData.Throw.nGroundBounceObjectPosY;
		
		if(bTest) {
			nX = GameData.Throw.nGroundHiddenBounceObjectStartPosX;
		}		
		
		Pos(nX, nY);
	}		
}
