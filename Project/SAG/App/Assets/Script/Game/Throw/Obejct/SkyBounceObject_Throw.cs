using UnityEngine;
using System.Collections;

public class SkyBounceObject_Throw : Object_Throw {
	public void OnStartGame() {
		SetType(Object_Throw.Type.sky_bounce);	
	}		
	
	public override void OnRound() {
		base.OnRound();
		
		float nX = Random.Range(GameData.Throw.nSkyBounceObjectMinAdderX, GameData.Throw.nSkyBounceObjectMaxAdderX);
		float nY = Random.Range(GameData.Throw.nSkyBounceObjectMinAdderY, GameData.Throw.nSkyBounceObjectMaxAdderY);
		
		SetAdder(nX, nY);
	}
	
	public override void SetPos() {
		float nX = Random.Range(GameData.Throw.nSkyBounceObjectStartPosX, GameData.Throw.nSkyBounceObjectEndPosX);
		float nY = Random.Range(GameData.Throw.nSkyBounceObjectStartPosY, GameData.Throw.nSkyBounceObjectEndPosY);
		
		if(bTest) {
			nX = GameData.Throw.nSkyBounceObjectStartPosX;
			nY = GameData.Throw.nSkyBounceObjectStartPosY;
		}		
		
		Pos(nX, nY);
	}
}
