using UnityEngine;
using System.Collections;

public class Player_Throw : MonoBehaviour {
	public tk2dSpriteAnimator aniPlayer = null;
	bool bPaused = false;

	string strPlayer;
	
	public void Update() {
		CheckGamePaused();
		if(bPaused) {
			return;
		}
		
		UpdateSpinSpeed();		
	}
	
	void CheckGamePaused() {
		if(!bPaused && GameData.nStatus == GameData.Status.PAUSE) {
			bPaused = true;
			aniPlayer.Pause();
		}
		else if(bPaused && GameData.nStatus == GameData.Status.PLAY) {
			bPaused = false;
			aniPlayer.Resume();
		}
	}
	
	public void OnRound() {
		strPlayer = GameData.strCharacterList[(int)GameData.nPlayerCharacter];
		
		aniPlayer.Play(strPlayer + "_Ready");
		gameObject.transform.localPosition = new Vector3(0f, gameObject.transform.localPosition.y, GameData.LAYER_PLAYER);
	}
	
	public void OnSpin() {
		aniPlayer.Play(strPlayer + "_Spin");
	}
	
	public void OnThrow() {
		aniPlayer.Play(strPlayer + "_Throw");
	}
	
	public void OnUpdateThrowingPos() {
		float dxMove = GameData.Throw.dxDisplayMove;
		float dyMove = GameData.Throw.dyDisplayMove;
		
		gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x + dxMove, gameObject.transform.localPosition.y + dyMove, GameData.LAYER_PLAYER);
	}		
	
	void UpdateSpinSpeed() {
		if(GameData.Throw.nPlayStatus != GameData.Throw.PlayStatus.SPIN) {
			return;
		}
		
		aniPlayer.ClipFps = GameData.Throw.nPlayerSpinFPS * (GameData.Throw.nSpeed / GameData.Throw.nMaxSpeed);
	}
}
