using UnityEngine;
using System.Collections;

public class GroundBounceObject_Throw : Object_Throw {
	public void OnStartGame() {
		SetType(Object_Throw.Type.ground_bounce);	
	}		
	
	public override void OnRound() {
		base.OnRound();
		
		float nX = 0f;
		float nY = 0f;
		nY = Random.Range(GameData.Throw.nGroundBounceObjectMinAdderY, GameData.Throw.nGroundBounceObjectMaxAdderY);
		
		SetAdder(nX, nY);
	}
	
	public override void SetPos() {
		float nX = Random.Range(GameData.Throw.nGroundBounceObjectStartPosX, GameData.Throw.nGroundBounceObjectEndPosX);
		float nY = GameData.Throw.nGroundBounceObjectPosY;
		
		if(bTest) {
			nX = GameData.Throw.nGroundBounceObjectStartPosX;
		}				
		
		Pos(nX, nY);
	}		
}
