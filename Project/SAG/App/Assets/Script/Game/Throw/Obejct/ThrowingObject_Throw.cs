using UnityEngine;
using System.Collections;

public class ThrowingObject_Throw : MovingObject {
	public GameData obGamePlay;
	
	string strPet;
	float nInitialThrowingSpeed = 0f;
	
	public override void Update() {
		base.Update();
		if(bPaused) {
			return;
		}
		
		UpdateSpinSpeed();		
		UpdateThrowingSpeed();		
	}		
	
	public void OnRound() {
		strPet = GameData.strPetList[(int)GameData.nPet];
		obAni.Play(strPet + "_Ready");
		
		Angle(0f);
		Pos (GameData.Throw.nThrowingObjectReadyPosX  , GameData.Throw.nThrowingObjectReadyPosY);
	}
	
	public void OnSpin() {
		obAni.Play(strPet + "_Spin");
		Pos (GameData.Throw.nThrowingObjectSpinPosX  , GameData.Throw.nThrowingObjectSpinPosY);
	}
	
	public void OnThrow() {
		obAni.Play(strPet + "_Throwing");
			
		GameData.Throw.nThrowingSpeedX = GameData.Throw.nThrowingAmpSpeedX * GameData.Throw.nSpeed * (1 + GameData.Throw.nPower/100f) * Mathf.Cos(GameData.Throw.nAngle * (Mathf.PI / 180f));
		GameData.Throw.nThrowingSpeedY = GameData.Throw.nThrowingAmpSpeedY * GameData.Throw.nSpeed * (1 + GameData.Throw.nPower/100f) * Mathf.Sin(GameData.Throw.nAngle * (Mathf.PI / 180f));
		
		nInitialThrowingSpeed = Mathf.Sqrt(GameData.Throw.nThrowingSpeedX*GameData.Throw.nThrowingSpeedX + GameData.Throw.nThrowingSpeedY*GameData.Throw.nThrowingSpeedY);
		if(nInitialThrowingSpeed == 0f) nInitialThrowingSpeed = 0.01f;
		
		GameData.Throw.nThrowingDistance = 0f;
		GameData.Throw.nThrowingHeight = GameData.Throw.nThrowingObjectSpinPosY;
	}	

	public void UpdateThrowing() {
		float dxTime = Time.deltaTime * GameData.Throw.nDisplaySpeed * GameData.Throw.nDisplaySlowEffectRatio;

		GameData.Throw.nThrowingSpeedX += (GameData.Throw.nWindSpeed * dxTime);
		GameData.Throw.nThrowingSpeedY -= (GameData.Throw.nGravity * dxTime);
		float dxMove = GameData.Throw.nThrowingSpeedX * dxTime;
		float dyMove = GameData.Throw.nThrowingSpeedY * dxTime;

		if(dyMove < 0f && (GameData.Throw.nThrowingHeight + dyMove) <= GameData.Throw.nLandingHeight) {
			dxMove = dxMove * Mathf.Abs(GameData.Throw.nThrowingHeight / dyMove);
			dyMove = GameData.Throw.nLandingHeight - GameData.Throw.nThrowingHeight;
		}
		else {
			// Set Angle only if not landing..
			float nAngle = 0f;
			if(Mathf.Abs(dxMove) == 0f) {
				nAngle = Mathf.Sign(dyMove) * 90f;
			}
			else {
				nAngle = Mathf.Atan(dyMove/dxMove) * (180f / Mathf.PI);
				if(dxMove < 0f) nAngle += 180f;
			}
			Angle(nAngle);
		}
		
		GameData.Throw.nThrowingPosX = gameObject.transform.localPosition.x + dxMove;
		GameData.Throw.nThrowingPosY = gameObject.transform.localPosition.y + dyMove;
		
		GameData.Throw.nThrowingDistance += dxMove;
		GameData.Throw.nThrowingHeight += dyMove;
	}
	
	public void OnUpdateThrowingPos() {
		float dxMove = GameData.Throw.dxDisplayMove;
		float dyMove = GameData.Throw.dyDisplayMove;
		
		Pos(GameData.Throw.nThrowingPosX + dxMove, GameData.Throw.nThrowingPosY + dyMove);
	}
	
	void UpdateSpinSpeed() {
		if(GameData.Throw.nPlayStatus != GameData.Throw.PlayStatus.SPIN) {
			return;
		}
		
		obAni.ClipFps = GameData.Throw.nPlayerSpinFPS * (GameData.Throw.nSpeed / GameData.Throw.nMaxSpeed);
	}
	
	void UpdateThrowingSpeed() {
		if(GameData.Throw.nPlayStatus != GameData.Throw.PlayStatus.THROW) {
			return;
		}
		
		float nSpeed = Mathf.Sqrt(GameData.Throw.nThrowingSpeedX*GameData.Throw.nThrowingSpeedX + GameData.Throw.nThrowingSpeedY*GameData.Throw.nThrowingSpeedY);
		obAni.ClipFps = GameData.Throw.nThrowingObjectThrowingFPS * (nSpeed / nInitialThrowingSpeed);
	}
	
	void Show(bool bShow) {
		if(bShow) {
			gameObject.transform.localPosition = new Vector3(X(), Y(), GameData.LAYER_OBJECT);
		}
		else {
			gameObject.transform.localPosition = new Vector3(X(), Y(), GameData.LAYER_HIDE);
		}
	}

	public void Landing() {
		Angle(0f);
		Pos (X() , GameData.Throw.nThrowingObjectLandingPosY);
		
		string strClipName = strPet + "_Landing";
		if(GameData.Throw.nSpeed == GameData.Throw.nMaxSpeed) strClipName += "_High";
		else if(GameData.Throw.nSpeed < GameData.Throw.nMaxSpeed / 4f) strClipName += "_Low";
		else strClipName += "_Mid";
		
		obAni.Play(strClipName);
		obAni.AnimationCompleted = OnCompleteLanding;	
	}
	
	public void OnCompleteLanding(tk2dSpriteAnimator ani, tk2dSpriteAnimationClip clip) {
		obAni.AnimationCompleted = null;
		
		obGamePlay.SendMessage("EndLanding");
	}
}
