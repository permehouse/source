using UnityEngine;
using System.Collections;

public class Object_Throw : MovingObject {
	public GameObject obSound;
	
	public enum Type {
		ground_bounce,
		ground_hidden_bounce,
		sky_bounce,
		sky_block
	}
	
	public enum Location {
		sky,
		ground,
	}
	
	protected Type nType = Type.ground_bounce;
	
	float nAmplifierX = 1f;
	float nAmplifierY = 1f;
	float nAdderX = 0f;
	float nAdderY = 0f;
	
	bool bCollision = false;
	float bDisplaySlowEffectTime = 0f;

	protected bool bTest = false;	
	
	public void SetType(Type type) {
		nType = type;
	}
	
	public void SetAmplifier(float nX, float nY) {
		nAmplifierX = nX;
		nAmplifierY = nY;
	}
	
	public void SetAdder(float nX, float nY) {
		nAdderX = nX;
		nAdderY = nY;
	}
	
	public void ModifyThrowingSpeed() {
		if(nType == Type.ground_bounce || nType == Type.ground_hidden_bounce || nType == Type.sky_bounce) {
			GameData.Throw.nThrowingSpeedY = Mathf.Abs(GameData.Throw.nThrowingSpeedY);
		}
		
		GameData.Throw.nThrowingSpeedX *= nAmplifierX;
		GameData.Throw.nThrowingSpeedY *= nAmplifierY;
		
		GameData.Throw.nThrowingSpeedX += nAdderX;
		GameData.Throw.nThrowingSpeedY += nAdderY;

		GameData.Throw.nDisplaySlowEffectRatio = GameData.Throw.nDisplaySlowEffectAdjustRatio;
		bDisplaySlowEffectTime = GameData.Throw.nDisplaySlowEffectDuration;
	}
	
	public void UpdateThrowingPos() {
		Translate(GameData.Throw.dxDisplayMove, GameData.Throw.dyDisplayMove);
		
		if(bDisplaySlowEffectTime > 0) {
			bDisplaySlowEffectTime -= Time.deltaTime;
			if(bDisplaySlowEffectTime < 0f) {
				bDisplaySlowEffectTime = 0f;
				GameData.Throw.nDisplaySlowEffectRatio = 1f;
			}
		}
	}
	
	public virtual void OnRound() {
		bCollision = false;	
		bDisplaySlowEffectTime = 0f;
		GameData.Throw.nDisplaySlowEffectRatio = 1f;
		
		SetPos();
	}
	
	void OnTriggerEnter(Collider other) {
		if(!bCollision) {
			CheckCollision(other.gameObject);
		}
	}
	
	void CheckCollision(GameObject obOther) {
		ThrowingObject_Throw obThrowingObject = obOther.GetComponent("ThrowingObject_Throw") as ThrowingObject_Throw;
		if(obThrowingObject) {
			bCollision = true;
			obAni.Play ();
			obSound.SendMessage("PlayObjectCollision", nType);
			ModifyThrowingSpeed();
		}
		
		Object_Throw obj = obOther.GetComponent("Object_Throw") as Object_Throw;
		if(obj) {
			obj.SetPos();
		}
	}
	
	public virtual void SetPos() {
		// Implemetation will be the in each overriding function.
	}
}
