using UnityEngine;
using System.Collections;

public class UI_Throw : MonoBehaviour {
	public GameObject obGamePlay;	
	
	public GameObject obControlSpeed;
	public GameObject obControlAngle;
	public GameObject obUIGoal;
	public GameObject obUIDistance;
	public GameObject obUIWind;
	public GameObject obUIResult;
	public GameObject obUILoading;
	
 	public UISprite spUIPowerGauge;
 	public UISprite spUISpeedGauge;
 	public UISprite spControlSpeedGauge;
 	public MovingObject obControlSpeedTarget;
 	public tk2dTextMesh txtUIPlayerName;
 	public tk2dTextMesh txtUIGoalDistance;
 	public tk2dTextMesh txtUIDistance;
 	public tk2dTextMesh txtUIWindSpeed;
 	public tk2dTextMesh txtUIResultRecord;
 	public tk2dTextMesh txtUIResultBestRecord;
 	public tk2dTextMesh txtUIResultRank;
 	public tk2dTextMesh txtUIResultMedalRecord;
 	public tk2dTextMesh txtUIResultNextMedalRecord;
	public tk2dSprite spUIResultMedal;
	public tk2dSprite spUIResultNextMedal;
	public tk2dSprite spUIGoalMedal;

 	public UISprite spControlAngleGauge;
	public GameObject obControlAngleTrotractorArrow;
	
 	public tk2dSpriteAnimator aniUIWind;
 	public tk2dSpriteAnimator aniUIControlSpeedResult;
 	public tk2dSpriteAnimator aniUIRound;
 	public tk2dSpriteAnimator aniUIPlayer;
	
	bool bPaused = false;
	
	public void Awake() {
	}
	
	public void Start() {
	}
	
	public void Update() {
		CheckGamePaused();		
		if(bPaused) {
			return;
		}
		
		UpdateControlSpeed();
		UpdateControlAngle();
		
		UpdateUIPower();
		UpdateUISpeed();
		
		UpdateUIGoal();
		UpdateUIDistance();
		UpdateUIWind();
		UpdateUIResult();
	}
	
	void CheckGamePaused() {
		if(!bPaused && GameData.nStatus == GameData.Status.PAUSE) {
			bPaused = true;
			
 			aniUIWind.Pause();
 			aniUIControlSpeedResult.Pause();
 			aniUIRound.Pause();
		}
		else if(bPaused && GameData.nStatus == GameData.Status.PLAY) {
			bPaused = false;
			
 			aniUIWind.Resume();
 			aniUIControlSpeedResult.Resume();
 			aniUIRound.Resume();
		}
	}	
	
	public void ShowUI() {
		obControlSpeed.SetActive(false);
		if(GameData.Throw.nPlayStatus == GameData.Throw.PlayStatus.SPIN) {
			obControlSpeed.SetActive(true);
			aniUIControlSpeedResult.gameObject.SetActive(false);			
		}
		
		obControlAngle.SetActive(false);
		if(GameData.Throw.nPlayStatus == GameData.Throw.PlayStatus.ANGLE) {
			obControlAngle.SetActive(true);
		}
		
		obUIDistance.SetActive(false);
		if(GameData.Throw.nPlayStatus == GameData.Throw.PlayStatus.THROW || GameData.Throw.nPlayStatus == GameData.Throw.PlayStatus.LANDING || GameData.Throw.nPlayStatus == GameData.Throw.PlayStatus.FINISH) {
			obUIDistance.SetActive(true);
		}
		
		obUIWind.SetActive(false);
		if(GameData.Throw.nPlayStatus == GameData.Throw.PlayStatus.THROW || GameData.Throw.nPlayStatus == GameData.Throw.PlayStatus.LANDING || GameData.Throw.nPlayStatus == GameData.Throw.PlayStatus.FINISH) {
			obUIWind.SetActive(true);
		}

		obUIResult.SetActive(false);
		if(GameData.Throw.nPlayStatus == GameData.Throw.PlayStatus.FINISH) {
			obUIResult.SetActive(true);
		}
		
		obUILoading.SetActive(false);
		if(GameData.nStatus == GameData.Status.START) {
			obUILoading.SetActive(true);
		}		
		
		obUIGoal.SetActive(!GameData.Throw.Result.bNoNextMedal);
	}
	
	public void OnRound() {
		ShowUICharacter();
		ShowUIRound();
	}	
	
	public void OnThrow() {
		if(GameData.Throw.nWindSpeed == GameData.Throw.nGoodWindSpeed) {
			aniUIWind.Play("GoodWind");
		}
		else {
			aniUIWind.Play("Wind");
		}
	}	
	
	void ShowUICharacter() {
		string strPlayer = GameData.strCharacterList[(int)GameData.nPlayerCharacter];
		aniUIPlayer.Play("Character_" + strPlayer);
		txtUIPlayerName.text = GlobalValues.gameUser.name;
	}
	
	void UpdateUIPower() {
		if(GameData.Throw.nPlayStatus != GameData.Throw.PlayStatus.ROUND && GameData.Throw.nPlayStatus != GameData.Throw.PlayStatus.SPIN) {
			return;
		}
		
		spUIPowerGauge.fillAmount = (GameData.Throw.nPower / GameData.Throw.nMaxPower);
	}
	
	void UpdateUISpeed() {
		if(GameData.Throw.nPlayStatus != GameData.Throw.PlayStatus.ROUND && GameData.Throw.nPlayStatus != GameData.Throw.PlayStatus.SPIN) {
			return;
		}
		
		spUISpeedGauge.fillAmount = (GameData.Throw.nSpeed / GameData.Throw.nMaxSpeed);
	}

	void UpdateUIGoal() {
		if(GameData.Throw.nPlayStatus != GameData.Throw.PlayStatus.ROUND) {
			return;
		}
		
		SetMedalImage(spUIGoalMedal, GameData.Throw.Result.nNextMedal);
		txtUIGoalDistance.text = GameData.Throw.Result.nNextMedalRecord.ToString("N0");
	}
	
	void UpdateUIDistance() {
		if(GameData.Throw.nPlayStatus != GameData.Throw.PlayStatus.THROW && GameData.Throw.nPlayStatus != GameData.Throw.PlayStatus.LANDING && GameData.Throw.nPlayStatus != GameData.Throw.PlayStatus.FINISH) {
			return;
		}
		
		GameData.Throw.Result.nRecord = (int)(GameData.Throw.nMeterPerPixel * GameData.Throw.nThrowingDistance);
		txtUIDistance.text = GameData.Throw.Result.nRecord.ToString("N0");
	}

	void UpdateUIWind() {
		if(GameData.Throw.nPlayStatus != GameData.Throw.PlayStatus.THROW) {
			return;
		}
		
		if(GameData.Throw.nWindSpeed == 0f) {
			aniUIWind.ClipFps = 0.1f;
			aniUIWind.gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
		}
		else if(GameData.Throw.nWindSpeed == GameData.Throw.nGoodWindSpeed) {
			aniUIWind.gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
		}
		else if(GameData.Throw.nWindSpeed>0) {
			aniUIWind.gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
			aniUIWind.ClipFps = GameData.Throw.nWindSpeedFPS * (GameData.Throw.nWindSpeed / GameData.Throw.nWindMaxSpeed);
		}
		else {
			aniUIWind.gameObject.transform.localScale = new Vector3(-1f, 1f, 1f);
			aniUIWind.ClipFps = GameData.Throw.nWindSpeedFPS * (-GameData.Throw.nWindSpeed / GameData.Throw.nWindMaxSpeed);
		}
		
		int nWindSpeed = Mathf.RoundToInt(GameData.Throw.nWindSpeed);
		
		if(GameData.Throw.nWindSpeed == 0f) {
			txtUIWindSpeed.text = "";
			txtUIWindSpeed.gameObject.transform.localPosition = new Vector3(GameData.Throw.nWindTextPosZero, txtUIWindSpeed.gameObject.transform.localPosition.y, txtUIWindSpeed.gameObject.transform.localPosition.z);
		}
		if(GameData.Throw.nWindSpeed > 0f) {
			txtUIWindSpeed.text = "+";
			txtUIWindSpeed.gameObject.transform.localPosition = new Vector3(GameData.Throw.nWindTextPosPlus, txtUIWindSpeed.gameObject.transform.localPosition.y, txtUIWindSpeed.gameObject.transform.localPosition.z);
		}
		else if(GameData.Throw.nWindSpeed < 0f) {
			txtUIWindSpeed.text = "-";
			txtUIWindSpeed.gameObject.transform.localPosition = new Vector3(GameData.Throw.nWindTextPosMinus, txtUIWindSpeed.gameObject.transform.localPosition.y, txtUIWindSpeed.gameObject.transform.localPosition.z);
		}
		txtUIWindSpeed.text += Mathf.Abs(nWindSpeed).ToString("0");
	}
	
	void UpdateUIResult() {
		if(GameData.Throw.nPlayStatus != GameData.Throw.PlayStatus.FINISH) {
			return;
		}

		if(GameData.Throw.Result.nRecord > GameData.Throw.Result.nBestRecord) {
			GameData.Throw.Result.nBestRecord = GameData.Throw.Result.nRecord;
		}
		
		txtUIResultRecord.text = GameData.Throw.Result.nRecord.ToString("0");
		txtUIResultBestRecord.text = GameData.Throw.Result.nBestRecord.ToString("0");
		txtUIResultRank.text = GameData.Throw.Result.nRank.ToString("0");
		txtUIResultMedalRecord.text = GameData.Throw.Result.nMedalRecord.ToString("0");
		txtUIResultNextMedalRecord.text = GameData.Throw.Result.nNextMedalRecord.ToString("0");
		
		SetMedalImage(spUIResultMedal, GameData.Throw.Result.nMedal);
		SetMedalImage(spUIResultNextMedal, GameData.Throw.Result.nNextMedal);
	}
	
	void SetMedalImage(tk2dSprite spMedel, GameData.Medal nMedal) {
		string strClipName = "UI_Medal_";
		switch(nMedal) {
		case GameData.Medal.GOLD : strClipName += "Gold_1"; break;
		case GameData.Medal.GOLD_2 : strClipName += "Gold_2"; break;
		case GameData.Medal.GOLD_3: strClipName += "Gold_3"; break;
		case GameData.Medal.SILVER : strClipName += "Silver"; break;
		case GameData.Medal.BRONZE : strClipName += "Bronze"; break;
		default : strClipName = null; break;
		}
		
		if(strClipName == null) {
			spMedel.gameObject.SetActive(false);
		}
		else {
			spMedel.gameObject.SetActive(true);
			spMedel.spriteId = spMedel.GetSpriteIdByName(strClipName);		
		}
	}
	
	void UpdateControlSpeed() {
		if(GameData.Throw.nPlayStatus != GameData.Throw.PlayStatus.SPIN) {
			return;
		}
		
		spControlSpeedGauge.fillAmount = (GameData.Throw.nControlSpeedGauge / GameData.Throw.nControlSpeedMaxGauge);

		// Target Pos
		obControlSpeedTarget.SetSpeed(GameData.Throw.nControlSpeedTargetMoveSpeed);
		obControlSpeedTarget.MoveToPos(GameData.Throw.nControlSpeedTargetPos, obControlSpeedTarget.Y());
		obControlSpeedTarget.UpdateMove();
	}
	
	void UpdateControlAngle() {
		if(GameData.Throw.nPlayStatus != GameData.Throw.PlayStatus.ANGLE) {
			return;
		}
		
		spControlAngleGauge.fillAmount = (GameData.Throw.nControlAngleGauge / GameData.Throw.nControlAngleMaxGauge);
		
		// Set Arrow Angle
		obControlAngleTrotractorArrow.transform.localEulerAngles = new Vector3(0f, 0f, GameData.Throw.nControlAngleGauge);
	}
	
	public void ShowControlSpeedResult(GameData.ControlResult nResult) {
		string strClipName;
		if(nResult == GameData.ControlResult.PERFECT) strClipName = "Control_Perfect";
		else if(nResult == GameData.ControlResult.GOOD) strClipName = "Control_Good";
		else strClipName = "Control_Bad";
		
		aniUIControlSpeedResult.gameObject.SetActive(true);
		aniUIControlSpeedResult.Play(strClipName);
		aniUIControlSpeedResult.AnimationCompleted = OnCompleteShowControlSpeedResult;	
	}
	
	public void OnCompleteShowControlSpeedResult(tk2dSpriteAnimator ani, tk2dSpriteAnimationClip clip) {
		aniUIControlSpeedResult.gameObject.SetActive(false);
	}
	
	public void ShowUIRound() {
		aniUIRound.gameObject.SetActive(true);

		string strClipName = "Round" + GameData.Throw.nRound;
		if(GameData.Throw.nRound == GameData.Throw.nMaxRound) {
			strClipName += "_Last";
		}
		aniUIRound.Play(strClipName);
		aniUIRound.AnimationCompleted = OnCompleteShowUIRound;	
	}
	
	public void OnCompleteShowUIRound(tk2dSpriteAnimator ani, tk2dSpriteAnimationClip clip) {
		string strClipName = "Round_Start";
		
		aniUIRound.Play(strClipName);
		aniUIRound.AnimationCompleted = OnCompleteShowUIRoundStart;	
	}	
	
	public void OnCompleteShowUIRoundStart(tk2dSpriteAnimator ani, tk2dSpriteAnimationClip clip) {
		aniUIRound.gameObject.SetActive(false);
		
		obGamePlay.SendMessage("EndShowUIRound");
	}
}
