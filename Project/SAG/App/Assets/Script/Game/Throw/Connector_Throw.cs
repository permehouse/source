using UnityEngine;
using System.Collections;

public class Connector_Throw : ServerConnector {
	public GameObject obGamePlay;
	
	public void OnStartGame() {
        StartGame();
        return;

		if(GlobalValues.sessionId != "") {		// alrady loginned
			StartGame();
		}
		else {													// login first
			CallbackResult cbReponse = delegate(bool bSuccess) {
				if(bSuccess) {
					StartGame();
				}
				else {
					Debug.Log("Error : Stop game because there's no login user!!");
				}
			};
			// for test
//			string strID = "test";
//			string strPW = "TESTPASSWORD";
			string strID = "Charles";
			string strPW = "Charles";
//			string strID = "Allright";// "supersonic";//"basketman";
//			string strPW = "Allright";//"supersonic";//"basketman";
			if (GlobalValues.userValues.isLogin == false)		//0629
				Login(strID, strPW, cbReponse);
		}
	}
	
	void StartGame() {
        obGamePlay.SendMessage("OnResponseStartGame", true);
        return;

		CallbackResult cbReponse = delegate(bool bSuccess) {
			obGamePlay.SendMessage("OnResponseStartGame", bSuccess);
		};
	
		GameStart_Throw(cbReponse);
	}
	
	public void OnLanding() {
        TempStartGame();
        return;

		CallbackResult cbReponse = delegate(bool bSuccess) {
			TempStartGame();		// request start game to get ranking information
		};
		
		GameEnd_Throw(cbReponse);
	}
	
	// temporary function
	public void TempStartGame() {
        obGamePlay.SendMessage("OnResponseUpdateRecord", true);
        return;

		CallbackResult cbReponse = delegate(bool bSuccess) {
			obGamePlay.SendMessage("OnResponseUpdateRecord", bSuccess);
		};
	
		GameStart_Throw(cbReponse);
	}
}