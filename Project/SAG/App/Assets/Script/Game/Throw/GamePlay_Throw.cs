using UnityEngine;
using System.Collections;

public class GamePlay_Throw : MonoBehaviour {
	public GameObject obBackground;
	public GameObject obPlayer;
	public GameObject obThrowingObject;
	public GameObject obUI;
	public GameObject obSound;
	public GameObject obObjects;
	public Controller_Throw obController;
	public Connector_Throw obConnector;
 	public MovingObject obControlSpeedTarget;
	
	// Object
	public Object_Throw pfGroundHiddenBounceObject;
	public Object_Throw pfGroundBounceObject;
	public Object_Throw pfSkyBounceObject;
	public Object_Throw pfSkyBlockObject;	
	ArrayList listObject = new ArrayList();
	
	bool bFinishLanding = false;
	bool bFinishUpdateRecord = false;
		
	// for Test
	public bool bTest = false;//true;//false;
	public float nTest_Speed = 50f;
	public float nTest_Angle = 45f;
	public float nTest_WindSpeed = 0f;
	public GameData.Character nTestPlayerCharacter = GameData.Character.NONE;

	public void Awake() {
		CreateObjects();
	}
	
	public void Start() {
		StartGame();
	}
	
	void StartGame() {
		GameData.nStatus = GameData.Status.START;
		GameData.Throw.nRound = 1;
		
		SetEvent("StartGame");
	}

	public void OnResponseStartGame(bool bSuccess) {
		if(bSuccess) {
			StartRound();
		}
		else {
			Debug.Log("ResponseStartGame Failure : " + GlobalValues.errorMessageConnector);
		}
	}
	
	void EndGame() {
		GameData.nStatus = GameData.Status.END;
		SetEvent("EndGame");
		
		QuitGame();
	}
	
	void QuitGame() {
		GameData.nStatus = GameData.Status.QUIT;
		SetEvent("QuitGame");
		
		Application.LoadLevel("Main");
	}
	
	void Update() {
		if(GameData.nStatus != GameData.Status.PLAY) {
			return;
		}
		
		UpdateThrowing();
		
		UpdatePower();
		UpdateControlSpeed();
		UpdateControlAngle();
	}
	
	public void CreateObjects() {
		Object_Throw obj = null;
		Vector3 pos = new Vector3(0f, 0f, GameData.LAYER_OBJECT);		
		
		if(bTest) {
			GameData.Throw.nGroundBounceObjectCount = 1;	
			GameData.Throw.nSkyBounceObjectCount = 1;	
			GameData.Throw.nSkyBlockObjectCount = 1;
			GameData.nPlayerCharacter = nTestPlayerCharacter;
		}
		
		// Special GroundHiddenBounceObject (behind start point)
		for(int i = 0; i < GameData.Throw.nGroundHiddenBounceObjectCount; i++) {
			obj =  MonoBehaviour.Instantiate(pfGroundHiddenBounceObject, pos, Quaternion.identity) as Object_Throw;
			obj.transform.parent = obObjects.transform;
			obj.obSound = obSound;
			
			listObject.Add(obj);
		}
		
		// GroundBounceObject
		for(int i = 0; i < GameData.Throw.nGroundBounceObjectCount; i++) {
			obj =  MonoBehaviour.Instantiate(pfGroundBounceObject, pos, Quaternion.identity) as Object_Throw;
			obj.transform.parent = obObjects.transform;
			obj.obSound = obSound;
			
			listObject.Add(obj);
		}
		
		// SkyBounceObject
		for(int i = 0; i < GameData.Throw.nSkyBounceObjectCount; i++) {
			obj =  MonoBehaviour.Instantiate(pfSkyBounceObject, pos, Quaternion.identity) as Object_Throw;
			obj.transform.parent = obObjects.transform;
			obj.obSound = obSound;
			
			listObject.Add(obj);
		}
		
		// SkyBlockObject
		for(int i = 0; i < GameData.Throw.nSkyBlockObjectCount; i++) {
			obj =  MonoBehaviour.Instantiate(pfSkyBlockObject, pos, Quaternion.identity) as Object_Throw;
			obj.transform.parent = obObjects.transform;
			obj.obSound = obSound;
			
			listObject.Add(obj);
		}
	}
	
	void StartRound() {
		GameData.nStatus = GameData.Status.PLAY;
		GameData.Throw.nPlayStatus = GameData.Throw.PlayStatus.ROUND;
		InitSpinPowerGauge();
		InitSpinSpeedGauge(true);
		InitSpinAngleGauge();
		
		SetEvent("Round");
	}
	
	public void EndShowUIRound() {
		StartSpin();	
	}
	
	void EndRound() {
		GameData.Throw.nRound++;
		if(GameData.Throw.nRound > GameData.Throw.nMaxRound) {
			EndGame();	
			return;
		}
		
		StartRound();
	}
	
	void StartSpin() {
		GameData.Throw.nPlayStatus = GameData.Throw.PlayStatus.SPIN;
		
		SetEvent("Spin");
		
		// for Test
		if(bTest) {
			GameData.Throw.nSpeed = nTest_Speed;
			GameData.Throw.nAngle = nTest_Angle;
			StartThrow();
		}
	}

	void ActionOnSpinStartSpeedGauge() {
		GameData.Throw.bControlSpeedGaugeActive = true;
	}
	
	void ActionOnSpinCheckSpeedGauge() {
		GameData.Throw.bControlSpeedGaugeActive = false;
		CheckSpinSpeedGauge();
	}
	
	void EndSpin() {
		StartAngle();
	}

	void StartAngle() {
		GameData.Throw.nPlayStatus = GameData.Throw.PlayStatus.ANGLE;
		
		SetEvent("Angle");
	}	
	
	void EndAngle() {
		GameData.Throw.nAngle = GameData.Throw.nControlAngleGauge;
		
		StartThrow();
	}
	
	void StartThrow() {
		GameData.Throw.nPlayStatus = GameData.Throw.PlayStatus.THROW;
		SetWindSpeed();
		
		SetEvent("Throw");
	}
	
	void EndThrow() {
		StartLanding();
	}
	
	void StartLanding() {
		GameData.Throw.nPlayStatus = GameData.Throw.PlayStatus.LANDING;
		
		bFinishLanding = false;
		bFinishUpdateRecord = false;
		
		SetEvent("Landing");
	}
	
	void EndLanding() {
		bFinishLanding = true;
		if(bFinishUpdateRecord) {
			Finish();	
		}
	}
	
	public void OnResponseUpdateRecord(bool bSuccess) {
		if(bSuccess) {
			bFinishUpdateRecord = true;
			if(bFinishLanding) {
				Finish();	
			}
		}
		else {
			Debug.Log("UpdateRecord Failure : " + GlobalValues.errorMessageConnector);
		}
	}
	
	void Finish() {
		GameData.Throw.nPlayStatus = GameData.Throw.PlayStatus.FINISH;
		SetEvent("Finish");
	}
	
	void SetEvent(string strMsg) {
		// Update ShowUI First
		obUI.SendMessage("ShowUI");
		
		strMsg = "On" + strMsg;
		
		obConnector.SendMessage(strMsg, null, SendMessageOptions.DontRequireReceiver);
		obBackground.SendMessage(strMsg, null, SendMessageOptions.DontRequireReceiver);
		obPlayer.SendMessage(strMsg, null, SendMessageOptions.DontRequireReceiver);
		obThrowingObject.SendMessage(strMsg, null, SendMessageOptions.DontRequireReceiver);
		obUI.SendMessage(strMsg, null, SendMessageOptions.DontRequireReceiver);
		obSound.SendMessage(strMsg, null, SendMessageOptions.DontRequireReceiver);
		
		SetEventToObjects(strMsg);
	}

	void SetEventToObjects(string strMsg) {
		Object_Throw obj; 
		for (int i =0; i<listObject.Count; i++) {
			obj = (Object_Throw)listObject[i];
			obj.gameObject.SendMessage(strMsg, null, SendMessageOptions.DontRequireReceiver);
		}
	}
	
	public void OnControllerTouchLeft() {
		switch(GameData.Throw.nPlayStatus) {
		case GameData.Throw.PlayStatus.SPIN : ActionOnSpinStartSpeedGauge(); break;
		case GameData.Throw.PlayStatus.FINISH : EndRound(); break;
		case GameData.Throw.PlayStatus.FOUL : EndRound(); break;
		}
	}
	
	public void OnControllerTouchRight() {
		switch(GameData.Throw.nPlayStatus) {
		case GameData.Throw.PlayStatus.SPIN : EndSpin(); break;
		}
	}
	
	public void OnControllerReleaseLeft() {
		switch(GameData.Throw.nPlayStatus) {
		case GameData.Throw.PlayStatus.SPIN : ActionOnSpinCheckSpeedGauge(); break;
		}
	}
	
	public void OnControllerReleaseRight() {
		switch(GameData.Throw.nPlayStatus) {
		case GameData.Throw.PlayStatus.ANGLE : EndAngle(); break;
		}
	}
	
	void UpdateThrowing() {
		if(	GameData.Throw.nPlayStatus != GameData.Throw.PlayStatus.THROW) {
			return;
		}
		
		obThrowingObject.SendMessage("UpdateThrowing");
		obBackground.SendMessage("UpdateThrowing");
		SetEvent("UpdateThrowingPos");
		SetEventToObjects("UpdateThrowingPos");
		obController.SendMessage("CheckInput");
		
		if(GameData.Throw.nThrowingHeight <= GameData.Throw.nLandingHeight) {
			obThrowingObject.SendMessage("Landing");
			
			EndThrow();
			return;
		}
	}
	
	void InitSpinPowerGauge() {
		GameData.Throw.nPower = GameData.Throw.nMaxPower;
	}		
	
	void InitSpinSpeedGauge(bool bStart) {
		if(bStart) {
			GameData.Throw.nSpeed = GameData.Throw.nMinSpeed;
			GameData.Throw.nControlSpeedGaugeMovePerSec = GameData.Throw.nControlSpeedGaugeStartMovePerSec;
		}
		
		GameData.Throw.nControlSpeedGauge = 0f;
		GameData.Throw.nControlSpeedGaugeUp = true;
		
		float nMinPos = GameData.Throw.nControlSpeedTargetSize;
		float nMaxPos = GameData.Throw.nControlSpeedGaugeSize - 2*GameData.Throw.nControlSpeedTargetSize;
		GameData.Throw.nControlSpeedTargetPos = Random.Range(nMinPos, nMaxPos);
	}
	
	bool IsControlSpeedGaugeInTargetRange(float nMinPos, float nMaxPos) {
		float nPos = GameData.Throw.nControlSpeedGauge / GameData.Throw.nControlSpeedMaxGauge * GameData.Throw.nControlSpeedGaugeSize;
		if(nPos >= nMinPos && nPos <= nMaxPos) {
			return true;
		}

		return false;
	}
	
	void CheckSpinSpeedGauge() {
		float nMinPos = obControlSpeedTarget.X();
		float nMaxPos = nMinPos + GameData.Throw.nControlSpeedTargetSize;
		if(IsControlSpeedGaugeInTargetRange(nMinPos, nMaxPos)) {
			nMinPos = (nMinPos + nMaxPos - GameData.Throw.nControlSpeedTargetPerfectSize) / 2f;
			nMaxPos = nMinPos + GameData.Throw.nControlSpeedTargetPerfectSize;
			if(IsControlSpeedGaugeInTargetRange(nMinPos, nMaxPos)) {
				SpeedUp(true);
				obUI.SendMessage("ShowControlSpeedResult", GameData.ControlResult.PERFECT);
				obSound.SendMessage("PlayControlSpeedResult", GameData.ControlResult.PERFECT);
			}
			else {
				SpeedUp(false);
				obUI.SendMessage("ShowControlSpeedResult", GameData.ControlResult.GOOD);
				obSound.SendMessage("PlayControlSpeedResult", GameData.ControlResult.GOOD);
			}
			
			GameData.Throw.nControlSpeedGaugeMovePerSec *= GameData.Throw.nControlSpeedGaugeStartMovePerSecRatio;
		}
		else {
			GameData.Throw.nControlSpeedGaugeMovePerSec /= GameData.Throw.nControlSpeedGaugeStartMovePerSecRatio;
			obUI.SendMessage("ShowControlSpeedResult", GameData.ControlResult.BAD);
			obSound.SendMessage("PlayControlSpeedResult", GameData.ControlResult.BAD);
		}
		
		InitSpinSpeedGauge(false);
	}	

	void InitSpinAngleGauge() {
		GameData.Throw.nControlAngleGauge = 0f;
	}
	
	void SpeedUp(bool bPerfect) {
		if(bPerfect) {
			GameData.Throw.nSpeed *= GameData.Throw.nSpeedUpPerfectRatio;
		}
		else {
			GameData.Throw.nSpeed *= GameData.Throw.nSpeedUpRatio;
		}
		if(GameData.Throw.nSpeed > GameData.Throw.nMaxSpeed) {
			GameData.Throw.nSpeed = GameData.Throw.nMaxSpeed;
		}
	}

	void UpdatePower() {
		if(GameData.Throw.nPlayStatus != GameData.Throw.PlayStatus.SPIN) {
			return;
		}
		
		GameData.Throw.nPower -= (GameData.Throw.nPowerMovePerSec) * Time.deltaTime;
		if(GameData.Throw.nPower <= 0) {
			GameData.Throw.nPower = 0;			
			GameData.Throw.nPlayStatus = GameData.Throw.PlayStatus.FOUL;
			
			// 파울 장면이 나와야함... 임시로 던짐.
			EndAngle();
		}
	}
	
	void UpdateControlSpeed() {
		if(GameData.Throw.nPlayStatus != GameData.Throw.PlayStatus.SPIN) {
			return;
		}
		
		if(GameData.Throw.bControlSpeedGaugeActive == false) {
			return;
		}
		
		float deltaSpeedGauge = (GameData.Throw.nControlSpeedGaugeMovePerSec) * Time.deltaTime;
		if(GameData.Throw.nControlSpeedGaugeUp) {
			GameData.Throw.nControlSpeedGauge += deltaSpeedGauge;
			if(GameData.Throw.nControlSpeedGauge >= GameData.Throw.nControlSpeedMaxGauge) {
				GameData.Throw.nControlSpeedGauge = GameData.Throw.nControlSpeedMaxGauge;
				GameData.Throw.nControlSpeedGaugeUp = false;
			}
		}
		else {
			GameData.Throw.nControlSpeedGauge -= deltaSpeedGauge;
			if(GameData.Throw.nControlSpeedGauge < 0) {
				GameData.Throw.nControlSpeedGauge = 0;
				GameData.Throw.nControlSpeedGaugeUp = true;
			}
		}			
	}
	
	void UpdateControlAngle() {
		if(GameData.Throw.nPlayStatus != GameData.Throw.PlayStatus.ANGLE) {
			return;
		}
		
		float deltaAngleGauge = (GameData.Throw.nControlAngleGaugeMovePerSec) * Time.deltaTime;
		GameData.Throw.nControlAngleGauge += deltaAngleGauge;
		if(GameData.Throw.nControlAngleGauge >= GameData.Throw.nControlAngleMaxGauge) {
			GameData.Throw.nControlAngleGauge = GameData.Throw.nControlAngleMaxGauge;
			EndAngle();
		}
	}
	
	void SetWindSpeed() {
		float nPossibility = Random.Range(0, 100f);
		if(nPossibility <= GameData.Throw.nGoodWindPossibility) {
			GameData.Throw.nWindSpeed = GameData.Throw.nGoodWindSpeed;
		}
		else {
			GameData.Throw.nWindSpeed = Random.Range(-GameData.Throw.nWindMaxSpeed, GameData.Throw.nWindMaxSpeed);
		}
		
		// for Test
		if(bTest) {
			GameData.Throw.nWindSpeed = nTest_WindSpeed;
		}
	}
}
