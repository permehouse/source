using UnityEngine;
using System.Collections;

public class Controller_Throw : MonoBehaviour {
	public GameObject obGamePlay;
	
	public Camera obCamera = null;
 	public tk2dSprite obPauseButton = null;
	Vector2 posPauseButton;
	Vector2 sizeHalfPauseButton;

	bool bTouchLeft;
	bool bReleaseLeft;
	bool bTouchRight;
	bool bReleaseRight;
	Vector2 posInput;
	
	void Start() {
		posPauseButton = new Vector2(obPauseButton.gameObject.transform.position.x, obPauseButton.gameObject.transform.position.y);
		sizeHalfPauseButton = new Vector2(obPauseButton.GetBounds().size.x/2, obPauseButton.GetBounds().size.y/2);
	}
	
	// Update is called once per frame
	void Update() {
		if(GameData.nStatus != GameData.Status.PLAY && GameData.nStatus != GameData.Status.END) {
			return;
		}
		
		CheckInput();
	}
	
	public void CheckInput() {
		bTouchLeft = false;
		bReleaseLeft = false;
		bTouchRight = false;
		bReleaseRight = false;
		posInput = new Vector2(0f, 0f);

		if(Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {
			CheckTouchInput();
		}
		else {
			CheckKeyInput();
			CheckMouseInput();
		}
		
		ProcessEvent();
	}
	
	public virtual void CheckKeyInput() {
		if(Input.GetKeyDown(KeyCode.LeftArrow)) {
			bTouchLeft = true;
		}
		else if(Input.GetKeyUp(KeyCode.LeftArrow)) {
			bReleaseLeft = true;
		}
		else if(Input.GetKeyDown(KeyCode.RightArrow)) {
			bTouchRight = true;
		}
		else if(Input.GetKeyUp(KeyCode.RightArrow)) {
			bReleaseRight = true;
		}
	}
	
	void CheckMouseInput() {
		Vector3 posMouse = obCamera.ScreenToWorldPoint(Input.mousePosition);
		posInput = new Vector2(posMouse.x, posMouse.y);
		
		if (Input.GetMouseButtonDown(0)) {
			bTouchLeft = true;
		}
		else if(Input.GetMouseButtonUp(0)) {
			bReleaseLeft = true;
		}
		else if(Input.GetMouseButtonDown(1)) {
			bTouchRight = true;
		}
		else if(Input.GetMouseButtonUp(1)) {
			bReleaseRight = true;
		}
	}
	
	public virtual void CheckTouchInput() {
		int nCount = Input.touchCount;
		for(int i = 0; i < nCount; i++) {
			posInput = Input.GetTouch(i).position;
			TouchPhase phaseTouch = Input.GetTouch(i).phase;
			
			if(phaseTouch == TouchPhase.Began) { // || phaseTouch == TouchPhase.Moved) {
				if(posInput.x < GameData.nScreenWidth/2) {
					bTouchLeft = true;
					return;
				}
				else {
					bTouchRight = true;	
					return;
				}
			}
			else if(phaseTouch == TouchPhase.Ended || phaseTouch == TouchPhase.Canceled) {
				if(posInput.x < GameData.nScreenWidth/2) {
					bReleaseLeft = true;
					return;
				}
				else {
					bReleaseRight = true;
					return;
				}
			}
		}
	}
	
	public void ProcessEvent() {
		if(CheckPosInMenuButton()) {
			return;
		}
		
		if(bTouchLeft) {
			obGamePlay.SendMessage("OnControllerTouchLeft");			
		}
		else if(bReleaseLeft)	{
			obGamePlay.SendMessage("OnControllerReleaseLeft");			
		}
		else if(bTouchRight) {
			obGamePlay.SendMessage("OnControllerTouchRight");			
		}
		else if(bReleaseRight)	{
			obGamePlay.SendMessage("OnControllerReleaseRight");			
		}
	}
	
	bool CheckPosInMenuButton() {
		if(posInput.x >= posPauseButton.x-sizeHalfPauseButton.x && posInput.x <= posPauseButton.x+sizeHalfPauseButton.x
			&& posInput.y >= posPauseButton.y-sizeHalfPauseButton.y && posInput.y <= posPauseButton.y+sizeHalfPauseButton.y) {
			
			return true;
		}
		
		return false;
	}
}
