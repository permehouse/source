using UnityEngine;
using System.Collections;

public class Sound_Throw : MonoBehaviour {
	// Sound
	public AudioSource audioBGM;
	
	public AudioClip sndSpin;
	public AudioClip sndThrow;	
	public AudioClip sndGoodWind;
	public AudioClip sndLanding;
	public AudioClip sndResultGood;
	public AudioClip sndResultBad;
	public AudioClip sndResultPerfect;
	public AudioClip sndGroundBounceObject;
	public AudioClip sndGroundHiddenBounceObject;
	public AudioClip sndSkyBounceObject;
	public AudioClip sndSkyBlockObject;
	
	void SoundBGM() {
		if(GameData.Settings.bSoundOn == false) {
			return;
		}
		
		audioBGM.Play();
	}	
	
	public void SoundEffect(AudioClip audioClip) {
		if(GameData.Settings.bSoundOn == false) {
			return;
		}
		
		AudioSource.PlayClipAtPoint(audioClip, transform.localPosition);
	}
	
	public void OnRound() {
		SoundBGM();
	}	
	public void OnSpin() {
		SoundEffect(sndSpin);
	}	
	public void OnAngle() {
	}	
	public void OnThrow() {
		SoundEffect(sndThrow);
	}		
	public void OnLanding() {
		SoundEffect(sndLanding);
	}		
	
	public void PlayControlSpeedResult(GameData.ControlResult nResult) {
		switch(nResult) {
		case GameData.ControlResult.PERFECT : SoundEffect(sndResultPerfect); break;
		case GameData.ControlResult.GOOD : SoundEffect(sndResultGood); break;
		case GameData.ControlResult.BAD : SoundEffect(sndResultBad); break;
		}
	}
	
	public void PlayObjectCollision(Object_Throw.Type nType) {
		switch(nType) {
		case Object_Throw.Type.ground_bounce : SoundEffect(sndGroundBounceObject); break;
		case Object_Throw.Type.ground_hidden_bounce : SoundEffect(sndGroundHiddenBounceObject); break;
		case Object_Throw.Type.sky_bounce : SoundEffect(sndSkyBounceObject); break;
		case Object_Throw.Type.sky_block : SoundEffect(sndSkyBlockObject); break;
		}
	}
}
