using UnityEngine;
using System.Collections;

public class Boxing : MonoBehaviour {
	
	public UISprite RightHand;	
	
	public UISprite RightHand1;
	public UISprite RightHand2;
	public UISprite RightHand3;
	
	public UISprite LeftHand1;
	public UISprite LeftHand2;
	public UISprite LeftHand3;
	
	
	public UISprite LeftGirl1;
	public UISprite LeftGirl2;
	public UISprite LeftGirl3;
	public UISprite RightGirl1;
	public UISprite RightGirl2;
	public UISprite RightGirl3;
	
	
	int RChoosen;
	int LChoosen;
	
	int countAll=0;
	int countOne=0;
	
	int delayStart = 80;
	int delayAction = 20;
	int delayEnemy = 70;
	
	int enemyCount = 0;
	
	// Use this for initialization
	void Start () {
	
		SetChoosenLeft(2);
		SetChoosenRight(2);
		
		RChoosen = 2;
		LChoosen = 2;
		
		Draw();
		
		
		
	}
	
	void enemyUpdate()
	{
		enemyCount++;
		
		if (enemyCount>delayEnemy)
		{
				
			switch (RChoosen)
			{
				case 0:
					SetChoosenLeft(1);
					//SetLeftGirl(1);
					break;
				case 1:
					SetChoosenLeft(2);
					//SetLeftGirl(2);
					break;
				case 2:
					SetChoosenLeft(0);
					//SetLeftGirl(0);
					break;
				
				
			}
				
			
			enemyCount -= 50;	
		}
		
	}
	
	
	
	// Update is called once per frame
	void Update () {
		
		return;
		
		enemyUpdate();
		//RightHand.gameObject.SetActive(false);
	
		countAll++;
		
		if (countAll > delayStart)
		{
			switch (RChoosen)
			{
				case 0:	
				
					//Debug.Log ("choose"+RChoosen);
					//가위.
					if (LChoosen == 2)
						Win ();
					else if (LChoosen == 1)
						Lose ();				
					countOne = delayAction;
					break;
				
				case 1:	//바위.
				
					if (LChoosen == 0)
						Win ();
					else if (LChoosen == 2)
						Lose ();				
					countOne = delayAction;
					break;
				
				case 2:	//보.
				
					if (LChoosen == 1)
						Win ();
					else if (LChoosen == 0)
						Lose ();				
					countOne = delayAction;
					break;
				
			}
			
			countAll-=delayStart;			
		}
		
			if (countOne >0)
			{
				countOne --;	
				
				if (countOne <=0)
				{
					Draw();	
				}
			}
		
		
	}
	
	public void btnOne()
	{
		
		SetChoosenRight(0);
		

		
		
	}
	
	public void btnTwo()
	{
		SetChoosenRight(1);
	}
	
	public void btnThree()
	{
		SetChoosenRight(2);
		
	}
	
	
	void SetChoosenLeft(int choosen)
	{
		
		Debug.Log ("L choosen"+choosen);
		
		LChoosen = choosen; 
		switch(choosen)
		{
			
			case 0:
				LeftHand1.gameObject.SetActive(true);
				LeftHand2.gameObject.SetActive(false);
				LeftHand3.gameObject.SetActive(false);
				break;			
			case 1:
				LeftHand1.gameObject.SetActive(false);
				LeftHand2.gameObject.SetActive(true);
				LeftHand3.gameObject.SetActive(false);
				break;			
			case 2:
				LeftHand1.gameObject.SetActive(false);
				LeftHand2.gameObject.SetActive(false);
				LeftHand3.gameObject.SetActive(true);
				break;			
		}		
	}
	
	void SetChoosenRight(int choosen)
	{
		
		RChoosen = choosen; 
		switch(choosen)
		{
			case 0:
				RightHand1.gameObject.SetActive(true);
				RightHand2.gameObject.SetActive(false);
				RightHand3.gameObject.SetActive(false);
				break;			
			case 1:
				RightHand1.gameObject.SetActive(false);
				RightHand2.gameObject.SetActive(true);
				RightHand3.gameObject.SetActive(false);
				break;			
			case 2:
				RightHand1.gameObject.SetActive(false);
				RightHand2.gameObject.SetActive(false);
				RightHand3.gameObject.SetActive(true);
				break;			
		}		
	}
	
	
	
	
	void SetLeftGirl(int choosen)
	{
		LChoosen = choosen;
		switch(choosen)
		{
			case 0:
				LeftGirl1.gameObject.SetActive(true);
				LeftGirl2.gameObject.SetActive(false);
				LeftGirl3.gameObject.SetActive(false);
				break;			
			case 1:
				LeftGirl1.gameObject.SetActive(false);
				LeftGirl2.gameObject.SetActive(true);
				LeftGirl3.gameObject.SetActive(false);
				break;			
			case 2:
				LeftGirl1.gameObject.SetActive(false);
				LeftGirl2.gameObject.SetActive(false);
				LeftGirl3.gameObject.SetActive(true);
				break;			
		}		
	}
	
	void SetRightGirl(int choosen)
	{
		
		switch(choosen)
		{
			case 0:
				RightGirl1.gameObject.SetActive(true);
				RightGirl2.gameObject.SetActive(false);
				RightGirl3.gameObject.SetActive(false);
				break;			
			case 1:
				RightGirl1.gameObject.SetActive(false);
				RightGirl2.gameObject.SetActive(true);
				RightGirl3.gameObject.SetActive(false);
				break;			
			case 2:
				RightGirl1.gameObject.SetActive(false);
				RightGirl2.gameObject.SetActive(false);
				RightGirl3.gameObject.SetActive(true);
				break;			
		}		
	}
	
	
	void Draw()
	{
		Debug.Log ("draw");
		SetLeftGirl(0);
		SetRightGirl(0);	
	}
	
	void Win()
	{
		Debug.Log ("win");
		SetRightGirl(1);
		SetLeftGirl(2);
		
	}
	
	void Lose()
	{
		Debug.Log ("lose");
		SetRightGirl(2);
		SetLeftGirl(1);
		
	}
	
}
