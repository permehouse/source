using UnityEngine;
using System.Collections;

public class Sound_Archery : MonoBehaviour {
	// Sound
	public AudioSource audioBGM;
	
	public AudioClip sndDraw;
	public AudioClip sndShoot;		
	public AudioClip sndMark;
	
	void SoundBGM() {
		if(GameData.Settings.bSoundOn == false) {
			return;
		}
		
		audioBGM.Play();
	}	
	
	public void SoundEffect(AudioClip audioClip) {
		if(GameData.Settings.bSoundOn == false) {
			return;
		}
		
		AudioSource.PlayClipAtPoint(audioClip, transform.localPosition);
	}
	
	public void OnStartGame() {
		SoundBGM();
	}
	
	public void OnDraw() {
		SoundEffect(sndDraw);
	}		
	public void OnFriendDraw() {
		OnDraw();
	}
	public void OnShoot() {
		SoundEffect(sndShoot);
	}		
	public void OnFriendShoot() {
		OnShoot();
	}
	public void OnMark() {
		SoundEffect(sndMark);
	}		
	public void OnFriendMark() {
		OnMark();
	}
}
