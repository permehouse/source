using UnityEngine;
using System.Collections;
                     
public class Connector_Archery : ServerConnector {
	public GameObject obGamePlay;
	
	public void OnStartGame() {
		if(GlobalValues.sessionId != "") {		// alrady loginned
			StartGame();
		}
		else {													// login first
			CallbackResult cbReponse = delegate(bool bSuccess) {
				if(bSuccess) {
					StartGame();
				}
				else {
					Debug.Log("Error : Stop game because there's no login user!!");
				}
			};
			// for test
			string strID = "test";
			string strPW = "TESTPASSWORD";
			
			Login(strID, strPW, cbReponse);
		}
	}
	
	void StartGame() {
		CallbackResult cbReponse = delegate(bool bSuccess) {
			obGamePlay.SendMessage("OnResponseStartGame", bSuccess);
		};
	
		GameStart_Throw(cbReponse);
	}
}
