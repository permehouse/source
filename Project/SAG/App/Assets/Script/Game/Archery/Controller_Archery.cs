using UnityEngine;
using System.Collections;

public class Controller_Archery : MonoBehaviour {
	public GameObject obGamePlay;
	
	public Camera obCamera = null;
 	public tk2dSprite obPauseButton = null;
	Vector2 posPauseButton;
	Vector2 sizeHalfPauseButton;

	protected bool bTouch;
	protected bool bRelease;
	protected bool bDrag;
	Vector2 posInput;
	Vector2 posInputBefore;
	Vector2 sizeDrag;
	
	protected bool bMoueButtonDown;
	
	void Start() {
		posPauseButton = new Vector2(obPauseButton.gameObject.transform.position.x, obPauseButton.gameObject.transform.position.y);
		sizeHalfPauseButton = new Vector2(obPauseButton.GetBounds().size.x/2, obPauseButton.GetBounds().size.y/2);
		
		bMoueButtonDown = false;
	}
	
	// Update is called once per frame
	void Update() {
		if(GameData.nStatus != GameData.Status.PLAY && GameData.nStatus != GameData.Status.END) {
			return;
		}
		
		CheckInput();
	}
	
	public void CheckInput() {
		bTouch = false;
		bRelease = false;
		posInput = new Vector2(0f, 0f);

		if(Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {
			CheckTouchInput();
		}
		else {
			CheckMouseInput();
		}
		
		ProcessEvent();
	}
	
	void CheckMouseInput() {
		Vector3 posMouse = obCamera.ScreenToWorldPoint(Input.mousePosition);
		posInput = new Vector2(posMouse.x, posMouse.y);
		
		if (Input.GetMouseButtonDown(0)) {
			bTouch = true;
			bMoueButtonDown = true;
		}
		else if(Input.GetMouseButtonUp(0)) {
			bRelease = true;
			bMoueButtonDown = false;
		}
		else if(bMoueButtonDown) {
			bDrag = true;
		}
	}
	
	public virtual void CheckTouchInput() {
		int nCount = Input.touchCount;
		for(int i = 0; i < nCount; i++) {
			posInput = Input.GetTouch(i).position;
			TouchPhase phaseTouch = Input.GetTouch(i).phase;
			
			if(phaseTouch == TouchPhase.Began) {
				bTouch = true;
			}
			else if(phaseTouch == TouchPhase.Ended || phaseTouch == TouchPhase.Canceled) {
				bRelease = true;
			}
			else if(phaseTouch == TouchPhase.Moved) {
				bDrag = true;
			}
		}
	}
	
	public void ProcessEvent() {
		if(CheckPosInMenuButton()) {
			return;
		}
		
		if(bTouch) {
			posInputBefore = posInput;			
			obGamePlay.SendMessage("OnControllerTouch");
		}
		else if(bRelease)	{
			obGamePlay.SendMessage("OnControllerRelease");			
		}
		else if(bDrag) {
			sizeDrag = new Vector2(posInput.x - posInputBefore.x, posInput.y - posInputBefore.y);
			posInputBefore = posInput;
			obGamePlay.SendMessage("OnControllerDrag", sizeDrag);			
		}
	}
	
	bool CheckPosInMenuButton() {
		if(posInput.x >= posPauseButton.x-sizeHalfPauseButton.x && posInput.x <= posPauseButton.x+sizeHalfPauseButton.x
			&& posInput.y >= posPauseButton.y-sizeHalfPauseButton.y && posInput.y <= posPauseButton.y+sizeHalfPauseButton.y) {
			
			return true;
		}
		
		return false;
	}
}
