using UnityEngine;
using System.Collections;

public class GamePlay_Archery : MonoBehaviour {
	public GameObject obPlayingField;
	public GameObject obPlayer;
	public GameObject obPlayerTarget;
	public GameObject obFriend;
	public GameObject obFriendTarget;
	public GameObject obArrow;
	public GameObject obFocus;
	public GameObject obUI;
	public GameObject obSound;
	public Controller_Archery obController;
	public Connector_Archery obConnector;
	
	// for Test
	public bool bTest = false;
	public GameData.Character nTestPlayerCharacter = GameData.Character.NONE;
	public GameData.Character nTestFriendCharacter = GameData.Character.NONE;
	
	public void Start() {
		StartGame();
	}
	
	void StartGame() {
		GameData.nStatus = GameData.Status.START;

		GameData.Archery.nScore = 0;
		GameData.Archery.nPlayerScore = 0;
		GameData.Archery.nFriendScore = 0;
		for(int i = 0; i < GameData.Archery.nMaxRound; i++) {
			GameData.Archery.listPlayerRoundScore[i] = 0;
			GameData.Archery.listFriendRoundScore[i] = 0;
		}
		
		GameData.Archery.nRound = 1;
		
		// for Test
		if(bTest) {
			GameData.nPlayerCharacter = nTestPlayerCharacter;
			GameData.nFriendCharacter = nTestFriendCharacter;
		}
		
		SetEvent("StartGame");
		
		StartRound();
	}
	
	public void OnResponseStartGame(bool bSuccess) {
		if(bSuccess) {
			GenerateFriendRoundScores();
			StartRound();
		}
		else {
			Debug.Log("ResponseStartGame Failure : " + GlobalValues.errorMessageConnector);
		}
	}
	
	void GenerateFriendRoundScores() {
		// for test
		GameData.Archery.nFriendScore = Random.Range(31, 43);
		
		Debug.Log("===== Friend's Total Score : " + GameData.Archery.nFriendScore);
		if(GameData.Archery.nFriendScore > GameData.Archery.nPerfectScore * GameData.Archery.nMaxRound) {
			GameData.Archery.nFriendScore = GameData.Archery.nPerfectScore * GameData.Archery.nMaxRound;
		}
		else if(GameData.Archery.nFriendScore < 0) {
			GameData.Archery.nFriendScore = 0;
		}
		
		int nScore = 0;
		while(GameData.Archery.nFriendScore > 0) {
			// set reverse order to make higher scores to the last round.
			for(int i = GameData.Archery.nMaxRound - 1; i >=0 && GameData.Archery.nFriendScore > 0; i--) {
				if(GameData.Archery.listFriendRoundScore[i] == GameData.Archery.nPerfectScore) {
					continue;
				}
				
				nScore = Random.Range(1, GameData.Archery.nPerfectScore - GameData.Archery.listFriendRoundScore[i] + 1);
				if(nScore > GameData.Archery.nFriendScore) {
					nScore = GameData.Archery.nFriendScore;
				}
				
				GameData.Archery.listFriendRoundScore[i] += nScore;
				GameData.Archery.nFriendScore -= nScore;
			}
		}
		
		for(int i = 0; i < GameData.Archery.nMaxRound; i++) {
			Debug.Log("===== Friend's Round Score(" + (i+1) + ") : " + GameData.Archery.listFriendRoundScore[i]);
		}
	}
	
	void EndGame() {
		GameData.nStatus = GameData.Status.END;
		SetEvent("EndGame");
	}
	
	void QuitGame() {
		GameData.nStatus = GameData.Status.QUIT;
		SetEvent("QuitGame");
		
		Application.LoadLevel("Main");
	}
	
	public void Update() {
		if(GameData.nStatus != GameData.Status.PLAY) {
			return;
		}
		
		UpdateAimTime();
	}

	void StartRound() {
		GameData.nStatus = GameData.Status.PLAY;
		GameData.Archery.nPlayStatus = GameData.Archery.PlayStatus.ROUND;
		
		GameData.Archery.nAimTime = GameData.Archery.nAimMaxTime;
		
		SetEvent("Round");
	}
	
	public void EndShowUIRound() {
		Ready();
	}
	
	void EndRound() {
		GameData.Archery.nRound++;
		if(GameData.Archery.nRound > GameData.Archery.nMaxRound) {
			EndGame();
			return;
		}
		
		StartRound();
	}
	
	void Ready() {
		GameData.Archery.nPlayStatus = GameData.Archery.PlayStatus.READY;
		
		SetEvent("Ready");
	}
	
	void EndReady() {
		StartDraw();
	}
	
	void StartDraw() {
		GameData.Archery.nPlayStatus = GameData.Archery.PlayStatus.DRAW;
		SetEvent("Draw");
	}
	
	void ActionOnDrawZoomIn() {
		SetEvent("ZoomIn");
	}
	
	void ActionOnDrawZoomOut() {
		SetEvent("ZoomOut");
	}
	
	void EndDraw() {
		StartAim();
	}
	
	void StartAim() {
		GameData.Archery.nPlayStatus = GameData.Archery.PlayStatus.AIM;
		
		InitAimDir();
		GameData.Archery.bAimTime = true;
		GameData.Archery.nAimTime = GameData.Archery.nAimMaxTime;
		
		SetEvent("Aim");
	}
	
	void ActionOnAimFinish() {
		GameData.Archery.bAimTime = false;
		
		SetEvent("ZoomOut");
	}
	
	void EndAim() {
		StartShoot();
	}
	
	void StartShoot() {
		GameData.Archery.nPlayStatus = GameData.Archery.PlayStatus.SHOOT;
		
		SetEvent("Shoot");
	}
	
	public void EndShoot() {
		StartMark();
	}
	
	void StartMark() {
		GameData.Archery.nPlayStatus = GameData.Archery.PlayStatus.MARK;
		
		CalcPlayerScore();
		GameData.Archery.nPlayerScore += GameData.Archery.nScore;
		GameData.Archery.listPlayerRoundScore[GameData.Archery.nRound - 1] = GameData.Archery.nScore;
		
		SetEvent("Mark");
	}
	
	void EndMark() {
		StartFinish();
	}
	
	void StartFinish() {
		GameData.Archery.nPlayStatus = GameData.Archery.PlayStatus.FINISH;
		
		SetEvent ("Finish");
	}
	
	void EndFinish() {
		FriendReady();
	}

	void FriendReady() {
		GameData.Archery.nPlayStatus = GameData.Archery.PlayStatus.FRIEND_READY;
		
		GameData.Archery.nAimTime = GameData.Archery.nAimMaxTime;
		
		SetEvent("FriendReady");
	}
	
	void EndFriendReady() {
		StartFriendDraw();
	}

	void StartFriendDraw() {
		GameData.Archery.nPlayStatus = GameData.Archery.PlayStatus.FRIEND_DRAW;
		
		SetEvent("FriendDraw");
	}

	void EndFriendDraw() {
		StartFriendAim();	
	}
	
	void StartFriendAim() {
		GameData.Archery.nPlayStatus = GameData.Archery.PlayStatus.FRIEND_AIM;
		
		GameData.Archery.bAimTime = true;
		SetFriendAimEndTime();
		
		SetEvent("FriendAim");
	}
	
	void EndFriendAim() {
		GameData.Archery.bAimTime = false;
		StartFriendShoot();	
	}
	
	void StartFriendShoot() {
		GameData.Archery.nPlayStatus = GameData.Archery.PlayStatus.FRIEND_SHOOT;
		SetEvent("FriendShoot");
	}
	
	void EndFriendShoot() {
		StartFriendMark();		
	}
	
	void StartFriendMark() {
		GameData.Archery.nPlayStatus = GameData.Archery.PlayStatus.FRIEND_MARK;
		
		CalcFriendScore();
		GameData.Archery.nFriendScore += GameData.Archery.nScore;
		
		SetEvent("FriendMark");
	}

	void EndFriendMark() {
		StartFriendFinish();		
	}
	
	void StartFriendFinish() {
		GameData.Archery.nPlayStatus = GameData.Archery.PlayStatus.FRIEND_FINISH;
		SetEvent ("FriendFinish");
	}
	
	void EndFriendFinish() {
		EndRound();	
	}

	void SetEvent(string strMsg) {
		// Update ShowUI First
		obUI.SendMessage("ShowUI");
		
		strMsg = "On" + strMsg;
		
		obConnector.SendMessage(strMsg, null, SendMessageOptions.DontRequireReceiver);
		obPlayingField.SendMessage(strMsg, null, SendMessageOptions.DontRequireReceiver);
		obPlayingField.SendMessage(strMsg, null, SendMessageOptions.DontRequireReceiver);
		obPlayer.SendMessage(strMsg, null, SendMessageOptions.DontRequireReceiver);
		obPlayerTarget.SendMessage(strMsg, null, SendMessageOptions.DontRequireReceiver);
		obFriend.SendMessage(strMsg, null, SendMessageOptions.DontRequireReceiver);
		obFriendTarget.SendMessage(strMsg, null, SendMessageOptions.DontRequireReceiver);		obArrow.SendMessage(strMsg, null, SendMessageOptions.DontRequireReceiver);
		obFocus.SendMessage(strMsg, null, SendMessageOptions.DontRequireReceiver);
		obUI.SendMessage(strMsg, null, SendMessageOptions.DontRequireReceiver);
		obSound.SendMessage(strMsg, null, SendMessageOptions.DontRequireReceiver);
	}
	
	public void OnControllerTouch() {
		switch(GameData.nStatus) {
			case GameData.Status.PLAY : {
				switch(GameData.Archery.nPlayStatus) {
					case GameData.Archery.PlayStatus.DRAW : ActionOnDrawZoomIn(); break;
					case GameData.Archery.PlayStatus.MARK : EndMark(); break;
					case GameData.Archery.PlayStatus.FRIEND_MARK : EndFriendMark(); break;
				}
			}
			break;
			case GameData.Status.END : {
				QuitGame();			
			}
			break;
		}
	}
	
	public void OnControllerRelease() {
		switch(GameData.Archery.nPlayStatus) {
		case GameData.Archery.PlayStatus.DRAW : ActionOnDrawZoomOut(); break;
		case GameData.Archery.PlayStatus.AIM : ActionOnAimFinish(); break;
		}		
	}
	
	public void OnControllerDrag(Vector2 sizeDrag) {
		switch(GameData.Archery.nPlayStatus) {
		case GameData.Archery.PlayStatus.AIM : UpdateAimControlPos(sizeDrag); break;
		}		
	}

	public void OnEndZoomIn() {
		switch(GameData.Archery.nPlayStatus) {
		case GameData.Archery.PlayStatus.DRAW : EndDraw(); break;
		}		
	}
	
	public void OnEndZoomOut() {
		switch(GameData.Archery.nPlayStatus) {
		case GameData.Archery.PlayStatus.AIM : EndAim(); break;
		case GameData.Archery.PlayStatus.FINISH : EndFinish(); break;
		case GameData.Archery.PlayStatus.FRIEND_FINISH : EndFriendFinish(); break;
		}		
	}
	
	public void OnEndShoot() {
		switch(GameData.Archery.nPlayStatus) {
		case GameData.Archery.PlayStatus.SHOOT : EndShoot(); break;
		case GameData.Archery.PlayStatus.FRIEND_SHOOT : EndFriendShoot(); break;
		}		
	}
	
	public void OnEndMoveToPlayer() {
		switch(GameData.Archery.nPlayStatus) {
		case GameData.Archery.PlayStatus.READY : EndReady(); break;
		}		
	}
	
	public void OnEndMoveToFriend() {
		switch(GameData.Archery.nPlayStatus) {
		case GameData.Archery.PlayStatus.FRIEND_READY : EndFriendReady(); break;
		}		
	}
	
	void InitAimDir() {
		// Set Wind Dir
		float nX = Random.Range(-GameData.Archery.nMaxDir, GameData.Archery.nMaxDir);
		float nY = Random.Range(-GameData.Archery.nMaxDir, GameData.Archery.nMaxDir);
		// Normalize Wind Vector
		float nR = Mathf.Sqrt(nX*nX + nY*nY);
		GameData.Archery.nWindDirX = nX/nR;
		GameData.Archery.nWindDirY = nY/nR;
		
		// Set Wind Speed (= Scalar)
		GameData.Archery.nWindSpeed = Random.Range(GameData.Archery.nWindMinSpeed, GameData.Archery.nWindMaxSpeed);
		
		// Set Initial Pos
		GameData.Archery.nAimPosX = 0f;
		GameData.Archery.nAimPosY = 0f;
		
		// Set Control Dir
		GameData.Archery.nAimControlDirX = 0f;
		GameData.Archery.nAimControlDirY = 0f;
	}
	
	void UpdateAimControlPos(Vector2 sizeDrag) {
		GameData.Archery.nAimPosX += sizeDrag.x;
		GameData.Archery.nAimPosY += sizeDrag.y;
	}		

	void SetFriendAimEndTime() {
		int nExpectedFriendScore = GameData.Archery.nFriendScore + GameData.Archery.listFriendRoundScore[GameData.Archery.nRound - 1];
		float nScoreDifference = Mathf.Clamp(Mathf.Abs(GameData.Archery.nPlayerScore - nExpectedFriendScore), 0f, 10f);
		float nMaxWaitTime = 	(10f - nScoreDifference)/2f;		// the more the score difference, the fater friend starts to shoot
		if(nMaxWaitTime < GameData.Archery.nAimFriendMimWaitTime) {
			nMaxWaitTime = GameData.Archery.nAimFriendMimWaitTime;
		}
		
		GameData.Archery.nAimFriendEndTime = GameData.Archery.nAimMaxTime - Random.Range(GameData.Archery.nAimFriendMimWaitTime, nMaxWaitTime);	
	}
	
	void UpdateAimTime() {
		if(GameData.Archery.bAimTime == false) {
			return;
		}
		
		if(GameData.Archery.nPlayStatus == GameData.Archery.PlayStatus.AIM) {
			GameData.Archery.nAimTime -= Time.deltaTime;
			if(GameData.Archery.nAimTime <= 0f) {
				GameData.Archery.nAimTime = 0f;
				ActionOnAimFinish();
			}
		}
		else if(GameData.Archery.nPlayStatus == GameData.Archery.PlayStatus.FRIEND_AIM) {
			GameData.Archery.nAimTime -= Time.deltaTime;
			if(GameData.Archery.nAimTime <= GameData.Archery.nAimFriendEndTime) {
				GameData.Archery.nAimTime = 0f;
				EndFriendAim();
			}			
		}
	}

	void CalcPlayerScore() {
		float nDistance = Mathf.Sqrt(GameData.Archery.nAimPosX*GameData.Archery.nAimPosX + GameData.Archery.nAimPosY*GameData.Archery.nAimPosY);
		if(nDistance <= GameData.Archery.nAimPerfectScoreDistance) {
			GameData.Archery.nScore = GameData.Archery.nPerfectScore;		// Perfect score
		}
		else if(nDistance > GameData.Archery.nAimMaxScoreDistance) {
			GameData.Archery.nScore = 0;			// out of target
		}
		else {
			float nAdjustDistance = GameData.Archery.nAimMaxScoreDistance - nDistance;
			float nMaxScore = GameData.Archery.nPerfectScore - 1;
			
			GameData.Archery.nScore = (int)Mathf.Clamp((nAdjustDistance / GameData.Archery.nAimIntervalScoreDistance) + 1f, 1f, nMaxScore);
		}
	}
	
	void CalcFriendScore() {
		GameData.Archery.nScore = GameData.Archery.listFriendRoundScore[GameData.Archery.nRound - 1];
		
		// if tie at the last round, we enforce to make friend win or lose
		if(GameData.Archery.nRound == GameData.Archery.nMaxRound) {
			// for test
//			GameData.Archery.nScore = 0;
//			GameData.Archery.nPlayerScore = GameData.Archery.nFriendScore + GameData.Archery.nScore;
				
			if(GameData.Archery.nPlayerScore == GameData.Archery.nFriendScore + GameData.Archery.nScore) {
				Debug.Log("### Tie Score : " + GameData.Archery.nScore);
				if(GameData.Archery.nScore == GameData.Archery.nPerfectScore) {		// if friend needs perfect score to tie, let player win
					GameData.Archery.nScore -= Random.Range(1, 3);		// make score 1 ~ 2 less
					Debug.Log("###### Adjust Score to win : " + GameData.Archery.nScore);
				}
				else if(GameData.Archery.nScore  == 0) {				// if friend needs 0 score to tie, let player lose
					GameData.Archery.nScore = Random.Range(1, GameData.Archery.nPerfectScore + 1);		// make score at least 1 more
					Debug.Log("###### Adjust Score to lose : " + GameData.Archery.nScore);
				}
				else {
					if(Random.Range(0, 2) == 0) {				
						GameData.Archery.nScore--;		// let player win by 1 score
						Debug.Log("###### Adjust Score to win : " + GameData.Archery.nScore);
					}
					else {
						GameData.Archery.nScore++;		// let player lose by 1 score
						Debug.Log("###### Adjust Score to lose : " + GameData.Archery.nScore);
					}
				}
			}
		}			

		// generate distane from center
		if(GameData.Archery.nScore == GameData.Archery.nPerfectScore) {
			float nDistance = Random.Range(0f, GameData.Archery.nAimPerfectScoreDistance);
			
			GameData.Archery.nAimPosX = Random.Range(-nDistance, nDistance);
			GameData.Archery.nAimPosY = Random.Range(-nDistance, nDistance);
		}
		else {
			float nInverseScore = GameData.Archery.nPerfectScore - GameData.Archery.nScore;
			float nMinDistance = (nInverseScore - 1) * GameData.Archery.nAimIntervalScoreDistance + GameData.Archery.nAimPerfectScoreDistance;
			float nMaxDistance = nInverseScore * GameData.Archery.nAimIntervalScoreDistance + GameData.Archery.nAimPerfectScoreDistance;
			float nDistance = Random.Range(nMinDistance, nMaxDistance);

			int nSingX = (Random.Range(0, 2) == 0)? -1 : 1;
			int nSingY = (Random.Range(0, 2) == 0)? -1 : 1;
			
			GameData.Archery.nAimPosX = nSingX * Random.Range(0f, nDistance);
			GameData.Archery.nAimPosY = nSingY * Mathf.Sqrt(nDistance*nDistance - GameData.Archery.nAimPosX*GameData.Archery.nAimPosX);
		}
	}
}
