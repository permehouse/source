using UnityEngine;
using System.Collections;

public class Friend_Archery : MovingObject {
	public GameObject obGamePlay;
	string strPlayer;

	public void OnStartGame() {
		strPlayer = GameData.strCharacterList[(int)GameData.nFriendCharacter];
		obAni.Play(strPlayer + "_Ready");		
		
		Pos(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y);
		
		SetScaleSpeed(GameData.Archery.nZoomPlayerSpeed);
		Scale(1f, 1f);
		SetScaleCenterPos(GameData.Archery.posZoomCenter);		
	}

	public void OnRound() {
		obAni.Play(strPlayer + "_Ready");		
	}	
	
	public void OnReady() {
		obAni.Play(strPlayer + "_Ready");		
	}	

	public void OnFriendDraw() {
		obAni.Play(strPlayer + "_Draw");
		obAni.AnimationCompleted = OnCompleteFriendDraw;	
	}	
	
	public void OnCompleteFriendDraw(tk2dSpriteAnimator ani, tk2dSpriteAnimationClip clip) {
		ani.AnimationCompleted = null;

		obGamePlay.SendMessage("EndFriendDraw");
	}

	public void OnFriendShoot() {
		obAni.Play(strPlayer + "_Shoot");
	}	
}
