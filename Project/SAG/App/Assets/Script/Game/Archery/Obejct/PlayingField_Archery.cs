using UnityEngine;
using System.Collections;

public class PlayingField_Archery : MovingObject {
	public GameObject obGamePlay;
	
	bool bMoveToPlayer = false;
	bool bMoveToFriend = false;
	
	bool bZoomIn = false;
	bool bZoomOut = false;
	
	public void OnStartGame() {
		SetSpeed(GameData.Archery.nPlayingFieldSpeed);
		
		SetScaleSpeed(GameData.Archery.nZoomBGSpeed);
		SetScaleCenterPos(GameData.Archery.posZoomCenter);
		Scale(1f, 1f);
	}	
	
	public void OnRound() {
		OnReady();
	}
	
	public void OnReady() {
		bMoveToPlayer = true;
		bMoveToFriend = false;

		MoveToPos(GameData.Archery.nPlayingFieldPlayerPosX, Y());
	}

	public void OnFriendReady() {
		bMoveToPlayer = false;
		bMoveToFriend = true;
		
		MoveToPos(-GameData.Archery.nPlayingFieldFriendPosX, Y());
	}
	
	public override void Update() {
		base.Update();
		
		CheckFinishMoving();
		CheckFinishZooming();
	}
	
	void CheckFinishMoving() {
		if(bMoveToPlayer && bMoving == false) {
			bMoveToPlayer = false;
			SetScaleCenterPos(GameData.Archery.posZoomCenter);
			obGamePlay.SendMessage("OnEndMoveToPlayer");
		}
		
		if(bMoveToFriend && bMoving == false) {
			bMoveToFriend = false;
			SetScaleCenterPos(new Vector2(GameData.Archery.posZoomCenter.x + GameData.Archery.nPlayingFieldPlayerPosX, GameData.Archery.posZoomCenter.y));
			obGamePlay.SendMessage("OnEndMoveToFriend");
		}
	}
	
	void CheckFinishZooming() {
		if(bZoomIn && bScaling == false) {
			bZoomIn = false;
			obGamePlay.SendMessage("OnEndZoomIn");
		}
		
		if(bZoomOut && bScaling == false) {
			bZoomOut = false;
			obGamePlay.SendMessage("OnEndZoomOut");
		}
	}
	
	public void OnZoomIn() {
		Zoom(GameData.Archery.nZoomScale, GameData.Archery.nZoomSpeed);		
	}
	
	public void OnZoomOut() {
		Zoom(1f, GameData.Archery.nZoomSpeed);
	}	
	
	public void OnMark() {
		Zoom(GameData.Archery.nZoomScale, GameData.Archery.nZoomFastSpeed);
	}
	
	public void OnFinish() {
		Zoom(1f, GameData.Archery.nZoomImmediateSpeed);
	}
	
	public void OnFriendMark() {
		Zoom(GameData.Archery.nZoomScale, GameData.Archery.nZoomFastSpeed);
	}
	
	public void OnFriendFinish() {
		Zoom(1f, GameData.Archery.nZoomImmediateSpeed);
	}
	
	void Zoom(float nScale, float nSpeed) {
		if(nScale > ScaleX()) {
			bZoomIn = true;
			bZoomOut = false;
		}
		else if(nScale < ScaleX()) {
			bZoomIn = false;
			bZoomOut = true;
		}
		
		SetScaleSpeed(nSpeed);
		ReScale(nScale, nScale);
	}
}
