using UnityEngine;
using System.Collections;

public class Focus_Archery : MovingObject {
	public void OnStartGame() {
		Pos (0f, 0f);
		Show(false);		
	}	

	public override void Update() {
		base.Update();
		
		if(bPaused) {
			return;
		}
		
		UpdatePos();
	}
	
	void UpdatePos() {
		if(GameData.Archery.nPlayStatus != GameData.Archery.PlayStatus.AIM) {
			return;
		}
		
		GameData.Archery.nAimPosX += (GameData.Archery.nWindDirX * GameData.Archery.nAimWindScoreDistance * (GameData.Archery.nWindSpeed/GameData.Archery.nWindMaxSpeed)) * Time.deltaTime;
		GameData.Archery.nAimPosY += (GameData.Archery.nWindDirY * GameData.Archery.nAimWindScoreDistance * (GameData.Archery.nWindSpeed/GameData.Archery.nWindMaxSpeed)) * Time.deltaTime;
	
		
		GameData.Archery.nAimPosX = Mathf.Clamp(GameData.Archery.nAimPosX, -GameData.Archery.nAimMaxTargetDistance, GameData.Archery.nAimMaxTargetDistance);
		GameData.Archery.nAimPosY = Mathf.Clamp(GameData.Archery.nAimPosY, -GameData.Archery.nAimMaxTargetDistance, GameData.Archery.nAimMaxTargetDistance);
		
		MoveToPos(GameData.Archery.nAimPosX, GameData.Archery.nAimPosY);
	}
	
	public void OnAim() {
		SetSpeed(GameData.Archery.nAimSpeed);
		Pos(GameData.Archery.nAimPosX, GameData.Archery.nAimPosY);
		Show(true);
	}

	public void OnShoot() {
		GameData.Archery.nAimPosX = X () + (GameData.Archery.nWindDirX * GameData.Archery.nAimMaxScoreDistance * (GameData.Archery.nWindSpeed/GameData.Archery.nWindMaxSpeed));
		GameData.Archery.nAimPosY = Y () + (GameData.Archery.nWindDirY * GameData.Archery.nAimMaxScoreDistance * (GameData.Archery.nWindSpeed/GameData.Archery.nWindMaxSpeed));
		
		GameData.Archery.nAimPosX = Mathf.Clamp(GameData.Archery.nAimPosX, -GameData.Archery.nAimMaxTargetDistance, GameData.Archery.nAimMaxTargetDistance);
		GameData.Archery.nAimPosY = Mathf.Clamp(GameData.Archery.nAimPosY, -GameData.Archery.nAimMaxTargetDistance, GameData.Archery.nAimMaxTargetDistance);
	}
	
	public void OnZoomOut() {
		Show(false);
	}
	
	void Show(bool bShow) {
		if(bShow) {
			gameObject.transform.localPosition = new Vector3(X(), Y(), GameData.LAYER_OBJECT_FRONT);
		}
		else {
			gameObject.transform.localPosition = new Vector3(X(), Y(), GameData.LAYER_HIDE);
		}
	}
}
