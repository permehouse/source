using UnityEngine;
using System.Collections;

public class Player_Archery : MovingObject {
	public GameObject obGamePlay;
	string strPlayer;

	public void OnStartGame() {
		strPlayer = GameData.strCharacterList[(int)GameData.nPlayerCharacter];
		obAni.Play(strPlayer + "_Ready");		
		
		Pos(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y);
		
		SetScaleSpeed(GameData.Archery.nZoomPlayerSpeed);
		Scale(1f, 1f);
		SetScaleCenterPos(GameData.Archery.posZoomCenter);		
	}

	public void OnRound() {
		obAni.Play(strPlayer + "_Ready");		
	}	
	
	public void OnReady() {
		obAni.Play(strPlayer + "_Ready");		
	}	

	public void OnDraw() {
		obAni.Play(strPlayer + "_Draw");
	}	
	
	public void OnShoot() {
		obAni.Play(strPlayer + "_Shoot");
	}	
}
