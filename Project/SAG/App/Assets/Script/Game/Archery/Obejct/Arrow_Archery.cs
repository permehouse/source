using UnityEngine;
using System.Collections;

public class Arrow_Archery : MovingObject {
	public GameObject obGamePlay;
	public GameObject obHole;
	
	public void OnStartGame() {
		Show(false, false);
	}	

	public void OnShoot() {
		Show(true, false);
		obAni.Play("FlyingArrow");
		obAni.AnimationCompleted = OnCompleteFlyingArrow;	
		Pos (GameData.Archery.nArrowFlyingPosX, GameData.Archery.nArrowFlyingPosY);
	}	
	
	public void OnCompleteFlyingArrow(tk2dSpriteAnimator ani, tk2dSpriteAnimationClip clip) {
		ani.AnimationCompleted = null;
		
		obGamePlay.SendMessage("OnEndShoot");
	}
	
	public void OnFriendShoot() {
		OnShoot();
	}
	
	public void OnMark() {
		Show(true, true);
		obAni.Play("Arrow");
		Pos (GameData.Archery.nAimPosX + GameData.Archery.nArrowImageAdjustPosX, GameData.Archery.nAimPosY + GameData.Archery.nArrowImageAdjustPosY);
	}

	public void OnFriendMark() {
		OnMark();
	}
	
	public void OnFinish() {
		Show(false, false);
	}
	
	public void OnFriendFinish() {
		OnFinish();
	}
	
	void Show(bool bShow, bool bShowHole) {
		if(bShow) {
			gameObject.transform.localPosition = new Vector3(X(), Y(), GameData.LAYER_OBJECT_FRONT);
		}
		else {
			gameObject.transform.localPosition = new Vector3(X(), Y(), GameData.LAYER_HIDE);
		}
		
		if(bShowHole) {
			obHole.SetActive(true);
		}
		else {
			obHole.SetActive(false);
		}
	}
}
