using UnityEngine;
using System.Collections;

public class UI_Archery : MonoBehaviour {
	public GameObject obGamePlay;	
	public GameObject obUIResult;
	public GameObject obUILoading;

 	public tk2dTextMesh txtUIScore;
 	public tk2dTextMesh txtUIPlayerScore;
 	public tk2dTextMesh txtUIFriendScore;
 	public tk2dSprite spPlayerScoreBoard;
 	public tk2dSprite spFriendScoreBoard;
 	public tk2dTextMesh []listUIPlayerScoreText = new tk2dTextMesh[GameData.Archery.nMaxRound];
 	public tk2dTextMesh []listUIFriendScoreText = new tk2dTextMesh[GameData.Archery.nMaxRound];

 	public UISprite spUIPlayerAimTimeGauge;
 	public UISprite spUIFriendAimTimeGauge;
	
	public GameObject obUIAim;
 	public tk2dTextMesh txtUIWindSpeed;
 	public tk2dSpriteAnimator aniUIWindArrow;

 	public tk2dSpriteAnimator aniUIRound;
 	public tk2dSpriteAnimator aniUIPlayer;
 	public tk2dSpriteAnimator aniUIFriend;
 	public tk2dTextMesh txtUIPlayerName;
 	public tk2dTextMesh txtUIFriendName;
	
	bool bPaused = false;
	
	public void Awake() {
	}
	
	public void Start() {
	}
	
	public void Update() {
		CheckGamePaused();		
		if(bPaused) {
			return;
		}
		
		UpdateAimTime();
	}
	
	void CheckGamePaused() {
		if(!bPaused && GameData.nStatus == GameData.Status.PAUSE) {
			bPaused = true;
			
 			aniUIWindArrow.Pause();
 			aniUIRound.Pause();
 			aniUIPlayer.Pause();
 			aniUIFriend.Pause();
		}
		else if(bPaused && GameData.nStatus == GameData.Status.PLAY) {
			bPaused = false;
			
 			aniUIWindArrow.Resume();
 			aniUIRound.Resume();
 			aniUIPlayer.Resume();
 			aniUIFriend.Resume();
		}
	}	
	
	void UpdateAimTime() {
		if(GameData.Archery.nPlayStatus == GameData.Archery.PlayStatus.ROUND || GameData.Archery.nPlayStatus == GameData.Archery.PlayStatus.AIM) {
			spUIPlayerAimTimeGauge.fillAmount = (GameData.Archery.nAimTime / GameData.Archery.nAimMaxTime);
		}
		else if(GameData.Archery.nPlayStatus == GameData.Archery.PlayStatus.FRIEND_READY || GameData.Archery.nPlayStatus == GameData.Archery.PlayStatus.FRIEND_AIM) {
			spUIFriendAimTimeGauge.fillAmount = (GameData.Archery.nAimTime / GameData.Archery.nAimMaxTime);
		}
	}
	
	
	public void ShowUI() {
		txtUIScore.gameObject.SetActive(false);
		if(GameData.Archery.nPlayStatus == GameData.Archery.PlayStatus.MARK || GameData.Archery.nPlayStatus == GameData.Archery.PlayStatus.FRIEND_MARK) {
			txtUIScore.gameObject.SetActive(true);
		}
		
		obUIAim.SetActive(false);
		if(GameData.Archery.nPlayStatus == GameData.Archery.PlayStatus.AIM) {
			obUIAim.SetActive(true);
		}
		
		obUIResult.SetActive(false);
		if(GameData.nStatus == GameData.Status.END) {
			obUIResult.SetActive(true);
		}
		
		obUILoading.SetActive(false);
		if(GameData.nStatus == GameData.Status.START) {
			obUIResult.SetActive(true);
		}				
	}
	
	public void OnStartGame() {
		ShowUICharacter();
		UpdateScoreBoard();
	}
	
	void ShowUICharacter() {
		string strPlayer = GameData.strCharacterList[(int)GameData.nPlayerCharacter];
		aniUIPlayer.Play("Character_" + strPlayer);
		txtUIPlayerName.text = GlobalValues.gameUser.name;
		
		string strFriend = GameData.strCharacterList[(int)GameData.nFriendCharacter];
		aniUIFriend.Play("Character_" + strFriend);
		txtUIFriendName.text = GameData.strFriendName;
	}
	
	public void OnAim() {
		aniUIWindArrow.Play("Wind");
		aniUIWindArrow.ClipFps = GameData.Archery.nWindSpeedFPS * (GameData.Archery.nWindSpeed / GameData.Archery.nWindMaxSpeed);
		float nAngle = Mathf.Atan2(GameData.Archery.nWindDirY, GameData.Archery.nWindDirX) * 180 / Mathf.PI;
		aniUIWindArrow.gameObject.transform.localEulerAngles = new Vector3(0f, 0f, nAngle);
		
		txtUIWindSpeed.text = GameData.Archery.nWindSpeed.ToString("N1");	
	}	

	public void OnRound() {
		ShowUIRound();
	}
	
	public void OnReady() {
		UpdateScoreBoard();
	}
	
	public void OnFriendReady() {
		OnReady();
	}
	
	public void OnMark() {
		UpdateScoreBoard();
	}	

	public void OnFriendMark() {
		OnMark();
	}	
	
	public void OnZoomOut() {
		obUIAim.SetActive(false);
	}		

	void UpdateScoreBoard() {
		txtUIPlayerScore.text = GameData.Archery.nPlayerScore.ToString("0");
        txtUIPlayerScore.FormatText(GameData.Archery.nPlayerScore.ToString("0"));
 		txtUIFriendScore.text = GameData.Archery.nFriendScore.ToString("0");
 
		// Scoreboard
		if(GameData.nStatus == GameData.Status.START) {
			spPlayerScoreBoard.gameObject.SetActive(false);
			spFriendScoreBoard.gameObject.SetActive(false);
			for(int i = 0; i < GameData.Archery.nMaxRound; i++) {
				listUIPlayerScoreText[i].gameObject.SetActive(false);
				listUIFriendScoreText[i].gameObject.SetActive(false);
			}
		}
		else {
			string strClipName = "UI_ScoreBoard" + GameData.Archery.nRound;
			for(int i = 0; i < GameData.Archery.nMaxRound; i++) {
				if(i == GameData.Archery.nRound - 1) {		// current round
					switch(GameData.Archery.nPlayStatus) {
						case GameData.Archery.PlayStatus.READY : {
							spPlayerScoreBoard.gameObject.SetActive(true);
							spPlayerScoreBoard.spriteId = spPlayerScoreBoard.GetSpriteIdByName(strClipName);
						}
						break;
						case GameData.Archery.PlayStatus.MARK : {
							listUIPlayerScoreText[i].gameObject.SetActive(true);
							listUIPlayerScoreText[i].text = GameData.Archery.listPlayerRoundScore[i].ToString("0");
						}
						break;
						case GameData.Archery.PlayStatus.FRIEND_READY : {
							spFriendScoreBoard.gameObject.SetActive(true);
							spFriendScoreBoard.spriteId = spFriendScoreBoard.GetSpriteIdByName(strClipName);
						}
						break;
						case GameData.Archery.PlayStatus.FRIEND_MARK : {
							listUIFriendScoreText[i].gameObject.SetActive(true);
							listUIFriendScoreText[i].text = GameData.Archery.listFriendRoundScore[i].ToString("0");
						}
						break;
					}
				}
			}
		}
	}

	public void ShowUIRound() {
		aniUIRound.gameObject.SetActive(true);

		string strClipName = "Round" + GameData.Archery.nRound;
		if(GameData.Archery.nRound == GameData.Archery.nMaxRound) {
			strClipName += "_Last";
		}		
		aniUIRound.Play(strClipName);
		aniUIRound.AnimationCompleted = OnCompleteShowUIRound;	
	}
	
	public void OnCompleteShowUIRound(tk2dSpriteAnimator ani, tk2dSpriteAnimationClip clip) {
		string strClipName = "Round_Start";
		
		aniUIRound.Play(strClipName);
		aniUIRound.AnimationCompleted = OnCompleteShowUIRoundStart;	
	}	
	
	public void OnCompleteShowUIRoundStart(tk2dSpriteAnimator ani, tk2dSpriteAnimationClip clip) {
		aniUIRound.gameObject.SetActive(false);
		
		obGamePlay.SendMessage("EndShowUIRound");
	}

    void OnTextChange() {
    }
}
