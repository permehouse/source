﻿using UnityEngine;
using System.Collections;

public class Camera : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        UpdatePos();
	}

    void UpdatePos() {
        GameObject player = GameObject.FindWithTag("Player");
        transform.position = player.transform.position;
        transform.localRotation = player.transform.localRotation;
    }
}
