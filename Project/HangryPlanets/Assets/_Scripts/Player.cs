﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
    public Planet obTarget;
    public float nDistFromTarget;
    public float nSpeed;
    public float nLerpSpeed;
    public float nLookAtDeviationSpeed;
    public float nLookAtFocusSpeed;
    public float nLookAtLerpSpeed;
    public float nRotateMoveLerpSpeed;
    public float nRotateLerpSpeed;
    Rigidbody iRigidBody;
    Vector3 posNew;
    Quaternion rotNew;
    Vector3 posLookAtCur;
    Vector3 posLookAtNext;

    Vector3 gyroOrigin = Vector3.zero;
    Vector3 gyroDiff = Vector3.zero;

    void Awake()
    {
        iRigidBody = GetComponent<Rigidbody>();
    }

	// Use this for initialization
	void Start () {
        posNew = transform.position;
        rotNew = transform.rotation;
        posLookAtNext = obTarget.transform.position;
        posLookAtCur = posLookAtNext;

        Input.gyro.enabled = true;
    }
	
	// Update is called once per frame
	void Update () {
        UpdateDistance();
        UpdatePos();
	}

    void UpdateDistance()
    {
        nDistFromTarget -= Time.deltaTime;
    }

    void UpdatePos()
    {
        float h = -Input.GetAxis("Horizontal") * nSpeed * Time.deltaTime;
        float v = Input.GetAxis("Vertical") * nSpeed * Time.deltaTime;
        float r = 5*Input.GetAxis("Rotation") * nSpeed * Time.deltaTime;
        float d = -10 * Input.GetAxis("Mouse ScrollWheel") * nSpeed * Time.deltaTime;

        if (Input.gyro.enabled)
        {
            if (gyroOrigin == Vector3.zero)
            {
                gyroOrigin = Input.gyro.attitude.eulerAngles;
            }

            gyroDiff = AdjustGyroDiffAngle(Input.gyro.attitude.eulerAngles - gyroOrigin);
            h = -gyroDiff.x * nSpeed * Time.deltaTime;
            v = gyroDiff.y * nSpeed * Time.deltaTime;
            r = gyroDiff.z * nSpeed * Time.deltaTime;
        }

        nDistFromTarget += d;
        
        Transform trPlayer = transform;
        Transform trTarget = obTarget.transform;

        Vector3 posBefore = trPlayer.position;
        trPlayer.RotateAround(obTarget.transform.position, transform.right, v);
        trPlayer.RotateAround(obTarget.transform.position, transform.up, h);

        Vector3 posDiff = trPlayer.position - posBefore;
        posNew += posDiff;
        posNew = (posNew - trTarget.position).normalized * nDistFromTarget;
        if ((posNew - posBefore).magnitude < 0.01)
        {
            trPlayer.position = posNew;
        }
        else
        {
            trPlayer.position = Vector3.Lerp(posBefore, posNew, nLerpSpeed * Time.deltaTime);
        }

        posDiff = posDiff * nLookAtDeviationSpeed * Time.deltaTime;
        posLookAtNext += posDiff;
        if ((posLookAtNext - posLookAtCur).magnitude < 0.01)
        {
            posLookAtCur = posLookAtNext;
        }
        else
        {
            posDiff = (trTarget.position - posLookAtNext).normalized * nLookAtFocusSpeed * Time.deltaTime;
            posLookAtNext += posDiff;
            posLookAtCur = Vector3.Lerp(posLookAtCur, posLookAtNext, nLookAtLerpSpeed * Time.deltaTime);
        }
        transform.LookAt(posLookAtCur, transform.up);

        rotNew.eulerAngles += transform.forward * r * nRotateMoveLerpSpeed;
        if ((rotNew.eulerAngles - transform.rotation.eulerAngles).magnitude < 0.01)
        {
            transform.rotation = rotNew;
        }
        else
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, rotNew, nRotateLerpSpeed * Time.deltaTime);
        }

/*
        Debug.DrawLine(transform.position, transform.position + transform.forward * 100, Color.blue);
        Debug.DrawLine(transform.position, transform.position + transform.right * 100, Color.red);
        Debug.DrawLine(transform.position, transform.position + transform.up * 100, Color.green);
*/
    }

    void OnGUI()
    {
        GUILayout.Label(gyroOrigin + " <- origin");
        GUILayout.Label(Input.gyro.attitude.eulerAngles + " <- gyro");
        GUILayout.Label(gyroDiff + " <- diff angle");
        GUILayout.Label(nSpeed + " <- speed");
    }

    public void OnButtonBack()
    {
        nDistFromTarget += 100;
    }

    public void OnButtonSpeedUp()
    {
        nSpeed *= 1.1f;
    }

    public void OnButtonSpeedDown()
    {
        nSpeed *= 0.9f;
    }

    public void OnButtonSetOrigin()
    {
        gyroOrigin = Input.gyro.attitude.eulerAngles;
    }


    Vector3 AdjustGyroDiffAngle(Vector3 angleDiff)
    {
        angleDiff.x = AdjustGyroDiffAngle(angleDiff.x);
        angleDiff.y = AdjustGyroDiffAngle(angleDiff.y);
        angleDiff.z = AdjustGyroDiffAngle(angleDiff.z);

        return angleDiff;
    }

    float AdjustGyroDiffAngle(float nAngle)
    {
        if (Mathf.Abs(nAngle) > 300)
        {
            nAngle += -Mathf.Sign(nAngle) * 360;
        }

        return nAngle;
    }
}
