using UnityEngine;
using System.Collections;

public class WorldManager {
	World iWorld = null;
	
	public void SelectWorld() {
		iWorld = new World_Normal();
		if(iWorld == null) {
			Debug.Log ("Error : iWorld is null.");	
			return;
		}
		
		iWorld.InitWorld();
	}
	
	public void UpdateDistance(float nDistance) {
		if(iWorld == null) {
			return;
		}
		
		iWorld.UpdateDistance(nDistance);
	}
	
	public float GetStageDistanceRate() {
		return iWorld.GetCurStage().GetProcessRate();
	}
}
