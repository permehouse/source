using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class World {
	List<Stage> iStageList = new List<Stage>();
	FriendPool iFriendPool = new FriendPool();
	
	int nIndex;
	bool bStarted = false;

	public virtual void InitWorld() {
		nIndex = 0;	
		bStarted = false;
	
		iFriendPool.InitPool();
	}
	
	public void AddStage(Stage iStage) {
		if(iStage == null) {
			Debug.Log ("Error : iStage is null.");	
			return;			
		}
		
		iStage.InitSet();
		iStageList.Add(iStage);
	}

	public Stage GetCurStage() {
		if(iStageList.Count <= 0) {
			return null;
		}
		
		if(nIndex >= iStageList.Count) {
			// if all of Stage is done, looping the last Stage
			nIndex = iStageList.Count - 1;
		}
		
		return iStageList[nIndex];
	}
	
	public Stage GetNextStage() {
		if(iStageList.Count <= 0) {
			return null;
		}
		
		nIndex++;
		if(nIndex >= iStageList.Count) {
			// if all of Stage is done, looping the last Stage
			nIndex = iStageList.Count - 1;
		}
	
		return iStageList[nIndex];
	}
	
	public void UpdateDistance(float nDistance) {
		Stage iStage = GetCurStage();
		if(iStage == null) {
//			Debug.Log ("Error : iStage is null. nIndex : " + nIndex + ", ListCount : " +  iStageList.Count);
			return;
		}
		
		if(bStarted == false) {
			bStarted = true;	
			iStage.StartStage(nDistance, false);
		}
		
		if(iStage.IsFinished(nDistance)) {
			Debug.Log (iStage.strName + " is finished. nDistance : " + nDistance);
			
			iStage = GetNextStage();
			iStage.StartStage(nDistance, true);
			
			Debug.Log (iStage.strName + " is started.");
		}
		
		iStage.UpdateDistance(nDistance);
		
		iFriendPool.UpdateDistance(nDistance);
	}
}
