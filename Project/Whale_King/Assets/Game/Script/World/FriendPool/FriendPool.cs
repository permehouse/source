using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FriendPool {
	public class FriendInfo {
		public int nRank;
		public string strName;
		public string strImageURL;
		public Player.PlayerType nPlayerType;
		public int nDistance;
		public int nScore;
		public bool bSpawn;
	}
	
	List<FriendInfo> iFriendList = new List<FriendInfo>();
	
	int nIndex = 0;
	
	public void InitPool() {
		// for Test
		AddTestFriendList();
		
		AddFriendList();
		SortFriendList();
		RankFriendList();

		// Show the first Rank User in the Score Area
		UpdateRankUser(0);
	}
	
	protected void AddTestFriendList() {
		Debug.Log(">>>>>>>>>>>>>>>>>>>>>>> AddTestFriendList <<<<<<<<<<<<<<<<<<<");
		int[] guids = {561, 525, 290, 305, 557, 558, 559, 560, 563, 562};
			
		GlobalValues.friendListInGame.Clear();
		for(int i = 0; i < 10; i++) {
			GlobalValues.GameContact contact = new GlobalValues.GameContact();
			contact.guid = guids[i];
			contact.name = "Friend"+(i+1);
			contact.character = "CH_DEFAULT";
			contact.weekScore = Random.Range (1000, 30000);
			contact.weekDistance = Random.Range (20, 5000);
			contact.week = GlobalValues.gameUser.week;
			contact.imageUrl = "/images/user/20.png";
			contact.digestFishes = 100;
			contact.isDead = false;		// set default as 'false'
			GlobalValues.friendListInGame.Add(contact);
		}
	}
	
	protected void AddFriendList() {
		Debug.Log(">>>>>>>>>>>>>>>>>>>>>>> AddFriendList <<<<<<<<<<<<<<<<<<<");
		Debug.Log("My name : " + GlobalValues.gameUser.name + ", week : " + GlobalValues.gameUser.week +", ImageURL : " + GlobalValues.gameUser.imageUrl);
		for(int i = 0; i < GlobalValues.friendListInGame.Count; i++) {
			GlobalValues.GameContact contact = (GlobalValues.GameContact)GlobalValues.friendListInGame[i];
			if(contact.week != GlobalValues.gameUser.week) {
				Debug.Log("---- Excluded Friend, name : " + contact.name + " by not same week : " + contact.week);
				continue;
			}
			
			if(contact.name == GlobalValues.gameUser.name) {
				Debug.Log("---- Excluded Friend, name : " + contact.name + " by not same name : " + contact.name);
				continue;
			}
			
			Player.PlayerType nPlayerType = Player.PlayerType.whale;
			if(contact.character == "CH_DEFAULT_C") {
				nPlayerType = Player.PlayerType.whale;
			}
			else if(contact.character == "CH_POLAR_B") {
				nPlayerType = Player.PlayerType.polarbear;
			}
			else if(contact.character == "CH_PORORO_A") {
				nPlayerType = Player.PlayerType.pororo;
			}
			else if(contact.character == "CH_MOBYDICK_B") {
				nPlayerType = Player.PlayerType.mobydick;
			}
			else if(contact.character == "CH_TURTLE_B") {
				nPlayerType = Player.PlayerType.turtle;
			}
			else if(contact.character == "CH_OCTOPUS_B") {
				nPlayerType = Player.PlayerType.octopus;
			}

			Debug.Log("Add Friend, name : " + contact.name + ", Type ; " + nPlayerType + ", ImageURL : " + contact.imageUrl);
			
			
			AddFriend(contact.name, contact.imageUrl, nPlayerType, contact.weekDistance, contact.weekScore, contact.digestFishes);
		}
	}
	
	public void SortFriendList() {
		iFriendList.Sort(SortCompareFunc);
	}
	
	int SortCompareFunc(FriendInfo iFriend1, FriendInfo iFriend2) {
	        if (iFriend1.nDistance > iFriend2.nDistance) {
			return 1;
		}
		else if(iFriend1.nDistance < iFriend2.nDistance) {
			return -1;
		}
		
		return 0;
	}
	
	public void RankFriendList() {
		int nRank = iFriendList.Count;
		for(int i = 0; i < iFriendList.Count; i++) {
			iFriendList[i].nRank = nRank;
			nRank--;

			Debug.Log("#### [Friend In Game]  Rank : " + iFriendList[i].nRank +"/" + iFriendList.Count +", Distance : " + iFriendList[i].nDistance + 
				", Character : " + iFriendList[i].nPlayerType + ", Name : " + iFriendList[i].strName + ", Score : " + iFriendList[i].nScore + ", ImageURL : " + iFriendList[i].strImageURL);
		}		
	}
	
	public void Clear() {
		iFriendList.Clear();
	}
	
	public void AddFriend(string strName, string strImageURL, Player.PlayerType nPlayerType, int nDistance, int nScore, int nHuntCount) {
		FriendInfo iFriendInfo = new FriendInfo();
		iFriendInfo.nRank = 0;
		iFriendInfo.strName = strName;
		iFriendInfo.strImageURL = strImageURL;
		iFriendInfo.nPlayerType = nPlayerType;
		iFriendInfo.nDistance = nDistance;
		iFriendInfo.nScore = nScore;
		iFriendInfo.bSpawn = false;
		
		iFriendList.Add(iFriendInfo);
	}

	public void UpdateDistance(float nDistance) {
		for(int i = nIndex; i < iFriendList.Count; i++) {
			if(iFriendList[i].bSpawn == false && iFriendList[i].nDistance <= nDistance) {
//				Debug.Log ("Friend Spawn!! - index : " + i + ", distance : " + iFriendList[i].nDistance);
				SpawnFriend(i);
			}
		}
	}
	
	public void SpawnFriend(int nIndexSpawn) {
		if(nIndexSpawn >= iFriendList.Count) {
			return;
		}
		
		FriendInfo iFriendInfo = iFriendList[nIndexSpawn];
		if(iFriendInfo.bSpawn == true) {
			return;
		}
		
		iFriendInfo.bSpawn = true;
		SpawnManager.SpawnFriend(iFriendInfo.strName, iFriendInfo.strImageURL, iFriendInfo.nPlayerType);			
		
		nIndex=nIndexSpawn+1;
		UpdateRankUser(nIndex);
	}
	
	public void UpdateRankUser(int nIndexRankUser) {
//		Debug.Log("========= nIndexRankUser : " + nIndexRankUser + ", iFriendList.Count : " + iFriendList.Count);
		if(nIndexRankUser >= iFriendList.Count) {
			ScoreManager.SetRank_NewRecord();			
			return;
		}
		
		FriendInfo iFriendInfo = iFriendList[nIndexRankUser];
		ScoreManager.SetRankUser(iFriendInfo.nRank, iFriendInfo.strName, iFriendInfo.strImageURL, iFriendInfo.nDistance);			
	}
}
