using UnityEngine;
using System.Collections;

public class World_Normal : World {
	public override void InitWorld() {
		base.InitWorld();


		
		AddStage(new Stage_Cave_C_1());			// 제일 쉬운 스테이지		
		AddStage(new Stage_Coast_C_2());		// 짧은 보너스 스테이지. 		
		
		AddStage(new Stage_Cave_A_1());			// 스테이지 1.
		AddStage(new Stage_Coast_C_1());		// 보너스 스테이지. 			
		
		AddStage(new Stage_Cave_B_1());			// 스테이지 2. 
		AddStage(new Stage_Coast_C_1());		// 보너스 스테이지. 				
		
		AddStage(new Stage_Coast_B_1());		// 매우 어려운 스테이지.		
		AddStage(new Stage_Coast_A_1());		// 해변가 처음 스테이지, 상어 나옴.
		
		AddStage(new Stage_Coast_C_1());		// 보너스 스테이지. 		
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		
		AddStage(new Stage_Coast_B_1());		// 매우 어려운 스테이지.					
		AddStage(new Stage_Cave_B_1());			// 스테이지 2. 
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		
		AddStage(new Stage_Coast_B_1());		// 매우 어려운 스테이지.					
		AddStage(new Stage_Cave_B_1());			// 스테이지 2. 
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		
		AddStage(new Stage_Coast_B_1());		// 매우 어려운 스테이지.					
		AddStage(new Stage_Cave_B_1());			// 스테이지 2. 
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		
		
		
		
		
		
		AddStage(new Stage_Coast_B_1());		// 매우 어려운 스테이지.					
		AddStage(new Stage_Cave_B_1());			// 스테이지 2. 
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		
		AddStage(new Stage_Coast_B_1());		// 매우 어려운 스테이지.					
		AddStage(new Stage_Cave_B_1());			// 스테이지 2. 
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		
		AddStage(new Stage_Coast_B_1());		// 매우 어려운 스테이지.					
		AddStage(new Stage_Cave_B_1());			// 스테이지 2. 
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		
		AddStage(new Stage_Coast_B_1());		// 매우 어려운 스테이지.					
		AddStage(new Stage_Cave_B_1());			// 스테이지 2. 
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		
		AddStage(new Stage_Coast_B_1());		// 매우 어려운 스테이지.					
		AddStage(new Stage_Cave_B_1());			// 스테이지 2. 
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		
		AddStage(new Stage_Coast_B_1());		// 매우 어려운 스테이지.					
		AddStage(new Stage_Cave_B_1());			// 스테이지 2. 
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		
		AddStage(new Stage_Coast_B_1());		// 매우 어려운 스테이지.					
		AddStage(new Stage_Cave_B_1());			// 스테이지 2. 
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		
		AddStage(new Stage_Coast_B_1());		// 매우 어려운 스테이지.					
		AddStage(new Stage_Cave_B_1());			// 스테이지 2. 
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		AddStage(new Stage_Coast_B_2());		// 제일 어려운 스테이지.		
		
		AddStage(new Stage_Cave_C_1());		
	}
}
