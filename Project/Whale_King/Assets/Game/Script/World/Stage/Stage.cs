using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Stage {
	public class FishInfo {
		public bool bFish;				// fish or item
		public float nDistance;
		public int nType;
		public int nMovePatternType;
		public int nLane;
		public bool bSpawn;
	}
	
	public string strName = null;
	protected string strBackground = null;
	
	List<FishInfo> iFishList = new List<FishInfo>();
	int nIndex = 0;

	float nStartDistance = 0f;
	bool bInitPlayerLevel = false;
	float nGameSpeed = GameSettings.nGameSpeed;
	
	public float GetEndDistance() {
		if(iFishList.Count > 0) {
			float nDistance = iFishList[iFishList.Count - 1].nDistance;
			return (nStartDistance + nDistance);
		}
		
		return 0f;
	}
	
	public float GetProcessRate() {
		float nStageDistance = GetEndDistance() - nStartDistance;
		float nCurDistance = GamePlay.nGameDistance - nStartDistance;
		
		return nCurDistance/nStageDistance;	
	}
	
	public virtual void InitSet() {
		nIndex = 0;
		nStartDistance = 0f;
	}
	
	public void SortFishList() {
		iFishList.Sort(SortCompareFunc);
	}
	
	public void Clear() {
		iFishList.Clear();
	}
	
	public void AddFish(float nDistance) {
		AddFish(nDistance, Fish.FishType.random, Fish.MovePatternType.random, -1);
	}
	
	public void AddFish(float nDistance, Fish.FishType nType) {
		AddFish (nDistance, nType, Fish.MovePatternType.random, -1);
	}

	public void AddFish(float nDistance, Fish.FishType nType, Fish.MovePatternType nMovePatternType) {
		AddFish (nDistance, nType, nMovePatternType, -1);
	}
	
	public void AddFish(float nDistance, Fish.FishType nType, Fish.MovePatternType nMovePatternType, int nLane) {
		FishInfo iFishInfo = new FishInfo();
		iFishInfo.bFish = true;
		iFishInfo.nDistance = nDistance;
		iFishInfo.nType = (int)nType;
		iFishInfo.nMovePatternType = (int)nMovePatternType;
		iFishInfo.nLane = nLane;
		iFishInfo.bSpawn = false;
		
		iFishList.Add(iFishInfo);
	}
	
	public void AddItem(float nDistance) {
		AddItem(nDistance, Item.ItemType.random, -1);
	}
	
	public void AddItem(float nDistance, Item.ItemType nType) {
		AddItem(nDistance, nType, -1);
	}
	
	public void AddItem(float nDistance, Item.ItemType nType, int nLane) {
		FishInfo iFishInfo = new FishInfo();
		iFishInfo.bFish = false;
		iFishInfo.nDistance = nDistance;
		iFishInfo.nType = (int)nType;
		iFishInfo.nMovePatternType = -1;
		iFishInfo.nLane = nLane;
		iFishInfo.bSpawn = false;
		
		iFishList.Add(iFishInfo);
	}

	public void RepeatFish(float nStartDistance, float nEndDistance, float nPeriod, Fish.FishType nType) {
		RepeatFish(nStartDistance, nEndDistance, nPeriod, nType, Fish.MovePatternType.random);
	}
	
	public void RepeatFish(float nStartDistance, float nEndDistance, float nPeriod, Fish.FishType nType, Fish.MovePatternType nMovePatternType) {
		RepeatFish(nStartDistance, nEndDistance, nPeriod, nType, nMovePatternType, -1);
	}
	
	public void RepeatFish(float nStartDistance, float nEndDistance, float nPeriod, Fish.FishType nType, Fish.MovePatternType nMovePatternType, int nLane) {
		for(float i = nStartDistance; i<=nEndDistance; i+=nPeriod) {
			AddFish(i, nType, nMovePatternType, nLane);
		}
	}

	public void RepeatBonus(float nStartDistance, float nEndDistance, float nPeriod) {
		for(float i = nStartDistance; i<=nEndDistance; i+=nPeriod) {
			AddFish(i, Fish.FishType.bonus);			
		}
	}
		
	public void RepeatItem(float nStartDistance, float nEndDistance, float nPeriod) {
		for(float i = nStartDistance; i<=nEndDistance; i+=nPeriod) {
			AddItem(i);			
		}
	}
	
	//yj edit
	public void RepeatItem(float nStartDistance, float nEndDistance, float nPeriod, Item.ItemType nType) {
		for(float i = nStartDistance; i<=nEndDistance; i+=nPeriod) {
			AddItem(i,nType);			
		}
	}

	public void RepeatItem(float nStartDistance, float nEndDistance, float nPeriod, Item.ItemType nType, int nLane) {
		for(float i = nStartDistance; i<=nEndDistance; i+=nPeriod) {
			AddItem(i,nType, nLane);			
		}
	}
	
	int SortCompareFunc(FishInfo iFish1, FishInfo iFish2) {
        	if (iFish1.nDistance > iFish2.nDistance) {
			return 1;
		}
		else if(iFish1.nDistance < iFish2.nDistance) {
			return -1;
		}
		
		return 0;
    	}

	public bool IsFinished(float nDistance) {
		if(nDistance > GetEndDistance()) {
//			Debug.Log (strName + " is finished. nDistance : " + nDistance + ", EndDistance : " + GetEndDistance());
			return true;
		}

		return false;
	}
	
	public void StartStage(float nDistance, bool bFadeOutBackground) {
		nStartDistance = nDistance;
		
		for(int i = 0; i < iFishList.Count; i++) {
			iFishList[i].bSpawn = false;
		}
		
		nIndex = 0;
		
		GamePlay.nStageGameSpeed = nGameSpeed;
		if(bInitPlayerLevel == true) {
			GamePlay.obPlayer.SetLevel(1);
		}
		
		Background.SetBackground(strBackground, bFadeOutBackground);	
	}

	public void UpdateDistance(float nDistance) {
		if(bInitPlayerLevel == true) {
			GamePlay.obPlayer.SetLevel(1);
		}
		
		for(int i = nIndex; i < iFishList.Count; i++) {
//			Debug.Log ("index : " + i + ", distance : " + iFishList[i].nDistance + ", total distance : " + nDistance);
			if(iFishList[i].bSpawn == false && (nStartDistance + iFishList[i].nDistance) <= nDistance) {
				SpawnFish(iFishList[i]);
				nIndex=i+1;
			}
		}
	}
	
	public void InitPlayerLevel() {
		bInitPlayerLevel = true;
	}

	public void SetStageSpeed(float nSpeed) {
		nGameSpeed = nSpeed;
	}
	
	public void SpawnFish(FishInfo iFishInfo) {
		if(iFishInfo.bSpawn == true) {
			return;
		}
		
		iFishInfo.bSpawn = true;
		
		if(iFishInfo.bFish) {
			SpawnManager.SpawnFish((Fish.FishType)iFishInfo.nType, (Fish.MovePatternType)iFishInfo.nMovePatternType, iFishInfo.nLane);
		}
		else {
			SpawnManager.SpawnItem((Item.ItemType)iFishInfo.nType, iFishInfo.nLane);			
		}
	}
}
