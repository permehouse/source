using UnityEngine;
using System.Collections;

public class Stage_Coast_C_2 : Stage {
	public override void InitSet() {
		
		base.InitSet();
		
		strName = "Stage_Coast_C_2";
		strBackground = "Coast";
		
		//float nDistance = 80f;
		
		
		int[,,] Map = {
		// 1f~80f.
		{
		{0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,  0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,  0,0,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0},// 0,2,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,  0,6,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,  0,0,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0},// 0,2,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,  0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,  6,0,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0},// 0,2,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,  0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,  0,0,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0}//, 0,2,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0}
		}
		};
		/*
		{
		{0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,  1,6,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,  6,0,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0, 0,2,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,2,0,0,0,0,1,0,0,0,0,0,0,0,2,0,1,  0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,  0,0,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0, 0,2,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,2,0,0,0,1,0,0,0,0,0,0,0,0,2,0,0,  1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,  0,0,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0, 0,2,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,2,0,0,0,0,1,0,0,0,0,0,0,0,2,0,0,  0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,  0,0,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0, 0,2,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0}
		}
		};
		*/
		
		for (int k=0;k<1;k++)				// 맵 배열. 
		{
			for (int j=0;j<4;j++)			// Lane.
			{
				for (int i=0;i<60;i++)		// 각 거리 위치당 Object.
				{				
					if (Map[k,j,i] > 0)
					{								
						// Object의 종류 식별.						
						switch (Map[k,j,i]){
						
							case 1:	// 붕어.
								AddFish(i+60*k, Fish.FishType.redcarp, Fish.MovePatternType.MPT_LINE, 3-j);							
								break;									
							case 2:	// 붕어.
								AddFish(i+60*k, Fish.FishType.redcarp, Fish.MovePatternType.MPT_MOVING_U_OR_REVERSE, 3-j);								
								break;									
							case 3:	// 위아래 중간 물고기.
								
								break;									
							case 4:	// 상어.
								
								break;														
							case 5:	// 하트 제외 랜덤 아이템.								
								switch (Random.Range(0,5))
								{
									case 0:			
										AddItem(i+60*k, Item.ItemType.Magnet);
										break;
									case 1:
										AddItem(i+60*k, Item.ItemType.PinkWhale);
										break;
									case 2:			
										AddItem(i+60*k, Item.ItemType.StarStick);
										break;
									case 3:			
										AddItem(i+60*k, Item.ItemType.GoldFish);
										break;
									case 4:			
										AddItem(i+60*k, Item.ItemType.Wing);
										break;										
								}					
								break;
							case 6:	// 자석 아이템.
								AddItem(i+60*k,Item.ItemType.Magnet);							
								break;
							case 7:	// 하트 아이템.
								AddItem(i+60*k,Item.ItemType.Heart);							
								break;
							case 8:	//자석,돌고래,거대화 중에 하나.								
								switch (Random.Range(0,3))
								{
									case 0:			
										AddItem(i+60*k, Item.ItemType.Magnet);
										break;
									case 1:
										AddItem(i+60*k, Item.ItemType.PinkWhale);
										break;
									case 2:			
										AddItem(i+60*k, Item.ItemType.StarStick);
										break;
								}
								break;
							case 9:	// 고정 랜덤 아이템.
								AddItem(i+60*k,Item.ItemType.random);							
								break;
							case 11:	// 황금 물고기 아이템.
								AddItem(i+60*k,Item.ItemType.GoldFish,3-j);							
								break;
							case 12:
							AddItem(i+60*k, Item.ItemType.StarStick);
							break;
						}							
					}				
				}			
			}
		}
		
		SortFishList();
	}
}
