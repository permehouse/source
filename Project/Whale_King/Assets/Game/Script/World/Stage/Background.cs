using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour {
	static public Background _instance;
	protected string strBG = null;
	
	// Use this for initialization
	public GameObject obBG_Far1;
	public GameObject obBG_Far2;
	public GameObject obBG_Mid1;
	public GameObject obBG_Mid2;
	public GameObject obBG_Near1;
	public GameObject obBG_Near2;
//	public GameObject obCurtain_Left;
//	public GameObject obCurtain_Right;

 	tk2dSprite obSpriteBG_Far1 = null;
 	tk2dSprite obSpriteBG_Far2 = null;
 	tk2dSprite obSpriteBG_Mid1 = null;
 	tk2dSprite obSpriteBG_Mid2 = null;
 	tk2dSprite obSpriteBG_Near1 = null;
 	tk2dSprite obSpriteBG_Near2 = null;
	
	int nBG_Far_Width=1280;
	int nBG_Mid_Width=1280;
	int nBG_Near_Width=2000;
//	int nCurtainWidth=320;
//	int nCurtainHeight=720;
	
	private float nFadeTime = 1f;

	private float nBubbleEffectTime = 0f;
	private float nBubbleEffectCount = 3f;
	private float nFlowBubbleEffectTime = 0f;
	private float nFlowBubbleEffectCount = 3f;
	
	void Awake() {
		_instance = this;	
		
	 	obSpriteBG_Far1 = obBG_Far1.GetComponent<tk2dSprite>();
	 	obSpriteBG_Far2 = obBG_Far2.GetComponent<tk2dSprite>();
	 	obSpriteBG_Mid1 = obBG_Mid1.GetComponent<tk2dSprite>();
	 	obSpriteBG_Mid2 = obBG_Mid2.GetComponent<tk2dSprite>();
	 	obSpriteBG_Near1 = obBG_Near1.GetComponent<tk2dSprite>();
	 	obSpriteBG_Near2 = obBG_Near2.GetComponent<tk2dSprite>();
	}
		
	// Use this for initialization
	void Start () {
		ResetPosition();

		// curtain
/*		int nLeftCurtainCenterX = GameSettings.nScreenPositionX - nCurtainWidth/2;
		int nLeftCurtainCenterY = GameSettings.nScreenPositionY + nCurtainHeight/2;
		obCurtain_Left.transform.position = new Vector3(nLeftCurtainCenterX, nLeftCurtainCenterY, GameSettings.LAYER_TOPMOST);		

		int nRightCurtainCenterX = GameSettings.nScreenPositionX + GameSettings.nScreenWidth + nCurtainWidth/2;
		int nRightCurtainCenterY = GameSettings.nScreenPositionY + nCurtainHeight/2;
		obCurtain_Right.transform.position = new Vector3(nRightCurtainCenterX, nRightCurtainCenterY, GameSettings.LAYER_TOPMOST);		
*/		
	}
	
	void ResetPosition() {
		nBG_Far_Width = (int)obSpriteBG_Far1.GetBounds().size.x;
		nBG_Mid_Width = (int)obSpriteBG_Mid1.GetBounds().size.x;
		nBG_Near_Width = (int)obSpriteBG_Near1.GetBounds().size.x;
			
		// 'upper left' anchor
		int nBG_Far_X = 0;
		int nBG_Far_Y = GameSettings.nScreenPositionY + GameSettings.nScreenHeight - 1;
		obBG_Far1.transform.position = new Vector3(nBG_Far_X, nBG_Far_Y, GameSettings.LAYER_BG_FAR);		

		nBG_Far_X += nBG_Far_Width;
		obBG_Far2.transform.position = new Vector3(nBG_Far_X, nBG_Far_Y, GameSettings.LAYER_BG_FAR);		
		
		// 'lower left' anchor
		int nBG_Mid_X = GameSettings.nScreenPositionX;
		int nBG_Mid_Y = GameSettings.nScreenPositionY;
		obBG_Mid1.transform.position = new Vector3(nBG_Mid_X, nBG_Mid_Y, GameSettings.LAYER_BG_MID);		
		
		nBG_Mid_X += nBG_Mid_Width;
		obBG_Mid2.transform.position = new Vector3(nBG_Mid_X, nBG_Mid_Y, GameSettings.LAYER_BG_MID);		

		// 'lower left' anchor
		int nBG_Near_X = GameSettings.nScreenPositionX;
		int nBG_Near_Y = GameSettings.nScreenPositionY;
		obBG_Near1.transform.position = new Vector3(nBG_Near_X, nBG_Near_Y, GameSettings.LAYER_BG_NEAR);		
		
		nBG_Near_X += nBG_Near_Width;
		obBG_Near2.transform.position = new Vector3(nBG_Near_X, nBG_Near_Y, GameSettings.LAYER_BG_NEAR);		
	}
	
	// Update is called once per frame
	void Update() {
		if(GamePlay.nGameStatus != GamePlay.GameStatus.PLAY) {
			return;
		}
		
		float nSpeed = GamePlay.nGameSpeed * GameSettings.nPixelPerMeter;
		
		float dxMove_BG_Far =  -nSpeed * GameSettings.nSpeedRate_BG_Far * Time.deltaTime;
		float dxMove_BG_Mid =  -nSpeed * GameSettings.nSpeedRate_BG_Mid * Time.deltaTime;
		float dxMove_BG_Near =  -nSpeed * GameSettings.nSpeedRate_BG_Near * Time.deltaTime;
		
		Move(obBG_Far1, dxMove_BG_Far);
		Move(obBG_Far2, dxMove_BG_Far);
		Move(obBG_Mid1, dxMove_BG_Mid);
		Move(obBG_Mid2, dxMove_BG_Mid);
		Move(obBG_Near1, dxMove_BG_Near);
		Move(obBG_Near2, dxMove_BG_Near);
		CheckRotate(obBG_Far1, nBG_Far_Width);
		CheckRotate(obBG_Far2, nBG_Far_Width);		
		CheckRotate(obBG_Mid1, nBG_Mid_Width);
		CheckRotate(obBG_Mid2, nBG_Mid_Width);
		CheckRotate(obBG_Near1, nBG_Near_Width);
		CheckRotate(obBG_Near2, nBG_Near_Width);
		
		UpdateBubbleEffect();
	}
	
	void Move(GameObject obBG, float dxMove) {
		obBG.transform.Translate(new Vector3(dxMove, 0, 0));
	}
	
	void CheckRotate(GameObject obBG, int nBGWidth) 	{
		if(obBG.transform.position.x <= (GameSettings.nScreenPositionX - nBGWidth)) {
			obBG.transform.Translate(new Vector3(2*nBGWidth, 0, 0));
		}
	}
	
	static public void SetBackground(string strBackground, bool bFadeOut) {
		_instance.StartCoroutine(_instance.CR_SetBackground(strBackground, bFadeOut));
	}
	
	IEnumerator CR_SetBackground(string strBackground, bool bFadeOut) {
		if(strBackground == null) {
			yield break;
		}
		
		if(strBG == strBackground) {
			yield break;
		}
		
		strBG = strBackground;
		
		float nTime;
		Color nColorFrom;
		Color nColorTo;
		if(bFadeOut) {
			// Fade out current background
			nTime = 0;
			nColorFrom = new Color(1,1,1,1);
			nColorTo = new Color(1,1,1,0);
			while (nTime < nFadeTime) {
				if(GamePlay.nGameStatus != GamePlay.GameStatus.PLAY) {
					yield return null;
					continue;
				}
				
				nTime += Time.deltaTime;
				
				obSpriteBG_Far1.color = Color.Lerp(nColorFrom, nColorTo, nTime/nFadeTime);
				obSpriteBG_Far2.color = Color.Lerp(nColorFrom, nColorTo, nTime/nFadeTime);
				obSpriteBG_Mid1.color = Color.Lerp(nColorFrom, nColorTo, nTime/nFadeTime);
				obSpriteBG_Mid2.color = Color.Lerp(nColorFrom, nColorTo, nTime/nFadeTime);
				obSpriteBG_Near1.color = Color.Lerp(nColorFrom, nColorTo, nTime/nFadeTime);
				obSpriteBG_Near2.color = Color.Lerp(nColorFrom, nColorTo, nTime/nFadeTime);
				
				yield return null;
			}
			
			// setting the full colors
			obSpriteBG_Far1.color = nColorTo;
			obSpriteBG_Far2.color = nColorTo;
			obSpriteBG_Mid1.color = nColorTo;
			obSpriteBG_Mid2.color = nColorTo;
			obSpriteBG_Near1.color = nColorTo;
			obSpriteBG_Near2.color = nColorTo;
		}
		
		string strBGName = "BG_" + strBG + "_Far";
		obSpriteBG_Far1.spriteId = obSpriteBG_Far2.GetSpriteIdByName(strBGName);
		obSpriteBG_Far2.spriteId = obSpriteBG_Far2.GetSpriteIdByName(strBGName);
		
		strBGName = "BG_" + strBG + "_Mid";
		obSpriteBG_Mid1.spriteId = obSpriteBG_Mid1.GetSpriteIdByName(strBGName);
		obSpriteBG_Mid2.spriteId = obSpriteBG_Mid2.GetSpriteIdByName(strBGName);

		strBGName = "BG_" + strBG + "_Near";
		obSpriteBG_Near1.spriteId = obSpriteBG_Near1.GetSpriteIdByName(strBGName);
		obSpriteBG_Near2.spriteId = obSpriteBG_Near2.GetSpriteIdByName(strBGName);
		
		ResetPosition();

		// Fade in new background
		nTime = 0;
		nColorFrom = new Color(1,1,1,0);
		nColorTo = new Color(1,1,1,1);
		while (nTime < nFadeTime) {
			if(GamePlay.nGameStatus != GamePlay.GameStatus.PLAY) {
				yield return null;
				continue;
			}
			
			nTime += Time.deltaTime;
			
			obSpriteBG_Far1.color = Color.Lerp(nColorFrom, nColorTo, nTime/nFadeTime);
			obSpriteBG_Far2.color = Color.Lerp(nColorFrom, nColorTo, nTime/nFadeTime);
			obSpriteBG_Mid1.color = Color.Lerp(nColorFrom, nColorTo, nTime/nFadeTime);
			obSpriteBG_Mid2.color = Color.Lerp(nColorFrom, nColorTo, nTime/nFadeTime);
			obSpriteBG_Near1.color = Color.Lerp(nColorFrom, nColorTo, nTime/nFadeTime);
			obSpriteBG_Near2.color = Color.Lerp(nColorFrom, nColorTo, nTime/nFadeTime);
			
			yield return null;
		}

		// setting the full colors
		obSpriteBG_Far1.color = nColorTo;
		obSpriteBG_Far2.color = nColorTo;
		obSpriteBG_Mid1.color = nColorTo;
		obSpriteBG_Mid2.color = nColorTo;
		obSpriteBG_Near1.color = nColorTo;
		obSpriteBG_Near2.color = nColorTo;
	}
	
	public void UpdateBubbleEffect() {
		nBubbleEffectTime += Time.deltaTime;
		if(nBubbleEffectTime > GameSettings.nBGBubbleEffectTime) {
			nBubbleEffectCount--;
			if(nBubbleEffectCount <= 0) {
				nBubbleEffectTime = 0f;
				nBubbleEffectCount = Random.Range(2,4);
			}
			else {
				nBubbleEffectTime -= Random.Range(0.2f, 0.5f);
			}
			
			int nOffsetX = 150;
			int nOffsetY = 150;
			int nX = Random.Range(nOffsetX, GameSettings.nScreenWidth - nOffsetX);
			int nY = Random.Range(nOffsetY, GameSettings.nScreenHeight - nOffsetY);
			
			EffectManager.ShowEffect_BG_Bubble(new Vector2(nX, nY));
			EffectManager.ShowEffect_BG_Flow_Bubble();
		}
		
		nFlowBubbleEffectTime += Time.deltaTime;
		if(nFlowBubbleEffectTime > GameSettings.nBGFlowBubbleEffectTime) {
			nFlowBubbleEffectCount--;
			if(nFlowBubbleEffectCount <= 0) {
				nFlowBubbleEffectTime = 0f;
				nFlowBubbleEffectCount = Random.Range(2,4);
			}
			else {
				nFlowBubbleEffectTime -= Random.Range(0.2f, 0.5f);
			}
			
			EffectManager.ShowEffect_BG_Flow_Bubble();
		}
	}
}
