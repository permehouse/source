using UnityEngine;
using System.Collections;

public class Stage_Coast_B_1 : Stage {
	public override void InitSet() {
		
		base.InitSet();
		
		strName = "Stage_Coast_B_1";
		strBackground = "Coast";		
		float nDistance = 300f;
		SetStageSpeed(1.0f * GameSettings.nGameSpeed);
		
		
		// Setting Fishes
		RepeatFish(20f, nDistance, 20f, Fish.FishType.redcarp, Fish.MovePatternType.MPT_LINE);
		RepeatFish(30f, nDistance, 20f, Fish.FishType.redcarp, Fish.MovePatternType.MPT_MOVING_U);
		
		RepeatFish(30f, nDistance, 10f, Fish.FishType.middlecarp);
		RepeatFish(20f, 200f, 15f, Fish.FishType.redshark);
		RepeatFish(20f, 200f, 15f, Fish.FishType.shark);
		RepeatFish(90f, 200f, 15f, Fish.FishType.shark);
		
		// Setting Bonus
		RepeatBonus(20f, nDistance, 20f);

		// Setting Items
		RepeatItem(30f, nDistance, 45f);
		
		SortFishList();
		
		
	}
	
}

