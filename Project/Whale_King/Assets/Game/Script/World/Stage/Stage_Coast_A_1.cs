using UnityEngine;
using System.Collections;

public class Stage_Coast_A_1 : Stage {
	public override void InitSet() {
		
		base.InitSet();
		
		strName = "Stage_Coast_A_1";
		strBackground = "Coast";		
		float nDistance = 300f;
		SetStageSpeed(1.0f * GameSettings.nGameSpeed);
		
		// Setting Fishes
		RepeatFish(20f, nDistance, 10f, Fish.FishType.redcarp, Fish.MovePatternType.MPT_LINE);
		RepeatFish(24f, nDistance, 10f, Fish.FishType.redcarp, Fish.MovePatternType.MPT_MOVING_U);
		
		RepeatFish(30f, nDistance, 10f, Fish.FishType.middlecarp);
		RepeatFish(20f, nDistance, 25f, Fish.FishType.shark);
		
		// Setting Bonus
		RepeatBonus(20f, nDistance, 20f);

		// Setting Items
		RepeatItem(50f, nDistance, 50f);
		
		SortFishList();
	}
}
