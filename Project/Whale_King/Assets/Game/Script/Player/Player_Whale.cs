using UnityEngine;
using System.Collections;

public class Player_Whale : Player {
	// Use this for initialization
	public override void Start () {
		strAniName = "Player_Whale";
		nType = PlayerType.whale;
		nMoveType = MoveType.speedy;
		
		SetProperty(PlayerSettings.obWhale);

		base.Start();			
	}
}
