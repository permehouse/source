using UnityEngine;
using System.Collections;

public class Player_Mobydick : Player {
	// Use this for initialization
	public override void Start () {
		strAniName = "Player_Mobydick";
		nType = PlayerType.mobydick;
		nMoveType = MoveType.speedy;
		
		SetProperty(PlayerSettings.obMobydick);
		
		base.Start();
	}
}
