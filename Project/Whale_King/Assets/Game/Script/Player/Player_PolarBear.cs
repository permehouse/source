using UnityEngine;
using System.Collections;

public class Player_PolarBear : Player {
	// Use this for initialization
	public override void Start () {
		strAniName = "Player_PolarBear";
		nType = PlayerType.polarbear;
		nMoveType = MoveType.speedy;
		
		SetProperty(PlayerSettings.obPolaBear);
		
		base.Start();		
	}
}
