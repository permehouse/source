using UnityEngine;
using System.Collections;

public class Player_Pororo : Player {
	// Use this for initialization
	public override void Start () {
		strAniName = "Player_Pororo";
		nType = Player.PlayerType.pororo;
		nMoveType = MoveType.speedy;

		SetProperty(PlayerSettings.obPororo);
		
		base.Start();				
	}
}
