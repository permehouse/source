using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	public enum PlayerType {
		none = -1,
		whale = 0,
		polarbear = 1,
		pororo = 2,
		mobydick = 3,
		turtle = 4,
		octopus = 5
	}
	
	public enum MoveType {
		simple,
		speedy,
		bounce,
	}
	
	public int nGrade = GameSettings.nGrade_C;
	public int nLevel = 0;
	public int nPowerModeLevel = 0;
	public float nLevelGage = 0;
	public float nHP = 100;
	public float nFullHP = 150;
	protected string strAniName = null;
	public PlayerSettings.Property obInitProp;
	
	public PlayerType nType = PlayerType.whale;
	protected MoveType nMoveType = MoveType.simple;
	protected int nMoveToLane = -1;
	protected float nMoveToPositionY = -1f;
	protected float nSpeed = GameSettings.nPlayerMoveSpeed;
	protected float nInitScale = 1f;
	
    	protected tk2dAnimatedSprite obAni;
	protected bool bAttack = false;
	
	public bool bDie = false;
	public bool bDieNoHP = false;
	public bool bDamage = false;
	public bool bBlink = false;

	public bool bPowerMode = false;
	public bool bBossMode = false;
	
	public bool bFeverMode = false;
	protected float nFeverTime = 0f;
	
	public bool bStartRun = false;
	protected float nStartRunDistance = 0f;

	public bool bLastRun = false;
	protected float nLastRunTime = 0f;
	
	public bool bMagnetMode = false;
	protected float nMagnetTime = 0f;

	public bool bWingMode = false;
	protected float nWingTime = 0f;
	
	protected bool bPlayerProperty_Magnet = false;
	
	protected float nBubbleEffectTime = 0f;
	
	// SWA
	public bool bSWA = false;
	float nSWATime = 0f;
	bool bCheckSWA = false;
	Vector2 posSWA;
	
	bool bPaused = false;
	
	public bool bRelay = false;
	
	bool bPlayerProperty_Escape = false;
	float nEscapeRate = 0f;
	
	// Effect Object
	private Effect obEffect_StartRun = null;
	private Effect obEffect_LastRun = null;
	private Effect obEffect_FeverMode = null;
	private Effect obEffect_PowerMode = null;
	
	void Awake() {
		obAni = GetComponent<tk2dAnimatedSprite>();
	}
	
	public virtual void Start() {
		obAni.Play(GetAniName());
		MoveToStartPosition();
	}
	
	public void SetProperty(PlayerSettings.Property obProp) {
		obInitProp = obProp;
			
		nGrade = obProp.nGrade;
		nLevel = obProp.nLevel;
		nHP = obProp.nHP;
		nFullHP = obProp.nFullHP;
		nSpeed *= obProp.nSpeedRate;
		nInitScale = obProp.nInitScale;
		nPowerModeLevel = obProp.nPowerModeLevel;
		
		if(obProp.bBoostExp) {
			BoostSettings.SetPlayerBoostExpRate(obProp.GetBoostExpRate());	
		}
		
		bPlayerProperty_Escape = obProp.bBoostEscape;
		if(bPlayerProperty_Escape) {
			nEscapeRate = obProp.GetBoostEscapeRate();
		}
		
		if(obProp.bBoostFastDigest) {
			BoostSettings.SetPlayerBoostFastDigestRate(obProp.GetBoostFastDigestRate());	
		}
		
		if(obProp.bBoostDamageDown) {
			BoostSettings.SetPlayerBoostDamageDownRate(obProp.GetBoostDamageDownRate());	
		}
		
		Debug.Log("Player Grade : " + obProp.nGrade + ", level : " + nLevel);
		
		transform.localScale = new Vector3(nInitScale, nInitScale, 1f);
		
		if(bRelay) {
			nHP = obProp.nRelayHP;
			SetBlink(true);
		}
		else {
			AddHP(BoostSettings.nHPUpAmount);
			
			if(BoostSettings.bStartRun) {
				BoostSettings.bStartRun = false;
				SetStartRun(true);
			}
		}
		
		// for testing 'Power Mode' quickly
//		ScoreManager.AddExpGage(99f);
	}
	
	// Update is called once per frame
	void Update() {
		CheckGamePause();
		if(GamePlay.nGameStatus != GamePlay.GameStatus.PLAY) {
			return;
		}
		
		UpdateFeverMode();
		UpdateStartRun();
		UpdateLastRun();
		UpdatePowerMode();
		UpdateMagnetMode();
		UpdateWingMode();
			
		UpdateMoveToPositionY();
		UpdateHP();
		
		UpdateBubbleEffect();
		
		UpdateSWATime();
		
//		float nDistance = GameSettings.nDistanceSWA;
//		Debug.DrawRay(transform.position, Vector3.right * nDistance, Color.white);		
	}

	public void MoveUp() {
		if(GamePlay.nPlayerLane<GameSettings.nPlayLaneCount-1) {
			CheckSWA(true);
			MoveToLane(GamePlay.nPlayerLane+1);
		}
	}
	
	public void MoveDown() {
		if(GamePlay.nPlayerLane>0) {
			CheckSWA(false);
			MoveToLane(GamePlay.nPlayerLane-1);
		}
	}
	
	public void MoveToLane(int nLane) {
		if(bDie) {
			return;
		}
		
		if(nLane < 0) nLane = 0;
		else if(nLane > GameSettings.nPlayLaneCount-1) nLane = GameSettings.nPlayLaneCount-1;
		
		int nY = GameSettings.listPlayLaneCenterY[nLane];
		Vector3 posEnd = new Vector3(GameSettings.nPlayerPositionX, nY, transform.position.z);

		 iTween.EaseType easeType = iTween.EaseType.linear;
		switch(nMoveType) {
		case MoveType.simple : easeType = iTween.EaseType.linear; break;
		case MoveType.speedy : easeType = iTween.EaseType.easeOutSine; break;
		case MoveType.bounce : easeType = iTween.EaseType.easeOutBack; break;
		}
		
		Hashtable tweenParam = new Hashtable();
		tweenParam.Add ("position", posEnd);
		tweenParam.Add ("easetype", easeType);
		tweenParam.Add ("speed", nSpeed);
		tweenParam.Add ("oncomplete", "OnFinishMove");
		
		iTween.MoveTo(gameObject, tweenParam);

		// Stop to move to position Y
		nMoveToPositionY = -1f;
	}
	
	public void MoveToPositionY(float nY) {
		if(bDie) {
			return;
		}
		
		if(nY < 0) {
			nY = 0;
		} 
		else if(nY > GameSettings.nPlayAreaStartPositionY + GameSettings.nPlayAreaHeight) {
			nY = GameSettings.nPlayAreaStartPositionY + GameSettings.nPlayAreaHeight;
		}
		
		nMoveToPositionY = nY;
	}
	
	public void MoveToStartPosition() {
		float nX = GameSettings.nScreenPositionX;
		transform.position = new Vector3(nX, transform.position.y, transform.position.z);
		
		int nLane = GamePlay.nPlayerLane;
		int nY = GameSettings.listPlayLaneCenterY[nLane];
		Vector3 posEnd = new Vector3(GameSettings.nPlayerPositionX, nY, transform.position.z);

		iTween.EaseType easeType = iTween.EaseType.linear;
		
		Hashtable tweenParam = new Hashtable();
		tweenParam.Add ("position", posEnd);
		tweenParam.Add ("easetype", easeType);
		tweenParam.Add ("speed", nSpeed / 3f);
		tweenParam.Add ("oncomplete", "OnFinishMove");
		
		iTween.MoveTo(gameObject, tweenParam);
	}

	void ChangeSize() {
		 iTween.EaseType easeType =  iTween.EaseType.easeInOutSine;
		
		Vector3 scaleChageSize;
		if(bFeverMode || bBossMode || bStartRun || bLastRun) {
			scaleChageSize = new Vector3(nInitScale*GameSettings.nFeverPlayerScaleUp, nInitScale*GameSettings.nFeverPlayerScaleUp, 1f);
		}
		else if(bPowerMode) {
			scaleChageSize = new Vector3(nInitScale*GameSettings.nPowerPlayerScaleUp, nInitScale*GameSettings.nPowerPlayerScaleUp, 1f);
		}
		else {
			scaleChageSize = new Vector3(nInitScale, nInitScale, 1f);
		}
		float nChageSizeTime = GameSettings.nPlayerChangeSizeTime;		
		
		Hashtable tweenParam = new Hashtable();
		tweenParam.Add ("scale", scaleChageSize);
		tweenParam.Add ("easetype", easeType);
		tweenParam.Add ("time", nChageSizeTime);
		
		iTween.ScaleTo(gameObject, tweenParam);
	}
	
	void OnFinishMove() {
		if(bCheckSWA) {		
			bCheckSWA = false;
			//bSWA = true;	// set later
		
			EffectManager.ShowEffect_SWA(posSWA);
		}
	}
	
	public void Attack() {
		if(bDie) {
			return;
		}
		
		if(bDamage) {
			return;
		}
		
		if(bAttack) {
			obAni.Stop();
		}
		
		bAttack = true;
		obAni.Play(GetAniName());		
		obAni.animationCompleteDelegate = OnCompleteAttackAnimation;
	}
	
	public void OnCompleteAttackAnimation(tk2dAnimatedSprite sprite, int clipId) {
		bAttack = false;
		obAni.Play (GetAniName());
	}		
	
	public void Die() {
		if(bDie) {
			return;
		}
		
		iTween.Stop(gameObject);
		obAni.Stop();
		
		bDie = true;
		if(bDieNoHP == false) {
			SetVisible(false);
		}
		
		obAni.Play(GetAniName());		
		obAni.animationCompleteDelegate = OnCompleteDieAnimation;		
		
		EffectManager.ShowEffect_Player_Die(new Vector2(transform.position.x, transform.position.y));
		
		GamePlay.SoundEffect(GamePlay._instance.sndGameOver);
	}
	
  	public void OnCompleteDieAnimation(tk2dAnimatedSprite sprite, int clipId) {
		bDie = false;
		
		if(BoostSettings.bRelay) {
			BoostSettings.bRelay = false;
			Destroy(gameObject);
			
			GamePlay._instance.SpawnRelayPlayer();
			return;
		}
		else if(BoostSettings.bLastRun) {
			SetVisible(true);
			
			BoostSettings.bLastRun = false;
			SetLastRun(true);
			
			MoveToStartPosition();
			return;
		}
		
		Destroy(gameObject);
		GamePlay.OnPlayerDead();
	}
		
	
	public void Damage(float nDamage, Fish obFish) {
		bCheckSWA = false;
		
		if(bDie) {
			return;
		}
		
		if(bLastRun) {
			return;
		}
		
		if(bDamage || bBlink) {
			return;
		}
		
		bool bEscapeFish = false;
		if(BoostSettings.bEscape) {
			bEscapeFish = true;
		}
		else if(bPlayerProperty_Escape) {
			float nValue = Random.Range(0f, 1f);
			if(nValue <  nEscapeRate) {
				bEscapeFish = true;
			}
		}
			
		if(bEscapeFish) {
			Escape(obFish);
			return;
		}			
		
		ReduceHP(nDamage);
		
		if(nHP <= 0) {
			obFish.Win();
			Die();
		}
		else {
			bDamage = true;
			obAni.Play(GetAniName());		
			obAni.animationCompleteDelegate = OnCompleteDamageAnimation;		
	
			SetBlink(true);
			GamePlay.SoundEffect(GamePlay._instance.sndPlayer_Die);
		}
	}
	
    	public void OnCompleteDamageAnimation(tk2dAnimatedSprite sprite, int clipId) {
		bDamage = false;
		obAni.Play (GetAniName());
    	}		
	
	public void SetBlink(bool bSet) {
		bBlink = bSet;
		if(bSet) {
			StartCoroutine("CR_SetBlink");
		}
		else {
			SetVisible(true);
			StopCoroutine("CR_SetBlink");
		}
	}
	
	IEnumerator CR_SetBlink() {
		bBlink = true;
		
		float nBlinkTime = GameSettings.nPlayerBlinkTime;
		float nTime = 0f;
		float nAlpha = 0f;
		
		while (nTime < nBlinkTime) {
			if(GamePlay.nGameStatus != GamePlay.GameStatus.PLAY) {
				yield return null;
				continue;
			}

			nTime += Time.deltaTime;

			nAlpha = 0.2f + (Mathf.Cos(4*Mathf.PI*nTime) + 1) * 0.4f;
			obAni.color = new Color(1f, 1f, 1f, nAlpha);

			yield return null;
		}
		
		obAni.color = new Color(1f, 1f, 1f, 1f);
		bBlink = false;
	}
	
	public string GetAniName() {
		string strName = strAniName;
		
		if(bDie) {
			strName += "_Die";

			if(bDieNoHP) {
				strName += "_NoHP";
			}
			
			return strName;
		}
		
		if(bDamage) {
			strName += "_Damage";
			return strName;
		}
		
		if(bFeverMode || bBossMode || bPowerMode || bStartRun || bLastRun) {
			strName += "_Fever";
		}
		
		if(bAttack) {
			strName += "_Attack";
		}
		
		return strName;
	}
	
	public void OnGetItem(Item.ItemType nItemType)	{
		switch(nItemType) {
		case Item.ItemType.StarStick : 
			SetFeverMode(true);
			break;			
		case Item.ItemType.Magnet : 
			SetMagnetMode(true);
			break;			
		case Item.ItemType.Wing : 
			SetWingMode(true);
			break;			 
		case Item.ItemType.PinkWhale : 
			SetPinkWhaleMode();
			break;			 
		case Item.ItemType.GoldFish : 
			SetGoldFishMode();
			break;			 
		case Item.ItemType.Heart : 
			SetHeartMode();
			break;			 
		}
		
		GamePlay.SoundEffect(GamePlay._instance.sndItem);
	}

	public void ChangeMode() {
		if(bDie) {
			return;
		}
		
		if(bAttack) {
			Attack();
		}
		else {
			obAni.Play(GetAniName());					
		}
	}
	
	public void SetFeverMode(bool bSet) {
		if(bBossMode) {
			bFeverMode = false;
			nFeverTime = 0f;
			
			return;
		}
		
		bFeverMode = bSet;
		if(bFeverMode) {
			nFeverTime = (GameSettings.nFeverTime*BoostSettings.nItemTimeUpRateByPet) + BoostSettings.nItemFeverUpTime;
			GamePlay.obPet.ShowSkill_BoostItemTime();
			
			if(obEffect_FeverMode == null) {
				int nOffsetX = -60;
				Vector2 posEffect = new Vector2(transform.position.x+nOffsetX, transform.position.y);
				obEffect_FeverMode = EffectManager.ShowEffect_StartRun(posEffect);
			}
		}
		else {
			if(obEffect_FeverMode) {
				EffectManager.HideEffect(obEffect_FeverMode);
				obEffect_FeverMode = null;
			}
			
			nFeverTime = 0f;
		}
			
		ChangeSize();
		ChangeMode();
		
		if(bSet) {
			SetBlink(false);			
		}
		else {
			if(!bDieNoHP) {
				SetBlink(true);
			}
		}
	}
	
	public void SetMagnetMode(bool bSet) {
		bMagnetMode = bSet;
		if(bMagnetMode) {
			nMagnetTime = (GameSettings.nMagnetTime*BoostSettings.nItemTimeUpRateByPet) + BoostSettings.nItemMagnetUpTime;
			GamePlay.obPet.ShowSkill_BoostItemTime();
		}
		else {
			nMagnetTime = 0f;
		}
	}
	
	public void SetWingMode(bool bSet) {
		bWingMode = bSet;
		if(bWingMode) {
			nWingTime = (GameSettings.nWingTime*BoostSettings.nItemTimeUpRateByPet);
			GamePlay.obPet.ShowSkill_BoostItemTime();
		}
		else {
			nWingTime = 0f;
		}
	}
	
	public void SetPinkWhaleMode() {
		SpawnManager.SpawnPet(Pet.PetType.dolphin, false);
	}
	
	public void SetGoldFishMode() {
		EffectManager.ShowEffect_Fish_Hunt_Count(new Vector2(transform.position.x, transform.position.y), GameSettings.nItem_GoldFish_HuntScore + BoostSettings.nItemFishUpCount);
//		ScoreManager.AddHuntScore(GameSettings.nItem_GoldFish_HuntScore + BoostSettings.nItemFishUpCount);		
		
		ScoreManager.AddGameScore(GameSettings.nItem_GoldFish_GameScore);
		ScoreManager.AddExpGage(GameSettings.nItem_GoldFish_ExpGage);		
	}
	
	public void SetHeartMode() {
		AddHP(GameSettings.nItem_Heart_HP + BoostSettings.nItemHeartUpHP);
	}
	
	public void UpdateFeverMode() {
		if(bFeverMode) {
			if(obEffect_FeverMode) {
				int nOffsetX = -60;
				EffectManager.UpdateEffectPos(obEffect_FeverMode, new Vector2(transform.position.x+nOffsetX, transform.position.y));
			}			
			
			nFeverTime -= Time.deltaTime;
			if(nFeverTime <= 0f) {
				SetFeverMode(false);
			}
		}
	}

	public void UpdateStartRun() {
		if(bStartRun) {
			if(obEffect_StartRun) {
				int nOffsetX = -60;
				EffectManager.UpdateEffectPos(obEffect_StartRun, new Vector2(transform.position.x+nOffsetX, transform.position.y));
			}
			
			if(nStartRunDistance <= GamePlay.nGameDistance) {
				SetStartRun(false);
			}
		}
	}
	
	public void UpdateLastRun() {
		if(bLastRun) {
			if(obEffect_LastRun) {
				int nOffsetX = -60;
				EffectManager.UpdateEffectPos(obEffect_LastRun, new Vector2(transform.position.x+nOffsetX, transform.position.y));
			}
			
			nLastRunTime -= Time.deltaTime;
			if(nLastRunTime <= 0f) {
				SetLastRun(false);
			}
		}
	}
	
	public void UpdatePowerMode() {
		if(obEffect_PowerMode) {
			int nOffsetX = -60;
			EffectManager.UpdateEffectPos(obEffect_PowerMode, new Vector2(transform.position.x+nOffsetX, transform.position.y));
		}		
	}
	
	public void UpdateMagnetMode() {
		if(bPlayerProperty_Magnet || bBossMode) {
			SetMagnetMode(true);
			return;
		}
		
		if(bMagnetMode) {
			nMagnetTime -= Time.deltaTime;
			if(nMagnetTime <= 0f) {
				SetMagnetMode(false);
			}
		}
	}
	
	public void UpdateWingMode() {
		if(bWingMode) {
			nWingTime -= Time.deltaTime;
			if(nWingTime <= 0f) {
				SetWingMode(false);
			}
		}
	}
	
	public void UpdateMoveToPositionY() {
		if(bDie) {
			return;
		}
		
		if(nMoveToPositionY == -1f || nMoveToPositionY == transform.position.y) {
			return;
		}
		
		float nRate = (15000f / nSpeed) * Time.deltaTime;
		if(nRate>1f) nRate = 1f;
			
		float nY = transform.position.y + (nMoveToPositionY - transform.position.y) * nRate;
		if(Mathf.Abs(nMoveToPositionY - nY) < 0.1f) {
			nY = nMoveToPositionY;
			nMoveToPositionY = -1f;
		}

		transform.position = new Vector3(GameSettings.nPlayerPositionX, nY, transform.position.z);
	}
	
	public int GetLane() {
		float nY = transform.position.y + GameSettings.nPlayLaneHeight/2;
		int nLane = (int)(((nY - GameSettings.nPlayAreaStartPositionY)/GameSettings.nPlayLaneHeight) - 0.5);
		
		if(nLane < 0) nLane = 0;
		else if(nLane > GameSettings.nPlayLaneCount-1) nLane = GameSettings.nPlayLaneCount-1;
		
		return nLane;
	}

	public void CheckSWA(bool bMoveUp) {
		bCheckSWA = false;
		
		RaycastHit obHit;
		float nDistance = GameSettings.nDistanceSWA;
		
		if(Physics.Raycast(transform.position, Vector3.right, out obHit, nDistance) == false) {
			return;
		}
		
		Fish obFish = obHit.collider.gameObject.GetComponent("Fish") as Fish;
		if(obFish) {
			if(nLevel < obFish.nLevel) {
				bCheckSWA = true;

				float nX = (2*transform.position.x + obFish.transform.position.x) / 3;
				posSWA = new Vector2(nX, transform.position.y);
				
				nSWATime += GameSettings.nTimeSWA*obFish.nLevel;
			}
		}
	}

	public void UpdateBubbleEffect() {
		if(bDie) {
			return;
		}
		
		nBubbleEffectTime += Time.deltaTime;
		if(nBubbleEffectTime > GameSettings.nPlayerBubbleEffectTime) {
			nBubbleEffectTime = 0f;
			
			float nOffsetX = -100f;
			if(bFeverMode || bBossMode) {
				nOffsetX *= GameSettings.nFeverPlayerScaleUp;
			}
			else if(bPowerMode) {
				nOffsetX *=  GameSettings.nPowerPlayerScaleUp;
			}
			
			Effect obBubbleEffect = EffectManager.ShowEffect_Player_Bubble(new Vector2(transform.position.x + nOffsetX, transform.position.y));
			
			Vector3 scaleChageSize;
			if(bFeverMode || bBossMode) {
				scaleChageSize = new Vector3(GameSettings.nFeverPlayerScaleUp, GameSettings.nFeverPlayerScaleUp, 1f);
			}
			else if(bPowerMode) {
				scaleChageSize = new Vector3(GameSettings.nPowerPlayerScaleUp, GameSettings.nPowerPlayerScaleUp, 1f);
			}
			else {
				scaleChageSize = new Vector3(nInitScale, nInitScale, 1f);
			}
			
			obBubbleEffect.transform.localScale = scaleChageSize;
		}
	}
	
	public void UpdateSWATime() {
		if(bSWA == false) {
			return;
		}
		
		nSWATime -= Time.deltaTime;
		if(nSWATime <= 0f) {
			nSWATime = 0f;
			bSWA = false;
		}	
	}
	
	void CheckGamePause() {
		if(!bPaused && GamePlay.nGameStatus == GamePlay.GameStatus.PAUSE) {
			bPaused = true;
			iTween.Pause(gameObject);
			obAni.Pause();
		}
		else if(bPaused && GamePlay.nGameStatus == GamePlay.GameStatus.PLAY) {
			bPaused = false;
			iTween.Resume(gameObject);						
			obAni.Resume();
		}
	}	
	
	void UpdateHP() {
		if(bDie || bLastRun) {
			return;
		}
		
		float nReduceHP = Time.deltaTime * 1.5f;
		ReduceHP(nReduceHP);
		if(nHP <= 0f) {
			bDieNoHP = true;
			
			if(bFeverMode) {
				SetFeverMode(false);
			}
			
			if(bPowerMode) {
				SetPowerMode(false);	
			}
			
			Die();
		}
	}
	
	public void ReduceHP(float nReduceHP) {
		nHP -= (nReduceHP * BoostSettings.nDamageDownRateByPlayer);
		if(nHP < 0) {
			nHP = 0f;
		}
	}
	
	public void AddHP(float nAddHP) {
		nHP += nAddHP;
		if(nHP > nFullHP) {
			nHP = nFullHP;
		}
	}
	
	public void SetLevel(int nValue) {
		if(nLevel == nValue) {
			return;
		}
		
		nLevel = nValue;
		if(nLevel < 1) {
			nLevel = 1;
		}
		else if(nLevel > GameSettings.nMaxLevel) {
			nLevel = GameSettings.nMaxLevel;	
		}

		bBossMode = false;

		if(bPowerMode) {
			SetPowerMode(false);
		}
		
		if(nLevel == nPowerModeLevel) {
			SetPowerMode(true);
		}
		else if(nLevel == GameSettings.nMaxLevel) {
			bBossMode = true;
		}
	}
	
	public void SetPowerMode(bool bSet) {
		bPowerMode = bSet;
		if(bPowerMode) {
			if(obEffect_PowerMode == null) {
				int nOffsetX = -60;
				Vector2 posEffect = new Vector2(transform.position.x+nOffsetX, transform.position.y);
				obEffect_PowerMode = EffectManager.ShowEffect_StartRun(posEffect);
			}
		}
		else {
			if(obEffect_PowerMode) {
				EffectManager.HideEffect(obEffect_PowerMode);
				obEffect_PowerMode = null;
			}
		}
			
		ChangeSize();
		ChangeMode();
		
		if(bSet) {
			SetBlink(false);			
		}
		else {
			if(!bDieNoHP) {
				SetBlink(true);
			}
		}
		
		GamePlay.obPet.ShowSkill_BoostBonusTime(bSet);
	}
	
	void SetStartRun(bool bSet) {
		bStartRun = bSet;
		if(bStartRun) {
			nStartRunDistance = GameSettings.nStartRunDistance;
			
			if(obEffect_StartRun == null) {
				int nOffsetX = -60;
				Vector2 posEffect = new Vector2(transform.position.x+nOffsetX, transform.position.y);
				obEffect_StartRun = EffectManager.ShowEffect_StartRun(posEffect);
			}
		}
		else {
			if(obEffect_StartRun) {
				EffectManager.HideEffect(obEffect_StartRun);
				obEffect_StartRun = null;
			}
			
			nStartRunDistance = 0f;
		}
			
		ChangeSize();
		ChangeMode();
		
		if(bSet) {
			SetBlink(false);			
		}
		else {
			if(!bDieNoHP) {
				SetBlink(true);
			}
		}
	}
	
	void SetLastRun(bool bSet) {
		bLastRun = bSet;
		obAni.Play(GetAniName());
		
		if(bLastRun) {
			nLastRunTime = GameSettings.nLastRunTime;
			
			ChangeSize();
			ChangeMode();

			int nOffsetX = -60;
			Vector2 posEffect = new Vector2(transform.position.x+nOffsetX, transform.position.y);
			obEffect_LastRun = EffectManager.ShowEffect_LastRun(posEffect);
		}
		else {
			if(obEffect_LastRun) {
				EffectManager.HideEffect(obEffect_LastRun);
				obEffect_LastRun = null;
			}
			
			nLastRunTime = 0f;
		
			// LastRun 이후에 죽는 것은 HP 가 없 어서 죽는 것으로 처리. 애니메이션 적용.
			bDieNoHP = true;
			Die();
		}
	}
	
	void Escape(Fish obFish)
	{
		Vector2 posEffect = new Vector2(transform.position.x, transform.position.y);
		EffectManager.ShowEffect_Ink(posEffect);

		// Move Player to another line
		int nLane = 0;
		if(GamePlay.nPlayerLane == 0) {
			nLane = 1;
		}
		else if(GamePlay.nPlayerLane == GameSettings.nPlayLaneCount - 1) {
			nLane = GameSettings.nPlayLaneCount - 2;
		}
		else {
			int nValue = Random.Range(0, 2);
			
			if(nValue == 0) nLane = GamePlay.nPlayerLane - 1;
			else nLane = GamePlay.nPlayerLane + 1;
		}
		
		MoveToLane(nLane);
		
		// Blink
		SetBlink(true);
	}
	
	void SetVisible(bool bVisible) {
		if(bVisible) {
			obAni.color = new Color(1f, 1f, 1f, 1f);
		}
		else {
			obAni.color = new Color(1f, 1f, 1f, 0f);
		}			
	}
}
