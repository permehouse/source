using UnityEngine;
using System.Collections;

public class Player_Octopus : Player {
	// Use this for initialization
	public override void Start () {
		strAniName = "Player_Octopus";
		nType = Player.PlayerType.octopus;
		nMoveType = MoveType.speedy;

		SetProperty(PlayerSettings.obOctopus);
		
		base.Start();			
	}
}
