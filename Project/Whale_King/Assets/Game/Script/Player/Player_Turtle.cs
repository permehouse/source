using UnityEngine;
using System.Collections;

public class Player_Turtle : Player {
	// Use this for initialization
	public override void Start () {
		strAniName = "Player_Turtle";
		nType = Player.PlayerType.turtle;
		nMoveType = MoveType.speedy;

		SetProperty(PlayerSettings.obTurtle);
		
		base.Start();			
	}
}
