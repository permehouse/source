using UnityEngine;
using System.Collections;

public class Effect : MonoBehaviour {
	public enum EffectType {
		SWA,
		fish_die,
		fish_hunted,
		rush,
		player_bubble,
		player_die,
		bg_bubble,
		bg_flow_bubble,
		combo,
		possess_fish,
		ink,
		startrun,
		lastrun,
		fish_digest_count,
		fish_hunt_count,
		item_appear
	}
	
	public EffectType nType = EffectType.SWA;
	public float nParam;
	bool bPaused = false;
	
	void Awake() {
		
	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		CheckGamePause();
		if(GamePlay.nGameStatus != GamePlay.GameStatus.PLAY) {
			return;
		}		
	}
	
	void CheckGamePause() {
		if(!bPaused && GamePlay.nGameStatus == GamePlay.GameStatus.PAUSE) {
			bPaused = true;
			iTween.Pause(gameObject);
			
			if(nType != EffectType.bg_flow_bubble) {
			 	tk2dAnimatedSprite obAni = GetComponent<tk2dAnimatedSprite>();
				if(obAni) {
					obAni.Pause();
				}
			}
		}
		else if(bPaused && GamePlay.nGameStatus == GamePlay.GameStatus.PLAY) {
			bPaused = false;
			iTween.Resume(gameObject);						
			
			if(nType != EffectType.bg_flow_bubble) {
			 	tk2dAnimatedSprite obAni = GetComponent<tk2dAnimatedSprite>();
				if(obAni) {
					obAni.Resume();
				}
			}
		}
	}		
}
