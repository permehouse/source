using UnityEngine;
using System.Collections;

public class Pet_LittleShark : Pet {
	// Use this for initialization
	void Start () {
		strAniName = "Pet_LittleShark";
		nType = PetType.littleshark;
		nMoveType = MoveType.trace;
		
		SetProperty(PetSettings.obLittleShark);
		
		MoveToPlayer();		
	}
}
