using UnityEngine;
using System.Collections;

public class Pet_Mermaid : Pet {
	// Use this for initialization
	void Start () {
		strAniName = "Pet_Mermaid";
		nType = PetType.mermaid;
		nMoveType = MoveType.trace;
		
		SetProperty(PetSettings.obMermaid);
		
		MoveToPlayer();		
	}
}
