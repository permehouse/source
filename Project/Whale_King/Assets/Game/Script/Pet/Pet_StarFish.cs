using UnityEngine;
using System.Collections;

public class Pet_StarFish : Pet {
	// Use this for initialization
	void Start () {
		strAniName = "Pet_Starfish";
		nType = PetType.starfish;
		nMoveType = MoveType.trace;
		
		SetProperty(PetSettings.obStarFish);
		
		MoveToPlayer();		
	}
}
