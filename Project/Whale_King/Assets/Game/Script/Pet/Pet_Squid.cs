using UnityEngine;
using System.Collections;

public class Pet_Squid : Pet {
	// Use this for initialization
	void Start () {
		strAniName = "Pet_Squid";
		nType = PetType.squid;
		nMoveType = MoveType.trace;
		
		SetProperty(PetSettings.obSquid);
		
		MoveToPlayer();		
	}
}
