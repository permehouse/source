using UnityEngine;
using System.Collections;

public class Pet_HammerShark : Pet {
	// Use this for initialization
	void Start () {
		strAniName = "Pet_HammerShark";
		nType = PetType.hammershark;
		nMoveType = MoveType.trace;

		SetProperty(PetSettings.obHammerShark);
		
		MoveToPlayer();
	}
}
