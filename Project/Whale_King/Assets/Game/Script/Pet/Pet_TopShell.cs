using UnityEngine;
using System.Collections;

public class Pet_TopShell : Pet {
	// Use this for initialization
	void Start () {
		strAniName = "Pet_TopShell";
		nType = PetType.topshell;
		nMoveType = MoveType.trace;
		
		SetProperty(PetSettings.obTopShell);
		
		MoveToPlayer();		
	}
}
