using UnityEngine;
using System.Collections;

public class Pet : MonoBehaviour {
	public enum PetType {
		dolphin,
		hammershark,
		littleshark,
		mermaid,
		jellyfish,
		walrus,
		topshell,
		starfish,
		squid,
		ray,
		penguin,
		clam,
		
		none = -1
	}
	
	public enum MoveType {
		simple,
		speedy,
		trace,
	}
	
	public int nGrade = GameSettings.nGrade_C;
	public int nLevel = 0;
	public PetType nType = PetType.dolphin;	
	protected string strAniName = null;
	public PetSettings.Property obInitProp;
	
	protected MoveType nMoveType = MoveType.simple;
//	protected int nMoveToLane = -1;
	protected float nMoveToPositionY = -1f;
 	protected float nSpeed = GameSettings.nPetMoveSpeed;
	protected int nEndPositionX = GameSettings.nPlayAreaEndPositionX;
	protected Vector3 posEnd;	

	protected float nInitScale = 1f;
	
	tk2dAnimatedSprite obAni;

	protected bool bAttack = false;
	
	public bool bPetProperty_Rush = false;	
	public bool bPetProperty_Attack = false;	
	public bool bPetProperty_HuntFish = false;	
	public bool bPetProperty_PossessFish = false;	
	public bool bPetProperty_Giant = false;	
	public bool bPetProperty_MakeItem = false;	
	public bool bPetProperty_FillHP = false;	
	public bool bPetProperty_BoostExp = false;	
	public bool bPetProperty_BoostItemTime = false;	
	public bool bPetProperty_BoostBonusTime = false;	

	// AttackFish
	public float nAttackRate = 0;
	public int nAttackLevel = 0;
	
	// Rush
	public bool bRush = false;
	protected float nRushCoolTime = 0;
	protected Effect obRushEffect = null;
	protected int nRushEffectOffsetX = 0;
	protected int nRushEffectOffsetY = 0;
	protected float nRushScale = 1f;

	// PossessFish
	public bool bPossessFish = false;
	protected float nPossessFishCoolTime = 0;
	public int nPossessFishLevel = 0;
	protected float nPossessFishScale = 1f;
	
	// HuntFish
	public bool bHuntFish = false;
	protected int nHuntFishScore = 0;
	protected float nHuntFishCoolTime = 0;
	protected float nHuntFishScale = 1f;
	
	// Giant
	public bool bGiant = false;
	protected float nGiantCoolTime = 0;
	protected float nGiantTime = 0;
	protected float nGiantScale = 1f;
	protected float nGiantDistance = 0f;

	// MakeItem
	public bool bMakeItem = false;
	protected float nMakeItemCoolTime = 0;
	protected float nMakeItemScale = 1f;

	// FillHP
	public bool bFillHP = false;
	protected float nFillHPCoolTime = 0;
	protected float nFillHPScale = 1f;
	
	// bBoostItemTime
	public bool bBoostItemTime = false;
	protected float nBoostItemTimeScale = 1f;

	// bBoostBonusTime
	public bool bBoostBonusTime = false;
	protected float nBoostBonusTimeScale = 1f;
	
	bool bPaused = false;	
	
	void Awake() {
		obAni = GetComponent<tk2dAnimatedSprite>();
	}
	
	public void SetProperty(PetSettings.Property obProp) {
		obInitProp = obProp;
		
		nGrade = obProp.nGrade;
		nLevel = obProp.nLevel;
		nSpeed *= obProp.nSpeedRate;
		nInitScale = obProp.nInitScale;
		
		bPetProperty_Attack = obProp.bAttack;	
		if(bPetProperty_Attack) {
			nAttackRate = obProp.GetAttackRate();
			nAttackLevel = obProp.GetAttackLevel();
		}

		bPetProperty_Rush = obProp.bRush;	
		if(bPetProperty_Rush) {
			nRushCoolTime = obProp.GetRushCoolTime();
			nRushScale = obProp.nRushScale;
			nRushEffectOffsetX = obProp.nRushEffectOffsetX;
		}
		
		bPetProperty_PossessFish = obProp.bPossessFish;	
		if(bPetProperty_PossessFish) {
			nPossessFishCoolTime = obProp.GetPossessFishCoolTime();
			nPossessFishLevel = obProp.GetPossessFishLevel();
			nPossessFishScale = obProp.nPossessFishScale;
		}
		
		bPetProperty_HuntFish = obProp.bHuntFish;	
		if(bPetProperty_HuntFish) {
			nHuntFishCoolTime = obProp.GetHuntFishCoolTime();
			nHuntFishScore = obProp.GetHuntFishScore();
			nHuntFishScale = obProp.nHuntFishScale;
		}

		bPetProperty_Giant = obProp.bGiant;
		if(bPetProperty_Giant) {
			nGiantCoolTime = obProp.GetGiantCoolTime();
			nGiantTime = obProp.GetGiantTime();
			nGiantScale = obProp.nGiantScale;
			nGiantDistance = obProp.nGiantDistance;
		}
		
		bPetProperty_MakeItem = obProp.bMakeItem;	
		if(bPetProperty_MakeItem) {
			nMakeItemCoolTime = obProp.GetMakeItemCoolTime();
			nMakeItemScale = obProp.nMakeItemScale;
		}
		
		bPetProperty_FillHP = obProp.bFillHP;	
		if(bPetProperty_FillHP) {
			nFillHPCoolTime = obProp.GetFillHPCoolTime();
			nFillHPScale = obProp.nFillHPScale;
		}
		
		bPetProperty_BoostExp = obProp.bBoostExp;
		if(bPetProperty_BoostExp) {
			BoostSettings.SetPetBoostExpRate(obProp.GetBoostExpRate());
		}
		
		bPetProperty_BoostItemTime = obProp.bBoostItemTime;
		if(bPetProperty_BoostItemTime) {
			BoostSettings.SetPetBoostItemTimeRate(obProp.GetBoostItemTimeRate());
			nBoostItemTimeScale = obProp.nBoostItemTimeScale;
		}

		bPetProperty_BoostBonusTime = obProp.bBoostBonusTime;
		if(bPetProperty_BoostBonusTime) {
			BoostSettings.SetPetBoostBonusTimeRate(obProp.GetBoostBonusTimeRate());
			nBoostBonusTimeScale = obProp.nBoostBonusTimeScale;
		}
		
		transform.localScale = new Vector3(nInitScale, nInitScale, 1f);
	}
	
	// Update is called once per frame
	public void Update() {
		CheckGamePause();
		if(GamePlay.nGameStatus != GamePlay.GameStatus.PLAY) {
			return;
		}
		if(GamePlay.obPlayer.bDie) {
			return;
		}
		
		CheckRush();
		CheckHuntFish();
		CheckPossessFish();
		CheckGiant();
		CheckMakeItem();
		CheckFillHP();
	}

	public void Move() {
		if(bRush) {
			return;
		}
		
		 iTween.EaseType easeType = iTween.EaseType.linear;
		switch(nMoveType) {
		case MoveType.simple : easeType = iTween.EaseType.linear; break;
		case MoveType.speedy : easeType = iTween.EaseType.easeOutCubic; break;
		}
		
		float nMoveSpeed = nSpeed * GamePlay.nGameSpeedChangeRate;
		
		Hashtable tweenParam = new Hashtable();
		tweenParam.Add ("position", posEnd);
		tweenParam.Add ("easetype", easeType);
		tweenParam.Add ("speed", nMoveSpeed);
		tweenParam.Add ("oncomplete", "OnFinishMove");
		
		iTween.MoveTo(gameObject, tweenParam);
	}	
	
	public void Rush() {
		if(bRush) {
			return;
		}
		
		bRush = true;
		obAni.Play(GetAniName());		
		
		ChangeSize();
		
		Vector3 posEnd = new Vector3(nEndPositionX, transform.position.y, transform.position.z);
		 iTween.EaseType easeType = iTween.EaseType.easeOutCubic;
		float nMoveSpeed = 2f * nSpeed * GamePlay.nGameSpeedChangeRate;
		
		Hashtable tweenParam = new Hashtable();
		tweenParam.Add ("position", posEnd);
		tweenParam.Add ("easetype", easeType);
		tweenParam.Add ("speed", nMoveSpeed);
		tweenParam.Add ("oncomplete", "OnFinishRush");
		
		iTween.MoveTo(gameObject, tweenParam);
		
		obRushEffect = EffectManager.ShowEffect_Rush(new Vector2(transform.position.x + nRushEffectOffsetX, transform.position.y + nRushEffectOffsetY));
	}	
	
	public void HuntFish() {
		if(bHuntFish) {
			return;
		}

		bHuntFish = true;
		obAni.Play(GetAniName());		
		obAni.animationCompleteDelegate = OnCompleteHuntFishAnimation;	
		
		ChangeSize();
	}	
	
	public void OnCompleteHuntFishAnimation(tk2dAnimatedSprite sprite, int clipId) {
		bHuntFish = false;
		obAni.Play (GetAniName());		
		
//  숫자가 날아가는 효과로 변경. 효과가 끝나면 HuntScore 가 올라감.
//		ScoreManager.AddHuntScore(nHuntFishScore);
		
		ChangeSize();
	}						
	
	public void PossessFish() {
		if(bPossessFish) {
			return;
		}

		bPossessFish = true;
		
		obAni.Play(GetAniName());		
		obAni.animationCompleteDelegate = OnCompletePossessFishAnimation;	
		
		ChangeSize();
	}	
	
	
	public void OnCompletePossessFishAnimation(tk2dAnimatedSprite sprite, int clipId) {
		bPossessFish = false;
		obAni.Play (GetAniName());		
		
		ChangeSize();
	}						

	public void Giant(bool bSet) {
		bGiant = bSet;
		
		obAni.Play(GetAniName());		
		obAni.CurrentClip.wrapMode = tk2dSpriteAnimationClip.WrapMode.Loop;
			
		ChangeSize();
		ChangePos();
	}
	
	public void MakeItem() {
		if(bMakeItem) {
			return;
		}

		bMakeItem = true;
		obAni.Play(GetAniName());		
		obAni.animationCompleteDelegate = OnCompleteMakeItemAnimation;	
		
		ChangeSize();
	}	
	
	public void OnCompleteMakeItemAnimation(tk2dAnimatedSprite sprite, int clipId) {
		// Set Item's Position.
		// Item will be shown after effect has disappeared.
		float nX = Random.Range(GameSettings.nItemAppearStartX, GameSettings.nItemAppearEndX);
		int nLane = Random.Range(0, GameSettings.nPlayLaneCount);
		int nY= GameSettings.listPlayLaneCenterY[nLane];			
		EffectManager.ShowEffect_Item_Appear(new Vector2(nX, nY));
		
		bMakeItem = false;
		obAni.Play (GetAniName());		
		
		ChangeSize();
	}						
	
	public void FillHP() {
		if(bFillHP) {
			return;
		}

		bFillHP = true;
		obAni.Play(GetAniName());		
		obAni.animationCompleteDelegate = OnCompleteFillHPAnimation;	
		
		ChangeSize();
	}	
	
	public void OnCompleteFillHPAnimation(tk2dAnimatedSprite sprite, int clipId) {
		float nHP = GamePlay.obPlayer.nFullHP * obInitProp.GetFillHPRate();
		GamePlay.obPlayer.AddHP(nHP);
		
		bFillHP = false;
		obAni.Play (GetAniName());		
		
		ChangeSize();
	}						

	public void MoveToLane(int nLane) {
		if(bRush) {
			return;
		}
		
		if(bGiant) {
			return;
		}
		
		if(nLane < 0) nLane = 0;
		else if(nLane > GameSettings.nPlayLaneCount-1) nLane = GameSettings.nPlayLaneCount-1;
		
		int nY = GameSettings.listPlayLaneCenterY[nLane];
		Vector3 posEnd = new Vector3(GameSettings.nPetPositionX, nY, transform.position.z);

		 iTween.EaseType easeType = iTween.EaseType.linear;
		switch(nMoveType) {
		case MoveType.simple : easeType = iTween.EaseType.linear; break;
		case MoveType.speedy : easeType = iTween.EaseType.easeOutSine; break;
		case MoveType.trace : easeType = iTween.EaseType.linear; break;
		}
		
		Hashtable tweenParam = new Hashtable();
		tweenParam.Add ("position", posEnd);
		tweenParam.Add ("easetype", easeType);
		tweenParam.Add ("speed", nSpeed);
		
		iTween.MoveTo(gameObject, tweenParam);

		// Stop to move to position Y
		nMoveToPositionY = -1f;
	}
	
	public void MoveToPositionY(float nY) {
		if(nY < 0) {
			nY = 0;
		} 
		else if(nY > GameSettings.nPlayAreaStartPositionY + GameSettings.nPlayAreaHeight) {
			nY = GameSettings.nPlayAreaStartPositionY + GameSettings.nPlayAreaHeight;
		}
		
		nMoveToPositionY = nY;
	}
	
	public void MoveToPlayer() {
		float nX = GameSettings.nScreenPositionX;
		float nY = GamePlay.obPlayer.transform.position.y;
		transform.position = new Vector3(nX, nY, transform.position.z);

		MoveToLane(GamePlay.nPlayerLane);
	}
	
	void OnFinishMove() {
		Destroy(gameObject);
	}

	void OnFinishRush() {
		bRush = false;
		obAni.Play(GetAniName());		
		
		if(obRushEffect) {
			EffectManager.HideEffect(obRushEffect);
			obRushEffect = null;
		}
		
		ChangeSize();
		MoveToPlayer();
	}
	
	void ChangeSize() {
		Vector3 scaleChageSize;
		if(bRush) scaleChageSize = new Vector3(nRushScale, nRushScale, 1f);
		else if(bHuntFish) scaleChageSize = new Vector3(nHuntFishScale, nHuntFishScale, 1f);
		else if(bPossessFish) scaleChageSize = new Vector3(nPossessFishScale, nPossessFishScale, 1f);
		else if(bGiant) scaleChageSize = new Vector3(nGiantScale, nGiantScale, 1f);
		else if(bMakeItem) scaleChageSize = new Vector3(nMakeItemScale, nMakeItemScale, 1f);
		else if(bFillHP) scaleChageSize = new Vector3(nFillHPScale, nFillHPScale, 1f);
		else if(bBoostItemTime) scaleChageSize = new Vector3(nBoostItemTimeScale, nBoostItemTimeScale, 1f);
		else if(bBoostBonusTime) scaleChageSize = new Vector3(nBoostBonusTimeScale, nBoostBonusTimeScale, 1f);
		else 	scaleChageSize = new Vector3(nInitScale, nInitScale, 1f);
		
		 iTween.EaseType easeType =  iTween.EaseType.linear;
		float nChageSizeTime = GameSettings.nPetChangeSizeTime;		
		
		Hashtable tweenParam = new Hashtable();
		tweenParam.Add ("scale", scaleChageSize);
		tweenParam.Add ("easetype", easeType);
		tweenParam.Add ("time", nChageSizeTime);
		tweenParam.Add ("oncomplete", "OnFinishChangeSize");
		
		iTween.ScaleTo(gameObject, tweenParam);
	}
	
	void OnFinishChangeSize() {
		if(bHuntFish) {
			EffectManager.ShowEffect_Fish_Hunt_Count(new Vector2(transform.position.x, transform.position.y), nHuntFishScore);
//			EffectManager.ShowEffect_Fish_Hunted(new Vector2(transform.position.x, transform.position.y));
		}
		
		if(bPossessFish) {
			EffectManager.ShowEffect_Possess_Fish(new Vector2(transform.position.x, transform.position.y));
		}
	}

	void ChangePos() {
		 iTween.EaseType easeType = iTween.EaseType.linear;
		
		float nMoveSpeed = nSpeed * GamePlay.nGameSpeedChangeRate;
		
		Vector3 pos;
		if(bGiant) {
			pos = new Vector3(GameSettings.nScreenWidth/2, GameSettings.nPlayAreaStartPositionY + GameSettings.nPlayAreaHeight/2, transform.position.z);
			Hashtable tweenParam = new Hashtable();
			tweenParam.Add ("position", pos);
			tweenParam.Add ("easetype", easeType);
			tweenParam.Add ("speed", nMoveSpeed);
			
			iTween.MoveTo(gameObject, tweenParam);
		}
		else {
			MoveToLane(GamePlay.nPlayerLane);
//			int nY = GameSettings.listPlayLaneCenterY[GamePlay.nPlayerLane];
//			pos = new Vector3(GameSettings.nScreenWidth/2, nY, transform.position.z);
		}
	}
	
	public virtual void SetMovePattern(Pet pfPet) {
	}
	
	public void Attack() {
		if(bRush) {
			return;
		}
		
		if(bAttack) {
			obAni.Stop();
		}
		
		bAttack = true;
		obAni.Play(GetAniName());		
		obAni.animationCompleteDelegate = OnCompleteAttackAnimation;
	}
	
    public void OnCompleteAttackAnimation(tk2dAnimatedSprite sprite, int clipId) {
		bAttack = false;
		obAni.Play (GetAniName());
    }		
	
/*	
	public void Die() {
		if(bDie) {
			return;
		}
		
		bDie = true;
		obAni.Play(GetAniName());		
		obAni.animationCompleteDelegate = OnCompleteDieAnimation;		
		
		GamePlay.SoundEffect(GamePlay._instance.sndPlayer_Die);
	}
	
    public void OnCompleteDieAnimation(tk2dAnimatedSprite sprite, int clipId) {
		bDie = false;
		
		nLifeCount--;
		if(nLifeCount > 0) {
			obAni.Play (GetAniName());
		}
		else {
			Destroy(gameObject);
			GamePlay.OnPlayerDead();
		}
    }
*/	
	public virtual string GetAniName() {
		string strName = strAniName;

/*		
		if(bDie) {
			strName += "_Die";
			return strName;
		}
		
		if(bFeverMode) {
			strName += "_Fever";
		}
		else if(bPowerMode) {
			strName += "_Power";
		}
*/		
		if(bRush) {
			strName += "_Rush";
		}
		else 	if(bAttack) {
			strName += "_Attack";
		}
		else if(bHuntFish || bPossessFish || bGiant || bMakeItem || bFillHP || bBoostItemTime || bBoostBonusTime) {
			strName += "_Skill";
		}

//		Debug.Log("Ani : " + strName);
		
		return strName;
	}
/*	
	public void OnGetItem(Item.ItemType nItemType)	{
		switch(nItemType) {
		case Item.ItemType.StarStick : 
			SetFeverMode(true);
			break;			
		case Item.ItemType.Magnet : 
			SetMagnetMode(true);
			break;			
		case Item.ItemType.Wing : 
			SetWingMode(true);
			break;			 
		}
		
		GamePlay.SoundEffect(GamePlay._instance.sndItem);
	}
*/
/*	public void ChangeMode() {
		if(bAttack) {
			Attack();
		}
		else {
			obAni.Play(GetAniName());					
		}
	}
*/
/*	
	public void SetPowerMode(bool bSet) {
		bPowerMode = bSet;
		if(bPowerMode) {
			GamePlay.nLevelGage = 100f;
		}
		else {
			GamePlay.nLevelGage = 0f;
		}
			
		ChangeMode();
	}
	
	public void SetFeverMode(bool bSet) {
		bFeverMode = bSet;
		if(bFeverMode) {
			nFeverTime = 	GameSettings.nFeverTime;
		}
		else {
			nFeverTime = 0f;
		}
			
		ChangeSize();
		ChangeMode();
	}
	
	public void SetMagnetMode(bool bSet) {
		bMagnetMode = bSet;
		if(bMagnetMode) {
			nMagnetTime = GameSettings.nMagnetTime; 
		}
		else {
			nMagnetTime = 0f;
		}
	}
	
	public void SetWingMode(bool bSet) {
		bWingMode = bSet;
		if(bWingMode) {
			nWingTime = GameSettings.nWingTime; 
		}
		else {
			nWingTime = 0f;
		}
	}
	
	public void UpdateFeverMode() {
		if(bFeverMode) {
			nFeverTime -= Time.deltaTime;
			if(nFeverTime <= 0f) {
				SetFeverMode(false);
			}
		}
	}

	public void UpdatePowerMode() {
		if(bFeverMode) {
			return;
		}
			
		if(bPowerMode) {
			GamePlay.nLevelGage -= GamePlay.nLevelGageDownSpeed * Time.deltaTime;
				
			if(GamePlay.nLevelGage <= 0f) {
				SetPowerMode(false);
			}
		}
	}
	
	public void UpdateMagnetMode() {
		if(bPlayerProperty_Magnet) {
			SetMagnetMode(true);
			return;
		}
		
		if(bMagnetMode) {
			nMagnetTime -= Time.deltaTime;
			if(nMagnetTime <= 0f) {
				SetMagnetMode(false);
			}
		}
	}
	
	public void UpdateWingMode() {
		if(bWingMode) {
			nWingTime -= Time.deltaTime;
			if(nWingTime <= 0f) {
				SetWingMode(false);
			}
		}
	}
*/	
	
	public void UpdateMoveToPositionY() {
		if(GamePlay.nGameStatus != GamePlay.GameStatus.PLAY) {
			return;
		}
		
		if(nMoveToPositionY == -1f || nMoveToPositionY == transform.position.y) {
			return;
		}
		
		float nRate = (4000f / nSpeed) * Time.deltaTime;
		if(nRate>1f) nRate = 1f;
			
		float nY = transform.position.y + (nMoveToPositionY - transform.position.y) * nRate;
		if(Mathf.Abs(nMoveToPositionY - nY) < 0.1f) {
			nY = nMoveToPositionY;
			nMoveToPositionY = -1f;
		}

		transform.position = new Vector3(GameSettings.nPlayerPositionX, nY, transform.position.z);
	}
	
	public int GetLane() {
		float nY = transform.position.y + GameSettings.nPlayLaneHeight/2;
		int nLane = (int)(((nY - GameSettings.nPlayAreaStartPositionY)/GameSettings.nPlayLaneHeight) - 0.5);
		
		if(nLane < 0) nLane = 0;
		else if(nLane > GameSettings.nPlayLaneCount-1) nLane = GameSettings.nPlayLaneCount-1;
		
		return nLane;
	}
	
	void CheckRush() {
		if(bPetProperty_Rush) {
			if(bRush) {
				if(obRushEffect) {
					EffectManager.UpdateEffectPos(obRushEffect, new Vector2(transform.position.x + nRushEffectOffsetX, transform.position.y + nRushEffectOffsetY));
				}
			}
			else {
				nRushCoolTime -= Time.deltaTime;
				if(nRushCoolTime < 0) {
					nRushCoolTime = obInitProp.GetRushCoolTime();
					Rush();
				}
			}
		}
	}

	void CheckHuntFish() {
		if(bPetProperty_HuntFish) {
			nHuntFishCoolTime -= Time.deltaTime;
			if(nHuntFishCoolTime < 0) {
				nHuntFishCoolTime = obInitProp.GetHuntFishCoolTime();
				HuntFish();
			}
		}
	}
	
	void CheckPossessFish() {
		if(bPetProperty_PossessFish) {
			nPossessFishCoolTime -= Time.deltaTime;
			if(nPossessFishCoolTime < 0) {
				nPossessFishCoolTime = obInitProp.GetPossessFishCoolTime();
				PossessFish();
			}
		}
	}
	
	void CheckGiant() {
		if(bPetProperty_Giant) {
			if(bGiant) {
				nGiantTime -= Time.deltaTime;
				if(nGiantTime < 0) {
					nGiantTime = obInitProp.GetGiantTime();
					Giant(false);
				}
			}
			else {
				nGiantCoolTime -= Time.deltaTime;
				if(nGiantCoolTime < 0) {
					nGiantCoolTime = obInitProp.GetGiantCoolTime();
					Giant(true);
				}
			}
		}
	}

	void CheckMakeItem() {
		if(bPetProperty_MakeItem) {
			nMakeItemCoolTime -= Time.deltaTime;
			if(nMakeItemCoolTime < 0) {
				nMakeItemCoolTime = obInitProp.GetMakeItemCoolTime();
				MakeItem();
			}
		}
	}
	
	void CheckFillHP() {
		if(bPetProperty_FillHP) {
			nFillHPCoolTime -= Time.deltaTime;
			if(nFillHPCoolTime < 0) {
				nFillHPCoolTime = obInitProp.GetFillHPCoolTime();
				FillHP();
			}
		}
	}
	
	void CheckGamePause() {
		if(!bPaused && GamePlay.nGameStatus == GamePlay.GameStatus.PAUSE) {
			bPaused = true;
			iTween.Pause(gameObject);
			obAni.Pause();
		}
		else if(bPaused && GamePlay.nGameStatus == GamePlay.GameStatus.PLAY) {
			bPaused = false;
			iTween.Resume(gameObject);						
			obAni.Resume();
		}
	}
	
	public void ShowSkill_BoostItemTime()
	{
		if(bPetProperty_BoostItemTime) {		
			bBoostItemTime = true;
			obAni.Play(GetAniName());		
			obAni.animationCompleteDelegate = OnCompleteBoostItemTimeAnimation;	
			
			ChangeSize();
		}
	}	
	
	public void OnCompleteBoostItemTimeAnimation(tk2dAnimatedSprite sprite, int clipId) {
		bBoostItemTime = false;
		obAni.Play(GetAniName());		
		
		ChangeSize();
	}
	
	public void ShowSkill_BoostBonusTime(bool bSet)	{
		if(bPetProperty_BoostBonusTime) {		
			bBoostBonusTime = bSet;
			obAni.Play(GetAniName());		
			obAni.CurrentClip.wrapMode = tk2dSpriteAnimationClip.WrapMode.Loop;
			
			ChangeSize();
		}
	}	
}
