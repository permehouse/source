using UnityEngine;
using System.Collections;

public class Pet_Clam : Pet {
	// Use this for initialization
	void Start () {
		strAniName = "Pet_Clam";
		nType = PetType.clam;
		nMoveType = MoveType.trace;
		
		SetProperty(PetSettings.obClam);
		
		MoveToPlayer();		
	}
}
