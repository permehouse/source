using UnityEngine;
using System.Collections;

public class Pet_Dolphin : Pet {
	// Use this for initialization
	void Start () {
		strAniName = "Pet_Dolphin";
		nType = PetType.dolphin;
		nMoveType = MoveType.speedy;
		
		SetProperty(PetSettings.obDolphin);
		
		posEnd = new Vector3(nEndPositionX, transform.position.y, transform.position.z);
		Move();
	}
	
	public override void SetMovePattern(Pet pfPet) {
		base.SetMovePattern(pfPet);
		
		int nLane = 0;
		float nY= GameSettings.listPlayLaneCenterY[nLane];
		float nX = GameSettings.nPlayAreaStartPositionX;
		Vector3 posSpawn = new Vector3(nX, nY, transform.position.z);
		transform.position = posSpawn;
	
		for (int i = 0; i < 3; i++) {
			nLane++;
			nY= GameSettings.listPlayLaneCenterY[nLane];
			posSpawn = new Vector3(nX, nY, transform.position.z + nLane);
			Pet obPet = Instantiate(pfPet, posSpawn, Quaternion.identity) as Pet;
			obPet.nLevel = nLevel;
		}
	}
}
