using UnityEngine;
using System.Collections;

public class Pet_Ray : Pet {
	// Use this for initialization
	void Start () {
		strAniName = "Pet_Ray";
		nType = PetType.ray;
		nMoveType = MoveType.trace;
		
		SetProperty(PetSettings.obRay);
		
		MoveToPlayer();		
	}
}
