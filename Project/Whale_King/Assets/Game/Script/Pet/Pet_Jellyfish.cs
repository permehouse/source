using UnityEngine;
using System.Collections;

public class Pet_Jellyfish : Pet {
	// Use this for initialization
	void Start () {
		strAniName = "Pet_Jellyfish";
		nType = PetType.jellyfish;
		nMoveType = MoveType.trace;
		
		SetProperty(PetSettings.obJellyfish);
		
		MoveToPlayer();
	}
}
