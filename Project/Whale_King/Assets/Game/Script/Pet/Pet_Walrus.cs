using UnityEngine;
using System.Collections;

public class Pet_Walrus : Pet {
	// Use this for initialization
	void Start () {
		strAniName = "Pet_Walrus";
		nType = PetType.walrus;
		nMoveType = MoveType.trace;
		
		SetProperty(PetSettings.obWalrus);
		
		MoveToPlayer();
	}
}
