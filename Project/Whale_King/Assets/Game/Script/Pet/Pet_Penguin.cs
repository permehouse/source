using UnityEngine;
using System.Collections;

public class Pet_Penguin : Pet {
	// Use this for initialization
	void Start () {
		strAniName = "Pet_Penguin";
		nType = PetType.penguin;
		nMoveType = MoveType.trace;
		
		SetProperty(PetSettings.obPenguin);
		
		MoveToPlayer();		
	}
}
