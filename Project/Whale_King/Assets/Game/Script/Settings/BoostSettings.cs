using UnityEngine;
using System.Collections;

public class BoostSettings : MonoBehaviour {
	static public float nFishUpRate = 1f;
	static public float nHPUpAmount = 0f;
	static public float nItemFishUpCount = 0f;
	static public float nItemFeverUpTime = 0f;
	static public float nItemMagnetUpTime = 0f;
	static public float nItemHeartUpHP = 0f;
	static public float nDigestUpRate = 0f;
	static public bool bStartRun = false;
	static public bool bEscape = false;
	static public bool bLastRun = false;
	static public bool bRelay = false;

	// boost by pet
	static public float nExpUpRateByPet = 1f;			// multiple by Exp
	static public float nItemTimeUpRateByPet = 1f;	// multiple by Time
	static public float nBonusTimeUpRateByPet = 1f;	// mutitple by Time
	
	// boost by player
	
	static public float nExpUpRateByPlayer = 1f;			// multiple by Exp
	static public float nFastDigestRateByPlayer = 0f;		// add to Digest rate
	static public float nDamageDownRateByPlayer = 1f;	// multiple by Damage

	
	static public void SetPlayerBoost() {
		// for Test
		GlobalValues.gameBoost.nLevel_FishUpRate = 1;
		GlobalValues.gameBoost.nLevel_HPUpAmount = 1;
		GlobalValues.gameBoost.nLevel_ItemFishUpCount = 1;
		GlobalValues.gameBoost.nLevel_ItemFeverUpTime = 1;
		GlobalValues.gameBoost.nLevel_ItemMagnetUpTime = 1;
		GlobalValues.gameBoost.nLevel_ItemHeartUpHP = 1;
		GlobalValues.gameBoost.nCount_StartRun = 0;
		GlobalValues.gameBoost.nCount_Escape = 0;
		GlobalValues.gameBoost.nCount_LastRun = 1;
		GlobalValues.gameBoost.nCount_Relay = 1;
		GlobalValues.gameBoost.nCount_FastDigest = 0;
		
		nFishUpRate = 1f + GlobalValues.gameBoost.nLevel_FishUpRate * 0.05f;
		nHPUpAmount = GlobalValues.gameBoost.nLevel_HPUpAmount * 5f;
		nItemFishUpCount = GlobalValues.gameBoost.nLevel_ItemFishUpCount * 1f;
		nItemFeverUpTime = GlobalValues.gameBoost.nLevel_ItemFeverUpTime * 0.2f;
		nItemMagnetUpTime = GlobalValues.gameBoost.nLevel_ItemMagnetUpTime * 0.3f;
		nItemHeartUpHP = GlobalValues.gameBoost.nLevel_ItemHeartUpHP * 5f;
		
		bStartRun = false;
		bEscape = false;
		bLastRun = false;
		bRelay = false;
		nDigestUpRate = 0f;
		
		if(GlobalValues.gameBoost.nCount_StartRun > 0) bStartRun = true;
		if(GlobalValues.gameBoost.nCount_Escape > 0) bEscape = true;
		if(GlobalValues.gameBoost.nCount_LastRun > 0) bLastRun = true;
		if(GlobalValues.gameBoost.nCount_Relay > 0) bRelay = true;
		if(GlobalValues.gameBoost.nCount_FastDigest > 0) nDigestUpRate = 0.2f;
	}
	
	//
	// Set Boost by Pet
	//
	static public void SetPetBoostExpRate(float nBoostExpRate) {
		nExpUpRateByPet = 1f +nBoostExpRate;
	}
	
	static public void SetPetBoostItemTimeRate(float nBoostItemTimeRate) {
		nItemTimeUpRateByPet = 1f +nBoostItemTimeRate;
	}
	
	static public void SetPetBoostBonusTimeRate(float nBoostBonusTimeRate) {
		nBonusTimeUpRateByPet = 1f +nBoostBonusTimeRate;
	}
	
	//
	// Set Boost by Player
	//
	static public void SetPlayerBoostExpRate(float nBoostExpRate) {
		nExpUpRateByPlayer = 1f +nBoostExpRate;
	}

	static public void SetPlayerBoostFastDigestRate(float nBoostFastDigestRate) {
		nFastDigestRateByPlayer = nBoostFastDigestRate;
	}
	
	static public void SetPlayerBoostDamageDownRate(float nBoostDamageDownRate) {
		nDamageDownRateByPlayer = 1f - nBoostDamageDownRate;
	}
}
