using UnityEngine;
using System.Collections;

public class FishSettings : MonoBehaviour {
	public struct Property {
		public int nLevel;
		public float nSpeedRate;
		public float nInitScale;
		public float nChangeToBonusScaleRate;
		
		public int nGameScore;
		public int nHuntScore;
		public float nExpGage;
		
		public bool bAttackAllLanes;
		public float nAttackDistance;
		public float nAttackDamage;
		
		public void Init() {
			nLevel = 1;
			nSpeedRate = 1f;
			nInitScale = 1f;
			nChangeToBonusScaleRate = 1.2f;
			
			nGameScore = 1;
			nHuntScore = 1;
			nExpGage = 1f;
			
			bAttackAllLanes = false;
			nAttackDistance = 0f;
			nAttackDamage = 0f;
		}		
	}
	
	static public Property obBonus;
	static public Property obCarp;
	static public Property obRedCarp;
	static public Property obCrab;
	static public Property obCroc;
	static public Property obFugu;
	static public Property obMiddleCarp;
	static public Property obRedShark;
	static public Property obShark;
	
	// Use this for initialization
	void Awake () {
		Init();
	}
	
	void Init() {
		Property obProp = new Property();
		
		// Bonus settings		
		obProp.Init();
		{
			obProp.nLevel = 0;
			obProp.nSpeedRate = 1.5f;
			obProp.nInitScale = 1f;
			
			obProp.nGameScore = 100;
			obProp.nHuntScore = 0;
			obProp.nExpGage = 2*GameSettings.nExpGageUnit;
		} 
		obBonus = obProp;
		
		// Carp settings		
		obProp.Init();
		{
			obProp.nLevel = 1;
			obProp.nSpeedRate = 1f;
			obProp.nInitScale = 1f;
			
			obProp.nGameScore = 100;
			obProp.nHuntScore = 1;
			obProp.nExpGage = 0.5f*0.3f*0.7f;
		} 
		obCarp = obProp;

		// RedCarp settings		
		obProp.Init();
		{
			obProp.nLevel = 1;
			obProp.nSpeedRate = 1f;
			obProp.nInitScale = 0.6f;  // 0.8f
			
			obProp.nGameScore = 100;
			obProp.nHuntScore = 1;
			obProp.nExpGage = GameSettings.nExpGageUnit*0.3f*0.7f;
		} 
		obRedCarp = obProp;
	
		// Crab settings		
		obProp.Init();
		{
			obProp.nLevel = 2;
			obProp.nSpeedRate = 0.7f;
			obProp.nInitScale = 1f;
			
			obProp.nGameScore = 200;
			obProp.nHuntScore = 2;
			obProp.nExpGage = 2*GameSettings.nExpGageUnit*0.3f;
			
			obProp.nAttackDamage = 70f;
		} 
		obCrab = obProp;

		// Croc settings		
		obProp.Init();
		{
			obProp.nLevel = 2;
			obProp.nSpeedRate = 1f;
			obProp.nInitScale = 1f;
			
			obProp.nGameScore = 300;
			obProp.nHuntScore = 3;
			obProp.nExpGage = 2*GameSettings.nExpGageUnit;

			obProp.nAttackDistance = (2f/3f) * GameSettings.nScreenWidth;
			obProp.nAttackDamage = 70f;
		} 
		obCroc = obProp;
		
		// Fugu settings		
		obProp.Init();
		{
			obProp.nLevel = 2;
			obProp.nSpeedRate = 1.8f;
			obProp.nInitScale = 1f;
			
			obProp.nGameScore = 200;
			obProp.nHuntScore = 2;
			obProp.nExpGage = 2*GameSettings.nExpGageUnit;
			
			obProp.nAttackDamage = 70f;
		} 
		obFugu = obProp;
		
		// MiddleCarp settings		
		obProp.Init();
		{
			obProp.nLevel = 2;
			obProp.nSpeedRate = 1.2f;
			obProp.nInitScale = 0.9f;//1f;			//1f
			obProp.nChangeToBonusScaleRate = 1.1f;
			
			obProp.nGameScore = 300;		// 중간 물고기 점수 상향.
			obProp.nHuntScore = 2;
			obProp.nExpGage = 2*GameSettings.nExpGageUnit*0.3f*0.7f;
			
			obProp.nAttackDistance = (GameSettings.nScreenWidth/3f);
			obProp.nAttackDamage = 40f;
		} 
		obMiddleCarp = obProp;
		
		// RedShark settings		
		obProp.Init();
		{
			obProp.nLevel = 3;
			obProp.nSpeedRate = 1.2f;
			obProp.nInitScale = 1f;
			obProp.nChangeToBonusScaleRate = 1.8f;
			
			obProp.nGameScore = 2000;
			obProp.nHuntScore = 4;
			obProp.nExpGage = 4*GameSettings.nExpGageUnit*0.7f;
			
			obProp.bAttackAllLanes = true;
			obProp.nAttackDistance = 2*(GameSettings.nScreenWidth/3);
			obProp.nAttackDamage = 150f;
		} 
		obRedShark = obProp;
		
		// Shark settings		
		obProp.Init();
		{
			obProp.nLevel = 3;
			obProp.nSpeedRate = 1f;
			obProp.nInitScale = 1f;
			obProp.nChangeToBonusScaleRate = 1.8f;
			
			obProp.nGameScore = 2000;		// 상어 점수 상향.
			obProp.nHuntScore = 4;
			obProp.nExpGage = 4*GameSettings.nExpGageUnit*0.7f;
			
			obProp.bAttackAllLanes = true;
			obProp.nAttackDistance = 2*(GameSettings.nScreenWidth/3);
			obProp.nAttackDamage = 150f;
		} 
		obShark = obProp;
	}
}
