using UnityEngine;
using System.Collections;

public class PetSettings : MonoBehaviour {
	public struct Property {
		public int nGrade;
		public int nLevel;
		public float nSpeedRate;
		public float nInitScale;
		
		public bool bAttack;
		public float [] listAttackRate;
		public int [] listAttackLevel;

		public bool bRush;
		public float nRushScale;
		public int nRushEffectOffsetX;
		public float[] listRushCoolTime;

		public bool bPossessFish;
		public float nPossessFishScale;
		public float[] listPossessFishCoolTime;
		public int [] listPossessFishLevel;
		
		public bool bHuntFish;
		public float nHuntFishScale;
		public float[] listHuntFishCoolTime;
		public int[] listHuntFishScore;
		
		public bool bGiant;
		public float nGiantScale;
		public float nGiantDistance;
		public float nGiantTime;
		public float[] listGiantCoolTime;
		
		public bool bMakeItem;
		public float nMakeItemScale;
		public float[] listMakeItemCoolTime;

		public bool bFillHP;
		public float nFillHPScale;
		public float[] listFillHPCoolTime;
		public float[] listFillHPRate;
		
		public bool bBoostExp;
		public float[] listBoostExpRate;

		public bool bBoostItemTime;
		public float nBoostItemTimeScale;
		public float[] listBoostItemTimeRate;
		
		public bool bBoostBonusTime;
		public float nBoostBonusTimeScale;
		public float[] listBoostBonusTimeRate;
		
		public void Init() {
			nGrade = GameSettings.nGrade_C;
			nLevel = 1;
			nSpeedRate = 1f;
			nInitScale = 1f;
		
			bAttack = false;
			listAttackRate = new float[GameSettings.nMaxPetLevel];
			listAttackRate[GameSettings.nGrade_C] = 0f;
			listAttackRate[GameSettings.nGrade_B] = 0.5f;
			listAttackRate[GameSettings.nGrade_A] = 0f;
			listAttackRate[GameSettings.nGrade_S] = 0.5f;			
			listAttackLevel = new int[GameSettings.nMaxPetLevel];
			listAttackLevel[GameSettings.nGrade_C] = 2;
			listAttackLevel[GameSettings.nGrade_B] = 2;
			listAttackLevel[GameSettings.nGrade_A] = 3;
			listAttackLevel[GameSettings.nGrade_S] = 3;			
			
			// Wallus
			bRush = false;
			nRushScale = 1f;
			nRushEffectOffsetX = 0;
			listRushCoolTime = new float[GameSettings.nMaxPetLevel];
			listRushCoolTime[GameSettings.nGrade_C] = 24f;
			listRushCoolTime[GameSettings.nGrade_B] = 20f;
			listRushCoolTime[GameSettings.nGrade_A] = 16f;
			listRushCoolTime[GameSettings.nGrade_S] = 10f;				

			// Mermaid
			bPossessFish = false;
			nPossessFishScale = 1.5f*nInitScale;
			listPossessFishCoolTime = new float[GameSettings.nMaxPetLevel];
			listPossessFishCoolTime[GameSettings.nGrade_C] = 32f;
			listPossessFishCoolTime[GameSettings.nGrade_B] = 27f;
			listPossessFishCoolTime[GameSettings.nGrade_A] = 24f;
			listPossessFishCoolTime[GameSettings.nGrade_S] = 25f;				
			listPossessFishLevel = new int[GameSettings.nMaxPetLevel];
			listPossessFishLevel[GameSettings.nGrade_C] = 2;
			listPossessFishLevel[GameSettings.nGrade_B] = 2;
			listPossessFishLevel[GameSettings.nGrade_A] = 2;
			listPossessFishLevel[GameSettings.nGrade_S] = 3;			
			
			// Jellyfish
			bHuntFish = false;
			nHuntFishScale = 1.5f*nInitScale;
			listHuntFishCoolTime = new float[GameSettings.nMaxPetLevel];
			listHuntFishCoolTime[GameSettings.nGrade_C] = 8;
			listHuntFishCoolTime[GameSettings.nGrade_B] = 6f;
			listHuntFishCoolTime[GameSettings.nGrade_A] = 4f;
			listHuntFishCoolTime[GameSettings.nGrade_S] = 3f;				
			listHuntFishScore = new int[GameSettings.nMaxPetLevel];
			listHuntFishScore[GameSettings.nGrade_C] = 3;
			listHuntFishScore[GameSettings.nGrade_B] = 3;
			listHuntFishScore[GameSettings.nGrade_A] = 3;
			listHuntFishScore[GameSettings.nGrade_S] = 5;
			
			// Penguin
			bGiant = false;
			nGiantScale = 5f;
			nGiantDistance = 10f;
			listGiantCoolTime = new float[GameSettings.nMaxPetLevel];
			listGiantCoolTime[GameSettings.nGrade_C] = 30f;
			listGiantCoolTime[GameSettings.nGrade_B] = 26f;
			listGiantCoolTime[GameSettings.nGrade_A] = 22f;
			listGiantCoolTime[GameSettings.nGrade_S] = 15f;
			nGiantTime = 5f;
			
			// TopShell
			bMakeItem = false;
			nMakeItemScale = 1.5f*nInitScale;
			listMakeItemCoolTime = new float[GameSettings.nMaxPetLevel];
			listMakeItemCoolTime[GameSettings.nGrade_C] = 30f;
			listMakeItemCoolTime[GameSettings.nGrade_B] = 26f;
			listMakeItemCoolTime[GameSettings.nGrade_A] = 22f;
			listMakeItemCoolTime[GameSettings.nGrade_S] = 15f;				
			
			// Squid
			bFillHP = false;
			nFillHPScale = 1.5f*nInitScale;
			listFillHPCoolTime = new float[GameSettings.nMaxPetLevel];
			listFillHPCoolTime[GameSettings.nGrade_C] = 40f;
			listFillHPCoolTime[GameSettings.nGrade_B] = 30f;
			listFillHPCoolTime[GameSettings.nGrade_A] = 40f;
			listFillHPCoolTime[GameSettings.nGrade_S] = 30f;				
			listFillHPRate = new float[GameSettings.nMaxPetLevel];
			listFillHPRate[GameSettings.nGrade_C] = 0.2f;
			listFillHPRate[GameSettings.nGrade_B] = 0.2f;
			listFillHPRate[GameSettings.nGrade_A] = 0.3f;
			listFillHPRate[GameSettings.nGrade_S] = 0.3f;		
			
			// Clam
			bBoostExp = false;
			listBoostExpRate = new float[GameSettings.nMaxPetLevel];
			listBoostExpRate[GameSettings.nGrade_C] = 0.1f;
			listBoostExpRate[GameSettings.nGrade_B] = 0.2f;
			listBoostExpRate[GameSettings.nGrade_A] = 0.3f;
			listBoostExpRate[GameSettings.nGrade_S] = 0.5f;
			
			// StarFish
			bBoostItemTime = false;
			nBoostItemTimeScale = 1.5f*nInitScale;
			listBoostItemTimeRate = new float[GameSettings.nMaxPetLevel];
			listBoostItemTimeRate[GameSettings.nGrade_C] = 0.1f;
			listBoostItemTimeRate[GameSettings.nGrade_B] = 0.13f;
			listBoostItemTimeRate[GameSettings.nGrade_A] = 0.2f;
			listBoostItemTimeRate[GameSettings.nGrade_S] = 0.3f;			
			
			// Ray
			bBoostBonusTime = false;
			nBoostBonusTimeScale = 1.5f*nInitScale;
			listBoostBonusTimeRate = new float[GameSettings.nMaxPetLevel];
			listBoostBonusTimeRate[GameSettings.nGrade_C] = 0.1f;
			listBoostBonusTimeRate[GameSettings.nGrade_B] = 0.15f;
			listBoostBonusTimeRate[GameSettings.nGrade_A] = 0.25f;
			listBoostBonusTimeRate[GameSettings.nGrade_S] = 0.4f;		
		}

		public float GetAttackRate() {
			return listAttackRate[nGrade] + (nLevel - 1)*0.05f;
		}
		
		public int GetAttackLevel() {
			return listAttackLevel[nGrade];
		}
		
		public float GetRushCoolTime() {
			return listRushCoolTime[nGrade] - (nLevel - 1)*0.4f;
		}

		public float GetPossessFishCoolTime() {
			return listPossessFishCoolTime[nGrade] - (nLevel - 1)*0.3f;
		}
		
		public int GetPossessFishLevel() {
			return listPossessFishLevel[nGrade];
		}
		
		public float GetGiantCoolTime() {
			return listGiantCoolTime[nGrade] - (nLevel - 1)*0.5f;
		}
		
		public float GetGiantTime() {
			return nGiantTime;
		}
		
		public float GetHuntFishCoolTime() {
			return listHuntFishCoolTime[nGrade] - (nLevel - 1)*0.2f;
		}
		
		public int GetHuntFishScore() {
			return listHuntFishScore[nGrade];
		}			
		
		public float GetMakeItemCoolTime() {
			return listMakeItemCoolTime[nGrade] - (nLevel - 1)*0.5f;
		}

		public float GetBoostItemTimeRate() {
			return listBoostItemTimeRate[nGrade] + (nLevel - 1)*0.003f;
		}		

		public float GetBoostBonusTimeRate() {
			return listBoostBonusTimeRate[nGrade] + (nLevel - 1)*0.005f;
		}		
		
		public float GetFillHPCoolTime() {
			return listFillHPCoolTime[nGrade];
		}
		
		public float GetFillHPRate() {
			return listFillHPRate[nGrade] + (nLevel - 1)*1f;
		}		
		
		public float GetBoostExpRate() {
			return listBoostExpRate[nGrade] + (nLevel - 1)*0.01f;
		}		
	}
	
	static public Property obDolphin;
	static public Property obHammerShark;
	static public Property obJellyfish;
	static public Property obLittleShark;
	static public Property obMermaid;
	static public Property obWalrus;
	
	static public Property obTopShell;
	static public Property obStarFish;
	static public Property obSquid;
	static public Property obRay;
	static public Property obPenguin;
	static public Property obClam;
	
	// Use this for initialization
	void Awake () {
		Init();
	}
	
	void Init() {
		Property obProp = new Property();
		
		// Dolphin settings		
		obProp.Init();
		{
			obProp.nLevel = 3;
			obProp.nSpeedRate = 1.6f;
			obProp.nInitScale = 0.9f;
			obProp.bAttack = true;
			obProp.listAttackRate[GameSettings.nGrade_C] = 1f;
			obProp.listAttackRate[GameSettings.nGrade_B] = 1f;
			obProp.listAttackRate[GameSettings.nGrade_A] = 1f;
			obProp.listAttackRate[GameSettings.nGrade_S] = 1f;			
			obProp.listAttackLevel[GameSettings.nGrade_C] = GameSettings.nMaxLevel;
			obProp.listAttackLevel[GameSettings.nGrade_B] = GameSettings.nMaxLevel;
			obProp.listAttackLevel[GameSettings.nGrade_A] = GameSettings.nMaxLevel;
			obProp.listAttackLevel[GameSettings.nGrade_S] = GameSettings.nMaxLevel;						
		} 
		obDolphin = obProp;
		
		// Jellyfish settings
		obProp.Init();
		{
			obProp.nLevel = 1;
			obProp.nSpeedRate = 0.8f;
			obProp.nInitScale = 0.6f;		//0.6f.
			obProp.bHuntFish = true;
			obProp.nHuntFishScale = 1.5f*obProp.nInitScale;
		}
		obJellyfish = obProp;

		// LittleShark settings
		obProp.Init();
		{
			obProp.nGrade = GameSettings.nGrade_B;	// 테스트를 위함.
			obProp.nLevel = 2;
			obProp.nSpeedRate = 1f;
			obProp.nInitScale = 0.8f;		//0.57f.
			
			obProp.bAttack = true;
		}
		obLittleShark = obProp;

		// Mermaid settings
		obProp.Init();
		{
			obProp.nLevel = 1;
			obProp.nSpeedRate = 0.8f;
			obProp.nInitScale = 0.37f;		//0.5f
			obProp.bPossessFish = true;
			obProp.nPossessFishScale = 1.5f*obProp.nInitScale;			
		}
		obMermaid = obProp;
		
		// Walrus settings
		obProp.Init();
		{
			obProp.nLevel = 3;
			obProp.nSpeedRate = 0.8f;
			obProp.nInitScale = 0.68f;//0.45f;
			obProp.bRush = true;
			obProp.nRushScale = 1.0f;
			obProp.nRushEffectOffsetX = -150;
		}
		obWalrus = obProp;

		// TopShell settings
		obProp.Init();
		{
			obProp.nLevel = 1;
			obProp.nSpeedRate = 1f;
			obProp.nInitScale = 0.8f;		//0.57f.
			
			obProp.bMakeItem = true;
			obProp.nMakeItemScale = obProp.nInitScale*1.5f;
		}
		obTopShell = obProp;
		
		// StarFish settings
		obProp.Init();
		{
			obProp.nLevel = 1;
			obProp.nSpeedRate = 1f;
			obProp.nInitScale = 1f;		//0.57f.
			
			obProp.bBoostItemTime = true;
			obProp.nBoostItemTimeScale = obProp.nInitScale*1.25f;			
		}
		obStarFish = obProp;
		
		// Squid settings
		obProp.Init();
		{
			obProp.nLevel = 1;
			obProp.nSpeedRate = 1f;
			obProp.nInitScale = 0.8f;		//0.57f.
			
			obProp.bFillHP = true;
			obProp.nFillHPScale = obProp.nInitScale*1.5f;
		}
		obSquid = obProp;
		
		// Ray settings
		obProp.Init();
		{
			obProp.nLevel = 3;
			obProp.nSpeedRate = 1f;
			obProp.nInitScale = 0.8f;		//0.57f.
			
			obProp.bBoostBonusTime = true;
			obProp.nBoostBonusTimeScale = obProp.nInitScale*1.25f;			
		}
		obRay = obProp;
		
		// Penguin settings
		obProp.Init();
		{
			obProp.nLevel = 1;
			obProp.nSpeedRate = 1f;
			obProp.nInitScale = 0.8f;		//0.57f.
			
			obProp.bGiant = true;
		}
		obPenguin = obProp;
		
		// Clam settings
		obProp.Init();
		{
			obProp.nLevel = 1;
			obProp.nSpeedRate = 1f;
			obProp.nInitScale = 0.8f;		//0.57f.
			
			obProp.bBoostExp = true;
		}
		obClam = obProp;
		
		// HammerShark settings
		obProp.Init();
		{
			obProp.nLevel = 3;
			obProp.nSpeedRate = 0.8f;
			obProp.nInitScale = 0.5f;
			
			obProp.bBoostExp = true;
		}
		obHammerShark = obProp;
		
	}
}
