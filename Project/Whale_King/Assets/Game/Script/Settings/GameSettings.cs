using UnityEngine;
using System.Collections;

public class GameSettings : MonoBehaviour {
	static public int nScreenWidth = 1280;				// actual screen width 
	static public int nScreenHeight = 720;					// actual screen height 
	static public int nScreenPositionX = 0;				
	static public int nScreenPositionY = 0;				

	static public int nScoreAreaHeight = 120;
	static public int nGameControllerAreaWidth = 120;
	static public int nPlayAreaHeight = nScreenHeight - nScoreAreaHeight - nGameControllerAreaWidth;
	
	static public int nScoreAreaWidth = 500;
	static public int nScoreAreaStartPositionY = nScreenHeight - nScoreAreaHeight;
	static public int nScoreAreaEatUpFishPositionX = 150;
	static public int nScoreAreaEatUpFishPositionY = 620;
	static public int nScoreAreaDigestFishPositionX = 210;
	static public int nScoreAreaDigestFishPositionY = 630;
	static public int nScoreAreaHuntFishPositionX = 130;
	static public int nScoreAreaHuntFishPositionY = 635;
	
	static public int nPlayAreaStartPositionX = -500;
	static public int nPlayAreaEndPositionX = nScreenWidth + 200;
	static public int nPlayAreaStartPositionY = nScoreAreaStartPositionY - nPlayAreaHeight;
	static public int nPlayAreaEndPositionY = nScoreAreaStartPositionY;
	
	static public int nPlayLaneCount = 4;
	static public int nPlayLaneHeight = nPlayAreaHeight / nPlayLaneCount;
	static public int[] listPlayLaneCenterY = new int[nPlayLaneCount];

	public const int nMaxLevel = 7;
	public const int nMaxPetLevel = 4;
	
	static public int nGameControllerAreaStartPositionY = nPlayAreaStartPositionY - nPlayAreaHeight;
	
	static public int nPlayerPositionX = 200;
	static public int nPetPositionX = 70;
	static public float nPlayerMoveSpeed = 500;			
	static public float nPlayerBlinkTime = 2f;
	static public float nPlayerBubbleEffectTime = 0.5f;
	static public float nBGBubbleEffectTime = 1.5f;
	static public float nBGFlowBubbleEffectTime = 1f;
	
	static public float nFriendMoveSpeed = 200;			
	static public int nFriendImageSize = 40;			
	static public int nRankUserImageSize = 64;			
	
	static public float nPetMoveSpeed = 300;	
	static public float nFishMoveSpeed = 600;	
	static public float nItemMoveSpeed = 400;
	static public float nBGFlowBubbleMoveSpeed = 2000;

	static public float nItemAppearStartX = GameSettings.nScreenWidth*(1f/2f);
	static public float nItemAppearEndX = GameSettings.nScreenWidth*(9f/10f);
	
	static public float nFishDigestCountSpeed = 500;	
	static public float nFishHuntCountSpeed = 400;	

	static public float nFishAbosorbTime = 0.2f;
	static public float nFishDisappearTime = 0.8f;
	
	static public float nPlayerChangeSizeTime = 1f;

	static public float nPowerPlayerScaleUp = 2.3f;//			1.5f;	//피버 모드와 같이 함.
	static public float nPowerSpeedUp = 1.25f;
	static public float nPowerModeTime = 10f;
	static public float nBossSpeedUp = 3f;
	
	static public float nStartRunDistance = 100f;					
	static public float nStartRunSpeedUp = 2.5f;
	static public float nLastRunSpeedUp = 2f;
	static public float nLastRunTime = 5f;

	static public float nFeverPlayerScaleUp = 2.3f;					// 피버 모드 일때 크기 .
	static public float nFeverSpeedUp = 2f;							//1.2f.
	static public float nFeverTime = 7.8f;							//5f.
	static public float nMagnetTime = 5f;
	static public float nWingTime = 0.7f;							//0.5f.
	static public int nItem_GoldFish_GameScore = 2000;			//황금 물고기 아이템. 
	static public int nItem_GoldFish_HuntScore = 10;					
	static public int nItem_GoldFish_ExpGage = 0;				// 10 ->0 진화되지 않도록 수정

	static public int nItem_Heart_HP = 60;						//yj edit 30 하트 아이템 효과
	
	static public float nPetChangeSizeTime = 0.5f;
	static public float nLookingAtPlayerTime = 2f;
	
	static public float nGameSpeed = 5f;			// meter per seconds (1 meter is 'nPixelPerMeter' pixels)
	static public float nGameSpeedUpRate = 1.2f;		
	static public float nDistancePerScreenWidth = 10f;
	static public float nPixelPerMeter = nScreenWidth/nDistancePerScreenWidth;
	static public float nSpeedRate_BG_Far = 0.2f;
	static public float nSpeedRate_BG_Mid = 0.5f;
	static public float nSpeedRate_BG_Near = 1f;
	
	static public float nDistanceSWA = 2f * nPixelPerMeter;			// SWA distance (unit : pixel)
	static public float nTimeSWA = 0.25f;
	
	static public int nCountBonusSet = 3;		// how many do bonus appear in one time?
	static public int nSpawnPositionXGap = 150;
	
	static public float nExpGageUnit = 0.6f;		//1f.
	
	static public int nComboGameScore = 500;
	static public int nComboHuntScore = 5;

	static public int nGrade_C = 0;
	static public int nGrade_B = 1;
	static public int nGrade_A = 2;
	static public int nGrade_S = 3;
	
	static public int LAYER_BG_FAR = -100;
	static public int LAYER_BG_MID = -200;
	static public int LAYER_BG_NEAR = -600;
	static public int LAYER_PLAYAREA = -300;
	static public int LAYER_FISH = -300;
	static public int LAYER_EFFECT_BEHIND = -350;
	static public int LAYER_FRIEND = -380;
	static public int LAYER_PLAYER = -400;
	static public int LAYER_PET = -450;
	static public int LAYER_FISH_WIN = -500;
	static public int LAYER_ITEM = -500;
	static public int LAYER_SCOREAREA = -700;
	static public int LAYER_EFFECT = -800;
	static public int LAYER_MENU = -900;
	static public int LAYER_CAMERA = -1000;
	
	static public Vector3 posSound = new Vector3(0, 0, LAYER_CAMERA);
	
	// Use this for initialization
	void Awake () {
		for (int i = 0; i < nPlayLaneCount; i++) {
			listPlayLaneCenterY[i] = (int)((nPlayAreaStartPositionY + (i + 0.5) * nPlayLaneHeight));
		}
	}
}
