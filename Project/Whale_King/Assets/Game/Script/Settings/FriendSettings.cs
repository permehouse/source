using UnityEngine;
using System.Collections;

public class FriendSettings : MonoBehaviour {
	public struct Property {
		public string strPlayerImage;
		public float nInitScale;
		
		public void Init() {
			strPlayerImage = ""; 
			nInitScale = 1f;
		}		
	}
	
	static public Property obWhale;
	static public Property obPolaBear;
	static public Property obPororo;
	static public Property obMobydick;
	static public Property obTurtle;
	static public Property obOctopus;
	
	
	// Use this for initialization
	void Awake () {
		Init();
	}
	
	void Init() {
		Property obProp = new Property();
		
		// Whale settings		
		obProp.Init();
		{
			obProp.strPlayerImage = "Player_Whale_Damage";
			obProp.nInitScale = PlayerSettings.obWhale.nInitScale * 0.85f;
		} 
		obWhale = obProp;
				
		// PolarBear settings		
		obProp.Init();
		{
			obProp.strPlayerImage = "Player_PolarBear_Damage";
			obProp.nInitScale = PlayerSettings.obPolaBear.nInitScale * 0.85f;
		} 
		obPolaBear = obProp;
		
		// Pororo settings		
		obProp.Init();
		{
			obProp.strPlayerImage = "Player_Pororo_Damage";
			obProp.nInitScale = PlayerSettings.obPororo.nInitScale * 0.83f;
		} 
		obPororo = obProp;
		
		// Mobydick settings		
		obProp.Init();
		{
			obProp.strPlayerImage = "Player_Mobydick_Damage";
			obProp.nInitScale = PlayerSettings.obMobydick.nInitScale * 0.85f;
		} 
		obMobydick = obProp;
		
		// Turtle settings		
		obProp.Init();
		{
			obProp.strPlayerImage = "Player_Turtle_Damage";
			obProp.nInitScale = PlayerSettings.obTurtle.nInitScale * 0.85f;
		}  
		obMobydick = obProp;
		
		// Octopus settings		
		obProp.Init();
		{
			obProp.strPlayerImage = "Player_Octopus_Damage";
			obProp.nInitScale = PlayerSettings.obOctopus.nInitScale * 0.85f;
		} 
		obMobydick = obProp;
	}
}
