using UnityEngine;
using System.Collections;

public class PlayerSettings : MonoBehaviour {
	public struct Property {
		public int nGrade;
		public int nLevel;
		public float nHP;
		public float nRelayHP;
		public float nFullHP;
		public float nSpeedRate;
		public float nInitScale;
		public int nPowerModeLevel;
		
		public bool bBoostExp;
		public float nBoostExpRate;
		
		public bool bBoostEscape;
		public float nBoostEscapeRate;
		
		public bool bBoostFastDigest;
		public float nBoostFastDigestRate;
		
		public bool bBoostDamageDown;
		public float nBoostDamageDownRate;
		
		public void Init() {
			nGrade = GameSettings.nGrade_C;
			nLevel = 1;
			nHP = 0f;
			nRelayHP = 0f;
			nFullHP = 0f;
			nSpeedRate = 1f;
			nInitScale = 1f;
			nPowerModeLevel = 2;
			
			// Mobydick, Pororo
			bBoostExp = false;
			nBoostExpRate = 0.3f;
			
			// Octopus
			bBoostEscape = false;
			nBoostEscapeRate= 0.25f;
			
			// PolarBear
			bBoostFastDigest = false;
			nBoostFastDigestRate = 0.1f;
			
			// Turtle, Pororo
			bBoostDamageDown = false;
			nBoostDamageDownRate = 0.25f;
		}		

		public float GetBoostExpRate() {
			return nBoostExpRate + (nLevel - 1)*0.01f;
		}				
		
		public float GetBoostEscapeRate() {
			return nBoostEscapeRate + (nLevel - 1)*0.01f;
		}				

		public float GetBoostFastDigestRate() {
			return nBoostFastDigestRate + (nLevel - 1)*0.05f;
		}				

		public float GetBoostDamageDownRate() {
			return nBoostDamageDownRate + (nLevel - 1)*0.01f;
		}				
	}
	
	static public Property obWhale;
	static public Property obPolaBear;
	static public Property obPororo;
	static public Property obMobydick;
	static public Property obTurtle;
	static public Property obOctopus;
	
	// Use this for initialization
	void Awake () {
		Init();
	}
	
	void Init() {
		Property obProp = new Property();
		
		// Whale settings		
		obProp.Init();
		{
			obProp.nLevel = 1;
			obProp.nHP = 120f;
			obProp.nRelayHP = 30f;
			obProp.nFullHP = 180f;
			obProp.nSpeedRate = 2f;
			obProp.nInitScale = 0.75f;
			obProp.nPowerModeLevel = 3;
		} 
		obWhale = obProp;
				
		// PolarBear settings		
		obProp.Init();
		{
			obProp.nLevel = 1;
			obProp.nHP = 120f;
			obProp.nRelayHP = 30f;
			obProp.nFullHP = 180f;
			obProp.nSpeedRate = 2f;
			obProp.nInitScale = 0.83f;
			obProp.nPowerModeLevel = 3;			
			
			obProp.bBoostFastDigest = true;
		} 
		obPolaBear = obProp;
		
		// Pororo settings		
		obProp.Init();
		{
			obProp.nLevel = 1;
			obProp.nHP = 120f;
			obProp.nRelayHP = 30f;
			obProp.nFullHP = 180f;
			obProp.nSpeedRate = 2f;
			obProp.nInitScale = 0.75f;
			obProp.nPowerModeLevel = 3;			
			
			obProp.bBoostExp = true;
			obProp.nBoostExpRate = 0.2f;
			obProp.bBoostDamageDown = true;
			obProp.nBoostDamageDownRate =0.2f;
		} 
		obPororo = obProp;
		
		// Mobydick settings		
		obProp.Init();
		{
			obProp.nLevel = 1;
			obProp.nHP = 120f;
			obProp.nRelayHP = 30f;
			obProp.nFullHP = 180f;
			obProp.nSpeedRate = 2f;
			obProp.nInitScale = 0.75f;
			obProp.nPowerModeLevel = 3;			
			
			obProp.bBoostExp = true;
		} 
		obMobydick = obProp;
		
		// Turtle settings		
		obProp.Init();
		{
			obProp.nLevel = 1;
			obProp.nHP = 120f;
			obProp.nRelayHP = 30f;
			obProp.nFullHP = 180f;
			obProp.nSpeedRate = 2f;
			obProp.nInitScale = 1f;
			obProp.nPowerModeLevel = 3;			
			
			obProp.bBoostDamageDown = true;
		} 
		obTurtle = obProp;
		
		// Octopus settings		
		obProp.Init();
		{
			obProp.nLevel = 1;
			obProp.nHP = 120f;
			obProp.nRelayHP = 30f;
			obProp.nFullHP = 180f;
			obProp.nSpeedRate = 2f;
			obProp.nInitScale = 1f;
			obProp.nPowerModeLevel = 3;			
			
			obProp.bBoostEscape = true;
		} 
		obOctopus = obProp;
	}
}
