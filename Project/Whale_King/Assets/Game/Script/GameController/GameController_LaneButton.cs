using UnityEngine;
using System.Collections;

public class GameController_LaneButton : GameController {
	tk2dAnimatedSprite obAni = null;
	protected bool bDrag = false;
	
	void Awake() {
		obAni = GetComponent<tk2dAnimatedSprite>();
	}
	
	void Start() {
	}

	public override void CheckKeyInput() {
		base.CheckKeyInput();
		
		if(nMoveToLane != -1) {
			UpdateButtonPosition();				
		}
	}
	
	public override void CheckTouchInput() {
		int nCount = Input.touchCount;
		for(int i = 0; i < nCount; i++) {
			Vector2 posTouch = Input.GetTouch(i).position;
			TouchPhase phaseTouch = Input.GetTouch(i).phase;
			
			if(phaseTouch == TouchPhase.Began) {
				bDrag = false;
				OnTouch(posTouch, false);
			}
			else if(phaseTouch == TouchPhase.Moved) {
				bDrag = true;
				OnDrag(posTouch);
			}
			else if(phaseTouch == TouchPhase.Ended || phaseTouch == TouchPhase.Canceled) {
				if(bDrag) {
					bDrag = false;
					OnTouch(posTouch, true);
				}
			}
		}
		
		if(Input.GetKey (KeyCode.Escape)) {
			bEscape = true;
		}
		if(Input.GetKey (KeyCode.Menu)) {
			bMenu = true;
		}
	}

	void OnTouch(Vector2 posTouch, bool bForceToMove) {
		float nY = posTouch.y  + GameSettings.nPlayLaneHeight/2;
		int nLane = (int)(((nY - GameSettings.nPlayAreaStartPositionY)/GameSettings.nPlayLaneHeight) - 0.5);
		if(nLane < 0) {
			nLane = 0;
		}
		else if(nLane > GameSettings.nPlayLaneCount-1) {
			nLane = GameSettings.nPlayLaneCount-1;
		}
		
		if(bForceToMove == false && nLane == GamePlay.nPlayerLane) {
			return;
		}
		
		nMoveToLane = nLane;
		UpdateButtonPosition();				
	}

	void OnDrag(Vector3 posTouch) {
		float nY = posTouch.y;
		if(nY < 0) {
			nY = 0;
		}
		else if(nY > GameSettings.nPlayAreaStartPositionY + GameSettings.nPlayAreaHeight) {
			nY = GameSettings.nPlayAreaStartPositionY + GameSettings.nPlayAreaHeight;
		}
		
		if(nMoveToPositionY == nY) {
			return;
		}		
		
		nMoveToPositionY = nY;
	}
	
	void UpdateButtonPosition() {
		SetVisible(true);
		
		float nX = GameSettings.nPlayerPositionX;
		float nY = GameSettings.listPlayLaneCenterY[nMoveToLane];
		transform.position = new Vector3(nX, nY, transform.position.z);
		
		obAni.Play ("Button_Lane");
		obAni.animationCompleteDelegate = OnCompleteShowAnimation;	
	}

	public virtual void OnCompleteShowAnimation(tk2dAnimatedSprite sprite, int clipId) {
		SetVisible(false);
    	}

	void SetVisible(bool bVisible) {
		if(bVisible) {
			transform.localScale = new Vector3(1f, 1f, 1f);
		}
		else {
			transform.localScale = new Vector3(0f, 0f, 0f);
		}			
	}
}
