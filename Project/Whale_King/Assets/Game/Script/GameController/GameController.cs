using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {
	protected bool bMoveUp;
	protected  bool bMoveDown;
	protected bool bMenu;
	protected bool bEscape;
	protected int nMoveToLane;
	protected float nMoveToPositionY;
	
	// Update is called once per frame
	void Update() {
		bMoveUp = false;
		bMoveDown = false;
		bMenu = false;
		bEscape = false;
		nMoveToLane = -1;
		nMoveToPositionY = -1f;
		
		if(CheckIgnoreInput()) {
			return;
		}

		if(Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {
			CheckTouchInput();
		}
		else {
			CheckKeyInput();
			CheckMouseInput();
		}
		
		ProcessEvent();
	}
	
	public void ProcessEvent() {
		if(bEscape) {
			GamePlay.OnControllerEscape();
		}
		else if(bMenu) {
			GamePlay.OnControllerMenu();
		}
		else if(bMoveUp) {
			GamePlay.OnControllerUp();
		}
		else if(bMoveDown)	{
			GamePlay.OnControllerDown();
		}
		else if(nMoveToLane >= 0) {
			GamePlay.OnControllerMoveToLane(nMoveToLane);	
		}
		else if(nMoveToPositionY >= 0) {
			GamePlay.OnControllerMoveToPositionY(nMoveToPositionY);	
		}
	}
	
	public virtual void CheckKeyInput() {
		if(Input.GetKeyDown(KeyCode.W)) {
			bMoveUp = true;
		}
		else if(Input.GetKeyDown(KeyCode.S)) {
			bMoveDown = true;
		}
		else if(Input.GetKey (KeyCode.Escape)) {
			bEscape = true;
		}
		else if(Input.GetKey (KeyCode.Alpha1)) {
			nMoveToLane = 0;
		}
		else if(Input.GetKey (KeyCode.Alpha2)) {
			nMoveToLane = 1;
		}
		else if(Input.GetKey (KeyCode.Alpha3)) {
			nMoveToLane = 2;
		}
		else if(Input.GetKey (KeyCode.Alpha4)) {
			nMoveToLane = 3;
		}
		else if(Input.GetKey (KeyCode.Alpha5)) {
			nMoveToLane = 4;
		}
		
//		float nY = GamePlay.obPlayer.transform.position.y + GameSettings.nPlayLaneHeight/2;
//		int nLane = (int)(((nY - GameSettings.nPlayAreaStartPositionY)/GameSettings.nPlayLaneHeight) - 0.5);
//		Debug.DrawLine(GamePlay.obPlayer.transform.position,new Vector3(GamePlay.obPlayer.transform.position.x + 300, GameSettings.listPlayLaneCenterY[nLane]));
	}
	
	public virtual void CheckMouseInput() {
	}
	
	public virtual void CheckTouchInput() {
		if(Input.touchCount == 1) {
			Touch inputScreenTouch = Input.GetTouch(0);
			if(inputScreenTouch.phase == TouchPhase.Began) {
				if(inputScreenTouch.position.y < Screen.height / 2)
				{
					bMoveDown = true;
				}
				else
				{
					bMoveUp = true;
				}
			}
		}
	}
	
	public virtual void ShowController(bool bShow) {
	}
	
	public virtual bool CheckIgnoreInput() {
		if(GamePlay.nGameStatus == GamePlay.GameStatus.PAUSE) {
			return true;
		}
		
		Camera cam = GamePlay._instance.obCamera;
		Vector3 posMouse = cam.ScreenToWorldPoint(Input.mousePosition);
		Vector3 posPauseButton = MenuManager._instance.obPause.gameObject.transform.position;
		int sizePauseButton = 30;		
		
		// if mouse position is in the area of 'Pause' button, ignore input		
		if(posMouse.x > posPauseButton.x - sizePauseButton && posMouse.x < posPauseButton.x + sizePauseButton
			&& posMouse.y > posPauseButton.y - sizePauseButton && posMouse.y < posPauseButton.y + sizePauseButton) {
			return true;
		}		
		
		return false;
	}
}
