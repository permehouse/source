using UnityEngine;
using System.Collections;

public class GameController_Joystick : GameController {
	protected Vector2 posCenter;
	protected int nGapX = 100;
	protected int nGapY = 60;
	protected Rect rcMoveBox;
	protected bool bMoved = false;
	
	void Start() {
		posCenter = new Vector2(transform.position.x, transform.position.y);

		rcMoveBox.xMin = posCenter.x - nGapX;
		rcMoveBox.xMax = posCenter.x + nGapX;
		rcMoveBox.yMin = posCenter.y - nGapY;
		rcMoveBox.yMax = posCenter.y + nGapY;
	}
	
	public override void CheckTouchInput () {
		int nCount = Input.touchCount;
		
		GamePlay.strDebug = "CheckTouchInput (" + nCount + ") ";
		
		for(int i = 0; i < nCount; i++) {
			Vector2 posTouch = Input.GetTouch(i).position;
			TouchPhase phaseTouch = Input.GetTouch(i).phase;
			GamePlay.strDebug += "(" + rcMoveBox.xMin + ", " + rcMoveBox.xMax + ", " + rcMoveBox.yMin + ", " + rcMoveBox.yMax + ") (" + posCenter.x + ", " + posCenter.y + ") (" + posTouch.x + ", " + posTouch.y + ")"
				+ ", phase : " + (int)phaseTouch;
			
			if(phaseTouch == TouchPhase.Ended || phaseTouch == TouchPhase.Canceled) {
				bMoved = false;
				UpdateControllerPosition();			// reset controller's position
			}
			else {
				if(rcMoveBox.Contains(posTouch)) {
					if(posTouch.y > posCenter.y) {
						bMoveUp = true;
						bMoveDown = false;
					}
					else {
						bMoveDown = true;
						bMoveUp = false;
					}
				}

				if(bMoveUp || bMoveDown) {
					bMoved = true;
					ProcessEvent();
					UpdateControllerPosition();				
				}
			}
		}
		
		if(Input.GetKey (KeyCode.Escape)) {
			bEscape = true;
		}
		if(Input.GetKey (KeyCode.Menu)) {
			bMenu = true;
		}
	}
	
	void UpdateControllerPosition() {
		if(bMoveUp) {
			transform.position = new Vector3(transform.position.x, rcMoveBox.yMax, transform.position.z);
		}
		else if(bMoveDown) {
			transform.position = new Vector3(transform.position.x, rcMoveBox.yMin, transform.position.z);
		}			
		else {
			transform.position = new Vector3(transform.position.x, posCenter.y, transform.position.z);
		}
	}
}
