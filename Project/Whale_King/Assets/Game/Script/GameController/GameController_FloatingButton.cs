using UnityEngine;
using System.Collections;

public class GameController_FloatingButton : GameController {
	private Vector2 posPivot;
	private float nGapY = 50;
	private bool bDirUp = false;
	
	public Transform pfButtonUp;
	public Transform pfButtonDown;
	Transform obButtonUp;
	Transform obButtonDown;
	
	void Awake() {
		obButtonUp = Instantiate(pfButtonUp, transform.position, Quaternion.identity) as Transform;
		obButtonDown = Instantiate(pfButtonDown, transform.position, Quaternion.identity) as Transform;
	}
	
	void Start() {
		bDirUp = false;
		int nY = GameSettings.listPlayLaneCenterY[GamePlay.nPlayerLane] - GameSettings.nPlayLaneHeight/2;
		posPivot = new Vector2(transform.position.x, nY);
		
		UpdateButtonPosition(posPivot);
	}
	
	public override void CheckTouchInput() {
		int nCount = Input.touchCount;
		for(int i = 0; i < nCount; i++) {
			Vector2 posTouch = Input.GetTouch(i).position;
			TouchPhase phaseTouch = Input.GetTouch(i).phase;
			
			if(phaseTouch == TouchPhase.Began) {
				OnTouch(posTouch);
			}
			else if(phaseTouch == TouchPhase.Moved) {
				OnDrag(posTouch);
			}
			else if(phaseTouch == TouchPhase.Ended || phaseTouch == TouchPhase.Canceled) {
				posPivot.x = posTouch.x;
				posPivot.y = posTouch.y;
				
				UpdateButtonPosition(posPivot);	
			}
		}
		
		if(Input.GetKey (KeyCode.Escape)) {
			bEscape = true;
		}
		if(Input.GetKey (KeyCode.Menu)) {
			bMenu = true;
		}
	}

	void OnTouch(Vector2 posTouch) {
		if(bDirUp) {
			if(posPivot.y - posTouch.y> nGapY) {
				bDirUp = false;	
				bMoveDown = true;				
			}
			else {
				bMoveUp = true;				
			}
		}
		else {
			if(posTouch.y - posPivot.y> nGapY) {
				bDirUp = true;	
				bMoveUp = true;				
			}
			else {
				bMoveDown = true;				
			}
		}
		
		posPivot.x = posTouch.x;
		posPivot.y = posTouch.y;
				
		UpdateButtonPosition(posPivot);				
	}

	void OnDrag(Vector3 posTouch) {
		UpdateButtonPosition(posTouch);				
		
		if(Mathf.Abs(posTouch.y - posPivot.y) < GameSettings.nPlayLaneHeight/4) {
			return;
		}
		
		float nY = posTouch.y  + GameSettings.nPlayLaneHeight/2;
		int nLane = (int)(((nY - GameSettings.nPlayAreaStartPositionY)/GameSettings.nPlayLaneHeight) - 0.5);
		
		if(nLane != GamePlay.nPlayerLane) {
			if(nLane < GamePlay.nPlayerLane) {
				bDirUp = false;	
				bMoveDown = true;				
			}
			else {
				bDirUp = true;	
				bMoveUp = true;				
			}
			
			posPivot.y = posTouch.y;
			posPivot.x = posTouch.x;
		}
	}
	
	void UpdateButtonPosition(Vector2 posButton) {
		float nX = posButton.x;
		float nZ = obButtonUp.transform.position.z;
			
		if(bDirUp) {
			obButtonUp.transform.position = new Vector3(nX, posButton.y, nZ);
			obButtonDown.transform.position = new Vector3(nX, posButton.y - 2*nGapY, nZ);
		}
		else {
			obButtonUp.transform.position = new Vector3(nX, posButton.y +  2*nGapY, nZ);
			obButtonDown.transform.position = new Vector3(nX, posButton.y, nZ);
		}
	}
}
