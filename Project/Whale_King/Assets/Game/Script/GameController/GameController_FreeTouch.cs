using UnityEngine;
using System.Collections;

public class GameController_FreeTouch : GameController {
	protected bool bDrag = false;
	tk2dAnimatedSprite obAni = null;
	
	void Awake() {
		obAni = GetComponent<tk2dAnimatedSprite>();
	}
	
	void Start() {
		SetIndicatorVisible(false);
	}
	
	public override void CheckTouchInput() {
		int nCount = Input.touchCount;
		for(int i = 0; i < nCount; i++) {
			Vector2 posTouch = Input.GetTouch(i).position;
			TouchPhase phaseTouch = Input.GetTouch(i).phase;
			
			if(phaseTouch == TouchPhase.Began || phaseTouch == TouchPhase.Moved) {
				OnDrag(posTouch);
			}
			else if(phaseTouch == TouchPhase.Ended || phaseTouch == TouchPhase.Canceled) {
				int nY = GetLanePositionY(posTouch.y); 
				OnDrag(new Vector2(posTouch.x, nY));
				UpdateIndicatorPosition();
			}
		}
		
		if(Input.GetKey (KeyCode.Escape)) {
			bEscape = true;
		}
		if(Input.GetKey (KeyCode.Menu)) {
			bMenu = true;
		}
	}

	void OnDrag(Vector2 posTouch) {
		float nY = posTouch.y;
		
		float minY = (float)GameSettings.listPlayLaneCenterY[0];
		float maxY = (float)GameSettings.listPlayLaneCenterY[GameSettings.nPlayLaneCount-1];
		if(nY < minY) nY = minY;
		else if(nY > maxY) nY = maxY;
		
		if(nMoveToPositionY == nY) {
			return;
		}		
		
		nMoveToPositionY = nY;
	}
	
	public override void CheckMouseInput() {
		if (Input.GetMouseButtonUp(0)) {
			bDrag = false;
			Camera cam = GamePlay._instance.obCamera;
			Vector3 posMouse = cam.ScreenToWorldPoint(Input.mousePosition);
			int nY = GetLanePositionY(posMouse.y); 
			OnDrag(new Vector2(posMouse.x, nY));
			UpdateIndicatorPosition();
		}
		else if (Input.GetMouseButtonDown(0) || bDrag) {
			bDrag = true;
			Camera cam = GamePlay._instance.obCamera;
			Vector3 posMouse = cam.ScreenToWorldPoint(Input.mousePosition);
			OnDrag(new Vector2(posMouse.x, posMouse.y));
		}
	}
	
	public int GetLanePositionY(float nCurY) {
		float nY = nCurY + GameSettings.nPlayLaneHeight/2;
		int nLane = (int)(((nY - GameSettings.nPlayAreaStartPositionY)/GameSettings.nPlayLaneHeight) - 0.5f);
		
		if(nLane < 0) nLane = 0;
		else if(nLane >GameSettings.nPlayLaneCount-1) nLane = GameSettings.nPlayLaneCount-1;

		return GameSettings.listPlayLaneCenterY[nLane];
	}
	
	void UpdateIndicatorPosition() {
		SetIndicatorVisible(true);
		
		float nX = GameSettings.nPlayerPositionX;
		float nY = nMoveToPositionY;
		transform.position = new Vector3(nX, nY, transform.position.z);
		
		obAni.Play ("Button_Lane");
		obAni.animationCompleteDelegate = OnCompleteShowAnimation;	
	}

	public virtual void OnCompleteShowAnimation(tk2dAnimatedSprite sprite, int clipId) {
		SetIndicatorVisible(false);
    	}

	void SetIndicatorVisible(bool bVisible) {
		if(bVisible) {
			transform.localScale = new Vector3(0.7f, 0.7f, 1f);
		}
		else {
			transform.localScale = new Vector3(0f, 0f, 0f);
		}			
	}	
}
