using UnityEngine;
using System.Collections;

public class GameController_UpDownButton : GameController {
	public Transform pfButtonUp;
	public Transform pfButtonDown;
	Transform obButtonUp;
 	Transform obButtonDown;
	tk2dSprite obSpriteButtonUp = null;
 	tk2dSprite obSpriteButtonDown = null;
	
	void Awake() {
		obButtonUp = Instantiate(pfButtonUp, transform.position, Quaternion.identity) as Transform;
		obButtonDown = Instantiate(pfButtonDown, transform.position, Quaternion.identity) as Transform;
	 	obSpriteButtonUp = obButtonUp.gameObject.GetComponent<tk2dSprite>();
	 	obSpriteButtonDown = obButtonDown.gameObject.GetComponent<tk2dSprite>();
		
		// set the position of ButtonUp
		int nButtonGapX = 20;
		int nSizeX = (int) (obSpriteButtonUp.GetBounds().size.x);
//		int nSizeY = (int) (obSpriteButtonUp.GetBounds().size.y);
		obButtonUp.transform.position = new Vector3(nSizeX/2 + nButtonGapX, GameSettings.nGameControllerAreaWidth/2, transform.position.z);
//		obButtonUp.transform.localScale = new Vector3(1.5f, 1.5f, 1f);

		// set the position of ButtonDown
		nSizeX = (int) (obSpriteButtonDown.GetBounds().size.x);
//		nSizeY = (int) (obSpriteButtonDown.GetBounds().size.y);
		obButtonDown.transform.position = new Vector3(GameSettings.nScreenWidth - nSizeX/2 - nButtonGapX, GameSettings.nGameControllerAreaWidth/2, transform.position.z);
//		obButtonDown.transform.localScale = new Vector3(1.5f, 1.5f, 1f);
	}
	
	public override void CheckTouchInput() {
		int nCount = Input.touchCount;
		for(int i = 0; i < nCount; i++) {
			Vector2 posTouch = Input.GetTouch(i).position;
			TouchPhase phaseTouch = Input.GetTouch(i).phase;
			
			if(phaseTouch == TouchPhase.Began) {
				OnTouch(posTouch, true);
			}
			else if(phaseTouch == TouchPhase.Ended || phaseTouch == TouchPhase.Canceled) {
				OnTouch(posTouch, false);
			}
		}
		
		if(Input.GetKey (KeyCode.Escape)) {
			bEscape = true;
		}
		if(Input.GetKey (KeyCode.Menu)) {
			bMenu = true;
		}
	}

	void OnTouch(Vector2 posTouch, bool bTouch) {
		bool bButtonUp = false;
		if(posTouch.x < GameSettings.nScreenWidth/2) {
			bButtonUp = true;
		}
		
		if(bTouch) {
			if(posTouch.y > GameSettings.nPlayAreaStartPositionY + GameSettings.nPlayAreaHeight/2) {
				return;
			}
			
			if(bButtonUp) {
				bMoveUp = true;
			}
			else	{
				bMoveDown = true;
			}
		}
		
		SetButtonState(bButtonUp, bTouch);
	}
	
	void SetButtonState(bool bButtonUp, bool bTouch) {
		if(bButtonUp) {
			if(bTouch) {
				obSpriteButtonUp.spriteId = obSpriteButtonUp.GetSpriteIdByName("Button_Up_Touch");
			}
			else {
				obSpriteButtonUp.spriteId = obSpriteButtonUp.GetSpriteIdByName("Button_Up");
			}			
		}
		else {
			if(bTouch) {
				obSpriteButtonDown.spriteId = obSpriteButtonDown.GetSpriteIdByName("Button_Down_Touch");
			}
			else {
				obSpriteButtonDown.spriteId = obSpriteButtonDown.GetSpriteIdByName("Button_Down");
			}			
		}
	}
	
	public override void CheckMouseInput() {
		if (Input.GetMouseButtonDown(0)) {
			Camera cam = GamePlay._instance.obCamera;
			Vector3 posMouse = cam.ScreenToWorldPoint(Input.mousePosition);			
			OnTouch(posMouse, true);
		}
		else if (Input.GetMouseButtonUp(0)) {
			Camera cam = GamePlay._instance.obCamera;
			Vector3 posMouse = cam.ScreenToWorldPoint(Input.mousePosition);			
			OnTouch(posMouse, false);
		}
	}
		

	public override void ShowController(bool bShow) {
		obButtonUp.gameObject.SetActive(bShow);
		obButtonDown.gameObject.SetActive(bShow);
	}
}
