using UnityEngine;
using System.Collections;

public class Friend : MonoBehaviour {
/*	public enum FriendType {
		Normal = 0,
		
		random = -1				// for special purpose
	}
*/	
	public enum MoveType {
		simple,
		flow,
/*		slowfast,
		fastslow, 
		bounce,
		twobounce,
*/		
		dragstart,
		drag,
		random	= -1			// for special purpose		
	}
	
	public string strName;
	public string strImageURL;
	public Player.PlayerType nPlayerType;
	public int nFishCount;

	protected MoveType nMoveType = MoveType.flow;	
//	public FriendType nFriendType;
	protected float nSpeed = GameSettings.nFriendMoveSpeed;
	protected Vector3 posEnd;
	protected const int nEndPositionX = -500;
	bool bDie = false;
		
	bool bMagnetDrag = false;
	float nMagnetDragDistance = (int)(GameSettings.nScreenWidth*3/4);	//  /2);	//yj edit : 자석 범위 조절
	int nMagnetDragPlayerLane = -1;
	
	bool bPaused = false;

	public tk2dTextMesh obName;
 	public tk2dSprite obUserImage;
	tk2dSpriteCollectionData cdUserImage = null;
 	tk2dSprite obPlayerImage = null;
	
	public bool bAbsorbed = false;
	int nAbsorbedPlayerLane = -1;
	float nAbsorbedTime = 0;
	
	protected FriendSettings.Property obInitProp;
	
	// Use this for initialization
	void Start () {
		SetProperty();
		SetDisplay();
		
		posEnd = new Vector3(nEndPositionX, transform.position.y, transform.position.z);
		Move();
	}
	
	public void SetProperty() {
		switch(nPlayerType) {
		case Player.PlayerType.whale : obInitProp = FriendSettings.obWhale; break;
		case Player.PlayerType.polarbear : obInitProp = FriendSettings.obPolaBear; break;
		case Player.PlayerType.pororo : obInitProp = FriendSettings.obPororo; break;
		case Player.PlayerType.mobydick : obInitProp = FriendSettings.obMobydick; break;
		case Player.PlayerType.turtle : obInitProp = FriendSettings.obTurtle; break;
		case Player.PlayerType.octopus : obInitProp = FriendSettings.obOctopus; break;
		}
	}
	
	void SetDisplay() {
		// Set Player Image
		obPlayerImage = GetComponent<tk2dSprite>();
		obPlayerImage.spriteId = obPlayerImage.GetSpriteIdByName(obInitProp.strPlayerImage);
		obPlayerImage.scale = new Vector3(obInitProp.nInitScale, obInitProp.nInitScale, 1f);
		obPlayerImage.FlipX();
		
		// Set User Image
		if(strImageURL != "") {
			StartCoroutine(_SetProfilePic(ServerConnector.GetInstance().strDomainName + strImageURL));
		}
		
		// Set Name
		obName.text = strName;
		obName.Commit();
	}
	
	protected IEnumerator _SetProfilePic(string strImageURLPath) {
		WWW www = new WWW(strImageURLPath);
     		yield return www;
		
		Texture2D texture = www.texture;			
		tk2dRuntime.SpriteCollectionSize sizeCollection = tk2dRuntime.SpriteCollectionSize.ForTk2dCamera ();
   		Rect rcSize = new Rect(0, 0, texture.width, texture.height);
   		Vector2 posAnch = new Vector2(texture.width/2, texture.height/2);
		
		cdUserImage = tk2dRuntime.SpriteCollectionGenerator.CreateFromTexture(texture, sizeCollection, rcSize, posAnch);
		obUserImage.SwitchCollectionAndSprite(cdUserImage, 0);
		obUserImage.spriteId = 0;
		
		float nScale = (float)GameSettings.nFriendImageSize/texture.width;
		obUserImage.scale = new Vector3(nScale, nScale, 1f);
	}
	
	// Update is called once per frame
	void Update () {
		CheckGamePause();
		if(GamePlay.nGameStatus != GamePlay.GameStatus.PLAY) {
			return;
		}
		
		CheckMagnetDrag();
		
		UpdateAbsorbed();
		UpdateMagnetDrag();
	}
	
	public void Move() {
		 iTween.EaseType easeType = iTween.EaseType.linear;
		switch(nMoveType) {
		case MoveType.simple : easeType = iTween.EaseType.linear; break;
		case MoveType.flow : easeType = iTween.EaseType.linear; break;
/*		case MoveType.slowfast : easeType = iTween.EaseType.easeInCubic; break;
		case MoveType.fastslow : easeType = iTween.EaseType.easeInCubic; break;
		case MoveType.bounce : easeType = iTween.EaseType.easeInBounce; break;
		case MoveType.twobounce : easeType = iTween.EaseType.easeInOutBounce; break;
		case MoveType.dragstart : easeType = iTween.EaseType.easeInOutSine; break;
		case MoveType.drag : easeType = iTween.EaseType.easeOutSine; break;
*/		
		}
		
		float nMoveSpeed = nSpeed * GamePlay.nGameSpeedChangeRate;
		
		if(nMoveType == MoveType.flow) {
			
			int nMovePointCount = 10;
			Vector3[] posPath = new Vector3[nMovePointCount];
			
			float nGapX = (transform.position.x - posEnd.x) / (nMovePointCount);
			float nGapY = 50;
			
			float nX = transform.position.x;
			float nY = transform.position.y;
			for(int i = 0; i < nMovePointCount; i++) {
				nX -= nGapX;
				if(nY + nGapY > GameSettings.nPlayAreaEndPositionY) {
					nY = nY + Random.Range(-nGapY, 0);
				}
				else if(nY - nGapY < GameSettings.nPlayAreaStartPositionY) {
					nY = nY + Random.Range(0,nGapY);
				}
				else {
					nY = nY + Random.Range(-nGapY, nGapY);
				}
				
				posPath[i] = new Vector3(nX, nY, transform.position.z);
			}
			
			Hashtable tweenParam = new Hashtable();
			tweenParam.Add ("path", posPath);
			tweenParam.Add ("easetype", easeType);
			tweenParam.Add ("speed", nMoveSpeed);
			tweenParam.Add ("oncomplete", "OnFinishMove");
			iTween.MoveTo(gameObject, tweenParam);
		}
		else {
			Hashtable tweenParam = new Hashtable();
			tweenParam.Add ("position", posEnd);
			tweenParam.Add ("easetype", easeType);
			tweenParam.Add ("speed", nMoveSpeed);
			tweenParam.Add ("oncomplete", "OnFinishMove");
			iTween.MoveTo(gameObject, tweenParam);
		}
	}	
	
	public void Stop() {
		iTween.Stop(gameObject);
	}
	
	void OnFinishMove() {
/*		UIWidget[] obChildren = GetComponentsInChildren<UIWidget>();
		foreach(UIWidget obChild in obChildren) {
			Destroy(obChild.gameObject);
		}
*/		
		if(cdUserImage) {
			Destroy(cdUserImage.gameObject);
		}
		
		Destroy(gameObject);
	}
	
	void OnTriggerEnter(Collider other) {
		CheckCollision(other.gameObject);
	}
	
	void CheckCollision(GameObject obOther) {
		if(bDie || GamePlay.obPlayer.bDie) {
			return;
		}
		
		if(bAbsorbed) {
			return;
		}
		
		Player obPlayer = obOther.GetComponent("Player") as Player;
		if(obPlayer) {
			obPlayer.Attack();
			Absorbed();
		}
	}
	
	void Die() {
		if(bDie) {
			return;
		}
/*		
		if(obAniBackground) {
			Stop();
			
			bDie = true;
			obAniBackground.Play("Friend_Background_Die");		
			
			obAniBackground.animationCompleteDelegate = OnCompleteDisapearAnimation;				
			
		}
		else {
*/		
		OnFinishMove();
//		}
	}
	
/*	void OnCompleteDisapearAnimation(tk2dAnimatedSprite sprite, int clipId) {
		OnFinishMove();
    	}						
*/	
	public void CheckMagnetDrag() {
		if(bDie) {
			return;
		}
		
		if(bMagnetDrag) {
			return;
		}
		
		if(GamePlay.obPlayer.bMagnetMode == false) {
			return;	
		}
		
		bool bCollision = false;
		int nLane = GetLane();
		if(nLane-1 <= GamePlay.nPlayerLane && nLane+1 >= GamePlay.nPlayerLane) {
			bCollision = true;
		}
		
		if(!bCollision) {
			return;
		}
		
		int nXDistance = (int)Mathf.Abs(transform.position.x - GameSettings.nPlayerPositionX);
		if(transform.position.x>=0 && nXDistance < nMagnetDragDistance) {
			bMagnetDrag = true;
			nMagnetDragPlayerLane = -1;
		}
	}
		
	void UpdateMagnetDrag() {
		if(bDie) {
			return;
		}
		
		if(!bMagnetDrag) {
			return;
		}
		
		if(GamePlay.nPlayerLane != nMagnetDragPlayerLane) {
			nSpeed = GameSettings.nFriendMoveSpeed * 1.5f;
			if(nMagnetDragPlayerLane == -1) {
				nMoveType = MoveType.dragstart;
			}
			else {
				nMoveType = MoveType.drag;
			}
	
			int nX = GameSettings.nPlayerPositionX;
			int nY = GameSettings.listPlayLaneCenterY[GamePlay.nPlayerLane];
			posEnd = new Vector3(nX, nY, transform.position.z);
			Move();
			
			nMagnetDragPlayerLane = GamePlay.nPlayerLane;
		}
	}
	
	public int GetLane() {
		float nY = transform.position.y + GameSettings.nPlayLaneHeight/2;
		int nLane = (int)(((nY - GameSettings.nPlayAreaStartPositionY)/GameSettings.nPlayLaneHeight) - 0.5);
		
		if(nLane < 0) nLane = 0;
		else if(nLane > GameSettings.nPlayLaneCount-1) nLane = GameSettings.nPlayLaneCount-1;
		
		return nLane;
	}
	
	void CheckGamePause() {
		if(!bPaused && GamePlay.nGameStatus == GamePlay.GameStatus.PAUSE) {
			bPaused = true;
			iTween.Pause(gameObject);
		}
		else if(bPaused && GamePlay.nGameStatus == GamePlay.GameStatus.PLAY) {
			bPaused = false;
			iTween.Resume(gameObject);
		}
	}	
	
	public void AbsorbedToPlayer() {
		 iTween.EaseType easeType = iTween.EaseType.linear;
		float nMoveTime = GameSettings.nFishAbosorbTime - nAbsorbedTime;
		if(nMoveTime < 0f) {
			nMoveTime = 0f;
		}
		
		nAbsorbedPlayerLane = GamePlay.nPlayerLane;
			
		int nX = (int)(GameSettings.nPlayerPositionX + transform.position.x)/2;
		int nY = GameSettings.listPlayLaneCenterY[nAbsorbedPlayerLane];
		posEnd = new Vector3(nX, nY, transform.position.z);
		
		Hashtable tweenParam = new Hashtable();
		tweenParam.Add ("position", posEnd);
		tweenParam.Add ("easetype", easeType);
		tweenParam.Add ("time", nMoveTime);
		tweenParam.Add ("oncomplete", "OnFinishAbsorbedToPlayer");
		
		iTween.MoveTo(gameObject, tweenParam);
	}
	
	void OnFinishAbsorbedToPlayer() {
		SetResultFriendDead();
//		Die();
	}
	
	void UpdateAbsorbed() {
		if(bDie) {
			return;
		}	
		
		if(!bAbsorbed) {
			return;
		}
		
		nAbsorbedTime += Time.deltaTime;
		
		if(GamePlay.nPlayerLane != nAbsorbedPlayerLane) {
			AbsorbedToPlayer();
		}
	}
	
	public void Absorbed() {
		if(bDie) {
			return;
		}
		
		Stop();
		
		bAbsorbed = true;
		nAbsorbedTime = 0;
		
		ChangeSize();
		AbsorbedToPlayer();
	}
	
	void ChangeSize() {
		 iTween.EaseType easeType =  iTween.EaseType.easeInOutSine;
		
		Vector3 scaleChageSize = new Vector3(1, 1, 1f);
		if(bAbsorbed) {
			transform.localScale = new Vector3(0.8f, 0.8f, 1f);
			scaleChageSize = new Vector3(0.2f, 0.2f, 1f);
		}
		float nChageSizeTime = GameSettings.nFishAbosorbTime;		
		
		Hashtable tweenParam = new Hashtable();
		tweenParam.Add ("scale", scaleChageSize);
		tweenParam.Add ("easetype", easeType);
		tweenParam.Add ("time", nChageSizeTime);
		
		iTween.ScaleTo(gameObject, tweenParam);
	}
	
	protected void SetResultFriendDead() {
		for(int i = 0; i < GlobalValues.friendListInGame.Count; i++) {
			GlobalValues.GameContact contact = (GlobalValues.GameContact)GlobalValues.friendListInGame[i];
			if(contact.name == strName) {
				contact.isDead = true;
				GlobalValues.friendListInGame[i] = contact;

				GetDigestFishes(contact.guid);
				break;
			}
		}
	}
		
	protected void GetDigestFishes(int nGuid) {
		ServerConnector.ServerConnectionFinished callback = delegate(int result)
		{
			int nDigestFishes = result; 	// result contains the count of digest fishes.
			Debug.Log(">>>>>>>>>>>>>>> nGuid : " + nGuid + ", nDigestFish : " + nDigestFishes);

			// Update Result Data
			for(int i = 0; i < GlobalValues.friendListInGame.Count; i++) {
				GlobalValues.GameContact contact = (GlobalValues.GameContact)GlobalValues.friendListInGame[i];
				if(contact.guid == nGuid) {
					contact.digestFishes = nDigestFishes;
					GlobalValues.friendListInGame[i] = contact;
					
					EffectManager.ShowEffect_Fish_Digest_Count(new Vector2(transform.position.x, transform.position.y), nDigestFishes);
					GamePlay.SoundEffect(GamePlay._instance.sndFish_Die);
					break;
				}
			}
			
			Die();
		};
		
		ServerConnector.GetInstance().GetDigestFishes(nGuid, callback);
	}	
}	
