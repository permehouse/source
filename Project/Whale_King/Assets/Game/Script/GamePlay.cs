using UnityEngine;
using System.Collections;

public class GamePlay : MonoBehaviour {
	static public GamePlay _instance = null;
	
	public enum GameStatus {
		READY,
		PLAY,
		PAUSE,
		RESUME,
		END,
		QUIT
	}

	public enum LevelGageStatus {
		NORMAL_UP,
		AUTO_DOWN,
	}
	
	static public GameStatus nGameStatus = GameStatus.READY;
	static public float nStageGameSpeed = GameSettings.nGameSpeed;
	static public float nGameSpeed = GameSettings.nGameSpeed;
	static public float nGameSpeedChangeRate = 1f;

	static public float nLevelGage = 0;
	static public LevelGageStatus nLevelGageStatus = LevelGageStatus.NORMAL_UP;
	static public float nLevelGageDownSpeed = 10f;		// 10 per second.
	
	static public Player obPlayer = null;
	static public Pet obPet = null;
	static public int nPlayerLane = 0;
	static public int nPlayerPreviousLane = 0;
	
	// Score and Distance
	static public float nEatUpFishScore = 0;
	static public float nDigestFishScore = 0;
	static public int nGameScore = 0;	
	static public float nGameDistance = 0;
	float nScoredDistance = 0;
	int nScoredDistanceUnit = 1;
	
	// For Player	
	public Player pfPlayer_Whale;
	public Player pfPlayer_PolarBear;
	public Player pfPlayer_Pororo;
	public Player pfPlayer_Mobydick;
	public Player pfPlayer_Turtle;
	public Player pfPlayer_Octopus;
	
	// For Fish Prefab
	public static int nCountFish = 8;
//	public Fish[] pfFishList = new Fish[nCountFish];

	public Fish pfFish_Bonus;
	public Fish pfFish_Carp;
	public Fish pfFish_RedCarp;
	public Fish pfFish_Crab;
	public Fish pfFish_Fugu;
	public Fish pfFish_MiddleCarp;
	public Fish pfFish_Croc;
	public Fish pfFish_Shark;
	public Fish pfFish_RedShark;

	// For Pet Prefab
	public static int nCountPet = 1;

	public Pet pfPet_Dolphin;
	public Pet pfPet_HammerShark;
	public Pet pfPet_LittleShark;
	public Pet pfPet_Mermaid;
	public Pet pfPet_Jellyfish;
	public Pet pfPet_Walrus;
	
	public Pet pfPet_Topshell;
	public Pet pfPet_Starfish;
	public Pet pfPet_Squid;
	public Pet pfPet_Ray;
	public Pet pfPet_Penguin;
	public Pet pfPet_Clam;

	
	// For Item Prefab
	public Transform pfItem_Background;
	
	public static int nCountItem = 7;
	public Item[] pfItemList = new Item[nCountItem];
	public int[] freqAppearItemList = new int[nCountItem];
	public int nMaxAppearItemList = 0;
	
	// For Friend Prefab
	public Friend pfFriend;
	
	// For Effect Prefab
	public Effect pfEffect_SWA;
	public Effect pfEffect_Fish_Die;
	public Effect pfEffect_Fish_Hunted;
	public Effect pfEffect_Rush;
	public Effect pfEffect_Player_Bubble;
	public Effect pfEffect_Player_Die;
	public Effect pfEffect_BG_Bubble;
	public Effect pfEffect_Combo;
	public Effect pfEffect_BG_Flow_Bubble;
	public Effect pfEffect_Possess_Fish;
	public Effect pfEffect_Ink;
	public Effect pfEffect_StartRun;
	public Effect pfEffect_LastRun;
	public Effect pfEffect_Fish_Digest_Count;
	public Effect pfEffect_Fish_Hunt_Count;
	public Effect pfEffect_Item_Appear;
	
	public int nProbAppearWeakEnemy = 50;
	
	public bool bShowDebugText = false;
	public GUIText txtDebug;
	static public string strDebug = null;
	public int nFishLevelDebug = -1;
	public int nFishTypeDebug = -1;
	public int nItemTypeDebug = -1;
	
	// GameController
	public GameController obGCDefault;
	public GameController_Joystick obGCJoystick;
	public GameController_FloatingButton obGCFloatingButton;
	public GameController_LaneButton obGCLaneButton;
	public GameController_FreeTouch obGCFreeTouch;
	public GameController_UpDownButton obGCUpDownButton;
	
	public GameController obGC = null;
	
	// WorldManager
	static public WorldManager iWorldManager;

	// Sound
	public AudioClip sndBGM;
	public AudioClip sndItem;
	public AudioClip sndFish_Die;
	public AudioClip sndPlayer_Die;
	public AudioClip sndGameOver;

	public AudioSource audioBGM;
		
	// FishCombo
	static public FishCombo iFishCombo;
	
	// Camera
	public Camera obCamera;
	
	void Awake() {
		_instance = this;
		
		InitComponents();
		
		// for test
		InitGameInput();
	}
	
	void InitGameInput() {
		GlobalValues.gamePlay.player.nPlayerType = Player.PlayerType.whale;
		GlobalValues.gamePlay.playerRelay.nPlayerType = Player.PlayerType.polarbear;
		GlobalValues.gamePlay.pet.nPetType = Pet.PetType.jellyfish;
	}
	
	void InitComponents() {
		if(Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {
/*			int nGapX = GameSettings.nScreenWidth / 10;
			int nGapY = GameSettings.nScreenHeight / 5;
			Vector3 posSpawn = new Vector3(nGapX, nGapY, GameSettings.LAYER_TOPMOST);
			Instantiate(obGCJoystick, posSpawn, Quaternion.identity);
			posSpawn = new Vector3(GameSettings.nScreenWidth - nGapX, nGapY, GameSettings.LAYER_TOPMOST);
			Instantiate(obGCJoystick, posSpawn, Quaternion.identity);
*/
			
/*			int nGapX = GameSettings.nScreenWidth / 12;
			Vector3 posSpawn = new Vector3(nGapX, 0, GameSettings.LAYER_TOPMOST);
			Instantiate(obGCFloatingButton, posSpawn, Quaternion.identity);
*/			
			Vector3 posSpawn = new Vector3(0, 0, GameSettings.LAYER_MENU);					// UP DOWN 버튼.
			if(GlobalValues.settings.dragController == 1) {
				obGC = Instantiate(obGCFreeTouch, posSpawn, Quaternion.identity) as GameController;		// Free Touch 방식.
			}
			else {
				obGC = Instantiate(obGCUpDownButton, posSpawn, Quaternion.identity) as GameController;		// UP DOWN 버튼.
			}
		}
		else {
//			Instantiate(obGC, new Vector3(0, 0, 0), Quaternion.identity);
			Vector3 posSpawn = new Vector3(0, 0, GameSettings.LAYER_MENU);
			if(GlobalValues.settings.dragController == 1) {
				obGC = Instantiate(obGCFreeTouch, posSpawn, Quaternion.identity) as GameController;		// Free Touch 방식.
			}
			else {
				obGC = Instantiate(obGCUpDownButton, posSpawn, Quaternion.identity) as GameController;		// UP DOWN 버튼.
			}
		}
		
		iWorldManager = new WorldManager();
		iFishCombo = new FishCombo();
	}
		
	// Use this for initialization
	void Start () {
		nGameStatus = GameStatus.PLAY;
		
		nStageGameSpeed = GameSettings.nGameSpeed;
		
		nGameDistance = 0;
		nScoredDistance = 0;
		
		nGameScore = 0;
		nEatUpFishScore = 0;
		nDigestFishScore = 0;
		
		iWorldManager.SelectWorld();

		//Debug.Log ("Set freqAppearItemList");
		
		// set frequency of item appearance. close to 0 : seldom, close to 100 : often
		freqAppearItemList[(int)Item.ItemType.GoldFish] =  100;		
		freqAppearItemList[(int)Item.ItemType.Magnet] =  100;		
		freqAppearItemList[(int)Item.ItemType.PinkWhale] =  100;		
		freqAppearItemList[(int)Item.ItemType.StarStick] =  100;		
		freqAppearItemList[(int)Item.ItemType.Wing] =  100;		
		freqAppearItemList[(int)Item.ItemType.Heart] =  100;		
		freqAppearItemList[(int)Item.ItemType.RandomBox] =  100;		

		//Debug.Log ("Set freqAppearItemList End");
		
		nMaxAppearItemList = 0;
		for(int i = 0; i < nCountItem; i++) {
			nMaxAppearItemList += freqAppearItemList[i];
		}
		
		//Debug.Log ("Do SpawnPlayer");
		
		SpawnPlayer();
		SpawnPet();
		
		SoundBGM ();
	}
	
	// Update is called once per frame
	void Update () {
		if(nGameStatus != GameStatus.PLAY) {
			return;
		}
		
		UpdateSpeed();
		
		float dxDistance =  nGameSpeed * Time.deltaTime;
		nGameDistance += dxDistance;
		
		iWorldManager.UpdateDistance(nGameDistance);
		
//		CheckUpdateLevel();
		
//		SpawnBonus();
//		SpawnEnemy();
//		SpawnItem();
		
		UpdatePlayerLane();
		UpdatePetLane();
		UpdateGameScoreByDistance();
	}
	
	void UpdateSpeed() {
		nGameSpeed = nStageGameSpeed;
			
		// fever and power mode
		if(GamePlay.obPlayer.bBossMode) {
			nGameSpeed *= GameSettings.nBossSpeedUp;
		}
		else if(GamePlay.obPlayer.bFeverMode) {
			nGameSpeed *= GameSettings.nFeverSpeedUp;
		}
		else if(GamePlay.obPlayer.bPowerMode) {
			nGameSpeed *= GameSettings.nPowerSpeedUp;
		}
		else if(GamePlay.obPlayer.bStartRun) {
			nGameSpeed *= GameSettings.nStartRunSpeedUp;
		}
		else if(GamePlay.obPlayer.bLastRun) {
			nGameSpeed *= GameSettings.nLastRunSpeedUp;
		}
		
		nGameSpeedChangeRate = nGameSpeed / GameSettings.nGameSpeed;
	}
	
	void UpdatePlayerLane() {
		nPlayerLane = obPlayer.GetLane();
	}
	
	void UpdatePetLane() {
		if(nPlayerPreviousLane != nPlayerLane) {
			obPet.MoveToLane(nPlayerPreviousLane);
			nPlayerPreviousLane = nPlayerLane;
		}
	}

	void SpawnPlayer() {
		// Set Player's Boost
		BoostSettings.SetPlayerBoost();
		
		Player.PlayerType nType = GlobalValues.gamePlay.player.nPlayerType;
		nPlayerLane = Mathf.CeilToInt(GameSettings.nPlayLaneCount/2);
		nPlayerPreviousLane = nPlayerLane;
		
		SpawnManager.SpawnPlayer(nType, nPlayerLane, false);
	}
	
	public void SpawnRelayPlayer() {
		Player.PlayerType nType = GlobalValues.gamePlay.playerRelay.nPlayerType;
		
		SpawnManager.SpawnPlayer(nType, nPlayerLane, true);
	}

	void SpawnPet() {
		Pet.PetType nType = GlobalValues.gamePlay.pet.nPetType;
		
		SpawnManager.SpawnPet(nType, true);
	}
	
	void UpdateGameScoreByDistance() {
		int nCurDist = ((int)nGameDistance) / nScoredDistanceUnit;
		int nBeforeDist = ((int)nScoredDistance) / nScoredDistanceUnit;
		
		if (nCurDist > nBeforeDist) {
			nScoredDistance = nGameDistance;
			
			int nScore = (nCurDist - nBeforeDist) * 10;
			ScoreManager.AddGameScore(nScore);
		}
	}
	
	void OnGUI() {
		if(bShowDebugText) {
			txtDebug.enabled = true;
			
			if(strDebug != null) {
				txtDebug.text = strDebug;
			}
			else {
				txtDebug.text = "Distance : " + (int)nGameDistance
					+ 	", PlayerLevel : " + obPlayer.nLevel
					+ 	", LevelGage : " + obPlayer.nLevelGage
					+ 	", PlayerHP : " + (int)obPlayer.nHP
					+ 	", GameScore : " + nGameScore;
			}
		}
		else {
			txtDebug.enabled = false;
		}
	}
	
	static public void OnPlayerDead() {
//		SetGameResult();		
		MenuManager.EndGame();
	}
	
	static void SetGameResult() {
		GlobalValues.gamePlay.score = nGameScore;
		GlobalValues.gamePlay.distance = (int)nGameDistance;
		GlobalValues.gamePlay.fishes = (int)nEatUpFishScore;
		GlobalValues.gamePlay.digestFishes = (int)nDigestFishScore;
		
		Debug.Log("End Game, score : " + GlobalValues.gamePlay.score);
		Debug.Log("End Game, distance : " + GlobalValues.gamePlay.distance);		
		Debug.Log("End Game, fishes : " + GlobalValues.gamePlay.fishes);
		Debug.Log("End Game, digestFishes : " + GlobalValues.gamePlay.digestFishes);		
	}

	static public void OnControllerUp() {
		if(nGameStatus != GameStatus.PLAY) {
			return;
		}
		
		obPlayer.MoveUp();
	}
	static public void OnControllerDown() {
		if(nGameStatus != GameStatus.PLAY) {
			return;
		}
		
		obPlayer.MoveDown();
	}
	static public void OnControllerMoveToLane(int nLane) {
		if(nGameStatus != GameStatus.PLAY) {
			return;
		}
		
		obPlayer.MoveToLane(nLane);
	}
	static public void OnControllerMoveToPositionY(float nY) {
		if(nGameStatus != GameStatus.PLAY) {
			return;
		}
		
		obPlayer.MoveToPositionY(nY);
	}
	static public void OnControllerMenu() {
		// Do nothing
	}
	static public void OnControllerEscape() {
		// Do nothing
	}
	
	static public void ShowController(bool bShow) {
		_instance.obGC.ShowController(bShow);
	}

	static public void SoundBGM() {
		if(GlobalValues.settings.soundPlay == 0) {
			return;
		}
		
		_instance.audioBGM = _instance.GetComponent<AudioSource>();
		_instance.audioBGM.clip = _instance.sndBGM;
		_instance.audioBGM.Play();
	}
	
	static public void SoundEffect(AudioClip audioClip) {
		if(GlobalValues.settings.soundPlay == 0) {
			return;
		}
		
		AudioSource.PlayClipAtPoint(audioClip, GameSettings.posSound);
	}
}
