using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour {
	public enum ItemType {
		GoldFish = 0,
		Magnet = 1,
		PinkWhale = 2,
		StarStick = 3,
		Wing = 4,
		Heart = 5,
		RandomBox = 6,
		
		random = -1				// for special purpose
	}
	
	public enum MoveType {
		simple,
		slowfast,
		fastslow,
		bounce,
		twobounce,
		dragstart,
		drag,
		
		random	= -1			// for special purpose		
	}

	public MoveType nMoveType = MoveType.simple;	
	public Transform obBackground = null;
	public ItemType nItemType;
	protected float nSpeed = GameSettings.nItemMoveSpeed;
	protected Vector3 posEnd;
	protected const int nEndPositionX = -500;
	bool bDisappear = false;
    	public tk2dAnimatedSprite obAniItem = null;
    	public tk2dAnimatedSprite obAniBackground = null;
		
	bool bMagnetDrag = false;
	float nMagnetDragDistance = (int)(GameSettings.nScreenWidth*3/4);	//  /2);	//yj edit : 자석 범위 조절
	int nMagnetDragPlayerLane = -1;
	
	bool bPaused = false;
	
	// Use this for initialization
	void Start () {
		obAniItem = GetComponent<tk2dAnimatedSprite>();
		if(obBackground) {
			obAniBackground = obBackground.GetComponent<tk2dAnimatedSprite>();
			obAniBackground.Play("Item_Background");		
		}
		
		posEnd = new Vector3(nEndPositionX, transform.position.y, transform.position.z);
		Move();
	}
	
	// Update is called once per frame
	void Update () {
		CheckGamePause();
		if(GamePlay.nGameStatus != GamePlay.GameStatus.PLAY) {
			return;
		}
		
		if(obBackground) {
			obBackground.transform.position = transform.position;
		}
		
		CheckMagnetDrag();
		UpdateMagnetDrag();
	}
	
	public void Move() {
		 iTween.EaseType easeType = iTween.EaseType.linear;
		switch(nMoveType) {
		case MoveType.simple : easeType = iTween.EaseType.linear; break;
		case MoveType.slowfast : easeType = iTween.EaseType.easeInCubic; break;
		case MoveType.fastslow : easeType = iTween.EaseType.easeInCubic; break;
		case MoveType.bounce : easeType = iTween.EaseType.easeInBounce; break;
		case MoveType.twobounce : easeType = iTween.EaseType.easeInOutBounce; break;
		case MoveType.dragstart : easeType = iTween.EaseType.easeInOutSine; break;
		case MoveType.drag : easeType = iTween.EaseType.easeOutSine; break;
		}
		
		float nMoveSpeed = nSpeed * GamePlay.nGameSpeedChangeRate;
		
		Hashtable tweenParam = new Hashtable();
		tweenParam.Add ("position", posEnd);
		tweenParam.Add ("easetype", easeType);
		tweenParam.Add ("speed", nMoveSpeed);
		tweenParam.Add ("oncomplete", "OnFinishMove");
		iTween.MoveTo(gameObject, tweenParam);
	}	
	
	public void Stop() {
		iTween.Stop(gameObject);
	}
	
	void OnFinishMove() {
		if(obBackground) {
			Destroy(obBackground.gameObject);
		}
		Destroy(gameObject);
	}
	
	void OnTriggerEnter(Collider other) {
		CheckCollision(other.gameObject);
	}
	
	void CheckCollision(GameObject obOther) {		
		if(bDisappear || GamePlay.obPlayer.bDie || GamePlay.obPlayer.bLastRun) {
			return;
		}
		
		Player obPlayer = obOther.GetComponent("Player") as Player;
		if(obPlayer) {
			Disappear();
			obPlayer.OnGetItem(nItemType);
		}
	}
	
	void Disappear() {
		if(bDisappear) {
			return;
		}
		
		string strAniBackgroundName = "Item_Background_Disappear";
		if(nItemType == Item.ItemType.RandomBox) {
			int nMaxAppearItemList = GamePlay._instance.nMaxAppearItemList - GamePlay._instance.freqAppearItemList[(int)Item.ItemType.RandomBox];
			int nValue = Random.Range(0, nMaxAppearItemList + 1);
			int nAppearItemRange = 0;
			for(int i = 0; i < GamePlay.nCountItem; i++) {
				nAppearItemRange += GamePlay._instance.freqAppearItemList[i];
				if(nValue <= nAppearItemRange) {
					nItemType = (Item.ItemType)i;
					break;
				}
			}
			
			string strAniName = "Item_RandomBox";
			switch(nItemType) {
			case Item.ItemType.StarStick : strAniName = "Item_StarStick"; break;
			case Item.ItemType.Magnet : strAniName = "Item_Magnet"; break;
			case Item.ItemType.Wing : strAniName = "Item_Wing"; break;
			case Item.ItemType.PinkWhale : strAniName = "Item_PinkWhale"; break;
			case Item.ItemType.GoldFish : strAniName = "Item_GoldFish"; break;
			case Item.ItemType.Heart : strAniName = "Item_Heart"; break;
			}
			
			obAniItem.Play(strAniName);
			strAniBackgroundName = "Item_Background_Disappear_RandomBox";
		}
		
		if(obAniBackground) {
			Stop();
			
			bDisappear = true;
			obAniBackground.Play(strAniBackgroundName);
			
			obAniBackground.animationCompleteDelegate = OnCompleteDisapearAnimation;				
		}
		else {
			OnFinishMove();
		}
	}
	
    	void OnCompleteDisapearAnimation(tk2dAnimatedSprite sprite, int clipId) {
		OnFinishMove();
    	}						
	
	public void CheckMagnetDrag() {
		if(bDisappear) {
			return;
		}
		
		if(bMagnetDrag) {
			return;
		}
		
		if(GamePlay.obPlayer.bMagnetMode == false && GamePlay.obPlayer.bStartRun == false) {
			return;	
		}
		
/*		bool bCollision = false;
		int nLane = GetLane();
		if(nLane-1 <= GamePlay.nPlayerLane && nLane+1 >= GamePlay.nPlayerLane) {
			bCollision = true;
		}
		
		if(!bCollision) {
			return;
		}
		
		int nXDistance = (int)Mathf.Abs(transform.position.x - GameSettings.nPlayerPositionX);
		if(transform.position.x>=0 && nXDistance < nMagnetDragDistance) {
			bMagnetDrag = true;
			nMagnetDragPlayerLane = -1;
		}
*/
		float nXDistance = Vector3.Distance(transform.position, GamePlay.obPlayer.transform.position);
		if(transform.position.x>=0 && nXDistance < nMagnetDragDistance) {
			bMagnetDrag = true;
			nMagnetDragPlayerLane = -1;
		}
	}
		
	void UpdateMagnetDrag() {
		if(bDisappear) {
			return;
		}
		
		if(!bMagnetDrag) {
			return;
		}
		
		if(GamePlay.nPlayerLane != nMagnetDragPlayerLane) {
			nSpeed = GameSettings.nItemMoveSpeed * 1.5f;
			if(nMagnetDragPlayerLane == -1) {
				nMoveType = MoveType.dragstart;
			}
			else {
				nMoveType = MoveType.drag;
			}
	
			int nX = GameSettings.nPlayerPositionX;
			int nY = GameSettings.listPlayLaneCenterY[GamePlay.nPlayerLane];
			posEnd = new Vector3(nX, nY, transform.position.z);
			Move();
			
			nMagnetDragPlayerLane = GamePlay.nPlayerLane;
		}
	}
	
	public int GetLane() {
		float nY = transform.position.y + GameSettings.nPlayLaneHeight/2;
		int nLane = (int)(((nY - GameSettings.nPlayAreaStartPositionY)/GameSettings.nPlayLaneHeight) - 0.5);
		
		if(nLane < 0) nLane = 0;
		else if(nLane > GameSettings.nPlayLaneCount-1) nLane = GameSettings.nPlayLaneCount-1;
		
		return nLane;
	}
	
	void CheckGamePause() {
		if(!bPaused && GamePlay.nGameStatus == GamePlay.GameStatus.PAUSE) {
			bPaused = true;
			iTween.Pause(gameObject);
			obAniItem.Pause();
			if(obBackground) {
				iTween.Pause(obBackground.gameObject);
				obAniBackground.Pause();
			}
		}
		else if(bPaused && GamePlay.nGameStatus == GamePlay.GameStatus.PLAY) {
			bPaused = false;
			iTween.Resume(gameObject);
			obAniItem.Resume();
			if(obBackground) {
				iTween.Resume(obBackground.gameObject);
				obAniBackground.Resume();
			}
		}
	}	
}
