using UnityEngine;
using System.Collections;
using System.Globalization;

public class MenuManager : MonoBehaviour {
	static public MenuManager _instance = null;
	
	public GameObject obPause;
	public GameObject obResume;
	public GameObject obQuit;
	public GameObject obCountDown;
	
	void Awake() {
		_instance = this;
	}
	
	void Start() {
		ShowMenu (false);
		
		int nPositionX = GameSettings.nScreenWidth/2;
		int nPositionY = GameSettings.nScreenHeight/2;
		obCountDown.transform.position = new Vector3(nPositionX, nPositionY, GameSettings.LAYER_MENU);
		obCountDown.SetActive(false);
	}

	static public void ShowMenu(bool bShow) {
		_instance.obResume.SetActive(bShow);
		_instance.obQuit.SetActive(bShow);
	}

	static public void Pause() {
		if(GamePlay.nGameStatus == GamePlay.GameStatus.PLAY) {
			GamePlay.nGameStatus = GamePlay.GameStatus.PAUSE;
			GamePlay.ShowController(false);
			_instance.obPause.SetActive(false);
			
			ShowMenu(true);	
		}
	}
	
	static public void Resume() {
		if(GamePlay.nGameStatus == GamePlay.GameStatus.PAUSE) {
			MenuManager.ShowMenu(false);	
			_instance.ShowCountDown();
		}
	}
	
	static public void Quit() {
		MenuManager.ShowMenu(false);	
		GamePlay.nGameStatus = GamePlay.GameStatus.QUIT;
		
		Application.LoadLevel("Ready");		
	}

	static public void EndGame() {
		MenuManager.ShowMenu(false);	
		GamePlay.nGameStatus = GamePlay.GameStatus.END;

		Application.LoadLevel("EndGame");		
    }
	
	public void ShowCountDown() {
		_instance.obCountDown.SetActive(true);
		
		StartCoroutine(CR_CountDown());
	}
	
	IEnumerator CR_CountDown() {
		for(int nCountDown = 3; nCountDown >=1; nCountDown--) {
			_CountDown(nCountDown);
			yield return new WaitForSeconds(1f);
		}
		
		_RestartGame();
	}
	
	void _CountDown(int nCountDown) {
	 	tk2dSprite obSprite = obCountDown.GetComponent<tk2dSprite>();
		obSprite.spriteId = obSprite.GetSpriteIdByName("CountDown_" + nCountDown);
			
		obCountDown.transform.localScale = new Vector3(0f, 0f, 0f);
			
		Vector3 scaleChageSize = new Vector3(1f, 1f, 1f);
		 iTween.EaseType easeType =  iTween.EaseType.easeOutBack;
		float nChageSizeTime = 0.6f;
		
		Hashtable tweenParam = new Hashtable();
		tweenParam.Add ("scale", scaleChageSize);
		tweenParam.Add ("easetype", easeType);
		tweenParam.Add ("time", nChageSizeTime);
		
		iTween.ScaleTo(obSprite.gameObject, tweenParam);
	}
	
	void _RestartGame() {
		obCountDown.SetActive(false);
	
		obPause.SetActive(true);
		GamePlay.ShowController(true);
		GamePlay.nGameStatus = GamePlay.GameStatus.PLAY;
	}
}


