using UnityEngine;
using System.Collections;

public class SpawnManager {
	static public Player SpawnPlayer(Player.PlayerType nType, int nLane, bool bRelay) {
		Player pfPlayer = null;
		switch(nType) {
		case Player.PlayerType.whale : pfPlayer = GamePlay._instance.pfPlayer_Whale; break;
		case Player.PlayerType.polarbear : pfPlayer = GamePlay._instance.pfPlayer_PolarBear; break;
		case Player.PlayerType.pororo : pfPlayer = GamePlay._instance.pfPlayer_Pororo; break;
		case Player.PlayerType.mobydick : pfPlayer = GamePlay._instance.pfPlayer_Mobydick; break; 
		case Player.PlayerType.turtle : pfPlayer = GamePlay._instance.pfPlayer_Turtle; break; 
		case Player.PlayerType.octopus : pfPlayer = GamePlay._instance.pfPlayer_Octopus; break; 
		}
		
		if(pfPlayer == null) {
			return null;
		}
		
		int nPlayerCenterX = GameSettings.nPlayerPositionX;
		int nPlayerCenterY = GameSettings.listPlayLaneCenterY[nLane];
		Vector3 posSpawn = new Vector3(nPlayerCenterX, nPlayerCenterY, GameSettings.LAYER_PLAYER);		
		
		Player obPlayer;
		obPlayer = MonoBehaviour.Instantiate(pfPlayer, posSpawn, Quaternion.identity) as Player;
		obPlayer.bRelay = bRelay;
		
		GamePlay.obPlayer= obPlayer;
		
		return obPlayer;
	}
	
	static public Pet SpawnPet(Pet.PetType nType, bool bMyPet) {
		Pet pfPet = null;
		switch(nType) {
		case Pet.PetType.dolphin : pfPet = GamePlay._instance.pfPet_Dolphin; break;
		case Pet.PetType.hammershark : pfPet = GamePlay._instance.pfPet_HammerShark; break;
		case Pet.PetType.littleshark : pfPet = GamePlay._instance.pfPet_LittleShark; break;
		case Pet.PetType.mermaid : pfPet = GamePlay._instance.pfPet_Mermaid; break;
		case Pet.PetType.jellyfish : pfPet = GamePlay._instance.pfPet_Jellyfish; break;
		case Pet.PetType.walrus : pfPet = GamePlay._instance.pfPet_Walrus; break;
			
		case Pet.PetType.topshell : pfPet = GamePlay._instance.pfPet_Topshell; break;
		case Pet.PetType.starfish : pfPet = GamePlay._instance.pfPet_Starfish; break;
		case Pet.PetType.squid : pfPet = GamePlay._instance.pfPet_Squid; break;
		case Pet.PetType.ray : pfPet = GamePlay._instance.pfPet_Ray; break;
		case Pet.PetType.penguin : pfPet = GamePlay._instance.pfPet_Penguin; break;
		case Pet.PetType.clam : pfPet = GamePlay._instance.pfPet_Clam; break;
		}
		
		if(pfPet == null) {
			return null;
		}
		
		Vector3 posSpawn = new Vector3(0, 0, GameSettings.LAYER_PET - (int)nType);		
		
		Pet obPet;
		obPet =  MonoBehaviour.Instantiate(pfPet, posSpawn, Quaternion.identity) as Pet;
		obPet.SetMovePattern(pfPet);
		
		if(bMyPet) {
			GamePlay.obPet = obPet;
		}
		
		return obPet;
	}
		
	static public Fish SpawnFish(Fish.FishType nType, Fish.MovePatternType nMovePatternType, int nLane) {
		if(nMovePatternType == Fish.MovePatternType.MPT_MOVING_U_OR_REVERSE) {
			int nValue = Random.Range(0, 2);
			if(nValue == 0) {
				nMovePatternType = Fish.MovePatternType.MPT_MOVING_U;
			}
			else {
				nMovePatternType = Fish.MovePatternType.MPT_MOVING_U_REVERSE;
			}
		}
		else if(nMovePatternType == Fish.MovePatternType.MPT_MOVING_UP_OR_DOWN) {
			int nValue = Random.Range(0, 2);
			if(nValue == 0) {
				nMovePatternType = Fish.MovePatternType.MPT_MOVING_UP;
			}
			else {
				nMovePatternType = Fish.MovePatternType.MPT_MOVING_DOWN;
			}
		}
		
		if(nType == Fish.FishType.random) {
			nType = (Fish.FishType)Random.Range(1, GamePlay.nCountFish);
		}
		
		Fish pfFish = null;
		switch(nType) {
		case Fish.FishType.bonus : pfFish = GamePlay._instance.pfFish_Bonus; break;
		case Fish.FishType.carp : pfFish = GamePlay._instance.pfFish_Carp; break;
		case Fish.FishType.redcarp : pfFish = GamePlay._instance.pfFish_RedCarp; break;
		case Fish.FishType.crab : pfFish = GamePlay._instance.pfFish_Crab; break;
		case Fish.FishType.fugu : pfFish = GamePlay._instance.pfFish_Fugu; break;
		case Fish.FishType.middlecarp : pfFish = GamePlay._instance.pfFish_MiddleCarp; break;
		case Fish.FishType.croc : pfFish = GamePlay._instance.pfFish_Croc; break;
		case Fish.FishType.shark : pfFish = GamePlay._instance.pfFish_Shark; break;
		case Fish.FishType.redshark : pfFish = GamePlay._instance.pfFish_RedShark; break;
		}
		
		if(pfFish == null) {
			return null;
		}
		
		Vector3 posSpawn = new Vector3(0, 0, GameSettings.LAYER_FISH - (int)nType);		
		
		Fish obFish;
		obFish =  MonoBehaviour.Instantiate(pfFish, posSpawn, Quaternion.identity) as Fish;
		obFish.SetMovePattern(pfFish, nMovePatternType, nLane);
		
		return obFish;
	}
	
	static public Item SpawnItem(Item.ItemType nType, int nLane) {
		if(nType == Item.ItemType.random) {
			if(GamePlay._instance.nMaxAppearItemList == 0) {
				return null;
			}
			
			int nValue = Random.Range(0, GamePlay._instance.nMaxAppearItemList + 1);
			int nAppearItemRange = 0;
			for(int i = 0; i < GamePlay.nCountItem; i++) {
				nAppearItemRange += GamePlay._instance.freqAppearItemList[i];
				if(nValue <= nAppearItemRange) {
					nType = (Item.ItemType)i;
					break;
				}
			}
		}
		
		// for test
//		nType = Item.ItemType.RandomBox;
		
		Item pfItem = GamePlay._instance.pfItemList[(int)nType];
		if(nLane < 0 || nLane>GameSettings.nPlayLaneCount-1) {
			nLane = Random.Range(0, GameSettings.nPlayLaneCount);
		}
		int nY= GameSettings.listPlayLaneCenterY[nLane];
		int nX = GameSettings.nPlayAreaEndPositionX;
		Vector3 posSpawn = new Vector3(nX, nY, GameSettings.LAYER_ITEM);		
		Vector3 posSpawnBackground = new Vector3(nX, nY, GameSettings.LAYER_ITEM+1);
		
		Item obItem;
		obItem = MonoBehaviour.Instantiate(pfItem, posSpawn, Quaternion.identity) as Item;
		obItem.obBackground = MonoBehaviour.Instantiate(GamePlay._instance.pfItem_Background, posSpawnBackground, Quaternion.identity) as Transform;
		obItem.nItemType = nType;
		
		return obItem;
	}
	
	static public Friend SpawnFriend(string strName, string strImageURL, Player.PlayerType nPlayterType) {
		Friend pfFriend = GamePlay._instance.pfFriend;

		int nLane = Random.Range(0, GameSettings.nPlayLaneCount);
		int nY= GameSettings.listPlayLaneCenterY[nLane];
		int nX = GameSettings.nPlayAreaEndPositionX;
		Vector3 posSpawn = new Vector3(nX, nY, GameSettings.LAYER_FRIEND);		
		
		Friend obFriend;
		obFriend = MonoBehaviour.Instantiate(pfFriend, posSpawn, Quaternion.identity) as Friend;
		obFriend.strName = strName;
		obFriend.strImageURL = strImageURL;
		obFriend.nPlayerType = nPlayterType;
		
		return obFriend;
	}
}
