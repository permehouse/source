using UnityEngine;
using System.Collections;
using System.Globalization;

public class ScoreManager : MonoBehaviour {
	// Use this for initialization
	public GameObject obSA_Background;
	public GameObject obSA_Exp_Gage;
	public GameObject obSA_Exp_Power_Gage;
	public GameObject obSA_HP_Gage;
	public GameObject obSA_Stage_Gage;
	public GameObject obSA_RankUser;
	
	public tk2dTextMesh obSA_Score_Text;
	public tk2dTextMesh obSA_EatUpFish_Text;
	public tk2dTextMesh obSA_DigestFish_Text;
	public tk2dTextMesh obSA_Rank_Text;
	public tk2dTextMesh obSA_RankScore_Text;
	public tk2dTextMesh obSA_RankHunt_Text;
	public tk2dTextMesh obSA_RankUserName_Text;
	public tk2dTextMesh obSA_Distance_Text;

 	public tk2dSprite obSA_RankBackImage;
	
 	public tk2dSprite obSA_RankUserImage;
	private tk2dSpriteCollectionData cdRankUserImage = null;	
	
	public int nSA_Exp_Gage_OffsetX = 86;
	public int nPrevLevelGageIndex = -1;
	public float nPrevLevelGageAlpha = 0;

	public float nPowerModeTime = 0f;
	public float nPowerModePassedTime = 0f;
	public float nCurHPGage = 0f;
	
	private int nRankUserDistance = 0;
	
	static ScoreManager _instance = null;

	public int nSA_ScorePositionX = 55;
	public int nSA_ScorePositionY = 675;
	public int nSA_HuntPositionX = 55;
	public int nSA_HuntPositionY = 610;
	public int nSA_DigestPositionX = 210;
	public int nSA_DigestPositionY = 610;
	public int nSA_RankBackPositionX = 1090;
	public int nSA_RankBackPositionY = 662;
	public int nSA_RankTextPositionX = 1060;
	public int nSA_RankTextPositionY = 677;
	public int nSA_RankScorePositionX = 1095;
	public int nSA_RankScorePositionY = 647;
	public int nSA_RankHuntPositionX = 1095;
	public int nSA_RankHuntPositionY = 615;
	public int nSA_RankUserPositionX = 1137;
	public int nSA_RankUserPositionY = 675;
	public int nSA_RankUserNamePositionX = 1088;
	public int nSA_RankUserNamePositionY = 630;
	public int nSA_Exp_GagePositionX =338;
	public int nSA_Exp_GagePositionY = 664;
	public int nSA_HP_GagePositionX =613;
	public int nSA_HP_GagePositionY = 680;
	public int nSA_Stage_GagePositionStartX =660;
	public int nSA_Stage_GagePositionEndX =830;
	public int nSA_Stage_GagePositionY = 640;
	public int nSA_DistancePositionX = 632;
	public int nSA_DistancePositionY = 617;
	
	void Awake() {
		_instance = this;
	}
	
	void Start () {
//	 	tk2dSprite obSprite;

		// settings for Background
//		obSprite =  obSA_Background.GetComponent<tk2dSprite>();
		int nSA_BackgroundPositionX = 5;
		int nSA_BackgroundPositionY = GameSettings.nScoreAreaStartPositionY;
//		int nSA_BackgroundWidth = (int)obSprite.GetBounds().size.x;
		int nSA_BackgroundHeight = GameSettings.nScoreAreaHeight;

		int nSA_BackgroundCenterY = nSA_BackgroundPositionY + nSA_BackgroundHeight/2;
		obSA_Background.transform.position = new Vector3(nSA_BackgroundPositionX, nSA_BackgroundCenterY, GameSettings.LAYER_SCOREAREA);
		
		// settings for Score
		obSA_Score_Text.transform.position = new Vector3(nSA_ScorePositionX, nSA_ScorePositionY, GameSettings.LAYER_SCOREAREA - 1);
//		obSA_Score_Text.transform.localScale = new Vector3(nXScale_Score_Text, nYScale_Score_Text, 1);

		Color nColor_Score_Text = new Color(255f/255f, 255f/255f, 255f/255f, 1f);
		obSA_Score_Text.color = nColor_Score_Text;
		obSA_Score_Text.Commit();
		
		// settings for Hunt
		obSA_EatUpFish_Text.transform.position = new Vector3(nSA_HuntPositionX, nSA_HuntPositionY, GameSettings.LAYER_SCOREAREA - 1);
		obSA_EatUpFish_Text.color = nColor_Score_Text;
		obSA_EatUpFish_Text.Commit();
		
		// settings for Digest
		obSA_DigestFish_Text.transform.position = new Vector3(nSA_DigestPositionX, nSA_DigestPositionY, GameSettings.LAYER_SCOREAREA - 1);
		obSA_DigestFish_Text.color = nColor_Score_Text;
		obSA_DigestFish_Text.Commit();

		// settings for Rank (+ RankScore, RankHunt, RankUser)
		obSA_RankBackImage.transform.position = new Vector3(nSA_RankBackPositionX, nSA_RankBackPositionY, GameSettings.LAYER_SCOREAREA);
		obSA_Rank_Text.transform.position = new Vector3(nSA_RankTextPositionX, nSA_RankTextPositionY, GameSettings.LAYER_SCOREAREA - 1);
		obSA_Rank_Text.transform.localScale = new Vector3(0.82f, 0.82f, 1f);
		obSA_RankScore_Text.transform.position = new Vector3(nSA_RankScorePositionX, nSA_RankScorePositionY, GameSettings.LAYER_SCOREAREA - 1);
		obSA_RankHunt_Text.transform.position = new Vector3(nSA_RankHuntPositionX, nSA_RankHuntPositionY, GameSettings.LAYER_SCOREAREA - 1);
		obSA_RankUser.transform.position = new Vector3(nSA_RankUserPositionX, nSA_RankUserPositionY, GameSettings.LAYER_SCOREAREA - 1);
		obSA_RankUserName_Text.transform.position = new Vector3(nSA_RankUserNamePositionX, nSA_RankUserNamePositionY, GameSettings.LAYER_SCOREAREA - 1);
		obSA_RankUserName_Text.transform.localScale = new Vector3(0.85f, 0.85f, 1f);
		
		// settings for Level Gage
		obSA_Exp_Gage.transform.position = new Vector3(nSA_Exp_GagePositionX, nSA_Exp_GagePositionY, GameSettings.LAYER_SCOREAREA+2);
		obSA_Exp_Power_Gage.transform.position = new Vector3(nSA_Exp_GagePositionX, nSA_Exp_GagePositionY, GameSettings.LAYER_SCOREAREA+2);
		
		// settings for HP Gage
		obSA_HP_Gage.transform.position = new Vector3(nSA_HP_GagePositionX, nSA_HP_GagePositionY, GameSettings.LAYER_SCOREAREA+1);

		// settings for HP Gage
		obSA_Stage_Gage.transform.position = new Vector3(nSA_Stage_GagePositionStartX, nSA_Stage_GagePositionY, GameSettings.LAYER_SCOREAREA-1);

		// settings for Distance
		obSA_Distance_Text.transform.position = new Vector3(nSA_DistancePositionX, nSA_DistancePositionY, GameSettings.LAYER_SCOREAREA - 1);
	}
	
	// Update is called once per frame
	void Update() {
		if(GamePlay.nGameStatus != GamePlay.GameStatus.PLAY) {
			return;
		}
		
		UpdateExpGage();
		UpdateHPGage();
		UpdateStageGage();
		UpdateDistance();
		UpdateRankUserDistance();
	}

	static public void AddGameScore(int nScore) {
		GamePlay.nGameScore += nScore;
		
		_instance.obSA_Score_Text.text = GamePlay.nGameScore.ToString("N0");
		_instance.obSA_Score_Text.Commit();
	}
	
	static public void AddHuntScore(float nScore) {
		nScore *= BoostSettings.nFishUpRate;
		float nDigestRate = 0.5f + (BoostSettings.nDigestUpRate + BoostSettings.nFastDigestRateByPlayer);
		float nEatUpFishScore = (nScore*nDigestRate);
		float nDigestFishScore = nScore - nEatUpFishScore;

		AddEatUpFishScore(nEatUpFishScore);
		AddDigestFishScore(nDigestFishScore);
	}

	static public void AddEatUpFishScore(float nScore) {
		GamePlay.nEatUpFishScore += nScore;
		
		int nEatUpFishScore = (int)GamePlay.nEatUpFishScore;
		_instance.obSA_EatUpFish_Text.text = nEatUpFishScore .ToString("N0");
		_instance.obSA_EatUpFish_Text.Commit();
	}

	static public void AddDigestFishScore(float nScore) {
		GamePlay.nDigestFishScore += nScore;
		
		int nDigestFishScore = (int)GamePlay.nDigestFishScore;
		_instance.obSA_DigestFish_Text.text = nDigestFishScore.ToString("N0");
		_instance.obSA_DigestFish_Text.Commit();
	}
	
	static public void AddExpGage(float nLevelGage) {
		if(GamePlay.obPlayer.bLastRun) {
			return;
		}
		
		if(GamePlay.obPlayer.nLevel == GamePlay.obPlayer.obInitProp.nLevel) {
			GamePlay.obPlayer.nLevelGage += (nLevelGage * (BoostSettings.nExpUpRateByPlayer + BoostSettings.nExpUpRateByPet));
			if(GamePlay.obPlayer.nLevelGage >= 100f) {
				GamePlay.obPlayer.nLevelGage = 100f;
				GamePlay.obPlayer.SetLevel(GamePlay.obPlayer.nPowerModeLevel);
				
				_instance.nPowerModeTime = (GameSettings.nPowerModeTime*BoostSettings.nBonusTimeUpRateByPet);
				_instance.nPowerModePassedTime = _instance.nPowerModeTime;
			}
		}
/*		if(GamePlay.obPlayer.nLevel < GameSettings.nMaxLevel) {
			GamePlay.obPlayer.nLevelGage += nLevelGage;
			if(GamePlay.obPlayer.nLevelGage >= 100f) {
				if(GamePlay.obPlayer.nLevel == 1) {
					GamePlay.obPlayer.SetLevel(2);
				}
				else if(GamePlay.obPlayer.nLevel == 2) {
					GamePlay.obPlayer.SetLevel(GameSettings.nMaxLevel);
				}					
					
				if(GamePlay.obPlayer.nLevel >= GameSettings.nMaxLevel) {
					GamePlay.obPlayer.nLevel = GameSettings.nMaxLevel;
					GamePlay.obPlayer.nLevelGage = 100f;
				}
				else {
					GamePlay.obPlayer.nLevelGage = 0f;
				}
			}
		}
*/		
	}
	
	void UpdateExpGage() {
		if(GamePlay.obPlayer.nLevel == GamePlay.obPlayer.obInitProp.nLevel) {
			float nExp = (GamePlay.obPlayer.nLevelGage / 100f);
			UISprite obSprite = obSA_Exp_Gage.GetComponent<UISprite>();	
			obSprite.fillAmount = nExp;
			
			obSA_Exp_Gage.SetActive(true);
			obSA_Exp_Power_Gage.SetActive(false);
		}
		else {
			if(GamePlay.obPlayer.bFeverMode) {
				return;
			}
			
			nPowerModePassedTime -= Time.deltaTime;
			GamePlay.obPlayer.nLevelGage = (nPowerModePassedTime / nPowerModeTime) * 100f;
			
			if(GamePlay.obPlayer.nLevelGage <= 0f) {
				GamePlay.obPlayer.nLevelGage = 0f;
				GamePlay.obPlayer.SetLevel(GamePlay.obPlayer.obInitProp.nLevel);
			}
		
			float nExp = (GamePlay.obPlayer.nLevelGage / 100f);
		 	UISprite obSprite = obSA_Exp_Power_Gage.GetComponent<UISprite>();	
			obSprite.fillAmount = nExp;
			
			obSA_Exp_Gage.SetActive(false);
			obSA_Exp_Power_Gage.SetActive(true);
		}
		
		
/*		float nExp = (GamePlay.obPlayer.nLevelGage / 100f);
		
		if(GamePlay.obPlayer.nLevel == 1) {
		 	UISprite obSprite = obSA_Exp_Gage.GetComponent<UISprite>();	
			obSprite.fillAmount = nExp;
			
			obSA_Exp_Power_Gage.SetActive(false);
		}
		else {
		 	UISprite obSprite = obSA_Exp_Gage.GetComponent<UISprite>();	
			obSprite.fillAmount = 1f;
			
			obSA_Exp_Power_Gage.SetActive(true);
		 	obSprite = obSA_Exp_Power_Gage.GetComponent<UISprite>();	
			obSprite.fillAmount = nExp;
		}
*/		
	}
	
	void UpdateHPGage() {
		if(GamePlay.obPlayer.bSWA) {
			return;
		}
		
		float nHP = GamePlay.obPlayer.nHP;
		float nFullHP = GamePlay.obPlayer.nFullHP;
			
		nCurHPGage += (nHP - nCurHPGage) * 0.1f;
		if(Mathf.Abs (nCurHPGage - nHP) < 0.1f) {
			nCurHPGage = nHP;
		}

	 	UISprite obSprite = obSA_HP_Gage.GetComponent<UISprite>();	
		obSprite.fillAmount = (nCurHPGage / nFullHP);
	}
	
	void UpdateStageGage() {
		float nRate = GamePlay.iWorldManager.GetStageDistanceRate();
		float nPositionX = nSA_Stage_GagePositionStartX + (nSA_Stage_GagePositionEndX - nSA_Stage_GagePositionStartX) * nRate;
		
		obSA_Stage_Gage.transform.position = new Vector3(nPositionX, nSA_Stage_GagePositionY, GameSettings.LAYER_SCOREAREA-1);
	}
	
	void UpdateDistance() {
		int nDistance = (int)GamePlay.nGameDistance;
		obSA_Distance_Text.text = nDistance.ToString("0") + "M";
		obSA_Distance_Text.Commit();
	}
	
	void UpdateRankUserDistance() {
		int nDistance = nRankUserDistance - (int)GamePlay.nGameDistance;
		if(nDistance < 0) nDistance = 0;
		
		_instance.obSA_RankScore_Text.text = nDistance.ToString("0")+"M";
		_instance.obSA_RankScore_Text.Commit();
	}

	static public void SetRank_NewRecord() {
		Debug.Log("=========>>>> SetRank_NewRecord");
		
		_instance.obSA_Rank_Text.gameObject.SetActive(false);
		_instance.obSA_RankScore_Text.gameObject.SetActive(false);
		_instance.obSA_RankHunt_Text.gameObject.SetActive(false);
		_instance.obSA_RankUserName_Text.gameObject.SetActive(false);
		_instance.obSA_RankUserImage.gameObject.SetActive(false);
		_instance.obSA_RankBackImage.gameObject.SetActive(false);
		
//		_instance.obSA_RankBackImage.spriteId = _instance.obSA_RankBackImage.GetSpriteIdByName("SA_Background_Rank_NewRecord");
	}
	
	static public void SetRankUser(int nRank, string strUserName, string strImageURL, int nRankDistance) {
//		Debug.Log("=========>>>><<<< SetRankUser");
		_instance.obSA_Rank_Text.text = nRank.ToString("0");
		_instance.obSA_Rank_Text.Commit();
		
		_instance.nRankUserDistance = nRankDistance;
		_instance.obSA_RankScore_Text.text = nRankDistance.ToString("0")+"M";
		_instance.obSA_RankScore_Text.Commit();
		
		if(strUserName.Length > 11) {
			strUserName = strUserName.Substring(0, 10);
			strUserName += "..";
		}
		
		_instance.obSA_RankUserName_Text.text = strUserName;
		_instance.obSA_RankUserName_Text.Commit();

		// Set User Image
		if(strImageURL != "") {
			_instance.StartCoroutine(_instance._SetProfilePic(ServerConnector.GetInstance().strDomainName + strImageURL));
		}
	}
		
	protected IEnumerator _SetProfilePic(string strImageURLPath) {
		WWW www = new WWW(strImageURLPath);
     		yield return www;
		
		Texture2D texture = www.texture;			
		tk2dRuntime.SpriteCollectionSize sizeCollection = tk2dRuntime.SpriteCollectionSize.ForTk2dCamera ();
   		Rect rcSize = new Rect(0, 0, texture.width, texture.height);
   		Vector2 posAnch = new Vector2(texture.width/2, texture.height/2);
		
		if(cdRankUserImage != null) {
			DestroyImmediate(cdRankUserImage.gameObject);
		}
		
		cdRankUserImage = tk2dRuntime.SpriteCollectionGenerator.CreateFromTexture(texture, sizeCollection, rcSize, posAnch);
		obSA_RankUserImage.SwitchCollectionAndSprite(cdRankUserImage, 0);
		obSA_RankUserImage.spriteId = 0;
		
		float nScale = (float)GameSettings.nRankUserImageSize/texture.width;
		obSA_RankUserImage.scale = new Vector3(nScale, nScale, 1f);
	}
}
