using UnityEngine;
using System.Collections;

public class EffectManager : MonoBehaviour {
	static public EffectManager _instance = null;
	
	void Awake() {
		_instance = this;
	}	
	
	static public Effect ShowEffect_SWA(Vector2 pos) {
		return ShowEffect(Effect.EffectType.SWA, pos, false);
	}
	
	static public Effect ShowEffect_Ink(Vector2 pos) {
		return ShowEffect(Effect.EffectType.ink, pos, false);
	}
	
	static public Effect ShowEffect_Fish_Die(Vector2 pos) {
		int nOffsetX = Random.Range(-50, 0);
		int nOffsetY = Random.Range(-25, 26);
		return ShowEffect(Effect.EffectType.fish_die, new Vector2(pos.x + nOffsetX, pos.y + nOffsetY), false);
	}

	static public Effect ShowEffect_Item_Appear(Vector2 pos) {
		Effect obEffect = ShowEffect(Effect.EffectType.item_appear, pos, false);
		if(obEffect) {
			tk2dAnimatedSprite obAni = obEffect.gameObject.GetComponent<tk2dAnimatedSprite>();
			if(obAni) {
				obAni.animationCompleteDelegate = OnCompleteShowEffectItemAppear;	
			}
		}
		
		return obEffect;
	}
	
	static public void OnCompleteShowEffectItemAppear(tk2dAnimatedSprite sprite, int clipId) {
		// Spawn Item
		Item obItem = SpawnManager.SpawnItem(Item.ItemType.random, -1);
		if(obItem) {
			obItem.transform.position = sprite.gameObject.transform.position;
		}
		
		MonoBehaviour.Destroy(sprite.gameObject);
    	}						
	
	static public Effect ShowEffect_Fish_Hunted(Vector2 pos) {
		Effect obEffect = ShowEffect(Effect.EffectType.fish_hunted, new Vector2(pos.x, pos.y), true);
		
		iTween.EaseType easeType = iTween.EaseType.easeOutSine;
		float nSpeed = GameSettings.nFishMoveSpeed;
		float nMoveSpeed = 1.5f * nSpeed * GamePlay.nGameSpeedChangeRate;
		
		Vector3[] posPath = new Vector3[2];

		// end point
		float nOffsetX = Random.Range(-100f, 100f);
		float nX = GameSettings.nScoreAreaEatUpFishPositionX + nOffsetX;
		float nY =  GameSettings.nScoreAreaEatUpFishPositionY;
		posPath[1] = new Vector3(nX, nY, obEffect.transform.position.z);
		
		// mid point
		nX =  (2*nX + obEffect.transform.position.x)/3;
		nY =  (nY + obEffect.transform.position.y)/2;
		posPath[0] = new Vector3(nX, nY, obEffect.transform.position.z);
		
		Hashtable tweenParam = new Hashtable();
		tweenParam.Add ("path", posPath);
		tweenParam.Add ("easetype", easeType);
		tweenParam.Add ("speed", nMoveSpeed);
		tweenParam.Add ("oncomplete", "OnFinishEffectMove");
		tweenParam.Add ("oncompletetarget", _instance.gameObject);
		tweenParam.Add ("oncompleteparams", obEffect);		
		
		iTween.MoveTo(obEffect.gameObject, tweenParam);
		
		return obEffect;
	}
	
	static public Effect ShowEffect_Fish_Digest_Count(Vector2 pos, int nFishDigestCount) {
		if(GlobalValues.settings.lowQuality == 1) {
			ScoreManager.AddDigestFishScore(nFishDigestCount);
			return null;
		}
		
		Effect obEffect = ShowEffect(Effect.EffectType.fish_digest_count, new Vector2(pos.x, pos.y), true);
		
		tk2dTextMesh obText = obEffect.gameObject.GetComponent<tk2dTextMesh>();
		obText.text = "+" + nFishDigestCount.ToString("N0");
		obText.Commit();

		iTween.EaseType easeType = iTween.EaseType.easeOutSine;
		float nSpeed = GameSettings.nFishDigestCountSpeed;
		float nMoveSpeed = 1.5f * nSpeed * GamePlay.nGameSpeedChangeRate;
		
		Vector3[] posPath = new Vector3[2];

		// end point
		float nX = GameSettings.nScoreAreaDigestFishPositionX;
		float nY =  GameSettings.nScoreAreaDigestFishPositionY;
		posPath[1] = new Vector3(nX, nY, obEffect.transform.position.z);
		
		// mid point
		nX =  (2*nX + obEffect.transform.position.x)/3;
		nY =  (nY + obEffect.transform.position.y)/2;
		posPath[0] = new Vector3(nX, nY, obEffect.transform.position.z);
		
		Hashtable tweenParam = new Hashtable();
		tweenParam.Add ("path", posPath);
		tweenParam.Add ("easetype", easeType);
		tweenParam.Add ("speed", nMoveSpeed);
		tweenParam.Add ("oncomplete", "OnFinishEffect_Fish_Digest_Count");
		tweenParam.Add ("oncompletetarget", _instance.gameObject);
		
		obEffect.nParam = nFishDigestCount;
		tweenParam.Add ("oncompleteparams", obEffect);		
		
		iTween.MoveTo(obEffect.gameObject, tweenParam);
		
		return obEffect;
	}

	static public Effect ShowEffect_Fish_Hunt_Count(Vector2 pos, float nFishHuntCount) {
		if(GlobalValues.settings.lowQuality == 1) {
			ScoreManager.AddHuntScore(nFishHuntCount);
			return null;
		}
		
		Effect obEffect = ShowEffect(Effect.EffectType.fish_hunt_count, new Vector2(pos.x, pos.y), true);
		
		tk2dTextMesh obText = obEffect.gameObject.GetComponent<tk2dTextMesh>();
		obText.text = "+" + nFishHuntCount.ToString("N0");
		obText.Commit();

		iTween.EaseType easeType = iTween.EaseType.easeOutSine;
		float nSpeed = GameSettings.nFishHuntCountSpeed;
		float nMoveSpeed = 1.5f * nSpeed * GamePlay.nGameSpeedChangeRate;
		
		Vector3[] posPath = new Vector3[2];

		// end point
		float nX = GameSettings.nScoreAreaHuntFishPositionX;
		float nY = GameSettings.nScoreAreaHuntFishPositionY;
		posPath[1] = new Vector3(nX, nY, obEffect.transform.position.z);
		
		// mid point
		nX =  (2*nX + obEffect.transform.position.x)/3;
		nY =  (nY + obEffect.transform.position.y)/2;
		posPath[0] = new Vector3(nX, nY, obEffect.transform.position.z);
		
		Hashtable tweenParam = new Hashtable();
		tweenParam.Add ("path", posPath);
		tweenParam.Add ("easetype", easeType);
		tweenParam.Add ("speed", nMoveSpeed);
		tweenParam.Add ("oncomplete", "OnFinishEffect_Fish_Hunt_Count");
		tweenParam.Add ("oncompletetarget", _instance.gameObject);
		
		obEffect.nParam = nFishHuntCount;
		tweenParam.Add ("oncompleteparams", obEffect);		
		
		iTween.MoveTo(obEffect.gameObject, tweenParam);
		
		return obEffect;
	}
	
	static public Effect ShowEffect_Possess_Fish(Vector2 pos) {
		Effect obEffect = ShowEffect(Effect.EffectType.possess_fish, pos, false);
		
		float nTime = 1f;
		int nX = GameSettings.nScreenWidth/2;
		
		// Change Position		
		Vector3 posEnd = new Vector3(nX, obEffect.transform.position.y, obEffect.transform.position.z);
		iTween.MoveTo(obEffect.gameObject, posEnd, nTime);

		// Change Scale
		obEffect.transform.localScale = new Vector3(0f, 0f, 0f);
		iTween.ScaleTo(obEffect.gameObject, new Vector3(1f, 1f, 1f), nTime);
		
		return obEffect;
	}
	
	static public Effect ShowEffect_Rush(Vector2 pos) {
		return ShowEffect(Effect.EffectType.rush, pos, true);
	}

	static public Effect ShowEffect_StartRun(Vector2 pos) {
		return ShowEffect(Effect.EffectType.startrun, pos, true);
	}

	static public Effect ShowEffect_LastRun(Vector2 pos) {
		return ShowEffect(Effect.EffectType.lastrun, pos, true);
	}

	static public Effect ShowEffect_Player_Bubble(Vector2 pos) {
		return ShowEffect(Effect.EffectType.player_bubble, pos, false);
	}

	static public Effect ShowEffect_Player_Die(Vector2 pos) {
		return ShowEffect(Effect.EffectType.player_die, pos, true);
	}
	
	static public Effect ShowEffect_BG_Bubble(Vector2 pos) {
		if(GlobalValues.settings.lowQuality == 1) {
			return null;
		}
		
		return ShowEffect(Effect.EffectType.bg_bubble, pos, false);
	}
	
	static public Effect ShowEffect_Combo(Vector2 pos) {
		return ShowEffect(Effect.EffectType.combo, pos, false);
	}
	
	static public Effect ShowEffect(Effect.EffectType nType, Vector2 pos, bool bShowBehind) {
		Effect pfEffect = null;
		switch(nType) {
		case Effect.EffectType.SWA : pfEffect = GamePlay._instance.pfEffect_SWA; break;
		case Effect.EffectType.fish_die : pfEffect = GamePlay._instance.pfEffect_Fish_Die; break;
		case Effect.EffectType.fish_hunted : pfEffect = GamePlay._instance.pfEffect_Fish_Hunted; break;
		case Effect.EffectType.rush : pfEffect = GamePlay._instance.pfEffect_Rush; break;
		case Effect.EffectType.player_bubble : pfEffect = GamePlay._instance.pfEffect_Player_Bubble; break;
		case Effect.EffectType.player_die : pfEffect = GamePlay._instance.pfEffect_Player_Die; break;
		case Effect.EffectType.bg_bubble :	pfEffect = GamePlay._instance.pfEffect_BG_Bubble; break;
		case Effect.EffectType.combo : pfEffect = GamePlay._instance.pfEffect_Combo; break;
		case Effect.EffectType.possess_fish : pfEffect = GamePlay._instance.pfEffect_Possess_Fish; break;
		case Effect.EffectType.ink : pfEffect = GamePlay._instance.pfEffect_Ink; break;
		case Effect.EffectType.startrun : pfEffect = GamePlay._instance.pfEffect_StartRun; break;
		case Effect.EffectType.lastrun : pfEffect = GamePlay._instance.pfEffect_LastRun; break;
		case Effect.EffectType.fish_digest_count : pfEffect = GamePlay._instance.pfEffect_Fish_Digest_Count; break;
		case Effect.EffectType.fish_hunt_count : pfEffect = GamePlay._instance.pfEffect_Fish_Hunt_Count; break;
		case Effect.EffectType.item_appear : pfEffect = GamePlay._instance.pfEffect_Item_Appear; break;
		}
		
		if(pfEffect == null) {
			return null;
		}
		
		int nLayer = GameSettings.LAYER_EFFECT;
		if(bShowBehind) nLayer = GameSettings.LAYER_EFFECT_BEHIND;
		
		Vector3 posSpawn = new Vector3(pos.x, pos.y, nLayer);
		Effect obEffect = MonoBehaviour.Instantiate(pfEffect, posSpawn, Quaternion.identity) as Effect;
		obEffect.nType = nType;
		
	 	tk2dAnimatedSprite obAni = obEffect.gameObject.GetComponent<tk2dAnimatedSprite>();
		if(obAni) {
			obAni.animationCompleteDelegate = OnCompleteShowEffect;	
		}
		
		return obEffect;
	}
	
	static public void OnCompleteShowEffect(tk2dAnimatedSprite sprite, int clipId) {
		MonoBehaviour.Destroy(sprite.gameObject);
    	}						
	
	static public void UpdateEffectPos(Effect obEffect, Vector2 pos) {
		obEffect.transform.position = new Vector3(pos.x, pos.y, obEffect.transform.position.z);
	}
	
	static public void HideEffect(Effect obEffect) {
		MonoBehaviour.Destroy(obEffect.gameObject);
	}
	
	static public Effect ShowEffect_BG_Flow_Bubble() {
		if(GlobalValues.settings.lowQuality == 1) {
			return null;
		}
		
		Effect pfEffect = GamePlay._instance.pfEffect_BG_Flow_Bubble;
		
		float nOffsetY = 150f;
		float nX = GameSettings.nPlayAreaEndPositionX;
		float nY = Random.Range(nOffsetY, GameSettings.nScreenHeight - nOffsetY);

		int nLayer = GameSettings.LAYER_EFFECT;
		if(Random.Range(0, 2) == 0) {
			nLayer = GameSettings.LAYER_EFFECT_BEHIND;
		}
		
		Vector3 posSpawn = new Vector3(nX, nY, nLayer);
		Effect obEffect = MonoBehaviour.Instantiate(pfEffect, posSpawn, Quaternion.identity) as Effect;
		obEffect.nType = Effect.EffectType.bg_flow_bubble;
		
	 	tk2dSprite obSprite = obEffect.gameObject.GetComponent<tk2dSprite>();
		obSprite.spriteId = obSprite.GetSpriteIdByName("Effect_BG_Flow_Bubble_" + Random.Range(1, 8));	
		
		Vector3[] posPath = new Vector3[2];
		nX = GameSettings.nPlayAreaStartPositionX;
		nY = Random.Range(nOffsetY, GameSettings.nScreenHeight - nOffsetY);
		posPath[1] = new Vector3(nX, nY, obEffect.transform.position.z);
		
		nX = GameSettings.nScreenWidth * Random.Range(1, 4) / 4f;
		nY = Random.Range(nOffsetY, GameSettings.nScreenHeight - nOffsetY);
		posPath[0] = new Vector3(nX, nY, obEffect.transform.position.z);
		
		 iTween.EaseType easeType = iTween.EaseType.linear;		
		float nMoveSpeed = GameSettings.nBGFlowBubbleMoveSpeed * GamePlay.nGameSpeedChangeRate * Random.Range(0.8f, 1.3f);
		
		Hashtable tweenParam = new Hashtable();
		tweenParam.Add ("path", posPath);
		tweenParam.Add ("easetype", easeType);
		tweenParam.Add ("speed", nMoveSpeed);
		tweenParam.Add ("oncomplete", "OnFinishEffectMove");
		tweenParam.Add ("oncompletetarget", _instance.gameObject);
		tweenParam.Add ("oncompleteparams", obEffect);
		
		iTween.MoveTo(obEffect.gameObject, tweenParam);
		
		return obEffect;		
	}
	
	public void OnFinishEffectMove(Effect obEffect) {
		MonoBehaviour.Destroy(obEffect.gameObject);
	}
	
	public void OnFinishEffect_Fish_Digest_Count(Effect obEffect) {
		ScoreManager.AddDigestFishScore(obEffect.nParam);
		MonoBehaviour.Destroy(obEffect.gameObject);
	}
	
	public void OnFinishEffect_Fish_Hunt_Count(Effect obEffect) {
		ScoreManager.AddHuntScore(obEffect.nParam);
		MonoBehaviour.Destroy(obEffect.gameObject);
	}
}
