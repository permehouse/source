using UnityEngine;
using System.Collections;

public class FishCombo {
	// Combo Buffer
	const int nCountComboBuffer = 16;
	int[] listComboBuffer = new int[nCountComboBuffer];
	int nIndex = 0;

	public int InitCombo(int nCount) {
		nIndex = (nIndex + 1) % nCountComboBuffer;
		
		listComboBuffer[nIndex] = nCount;
		
		return nIndex;
	}
	
	public bool Hit(int nIndex) {
		if(nIndex < 0 || nIndex >= nCountComboBuffer) {
			return false;
		}

		listComboBuffer[nIndex]--;
		if(listComboBuffer[nIndex] == 0) {
			return true;			
		}
		
//		Debug.Log ("Hit [" + nIndex + "] : " + listComboBuffer[nIndex]);
		
		return false;
	}
}
