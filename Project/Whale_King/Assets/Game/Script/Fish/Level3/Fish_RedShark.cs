using UnityEngine;
using System.Collections;

public class Fish_RedShark : Fish {
	// Use this for initialization
	void Start () {
		strAniName = "Fish_RedShark";
		nType = FishType.redshark;
		
		SetProperty(FishSettings.obRedShark);

		nMoveType = Fish.MoveType.lookandattack;
		
/*		int nValue = Random.Range(0, 2);
		switch(nValue) {
		case 0 : nMoveType = MoveType.bounce; break;
		case 1 : nMoveType = MoveType.twobounce; break;
		}
		
		int nValue = Random.Range(0, 2);
		switch(nValue) {
		case 0 : nMoveType = MoveType.slowattack; break;
		case 1 : nMoveType = MoveType.fastattack; break;
		}
*/		
		int nX = GameSettings.nScreenWidth;
		int nY = GameSettings.listPlayLaneCenterY[GamePlay.nPlayerLane];
		posEnd = new Vector3(nX, nY, transform.position.z);
		
		Move();
	}
	
	public override bool CheckChangeToBonus() {
		bool bChanged = base.CheckChangeToBonus();
		
		if(bChanged) {
			nGameScore = 500;
			nHuntScore = 0;
			
		}
		
		return bChanged;
	}
}
