using UnityEngine;
using System.Collections;

public class Fish_Shark : Fish {
	// Use this for initialization
	void Start () {
		strAniName = "Fish_Shark";
		nType = FishType.shark;
		
		SetProperty(FishSettings.obShark);
		
/*		int nValue = Random.Range(0, 2);
		switch(nValue) {
		case 0 : nMoveType = MoveType.bounce; break;
		case 1 : nMoveType = MoveType.twobounce; break;
		}
*/
		nMoveType = MoveType.twobounce;
	
		posEnd = new Vector3(nEndPositionX, transform.position.y, transform.position.z);
		Move();
	}
	
	public override bool CheckChangeToBonus() {
		bool bChanged = base.CheckChangeToBonus();
		
		if(bChanged) {
			nGameScore = 500;
			nHuntScore = 0;
		}
		
		return bChanged;
	}	
}
