using UnityEngine;
using System.Collections;

public class Fish : MonoBehaviour {
	public enum FishType {
		bonus,
		carp,
		redcarp,
		crab,
		fugu,
		middlecarp,
		croc,
		shark,
		redshark,
		
		random = -1			// for special purpose
	}
	
	public enum MoveType {
		simple,
		slowfast,
		fastslow,
		trace,
		cross,
		slowattack,
		fastattack,
		bounce,
		twobounce,
		dragstart,
		drag,
		moving_u,
		moving_u_reverse,
		moving_up,
		moving_down,
		lookandattack,
		
		random = -1				// for special purpose
	}
	
	public enum MovePatternType {
		MPT_M = 1,
		MPT_W = 2,
		MPT_V = 3,
		MPT_V_REVERSE = 4,			// ^ shape
		MPT_LINE = 5,
		MPT_LINE_FAST = 6,
		MPT_MOVING_U = 7,
		MPT_MOVING_U_REVERSE = 8,
		MPT_MOVING_U_OR_REVERSE = 9,
		MPT_MOVING_UP =  10,
		MPT_MOVING_DOWN = 11,
		MPT_MOVING_UP_OR_DOWN = 12,
		MPT_COUNT = 12,
		
		random = -1				// for special purpose
	}
	
	public int nLevel = 0;
	public FishType nType = FishType.bonus;
	public MoveType nMoveType = MoveType.simple;
 	public float nSpeed = GameSettings.nFishMoveSpeed;
	protected int nEndPositionX = GameSettings.nPlayAreaStartPositionX;
	protected Vector3 posEnd;
	
	protected string strAniName = null;
	
    	public tk2dAnimatedSprite obAni = null;
	public bool bAttack = false;
	protected bool bAttackAllLanes = false;
	protected float nAttackDistance = 0f;
	protected float nAttackDamage = 0f;
	
	bool bMagnetDrag = false;
	float nMagnetDragDistance = GameSettings.nScreenWidth/2f;
	int nMagnetDragPlayerLane = -1;
	
	bool bPossessed = false;
	
	public bool bDie = false;
	public bool bHunted = false;
	public bool bAbsorbed = false;
	int nAbsorbedPlayerLane = -1;
	float nAbsorbedTime = 0;
	
	bool bPaused = false;
	
	bool bWin = false;
	bool bWinAndStop = false;
	
	public iTween.EaseType forceEaseType = iTween.EaseType.linear;
	
	protected float nInitScale = 1f;
	protected float nChangeToBonusScaleRate =1f;
	
	// Score
	protected int nGameScore = 0;
	protected int nHuntScore = 0;
	protected float nExpGage = 0f;
	
	// Combo
	public bool bCheckCombo = false;
	public int nIndexCombo = 0;

	// look and attack
	protected bool bLookingAtPlayer = false;
	protected float nLookingAtPlayerTime = 0f;
	protected float nMoveToPositionY = -1f;
	
	void Awake() {
        	obAni = GetComponent<tk2dAnimatedSprite>();
	}
	
	public void SetProperty(FishSettings.Property obProp) {
		nLevel = obProp.nLevel;
		nSpeed *= obProp.nSpeedRate;
		nInitScale = obProp.nInitScale;
		nChangeToBonusScaleRate = obProp.nChangeToBonusScaleRate;
		
		nGameScore = obProp.nGameScore;
		nHuntScore = obProp.nHuntScore;
		nExpGage = obProp.nExpGage;
		
		bAttackAllLanes = obProp.bAttackAllLanes;
		nAttackDistance = obProp.nAttackDistance;
		nAttackDamage = obProp.nAttackDamage;
		
		transform.localScale = new Vector3(nInitScale, nInitScale, 1f);
	}
	
	public void Move() {
		 iTween.EaseType easeType = iTween.EaseType.linear;
		switch(nMoveType) {
		case MoveType.simple : easeType = iTween.EaseType.linear; break;
		case MoveType.slowfast : easeType = iTween.EaseType.easeInCubic; break;
		case MoveType.fastslow : easeType = iTween.EaseType.easeOutCubic; break;
		case MoveType.trace : easeType = iTween.EaseType.easeInCirc; break;
		case MoveType.cross : easeType = iTween.EaseType.easeInOutExpo; break;
		case MoveType.slowattack : easeType = iTween.EaseType.easeInExpo; break;
		case MoveType.fastattack : easeType = iTween.EaseType.easeInOutExpo; break;
		case MoveType.bounce : easeType = iTween.EaseType.easeInBounce; break;
		case MoveType.twobounce : easeType = iTween.EaseType.easeInOutBounce; break;
		case MoveType.dragstart : easeType = iTween.EaseType.easeOutSine; break;
		case MoveType.drag : easeType = iTween.EaseType.easeOutSine; break;
		case MoveType.moving_u : easeType = iTween.EaseType.linear; break;
		case MoveType.moving_u_reverse : easeType = iTween.EaseType.linear; break;
		case MoveType.moving_up : easeType = iTween.EaseType.linear; break;
		case MoveType.moving_down : easeType = iTween.EaseType.linear; break;
		case MoveType.lookandattack : easeType = iTween.EaseType.linear; break;
		}
		
		float nMoveSpeed = nSpeed * GamePlay.nGameSpeedChangeRate;
		
		if(nMoveType == MoveType.moving_u) {
			Vector3[] posPath = new Vector3[2];

			float nX = (transform.position.x + posEnd.x) / 2;
			float nY =  GameSettings.listPlayLaneCenterY[0];
			posPath[0] = new Vector3(nX, nY, transform.position.z);
			posPath[1] = posEnd;
			
			Hashtable tweenParam = new Hashtable();
			tweenParam.Add ("path", posPath);
			tweenParam.Add ("easetype", easeType);
			tweenParam.Add ("speed", nMoveSpeed);
			tweenParam.Add ("oncomplete", "OnFinishMove");
			
			iTween.MoveTo(gameObject, tweenParam);
		}
		else if(nMoveType == MoveType.moving_u_reverse) {
			Vector3[] posPath = new Vector3[2];
			// - 400 - 800 // 0 +200
			float nX = (transform.position.x + posEnd.x) / 2;
			float nY =  GameSettings.listPlayLaneCenterY[GameSettings.nPlayLaneCount-1];
			posPath[0] = new Vector3(nX, nY -50, transform.position.z);					//yj edit
			posPath[1] = posEnd; 
			
			Hashtable tweenParam = new Hashtable();
			tweenParam.Add ("path", posPath);
			tweenParam.Add ("easetype", easeType);
			tweenParam.Add ("speed", nMoveSpeed);
			tweenParam.Add ("oncomplete", "OnFinishMove");
			
			iTween.MoveTo(gameObject, tweenParam);
		}
		else if(nMoveType == MoveType.moving_up) {
			Vector3[] posPath = new Vector3[3];

			float nX = GameSettings.nScreenWidth * (4f/5f);
			float nY = transform.position.y;
			posPath[0] = new Vector3(nX, nY, transform.position.z);

			nX = GameSettings.nScreenWidth * (1f/4f);
			nY = GameSettings.listPlayLaneCenterY[GetLane()+2];
			posPath[1] = new Vector3(nX, nY, transform.position.z);

			nX = posEnd.x;
			posPath[2] = new Vector3(nX, nY, transform.position.z);
			
			Hashtable tweenParam = new Hashtable();
			tweenParam.Add ("path", posPath);
			tweenParam.Add ("easetype", easeType);
			tweenParam.Add ("speed", nMoveSpeed);
			tweenParam.Add ("oncomplete", "OnFinishMove");
			
			iTween.MoveTo(gameObject, tweenParam);
		}
		else if(nMoveType == MoveType.moving_down) {
			Vector3[] posPath = new Vector3[3];

			float nX = GameSettings.nScreenWidth * (4f/5f);
			float nY = transform.position.y;
			posPath[0] = new Vector3(nX, nY, transform.position.z);

			nX = GameSettings.nScreenWidth * (1f/4f);
			nY = GameSettings.listPlayLaneCenterY[GetLane()-2];
			posPath[1] = new Vector3(nX, nY, transform.position.z);

			nX = posEnd.x;
			posPath[2] = new Vector3(nX, nY, transform.position.z);
			
			Hashtable tweenParam = new Hashtable();
			tweenParam.Add ("path", posPath);
			tweenParam.Add ("easetype", easeType);
			tweenParam.Add ("speed", nMoveSpeed);
			tweenParam.Add ("oncomplete", "OnFinishMove");
			
			iTween.MoveTo(gameObject, tweenParam);
		}
		else if(nMoveType == MoveType.lookandattack) {
			nMoveSpeed /= 2f;
			
			Hashtable tweenParam = new Hashtable();
			tweenParam.Add ("position", posEnd);
			tweenParam.Add ("easetype", easeType);
			tweenParam.Add ("speed", nMoveSpeed);
			tweenParam.Add ("oncomplete", "OnFinishMoveLookAndAttack");
			
			iTween.MoveTo(gameObject, tweenParam);
		}
		else {
			Hashtable tweenParam = new Hashtable();
			tweenParam.Add ("position", posEnd);
			tweenParam.Add ("easetype", easeType);
			tweenParam.Add ("speed", nMoveSpeed);
			tweenParam.Add ("oncomplete", "OnFinishMove");
			
			iTween.MoveTo(gameObject, tweenParam);
		}
	}	
	
	public void MoveToHuntScore() {
		// 현재 위치를 Random 하게 약간 조정함.
		float nOffsetX = Random.Range(-70f, 30f);
		float nOffsetY = Random.Range(-50f, 50f);
		transform.position = new Vector3(transform.position.x+nOffsetX, transform.position.y+nOffsetY, transform.position.z);
		
		 iTween.EaseType easeType = iTween.EaseType.easeOutSine;
		float nMoveSpeed = 1.4f * nSpeed * 1f;//GamePlay.nGameSpeedChangeRate;		//yj edit. 1.5f->1.4f.
		
		Vector3[] posPath = new Vector3[2];

		// end point
		nOffsetX = Random.Range(-100f, 100f);
		float nX = GameSettings.nScoreAreaEatUpFishPositionX + nOffsetX;
		float nY =  GameSettings.nScoreAreaEatUpFishPositionY;
		posPath[1] = new Vector3(nX, nY, transform.position.z);
		
		// 중간 위치를 Random 하게 약간 조정하여 뼈다귀가 변화있게 날아가게 함.		yj edit.
		float nMidX = Random.Range(-120f, 120f);
		float nMidY = Random.Range(-120f, 120f);
		
		// mid point
		nX =  (2*nX + transform.position.x)/3+nMidX;
		nY =  (nY + transform.position.y)/2+nMidY;
		posPath[0] = new Vector3(nX, nY, transform.position.z);
		
		Hashtable tweenParam = new Hashtable();
		tweenParam.Add ("path", posPath);
		tweenParam.Add ("easetype", easeType);
		tweenParam.Add ("speed", nMoveSpeed);
		tweenParam.Add ("oncomplete", "OnFinishMoveToHuntScore");
		
		iTween.MoveTo(gameObject, tweenParam);
	}	
	
	public void AbsorbedToPlayer() {
		 iTween.EaseType easeType = iTween.EaseType.linear;
		float nMoveTime = GameSettings.nFishAbosorbTime - nAbsorbedTime;
		if(nMoveTime < 0f) {
			nMoveTime = 0f;
		}
		
		nAbsorbedPlayerLane = GamePlay.nPlayerLane;
			
		int nX = (int)(GameSettings.nPlayerPositionX + transform.position.x)/2;
		int nY = GameSettings.listPlayLaneCenterY[nAbsorbedPlayerLane];
		posEnd = new Vector3(nX, nY, transform.position.z);
		
		Hashtable tweenParam = new Hashtable();
		tweenParam.Add ("position", posEnd);
		tweenParam.Add ("easetype", easeType);
		tweenParam.Add ("time", nMoveTime);
		tweenParam.Add ("oncomplete", "OnFinishAbsorbedToPlayer");
		
		iTween.MoveTo(gameObject, tweenParam);
	}
	
	public void Stop() {
		iTween.Stop(gameObject);
	}
	
	void OnFinishMove() {
		Destroy(gameObject);
	}

	void OnFinishMoveToHuntScore() {
		ScoreManager.AddHuntScore(nHuntScore);
		OnFinishMove();
	}
	
	void OnFinishMoveLookAndAttack() {
		bLookingAtPlayer = true;
		nLookingAtPlayerTime = GameSettings.nLookingAtPlayerTime;
	}
	
	void OnFinishAbsorbedToPlayer() {
		Die();
	}
	
	public virtual void SetMovePattern(Fish pfFish, Fish.MovePatternType nMovePatternType, int nLane) {
		if(nLane == -1) {
			nLane = Random.Range(0, GameSettings.nPlayLaneCount);
		}
		
		switch(nMovePatternType) {
		case MovePatternType.MPT_MOVING_U : nMoveType = MoveType.moving_u; break;
		case MovePatternType.MPT_MOVING_U_REVERSE : nMoveType = MoveType.moving_u_reverse; break;
		case MovePatternType.MPT_MOVING_UP :
			nMoveType = MoveType.moving_up;
			nLane = Random.Range(0, 2);
			break;
		case MovePatternType.MPT_MOVING_DOWN :
			nMoveType = MoveType.moving_down;
			nLane = Random.Range(GameSettings.nPlayLaneCount - 2, GameSettings.nPlayLaneCount);
			break;
		}
		
		float nY= GameSettings.listPlayLaneCenterY[nLane];
		float nX = GameSettings.nPlayAreaEndPositionX;
		float nZ = transform.position.z;
		transform.position = new Vector3(nX, nY, nZ);		
	}
	
	public void CheckAttack() {
		if(bDie) {
			return;
		}
		
		if(bAttack) {
			return;
		}
		
		bool bCollision = false;
		int nLane = GetLane();
		if(bAttackAllLanes || nLane == GamePlay.nPlayerLane) {
			bCollision = true;
		}

/*		int nSize = 1;
		if(nType == FishType.shark) {
			nSize = 2;
		}
		
		if(!bCollision && nSize == 2) {
			if(nLane-1 == GamePlay.nPlayerLane || nLane+1 == GamePlay.nPlayerLane) {
				bCollision = true;
			}
		}
*/		
		if(!bCollision) {
			return;
		}
		
		int nXDistance = (int)transform.position.x - GameSettings.nPlayerPositionX;
		if(nXDistance > 0 && nXDistance < nAttackDistance) {
			if(GamePlay.obPlayer.bFeverMode == false && GamePlay.obPlayer.bStartRun == false && GamePlay.obPlayer.bLastRun == false && GamePlay.obPlayer.nLevel < nLevel) {
				bAttack = true;
				obAni.Play(GetAniName());
				obAni.animationCompleteDelegate = OnCompleteAttackAnimation;	
			}
		}
	}

	public virtual void OnCompleteAttackAnimation(tk2dAnimatedSprite sprite, int clipId) {
		bAttack = false;
		obAni.Play(GetAniName());
	}
	
	public void CheckMagnetDrag() {
		if(bDie) {
			return;
		}
		
		if(bMagnetDrag) {
			return;
		}
		
		if(GamePlay.obPlayer.bMagnetMode == false && GamePlay.obPlayer.bStartRun == false) {
			return;	
		}
		
/*		bool bCollision = false;
		int nLane = GetLane();
		if(nLane-1 <= GamePlay.nPlayerLane && nLane+1 >= GamePlay.nPlayerLane) {
			bCollision = true;
		}
		
		if(!bCollision) {
			return;
		}
*/		
//		int nXDistance = (int)Mathf.Abs(transform.position.x - GameSettings.nPlayerPositionX);
		float nXDistance = Vector3.Distance(transform.position, GamePlay.obPlayer.transform.position);
		if(transform.position.x>=0 && nXDistance < nMagnetDragDistance) {
			if(GamePlay.obPlayer.bFeverMode || GamePlay.obPlayer.bStartRun || GamePlay.obPlayer.nLevel >= nLevel) {
				bMagnetDrag = true;
				nMagnetDragPlayerLane = -1;
			}
		}
	}

	public void CheckPossessed() {
		if(bDie) {
			return;
		}
		
		if(bPossessed) {
			return;
		}
		
		if(GamePlay.obPet.bPossessFish == false) {
			return;	
		}
		
		if(nType == FishType.bonus) {
			return;
		}
		
		if(transform.position.x > 0 && transform.position.x <= GameSettings.nScreenWidth) {
			if(nLevel <= GamePlay.obPet.nPossessFishLevel) {
				bMagnetDrag = true;
				nMagnetDragPlayerLane = -1;
				
				bPossessed = true;
				obAni.Play(GetAniName());		
			}
		}
	}
	
	public virtual bool CheckChangeToBonus() {
		if(bDie) {
			return false;
		}
		
		if(GamePlay.obPlayer.bWingMode == false) {
			return false;	
		}
		
		if(nType == FishType.bonus) {
			return false;
		}
		
		if(transform.position.x > 0 && transform.position.x <= GameSettings.nScreenWidth) {
			strAniName = "Fish_Bonus";
			nLevel = 0;
			nExpGage *= 2;
			bCheckCombo = false;

			nType = FishType.bonus;
			obAni.Play(strAniName);		
			
/*			Fish pfFish = GamePlay._instance.pfFish_Bonus;
			MonoBehaviour.Instantiate(pfFish, transform.position, Quaternion.identity);
			OnFinishMove();
*/			
			RescaleOnChange();			
			
			return true;
		}
		
		return false;
	}
	
	void CheckWinPos() {
		if(bWin == false) {
			return;	
		}
		
		transform.position = new Vector3(transform.position.x, transform.position.y, GameSettings.LAYER_FISH_WIN);
		if(bWinAndStop == false) {
			int nOffsetX = 20;
			if(transform.position.x <= GameSettings.nPlayerPositionX + nOffsetX) {
				bWinAndStop = true;
				iTween.Pause(gameObject);
			}		
		}
	}
	
	void UpdateMagnetDrag() {
		if(bDie) {
			return;
		}	
		
		if(!bMagnetDrag) {
			return;
		}
		
		if(GamePlay.nPlayerLane != nMagnetDragPlayerLane) {
			nSpeed = GameSettings.nFishMoveSpeed * 1.5f;
			if(nMagnetDragPlayerLane == -1) {
				nMoveType = MoveType.dragstart;
			}
			else {
				nMoveType = MoveType.drag;
			}
	
			int nX = GameSettings.nPlayerPositionX;
			int nY = GameSettings.listPlayLaneCenterY[GamePlay.nPlayerLane];
			posEnd = new Vector3(nX, nY, transform.position.z);
			Move();
			
			nMagnetDragPlayerLane = GamePlay.nPlayerLane;
		}
	}
	
	void UpdateAbsorbed() {
		if(bDie) {
			return;
		}	
		
		if(!bAbsorbed) {
			return;
		}
		
		nAbsorbedTime += Time.deltaTime;
		
		if(GamePlay.nPlayerLane != nAbsorbedPlayerLane) {
			AbsorbedToPlayer();
		}
	}
	
	public int GetLane() {
		float nY = transform.position.y + GameSettings.nPlayLaneHeight/2;
		int nLane = (int)(((nY - GameSettings.nPlayAreaStartPositionY)/GameSettings.nPlayLaneHeight) - 0.5);
		
		if(nLane < 0) nLane = 0;
		else if(nLane > GameSettings.nPlayLaneCount-1) nLane = GameSettings.nPlayLaneCount-1;
		
		return nLane;
	}
	
	public void Die() {
		if(bDie) {
			return;
		}
		
		if(obAni) {
			Stop();

			bDie = true;
			RescaleOnChange();

			if(nLevel == 0) {
				OnFinishMove();	
			}
			else {
				bHunted = true;
				obAni.Play(GetAniName());		
				
				if(GlobalValues.settings.lowQuality == 1) {
					OnFinishMoveToHuntScore();
				}
				else {
					MoveToHuntScore();
				}
			}
			
//			obAni.Play(GetAniName());		
//			obAni.animationCompleteDelegate = OnCompleteDieAnimation;
		}
		else {
			OnFinishMove();
		}
	}
	
	public void Absorbed() {
		if(bDie) {
			return;
		}
		
		Stop();
		
		bAbsorbed = true;
		nAbsorbedTime = 0;
		
		ChangeSize();
		AbsorbedToPlayer();
	}
	
	void ChangeSize() {
		 iTween.EaseType easeType =  iTween.EaseType.easeInOutSine;
		
		Vector3 scaleChageSize = new Vector3(nInitScale, nInitScale, 1f);
		if(bAbsorbed) {
			transform.localScale = new Vector3(nInitScale*0.8f, nInitScale*0.8f, 1f);
			scaleChageSize = new Vector3(nInitScale*0.2f, nInitScale*0.2f, 1f);
		}
		float nChageSizeTime = GameSettings.nFishAbosorbTime;		
		
		Hashtable tweenParam = new Hashtable();
		tweenParam.Add ("scale", scaleChageSize);
		tweenParam.Add ("easetype", easeType);
		tweenParam.Add ("time", nChageSizeTime);
		
		iTween.ScaleTo(gameObject, tweenParam);
	}
	
/*	
    void OnCompleteDieAnimation(tk2dAnimatedSprite sprite, int clipId) {
		if(nLevel == 0) {
			OnFinishMove();	
		}
		else {
			bHunted = true;
			obAni.Play(GetAniName());		
			
			MoveToHuntScore();
		}
    }						
*/	
	public void RescaleOnChange() {
		transform.localScale = new Vector3(nInitScale*nChangeToBonusScaleRate, nInitScale*nChangeToBonusScaleRate, 1f);		
	}
	
	void OnTriggerEnter(Collider other) {
		CheckCollision(other.gameObject);
	}
	
	void CheckCollision(GameObject obOther) {
		if(bDie || GamePlay.obPlayer.bDie) {
			return;
		}
		
		if(bAbsorbed) {
			return;
		}
		
		Player obPlayer = obOther.GetComponent("Player") as Player;
		if(obPlayer) {
//			Debug.Log ("Level, Fish : " + nLevel + ", Player : " + obPlayer.nLevel);
			if(obPlayer.bFeverMode || obPlayer.bStartRun || obPlayer.bLastRun || bMagnetDrag || nLevel <= obPlayer.nLevel) {	// 유저가 먹는것.
				obPlayer.Attack();
				Absorbed();
				
				ScoreManager.AddGameScore(nGameScore);
				ScoreManager.AddExpGage(nExpGage);						// yj edit. 임시.
				
				// 임시 삭제.
				//EffectManager.ShowEffect_Fish_Die(new Vector2(transform.position.x, transform.position.y));
				
				CheckCombo();
				
				GamePlay.SoundEffect(GamePlay._instance.sndFish_Die);
			}
			else {
				obPlayer.Damage(nAttackDamage, this);
			}
		}
		
		Pet obPet = obOther.GetComponent("Pet") as Pet;
		if(obPet && nLevel > 0) {
			if(obPet.bGiant || obPet.bRush) {
				OnFinishMove();
				
				ScoreManager.AddGameScore(nGameScore);
				GamePlay.SoundEffect(GamePlay._instance.sndFish_Die);
			}
			else 	if(obPet.bPetProperty_Attack) {
				bool bPetAttack = false;
				if(nLevel < obPet.nAttackLevel) {
					bPetAttack = true;
				}
				else if(nLevel == obPet.nAttackLevel) {
					float nValue = Random.Range(0f, 1f);
					if(nValue <  obPet.nAttackRate) {
						bPetAttack = true;
					}
				}
				
				if(bPetAttack) {
					float nX = obOther.transform.position.x;
					if((nLevel <= obPet.nLevel) && (nX <= GameSettings.nScreenWidth)) {
						obPet.Attack();
						Die();
						
						ScoreManager.AddGameScore(nGameScore);
						ScoreManager.AddExpGage(nExpGage);
						
						GamePlay.SoundEffect(GamePlay._instance.sndFish_Die);
					}
				}
			}
		}
	}
	
	public float GetSpeed() {
		return nSpeed;
	}
	
	public void Update() {
		CheckGamePause();
		if(GamePlay.nGameStatus != GamePlay.GameStatus.PLAY) {
			return;
		}
		
		CheckAttack();
		CheckMagnetDrag();
		CheckPossessed();
		CheckChangeToBonus();
		CheckWinPos();
		
		UpdateMagnetDrag();
		UpdateAbsorbed();
		UpdateLookingAtPlayer();
	}

	public string GetAniName() {
		string strName = strAniName;

		if(bHunted) {
			strName = "Fish_Hunted_Long";
//			strName = "Fish_Hunted";
//		Debug.Log("Fish Ani : " + strName);
			return strName;
		}

		if(bPossessed) {
			strName += "_Possessed";
//		Debug.Log("Fish Ani : " + strName);
			return strName;
		}

		if(bWin) {
			strName += "_Win";
			return strName;
		}
		
		if(bAttack) {
			strName += "_Attack";
		}
		
//		Debug.Log("Fish Ani : " + strName);
		
		return strName;
	}
	
	void CheckGamePause() {
		if(!bPaused && GamePlay.nGameStatus == GamePlay.GameStatus.PAUSE) {
			bPaused = true;
			iTween.Pause(gameObject);
			obAni.Pause();
		}
		else if(bPaused && GamePlay.nGameStatus == GamePlay.GameStatus.PLAY) {
			bPaused = false;
			iTween.Resume(gameObject);		
			obAni.Resume();
		}
	}

	public void InitCombo(int nCount) {
		bCheckCombo = true;	
		nIndexCombo = GamePlay.iFishCombo.InitCombo(nCount);
	}
	
	public void SetCombo(int nIndex) {
		bCheckCombo = true;	
		nIndexCombo = nIndex;
	}
	
	void CheckCombo() {
		if(bCheckCombo == false) {
			return;
		}
		
		bool bCombo = GamePlay.iFishCombo.Hit(nIndexCombo);
		if(bCombo) {
			ScoreManager.AddHuntScore(GameSettings.nComboHuntScore);
			ScoreManager.AddGameScore(GameSettings.nComboGameScore);
			
			float nX = 60f;
			float nY = 570f;
			EffectManager.ShowEffect_Combo(new Vector2(nX, nY));
		}
	}
	
	void UpdateLookingAtPlayer() {
		if(bLookingAtPlayer == false) {
			return;
		}
		
		nLookingAtPlayerTime -= Time.deltaTime;
		if(nLookingAtPlayerTime <= 0f) {
			bLookingAtPlayer = false;
			nLookingAtPlayerTime = 0f;
			
			nMoveType = MoveType.fastattack;
		
			posEnd = new Vector3(nEndPositionX, transform.position.y, transform.position.z);
			Move ();
			return;
		}
			
		nMoveToPositionY = GameSettings.listPlayLaneCenterY[GamePlay.nPlayerLane];
		UpdateMoveToPositionY();
	}
	
	void UpdateMoveToPositionY() {
		if(GamePlay.nGameStatus != GamePlay.GameStatus.PLAY) {
			return;
		}
		
		if(nMoveToPositionY == -1f || nMoveToPositionY == transform.position.y) {
			return;
		}
		
		float nRate = (500f / nSpeed) * Time.deltaTime;
		if(nRate>1f) nRate = 1f;
			
		float nY = transform.position.y + (nMoveToPositionY - transform.position.y) * nRate;
		if(Mathf.Abs(nMoveToPositionY - nY) < 0.1f) {
			nY = nMoveToPositionY;
			nMoveToPositionY = -1f;
		}

		transform.position = new Vector3(transform.position.x, nY, transform.position.z);
	}
	
	public void Disappear() {
		StartCoroutine("CR_Disappear");
	}
	
	IEnumerator CR_Disappear() {
		bDie = true;
		iTween.Pause(gameObject);
		obAni.Pause();
		
		float nHoldTime = GameSettings.nFishDisappearTime;
		float nTime = 0f;
		float nAlpha = 0f;
		float nScale = 0f;
		float nMinScale = nInitScale * 0.5f;
		
		while (nTime < nHoldTime) {
			if(GamePlay.nGameStatus != GamePlay.GameStatus.PLAY) {
				yield return null;
				continue;
			}

			nTime += Time.deltaTime;
			
			nAlpha = 1 - nTime/nHoldTime;
			obAni.color = new Color(1f, 1f, 1f, nAlpha);
			
			nScale = (nInitScale - nMinScale) * nAlpha + nMinScale;
			
			gameObject.transform.localScale = new Vector3(nScale, nScale, 1f);
			
			yield return null;
		}

		OnFinishMove();
	}	
	
	public void Win() {
		bWin = true;
		obAni.Play(GetAniName());
		obAni.animationCompleteDelegate = OnCompleteWinAnimation;	
	}
	
	public virtual void OnCompleteWinAnimation(tk2dAnimatedSprite sprite, int clipId) {
		transform.position = new Vector3(transform.position.x, transform.position.y, GameSettings.LAYER_FISH);
		iTween.Resume(gameObject);
		
		bWin = false;
		bWinAndStop = false;
		obAni.Play(GetAniName());
	}
}
