using UnityEngine;
using System.Collections;

public class Fish_RedCarp : Fish {
	int nFishGap = 100;
	float nMoveFastRate = 1.2f;				// yj edit
	
	// Use this for initialization
	void Start () {
		strAniName = "Fish_RedCarp";
		nType = FishType.redcarp;
		
		SetProperty(FishSettings.obRedCarp);
		
		nMoveFastRate = 1.5f;
		if(nMoveType == MoveType.moving_u || nMoveType == MoveType.moving_u_reverse) {
			nSpeed *= 2.25f;
			nEndPositionX = -100;
		}
		
		posEnd = new Vector3(nEndPositionX, transform.position.y, transform.position.z);
		Move();
	}
	
	public override void SetMovePattern(Fish pfFish, Fish.MovePatternType nMovePatternType, int nLane) {
		base.SetMovePattern(pfFish, nMovePatternType, nLane);
		
		if(nMovePatternType == Fish.MovePatternType.random) {
			nMovePatternType = (MovePatternType)Random.Range(1, (int)MovePatternType.MPT_COUNT + 1);
		}
		
		switch(nMovePatternType) {
		case MovePatternType.MPT_M : SetMovePattern_M(pfFish, nLane); break;
		case MovePatternType.MPT_W : SetMovePattern_W(pfFish, nLane); break;
		case MovePatternType.MPT_V : SetMovePattern_V(pfFish, nLane); break;
		case MovePatternType.MPT_V_REVERSE : SetMovePattern_V_Reverse(pfFish, nLane); break;
		case MovePatternType.MPT_LINE : SetMovePattern_Line(pfFish, nLane); break;
		case MovePatternType.MPT_LINE_FAST : SetMovePattern_Line_Fast(pfFish, nLane); break;
		case MovePatternType.MPT_MOVING_U : SetMovePattern_Moving_U(pfFish, nLane); break;
		case MovePatternType.MPT_MOVING_U_REVERSE : SetMovePattern_Moving_U_Reverse(pfFish, nLane); break;
		}
	}
	
	void SetMovePattern_M(Fish pfFish, int nLane) {
		if(nLane < 0 || nLane > 2) {
			nLane = Random.Range(0, 3);
		}
		
		float nY= GameSettings.listPlayLaneCenterY[nLane];
		float nX = transform.position.x;
		Vector3 posSpawn = new Vector3(nX, nY, transform.position.z);
		transform.position = posSpawn;
	
		int nFishCount = 9;
		InitCombo(nFishCount);		
		
		for (int i = 0; i < nFishCount-1; i++) {
			switch(i) {
			case 0 :
			case 1 :
			case 4 :
			case 5 :
				nLane++;
				break;
			default :
				nLane--;
				break;
			}
			
			nY= GameSettings.listPlayLaneCenterY[nLane];
			nX += nFishGap;
			posSpawn = new Vector3(nX, nY, transform.position.z);
			Fish obFish = Instantiate(pfFish, posSpawn, Quaternion.identity) as Fish;
			obFish.nLevel = nLevel;
			obFish.SetCombo(nIndexCombo);						
		}		
	}

	void SetMovePattern_W(Fish pfFish, int nLane) {
		if(nLane < 2 || nLane > GameSettings.nPlayLaneCount-1) {
			nLane = Random.Range(2, GameSettings.nPlayLaneCount);
		}
		
		float nY= GameSettings.listPlayLaneCenterY[nLane];
		float nX = transform.position.x;
		Vector3 posSpawn = new Vector3(nX, nY, transform.position.z);
		transform.position = posSpawn;
	
		int nFishCount = 9;
		InitCombo(nFishCount);		
		
		for (int i = 0; i < nFishCount-1; i++) {
			switch(i) {
			case 0 :
			case 1 :
			case 4 :
			case 5 :
				nLane--;
				break;
			default :
				nLane++;
				break;
			}
			
			nY= GameSettings.listPlayLaneCenterY[nLane];
			nX += nFishGap;
			posSpawn = new Vector3(nX, nY, transform.position.z);
			Fish obFish = Instantiate(pfFish, posSpawn, Quaternion.identity) as Fish;
			obFish.nLevel = nLevel;
			obFish.SetCombo(nIndexCombo);						
		}		
	}	

	void SetMovePattern_V(Fish pfFish, int nLane) {
		nLane = GameSettings.nPlayLaneCount-1;
		float nY= GameSettings.listPlayLaneCenterY[nLane];
		float nX = transform.position.x;
		Vector3 posSpawn = new Vector3(nX, nY, transform.position.z);
		transform.position = posSpawn;
	
		int nFishCount = 7;
		InitCombo(nFishCount);		
		
		for (int i = 0; i < nFishCount-1; i++) {
			switch(i) {
			case 0 :
			case 1 :
			case 2 :
				nLane--;
				break;
			default :
				nLane++;
				break;
			}
			
			nY= GameSettings.listPlayLaneCenterY[nLane];
			nX += nFishGap;
			posSpawn = new Vector3(nX, nY, transform.position.z);
			Fish obFish = Instantiate(pfFish, posSpawn, Quaternion.identity) as Fish;
			obFish.nLevel = nLevel;
			obFish.SetCombo(nIndexCombo);			
		}		
	}	

	void SetMovePattern_V_Reverse(Fish pfFish, int nLane) {
		nLane = 0;
		float nY= GameSettings.listPlayLaneCenterY[nLane];
		float nX = transform.position.x;
		Vector3 posSpawn = new Vector3(nX, nY, transform.position.z);
		transform.position = posSpawn;
	
		int nFishCount = 7;
		InitCombo(nFishCount);		
		
		for (int i = 0; i < nFishCount-1; i++) {
			switch(i) {
			case 0 :
			case 1 :
			case 2 :
				nLane++;
				break;
			default :
				nLane--;
				break;
			}
			
			nY= GameSettings.listPlayLaneCenterY[nLane];
			nX += nFishGap;
			posSpawn = new Vector3(nX, nY, transform.position.z);
			Fish obFish = Instantiate(pfFish, posSpawn, Quaternion.identity) as Fish;
			obFish.nLevel = nLevel;
			obFish.SetCombo(nIndexCombo);
		}		
	}	

	void SetMovePattern_Line(Fish pfFish, int nLane) {
		if(nLane < 0 || nLane > GameSettings.nPlayLaneCount-1) {
			nLane = Random.Range(0, GameSettings.nPlayLaneCount);
		}
		
		float nY= GameSettings.listPlayLaneCenterY[nLane];
		float nX = transform.position.x;
		Vector3 posSpawn = new Vector3(nX, nY, transform.position.z);
		transform.position = posSpawn;

		int nFishCount = 7;
		InitCombo(nFishCount);
		
		for (int i = 0; i < nFishCount-1; i++) {
			nY= GameSettings.listPlayLaneCenterY[nLane];
			nX += nFishGap;
			posSpawn = new Vector3(nX, nY, transform.position.z);
			Fish obFish = Instantiate(pfFish, posSpawn, Quaternion.identity) as Fish;
			obFish.nLevel = nLevel;
			obFish.SetCombo(nIndexCombo);
		}		
	}	
	
	void SetMovePattern_Line_Fast(Fish pfFish, int nLane) {
		nSpeed *= nMoveFastRate;

		if(nLane < 0 || nLane > GameSettings.nPlayLaneCount-1) {
			nLane = Random.Range(0, GameSettings.nPlayLaneCount);
		}
		
		float nY= GameSettings.listPlayLaneCenterY[nLane];
		float nX = transform.position.x;
		Vector3 posSpawn = new Vector3(nX, nY, transform.position.z);
		transform.position = posSpawn;
	
		int nFishCount = 7;
		InitCombo(nFishCount);
		
		for (int i = 0; i < nFishCount-1; i++) {
			nY= GameSettings.listPlayLaneCenterY[nLane];
			nX += nFishGap;
			posSpawn = new Vector3(nX, nY, transform.position.z);
			Fish obFish = Instantiate(pfFish, posSpawn, Quaternion.identity) as Fish;
			obFish.nLevel = nLevel;
			obFish.nSpeed *= nMoveFastRate;
			obFish.SetCombo(nIndexCombo);			
		}		
	}	
	
	void SetMovePattern_Moving_U(Fish pfFish, int nLane) {
		StartCoroutine(CR_SetMovePattern_Moving_U(pfFish, nLane));
	}
	
	IEnumerator CR_SetMovePattern_Moving_U(Fish pfFish, int nLane) {
		nMoveType = MoveType.moving_u;
		
		float nY=0;
		if(nLane < 0 || nLane > GameSettings.nPlayLaneCount-1) {
			nY = Random.Range(GameSettings.listPlayLaneCenterY[1], GameSettings.nScreenHeight);
		}
		else {
			nY= GameSettings.listPlayLaneCenterY[nLane];
		}
			
		float nX = transform.position.x;
		Vector3 posSpawn = new Vector3(nX, nY, transform.position.z);
		transform.position = posSpawn;
	
		float nTimeGap = 0.2f;
		nTimeGap /= 1f;					// 1f should be the same value to the modifer applied to the speed. refere to 'nSpeed *= 1f;'
		nTimeGap /= GamePlay.nGameSpeedChangeRate;
		
		int nFishCount = 7;
		InitCombo(nFishCount);
		
		float nTime = 0f;
		for(int i = 0;  i < nFishCount-1; i++) {
			nTime = 0f;
			while(	nTime < nTimeGap) {
				if(GamePlay.nGameStatus == GamePlay.GameStatus.PLAY) {
					nTime += Time.deltaTime;
				}
					
				yield return null;
			}				
			
			posSpawn = new Vector3(nX, nY, transform.position.z);
			Fish obFish = Instantiate(pfFish, posSpawn, Quaternion.identity) as Fish;
			obFish.nLevel = nLevel;
			obFish.nMoveType = nMoveType;
			obFish.SetCombo(nIndexCombo);						
		}		
	}	
	
	void SetMovePattern_Moving_U_Reverse(Fish pfFish, int nLane) {
		StartCoroutine(CR_SetMovePattern_Moving_U_Reverse(pfFish, nLane));
	}
	
	IEnumerator CR_SetMovePattern_Moving_U_Reverse(Fish pfFish, int nLane) {
		nMoveType = MoveType.moving_u_reverse;
		
		float nY=0;
		if(nLane < 0 || nLane > GameSettings.nPlayLaneCount-1) {
			nY = Random.Range(0, GameSettings.listPlayLaneCenterY[GameSettings.nPlayLaneCount-2]);
		}
		else {
			nY= GameSettings.listPlayLaneCenterY[nLane];
		}
			
		float nX = transform.position.x;
		Vector3 posSpawn = new Vector3(nX, nY, transform.position.z);
		transform.position = posSpawn;
	
		float nTimeGap = 0.2f;
		nTimeGap /= 1f;					// 1f should be the same value to the modifer applied to the speed. refere to 'nSpeed *= 1f;'
		nTimeGap /= GamePlay.nGameSpeedChangeRate;
		
		int nFishCount = 7;
		InitCombo(nFishCount);
		
		float nTime = 0f;
		for(int i = 0;  i < nFishCount-1; i++) {
			nTime = 0f;
			while(	nTime < nTimeGap) {
				if(GamePlay.nGameStatus == GamePlay.GameStatus.PLAY) {
					nTime += Time.deltaTime;
				}
					
				yield return null;
			}				
				
			posSpawn = new Vector3(nX, nY, transform.position.z);
			Fish obFish = Instantiate(pfFish, posSpawn, Quaternion.identity) as Fish;
			obFish.nLevel = nLevel;
			obFish.nMoveType = nMoveType;
			obFish.SetCombo(nIndexCombo);			
		}		
	}	
	
	public override bool CheckChangeToBonus() {
		bool bChanged = base.CheckChangeToBonus();
		
		if(bChanged) {
			nGameScore = 100;
			nHuntScore = 0;
		}
		
		return bChanged;
	}
}
