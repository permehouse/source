using UnityEngine;
using System.Collections;

public class Fish_Carp : Fish {
	// Use this for initialization
	void Start () {
		strAniName = "Fish_Carp";
		nType = FishType.carp;
		nMoveType = MoveType.simple;

		SetProperty(FishSettings.obCarp);
			
		posEnd = new Vector3(nEndPositionX, transform.position.y, transform.position.z);
		Move();
	}
	
	public override bool CheckChangeToBonus() {
		bool bChanged = base.CheckChangeToBonus();
		
		if(bChanged) {
			nGameScore = 100;
			nHuntScore = 0;
		}
		
		return bChanged;
	}
}
