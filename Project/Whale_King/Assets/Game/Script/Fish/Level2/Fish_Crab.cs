using UnityEngine;
using System.Collections;

public class Fish_Crab : Fish {
	// Use this for initialization
	void Start () {
		strAniName = "Fish_Crab";
		nType = FishType.crab;
		nMoveType = MoveType.slowfast;
		
		SetProperty(FishSettings.obCrab);		
		
		int nY= GameSettings.listPlayLaneCenterY[0];		// fix the lane to 0(ground) if crab
		transform.position = new Vector3(transform.position.x, nY, transform.position.z);
			
		posEnd = new Vector3(nEndPositionX, nY, transform.position.z);
		Move();
	}
	
	public override bool CheckChangeToBonus() {
		bool bChanged = base.CheckChangeToBonus();
		
		if(bChanged) {
			nGameScore = 300;
			nHuntScore = 0;
		}
		
		return bChanged;
	}			
}
