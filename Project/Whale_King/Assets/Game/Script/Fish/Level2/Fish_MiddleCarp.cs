using UnityEngine;
using System.Collections;

public class Fish_MiddleCarp : Fish {
	// Use this for initialization
	void Start () {
		strAniName = "Fish_MiddleCarp";
		nType = FishType.middlecarp;
		
		SetProperty(FishSettings.obMiddleCarp);
		
		//int nDistanceX = Random.Range(0, (int)(GameSettings.nScreenWidth/2));		// adjust distance for randomizing middlecarp's expansion position
		transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);		
		
		if(nMoveType == MoveType.moving_up || nMoveType == MoveType.moving_down) {
			nSpeed *= 2.25f;
		}
		
		posEnd = new Vector3(nEndPositionX, transform.position.y, transform.position.z);
		Move();
	}
	
	public override bool CheckChangeToBonus() {
		bool bChanged = base.CheckChangeToBonus();
		
		if(bChanged) {
			nGameScore = 300;
			nHuntScore = 0;
		}
		
		return bChanged;
	}
}
