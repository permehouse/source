using UnityEngine;
using System.Collections;

public class Fish_Croc : Fish {
	// Use this for initialization
	void Start () {
		strAniName = "Fish_Croc";
		nType = FishType.croc;
		nMoveType = MoveType.trace;
		
		SetProperty(FishSettings.obCroc);
		
		int nY = GameSettings.listPlayLaneCenterY[GamePlay.nPlayerLane];
		posEnd = new Vector3(nEndPositionX, nY, transform.position.z);
		Move();
	}
	
	public override bool CheckChangeToBonus() {
		bool bChanged = base.CheckChangeToBonus();
		
		if(bChanged) {
			nGameScore = 400;
			nHuntScore = 0;
		}
		
		return bChanged;
	}		
}
