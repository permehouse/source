using UnityEngine;
using System.Collections;

public class Fish_Fugu : Fish {
	// Use this for initialization
	void Start () {
		strAniName = "Fish_Fugu";
		nType = FishType.fugu;
		
		SetProperty(FishSettings.obFugu);
		
		int nDistanceX = Random.Range(0, (int)(GameSettings.nScreenWidth/2));		// adjust distance for randomizing fugu's expansion position
		transform.position = new Vector3(transform.position.x + nDistanceX, transform.position.y, transform.position.z);		
		
		posEnd = new Vector3(nEndPositionX, transform.position.y, transform.position.z);
		Move();
	}
	
	public override bool CheckChangeToBonus() {
		bool bChanged = base.CheckChangeToBonus();
		
		if(bChanged) {
			nGameScore = 300;
			nHuntScore = 0;
		}
		
		return bChanged;
	}	
}
