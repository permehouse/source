using UnityEngine;
using System.Collections;

public class Fish_Bonus : Fish {
	// Use this for initialization
	void Start () {
		strAniName = "Fish_Bonus";
		nType = FishType.bonus;
		
		SetProperty(FishSettings.obBonus);
		
		posEnd = new Vector3(nEndPositionX, transform.position.y, transform.position.z);
		Move();
	}
	
	public override void  SetMovePattern(Fish pfFish, Fish.MovePatternType nMovePatternType, int nLane) {
		if(nLane == -1) {
			nLane = Random.Range(0, GameSettings.nPlayLaneCount);
		}
		
		int nY= GameSettings.listPlayLaneCenterY[nLane];
		int nX = GameSettings.nScreenWidth + 200;
		transform.position = new Vector3(nX, nY, transform.position.z);		

		for (int i = 1; i < GameSettings.nCountBonusSet; i++) {
			Vector3 posSpawn = new Vector3(nX + (i * 150), nY, transform.position.z);		
			Fish obFish = Instantiate(pfFish, posSpawn, Quaternion.identity) as Fish;
			obFish.nLevel = nLevel;
		}		
	}
}
