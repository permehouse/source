using UnityEngine;
using System.Collections;
using System.Globalization;

public class Menu_Pause : MonoBehaviour {
	void Start () {
	 	tk2dSprite obSprite;
		obSprite =  GetComponent<tk2dSprite>();
		int nPositionX = GameSettings.nScreenWidth;
		int nPositionY = GameSettings.nScreenHeight;
		int nWidth = (int)obSprite.GetBounds().size.x;
		int nHeight = (int)obSprite.GetBounds().size.y;
		
		int nGap = 10;
		int nCenterX = nPositionX - nWidth/2 - nGap;
		int nCenterY = nPositionY - nHeight/2 - nGap;
		transform.position = new Vector3(nCenterX, nCenterY, GameSettings.LAYER_MENU);
	}		

	public void OnTouch() {
		MenuManager.Pause();
	}
}


