using UnityEngine;
using System.Collections;
using System.Globalization;

public class Menu_Quit : MonoBehaviour {
	void Start () {
		int nGapY = -60;
		int nCenterX = GameSettings.nScreenWidth/2;
		int nCenterY = GameSettings.nScreenHeight/2 + nGapY;
		transform.position = new Vector3(nCenterX, nCenterY, GameSettings.LAYER_MENU);
	}		

	public void OnTouch() {
		MenuManager.Quit();
	}
}


