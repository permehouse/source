using UnityEngine;
using System.Collections;

public class MgrGUICamera : MonoBehaviour
{
	public UIPanel panelMessageBox;
	public UIPanel panelLoading;

	// Use this for initialization
	void Start ()
	{
		const float baseWidth = 1280.0f;
		const float baseHeight = 720.0f;
		float sizeRate = 1.0f;

		sizeRate = baseHeight / (float)Screen.height;
		
		if((float)Screen.width < baseWidth / sizeRate)
		{
			sizeRate = baseWidth / (float)Screen.width;
		}

		GetComponent<Camera>().orthographicSize = sizeRate;

		panelMessageBox.gameObject.SetActive(false);
		panelLoading.gameObject.SetActive(false);
	}

	// Update is called once per frame
	void Update ()
	{
		
	}
	
	protected void ShowMessageBox(CommonDef.MessageBoxParam param)
	{
		panelMessageBox.gameObject.SetActive(true);
		panelMessageBox.SendMessage("ShowMessageBox", param);
	}

	protected void ShowLoadingScreen(bool bShow)
	{
		Debug.Log("active loading");
		panelLoading.gameObject.SetActive(bShow);
	}
}
