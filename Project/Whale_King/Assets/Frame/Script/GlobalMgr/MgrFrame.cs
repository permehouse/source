using UnityEngine;
using System.Collections;

public class MgrFrame : MonoBehaviour
{
	private bool bEscaping;
	public MgrFrame()
	{
		bEscaping = false;
	}
	
	void Start ()
	{
		GlobalValues.loadConfigValues();
		UICharactorPet.InitUICharactorPet();
		UIPayment.InitUIPayment();
	}
	
	void Update ()
	{
		if(Application.loadedLevelName == "Ready" || Application.loadedLevelName == "EndGame")
		{
			if (Input.GetKey(KeyCode.Escape) || Input.GetKey(KeyCode.Home))
			{
				if(false == bEscaping)
				{
					MessageBox.MessageBoxCallback callback = delegate(CommonDef.MESSAGE_BOX_RESULT result)
					{
						if(result == CommonDef.MESSAGE_BOX_RESULT.MB_YES)
						{
							Application.Quit();
						}
						bEscaping = false;
					};
	
					bEscaping = true;
					MessageBox.ShowMessageBox(GlobalStrings.confirmQuit, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_YESNO, callback);
				}
			}
		}
/*		else if(Application.loadedLevelName == "InGame")
		{
			if(Application.platform == RuntimePlatform.Android)
			{
//				if(Input.touchCount == 1)
//				{
//					Application.Quit();
//				}

				if (Input.GetKey(KeyCode.Escape) || Input.GetKey(KeyCode.Home))
				{
					Application.Quit();
				}
				if (Input.GetKey(KeyCode.Menu))
				{
					Application.LoadLevel("Ready");
				}
			}
			else if(Application.platform == RuntimePlatform.WindowsPlayer)
			{
				if(Input.GetMouseButtonDown(0) || Input.GetButtonDown("Fire1"))
				{
					Application.Quit();
				}
				if(Input.GetKey(KeyCode.Escape))
				{
					Application.Quit();
				}
				if (Input.GetKey(KeyCode.Backspace))
				{
					Application.LoadLevel("Ready");
				}
			}
			else if(Application.platform == RuntimePlatform.IPhonePlayer)
			{
				//Not supported
			}
			else
			{
				if(Input.GetMouseButtonDown(0) || Input.GetButtonDown("Fire1"))
				{
					Application.Quit();
				}
			}
		}
		else
		{
		}
*/		
	}

	void OnApplicationQuit()
	{
		ServerConnector connector = ServerConnector.GetInstance();
		if(null != connector)
			connector.Logout(null);
	}
}
