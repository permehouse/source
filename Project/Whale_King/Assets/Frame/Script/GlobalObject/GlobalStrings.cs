using UnityEngine;
using System.Collections;

public class GlobalStrings
{
	private GlobalStrings()
	{
	}
	
	static public string confirmQuit = "게임을 종료하시겠습니까?";
	static public string notEnoughLife = "라이프가 부족하여 게임을 실행할 수 없습니다.";
	
	static public string nouse = "소유하고 있지 않습니다.";		//임시.
	static public string willYouBuy = "구매하시겠습니까?";		//임시.
	
	static public string notEnoughGold = "골드가 부족합니다. 충전하시겠습니까?";
	static public string notEnoughDia = "다이아가 부족합니다. 충전하시겠습니까?";
	static public string alreadyBuy = "이미 구매하셨습니다.";
	static public string forbidBuy = "구매가능한 상품이 없습니다.";
	static public string successBuy = "구매가 완료되었습니다.";
	
	static public string newRecord = "주간 신기록 갱신으로 기회가 최대치로 충전됩니다.";
	
}
