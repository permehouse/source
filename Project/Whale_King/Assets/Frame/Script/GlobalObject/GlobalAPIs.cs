using UnityEngine;
using System.Collections;
using System.IO;

public class GlobalAPIs
{
	static public string GetCurTimeString()
	{
		//YYMMDDHHMMSS
		return System.DateTime.UtcNow.ToString(CommonDef.strDefaultDateTimeFormat);
	}

	static public void writeStringToFile(string str, string filename)
	{
		string path = pathForDocumentsFile(filename);

		FileStream file = new FileStream (path, FileMode.Create, FileAccess.Write);
		
		StreamWriter sw = new StreamWriter( file );
		//TODO solmea : To write multiline
		sw.WriteLine(str);
		
		sw.Close();
		file.Close();
	}

	static public string readStringFromFile(string filename)
	{
		string path = pathForDocumentsFile(filename);
		
		Debug.Log("readStringFromFile : " + path);

		if(File.Exists(path))
		{
			FileStream file = new FileStream (path, FileMode.Open, FileAccess.Read);
			StreamReader sr = new StreamReader( file );

			string strResult = "";
			//TODO solmea : To read multiline
			strResult = sr.ReadLine();

			sr.Close();
			file.Close();

			return strResult;
		}
		else
		{
			return "";
		}
	}

	static private string pathForDocumentsFile(string filename)
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			string path = Application.dataPath.Substring(0, Application.dataPath.Length - 5);
			path = path.Substring(0, path.LastIndexOf('/'));
			return Path.Combine(Path.Combine( path, "Documents" ), filename);
		}
		else if(Application.platform == RuntimePlatform.Android)
		{
			string path = Application.persistentDataPath; 
			path = path.Substring(0, path.LastIndexOf('/')); 
			return Path.Combine(path, filename);
		} 
		else 
		{
			string path = Application.dataPath; 
			path = path.Substring(0, path.LastIndexOf('/'));
			return Path.Combine(path, filename);
		}
	}

	static public void ShowLoadingScreen(bool bShow)
	{
		Debug.Log("functin ShowLoadingScreen");
		
		GameObject[] allPanel = GameObject.FindGameObjectsWithTag("GUICamera");
		foreach(GameObject panel in allPanel)
		{
			if(panel.name == "GUICamera")
			{
				Debug.Log("GUICamera - Show loading");
				panel.SendMessage("ShowLoadingScreen", bShow);
				break;
			}
		}
	}
}
