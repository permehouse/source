using UnityEngine;
using System.Collections;

public class UIPayment
{
	
	static public ArrayList starInfoList = new ArrayList();
	static public ArrayList goldInfoList = new ArrayList();
	static public ArrayList diaInfoList = new ArrayList();
		
	public struct StarInfo
	{
		public int nPrice;		//구입에 필요한 다이아 양.
		public int nCount;		//개수.
		
			public int nMultiply;		// 1 Price 로 얻을 수 있는 기본 Count.
		public int nDivid;	
		
		public StarInfo(int _Price,int _Count)
		{
			nCount = _Count;
			nPrice = _Price;	
			
			nMultiply = 1;
			nDivid = 1;
		}			
	};	
	
	public struct GoldInfo
	{
		public int nPrice;		//구입에 필요한 다이아 양.
		public int nCount;		//개수.
		
			public int nMultiply;		// 1 Price 로 얻을 수 있는 기본 Count.
		public int nDivid;	
		
		public GoldInfo(int _Price,int _Count)
		{
			nCount = _Count;
			nPrice = _Price;	
			
			nMultiply = 1000;
			nDivid = 1;
		}			
	};	
	
	public struct DiaInfo
	{
		public int nPrice;		//구입에 필요한 다이아 양.
		public int nCount;		//개수.
		
		public int nMultiply;		// 1 Price 로 얻을 수 있는 기본 Count.
		public int nDivid;	
		
		public DiaInfo(int _Price,int _Count)
		{
			nCount = _Count;
			nPrice = _Price;	
			nMultiply = 10;
			nDivid = 1100;
		}			
	};	
	
		
	static public void InitUIPayment()
	{
		starInfoList.Clear();
				
		starInfoList.Add(new StarInfo(5,5));
		starInfoList.Add(new StarInfo(10,11));
		starInfoList.Add(new StarInfo(20,24));
		starInfoList.Add(new StarInfo(50,65));
		starInfoList.Add(new StarInfo(100,150));
		
		
		goldInfoList.Clear();
		goldInfoList.Add(new GoldInfo(10,10000));
		goldInfoList.Add(new GoldInfo(44,48400));
		goldInfoList.Add(new GoldInfo(100,120000));
		goldInfoList.Add(new GoldInfo(250,325000));
		goldInfoList.Add(new GoldInfo(470,705000));
		
		diaInfoList.Clear();
		diaInfoList.Add(new DiaInfo(1100,10));
		diaInfoList.Add(new DiaInfo(4400,44));
		diaInfoList.Add(new DiaInfo(9900,108));
		diaInfoList.Add(new DiaInfo(29700,360));
		diaInfoList.Add(new DiaInfo(49500,675));
		diaInfoList.Add(new DiaInfo(99000,1595));
		
	}
	
	
	
}