using UnityEngine;
using System.Collections;

public class UICharactorPet
{
	public enum CHARACTOR_TYPE
	{
		CHARACTOR_DEFAULT = 0,
		CHARACTOR_SALABLE,
		CHARACTOR_UPGRADED,
	}

	public struct CharactorInfo
	{
//		public string charactorID;
		public string CharacterCode;		
		public string charactorName;
		public CHARACTOR_TYPE charactorType;
		public string charactorIconName;
		public int animationFramesPerSecond;
		public string animationNamePrefix;
		public int priceJewel;
		public int priceFish;
		public char grade;
		public string charactorExpressionName;
		public string charactorDescription;

		public CharactorInfo(string _CharacterCode, string _charactorName, CHARACTOR_TYPE _charactorType, string _charactorIconName, int _animationFramesPerSecond, string _animationNamePrefix, int _priceJewel, int _priceFish, char _grade, string _charactorExpressionName, string _charactorDescription)
		{
//			charactorID = _charactorID;
			CharacterCode = _CharacterCode;
			charactorName = _charactorName;
			charactorType = _charactorType;
			charactorIconName = _charactorIconName;
			animationFramesPerSecond = _animationFramesPerSecond;
			animationNamePrefix = _animationNamePrefix;
			priceJewel = _priceJewel;
			priceFish = _priceFish;
			grade = _grade;
			charactorExpressionName = _charactorExpressionName;
			charactorDescription = _charactorDescription;
		}
	};
	
	public struct PetInfo
	{
//		public string petID;
		public string petCode;
		public string petName;
		public int animationFramesPerSecond;
		public string animationNamePrefix;
		public int price;
		public char grade;
		public string petExpressionName;
		public string petDescription;

		public PetInfo(string _petCode, string _petName, int _animationFramesPerSecond, string _animationNamePrefix,  int _price, char _grade, string _petExpressionName, string _petDescription)
		{
//			petID = _petID;
			petCode = _petCode;
			petName = _petName;
			animationFramesPerSecond = _animationFramesPerSecond;
			animationNamePrefix = _animationNamePrefix;
			price = _price;
			grade = _grade;
			petExpressionName = _petExpressionName;
			petDescription = _petDescription;
		}
	};
	
	static public ArrayList charactorInfoList = new ArrayList();
	static public ArrayList petInfoList = new ArrayList();

	// 처음 가지고 있는 캐릭터와 물개 세팅. 차후에는 파일로드로 구현해야 함.
	static public string selectedCharacterCode = "CH_DEFAULT";
	static public string selectedRelayCharacterCode = "";
	static public string selectedPetCode = "PET_SEAL";			// 물개.
	static public string selectedCharacterExpressionName = "그냥 고래";			// yj edit.
	static public string selectedExpressionPet_1_Name = "";
	
	static public void InitUICharactorPet()
	{
		charactorInfoList.Clear();
		
		//charactorInfoList.Add(new CharactorInfo("NONE", "NoCharactor", CHARACTOR_TYPE.CHARACTOR_DEFAULT, "", 0, "NoCharactor", 0, 0, 'N', "", ""));
		charactorInfoList.Add(new CharactorInfo("CH_DEFAULT_C", "Whale", CHARACTOR_TYPE.CHARACTOR_DEFAULT, "CharactorWhaleIcon", 5, "CharactorWhaleAni_", 0, 0, 'C', "그냥 고래", "기본능력"));
		//charactorInfoList.Add(new CharactorInfo("1B", "Whale", CHARACTOR_TYPE.CHARACTOR_UPGRADED, "CharactorWhaleIcon", 5, "CharactorWhaleAni_", 0, 0, 'B', "미남 고래", "내가 고래다"));

		//charactorInfoList.Add(new CharactorInfo("2B", "Octopus", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorWhaleIcon", 5, "CharactorWhaleAni_", 10000, 0, 'B', "어리숙한 문어", "1회 도망.\n(레벨-1) * 10 HP 상승"));
		//charactorInfoList.Add(new CharactorInfo("2A", "Octopus", CHARACTOR_TYPE.CHARACTOR_UPGRADED, "CharactorWhaleIcon", 5, "CharactorWhaleAni_", 0, 0, 'A', "번질거리는 문어", "2회 도망.\n(레벨-1) * 10 HP 상승"));

		charactorInfoList.Add(new CharactorInfo("CH_POLAR_B", "PolarBear", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorPolarBearIcon", 5, "CharactorPolarBearAni_", 0, 45, 'B', "잘먹는 백곰", "10% 소화능력상승")); // + (레벨-1) X 0.5%"));      //yj edit   //임시로 25->30% 상향.
		//charactorInfoList.Add(new CharactorInfo("3A", "PolarBear", CHARACTOR_TYPE.CHARACTOR_UPGRADED, "CharactorPolarBearIcon", 5, "CharactorPolarBearAni_", 0, 0, 'A', "대식가 백곰", "소화능력 50% 상승 + (레벨-1) X 0.5%"));
		
		//charactorInfoList.Add(new CharactorInfo("4B", "Turtle", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorWhaleIcon", 5, "CharactorWhaleAni_", 12000, 0, 'B', "단단한 거북", "피해감소 25% 상승 + (레벨-1) X 0.5%"));
		//charactorInfoList.Add(new CharactorInfo("4A", "Turtle", CHARACTOR_TYPE.CHARACTOR_UPGRADED, "CharactorWhaleIcon", 5, "CharactorWhaleAni_", 0, 0, 'A', "무적방패 문어", "피해감소 50% 상승 + (레벨-1) X 0.5%"));

		//charactorInfoList.Add(new CharactorInfo("CH_PORORO", "Pororo", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorPororoIcon", 5, "CharactorPororoAni_", 0, 90, 'B', "꼬마 펭귄", "20% 빠른 성장\n20% 피해 감소"));							//yj edit		//임시로 수치 상향.
		charactorInfoList.Add(new CharactorInfo("CH_PORORO_A", "Pororo", CHARACTOR_TYPE.CHARACTOR_UPGRADED, "CharactorPororoIcon", 5, "CharactorPororoAni_", 0, 60, 'A', "뽀통령",   "20% 빠른성장"));							//yj edit		//임시로 수치 상향.
		
		//테스트.
		charactorInfoList.Add(new CharactorInfo("CH_MOBYDICK_B", "Mobydick", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorMobydickIcon", 5, "CharactorMobydickAni", 0, 50, 'B', "모비딕",   "30% 빠른성장"));							//yj edit		//임시로 수치 상향.
		charactorInfoList.Add(new CharactorInfo("CH_TURTLE_B", "Turtle", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorTurtleIcon", 5, "CharactorTurtleAni", 0, 70, 'A', "왕거북",   "25% 피해감소"));							//yj edit		//임시로 수치 상향.
		charactorInfoList.Add(new CharactorInfo("CH_OCTOPUS_B", "Octopus", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorOctopusIcon", 5, "CharactorOctopusAni", 0, 60, 'B',"문어",   "25% 도망"));							//yj edit		//임시로 수치 상향.
		//charactorInfoList.Add(new CharactorInfo("CH_PORORO", "Mobydick", CHARACTOR_TYPE.CHARACTOR_SALABLE, "CharactorMobydickIcon", 5, "Game_greyGorae_swim1", 0, 0, 'B', "모비딕",   "30딕% 빠른 성장\n40% 피해 감소"));							//yj edit		//임시로 수치 상향.
		
		//charactorInfoList.Add(new CharactorInfo("CH_PORORO", "Pororo", CHARACTOR_TYPE.CHARACTOR_UPGRADED, "CharactorPororoIcon", 5, "CharactorPororoAni_", 0, 0, 'A', "뽀통령",   "30% 빠른 성장\n40% 피해 감소"));							//yj edit		//임시로 수치 상향.
		//charactorInfoList.Add(new CharactorInfo("CH_PORORO", "Pororo", CHARACTOR_TYPE.CHARACTOR_UPGRADED, "CharactorPororoIcon", 5, "CharactorPororoAni_", 0, 0, 'A', "뽀통령",   "30% 빠른 성장\n40% 피해 감소"));							//yj edit		//임시로 수치 상향.

		//petInfoList.Add(new PetInfo("0", "PetUnknown", 0, "PetUnknown", 'N', "팻없음", ""));
		
		petInfoList.Clear();
		petInfoList.Add(new PetInfo("PET_SHARK_C", "PetShark", 5, "PetSharkAni_", 10,  'C', "아기 상어", "소형 적 사냥"));
		//petInfoList.Add(new PetInfo("1B", "PetShark", 5, "PetSharkAni_", 'B', "꼬마", "소형 적 사냥\n5% 중형 적 사냥"));
		//petInfoList.Add(new PetInfo("1A", "PetShark", 5, "PetSharkAni_", 'A', "소년", "소형,중형 적 사냥\n5% 대형 적 사냥"));
		//petInfoList.Add(new PetInfo("1S", "PetShark", 5, "PetSharkAni_", 'S', "쌍둥이", "2 마리\n소형,중형 적 사냥\n5% 대형 적 사냥"));

		petInfoList.Add(new PetInfo("PET_SEAL_C", "PetWalrus", 5, "PetWalrusAni_", 15, 'C', "작은 물개", "8초 마다 돌진하여\n모든 적 공격"));			//바다 코끼리.
		//petInfoList.Add(new PetInfo("2B", "PetWalrus", 5, "PetWalrusAni_", 'B', "보통", "2초 마다 돌진"));
		//petInfoList.Add(new PetInfo("2A", "PetWalrus", 5, "PetWalrusAni_", 'A', "커다란", "1초 마다 돌진"));
		//petInfoList.Add(new PetInfo("2S", "PetWalrus", 5, "PetWalrusAni_", 'S', "괴물", "1초 마다 강력한 돌진"));
		
		petInfoList.Add(new PetInfo("PET_JELLYFISH_C", "PetJellyfish", 5, "PetJellyfishAni_", 15, 'C', "작은 해파리", "3초 마다 물고기\n3마리 증가"));
		petInfoList.Add(new PetInfo("PET_MERMAID_A", "PetPeoplefish", 5, "PetPeoplefishAni_", 20, 'C', "소녀 인어", "20초 마다\n작은 유혹"));
		petInfoList.Add(new PetInfo("PET_RAY_C", "PetRay", 5, "PetRayAni_", 15, 'C', "가오리", "거대화 시간 증가"));
		petInfoList.Add(new PetInfo("PET_PENGUIN_B", "PetPenguin", 5, "PetPenguinAni_", 10, 'C', "펭귄", "거대 펭귄 변신"));
		petInfoList.Add(new PetInfo("PET_SQUID_B", "PetSquid", 5, "PetSquidAni_", 15, 'C', "오징어", "수혈 에너지 증가"));
		petInfoList.Add(new PetInfo("PET_STARFISH_C", "PetStarfish", 5, "PetStarfishAni_", 15, 'C', "불가사리", "아이템 사용시간 증가"));
		petInfoList.Add(new PetInfo("PET_TOPSHELL_C", "PetTopshell", 5, "PetTopshellAni_", 20, 'C', "소라", "아이템 생성"));
		petInfoList.Add(new PetInfo("PET_CLAM_C", "PetClam", 5, "PetClamAni_", 15, 'C', "조개", "경험치 상승"));
		
		//petInfoList.Add(new PetInfo("8B", "PetPeoplefish", 5, "PetPeoplefishAni_", 'B', "예쁜", "27초 마다 작은 유혹"));
		//petInfoList.Add(new PetInfo("8A", "PetPeoplefish", 5, "PetPeoplefishAni_", 'A', "매혹의", "24초 마다 작은 유혹"));
		//petInfoList.Add(new PetInfo("8S", "PetPeoplefish", 5, "PetPeoplefishAni_", 'S', "팜므파탈", "24초 마다 큰 유혹"));
	}
	
	static public CharactorInfo GetCharacterInfoFromCode(string strCharacterCode)
	{
		CharactorInfo character = (CharactorInfo)charactorInfoList[0];
//		if("CH_DEFAULT" != strCharacterID && "NONE" != strCharacterID)
		if("NONE" != strCharacterCode)
		{
			for(int i = 0;i<charactorInfoList.Count;++i)
			{
				if(strCharacterCode == ((CharactorInfo)charactorInfoList[i]).CharacterCode)
				{
					character = (CharactorInfo)charactorInfoList[i];
					break;
				}
			}
		}

		return character;
	}
	
	static public PetInfo GetPetInfoFromCode(string strPetCode)
	{
		PetInfo pet = (PetInfo)petInfoList[0];
		for(int i = 0;i<petInfoList.Count;++i)
		{
			if(strPetCode == ((PetInfo)petInfoList[i]).petCode)
			{
				pet = (PetInfo)petInfoList[i];
				break;
			}
		}

		return pet;
	}
	
	static public Player.PlayerType GetCurrentInGameCharactor()
	{
		return GetGameCharactorType(selectedCharacterCode);
	}

	static public Player.PlayerType GetCurrentInGameRelayCharactor()
	{
		Debug.Log(">>> Get Relay Character Type");
		Player.PlayerType nType = GetGameCharactorType(selectedRelayCharacterCode);

		if(nType == Player.PlayerType.none) {
			GlobalValues.gameBoost.nCount_Relay = 0;
		}
		else {
			GlobalValues.gameBoost.nCount_Relay = 1;
		}
		
		return nType;
	}
	
	static public Player.PlayerType GetGameCharactorType(string strCharacterCode) 
	{
		if(strCharacterCode == "CH_DEFAULT_C") {
			Debug.Log("Player.PlayerType.whale");
			return Player.PlayerType.whale;
		}
		else if(strCharacterCode == "CH_POLAR_B") {
			Debug.Log("Player.PlayerType.polarbear");
			return Player.PlayerType.polarbear;
		}
		else if(strCharacterCode == "CH_PORORO_A") {
			Debug.Log("Player.PlayerType.pororo");
			return Player.PlayerType.pororo;
		}
		else if(strCharacterCode == "CH_MOBYDICK_B") {
			Debug.Log("Player.PlayerType.mobydick");
			return Player.PlayerType.mobydick;			
		}
		else if(strCharacterCode == "CH_TURTLE_B") {
			Debug.Log("Player.PlayerType.turtle");
			return Player.PlayerType.turtle;			
		}
		else if(strCharacterCode == "CH_OCTOPUS_B") {
			Debug.Log("Player.PlayerType.octopus");
			return Player.PlayerType.octopus;			
		}
		
		Debug.Log("Player.PlayerType.none");
		return Player.PlayerType.none;
	}

	static public Pet.PetType GetCurrentInGamePet()
	{
		if("" == selectedPetCode)
		{
			Debug.Log("Pet id is null");
			return Pet.PetType.none;
		}
		//차후 수정.
		
		if(selectedPetCode == "PET_SHARK_C") {
			Debug.Log("Pet.PetType.littleshark");
			return Pet.PetType.littleshark;
		}
		else if(selectedPetCode == "PET_SEAL_C") {
			Debug.Log("Pet.PetType.walrus");
			return Pet.PetType.walrus;			
		}
		else if(selectedPetCode == "PET_JELLYFISH_C") {
			Debug.Log("Pet.PetType.jellyfish");
			return Pet.PetType.jellyfish;			
		}
		else if(selectedPetCode == "PET_MERMAID_A") {
			Debug.Log("Pet.PetType.mermaid");
			return Pet.PetType.mermaid;			
		}		
		else if(selectedPetCode == "PET_TOPSHELL_C") {
			Debug.Log("Pet.PetType. topshell");
			return Pet.PetType.topshell;			
		}
		else if(selectedPetCode == "PET_STARFISH_C") {
			Debug.Log("Pet.PetType starfish");
			return Pet.PetType.starfish;			
		}
		else if(selectedPetCode == "PET_SQUID_B") {
			Debug.Log("Pet.PetType squid");
			return Pet.PetType.squid;			
		}
		else if(selectedPetCode == "PET_RAY_C") {
			Debug.Log("Pet.PetType ray");
			return Pet.PetType.ray;			
		}
		else if(selectedPetCode == "PET_PENGUIN_B") {
			Debug.Log("Pet.PetType penguin");
			return Pet.PetType.penguin;			
		}
		else if(selectedPetCode == "PET_CLAM_C") {
			Debug.Log("Pet.PetType clam");
			return Pet.PetType.clam;			
		}
		
		Debug.Log("Default - Pet.PetType.none");
		return Pet.PetType.none;		
/*		
		switch(selectedPetCode[0])
		{
		case '0':
			Debug.Log("Pet.PetType.none");
			return Pet.PetType.none;
		case '1':
			Debug.Log("Pet.PetType.littleshark");
			return Pet.PetType.littleshark;
		case '2':
			Debug.Log("Pet.PetType.walrus");
			return Pet.PetType.walrus;
		case '3':
			Debug.Log("Pet.PetType.jellyfish");
			return Pet.PetType.jellyfish;
//		case '4':
//			Debug.Log("Pet.PetType.PetConch");
//			return Pet.PetType.PetConch;
//		case '5':
//			Debug.Log("Pet.PetType.PetPenguin");
//			return Pet.PetType.PetPenguin;
//		case '6':
//			Debug.Log("Pet.PetType.PetStarfish");
//			return Pet.PetType.PetStarfish;
//		case '7':
//			Debug.Log("Pet.PetType.PetCoral");
//			return Pet.PetType.PetCoral;
		case '8':
			Debug.Log("Pet.PetType.mermaid");
			return Pet.PetType.mermaid;
		default:
			Debug.Log("Default - Pet.PetType.none");
			return Pet.PetType.none;
		}
*/		
	}
}
