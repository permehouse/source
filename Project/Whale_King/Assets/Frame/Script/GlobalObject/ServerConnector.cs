using UnityEngine;
using System.Collections;

public class ServerConnector : MonoBehaviour
{
	public string strDomainName { get { return "http://" + GlobalValues.strHostName; } }
	protected string strURLLogin { get { return strDomainName + "/game/user/login.do"; } }
	protected string strURLLogout { get { return strDomainName + "/game/user/logout.do"; } }

	protected string strURLNotice { get { return strDomainName + "/game/user/retrieve_notice.do"; } }
	protected string strURLFriendList { get { return strDomainName + "/game/user/retrieve_contactlist_by_rank.do"; } }
	protected string strURLFriendListInGame { get { return strDomainName + "/game/user/retrieve_target_contactlist_by_distance.do"; } }
//	protected string strURLFriendList { get { return strDomainName + "/game/user/list.do"; } }
	
	protected string strURLStartGame { get { return strDomainName + "/game/play/start.do"; } }
	protected string strURLEndGame { get { return strDomainName + "/game/play/end.do"; } }
	protected string strURLGetDigestFishes { get { return strDomainName + "/game/play/intercept.do"; } }
	protected string strURLStoreSettings { get { return strDomainName + "/game/user/settings.do"; } }
	protected string strURLRetrieveGameUser { get { return strDomainName + "/game/user/retrieve_gameuser.do"; } }

	protected string strURLChangeCharacter { get { return strDomainName + "/game/user/change_character.do"; } }
	protected string strURLChangePet { get { return strDomainName + "/game/user/change_pet.do"; } }
	
	protected string strURLBuy { get { return strDomainName + "/game/item/buy.do"; } }		// 캐릭터,펫,아이템 구입.
	
	
	
	static public ServerConnector _instance = null;
	
	static public ServerConnector GetInstance()
	{
		if(_instance == null) {
			GameObject[] objects = GameObject.FindGameObjectsWithTag("Connection"); 
			foreach(GameObject connectorObject in objects)
			{
				if(connectorObject.name == "ServerConnector")
				{
					_instance = connectorObject.GetComponentInChildren<ServerConnector>();
				}
			}
		}
		
		return _instance;
	}

	public ServerConnector()
	{
	}

	void Start ()
	{
	}
	
	void Update ()
	{
	}
	
	public delegate void ServerConnectionFinished(int result);

	public void Login(ServerConnectionFinished finishedCallback)
	{
		ServerConnectorCallback callback = delegate(string resultJSONString)
		{
			int result = 0;

			JSONObject jsonObjResult = new JSONObject(resultJSONString);
			if(null != jsonObjResult["result"])
			{
				result = System.Convert.ToInt32(jsonObjResult["result"].str);
			}
			else
			{
				result = CommonDef.errorNotFoundResult;
			}
			
			if(CommonDef.errorSuccessed == result)
			{
				GlobalValues.sessionId = jsonObjResult["sessionId"].str;
				GlobalValues.serverTime = System.DateTime.ParseExact(jsonObjResult["loginDate"].str, CommonDef.strDefaultDateTimeFormat, null);
				GlobalValues.timeGabFromServer = GlobalValues.serverTime - System.DateTime.UtcNow;
				Debug.Log("serverTime, " + GlobalValues.serverTime.ToString());
				Debug.Log("timeGabFromServer, " + GlobalValues.timeGabFromServer.ToString());
				Debug.Log("Server/Client time gab:" + GlobalValues.timeGabFromServer.ToString());

				JSONObject jsonObjGameUser = jsonObjResult["gameUser"];

				GlobalValues.gameUser.SetGameUserData(jsonObjGameUser);
				GlobalValues.settings.chanceReceive = jsonObjGameUser["chanceReceive"].n;
				GlobalValues.settings.soundPlay = jsonObjGameUser["sound"].n;
				GlobalValues.settings.pushReceive = jsonObjGameUser["pushReceive"].n;
				GlobalValues.settings.dragController = jsonObjGameUser["control"].n;
				GlobalValues.settings.lowQuality = jsonObjGameUser["quality"].n;				

				GameObject[] allPanel = GameObject.FindGameObjectsWithTag("UIPanel");
				foreach(GameObject panel in allPanel)
				{
					if(panel.name == "PanelReady")
					{
						panel.SendMessage("LoginCompleted");
						break;
					}
				}
			}
			else
			{
				MessageBox.ShowMessageBox("Login error\nErrorCode:" + result.ToString(), CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
			}

			if(null != finishedCallback)
				finishedCallback(result);
		};

		JSONObject objLogin = new JSONObject(JSONObject.Type.OBJECT);
		objLogin.AddField("udid", SystemInfo.deviceUniqueIdentifier);
		objLogin.AddField("email", GlobalValues.EMail);
		objLogin.AddField("name", GlobalValues.EMail);
		//Environment
		objLogin.AddField("deviceName", SystemInfo.deviceName);
		objLogin.AddField("deviceModel", SystemInfo.deviceModel);
		objLogin.AddField("deviceType", SystemInfo.deviceType.ToString());
		objLogin.AddField("os", SystemInfo.operatingSystem);
		objLogin.AddField("processor", SystemInfo.processorType);
		objLogin.AddField("processorCount", SystemInfo.processorCount);
		objLogin.AddField("graphicsDeviceName", SystemInfo.graphicsDeviceName);
		objLogin.AddField("graphicsDeviceVendor", SystemInfo.graphicsDeviceVendor);
		
		Debug.Log("Logon URL : " + strURLLogin);
		
		QueryServer(strURLLogin, objLogin.print(), callback, true);
	}

	public void Logout(ServerConnectionFinished finishedCallback)
	{
		ServerConnectorCallback callback = delegate(string resultJSONString)
		{
			int result = 0;

			JSONObject jsonObjResult = new JSONObject(resultJSONString);
			if(null != jsonObjResult["result"])
			{
				result = System.Convert.ToInt32(jsonObjResult["result"].str);
			}
			else
			{
				result = CommonDef.errorNotFoundResult;
			}
			
			if(CommonDef.errorSuccessed == result)
			{
				GlobalValues.sessionId = "";
				GlobalValues.gameUser.ResetGameUserData();
			}

			if(null != finishedCallback)
				finishedCallback(result);
		};

		JSONObject objLogout = new JSONObject(JSONObject.Type.OBJECT);
		objLogout.AddField("sessionId", GlobalValues.sessionId);

		QueryServer(strURLLogout, objLogout.print(), callback, true);
	}
	
	public void RetrieveGameUser(ServerConnectionFinished finishedCallback)
	{
		ServerConnectorCallback callback = delegate(string resultJSONString)
		{
			int result = 0;

			JSONObject jsonObjResult = new JSONObject(resultJSONString);
			if(null != jsonObjResult["result"])
			{
				result = System.Convert.ToInt32(jsonObjResult["result"].str);
			}
			else
			{
				result = CommonDef.errorNotFoundResult;
			}
			
			Debug.Log ("Game user Enter2 "+CommonDef.errorSuccessed+"  "+result);
			if(CommonDef.errorSuccessed == result)
			{
				Debug.Log ("Game user Enter");
				GlobalValues.gameUser.SetGameUserData(jsonObjResult["gameUser"]);
			}
			else
			{
//				MessageBox.ShowMessageBox("Retrieve data error\nErrorCode:" + result.ToString(), CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
			}

			if(null != finishedCallback)
				finishedCallback(result);
		};

		JSONObject objRetrieve = new JSONObject(JSONObject.Type.OBJECT);
		objRetrieve.AddField("sessionId", GlobalValues.sessionId);
		
		Debug.Log("RetrieveGameUser, " + GlobalValues.sessionId);

		QueryServer(strURLRetrieveGameUser, objRetrieve.print(), callback);
	}

	public void QueryNotice(ServerConnectionFinished finishedCallback)
	{
		ServerConnectorCallback callback = delegate(string resultJSONString)
		{
			int result = CommonDef.errorSuccessed;
			JSONObject jsonObjResult = new JSONObject(resultJSONString);
			if(null != jsonObjResult["result"])
			{
				result = System.Convert.ToInt32(jsonObjResult["result"].str);
			}
			else
			{
				result = CommonDef.errorNotFoundResult;
			}

			if(CommonDef.errorSuccessed == result)
			{
				JSONObject jsonObjNotice = jsonObjResult["noticeList"];
//				Debug.Log("Notice list count:" + jsonObjNotice.count().ToString());
				for(int i = 0;i<jsonObjNotice.count();++i)
				{
					GlobalValues.Board noticeItem = new GlobalValues.Board();
					noticeItem.subject = jsonObjNotice[i]["subject"].str;
					noticeItem.content = jsonObjNotice[i]["content"].str;
					noticeItem.writeDate = jsonObjNotice[i]["writeDate"].str;
					GlobalValues.noticeList.Add(noticeItem);
				}
			}

			if(null != finishedCallback)
				finishedCallback(result);
		};

		JSONObject objQueryNotice = new JSONObject(JSONObject.Type.OBJECT);
		objQueryNotice.AddField("sessionId", GlobalValues.sessionId);

		QueryServer(strURLNotice, objQueryNotice.print(), callback);
	}

	public void QueryFriendList(ServerConnectionFinished finishedCallback)
	{
		ServerConnectorCallback callback = delegate(string resultJSONString)
		{
			int result = 0;
			JSONObject jsonObjResult = new JSONObject(resultJSONString);
			if(null != jsonObjResult["result"])
			{
				result = System.Convert.ToInt32(jsonObjResult["result"].str);
			}
			else
			{
				result = CommonDef.errorNotFoundResult;
			}

			GlobalValues.friendList.Clear();

			if(CommonDef.errorSuccessed == result)
			{
				JSONObject jsonObjGameContace = jsonObjResult["contactList"];
				for(int i=0;i<jsonObjGameContace.count();++i)
				{
					GlobalValues.GameContact contact = new GlobalValues.GameContact();
					contact.guid = jsonObjGameContace[i]["guid"].n;
					contact.email = jsonObjGameContace[i]["email"].str;
					contact.name = jsonObjGameContace[i]["name"].str;
					contact.character = jsonObjGameContace[i]["character"].str;
					contact.weekScore = jsonObjGameContace[i]["weekScore"].n;
					contact.digestFishes = jsonObjGameContace[i]["digestFishes"].n;
					contact.weekDistance = jsonObjGameContace[i]["weekDistance"].n;
					contact.week = jsonObjGameContace[i]["week"].n;
					contact.imageUrl = jsonObjGameContace[i]["imageUrl"].str;
					contact.chanceReceive = jsonObjGameContace[i]["chanceReceive"].n;
					contact.pushReceive = jsonObjGameContace[i]["pushReceive"].n;
					GlobalValues.friendList.Add(contact);
					
				//	Debug.Log(">>> FriendList " + (i+1) + "/" + jsonObjGameContace.count() + ", guid : " + contact.guid + ", name : " + contact.name + ",  week : " + contact.week + ",  weekScore : " + contact.weekScore);
				}
			}

			if(null != finishedCallback)
				finishedCallback(result);
		};

		JSONObject objQueryFriendList = new JSONObject(JSONObject.Type.OBJECT);
		objQueryFriendList.AddField("sessionId", GlobalValues.sessionId);

		QueryServer(strURLFriendList, objQueryFriendList.print(), callback);
	}
	
	public void QueryFriendListInGame(ServerConnectionFinished finishedCallback)
	{
		ServerConnectorCallback callback = delegate(string resultJSONString)
		{
			int result = 0;
			JSONObject jsonObjResult = new JSONObject(resultJSONString);
			if(null != jsonObjResult["result"])
			{
				result = System.Convert.ToInt32(jsonObjResult["result"].str);
			}
			else
			{
				result = CommonDef.errorNotFoundResult;
			}

			GlobalValues.friendListInGame.Clear();

			if(CommonDef.errorSuccessed == result)
			{
				JSONObject jsonObjGameContace = jsonObjResult["contactList"];
				for(int i=0;i<jsonObjGameContace.count();++i)
				{
					GlobalValues.GameContact contact = new GlobalValues.GameContact();
					contact.guid = jsonObjGameContace[i]["guid"].n;
					contact.email = jsonObjGameContace[i]["email"].str;
					contact.name = jsonObjGameContace[i]["name"].str;
					contact.character = jsonObjGameContace[i]["character"].str;
					contact.weekScore = jsonObjGameContace[i]["weekScore"].n;
					contact.digestFishes = jsonObjGameContace[i]["digestFishes"].n;
					contact.weekDistance = jsonObjGameContace[i]["weekDistance"].n;
					contact.week = jsonObjGameContace[i]["week"].n;
					contact.imageUrl = jsonObjGameContace[i]["imageUrl"].str;
					contact.chanceReceive = jsonObjGameContace[i]["chanceReceive"].n;
					contact.pushReceive = jsonObjGameContace[i]["pushReceive"].n;
					contact.isDead = false;		// set default as 'false'
					GlobalValues.friendListInGame.Add(contact);
					
					Debug.Log(">>> FriendListInGame " + (i+1) + "/" + jsonObjGameContace.count() + ", guid : " + contact.guid + ", name : " + contact.name + ",  week : " + contact.week + ",  weekScore : " + contact.weekScore + ",  digestFishes : " + contact.digestFishes);
				}
			}

			if(null != finishedCallback)
				finishedCallback(result);
		};

		JSONObject objQueryFriendList = new JSONObject(JSONObject.Type.OBJECT);
		objQueryFriendList.AddField("sessionId", GlobalValues.sessionId);

		QueryServer(strURLFriendListInGame, objQueryFriendList.print(), callback);
	}
	
	public void GetFriendListInGame(ServerConnectionFinished finishedCallback) {
		ServerConnector.ServerConnectionFinished callback = delegate(int result)	{
			if(CommonDef.errorSuccessed == result) {
				Debug.Log("Get QueryFriendListInGame OK");
			}
			else	{
				Debug.Log("Get QueryFriendListInGame, code:" + result.ToString());
			}

			if(null != finishedCallback)
				finishedCallback(result);			
		};
		
		QueryFriendListInGame(callback);
	}
	
	public void StartGame(ServerConnectionFinished finishedCallback) {
		ServerConnectorCallback callback = delegate(string resultJSONString)
		{
			int result = 0;
			JSONObject jsonObjResult = new JSONObject(resultJSONString);
			if(null != jsonObjResult["result"])
			{
				result = System.Convert.ToInt32(jsonObjResult["result"].str);
			}
			else
			{
				result = CommonDef.errorNotFoundResult;
			}

			if(CommonDef.errorSuccessed == result)
			{
				GlobalValues.gamePlay.gpid = System.Convert.ToInt32(jsonObjResult["playId"].n);
				
				GetFriendListInGame(finishedCallback);
			}
			else {
				GlobalAPIs.ShowLoadingScreen(false);
				finishedCallback(result);			
			}
		};
		
		Debug.Log("sessionId : " + GlobalValues.sessionId);
		
		JSONObject objStartGame = new JSONObject(JSONObject.Type.OBJECT);
		objStartGame.AddField("sessionId", GlobalValues.sessionId);

		QueryServer(strURLStartGame, objStartGame.print(), callback, true, true);
	}
	
	public void EndGame(ServerConnectionFinished finishedCallback)
	{
		ServerConnectorCallback callback = delegate(string resultJSONString)
		{
			int result = 0;			
			GlobalValues.userValues.chanceUpdated = 0;		
			JSONObject jsonObjResult = new JSONObject(resultJSONString);
			if(null != jsonObjResult["result"])
			{
				result = System.Convert.ToInt32(jsonObjResult["result"].str);
				GlobalValues.userValues.chanceUpdated = jsonObjResult["chanceUpdated"].n;		// 주간 최고 기록 세웠는지 저장함.								
				Debug.Log(":::: GlobalValues.userValues.chanceUpdated : " + GlobalValues.userValues.chanceUpdated);
			}
			else
			{
				result = CommonDef.errorNotFoundResult;				
				
			}
			if(null != finishedCallback)
				finishedCallback(result);
		};

		JSONObject objEndGame = new JSONObject(JSONObject.Type.OBJECT);
		objEndGame.AddField("sessionId", GlobalValues.sessionId);
		objEndGame.AddField("playId", GlobalValues.gamePlay.gpid);
		objEndGame.AddField("digestFishes", GlobalValues.gamePlay.digestFishes);  //  131127 수정.
		objEndGame.AddField("fishes", GlobalValues.gamePlay.fishes);
		objEndGame.AddField("score", GlobalValues.gamePlay.score);
		objEndGame.AddField("distance", GlobalValues.gamePlay.distance);
		
		JSONObject obFriendDeadList = new JSONObject(JSONObject.Type.ARRAY);
		for(int i = 0; i < GlobalValues.friendListInGame.Count; i++) {
			GlobalValues.GameContact contact = (GlobalValues.GameContact)GlobalValues.friendListInGame[i];
			if(contact.isDead) {
				obFriendDeadList.Add(new JSONObject(contact.guid));
			}
		}
		
		if(obFriendDeadList.count() > 0) {
			objEndGame.AddField("guids", obFriendDeadList);
		}
		
		Debug.Log("Query End Game, Fishes : " + GlobalValues.gamePlay.fishes);
		Debug.Log("Query End Game, digestFishes : " + GlobalValues.gamePlay.digestFishes);
		Debug.Log("Query End Game, score : " + GlobalValues.gamePlay.score);
		Debug.Log("Query End Game, distance : " + GlobalValues.gamePlay.distance);		
		
		QueryServer(strURLEndGame, objEndGame.print(), callback);
	}

	public void GetDigestFishes(int guid, ServerConnectionFinished finishedCallback)
	{
		ServerConnectorCallback callback = delegate(string resultJSONString)
		{
			int result = 0;
			JSONObject jsonObjResult = new JSONObject(resultJSONString);
			if(null != jsonObjResult["result"])
			{
				result = System.Convert.ToInt32(jsonObjResult["result"].str);
			}
			else
			{
				result = CommonDef.errorNotFoundResult;
			}

			int amount = 0;
			if(CommonDef.errorSuccessed == result) {
				if(null != jsonObjResult["amount"])
				{
					amount = jsonObjResult["amount"].n;
				}
			}
			
			if(null != finishedCallback)
				finishedCallback(amount);
		};

		JSONObject objGetDigestFishes = new JSONObject(JSONObject.Type.OBJECT);
		objGetDigestFishes.AddField("sessionId", GlobalValues.sessionId);
		objGetDigestFishes.AddField("playId", GlobalValues.gamePlay.gpid);
		objGetDigestFishes.AddField("guid", guid);
		int nVerify = Random.Range(1000,10000);
		objGetDigestFishes.AddField("verify", nVerify.ToString());
		
		Debug.Log("sessionId : " + GlobalValues.sessionId);
		Debug.Log("playerId : " + GlobalValues.gamePlay.gpid);
		Debug.Log("Query GetDigestFishes, guid : " + guid);
		Debug.Log("Query Verify : " + nVerify);
		
		QueryServer(strURLGetDigestFishes, objGetDigestFishes.print(), callback);
	}
	
	public void StoreSettings(ServerConnectionFinished finishedCallback)
	{
		ServerConnectorCallback callback = delegate(string resultJSONString)
		{
			int result = 0;
			JSONObject jsonObjResult = new JSONObject(resultJSONString);
			if(null != jsonObjResult["result"])
			{
				result = System.Convert.ToInt32(jsonObjResult["result"].str);
				Debug.Log("result:" + result.ToString());
			}
			else
			{
				result = CommonDef.errorNotFoundResult;
			}

			if(null != finishedCallback)
				finishedCallback(result);
		};

		JSONObject objSettings = new JSONObject(JSONObject.Type.OBJECT);
		objSettings.AddField("sessionId", GlobalValues.sessionId);
		objSettings.AddField("sound", GlobalValues.settings.soundPlay);
		objSettings.AddField("chanceReceive", GlobalValues.settings.chanceReceive);
		objSettings.AddField("pushReceive", GlobalValues.settings.pushReceive);
		objSettings.AddField("control", GlobalValues.settings.dragController);
		objSettings.AddField("quality", GlobalValues.settings.lowQuality);

		QueryServer(strURLStoreSettings, objSettings.print(), callback);
	}
	
	public void ChangeCharacter(ServerConnectionFinished finishedCallback)
	{
		ServerConnectorCallback callback = delegate(string resultJSONString)
		{
			int result = 0;
			JSONObject jsonObjResult = new JSONObject(resultJSONString);
			
			Debug.Log ("json = "+resultJSONString);
			Debug.Log ("~~~result ="+result);
			
			if(null != jsonObjResult["result"])
			{
				Debug.Log ("~~~result success");
				result = System.Convert.ToInt32(jsonObjResult["result"].str);
				Debug.Log("result:" + result.ToString());
			}
			else
			{
				Debug.Log ("~~~result fail");
				result = CommonDef.errorNotFoundResult;
			}

			if(null != finishedCallback)
				finishedCallback(result);
		};
		
		Debug.Log("========== ChangeCharacter ==========");
		Debug.Log("sessionId : " + GlobalValues.sessionId);
		Debug.Log("gigcode : " + UICharactorPet.selectedCharacterCode);
		Debug.Log("========== /ChangeCharacter ==========");
		
		JSONObject objChangeCharacter = new JSONObject(JSONObject.Type.OBJECT);
		objChangeCharacter.AddField("sessionId", GlobalValues.sessionId);
		objChangeCharacter.AddField("gigcode", UICharactorPet.selectedCharacterCode);

		QueryServer(strURLChangeCharacter, objChangeCharacter.print(), callback);
	}
	
	public void ChangePet(ServerConnectionFinished finishedCallback)
	{
		ServerConnectorCallback callback = delegate(string resultJSONString)
		{
			int result = 0;
			JSONObject jsonObjResult = new JSONObject(resultJSONString);
			if(null != jsonObjResult["result"])
			{
				result = System.Convert.ToInt32(jsonObjResult["result"].str);
				Debug.Log("result:" + result.ToString());
			}
			else
			{
				result = CommonDef.errorNotFoundResult;
			}

			if(null != finishedCallback)
				finishedCallback(result);
		};
		
		Debug.Log("========== ChangePet ==========");
		Debug.Log("sessionId : " + GlobalValues.sessionId);
		Debug.Log("gigcode : " + UICharactorPet.selectedPetCode);
		Debug.Log("========== /ChangePet ==========");
		
		JSONObject objChangePet = new JSONObject(JSONObject.Type.OBJECT);
		objChangePet.AddField("sessionId", GlobalValues.sessionId);
		objChangePet.AddField("gigcode", UICharactorPet.selectedPetCode);

		QueryServer(strURLChangePet, objChangePet.print(), callback);
	}
	
	// 캐릭터, 펫, 아이템 구입.
	public void Buy(ServerConnectionFinished finishedCallback)
	{
		ServerConnectorCallback callback = delegate(string resultJSONString)
		{
			int result = 0;
			JSONObject jsonObjResult = new JSONObject(resultJSONString);
			if(null != jsonObjResult["result"])
			{
				result = System.Convert.ToInt32(jsonObjResult["result"].str);
				Debug.Log("result:" + result.ToString());
			}
			else
			{
				result = CommonDef.errorNotFoundResult;
			}

			if(null != finishedCallback)
				finishedCallback(result);
		};
		
		Debug.Log("========== Buy ==========");
		Debug.Log("sessionId : " + GlobalValues.sessionId);
//		Debug.Log("gicode : " + GlobalValues.userValues.chooseBuyGicode);
		Debug.Log("gigcode : " + GlobalValues.userValues.chooseBuyGigCode);
		Debug.Log("level : " + GlobalValues.userValues.chooseBuyLevel);
		Debug.Log("grade : " + GlobalValues.userValues.chooseBuyGrade);
		Debug.Log("========== /Buy ==========");
		
		JSONObject objBuy = new JSONObject(JSONObject.Type.OBJECT);
		objBuy.AddField("sessionId", GlobalValues.sessionId);
//		objBuy.AddField("gicode", GlobalValues.userValues.chooseBuyGicode);
		objBuy.AddField("gigcode", GlobalValues.userValues.chooseBuyGigCode);		
		objBuy.AddField("level", GlobalValues.userValues.chooseBuyLevel);		
//		objBuy.AddField("grade", GlobalValues.userValues.chooseBuyGrade);		

		QueryServer(strURLBuy, objBuy.print(), callback);
	}

	//Common connector
	protected delegate void ServerConnectorCallback(string resultJSONString);
	protected void QueryServer(string URL, string queryJSONString, ServerConnectorCallback callback = null, bool bShowLoadingScreen = false, bool bKeepShowingLoadingScreen = false)
	{
		StartCoroutine(ConnectServer(URL, queryJSONString, callback, bShowLoadingScreen, bKeepShowingLoadingScreen));
	} 

	protected IEnumerator ConnectServer(string URL, string queryJSONString, ServerConnectorCallback callback, bool bShowLoadingScreen, bool bKeepShowingLoadingScreen)
	{
        yield return null;
/*
		Hashtable headers = new Hashtable();
		//headers["Authorization"]
		headers["Host"] = GlobalValues.strHostName;
		headers["Content-Type"] = "application/json; charset=utf-8";
		byte[] data = System.Text.Encoding.UTF8.GetBytes(queryJSONString);
		Debug.Log(" *** URL ="+URL);
		
		if(bShowLoadingScreen)
			GlobalAPIs.ShowLoadingScreen(true);

		WWW www = new WWW(URL, data, headers);
		yield return www;

		Debug.Log(" *** wwwtext= "+www.text);

		if(bShowLoadingScreen && !bKeepShowingLoadingScreen)
			GlobalAPIs.ShowLoadingScreen(false);

		if(null != callback)
			callback(www.text);*/
	}
}
