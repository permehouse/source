using UnityEngine;
using System.Collections;

public class GlobalValues
{
	static public string strConfigFileName = "KOH_Config.txt";
	static public string strHostName = "www.goraeking.com";
	static public bool bNoPlayInGame = false;

	static public string strTestHostName = "test.goraeking.com";
	static public bool bUseTestServer = true;//false;
	
	
	public class UserValues		// 사용자 가 선택 등 게임 중에 하는행위. 편의를 위한 변수. 
	{
		public string chooseBtnCPI;					//캐릭터,펫,장비 중에 어떤 Tab을  선택하였는지 정보 "Pet","Charactor","Item". 
//		public string chooseBuyGicode;				//  더이상 안쓰임. 캐릭터,펫,아이템 중에서 구매하려고 선택한 특정 상품의 gicode.
		public string chooseBuyGigCode;				// 등급 값이 저장된 코드.
		
		public int chooseBuyGrade;					//등급.		0 = C , 1 = B, 2 = A, 3 = S.
		public int chooseBuyLevel;					//레벨.
		public string chooseCharactorDialogMode;	//캐릭터 선택 or 릴레이 선택 중 하나.
		public int chanceUpdated = 0;				// 0 = no update , 1= update 로 별이 max 로 변경.
		
	}
	
	private GlobalValues()
	{
	}
	
	public struct Board
	{
		public string subject;
		public string content;
		public string writeDate;
	}

	public struct GameCharactor
	{
		public string code;
		public int level;
		public string grade;		//1128
		public int price;
		public string playDate;
		public string writeDate;
	}
	
	public struct GamePet
	{	
		public string code;
		public int level;
		public string grade;		//1128
		public int price;
		public string playDate;
		public string writeDate;
	}

	public class GameUser
	{
		// 삭제함. Grade 포함된 ID 를 정식 식별자로 보고, 기존의 characterCode/petCode 으로 처리함.
//		public string characterGradeCode;				//1128
//		public string petGradeCode;
		
		//public string grade;						//yj edit//1128
		public int level;						//yj edit
		public string expressionName;			//yj edit
		public string expressionPet_1_Name;		//yj edit
		public string expressionPet_2_Name;		//yj edit
		
		public int totalEarnedFishes;			//환경설정 추가.
		public int totalInterceptFishes;		//환경설정 추가.
		public int totalLoseFishes;				//환경설정 추가.
		public int totalGameCount;				//환경설정 추가.
		
		public string name;
		public string email;
		public string characterCode;
		public string petCode;
		public int jewels;
		public int fishes;
		public int digestFishes;
		public int digestTimer;
		public System.DateTime digestUpdateTime;
		public int chance;
		public int chanceTimer;
		public System.DateTime chanceUpdateTime;
		public int bestScore;
		public int bestDistance;
		public int weekScore;
		public int weekDistance;
		public int week;
		public string imageUrl;
//		public int chanceReceive;
//		public int sound;
		public string lastLogin;
		public int loginCount;
		public string writeDate;
		public string modifyDate;
		public ArrayList gameCharacters;	//GameCharacter[]
		public ArrayList gamePets;			//GamePet[]
		
		public void SetGameUserData(JSONObject jsonObjGameUser)
		{			
			name = jsonObjGameUser["name"].str;
			email = jsonObjGameUser["email"].str;
			// 등급이 붙어 있는 것을 정식 ID 로 처리함.		
			characterCode = jsonObjGameUser["character"].str;
			petCode = jsonObjGameUser["pet"].str;
												
			// yj edit.			
			// expressionName 이 Charator 와 Pet List 로 넘어오고 사용자는 이 List 중에서 자기가 보유한 캐릭터 Pet에 대한 .
			// 고유 번호를 받아서 그 번호로 List 에서 자기것을 찾아서 하게 되어 있는데 현재는 고유 번호를 넘겨오지 않아서 구현 못함.
			// 퍼블리싱 때문에 문제가 되는 서버 구현은 하지 않고 있음. 
			// yj edit.임시.
			//grade = "";
			level = 1;
			expressionName = "";
			expressionPet_1_Name = "";
			expressionPet_2_Name = "";							
			
			Debug.Log("========== LogIn - SetGameUserData ==========");
			Debug.Log("name : " + name);
			Debug.Log("email : " + email);
			Debug.Log("character : " + characterCode);
			Debug.Log("pet : " + petCode);
			
			
			jewels = jsonObjGameUser["jewels"].n;
			fishes = jsonObjGameUser["fishes"].n;
			digestFishes = jsonObjGameUser["digestFishes"].n;
			digestTimer = jsonObjGameUser["digestTimer"].n;
			digestUpdateTime = System.DateTime.ParseExact(jsonObjGameUser["digestUpdateTime"].str, CommonDef.strDefaultDateTimeFormat, null);
			Debug.Log("digestUpdateTime, " + digestUpdateTime.ToString());
			digestUpdateTime = digestUpdateTime.AddSeconds((double)digestTimer);
			Debug.Log("digestUpdateTime added, " + digestUpdateTime.ToString());
			chance = jsonObjGameUser["chance"].n;
			chanceTimer = jsonObjGameUser["chanceTimer"].n;
			chanceUpdateTime = System.DateTime.ParseExact(jsonObjGameUser["chanceUpdateTime"].str, CommonDef.strDefaultDateTimeFormat, null);
			chanceUpdateTime = chanceUpdateTime.AddSeconds((double)chanceTimer);
			
			totalEarnedFishes = jsonObjGameUser["totalEarnedFishes"].n;			//환경설정 추가.
			totalInterceptFishes = jsonObjGameUser["totalInterceptFishes"].n;	
			totalLoseFishes = jsonObjGameUser["totalLoseFishes"].n;				
			totalGameCount = jsonObjGameUser["totalGameCount"].n;				
			
			
			Debug.Log ("totalLoseFishes= "+totalLoseFishes);
			
			bestScore = jsonObjGameUser["bestScore"].n;
			bestDistance = jsonObjGameUser["bestDistance"].n;
			weekScore = jsonObjGameUser["weekScore"].n;
			weekDistance = jsonObjGameUser["weekDistance"].n;
			week = jsonObjGameUser["week"].n;
			imageUrl = jsonObjGameUser["imageUrl"].str;

			lastLogin = jsonObjGameUser["lastLogin"].str;
			loginCount = jsonObjGameUser["loginCount"].n;
			writeDate = jsonObjGameUser["writeDate"].str;
			modifyDate = jsonObjGameUser["modifyDate"].str;

			int i = 0;
			JSONObject jsonObjCharactors = jsonObjGameUser["gameCharacters"];
			gameCharacters = new ArrayList();
			for (i = 0; i < jsonObjCharactors.count(); ++i)
			{
				GlobalValues.GameCharactor gameCharactor = new GlobalValues.GameCharactor();
				gameCharactor.code = jsonObjCharactors[i]["gigcode"].str;		//중복을 위해서는 gicode로 해야함.
				gameCharactor.level = jsonObjCharactors[i]["level"].n;				
				gameCharactor.grade = jsonObjCharactors[i]["gigcode"].str;
				
//				gameCharactor.price = jsonObjCharactors[i]["price"].n;
				gameCharactor.playDate = jsonObjCharactors[i]["playDate"].str;
				gameCharactor.writeDate = jsonObjCharactors[i]["writeDate"].str;
								
				if (characterCode == gameCharactor.code)						//수정. 중복 보유를 위해서는 사용자 고유넘버로 해야 하는데 임시로 characterCode 로 하고 있음.
				{
					expressionName = jsonObjCharactors[i]["name"].str;			
					UICharactorPet.selectedCharacterExpressionName = expressionName;						
				}				
				gameCharacters.Add(gameCharactor);				
				Debug.Log("Own Character [" + i + "] : " + gameCharactor.code);
			}
			
			JSONObject jsonObjPets = jsonObjGameUser["gamePets"];
			gamePets = new ArrayList();
			for (i = 0; i < jsonObjPets.count(); ++i)
			{
				GlobalValues.GamePet gamePet = new GlobalValues.GamePet();
				gamePet.code = jsonObjPets[i]["gigcode"].str;					//중복을 위해서는 gicode로 해야함.
				gamePet.level = jsonObjPets[i]["level"].n;
				gamePet.grade = jsonObjPets[i]["gigcode"].str;
//				gamePet.price = jsonObjPets[i]["price"].n;
				gamePet.playDate = jsonObjPets[i]["playDate"].str;
				gamePet.writeDate = jsonObjPets[i]["writeDate"].str;
				
				if (petCode == gamePet.code)									//수정. 중복 보유를 위해서는 사용자 고유넘버로 해야 하는데 임시로 petCode 로 하고 있음.
				{
					expressionPet_1_Name = jsonObjPets[i]["name"].str;			
					UICharactorPet.selectedExpressionPet_1_Name	= expressionPet_1_Name;
				}				
				gamePets.Add(gamePet);				
				Debug.Log("Own Pet [" + i + "] : " + gamePet.code);
			}
			
			UICharactorPet.selectedCharacterCode = characterCode;				//1128 why?.
			UICharactorPet.selectedPetCode = petCode;
						
			//charactorGradeCode = jsonObjGameUser["characterGrade"].str;
			//petGradeCode = jsonObjGameUser["petGrade"].str;
			
			Debug.Log("========== /LogIn - SetGameUserData ==========");
		}
		
		public void ResetGameUserData()
		{
			name = "";
			email = "";
			characterCode = "";
			petCode = "";
			jewels = 0;
			fishes = 0;
			digestFishes = 0;
			digestTimer = 0;
			chance = 0;
			chanceTimer = 0;
			totalEarnedFishes = 0;		//내기록 위해 추가된 변수도 초기화.
			totalInterceptFishes = 0;	//내기록 위해 추가된 변수도 초기화.
			totalLoseFishes = 0;		//내기록 위해 추가된 변수도 초기화.
			totalGameCount = 0;			//내기록 위해 추가된 변수도 초기화.
			bestScore = 0;			
			bestDistance = 0;
			weekScore = 0;
			weekDistance = 0;
			week = 0;
			imageUrl = "";
			lastLogin = "";
			loginCount = 0;
			writeDate = "";
			modifyDate = "";
			gameCharacters.Clear();
			gamePets.Clear();
		}
	};
	
	public struct PlayerProperty {
		public Player.PlayerType nPlayerType;
	}
	
	public struct PetProperty {
		public Pet.PetType nPetType;
	}
	
	public struct GamePlay
	{
		public int gpid;
		public int score;
		public int distance;
		public int fishes;
		public int digestFishes;
		public string startDate;
		public string endDate;
		
		public PlayerProperty player;
		public PlayerProperty playerRelay;
		public PetProperty pet;
	};
	
	public struct Settings
	{
		public int soundPlay;
		public int pushReceive;
		public int chanceReceive;
		public int dragController;
		public int lowQuality;
	};
	
	public struct GameContact
	{
		public int guid;
		public string email;
		public string name;
		public string character;
		public int weekScore;
		public int digestFishes;
		public int weekDistance;
		public int week;
		public string imageUrl;
		public int chanceReceive;
		public int pushReceive;
		public bool isDead;
	}
	
	public struct GameBoost
	{
		public int nLevel_FishUpRate;
		public int nLevel_HPUpAmount;
		public int nLevel_ItemFishUpCount;
		public int nLevel_ItemFeverUpTime;
		public int nLevel_ItemMagnetUpTime;
		public int nLevel_ItemHeartUpHP;
		public int nCount_StartRun;
		public int nCount_Escape;
		public int nCount_LastRun;
		public int nCount_Relay;
		public int nCount_FastDigest;
	}
	
	static public string sessionId = "";
	static public System.DateTime serverTime;
	static public System.TimeSpan timeGabFromServer;
	static public string EMail = "";

	static public ArrayList noticeList = new ArrayList();
	static public GameUser gameUser = new GameUser();
	static public UserValues userValues = new UserValues();
	static public GlobalValues.GamePlay gamePlay = new GlobalValues.GamePlay();
	static public Settings settings = new Settings();
	static public ArrayList friendList = new ArrayList();
	static public ArrayList friendListInGame = new ArrayList();
	static public GameBoost gameBoost = new GameBoost();
	
	static public bool loadConfigValues()
	{
		JSONObject jsonConfig = new JSONObject(GlobalAPIs.readStringFromFile(strConfigFileName));
		string hostName;

		Debug.Log("======= loadConfigValues =======");
		
		if(null != jsonConfig["hostName"])
		{
			Debug.Log("hostName:" + jsonConfig["hostName"].str);
			hostName = jsonConfig["hostName"].str;
			if("" != hostName)
			{
				GlobalValues.strHostName = hostName;
			}
		}

		//test values
		if(null != jsonConfig["eMail"])
		{
			Debug.Log("eMail:" + jsonConfig["eMail"].str);
			GlobalValues.EMail = jsonConfig["eMail"].str;
		}
		
		if(null != jsonConfig["noPlayInGame"])
		{
			Debug.Log("noPlayInGame:" + jsonConfig["noPlayInGame"].n.ToString());
			if(0 == jsonConfig["noPlayInGame"].n)
				GlobalValues.bNoPlayInGame = false;
			else
				GlobalValues.bNoPlayInGame = true;
		}
		
		Debug.Log("======= /loadConfigValues =======");
		
		// Add for using test server.
		if(GlobalValues.bUseTestServer) {
			GlobalValues.strHostName = GlobalValues.strTestHostName;
			Debug.Log(">>>>> Use Test Server : " + GlobalValues.strHostName);
		}
		
		return true;
	}

	static public bool saveConfigValues()
	{
		JSONObject jsonConfig = new JSONObject(JSONObject.Type.OBJECT);
		//TODO solmea, delete HostName in config file
		jsonConfig.AddField("hostName", GlobalValues.strHostName);
		jsonConfig.AddField("eMail", GlobalValues.EMail);
//		if(GlobalValues.bNoPlayInGame)
			jsonConfig.AddField("noPlayInGame", (int)(GlobalValues.bNoPlayInGame?1:0));

		GlobalAPIs.writeStringToFile(jsonConfig.print(), strConfigFileName);
		return true;
	}
	
	//메인화면(panelReady) 보석, 골드 사용된 결과를 갱신(Retrieve User) 해줌.
	static public void RetrieveGameUserMain()	
	{		
		ServerConnector.ServerConnectionFinished callback = delegate(int result)
		{
			if(CommonDef.errorSuccessed == result)
			{
				GameObject[] allPanel = GameObject.FindGameObjectsWithTag("UIPanel");
				foreach(GameObject panel in allPanel)
				{
					if(panel.name == "PanelReady")
					{
						panel.SendMessage("RefreshUIData");					//메인 메뉴로 돌아갈때 이름 표시를 Refresh 해주는 것임. 고쳐야 할듯함. 임시.
					}
				}
			}
			else
			{
				//Retry retrieve game user data 10 seconds later if result is not successed.
				Debug.Log("Retrieve error code:" + result.ToString());
				Debug.Log("Set chanceUpdateTime, " + GlobalValues.gameUser.chanceUpdateTime.ToString());
				Debug.Log("Set digestUpdateTime, " + GlobalValues.gameUser.digestUpdateTime.ToString());
			}
		};
		Debug.Log("Try retrieve game user data");
		ServerConnector connector = ServerConnector.GetInstance();
		if(null != connector)
		{	
			connector.RetrieveGameUser(callback);
		}
	}
	
	static public void RetrieveGameUserForRefreshStats()	
	{		
		Debug.Log ("arrave");
		ServerConnector.ServerConnectionFinished callback = delegate(int result)
		{
			
			Debug.Log ("arrave2");
			if(CommonDef.errorSuccessed == result)
			{
				Debug.Log ("arrave3");
				GameObject[] allPanel = GameObject.FindGameObjectsWithTag("UIPanel");
				
				foreach(GameObject panel in allPanel)
				{
					Debug.Log ("arrave4 "+panel.name);
					if(panel.name == "PanelReady")
					{
						//panel.SendMessage("RefreshUIData");					//메인 메뉴로 돌아갈때 이름 표시를 Refresh 해주는 것임. 고쳐야 할듯함. 임시.
					
						panel.SendMessage("RunPopupConfig");
						Debug.Log ("arrave5");
						//GetComponent<_14_HandlerPanelCheckPoint>().SetCurrentCheckPointTitle();
						//panel.GetComponent<HandlerPopupConfig>().gameObject.SetActive(true);
						//panel.GetComponent<HandlerPopupConfig>().ApplyGlobalValues();
						
					}
				}
			}
			else
			{
				//Retry retrieve game user data 10 seconds later if result is not successed.
				Debug.Log("Retrieve error code:" + result.ToString());
				Debug.Log("Set chanceUpdateTime, " + GlobalValues.gameUser.chanceUpdateTime.ToString());
				Debug.Log("Set digestUpdateTime, " + GlobalValues.gameUser.digestUpdateTime.ToString());
			}
		};
		Debug.Log("Try retrieve game user data");
		ServerConnector connector = ServerConnector.GetInstance();
		if(null != connector)
		{	
			Debug.Log ("connector call");
			connector.RetrieveGameUser(callback);
		}
	}
}
