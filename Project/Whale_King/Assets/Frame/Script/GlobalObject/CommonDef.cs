using UnityEngine;
using System.Collections;

public class CommonDef
{
	public enum MESSAGE_BOX_TYPE
	{
		MB_TYPE_OK = 0,
		MB_TYPE_YESNO,
	};
	
	public enum MESSAGE_BOX_RESULT
	{
		MB_OK = 0,
		MB_YES,
		MB_NO,
	};

	public struct MessageBoxParam
	{
		public string strMessage;
		public MESSAGE_BOX_TYPE mbType;
		public MessageBox.MessageBoxCallback callback;
	};

	static public string strDefaultDateTimeFormat = "yyMMddHHmmss";

	//ErrorCodes
	static public int errorNotFoundResult = -999;
	static public int errorSuccessed = 0;
	static public int errorRegisterUserOK = 1;
	static public int errorFailNormal = 10;
	static public int errorInvalidLoginInfo = 11;
	static public int errorLogout = 21;
	static public int errorExpiredSession = 99;

	static public int maxChanceCount = 5;
}
