using UnityEngine;
using System.Collections;

public class MessageBox
{
	private MessageBox() { }
	
	public delegate void MessageBoxCallback(CommonDef.MESSAGE_BOX_RESULT result);

	static public void ShowMessageBox(string strMessage, CommonDef.MESSAGE_BOX_TYPE mbType, MessageBoxCallback callback)
	{
		GameObject[] allPanel = GameObject.FindGameObjectsWithTag("GUICamera");
		foreach(GameObject panel in allPanel)
		{
			if(panel.name == "GUICamera")
			{
				CommonDef.MessageBoxParam param = new CommonDef.MessageBoxParam();
				param.strMessage = strMessage;
				param.mbType = mbType;
				param.callback = callback;

				panel.SendMessage("ShowMessageBox", param);
				break;
			}
		}
	}
}
