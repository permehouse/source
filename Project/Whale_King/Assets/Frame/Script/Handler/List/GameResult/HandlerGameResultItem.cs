using UnityEngine;
using System.Collections;

public class HandlerGameResultItem : MonoBehaviour {
	
	public GameObject prefabFriendFishList;
	private int indexBtnCheckPointLevel;
	
	// Use this for initialization
	void Start () {
	
	}
		
	// prefab Instance 에 필요한 정보 넣기.
	public void SetItemInfo(int index,string name,int digestFishes,string imgUrl)
	{
		indexBtnCheckPointLevel = index;
		
		UILabel[] labels = GetComponentsInChildren<UILabel>();
		foreach(UILabel label in labels)
		{
			if(label.name == "lblFriendName")
			{
				label.text = name;
			}
			else if(label.name == "lblFriendFish")
			{
				label.text = digestFishes.ToString();
			}
			
		}
		
		UITexture[] textureURL =     GetComponentsInChildren<UITexture>();		
		textureURL[0].GetComponent<loadURLImage>().DrawChart(imgUrl);	
		
	}
}
