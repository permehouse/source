using UnityEngine;
using System.Collections;

public class HandlerGameResultGrid : MonoBehaviour {
	
	public GameObject prefabFriendFishList;
	
	// Use this for initialization
	void Start () {
		InitInstanceFriendFishList();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	protected void InitInstanceFriendFishList()
	{	
		float friendBoxShiftX;
		float friendBoxSizeX = 100f;
		float startX = -230f;
		float startY = -220f;
		
		Debug.Log ("count = "+GlobalValues.friendListInGame.Count);
		
		
		for (int i = 0; i < GlobalValues.friendListInGame.Count; i++)
		{
			
			//뺏은 친구 물고기 리스트 채워주기.
			GlobalValues.GameContact contact = (GlobalValues.GameContact)GlobalValues.friendListInGame[i];			
			
			if (contact.isDead == true)
			{
				Debug.Log ("gogo");
				GameObject obj = Instantiate(prefabFriendFishList, new Vector3(0.0f, 0.0f, -0.043f), Quaternion.identity) as GameObject;
				obj.transform.parent = this.transform;
				obj.transform.localScale = new Vector3(1f, 1f, 1f);			
				friendBoxShiftX = i * friendBoxSizeX;							
				obj.transform.Translate(new Vector3( (startX+friendBoxShiftX)/180f,(startY)/180f,0f));			
				
				
				HandlerGameResultItem itemHandler = obj.GetComponentInChildren<HandlerGameResultItem>();
				
				Debug.Log ("gogo1"+contact.name);
				Debug.Log ("gogo2"+contact.digestFishes);
				
				itemHandler.SetItemInfo(i,contact.name,contact.digestFishes,contact.imageUrl);
			}
			
		}
		
		
		
		
		
	
	}
}




