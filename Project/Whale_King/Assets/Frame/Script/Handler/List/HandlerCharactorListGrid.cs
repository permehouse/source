using UnityEngine;
using System.Collections;

public class HandlerCharactorListGrid : MonoBehaviour
{
	public GameObject prefabCharactorListItem;

	public HandlerCharactorListGrid()
	{
	}

	void Start ()
	{
		InitCharactorList();
	}
	
	void Update ()
	{
	
	}

	protected void InitCharactorList()
	{
		Debug.Log("InitCharactorList(), " + UICharactorPet.charactorInfoList.Count.ToString());
		for (int i = 0; i < UICharactorPet.charactorInfoList.Count; ++i)
		{
			//TODO solmea - remove comment if you show salable charactor only
//			UICharactorPet.CharactorInfo charactor = (UICharactorPet.CharactorInfo)UICharactorPet.charactorInfoList[i];
//			if(CHARACTOR_TYPE.CHARACTOR_DEFAULT != charactor.charactorType && CHARACTOR_TYPE.CHARACTOR_SALABLE != charactor.charactorType)
//				continue;

			GameObject obj = Instantiate(prefabCharactorListItem, new Vector3(0.0f, 0.0f, -0.043f), Quaternion.identity) as GameObject;

			obj.transform.parent = this.transform;
			obj.transform.localScale = new Vector3(1f, 1f, 1f);
			HandlerCharactorListItem itemHandler = obj.GetComponentInChildren<HandlerCharactorListItem>();
			itemHandler.SetItemInfo(i);
		}

		GetComponent<UIGrid>().Reposition();
	}

	protected void OnSelectedItem()
	{
		for(int i = 0;i<this.transform.childCount;++i)
		{
			this.transform.GetChild(i).gameObject.SendMessage("OnSelectedItem");
		}
	}
	
	protected void OnSelectedRelay()
	{
		for(int i = 0;i<this.transform.childCount;++i)
		{
			this.transform.GetChild(i).gameObject.SendMessage("OnSelectedRelay");
		}
	}
}
