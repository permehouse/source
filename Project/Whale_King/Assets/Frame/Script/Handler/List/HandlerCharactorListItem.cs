using UnityEngine;
using System.Collections;

public class HandlerCharactorListItem : MonoBehaviour
{
	public UISprite bgDefault;
	public UISprite bgSelected;
	public UISprite bgOwned;
	public UIImageButton btnChoose;
	public UIImageButton btnBuy;
	public UIImageButton btnSelected;
	public UIImageButton btnLevelUp;
	public UISprite imgRelay;

	private string m_strCharacterCode;
	private string m_strCharacterExpressionName;		//yj edit.
	private bool m_bOwned;
	private bool m_bSelected;
	private bool m_bRelay;

	public HandlerCharactorListItem()
	{
		InitData();
	}
	
	void Start ()
	{
	
	}

	void Update ()
	{
	
	}
	
	private void InitData()
	{
		m_strCharacterCode = "";
		m_strCharacterExpressionName = ""; //yj edit.
		m_bOwned = false;
		m_bSelected = false;
		m_bRelay = false;
	}
	
	public void OnClickChoose()
	{		
		UICharactorPet.CharactorInfo charInfo = UICharactorPet.GetCharacterInfoFromCode(m_strCharacterCode);
		Debug.Log ("priceFish= " + charInfo.priceFish);
		Debug.Log ("priceJewel= " + charInfo.priceJewel);
		
		if (GlobalValues.userValues.chooseCharactorDialogMode == "Charactor")
				OnClickChoose_Charactor();
		else if(GlobalValues.userValues.chooseCharactorDialogMode == "Relay")
				OnClickChoose_Relay();		
	}
	
	void OnClickChoose_Charactor()
	{
		//HandlerRankListItem itemHandler = this.transform.parent.GetComponentInChildren<HandlerRankListItem>();		
		//m_bOwned	중요. 		
		//선택한 캐릭터를 보유하고 있는지 검사.
		if(m_bOwned) {
/*		GlobalValues.GameCharactor gameCharactor = new GlobalValues.GameCharactor();								
		for(int i=0;i<GlobalValues.gameUser.gameCharacters.Count;i++)
		{			
			gameCharactor = (GlobalValues.GameCharactor)GlobalValues.gameUser.gameCharacters[i];
			
			Debug.Log ("gameCharactor.code= "+gameCharactor.code);
			Debug.Log ("m_strCharacterCode= "+m_strCharacterCode);
			Debug.Log ("m_strCharacterCode= "+m_strCharacterCode);
			
			// 보유한 경우 : GlobalValues.gameUser.gameCharacters List 에 선택한 캐릭터가 하나라도 있다면 해당 캐릭터를 보유하고 있는 것으로 보며 선택하게 된다.
			if (gameCharactor.code == m_strCharacterCode)
			{
*/								
				UICharactorPet.selectedCharacterCode = m_strCharacterCode;		
				UICharactorPet.selectedCharacterExpressionName = m_strCharacterExpressionName;		
				this.transform.parent.gameObject.SendMessage("OnSelectedItem");		
				//서버에 변경 사항 등록 한 후에 메인화면 Update.
				SaveCharactorToServer();				
//				return;
//			}			
		}
		else {
			Debug.Log ("m_strCharacterCode"+m_strCharacterCode);
			//Debug.Log ("UICharactorPet.selectedCharacterCode"+UICharactorPet.selectedCharacterCode);
			// 캐릭터를 보유하고 있지 않을 경우. //구매 시도 한다.
			MessageBox.MessageBoxCallback callbackWillYouBuy = delegate(CommonDef.MESSAGE_BOX_RESULT result)
			{
				if(result == CommonDef.MESSAGE_BOX_RESULT.MB_YES)
				{				
					// 캐릭터 구매 시도.
					buyChooseMainCharactor();				
				}		
			};
			MessageBox.ShowMessageBox(GlobalStrings.willYouBuy, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_YESNO, callbackWillYouBuy);	
		}
	}
	
	//캐릭터 구매 시도.
	void buyChooseMainCharactor()
	{		
		// 구입 시도.
		ServerConnector connector = ServerConnector.GetInstance();
		ServerConnector.ServerConnectionFinished callbackBuyCharactor = delegate(int result)
		{
			// 구입 성공.
			if(CommonDef.errorSuccessed == result) {															
				//캐릭터 선택.				
				m_bOwned = true;
				UICharactorPet.selectedCharacterCode = m_strCharacterCode;					
				UICharactorPet.selectedCharacterExpressionName = m_strCharacterExpressionName;		
				this.transform.parent.gameObject.SendMessage("OnSelectedItem");	
				//서버에 변경 사항 등록 한 후에 메인화면 Update.
				SaveCharactorToServer();
				MessageBox.ShowMessageBox(GlobalStrings.successBuy, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
				
				Debug.Log("buy success"); 
			}
			else // 구입 실패.
			{			
				switch (result)
				{
					case 41:
						MessageBox.ShowMessageBox(GlobalStrings.notEnoughGold, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
						break;
					case 42:
						MessageBox.ShowMessageBox(GlobalStrings.alreadyBuy, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
						break;
					case 43:
						MessageBox.ShowMessageBox(GlobalStrings.forbidBuy, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
						break;
					case 44:
						MessageBox.ShowMessageBox(GlobalStrings.notEnoughDia, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
						break;
				}			
				Debug.Log ("buy fail= "+result);
			}		
		};
		if(null != connector) {
			Debug.Log ("BBBB m_strCharacterCode"+m_strCharacterCode);
			Debug.Log ("BBBB m_strCharacterCode"+m_strCharacterCode);
			Debug.Log ("BBBB m_strCharacterCode"+m_strCharacterCode);
			string tempCode ="";
			
			for (int i=0;i < m_strCharacterCode.Length-2 ;i++)				//1128
				tempCode += m_strCharacterCode.Substring(i,1);
			
			
			
//			GlobalValues.userValues.chooseBuyGicode = tempCode;
			GlobalValues.userValues.chooseBuyGigCode = m_strCharacterCode;
			//GlobalValues.userValues.chooseBuyGrade  = 0;							
			GlobalValues.userValues.chooseBuyLevel = 0;
			connector.Buy(callbackBuyCharactor);
		}		
	}
	
	//릴레이 선택에서 캐릭터 선택한 경우.
	void OnClickChoose_Relay()
	{
		//HandlerRankListItem itemHandler = this.transform.parent.GetComponentInChildren<HandlerRankListItem>();
		Debug.Log("OnClickChoose Relay, ID:" + m_strCharacterCode);
		//m_bOwned	중요. 		
		//선택한 캐릭터를 보유하고 있는지 검사.
		if(m_bOwned) {
/*		GlobalValues.GameCharactor gameCharactor = new GlobalValues.GameCharactor();								
		for(int i=0;i<GlobalValues.gameUser.gameCharacters.Count;i++)
		{
			gameCharactor = (GlobalValues.GameCharactor)GlobalValues.gameUser.gameCharacters[i];
			if (gameCharactor.code == m_strCharacterCode)
			{
*/									
				UICharactorPet.selectedRelayCharacterCode = m_strCharacterCode;
				this.transform.parent.gameObject.SendMessage("OnSelectedRelay");	
//				return;				
//			}
		}
		else {
			// 캐릭터를 보유하고 있지 않을 경우 구입시도.
			MessageBox.MessageBoxCallback callbackWillYouBuy = delegate(CommonDef.MESSAGE_BOX_RESULT result)
			{
				if(result == CommonDef.MESSAGE_BOX_RESULT.MB_YES)
				{				
					// 캐릭터 구매 시도.
					buyChooseRelayCharactor();												
				}		
			};
			MessageBox.ShowMessageBox(GlobalStrings.willYouBuy, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_YESNO, callbackWillYouBuy);	
		}
	}
		
	//릴레이 모드에서 캐릭터 구매 시도.
	void buyChooseRelayCharactor()
	{		
		// 구입 시도.
		ServerConnector connector = ServerConnector.GetInstance();
		ServerConnector.ServerConnectionFinished callbackBuyCharactor = delegate(int result)
		{
			// 구입 성공.
			if(CommonDef.errorSuccessed == result) {															
				//구입했음을 서버에서 받아서봐야함.
				GlobalValues.RetrieveGameUserMain();
				m_bOwned = true;
				//릴레이 지정.				
				UICharactorPet.selectedRelayCharacterCode = m_strCharacterCode;					
				this.transform.parent.gameObject.SendMessage("OnSelectedRelay");	
//				RelayCharactorConfirm(m_strCharacterCode);
				MessageBox.ShowMessageBox(GlobalStrings.successBuy, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
			}
			else // 구입 실패.
			{			
				switch (result)
				{
					case 41:
						MessageBox.ShowMessageBox(GlobalStrings.notEnoughGold, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
						break;
					case 42:
						MessageBox.ShowMessageBox(GlobalStrings.alreadyBuy, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
						break;
					case 43:
						MessageBox.ShowMessageBox(GlobalStrings.forbidBuy, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
						break;
					case 44:
						MessageBox.ShowMessageBox(GlobalStrings.notEnoughDia, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
						break;
				}				
			}		
		};
		if(null != connector) {
			GlobalValues.userValues.chooseBuyGigCode = m_strCharacterCode;
			GlobalValues.userValues.chooseBuyGrade  = 0;							
			GlobalValues.userValues.chooseBuyLevel = 0;
			connector.Buy(callbackBuyCharactor);
		}		
	}
	
	// 선택한 캐릭터로 릴레이 캐릭터를 지정하는 함수.
/*	bool RelayCharactorConfirm(string gameCharactor_code)
	{
		Debug.Log("!!!input gameCharactor_code code = "+gameCharactor_code);
		if (gameCharactor_code == m_strCharacterCode)		// 수정 보완 필요.
		{			
			//릴레이 지정.
			switch(m_strCharacterCode)
			{
			case "CH_DEFAULT_C":
				GlobalValues.gamePlay.playerRelay.nPlayerType = Player.PlayerType.whale;	
				break;
			case "CH_POLAR_B":
				GlobalValues.gamePlay.playerRelay.nPlayerType = Player.PlayerType.polarbear;	
				break;				
			case "CH_PORORO_A":
				GlobalValues.gamePlay.playerRelay.nPlayerType = Player.PlayerType.pororo;	
				break;
			case "CH_MOBYDICK_B":
				GlobalValues.gamePlay.playerRelay.nPlayerType = Player.PlayerType.mobydick;	
				break;
			case "CH_TURTLE_B":
				GlobalValues.gamePlay.playerRelay.nPlayerType = Player.PlayerType.turtle;	
				break;
			case "CH_OCTOPUS_B":
				GlobalValues.gamePlay.playerRelay.nPlayerType = Player.PlayerType.octopus;	
				break;
			default:
				//에러. 릴레이 캐릭터 못 찾을때 임시로 고래.
				GlobalValues.gamePlay.playerRelay.nPlayerType = Player.PlayerType.whale;	
				break;				
			}
			Debug.Log("!!!charactor code = "+m_strCharacterCode);	
			Debug.Log("!!!Relay = "+GlobalValues.gamePlay.playerRelay.nPlayerType);
		
			return true;			
		}
		
		return false;
	}	
*/		 
		
	void SaveCharactorToServer()
	{
		Debug.Log("Save selected charactor to server");		
				
		// Save selected character to Server
		ServerConnector.ServerConnectionFinished callbackChangeCharacter = delegate(int result)
		{
			if(CommonDef.errorSuccessed == result) {
				GlobalValues.gameUser.characterCode = UICharactorPet.selectedCharacterCode;	
				
				//메인화면(panelReady)에서 보석, 골드 사용된 결과를 갱신(Retrieve User) 해줌.여기에서 해줘야지 메인 화면으로 변경시에 Delay 없음.				
				GlobalValues.RetrieveGameUserMain();		
			}
			else {
				MessageBox.ShowMessageBox("Error:Chr Store setting errorCode:" + result.ToString(), CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
			}		
			//MessageBox.ShowMessageBox(GlobalValues.gameUser.characterCode+"c code" + result.ToString(), CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
		};
		ServerConnector connector = ServerConnector.GetInstance();
		if(null != connector) {
			connector.ChangeCharacter(callbackChangeCharacter);
		}		
	}
	
	public void OnClickLevelUp()
	{
//		HandlerRankListItem itemHandler = this.transform.parent.GetComponentInChildren<HandlerRankListItem>();
		Debug.Log("OnClickLevelUp, ID:" + m_strCharacterCode);
//		GlobalValues.settings.selectedCharacterCode = m_strCharacterCode;
//		this.transform.parent.parent.gameObject.SendMessage("OnSelectedItem");
	}

	public void SetItemInfo(int iCharactorIndex)
	{
		//For test
		if(iCharactorIndex < 0 || UICharactorPet.charactorInfoList.Count <= iCharactorIndex)
			return;

		UICharactorPet.CharactorInfo charactor = (UICharactorPet.CharactorInfo)UICharactorPet.charactorInfoList[iCharactorIndex];
		
		
		//Debug.Log("*** charactor.CharacterCode"+charactor.CharacterCode);
		
		m_strCharacterCode = charactor.CharacterCode;
		m_strCharacterExpressionName = charactor.charactorExpressionName;		// yj edit.
		
		m_bOwned = CheckOwnedChar(m_strCharacterCode);
		m_bSelected = (bool)(m_strCharacterCode == UICharactorPet.selectedCharacterCode);
		
		SetBGState();
		SetButtonState();

		UILabel[] labelChildren = GetComponentsInChildren<UILabel>();
		foreach(UILabel labelItem in labelChildren)
		{
			if(labelItem.name == "lblName")
			{
				labelItem.text = charactor.charactorExpressionName;
			}
			else if(labelItem.name == "lblLevel")
			{
				labelItem.text = "1";		//수정. 차후에 레벨 추가.
			}
			else if(labelItem.name == "lblDescription")
			{
				labelItem.text = charactor.charactorDescription;
			}
			else if(labelItem.name == "lblPrice") {
				labelItem.text = charactor.priceFish.ToString();
			}
		}
		
		UISprite[] spriteChildren = GetComponentsInChildren<UISprite>();
		foreach(UISprite spriteItem in spriteChildren)
		{
			if(spriteItem.name == "imgGrade")
			{
				spriteItem.gameObject.SetActive(charactor.grade != 'N');
				spriteItem.spriteName = "PopupCharactorListItemGrade" + charactor.grade;
				spriteItem.MakePixelPerfect();
			}
		}
		
		
		//캐릭터 표시.
		UISpriteAnimation[] spriteAniChildren = GetComponentsInChildren<UISpriteAnimation>();
		foreach(UISpriteAnimation spriteAniItem in spriteAniChildren)
		{
			if(spriteAniItem.name == "imgCharactor")
			{
				spriteAniItem.framesPerSecond = charactor.animationFramesPerSecond;
				spriteAniItem.namePrefix = charactor.animationNamePrefix;
			}
		}
		
		
		UISlider[] sliderChildren = GetComponentsInChildren<UISlider>();
		foreach(UISlider sliderItem in sliderChildren)
		{
			if(sliderItem.name == "progLevel")
			{
				sliderItem.sliderValue = 0.127f;
			}
		}
		
		SetImageRelay();
	}
	
	protected void SetBGState()
	{
		bgSelected.gameObject.SetActive(false);
		bgOwned.gameObject.SetActive(false);
		
		if(m_bSelected) {
			bgSelected.gameObject.SetActive(true);
		}
		else {
			bgDefault.gameObject.SetActive(true);
			if(m_bOwned) {
				bgOwned.gameObject.SetActive(true);
			}
		}
	}
	
	protected void SetButtonState()
	{
		btnSelected.gameObject.SetActive(false);
		btnChoose.gameObject.SetActive(false);
		btnBuy.gameObject.SetActive(false);
		btnLevelUp.gameObject.SetActive(false);

		if(m_bSelected) {
			btnSelected.gameObject.SetActive(true);
		}
		else {
			if(m_bOwned) {
				btnChoose.gameObject.SetActive(true);
			}
			else {
				btnBuy.gameObject.SetActive(true);
			}
			//btnLevelUp.gameObject.SetActive(!m_bSelected && m_bOwned);
		}
	}

	protected void OnSelectedItem()
	{
		m_bSelected = (m_strCharacterCode == UICharactorPet.selectedCharacterCode);
		SetBGState();
		SetButtonState();
	}

	protected void OnSelectedRelay()
	{
		m_bRelay = (m_strCharacterCode == UICharactorPet.selectedRelayCharacterCode);
		SetImageRelay();
	}
	
	protected void SetImageRelay()
	{
		imgRelay.gameObject.SetActive(m_bRelay);
	}
	
	protected bool CheckOwnedChar(string strCharCode) 
	{
		//선택한 캐릭터를 보유하고 있는지 검사.
		GlobalValues.GameCharactor gameCharactor = new GlobalValues.GameCharactor();								
		for(int i=0;i<GlobalValues.gameUser.gameCharacters.Count;i++)
		{			
			gameCharactor = (GlobalValues.GameCharactor)GlobalValues.gameUser.gameCharacters[i];
			
			// 보유한 경우 : GlobalValues.gameUser.gameCharacters List 에 선택한 캐릭터가 하나라도 있다면 해당 캐릭터를 보유하고 있는 것으로 보며 선택하게 된다.
			if (gameCharactor.code == strCharCode)
			{					
				return true;
			}			
		}			
		
		return false;
	}
}
