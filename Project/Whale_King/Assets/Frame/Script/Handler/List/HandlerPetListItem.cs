using UnityEngine;
using System.Collections;

public class HandlerPetListItem : MonoBehaviour
{
	public UISprite bgDefault;
	public UISprite bgSelected;
	public UISprite bgOwned;
	public UIImageButton btnChoose;
	public UIImageButton btnBuy;
	public UIImageButton btnSelected;
	public UIImageButton btnLevelUp;

	private string m_strPetCode;
	private string m_strExpressionPet_1_Name;		//yj edit.

	private bool m_bOwned;
	private bool m_bSelected;

	public HandlerPetListItem()
	{
		InitData();
	}
	
	void Start ()
	{
	
	}

	void Update ()
	{
	
	}
	
	private void InitData()
	{
		m_strPetCode = "";
		m_strExpressionPet_1_Name = "";
		m_bOwned = false;
		m_bSelected = false;
	}
	
	public void OnClickChoose()
	{
//		HandlerRankListItem itemHandler = this.transform.parent.GetComponentInChildren<HandlerRankListItem>();		
		Debug.Log("OnClickChoose, ID:" + m_strPetCode);
		
		//선택한 펫을 보유하고 있는지 검사.
		if(m_bOwned) {
/*		GlobalValues.GamePet gamePet = new GlobalValues.GamePet();				
		for(int i=0;i<GlobalValues.gameUser.gamePets.Count;i++)
		{
			gamePet = (GlobalValues.GamePet)GlobalValues.gameUser.gamePets[i];
			// 해당 펫을 보유한 경우. 
			// GlobalValues.gameUser.gamePets List 에 선택한 펫 하나라도 있다면 해당 펫를 보유하고 있는 것으로 보며 선택하게 된다.
			if (gamePet.code == m_strPetCode)
			{
*/							
				UICharactorPet.selectedPetCode = m_strPetCode;
				UICharactorPet.selectedExpressionPet_1_Name = m_strExpressionPet_1_Name;
				this.transform.parent.gameObject.SendMessage("OnSelectedItem");				
				//서버에 변경 사항 등록 한 후에 메인화면 Update.
				SavePetToServer();
//				return;
//			}			
		}			
		else {
			// 펫을 보유하고 있지 않을 경우. //구매 시도 한다.
			MessageBox.MessageBoxCallback callbackWillYouBuy = delegate(CommonDef.MESSAGE_BOX_RESULT result)
			{
				if(result == CommonDef.MESSAGE_BOX_RESULT.MB_YES)
				{				
					// 펫 구매 시도.
					buyChoosePet();				
				}		
			};
			MessageBox.ShowMessageBox(GlobalStrings.willYouBuy, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_YESNO, callbackWillYouBuy);				
		}
	}
	
	void buyChoosePet()
	{
		ServerConnector connector = ServerConnector.GetInstance();
		ServerConnector.ServerConnectionFinished callbackBuyPet = delegate(int result)
		{
			// 구입 성공.
			if(CommonDef.errorSuccessed == result) {																			
				//펫 선택.
				m_bOwned = true;
				UICharactorPet.selectedPetCode = m_strPetCode;
				UICharactorPet.selectedExpressionPet_1_Name = m_strExpressionPet_1_Name;
				this.transform.parent.gameObject.SendMessage("OnSelectedItem");	
				//서버에 변경 사항 등록 한 후에 메인화면 Update.
				SavePetToServer();
				MessageBox.ShowMessageBox(GlobalStrings.successBuy, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
			}
			else // 구입 실패.
			{
				switch (result)
				{
					case 41:
						MessageBox.ShowMessageBox(GlobalStrings.notEnoughGold, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
						break;
					case 42:
						MessageBox.ShowMessageBox(GlobalStrings.alreadyBuy, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
						break;
					case 43:
						MessageBox.ShowMessageBox(GlobalStrings.forbidBuy, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
						break;
					case 44:
						MessageBox.ShowMessageBox(GlobalStrings.notEnoughDia, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
						break;
				}	
			}		
		};
		if(null != connector) {
//			GlobalValues.userValues.chooseBuyGicode = m_strPetCode;
			GlobalValues.userValues.chooseBuyGigCode = m_strPetCode;
			GlobalValues.userValues.chooseBuyGrade  = 0;							
			GlobalValues.userValues.chooseBuyLevel = 0;
			
			connector.Buy(callbackBuyPet);
		}			
	}
	
	void SavePetToServer()
	{
		Debug.Log("Save selected Pet to server");		
				
		// Save selectd pet to Server
		ServerConnector.ServerConnectionFinished callbackChangePet = delegate(int result)
		{
			if(CommonDef.errorSuccessed == result) {
				GlobalValues.gameUser.petCode = UICharactorPet.selectedPetCode;		//yj 선택한 펫 정보 넣어줌. 레벨,등급,이름까지 넣어줘야 할듯.
			}
			else {
				MessageBox.ShowMessageBox("Error:Pet Store setting errorCode:" + result.ToString(), CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
			}		
		};
		ServerConnector connector = ServerConnector.GetInstance();
		if(null != connector) {
			connector.ChangePet(callbackChangePet);
		}
		
		//메인화면(panelReady)에서 보석, 골드 사용된 결과를 갱신(Retrieve User) 해줌.				
		GlobalValues.RetrieveGameUserMain();		
	}
	
	public void OnClickLevelUp()
	{
//		HandlerRankListItem itemHandler = this.transform.parent.GetComponentInChildren<HandlerRankListItem>();
		Debug.Log("OnClickLevelUp, ID:" + m_strPetCode);
//		GlobalValues.settings.selectedPetCode = m_strPetID;
//		this.transform.parent.parent.gameObject.SendMessage("OnSelectedItem");
	}

	public void SetItemInfo(int iPetIndex)
	{
		//For test
		if(iPetIndex < 0 || UICharactorPet.petInfoList.Count <= iPetIndex)
			return;

		UICharactorPet.PetInfo pet = (UICharactorPet.PetInfo)UICharactorPet.petInfoList[iPetIndex];

		m_strPetCode = pet.petCode;
		m_strExpressionPet_1_Name = pet.petExpressionName;
		
		m_bOwned = CheckOwnedPet(m_strPetCode);
		m_bSelected = (bool)(m_strPetCode == UICharactorPet.selectedPetCode);

		SetButtonState();
		SetBGState();

		UILabel[] labelChildren = GetComponentsInChildren<UILabel>();
		foreach(UILabel labelItem in labelChildren)
		{
			if(labelItem.name == "lblName")
			{
				labelItem.text = pet.petExpressionName;
			}
			else if(labelItem.name == "lblLevel")
			{
				labelItem.text = "1";
			}
			else if(labelItem.name == "lblDescription")
			{
				labelItem.text = pet.petDescription;
			}
			else if(labelItem.name == "lblPrice") {
				labelItem.text = pet.price.ToString();
			}
		}
		
		UISprite[] spriteChildren = GetComponentsInChildren<UISprite>();
		foreach(UISprite spriteItem in spriteChildren)
		{
			if(spriteItem.name == "imgGrade")
			{
				spriteItem.gameObject.SetActive(pet.grade != 'N');
				spriteItem.spriteName = "PopupCharactorListItemGrade" + pet.grade;
				spriteItem.MakePixelPerfect();
			}
		}

		UISpriteAnimation[] spriteAniChildren = GetComponentsInChildren<UISpriteAnimation>();
		foreach(UISpriteAnimation spriteAniItem in spriteAniChildren)
		{
			if(spriteAniItem.name == "imgPet")
			{
				spriteAniItem.framesPerSecond = pet.animationFramesPerSecond;
				spriteAniItem.namePrefix = pet.animationNamePrefix;
				if(0 == pet.animationFramesPerSecond)
				{
					UISprite sprite = spriteAniItem.GetComponent<UISprite>();
					sprite.spriteName = pet.animationNamePrefix;
					sprite.MakePixelPerfect();
				}
			}
		}

		UISlider[] sliderChildren = GetComponentsInChildren<UISlider>();
		foreach(UISlider sliderItem in sliderChildren)
		{
			if(sliderItem.name == "progLevel")
			{
				sliderItem.sliderValue = 0.127f;		//차후 수정.
			}
		}
	}
	
	protected void SetBGState()
	{
		bgSelected.gameObject.SetActive(false);
		bgOwned.gameObject.SetActive(false);
		
		if(m_bSelected) {
			bgSelected.gameObject.SetActive(true);
		}
		else {
			bgDefault.gameObject.SetActive(true);
			if(m_bOwned) {
				bgOwned.gameObject.SetActive(true);
			}
		}
	}
	
	protected void SetButtonState()
	{
		btnSelected.gameObject.SetActive(false);
		btnChoose.gameObject.SetActive(false);
		btnBuy.gameObject.SetActive(false);
		btnLevelUp.gameObject.SetActive(false);

		if(m_bSelected) {
			btnSelected.gameObject.SetActive(true);
		}
		else {
			if(m_bOwned) {
				btnChoose.gameObject.SetActive(true);
			}
			else {
				btnBuy.gameObject.SetActive(true);
			}
			//btnLevelUp.gameObject.SetActive(!m_bSelected && m_bOwned);
		}
	}

	protected void OnSelectedItem()
	{
		m_bSelected = (m_strPetCode == UICharactorPet.selectedPetCode);
		SetBGState();
		SetButtonState();
	}

	protected bool CheckOwnedPet(string strPetCode) 
	{
		GlobalValues.GamePet gamePet = new GlobalValues.GamePet();				
		for(int i=0;i<GlobalValues.gameUser.gamePets.Count;i++)
		{
			gamePet = (GlobalValues.GamePet)GlobalValues.gameUser.gamePets[i];
			// 해당 펫을 보유한 경우. 
			// GlobalValues.gameUser.gamePets List 에 선택한 펫 하나라도 있다면 해당 펫를 보유하고 있는 것으로 보며 선택하게 된다.
			if (gamePet.code == strPetCode)
			{
				return true;
			}
		}
		
		return false;
	}
}
