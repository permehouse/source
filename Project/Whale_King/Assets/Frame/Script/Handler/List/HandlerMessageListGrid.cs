using UnityEngine;
using System.Collections;

public class HandlerMessageListGrid : MonoBehaviour
{
	public GameObject prefabMessageListItem;

	public HandlerMessageListGrid()
	{
	}

	void Start ()
	{
		InitItem();
	}

	void Update () {
	
	}

	protected void InitItem()
	{
//		for (int i = 0; i < messageList.count(); ++i)
		for (int i = 0; i < 20; ++i)
		{
//			userInfo.name = messageList[i]["name"].str;
//			userInfo.level = System.Convert.ToUInt16(messageList[i]["level"].n);
//			userInfo.score = (uint)System.Convert.ToUInt64(messageList[i]["totalScore"].n);
//			userInfo.imageURL = strDomainName + messageList[i]["imageUrl"].str;
//			userInfo.canPresentLife = true;

//			GlobalValues.friendList.Add(userInfo);

			GameObject obj = Instantiate(prefabMessageListItem, new Vector3(0.0f, 0.0f, -0.004f), Quaternion.identity) as GameObject;

			obj.transform.parent = this.transform;
			obj.transform.localScale = new Vector3(1f, 1f, 1f);
			HandlerMessageListItem itemHandler = obj.GetComponentInChildren<HandlerMessageListItem>();

			itemHandler.SetMessageInfo("User_" + i.ToString(), "One line message " + i.ToString());
		}

		GetComponent<UIGrid>().Reposition();
    }
}
