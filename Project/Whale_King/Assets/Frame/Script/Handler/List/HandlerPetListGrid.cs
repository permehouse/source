using UnityEngine;
using System.Collections;

public class HandlerPetListGrid : MonoBehaviour
{
	public GameObject prefabPetListItem;

	public HandlerPetListGrid()
	{
	}

	void Start ()
	{
		InitPetList();
	}
	
	void Update ()
	{
	
	}

	protected void InitPetList()
	{
		Debug.Log("InitPetList(), " + UICharactorPet.petInfoList.Count.ToString());
		for (int i = 0; i < UICharactorPet.petInfoList.Count; ++i)
		{
			GameObject obj = Instantiate(prefabPetListItem, new Vector3(0.0f, 0.0f, -0.043f), Quaternion.identity) as GameObject;

			obj.transform.parent = this.transform;
			obj.transform.localScale = new Vector3(1f, 1f, 1f);
			HandlerPetListItem itemHandler = obj.GetComponentInChildren<HandlerPetListItem>();
			itemHandler.SetItemInfo(i);
		}

		GetComponent<UIGrid>().Reposition();
	}

	protected void OnSelectedItem()
	{
		for(int i = 0;i<this.transform.childCount;++i)
		{
			this.transform.GetChild(i).gameObject.SendMessage("OnSelectedItem");
		}
	}
}
