using UnityEngine;
using System.Collections;

public class HandlerRankListGrid : MonoBehaviour
{
	public GameObject prefabRankListItem;

	public HandlerRankListGrid()
	{
	}

	void Start ()
	{
//		DrawFriendList();
	}
	
	void Update ()
	{
	
	}
	
	protected void GetFriendList()
	{
		ServerConnector.ServerConnectionFinished callback = delegate(int result)
		{
			if(CommonDef.errorSuccessed == result)
			{
				DrawFriendList();
			}
			else
			{
				Debug.Log("Get friend list error. code:" + result.ToString());
			}
		};

		ServerConnector connector = ServerConnector.GetInstance();
		if(null != connector)
		{
			connector.QueryFriendList(callback);
		}
	}
	
	protected void DrawFriendList()
	{
		RemoveAllChildren();
		int iInsertedIndex = 0;
		int i = 0;
		for (i = 0; i < GlobalValues.friendList.Count; ++i)
		{
			GlobalValues.GameContact contact = (GlobalValues.GameContact)GlobalValues.friendList[i];
			if(contact.week != GlobalValues.gameUser.week)
				continue;

			GameObject obj = Instantiate(prefabRankListItem, new Vector3(0.0f, 0.0f, -0.004f), Quaternion.identity) as GameObject;

			obj.transform.parent = this.transform;
			obj.transform.localScale = new Vector3(1f, 1f, 1f);
			HandlerRankListItem itemHandler = obj.GetComponentInChildren<HandlerRankListItem>();
			itemHandler.SetItemInfo(iInsertedIndex, iInsertedIndex+1, iInsertedIndex);
			++iInsertedIndex;
		}
/*		
		for (i = 0; i < GlobalValues.friendList.Count; ++i)
		{
			GlobalValues.GameContact contact = (GlobalValues.GameContact)GlobalValues.friendList[i];
			if(contact.week == GlobalValues.gameUser.week)
				continue;

			GameObject obj = Instantiate(prefabRankListItem, new Vector3(0.0f, 0.0f, -0.004f), Quaternion.identity) as GameObject;

			obj.transform.parent = this.transform;
			obj.transform.localScale = new Vector3(1f, 1f, 1f);
			HandlerRankListItem itemHandler = obj.GetComponentInChildren<HandlerRankListItem>();
			itemHandler.SetItemInfo(iInsertedIndex, iInsertedIndex+1, iInsertedIndex);
			++iInsertedIndex;
		}
*/
		GetComponent<UIGrid>().Reposition();
	}
	
	protected void RemoveAllChildren()
	{
		HandlerRankListItem[] children = GetComponentsInChildren<HandlerRankListItem>();
		foreach(HandlerRankListItem childObj in children)
		{
			DestroyImmediate(childObj.gameObject);
		}
		transform.DetachChildren();
	}
}
