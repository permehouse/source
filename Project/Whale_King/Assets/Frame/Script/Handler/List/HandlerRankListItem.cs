using UnityEngine;
using System.Collections;

public class HandlerRankListItem : MonoBehaviour
{
	private int m_iKakaoID;
	private int m_iRank;
	private int m_iFriendListID;
	private bool m_bIsMe;
	
	public HandlerRankListItem()
	{
		InitData();
	}
	
	void Start ()
	{
	
	}

	void Update ()
	{
	
	}
	
	private void InitData()
	{
		m_iKakaoID = -1;
		m_iRank = -1;
		m_iFriendListID = -1;
		m_bIsMe = false;
	}
	
	public void OnClickPresentLife()
	{
//		HandlerRankListItem itemHandler = this.transform.parent.GetComponentInChildren<HandlerRankListItem>();
//		Debug.Log(string.Format("Hello? My number is {0}", itemHandler.GetKakaoID()));
		Debug.Log(string.Format("Hello? My number is {0}, {1}", m_iKakaoID, m_bIsMe));
	}
	
	public void SetItemInfo(int iKakaoID, int iRank, int iFriendListID)
	{
		m_iKakaoID = iKakaoID;
		m_iFriendListID = iFriendListID;
		m_iRank = iRank;

		GlobalValues.GameContact contact = (GlobalValues.GameContact)GlobalValues.friendList[m_iFriendListID];

		m_bIsMe = (contact.email == GlobalValues.gameUser.email);
		
		//Profile image
		if(contact.imageUrl != "")
		{
			StartCoroutine(SetProfilePic(ServerConnector.GetInstance().strDomainName + contact.imageUrl));
		}

		//Item background, Rank number
		UISprite[] spriteChildren = GetComponentsInChildren<UISprite>();
		foreach(UISprite spriteItem in spriteChildren)
		{
			if(spriteItem.name == "RankNumber")
			{
				spriteItem.spriteName = "CommonRankNo_" + m_iRank.ToString();
			}
			else if(spriteItem.name == "RankItemBGNormal")
			{
				spriteItem.gameObject.SetActive(!m_bIsMe);
			}
			else if(spriteItem.name == "RankItemBGMe")
			{
				spriteItem.gameObject.SetActive(m_bIsMe);
			}
			else if(spriteItem.name == "imgCharactorIcon")
			{
				//TODO solmea, Always show now, because charactor id is allmost -1 yet.
				spriteItem.gameObject.SetActive(true);
				UICharactorPet.CharactorInfo charactor = UICharactorPet.GetCharacterInfoFromCode(contact.character);
				if("" != charactor.charactorIconName)
				{
					spriteItem.gameObject.SetActive(true);
					spriteItem.spriteName = charactor.charactorIconName;
				}
			}
		}

		//Label text
		UILabel[] labelChildren = GetComponentsInChildren<UILabel>();
		foreach(UILabel labelItem in labelChildren)
		{
			if(labelItem.name == "lblName")
			{
				labelItem.text = contact.name;
			}
			else if(labelItem.name == "lblTopScore")
			{
				bool bExistScore = true;

				if(bExistScore)
				{
					labelItem.text = contact.weekScore.ToString("n0");
				}
				else
				{
					labelItem.text = "No Score";
					labelItem.color = new Color(19f, 87f, 117f);	//#135775
				}
/*
- "c" : 해당 컴퓨터에서 사용하는 통화 단위로 변환
- "f3" : 소수점 아래로 세 자리까지 표시
- "0" : 정수 형태로 변환
- "0%" : 정수 퍼센트 단위로 변환
- "n" : 천 단위마다 쉼표를 집어넣음(소수점 2자리 나옴)
- "n0" : 천 단위마다 쉼표를 집어넣음(소수점 안나옴)
- "yyyy-MM-dd HH:mm:ss" : 날짜 형식으로 표시
- "yyyy-MM-dd tt hh:mm:ss" : 날짜 형식으로 표시(오전/오후 표시)
- "yyyy-MM-dd tt hh:mm:ss ffffff" : 밀리세컨즈까지 표시
*/
			}
		}
	}

	public int GetKakaoID()
	{
		return m_iKakaoID;
	}
	
	protected IEnumerator SetProfilePic(string strProfilePicURL)
	{
		Debug.Log(">>>> SetProfilePic : " + strProfilePicURL);
		
		UITexture[] textureChildren = GetComponentsInChildren<UITexture>();
		foreach(UITexture textureItem in textureChildren)
		{
			if(textureItem.name == "imgProfilePic")
			{
				WWW www = new WWW(strProfilePicURL);
		     		yield return www;

				Material mat = new Material(textureItem.material);
				mat.mainTexture = www.texture;
		       		textureItem.material = mat;
				break;
			}
		}
	}
}
