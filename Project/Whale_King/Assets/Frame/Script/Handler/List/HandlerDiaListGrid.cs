using UnityEngine;
using System.Collections;

public class HandlerDiaListGrid : MonoBehaviour
{
	public GameObject prefabDiaListItem;

	public HandlerDiaListGrid()
	{
	}

	void Start ()
	{
		InitDiaList();
	}
	
	void Update ()
	{
	
	}
	
	protected void InitDiaList()
	{		
		Debug.Log("Init Dia List(), " + UIPayment.diaInfoList.Count.ToString());
		for (int i = 0; i < UIPayment.diaInfoList.Count; ++i)
		{		
			GameObject obj = Instantiate(prefabDiaListItem, new Vector3(0.0f, 0.0f, -0.043f), Quaternion.identity) as GameObject;

			obj.transform.parent = this.transform;
			obj.transform.localScale = new Vector3(1f, 1f, 1f);
			HandlerDiaListItem itemHandler = obj.GetComponentInChildren<HandlerDiaListItem>();
			itemHandler.SetItemInfo(i);
		}
		GetComponent<UIGrid>().Reposition();
	}

	protected void OnSelectedItem()
	{
		for(int i = 0;i<this.transform.childCount;++i)
		{
			this.transform.GetChild(i).gameObject.SendMessage("OnSelectedItem");
		}
	}
}
