using UnityEngine;
using System.Collections;

public class HandlerGoldListItem : MonoBehaviour
{
	
	private int chooseGold;
	

	public HandlerGoldListItem()
	{		
	}
	
	void Diat ()
	{	
	}

	void Update ()
	{	
	}
		
	public void OnClickChoose()
	{		
		Debug.Log("btn Gold Payment Click = "+chooseGold); //선택한 칸.		
	}

	public void SetItemInfo(int iGoldIndex)
	{
		Debug.Log("...Gold Set ItemInfo");
		
		//For test
		if(iGoldIndex < 0 || UIPayment.goldInfoList.Count <= iGoldIndex)
			return;
		
		chooseGold = iGoldIndex;
		
		UIPayment.GoldInfo gold = (UIPayment.GoldInfo)UIPayment.goldInfoList[iGoldIndex];
		
		UILabel[] labelChildren = GetComponentsInChildren<UILabel>();
		foreach(UILabel labelItem in labelChildren)
		{
			if(labelItem.name == "lblBonus")				//보너스 % 표시.
			{
				labelItem.text = (100*(gold.nCount-(gold.nPrice*gold.nMultiply/gold.nDivid)) /(gold.nPrice*gold.nMultiply/gold.nDivid))+"%";
			}			
			else if(labelItem.name == "lblGoods")		
			{	
				labelItem.text = gold.nCount.ToString();				
			}
			else if(labelItem.name == "lblPay")				//가격.
			{
				labelItem.text = gold.nPrice.ToString();				
			}
		}		
	}	
}
