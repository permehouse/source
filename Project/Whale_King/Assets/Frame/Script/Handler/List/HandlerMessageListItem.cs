using UnityEngine;
using System.Collections;

public class HandlerMessageListItem : MonoBehaviour
{
	public HandlerMessageListItem()
	{
		InitData();
	}

	void Start ()
	{
	
	}

	void Update ()
	{
	
	}
	
	private void InitData()
	{
		
	}
	
	public void OnClickReply()
	{
		Debug.Log("OnClickReply()");
//		Debug.Log(string.Format("Hello? My number is {0}, {1}", m_iKakaoID, m_bIsMe));
	}

	public void SetMessageInfo(string strSenderName, string strMessage)
	{
		//Profile image
//		StartCoroutine(SetProfilePic(strProfilePicURL));

		//Label text
		UILabel[] labelChildren = GetComponentsInChildren<UILabel>();
		foreach(UILabel labelItem in labelChildren)
		{
			if(labelItem.name == "lblSenderName")
			{
				labelItem.text = strSenderName;
			}
			else if(labelItem.name == "lblMessage")
			{
				labelItem.text = strMessage;
			}
		}
	}

/*	
	protected IEnumerator SetProfilePic(string strProfilePicURL)
	{
		UITexture[] textureChildren = GetComponentsInChildren<UITexture>();
		foreach(UITexture textureItem in textureChildren)
		{
			if(textureItem.name == "imgProfilePic")
			{
				WWW www = new WWW(strProfilePicURL);
		        yield return www;

				Material mat = new Material(textureItem.material);
				mat.mainTexture = www.texture;
		        textureItem.material = mat;
				break;
			}
		}
	}
*/
}
