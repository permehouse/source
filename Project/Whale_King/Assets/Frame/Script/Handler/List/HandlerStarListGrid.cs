using UnityEngine;
using System.Collections;

public class HandlerStarListGrid : MonoBehaviour
{
	public GameObject prefabStarListItem;

	public HandlerStarListGrid()
	{
	}

	void Start ()
	{
		InitStarList();
	}
	
	void Update ()
	{
	
	}
	
	protected void InitStarList()
	{		
		Debug.Log("Init Star List(), " + UIPayment.starInfoList.Count.ToString());
		for (int i = 0; i < UIPayment.starInfoList.Count; ++i)
		{		
			GameObject obj = Instantiate(prefabStarListItem, new Vector3(0.0f, 0.0f, -0.043f), Quaternion.identity) as GameObject;

			obj.transform.parent = this.transform;
			obj.transform.localScale = new Vector3(1f, 1f, 1f);
			HandlerStarListItem itemHandler = obj.GetComponentInChildren<HandlerStarListItem>();
			itemHandler.SetItemInfo(i);
		}
		GetComponent<UIGrid>().Reposition();
	}

	protected void OnSelectedItem()
	{
		for(int i = 0;i<this.transform.childCount;++i)
		{
			this.transform.GetChild(i).gameObject.SendMessage("OnSelectedItem");
		}
	}
}
