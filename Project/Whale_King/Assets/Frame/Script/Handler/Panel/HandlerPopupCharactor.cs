using UnityEngine;
using System.Collections;

public class HandlerPopupCharactor : MonoBehaviour
{
	public GameObject pageCharactor;
	public GameObject pagePet;
	public GameObject pageEquipment;
	public UIImageButton tabBtnCharactor;
	public UIImageButton tabBtnPet;
	public UIImageButton tabBtnEquipment;
	public UIImageButton btnRelayOK;
	public UISprite tabImgCharactor;
	public UISprite tabImgPet;
	public UISprite tabImgEquipment;
	public UISprite imgTxtChooseRelayCharactornRelayClickOK;
	public UILabel lblGold;
	public UILabel lblSapphire;	
	public int nCurrentPAGE;
	
	
	private enum PAGE_INDEX
	{
		PAGE_CHARACTOR = 0,
		PAGE_PET,
		PAGE_EQUIPMENT,
		PAGE_CHARACTOR_RELAY,
	}
	
	//원래 Start()에 있는 내용인데, Start 는 처음한번만 실행되고 실행시마다 분기해야 할 필요가 있어서 OnEnable 에 넣음.
	void OnEnable ()	
	{			
		switch (GlobalValues.userValues.chooseBtnCPI) {
		case "Charactor":
				SwitchPage(PAGE_INDEX.PAGE_CHARACTOR);
				break;
		case "Pet":
				SwitchPage(PAGE_INDEX.PAGE_PET);
				break;
		case "Item":
				SwitchPage(PAGE_INDEX.PAGE_EQUIPMENT);
				break;			
		}		
	}
	
	void Start ()
	{		
		lblGold.gameObject.SetActive(true);
		//TODO solmea
		lblGold.text = "0";
		lblSapphire.gameObject.SetActive(true);
		lblSapphire.text = GlobalValues.gameUser.jewels.ToString("n0");
	}

	void Update ()
	{
		/*
	switch (GlobalValues.userValues.chooseBtnCPI) {
		case "Charactor":
				SwitchPage(PAGE_INDEX.PAGE_CHARACTOR);
				break;
		case "Pet":
				SwitchPage(PAGE_INDEX.PAGE_PET);
				break;
		case "Item":
				SwitchPage(PAGE_INDEX.PAGE_EQUIPMENT);
				break;
			
		}
		*/
	}	
	
	// 릴레이 버튼.
	void OnRelayClickOK()
	{
		Debug.Log("Relay OnClickOK()");
		SwitchPage(PAGE_INDEX.PAGE_CHARACTOR_RELAY);
	}
	
	// 확인 버튼.
	void OnClickOK()
	{
		Debug.Log("Popup Charactor OnClickOK()");		
			
		switch(nCurrentPAGE)
		{
			case (int)PAGE_INDEX.PAGE_CHARACTOR:			
			case (int)PAGE_INDEX.PAGE_PET:
			case (int)PAGE_INDEX.PAGE_EQUIPMENT:			
				//다이얼로그 박스 닫음. 
				GameObject currentPanel = GetComponent<UIPanel>().gameObject;
				currentPanel.gameObject.SetActive(false);		
				break;
			case (int)PAGE_INDEX.PAGE_CHARACTOR_RELAY:
				// 캐릭터 페이지로 이동.
				SwitchPage(PAGE_INDEX.PAGE_CHARACTOR);
				break;			
		}		
	}
	
	void OnClickTabCharactor()
	{
		SwitchPage(PAGE_INDEX.PAGE_CHARACTOR);
	}
	
	void OnClickTabPet()
	{
		SwitchPage(PAGE_INDEX.PAGE_PET);
	}
	
	void OnClickTabEquipment()
	{
		SwitchPage(PAGE_INDEX.PAGE_EQUIPMENT);
	}
	
	protected void OnClickBuyGold()
	{
		Debug.Log("OnClickBuyGold()");
	}
	
	protected void OnClickBuySapphire()
	{
		Debug.Log("OnClickBuySapphire()");
	}
	
	private void SwitchPage(PAGE_INDEX indexPage)
	{		
		nCurrentPAGE = (int)indexPage;
		
		//pageCharactor.SetActive(false);
		pagePet.SetActive(false);
		pageEquipment.SetActive(false);		
		
		//tabBtnCharactor.gameObject.SetActive(true);
		tabBtnPet.gameObject.SetActive(true);
		tabBtnEquipment.gameObject.SetActive(true);
		
		//tabImgCharactor.gameObject.SetActive(false);
		tabImgPet.gameObject.SetActive(false);
		tabImgEquipment.gameObject.SetActive(false);
		
		btnRelayOK.gameObject.SetActive(false); 
		imgTxtChooseRelayCharactornRelayClickOK.gameObject.SetActive(false);
		
		switch(indexPage)
		{
		case PAGE_INDEX.PAGE_CHARACTOR:
			pageCharactor.SetActive(true);
			tabBtnCharactor.gameObject.SetActive(false);
			tabImgCharactor.gameObject.SetActive(true);
			btnRelayOK.gameObject.SetActive(true);			
			GlobalValues.userValues.chooseCharactorDialogMode = "Charactor";
			break;
		case PAGE_INDEX.PAGE_PET:
			pageCharactor.SetActive(false);
			tabBtnCharactor.gameObject.SetActive(true);
			tabImgCharactor.gameObject.SetActive(false);
			pagePet.SetActive(true);
			tabBtnPet.gameObject.SetActive(false);
			tabImgPet.gameObject.SetActive(true);			
			break;
		case PAGE_INDEX.PAGE_EQUIPMENT:
			pageCharactor.SetActive(false);
			tabBtnCharactor.gameObject.SetActive(true);
			tabImgCharactor.gameObject.SetActive(false);
			pageEquipment.SetActive(true);
			tabBtnEquipment.gameObject.SetActive(false);
			tabImgEquipment.gameObject.SetActive(true);			
			break;
		case PAGE_INDEX.PAGE_CHARACTOR_RELAY:
			pageCharactor.SetActive(true);
			tabBtnCharactor.gameObject.SetActive(false);
			tabImgCharactor.gameObject.SetActive(true);			
			imgTxtChooseRelayCharactornRelayClickOK.gameObject.SetActive(true);
			tabBtnPet.gameObject.SetActive(false);
			tabBtnEquipment.gameObject.SetActive(false);			
			GlobalValues.userValues.chooseCharactorDialogMode = "Relay";
			break;			
		}		
	}
}


/*
  // 확인 버튼  .
	void OnClickOK()
	{
		Debug.Log("Popup Charactor OnClickOK()");		
	
		//Refresh ChararctorPet.
		GameObject[] allPanel = GameObject.FindGameObjectsWithTag("UIPanel");
		foreach(GameObject panel in allPanel)
		{
			if(panel.name == "PanelReady")
			{				
				panel.SendMessage("RefreshCharactorPet");				
			}
		}		
		
		//캐릭터 다이얼로그 박스 닫음. 
		GameObject currentPanel = GetComponent<UIPanel>().gameObject;
		currentPanel.gameObject.SetActive(false);
		
		
		// Save selected character to Server
		ServerConnector.ServerConnectionFinished callbackChangeCharacter = delegate(int result)
		{
			if(CommonDef.errorSuccessed == result) {
				GlobalValues.gameUser.characterCode = UICharactorPet.selectedCharacterCode;	
				Debug.Log("....SelectedCharacterCode.."+UICharactorPet.selectedCharacterCode);
				Debug.Log("....chExpress.."+UICharactorPet.selectedCharacterExpressionName);				
			}
			else {
				MessageBox.ShowMessageBox("Error:Chr Store setting errorCode:" + result.ToString(), CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
			}		
			//MessageBox.ShowMessageBox(GlobalValues.gameUser.characterCode+"c code" + result.ToString(), CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
		};
		ServerConnector connector = ServerConnector.GetInstance();
		if(null != connector) {
			connector.ChangeCharacter(callbackChangeCharacter);
		}
		
		// Save selected pet to Server
		ServerConnector.ServerConnectionFinished callbackChangePet = delegate(int result)
		{
			if(CommonDef.errorSuccessed == result) {
				GlobalValues.gameUser.petCode = UICharactorPet.selectedPetCode;		//yj 선택한 펫 정보 넣어줌. 레벨,등급,이름까지 넣어줘야 할듯.
			}
			else {
				MessageBox.ShowMessageBox("Error:Pet Store setting errorCode:" + result.ToString(), CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
			}		
		};
		if(null != connector) {
			connector.ChangePet(callbackChangePet);
		}
		
		//메인화면(panelReady)에서 보석, 골드 사용된 결과를 갱신(Retrieve User) 해줌.				
		//GlobalValues.RetrieveGameUserMain();
		
	}  
 */ 
