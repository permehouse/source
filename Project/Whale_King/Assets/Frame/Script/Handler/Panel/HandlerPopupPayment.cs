using UnityEngine;
using System.Collections;

public class HandlerPopupPayment : MonoBehaviour
{
	public GameObject pageStar;
	public GameObject pageGold;
	public GameObject pageDia;
	
	public UIImageButton tabBtnStar;
	public UIImageButton tabBtnGold;
	public UIImageButton tabBtnDia;
	
	public UISprite tabImgStar;
	public UISprite tabImgGold;
	public UISprite tabImgDia;
	
	public UILabel lblGold;
	public UILabel lblSapphire;	
	public int nCurrentPAGE;
	
	
	private enum PAGE_INDEX
	{
		PAGE_STAR = 0,
		PAGE_GOLD,
		PAGE_DIA,
		PAGE_STAR_RELAY,
	}
	
	//원래 Start()에 있는 내용인데, Start 는 처음한번만 실행되고 실행시마다 분기해야 할 필요가 있어서 OnEnable 에 넣음.
	void OnEnable ()	
	{			
		switch (GlobalValues.userValues.chooseBtnCPI) {
		case "Star":
				SwitchPage(PAGE_INDEX.PAGE_STAR);
				break;
		case "Gold":
				SwitchPage(PAGE_INDEX.PAGE_GOLD);
				break;
		case "Dia":
				SwitchPage(PAGE_INDEX.PAGE_DIA);
				break;			
		}		
	}
	
	void Start ()
	{		
		lblGold.gameObject.SetActive(true);
		//TODO solmea
		lblGold.text = "0";
		lblSapphire.gameObject.SetActive(true);
		lblSapphire.text = GlobalValues.gameUser.jewels.ToString("n0");
	}

	void Update ()
	{
		/*
	switch (GlobalValues.userValues.chooseBtnCPI) {
		case "Charactor":
				SwitchPage(PAGE_INDEX.PAGE_STAR);
				break;
		case "Pet":
				SwitchPage(PAGE_INDEX.PAGE_GOLD);
				break;
		case "Item":
				SwitchPage(PAGE_INDEX.PAGE_DIA);
				break;
			
		}
		*/
	}	
	
	
	// 확인 버튼.
	void OnClickOK()
	{
		Debug.Log("Popup Charactor OnClickOK()");		
			
		switch(nCurrentPAGE)
		{
			case (int)PAGE_INDEX.PAGE_STAR:			
			case (int)PAGE_INDEX.PAGE_GOLD:
			case (int)PAGE_INDEX.PAGE_DIA:			
				//다이얼로그 박스 닫음. 
				GameObject currentPanel = GetComponent<UIPanel>().gameObject;
				currentPanel.gameObject.SetActive(false);		
				break;
			case (int)PAGE_INDEX.PAGE_STAR_RELAY:
				// 캐릭터 페이지로 이동.
				SwitchPage(PAGE_INDEX.PAGE_STAR);
				break;			
		}		
	}
	
	void OnClickTabCharactor()
	{
		SwitchPage(PAGE_INDEX.PAGE_STAR);
	}
	
	void OnClickTabPet()
	{
		SwitchPage(PAGE_INDEX.PAGE_GOLD);
	}
	
	void OnClickTabEquipment()
	{
		SwitchPage(PAGE_INDEX.PAGE_DIA);
	}
	
	private void SwitchPage(PAGE_INDEX indexPage)
	{		
		nCurrentPAGE = (int)indexPage;
		
		//pageStar.SetActive(false);
		pageGold.SetActive(false);
		pageDia.SetActive(false);		
		
		//tabBtnStar.gameObject.SetActive(true);
		tabBtnGold.gameObject.SetActive(true);
		tabBtnDia.gameObject.SetActive(true);
		
		//tabImgStar.gameObject.SetActive(false);
		tabImgGold.gameObject.SetActive(false);
		tabImgDia.gameObject.SetActive(false);
		
		
		
		switch(indexPage)
		{
		case PAGE_INDEX.PAGE_STAR:
			pageStar.SetActive(true);
			tabBtnStar.gameObject.SetActive(false);
			tabImgStar.gameObject.SetActive(true);
				
			GlobalValues.userValues.chooseCharactorDialogMode = "Charactor";
			break;
		case PAGE_INDEX.PAGE_GOLD:
			pageStar.SetActive(false);
			tabBtnStar.gameObject.SetActive(true);
			tabImgStar.gameObject.SetActive(false);
			pageGold.SetActive(true);
			tabBtnGold.gameObject.SetActive(false);
			tabImgGold.gameObject.SetActive(true);			
			break;
		case PAGE_INDEX.PAGE_DIA:
			pageStar.SetActive(false);
			tabBtnStar.gameObject.SetActive(true);
			tabImgStar.gameObject.SetActive(false);
			pageDia.SetActive(true);
			tabBtnDia.gameObject.SetActive(false);
			tabImgDia.gameObject.SetActive(true);			
			break;
		case PAGE_INDEX.PAGE_STAR_RELAY:
			pageStar.SetActive(true);
			tabBtnStar.gameObject.SetActive(false);
			tabImgStar.gameObject.SetActive(true);			
			
			tabBtnGold.gameObject.SetActive(false);
			tabBtnDia.gameObject.SetActive(false);			
			
			break;			
		}		
	}
}


/*
  // 확인 버튼  .
	void OnClickOK()
	{
		Debug.Log("Popup Charactor OnClickOK()");		
	
		//Refresh ChararctorPet.
		GameObject[] allPanel = GameObject.FindGameObjectsWithTag("UIPanel");
		foreach(GameObject panel in allPanel)
		{
			if(panel.name == "PanelReady")
			{				
				panel.SendMessage("RefreshCharactorPet");				
			}
		}		
		
		//캐릭터 다이얼로그 박스 닫음. 
		GameObject currentPanel = GetComponent<UIPanel>().gameObject;
		currentPanel.gameObject.SetActive(false);
		
		
		// Save selected character to Server
		ServerConnector.ServerConnectionFinished callbackChangeCharacter = delegate(int result)
		{
			if(CommonDef.errorSuccessed == result) {
				GlobalValues.gameUser.characterCode = UICharactorPet.selectedCharacterCode;	
				Debug.Log("....SelectedCharacterCode.."+UICharactorPet.selectedCharacterCode);
				Debug.Log("....chExpress.."+UICharactorPet.selectedCharacterExpressionName);				
			}
			else {
				MessageBox.ShowMessageBox("Error:Chr Store setting errorCode:" + result.ToString(), CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
			}		
			//MessageBox.ShowMessageBox(GlobalValues.gameUser.characterCode+"c code" + result.ToString(), CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
		};
		ServerConnector connector = ServerConnector.GetInstance();
		if(null != connector) {
			connector.ChangeCharacter(callbackChangeCharacter);
		}
		
		// Save selected pet to Server
		ServerConnector.ServerConnectionFinished callbackChangePet = delegate(int result)
		{
			if(CommonDef.errorSuccessed == result) {
				GlobalValues.gameUser.petCode = UICharactorPet.selectedPetCode;		//yj 선택한 펫 정보 넣어줌. 레벨,등급,이름까지 넣어줘야 할듯.
			}
			else {
				MessageBox.ShowMessageBox("Error:Pet Store setting errorCode:" + result.ToString(), CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
			}		
		};
		if(null != connector) {
			connector.ChangePet(callbackChangePet);
		}
		
		//메인화면(panelReady)에서 보석, 골드 사용된 결과를 갱신(Retrieve User) 해줌.				
		//GlobalValues.RetrieveGameUserMain();
		
	}  
 */ 
