using UnityEngine;
using System.Collections;

public class HandlerPanelReady : MonoBehaviour
{
	public UIPanel panelRankListView;

	public UIPanel panelPopupNotice;
	public UIPanel panelPopupConfig;
	public UIPanel panelPopupInvite;
	public UIPanel panelPopupMailBox;
	public UIPanel panelPopupCharactor;
	public UIPanel panelPopupShop;
	public UIPanel panelPopupPayment;
	
	public UIPanel panelPopupLogin;

	public UILabel lblDigestFishCount;
	public UILabel lblJewelCount;
	public UILabel lblMyFishCount;
	
	public UILabel lblCharactorName;			//메인화면에서 캐릭터 이름 표시.
	public UILabel lblPetName;					//메인화면에서 펫 이름 표시.
	public UILabel lblCharactorLevel;			//메인화면에서 캐릭터 레벨 표시.
	public UILabel lblPetLevel;					//메인화면에서 펫 레벨 표시.
	
	public UISpriteAnimation imgDigestAni;
	public UISprite imgCountLife;
	//Life
	public UISlider progLife;
	public UILabel lblRemainTimeToAddLife;
	//Charactor, Pet
	public UISpriteAnimation imgPreviewCharactorAni;
	public UISpriteAnimation imgPreviewCharactorBackAni;
	public UISpriteAnimation imgPreviewPetAni;

	public AudioSource audioBGMReady;

	private float timerSecondBase;
	private float timerSecondCountDown;
	private bool allowRetrieveGameUserData;
	private int retrieveGameUserDataFailedCount;
	
	public UISprite imgLoadingScreen;

	HandlerPanelReady()
	{
		timerSecondBase = 0.0f;
		timerSecondCountDown = 0.0f;
		allowRetrieveGameUserData = false;
		retrieveGameUserDataFailedCount = 0;
	}

	void Start ()
	{		
		bool isLogined = (GlobalValues.sessionId != "");
		//Panels
		panelRankListView.gameObject.SetActive(isLogined);

		panelPopupNotice.gameObject.SetActive(false);
		panelPopupConfig.gameObject.SetActive(false);
		panelPopupInvite.gameObject.SetActive(false);
		panelPopupMailBox.gameObject.SetActive(false);		
		panelPopupCharactor.gameObject.SetActive(false);		
		panelPopupShop.gameObject.SetActive(false);
		panelPopupLogin.gameObject.SetActive(false);		//131013 Add.
		
		
		if(!isLogined && "" != GlobalValues.EMail)
		{
			Debug.Log ("DDDDDDDDDDDDDDDDDDDDDDDOOOOOOOOO Login : " + GlobalValues.EMail);
			doLogin();
		}
		else
		{
			HideLoadingScreen();
			panelPopupLogin.gameObject.SetActive(!isLogined);
		}

		//UI objects
		imgPreviewCharactorBackAni.gameObject.SetActive(false);
		imgDigestAni.framesPerSecond = 0;
		imgDigestAni.gameObject.SetActive(false);
		imgPreviewCharactorAni.gameObject.SetActive(false);
		imgPreviewCharactorBackAni.gameObject.SetActive(false);
		imgPreviewPetAni.gameObject.SetActive(false);

		progLife.sliderValue = 0.0f;
		lblRemainTimeToAddLife.gameObject.SetActive(false);
		
		timerSecondBase = 0.0f;
		timerSecondCountDown = 0.0f;
		
		audioBGMReady.Stop();

		if(isLogined)
		{
			if(0 != GlobalValues.settings.soundPlay)
			{
				audioBGMReady.Play();
			}

			ServerConnector.ServerConnectionFinished callback = delegate(int result)
			{
				timerSecondBase = 1.0f;
				if(CommonDef.errorSuccessed == result)
				{
					SendMessage("RefreshUIData");
					allowRetrieveGameUserData = true;
				}
			};

			ServerConnector connector = ServerConnector.GetInstance();
			if(null != connector)
			{
				allowRetrieveGameUserData = false;
				connector.RetrieveGameUser(callback);
			}
			
			GameObject target = GetComponentInChildren<HandlerRankListGrid>().gameObject;
			target.SendMessage("GetFriendList");
		}

		SendMessage("RefreshUIData");
	
		
	}
	
	void OnEnable ()
	{
			
	}

	void Update ()
	{
		if(0.0f < timerSecondBase)
		{
			timerSecondCountDown -= Time.deltaTime;
			if(timerSecondCountDown <= 0)
			{
				timerSecondCountDown = timerSecondBase;
				handlerTimerSecond();
			}
		}
	}
	
	

	// 중간의 4가지 버튼.
	
	protected void OnClickPet()
	{		
		Debug.Log("OnClick Pet()");
		GlobalValues.userValues.chooseBtnCPI = "Pet";
		panelPopupCharactor.gameObject.SetActive(true);
	}
	
	protected void OnClickItem()
	{
		Debug.Log("OnClick Item()");
		GlobalValues.userValues.chooseBtnCPI = "Item";
		panelPopupCharactor.gameObject.SetActive(true);
	}
	protected void OnClickChractor()
	{
		Debug.Log("OnClick Chractor()");
		GlobalValues.userValues.chooseBtnCPI = "Charactor";
		panelPopupCharactor.gameObject.SetActive(true);		
	}

	protected void OnClickShop()
	{
		Debug.Log("OnClickShop()");
		panelPopupShop.gameObject.SetActive(true);
	}
	
	// 오른쪽 4가지 버튼.
	
	protected void OnClickConfig()
	{
		Debug.Log("OnClickConfig()");
		GlobalValues.RetrieveGameUserForRefreshStats();	//뺏긴 물고기의 빠른 리플레시를 위해서 시도했으나 효과 없음.
		//panelPopupConfig.gameObject.SetActive(true);
		//panelPopupConfig.SendMessage("ApplyGlobalValues");
		
	}
	public void RunPopupConfig()
	{
		Debug.Log("RunPopupConfig()");
		panelPopupConfig.gameObject.SetActive(true);
		panelPopupConfig.SendMessage("ApplyGlobalValues");		
		
	}
	

	protected void OnClickEvent()
	{
		Debug.Log("OnClickEvent()");
		//panelPopupMessageSend.gameObject.SetActive(true);
	}
	
	protected void OnClickMessage()
	{
		Debug.Log("OnClickMessage()");
		panelPopupMailBox.gameObject.SetActive(true);
	}
	
	protected void OnClickInvite()
	{
		Debug.Log("OnClickInvite()");
		panelPopupInvite.gameObject.SetActive(true);
	}	
	
	// 돈 주고 구매, 기회, 물고기, 보석.	
	
	protected void OnClickAddLife()
	{
		Debug.Log("OnClickAddLife()");
		GlobalValues.userValues.chooseBtnCPI = "Star";
		panelPopupPayment.gameObject.SetActive(true);
	}
	
	protected void OnClickAddFish()
	{
		Debug.Log("OnClickAdd Fish()");
		GlobalValues.userValues.chooseBtnCPI = "Gold";
		panelPopupPayment.gameObject.SetActive(true);
	}
	
	protected void OnClickAddDia()
	{
		Debug.Log("OnClickAdd Dia()");
		GlobalValues.userValues.chooseBtnCPI = "Dia";
		panelPopupPayment.gameObject.SetActive(true);
	}
	
	protected void OnClickStart()
	{
		// for Test
//		GlobalValues.gamePlay.player.nPlayerType = UICharactorPet.GetCurrentInGameCharactor();
//		GlobalValues.gamePlay.pet.nPetType = UICharactorPet.GetCurrentInGamePet();
//		Application.LoadLevel("Game");
		 
		//test
//		RefreshUIData();
//		return;
		Debug.Log("OnClickStart()");
		if(GlobalValues.gameUser.chance <= 0)
		{
			MessageBox.ShowMessageBox(GlobalStrings.notEnoughLife, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
			return;
		}

		ServerConnector.ServerConnectionFinished callbackStartGame = delegate(int result)
		{
			if(CommonDef.errorSuccessed == result)
			{
//				MessageBox.MessageBoxCallback callbackMessage = delegate(CommonDef.MESSAGE_BOX_RESULT resultMessage)
//				{
//					if(resultMessage == CommonDef.MESSAGE_BOX_RESULT.MB_OK)
						StartGame();
//				};
//				MessageBox.ShowMessageBox("Game started, PlayID:" + GlobalValues.gamePlay.gpid.ToString(), CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, callbackMessage);
			}
			else
			{
				MessageBox.ShowMessageBox("Error StartGame\nCode:" + result.ToString(), CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
			}
		};
		ServerConnector connector = ServerConnector.GetInstance();
		if(null != connector)
			connector.StartGame(callbackStartGame);
	}

	protected void StartGame()
	{
		if(0 == GlobalValues.gamePlay.gpid)
			return;

		--GlobalValues.gameUser.chance;

		GlobalValues.gamePlay.player.nPlayerType = UICharactorPet.GetCurrentInGameCharactor();
		GlobalValues.gamePlay.playerRelay.nPlayerType = UICharactorPet.GetCurrentInGameRelayCharactor();
		GlobalValues.gamePlay.pet.nPetType = UICharactorPet.GetCurrentInGamePet();

		GlobalValues.gamePlay.fishes = 0;
		GlobalValues.gamePlay.score = 0;
		GlobalValues.gamePlay.distance = 0;
		GlobalValues.gamePlay.startDate = GlobalAPIs.GetCurTimeString();	//YYMMDDHHMMSS
		GlobalValues.gamePlay.endDate = "";

		//TODO solmea, delete or modify by correct policy
		GlobalValues.gameUser.chanceUpdateTime = (System.DateTime.UtcNow + GlobalValues.timeGabFromServer).AddSeconds(GlobalValues.gameUser.chanceTimer);
		Debug.Log("Reset GlobalValues.gameUser.chanceUpdateTime, " + GlobalValues.gameUser.chanceUpdateTime.ToString());
		allowRetrieveGameUserData = true;
		retrieveGameUserDataFailedCount = 0;

		Debug.Log("GlobalValues.bNoPlayInGame:" + GlobalValues.bNoPlayInGame.ToString());
		if(true == GlobalValues.bNoPlayInGame)
		{
			GlobalValues.gamePlay.fishes = Random.Range(0, 100);
			GlobalValues.gamePlay.score = Random.Range(0, 10000);
			GlobalValues.gamePlay.distance = Random.Range(0, 1000);
			GlobalValues.gamePlay.startDate = GlobalAPIs.GetCurTimeString();	//YYMMDDHHMMSS
			GlobalValues.gamePlay.endDate = GlobalAPIs.GetCurTimeString();	//YYMMDDHHMMSS;
			Application.LoadLevel("EndGame");
		}
		else
		{
			Application.LoadLevel("Game");
		}
	}

	protected void RefreshUIData()
	{
		bool bIsLogined = (GlobalValues.sessionId != "");

		if(bIsLogined)
		{
			if(0 < GlobalValues.gameUser.digestFishes)
			{
				imgDigestAni.framesPerSecond = 4;
				imgDigestAni.gameObject.SetActive(true);
			}
			else
			{
				imgDigestAni.framesPerSecond = 0;
				imgDigestAni.gameObject.SetActive(false);
			}
			//count
			lblDigestFishCount.text = GlobalValues.gameUser.digestFishes.ToString();
			lblJewelCount.text = GlobalValues.gameUser.jewels.ToString();
			lblMyFishCount.text = GlobalValues.gameUser.fishes.ToString();
			
			
			//캐릭터 이름.
			lblCharactorName.text = GlobalValues.gameUser.expressionName;			//yj edit.
			lblPetName.text = GlobalValues.gameUser.expressionPet_1_Name;			//yj edit.

			//Life
			float sliderValue = (float)GlobalValues.gameUser.chance / (float)CommonDef.maxChanceCount;
			if(1.0f < sliderValue)
				sliderValue = 1.0f;
			progLife.sliderValue = sliderValue;

			lblRemainTimeToAddLife.gameObject.SetActive(true);
			if(GlobalValues.gameUser.chance < CommonDef.maxChanceCount)
			{
				allowRetrieveGameUserData = true;
			}
			else
			{
				int iOverCount = GlobalValues.gameUser.chance - CommonDef.maxChanceCount;
				if(0 == iOverCount)
					lblRemainTimeToAddLife.text = "Max";
				else
					lblRemainTimeToAddLife.text = "+" + iOverCount.ToString();
			}

			imgCountLife.spriteName = "CommonCountLife_" + GlobalValues.gameUser.chance.ToString();
		}
		else
		{
			imgDigestAni.gameObject.SetActive(false);
			lblDigestFishCount.text = "";
			lblJewelCount.text = "";
			lblMyFishCount.text = "";
			lblCharactorName.text = "";
			progLife.sliderValue = 0.0f;
			lblRemainTimeToAddLife.text = "00:00";
			imgCountLife.spriteName = "CommonCountLife_0";
		}

		RefreshCharactorPet();		
		
		// OnEnable 에서 MessageBox 표시가 안되어 RefreshUI 에서 신기록 표시 해줌.
		NewRecord();	
	}
	
	void NewRecord()
	{
		if (GlobalValues.userValues.chanceUpdated == 1) //신기록 세웠을 경우.
		{					
			MessageBox.MessageBoxCallback callbackNewRecord = delegate(CommonDef.MESSAGE_BOX_RESULT result)
			{
					GlobalValues.userValues.chanceUpdated = 0;		// OK 버튼 누르면 다시 표시 않도록 수정.
			};
			MessageBox.ShowMessageBox(GlobalStrings.newRecord, CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, callbackNewRecord);		
		}		
	}
	
	protected void RefreshCharactorPet()
	{
		bool bIsLogined = (GlobalValues.sessionId != "");

		if(bIsLogined)
		{
			Debug.Log("############ RefreshCharactorPet ################");
			
			imgPreviewCharactorBackAni.gameObject.SetActive(true);
			imgPreviewCharactorBackAni.framesPerSecond = 5;

			//charactor
			UICharactorPet.CharactorInfo charactorInfo = UICharactorPet.GetCharacterInfoFromCode(UICharactorPet.selectedCharacterCode);
			imgPreviewCharactorAni.gameObject.SetActive(true);
			imgPreviewCharactorAni.framesPerSecond = charactorInfo.animationFramesPerSecond;
			imgPreviewCharactorAni.namePrefix = charactorInfo.animationNamePrefix;

			//pet
			UICharactorPet.PetInfo petInfo = UICharactorPet.GetPetInfoFromCode(UICharactorPet.selectedPetCode);
			imgPreviewPetAni.gameObject.SetActive(true);
			imgPreviewPetAni.framesPerSecond = petInfo.animationFramesPerSecond;
			imgPreviewPetAni.namePrefix = petInfo.animationNamePrefix;
			
			if(0 == petInfo.animationFramesPerSecond)
			{
				UISprite sprite = imgPreviewPetAni.GetComponent<UISprite>();
				sprite.spriteName = petInfo.animationNamePrefix;
				sprite.MakePixelPerfect();
			}
		}
		else
		{
			imgPreviewCharactorBackAni.gameObject.SetActive(false);
			imgPreviewCharactorAni.gameObject.SetActive(false);
			imgPreviewCharactorAni.framesPerSecond = 0;
			imgPreviewCharactorAni.namePrefix = "";
			imgPreviewPetAni.gameObject.SetActive(false);
			imgPreviewPetAni.framesPerSecond = 0;
			imgPreviewPetAni.namePrefix = "";
		}
	}

	protected void ShowNotice()
	{
//		Debug.Log("ShowNotice()");
		if(GlobalValues.noticeList.Count <= 0)
			return;

		panelPopupNotice.gameObject.SetActive(true);
		panelPopupNotice.SendMessage("UpdateNotice");
	}
	
	protected void LoginCompleted()
	{
		timerSecondBase = 1.0f;
		timerSecondCountDown = 0.0f;

		SendMessage("RefreshUIData");

		ServerConnector.ServerConnectionFinished callback = delegate(int result)
		{
			if(CommonDef.errorSuccessed == result)
			{
				HideLoadingScreen();				
				ShowNotice();
			}
		};

		ServerConnector connector = ServerConnector.GetInstance();
		if(null != connector)
			connector.QueryNotice(callback);

		allowRetrieveGameUserData = true;
		retrieveGameUserDataFailedCount = 0;

		if(0 != GlobalValues.settings.soundPlay)
		{
			audioBGMReady.Play();
		}

		panelRankListView.gameObject.SetActive(true);
		GameObject target = GetComponentInChildren<HandlerRankListGrid>().gameObject;
		target.SendMessage("GetFriendList");
	}
	
	protected void LogoutCompleted()
	{
		SendMessage("RefreshUIData");
		panelPopupLogin.gameObject.SetActive(true);

		timerSecondBase = 0.0f;
		timerSecondCountDown = 0.0f;
		allowRetrieveGameUserData = false;
		retrieveGameUserDataFailedCount = 0;

		if(audioBGMReady.isPlaying)
		{
			audioBGMReady.Stop();
		}
		
		GameObject target = GetComponentInChildren<HandlerRankListGrid>().gameObject;
		target.SendMessage("RemoveAllChildren");
	}

	protected void ApplySettingsBGM()
	{
		if(0 == GlobalValues.settings.soundPlay)
		{
			if(audioBGMReady.isPlaying)
				audioBGMReady.Stop();
		}
		else
		{
			if(!audioBGMReady.isPlaying)
				audioBGMReady.Play();
		}
	}
	
	protected void handlerTimerSecond()
	{
//		Debug.Log("handlerTimerSecond");
		if(allowRetrieveGameUserData)
		{
			bool needRequestToServer = false;
			System.TimeSpan timeRemain;

			//Chance
			if(GlobalValues.gameUser.chance < CommonDef.maxChanceCount)
			{
				timeRemain = GlobalValues.gameUser.chanceUpdateTime - (System.DateTime.UtcNow + GlobalValues.timeGabFromServer);
				if(0 < timeRemain.Ticks)
				{
//					Debug.Log("Chance time remain:" + timeRemain.ToString());
					lblRemainTimeToAddLife.text = ((timeRemain.Minutes<10)?"0":"") + timeRemain.Minutes.ToString() + ":" + ((timeRemain.Seconds<10)?"0":"") + timeRemain.Seconds.ToString();
				}
				else
				{
					lblRemainTimeToAddLife.text = "00:00";
					needRequestToServer = true;
					GlobalValues.gameUser.chanceUpdateTime = (System.DateTime.UtcNow + GlobalValues.timeGabFromServer).AddSeconds(10.0f);
				}
			}
			else
			{
				int iOverCount = GlobalValues.gameUser.chance - CommonDef.maxChanceCount;
				if(0 == iOverCount)
					lblRemainTimeToAddLife.text = "Max";
				else
					lblRemainTimeToAddLife.text = "+" + iOverCount.ToString();
			}

			//Digest
			if(0 < GlobalValues.gameUser.digestFishes)
			{
				timeRemain = GlobalValues.gameUser.digestUpdateTime - (System.DateTime.UtcNow + GlobalValues.timeGabFromServer);
				if(0 < timeRemain.Ticks)
				{
					//Debug.Log("Digest time remain:" + timeRemain.ToString());
				}
				else
				{
					GlobalValues.gameUser.digestUpdateTime = (System.DateTime.UtcNow + GlobalValues.timeGabFromServer).AddSeconds(10.0f);
					needRequestToServer = true;
				}
			}
			
			if(needRequestToServer)
			{
				ServerConnector.ServerConnectionFinished callback = delegate(int result)
				{
					if(CommonDef.errorSuccessed == result)
					{
						SendMessage("RefreshUIData");
						allowRetrieveGameUserData = true;
					}
					else
					{
						//Retry retrieve game user data 10 seconds later if result is not successed.
						Debug.Log("Retrieve error code:" + result.ToString());
						Debug.Log("Set chanceUpdateTime, " + GlobalValues.gameUser.chanceUpdateTime.ToString());
						Debug.Log("Set digestUpdateTime, " + GlobalValues.gameUser.digestUpdateTime.ToString());

						allowRetrieveGameUserData = (retrieveGameUserDataFailedCount++ < 3);
					}
				};
				Debug.Log("Try retrieve game user data");
				ServerConnector connector = ServerConnector.GetInstance();
				if(null != connector)
				{
					allowRetrieveGameUserData = false;
					connector.RetrieveGameUser(callback);
				}
			}
		}
	}
	
	protected void doLogin()
	{
//		ServerConnector.ServerConnectionFinished callback = delegate(int result)
//		{
//			if(CommonDef.errorSuccessed == result)
//			{
//				GlobalValues.saveConfigValues();
//			}
//		};
		ServerConnector connector = ServerConnector.GetInstance();
		if(null != connector)
			connector.Login(null);
	}
	
	protected void HideLoadingScreen()
	{
		imgLoadingScreen.gameObject.SetActive(false);
	}
}
