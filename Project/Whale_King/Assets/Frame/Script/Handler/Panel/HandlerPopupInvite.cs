using UnityEngine;
using System.Collections;

public class HandlerPopupInvite : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnClickOK()
	{
		Debug.Log("OnClickOK()");
		GameObject currentPanel = GetComponent<UIPanel>().gameObject;
		currentPanel.gameObject.SetActive(false);
	}

	void OnClickClose()
	{
		Debug.Log("OnClickClose()");
		GameObject currentPanel = GetComponent<UIPanel>().gameObject;
		currentPanel.gameObject.SetActive(false);
	}
}
