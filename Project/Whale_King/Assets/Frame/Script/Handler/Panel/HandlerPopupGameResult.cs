using UnityEngine;
using System.Collections;

public class HandlerPopupGameResult : MonoBehaviour
{
	public UISprite imgNewRecord;	

	void Start ()
	{		
		imgNewRecord.gameObject.SetActive(false);		
		SetLabels();
	}

	void Update ()
	{
		//신기록 표시.
		if (GlobalValues.userValues.chanceUpdated==1)
			imgNewRecord.gameObject.SetActive(true);
	}
	
	protected void OnClickOK()
	{
		Debug.Log("OnClickOK()");
		GlobalAPIs.ShowLoadingScreen(true);
		Application.LoadLevel("Ready");
//		GameObject currentPanel = GetComponent<UIPanel>().gameObject;
//		currentPanel.gameObject.SetActive(false);
	}
	
	protected void SetLabels()
	{
		string[] friendName = new string[10];		//수정. 적용해야함...
		int[] friendFishes = new int[10];
		
		
		Debug.Log("::::::::::::::");
		Debug.Log("::::::::::::::");
		Debug.Log("::::::::::::::");
		Debug.Log("::::::::::::::");
		Debug.Log("::::::::::::::");
		
		int eatCount=0;
			//더 수정해야함.
			for(int i = 0; i < GlobalValues.friendListInGame.Count; i++) 
			{ 
				GlobalValues.GameContact contact = (GlobalValues.GameContact)GlobalValues.friendListInGame[i];
				
				
				if (contact.isDead == true)
				{
					Debug.Log("");
					Debug.Log("isDead = "+contact.isDead);
					Debug.Log("guid = "+contact.guid);
					Debug.Log("digestFishes = "+contact.digestFishes);
					Debug.Log("name = "+contact.name);
					Debug.Log("");
					friendName[eatCount] = contact.name;
					friendFishes[eatCount] = contact.digestFishes; 
					eatCount++;
					//contact.imageUrl				
				}				
			}
		
		Debug.Log("::::::::::::::");
		Debug.Log("::::::::::::::");
		Debug.Log("::::::::::::::");
		Debug.Log("::::::::::::::");
		Debug.Log("::::::::::::::");
			
			//contact.isDead <- 죽은 친구 물고기 확인, contact.guid <- guid, contact.digestFishes <- 소화중 물고기 수 } .
		
		
		UILabel[] labels = GetComponentsInChildren<UILabel>();
		foreach(UILabel label in labels)
		{
			if(label.name == "lblTotalScore")
			{
				label.text = GlobalValues.gamePlay.score.ToString("n0");
			}
			else if(label.name == "lblFishAcquire")
			{				
				label.text = GlobalValues.gamePlay.fishes.ToString("n0");
			}
			else if(label.name == "lblFishDigest")
			{
				label.text = GlobalValues.gamePlay.digestFishes.ToString("n0");
			}			
		}
		
	}
}
