using UnityEngine;
using System.Collections;

public class HandlerPopupNotice : MonoBehaviour
{
	void Start ()
	{
	}
	
	void Update ()
	{
	}
	
	void UpdateNotice()
	{
		Debug.Log("UpdateNotice");
		if(0 < GlobalValues.noticeList.Count)
		{
			GlobalValues.Board noticeItem = (GlobalValues.Board)GlobalValues.noticeList[0];

			UILabel[] labelChildren = GetComponentsInChildren<UILabel>();
			foreach(UILabel labelItem in labelChildren)
			{
				if(labelItem.name == "lblNoticeSubject")
				{
					labelItem.text = noticeItem.subject;
				}
				else if(labelItem.name == "lblNoticeContent")
				{
					labelItem.text = noticeItem.content;
				}
			}
			GlobalValues.noticeList.RemoveAt(0);
//			Debug.Log("Remain notice count:" + GlobalValues.noticeList.Count.ToString());
		}
	}

	void OnClickOK()
	{
		Debug.Log("OnClickOK()");
		GameObject currentPanel = GetComponent<UIPanel>().gameObject;
		currentPanel.gameObject.SetActive(false);
		
		GameObject[] allPanel = GameObject.FindGameObjectsWithTag("UIPanel");
		foreach(GameObject panel in allPanel)
		{
			if(panel.name == "PanelReady")
			{
				panel.SendMessage("ShowNotice");
				break;
			}
		}
	}
}
