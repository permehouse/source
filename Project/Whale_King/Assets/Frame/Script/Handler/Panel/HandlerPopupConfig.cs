using UnityEngine;
using System.Collections;

public class HandlerPopupConfig : MonoBehaviour
{
	public GameObject pageSetting;
	public GameObject pageMyRecord;
	public UIImageButton tabBtnSetting;
	public UIImageButton tabBtnMyRecord;
	public UISprite tabImgSetting;
	public UISprite tabImgMyRecord;

	//pageSetting
	public UIImageButton btnMusinOn;
	public UIImageButton btnMusinOff;
	public UIImageButton btnAlertOn;
	public UIImageButton btnAlertOff;
	
	public UIImageButton btnDragOn;
	public UIImageButton btnDragOff;
	public UIImageButton btnLowOn;
	public UIImageButton btnLowOff;
	
	//pageMyRecord
	
	private bool bSettingChanged;

	private enum PAGE_INDEX
	{
		PAGE_SETTING = 0,
		PAGE_MY_RECORD,
	}
	
	void OnEnable ()
	{
		//bSettingChanged = false;
		//SwitchPage(PAGE_INDEX.PAGE_SETTING);		
		//ApplyGlobalValues();
	}
	
	void Start ()
	{
		bSettingChanged = false;
		SwitchPage(PAGE_INDEX.PAGE_SETTING);	
	}
	
	

	void Update ()
	{
	}
	
	void ApplyGlobalValues()
	{
		Debug.Log("ApplyGlobalValues");
		ApplySettingMusicButtons();
		ApplySettingAlertButtons();
		ApplySettingDragButtons();
		ApplySettingLowButtons();
		Debug.Log("ApplyGlobalValues22");
		ApplyMyRecord();
		Debug.Log("ApplyGlobalValues33");
		SwitchPage(PAGE_INDEX.PAGE_SETTING);			//설정 화면 먼저 나오도록 수정.		
	}

	void OnClickClose()
	{
		Debug.Log("OnClickClose()");
		if(bSettingChanged)
		{
			ServerConnector.ServerConnectionFinished callbackStoreSettings = delegate(int result)
			{
				if(CommonDef.errorSuccessed == result)
				{
					bSettingChanged = false;
					GameObject currentPanel = GetComponent<UIPanel>().gameObject;
					currentPanel.gameObject.SetActive(false);
				}
				else
				{
					MessageBox.ShowMessageBox("Error:Store setting errorCode:" + result.ToString(), CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
				}
			};
			ServerConnector connector = ServerConnector.GetInstance();
			if(null != connector)
				connector.StoreSettings(callbackStoreSettings);
		}
		else
		{
			GameObject currentPanel = GetComponent<UIPanel>().gameObject;
			currentPanel.gameObject.SetActive(false);
		}
	}

	void OnClickTabSetting()
	{
		SwitchPage(PAGE_INDEX.PAGE_SETTING);
	}

	void OnClickTabMyRecord()
	{
		SwitchPage(PAGE_INDEX.PAGE_MY_RECORD);
	}

	private void SwitchPage(PAGE_INDEX indexPage)
	{
		pageSetting.SetActive(false);
		pageMyRecord.SetActive(false);
		tabBtnSetting.gameObject.SetActive(true);
		tabBtnMyRecord.gameObject.SetActive(true);
		tabImgSetting.gameObject.SetActive(false);
		tabImgMyRecord.gameObject.SetActive(false);

		switch(indexPage)
		{
		case PAGE_INDEX.PAGE_SETTING:
			pageSetting.SetActive(true);
			tabBtnSetting.gameObject.SetActive(false);
			tabImgSetting.gameObject.SetActive(true);
			break;
		case PAGE_INDEX.PAGE_MY_RECORD:
			pageMyRecord.SetActive(true);
			ApplyMyRecord();
			tabBtnMyRecord.gameObject.SetActive(false);
			tabImgMyRecord.gameObject.SetActive(true);
			break;
		}
	}

	void OnClickLogout()
	{
		Debug.Log("OnClickLogout()");
		ServerConnector.ServerConnectionFinished callbackLogout = delegate(int result)
		{
			if(CommonDef.errorSuccessed == result)
			{
				GameObject[] allPanel = GameObject.FindGameObjectsWithTag("UIPanel");
				foreach(GameObject panel in allPanel)
				{
					if(panel.name == "PanelReady")
					{
						panel.SendMessage("LogoutCompleted");
						break;
					}
				}
				bSettingChanged = false;
				GameObject currentPanel = GetComponent<UIPanel>().gameObject;
				currentPanel.gameObject.SetActive(false);
			}
			else
			{
				MessageBox.ShowMessageBox("Error:Logout errorCode:" + result.ToString(), CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
			}
		};
		ServerConnector connector = ServerConnector.GetInstance();
		if(null != connector)
			connector.Logout(callbackLogout);
	}
	
	void OnClickQuestion()
	{
		Debug.Log("OnClickQuestion()");
	}
	
	void OnClickRetire()
	{
		Debug.Log("OnClickRetire()");
	}
	
	void OnClickLow()
	{
		Debug.Log("On Low");
		bSettingChanged = true;
		GlobalValues.settings.lowQuality  = 1 - GlobalValues.settings.lowQuality;
		ApplySettingLowButtons();
	}
	
	void OnClickDrag()
	{
		Debug.Log("On Drag");
		bSettingChanged = true;
		GlobalValues.settings.dragController  = 1 - GlobalValues.settings.dragController;
		ApplySettingDragButtons();
	}
	
	void OnClickMusic()
	{
		Debug.Log("OnClickMusic()");
		bSettingChanged = true;
		GlobalValues.settings.soundPlay = 1 - GlobalValues.settings.soundPlay;
		ApplySettingMusicButtons();

		GameObject[] allPanel = GameObject.FindGameObjectsWithTag("UIPanel");
		foreach(GameObject panel in allPanel)
		{
			if(panel.name == "PanelReady")
			{
				panel.SendMessage("ApplySettingsBGM");
				break;
			}
		}
	}

	void OnClickAlert()
	{
		Debug.Log("OnClickAlert()");
		bSettingChanged = true;
		GlobalValues.settings.pushReceive = 1 - GlobalValues.settings.pushReceive;
		ApplySettingAlertButtons();
	}

	protected void ApplySettingMusicButtons()
	{
		btnMusinOn.gameObject.SetActive(0 != GlobalValues.settings.soundPlay);
		btnMusinOff.gameObject.SetActive(0 == GlobalValues.settings.soundPlay);
	}
	
	protected void ApplySettingAlertButtons()
	{
		btnAlertOn.gameObject.SetActive(0 != GlobalValues.settings.pushReceive);
		btnAlertOff.gameObject.SetActive(0 == GlobalValues.settings.pushReceive);
	}
	
	protected void ApplySettingDragButtons()
	{
		btnDragOn.gameObject.SetActive(0 != GlobalValues.settings.dragController);
		btnDragOff.gameObject.SetActive(0 == GlobalValues.settings.dragController);
	}
	
	protected void ApplySettingLowButtons()
	{
		btnLowOn.gameObject.SetActive(0 != GlobalValues.settings.lowQuality);
		btnLowOff.gameObject.SetActive(0 == GlobalValues.settings.lowQuality);
	}
	
	protected void ApplyMyRecord()
	{
		Debug.Log ("apply my reacode");
		
		UILabel[] labelChildren = GetComponentsInChildren<UILabel>();
		foreach(UILabel labelItem in labelChildren)
		{			
			Debug.Log ("foreach "+labelItem.name);
			
			if(labelItem.name == "lblTopScore")
			{
				labelItem.text = GlobalValues.gameUser.bestScore.ToString("n0");
			}
			if(labelItem.name == "lblTopDistance")
			{
				labelItem.text = GlobalValues.gameUser.bestDistance.ToString("n0");
			}						
			if(labelItem.name == "lblSumGold")
			{
				labelItem.text = GlobalValues.gameUser.totalEarnedFishes.ToString("n0");
			}
			if(labelItem.name == "lblSumTakeFish")
			{
				labelItem.text = GlobalValues.gameUser.totalInterceptFishes.ToString("n0");
			}
			if(labelItem.name == "lblSumLostFish")
			{
				Debug.Log ("Final = "+GlobalValues.gameUser.totalLoseFishes);
				Debug.Log ("Final = "+GlobalValues.gameUser.totalLoseFishes.ToString ("n0"));
				labelItem.text = GlobalValues.gameUser.totalLoseFishes.ToString("n0");
			}
			if(labelItem.name == "lblSumGame")
			{
				labelItem.text = GlobalValues.gameUser.totalGameCount.ToString("n0");
			}
		}
	}
}
