using UnityEngine;
using System.Collections;

public class HandlerPopupLogin : MonoBehaviour
{
	public UIInput inputEMail;

	void Start ()
	{
		inputEMail.text = GlobalValues.EMail;
	}

	void Update ()
	{
	
	}
	
	void OnClickOK()
	{
		GlobalValues.EMail = inputEMail.text;
		
		Debug.Log ("********************************************************************** Login : " + inputEMail.text);

		doLogin();
	}
	
	void OnClickClose()
	{
		GameObject currentPanel = GetComponent<UIPanel>().gameObject;
		currentPanel.gameObject.SetActive(false);
	}
	
	protected void doLogin()
	{
		ServerConnector.ServerConnectionFinished callback = delegate(int result)
		{
			if(CommonDef.errorSuccessed == result)
			{
				GlobalValues.saveConfigValues();

				GameObject currentPanel = GetComponent<UIPanel>().gameObject;
				currentPanel.gameObject.SetActive(false);
			}
		};

		ServerConnector connector = ServerConnector.GetInstance();
		if(null != connector)
			connector.Login(callback);
	}
}
