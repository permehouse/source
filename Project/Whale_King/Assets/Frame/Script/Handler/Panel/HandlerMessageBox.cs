using UnityEngine;
using System.Collections;

public class HandlerMessageBox : MonoBehaviour
{
	public UILabel lblMessage;
	public UIImageButton btnOK;
	public UIImageButton btnYes;
	public UIImageButton btnNo;
	
	private MessageBox.MessageBoxCallback callback;

	void Start ()
	{
	}
	
	void Update ()
	{
	}
	
	void OnClickOK()
	{
		GameObject currentPanel = GetComponent<UIPanel>().gameObject;
		currentPanel.gameObject.SetActive(false);
		if(callback != null)
			callback(CommonDef.MESSAGE_BOX_RESULT.MB_OK);
	}
	
	void OnClickYes()
	{
		GameObject currentPanel = GetComponent<UIPanel>().gameObject;
		currentPanel.gameObject.SetActive(false);
		if(callback != null)
			callback(CommonDef.MESSAGE_BOX_RESULT.MB_YES);
	}
	
	void OnClickNo()
	{
		GameObject currentPanel = GetComponent<UIPanel>().gameObject;
		currentPanel.gameObject.SetActive(false);
		if(callback != null)
			callback(CommonDef.MESSAGE_BOX_RESULT.MB_NO);
	}

	void ShowMessageBox(CommonDef.MessageBoxParam param)
	{
		Debug.Log("Handler" + param.strMessage + ", " + param.mbType.ToString());
		lblMessage.gameObject.SetActive(false);
		btnOK.gameObject.SetActive(false);
		btnYes.gameObject.SetActive(false);
		btnNo.gameObject.SetActive(false);

		callback = param.callback;

		if(param.strMessage != "")
		{
			lblMessage.gameObject.SetActive(true);
			lblMessage.text = param.strMessage;
		}
		
		switch(param.mbType)
		{
		case CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK:
			btnOK.gameObject.SetActive(true);
			break;
		case CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_YESNO:
			btnYes.gameObject.SetActive(true);
			btnNo.gameObject.SetActive(true);
			break;
		default:
			break;
		}
	}
}
