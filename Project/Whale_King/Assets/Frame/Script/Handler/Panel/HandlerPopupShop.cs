using UnityEngine;
using System.Collections;

public class HandlerPopupShop : MonoBehaviour
{
	public UILabel lblGold;
	public UILabel lblSapphire;
	
	public UILabel lblBItemBox;
	public UILabel lblBItemDigest;
	public UILabel lblBItemLRun;
	public UILabel lblBItemOctopus;
	public UILabel lblBItemRelay;
	public UILabel lblBItemSRun;
	public UILabel lblUItemCan;
	public UILabel lblUItemFish;
	public UILabel lblUItemHeart;
	public UILabel lblUItemHP;
	public UILabel lblUItemMagnet;
	public UILabel lblUItemUnbeatable;
	
	
//	public UISprite imgSelected;

	void Start ()
	{
		lblGold.gameObject.SetActive(true);
		//TODO solmea
		lblGold.text = "0";
		lblSapphire.gameObject.SetActive(true);
		lblSapphire.text = GlobalValues.gameUser.jewels.ToString("n0");

	//	imgSelected.gameObject.SetActive(true);
	}
	
	void OnEnable()
	{
		int zero = 0;	//임시.
		int one = 1;
		
		//차후 수정. 서버연동.
		lblUItemCan.text = ""+one.ToString();
		lblUItemFish.text = ""+one.ToString();
		lblUItemHeart.text = ""+one.ToString();
		lblUItemHP.text = ""+one.ToString();
		lblUItemMagnet.text = ""+one.ToString();
		lblUItemUnbeatable.text = ""+one.ToString();
		
		lblBItemBox.text = zero.ToString();
		lblBItemDigest.text = zero.ToString();
		lblBItemLRun.text = zero.ToString();
		lblBItemOctopus.text = zero.ToString();
		lblBItemRelay.text = zero.ToString();
		lblBItemSRun.text = zero.ToString();	
		
	}
	
	void Update ()
	{
	
	}
	
	protected void OnClickOK()
	{
		Debug.Log("Shop OnClickOK()");
		GameObject currentPanel = GetComponent<UIPanel>().gameObject;
		currentPanel.gameObject.SetActive(false);
	}
	
	protected void OnClickBuyGold()
	{
		Debug.Log("OnClickBuyGold()");
	}
	
	protected void OnClickBuySapphire()
	{
		Debug.Log("OnClickBuySapphire()");
	}
	
	
	
	
	
	
	
	
	
	// 업그레이드 버튼 클릭. 돈 만큼 줄어들고 업그레이드 증가함.
	protected void OnUpgradeItemClickOK()
	{
		Debug.Log("Upgrade Item OnClickOK()");
	}
	// 구매 버튼 클릭. 돈 만큼 줄어들고 개수 증가함.
	protected void OnBuyItemClickOK()
	{
		Debug.Log("Buy Item OnClickOK()");
	}
	
	
	// 업그레이드 형. 아이템.
	//물고기 보너스.
	protected void OnUItemFishClickOK()
	{
		Debug.Log("UItem Fish OnClickOK()");
	}	
	//체력 보너스.
	protected void OnUItemHPClickOK()
	{
		Debug.Log("UItem HP OnClickOK()");
	}
	//게임중 통조림.	
	protected void OnUItemCanClickOK()
	{
		Debug.Log("UItem Can OnClickOK()");
	}
	//게임중 하트.
	protected void OnUItemHeartClickOK()
	{
		Debug.Log("UItem Heart OnClickOK()");
	}
	//게임중 자석.
	protected void OnUItemMagnetClickOK()
	{
		Debug.Log("UItem Magnet OnClickOK()");
	}
	//게임중 무적.
	protected void OnUItemUnbeatableClickOK()
	{
		Debug.Log("UItem Unbeatable OnClickOK()");
	}
	
	
	
	
	
	
	// 구매형. 
	/* 아이템 버튼 클릭. */
	
	//소화제.
	protected void OnBItemDigestClickOK()
	{
		Debug.Log("BItem Digest OnClickOK()");
		/*
		// 캐릭터를 보유하고 있지 않을 경우.
		// 구입 시도.
		ServerConnector connector = ServerConnector.GetInstance();
		ServerConnector.ServerConnectionFinished callbackBuyBItemDigest = delegate(int result)
		{
			// 구입 성공.
			if(CommonDef.errorSuccessed == result) {															
				MessageBox.ShowMessageBox("Buy BItem Digest Success!!!", CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);				
			}
			else // 구입 실패.
			{
				MessageBox.ShowMessageBox("Error:Buy errorCode:" + result.ToString(), CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, null);
			}		
		};
		if(null != connector) {
			GlobalValues.userValues.chooseBuyGicode = "BITEM_DIGEST";
			connector.Buy(callbackBuyBItemDigest);
		}
		*/
		
	}	
	//처음 런.
	protected void OnBItemSRunClickOK()
	{
		Debug.Log("BItem SRun OnClickOK()");
	}
	//라스트 런.	
	protected void OnBItemLRunClickOK()
	{
		Debug.Log("BItem LRun OnClickOK()");
	}
	//이어 달리기.
	protected void OnBItemRelayClickOK()
	{
		Debug.Log("BItem Relay OnClickOK()");
	}
	// 문어 호출.
	protected void OnBItemOctopusClickOK()
	{
		Debug.Log("BItem Octopus OnClickOK()");
	}
	// 아이템 박스.
	protected void OnBItemBoxClickOK()
	{
		Debug.Log("BItem Box OnClickOK()");
	}
	
	

}
