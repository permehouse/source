using UnityEngine;
using System.Collections;

public class HandlerPanelGameEnd : MonoBehaviour
{
	public UIPanel panelGameResult;

	void Start ()
	{
		panelGameResult.gameObject.SetActive(true);

		EndGame();
	}

	void Update ()
	{
	}

	protected void EndGame()
	{
		Debug.Log(string.Format("Game Ended, GPID:{0}, Fishes:{1}, Score:{2}, Distance:{3}", GlobalValues.gamePlay.gpid, GlobalValues.gamePlay.fishes, GlobalValues.gamePlay.score, GlobalValues.gamePlay.distance));

		if(0 == GlobalValues.gamePlay.gpid)
		{
			MessageBox.MessageBoxCallback callbackMessage = delegate(CommonDef.MESSAGE_BOX_RESULT resultMessage)
			{
				Application.LoadLevel("Ready");
			};
			MessageBox.ShowMessageBox("Invalid game play ID", CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, callbackMessage);

			return;
		}

		ServerConnector.ServerConnectionFinished callbackEndGame = delegate(int result)
		{
			if(CommonDef.errorSuccessed == result)
			{
				//Do nothing...
			}
			else
			{
				MessageBox.MessageBoxCallback callbackMessage = delegate(CommonDef.MESSAGE_BOX_RESULT resultMessage)
				{
				//	Application.LoadLevel("Ready");
				};
				MessageBox.ShowMessageBox("Error EndGame\nCode:" + result.ToString(), CommonDef.MESSAGE_BOX_TYPE.MB_TYPE_OK, callbackMessage);
			}
		};

		ServerConnector connector = ServerConnector.GetInstance();
		if(null != connector)
			connector.EndGame(callbackEndGame);
	}
}
