using UnityEngine;
using System.Collections;

public class HandlerPopupMailBox : MonoBehaviour
{
	public GameObject pageMessage;
	public GameObject pageMission;
	public GameObject pageEvent;
	public UIImageButton tabBtnMessage;
	public UIImageButton tabBtnMission;
	public UIImageButton tabBtnEvent;
	public UISprite tabImgMessage;
	public UISprite tabImgMission;
	public UISprite tabImgEvent;
	
	private enum PAGE_INDEX
	{
		PAGE_MESSAGE = 0,
		PAGE_MISSION,
		PAGE_EVENT,
	}

	void Start ()
	{
		SwitchPage(PAGE_INDEX.PAGE_MESSAGE);
	}
	
	void Update ()
	{
	
	}
	
	void OnClickClose()
	{
		Debug.Log("OnClickClose()");
		GameObject currentPanel = GetComponent<UIPanel>().gameObject;
		currentPanel.gameObject.SetActive(false);
	}
	
	void OnClickTabMessage()
	{
		SwitchPage(PAGE_INDEX.PAGE_MESSAGE);
	}
	
	void OnClickTabMission()
	{
		SwitchPage(PAGE_INDEX.PAGE_MISSION);
	}
	
	void OnClickTabEvent()
	{
		SwitchPage(PAGE_INDEX.PAGE_EVENT);
	}
	
	private void SwitchPage(PAGE_INDEX indexPage)
	{
		pageMessage.SetActive(false);
		pageMission.SetActive(false);
		pageEvent.SetActive(false);
		tabBtnMessage.gameObject.SetActive(true);
		tabBtnMission.gameObject.SetActive(true);
		tabBtnEvent.gameObject.SetActive(true);
		tabImgMessage.gameObject.SetActive(false);
		tabImgMission.gameObject.SetActive(false);
		tabImgEvent.gameObject.SetActive(false);
			
		switch(indexPage)
		{
		case PAGE_INDEX.PAGE_MESSAGE:
			pageMessage.SetActive(true);
			tabBtnMessage.gameObject.SetActive(false);
			tabImgMessage.gameObject.SetActive(true);
			break;
		case PAGE_INDEX.PAGE_MISSION:
			pageMission.SetActive(true);
			tabBtnMission.gameObject.SetActive(false);
			tabImgMission.gameObject.SetActive(true);
			break;
		case PAGE_INDEX.PAGE_EVENT:
			pageEvent.SetActive(true);
			tabBtnEvent.gameObject.SetActive(false);
			tabImgEvent.gameObject.SetActive(true);
			break;
		}
	}
}
