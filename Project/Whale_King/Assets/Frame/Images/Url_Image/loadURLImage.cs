using UnityEngine;
using System.Collections;

public class loadURLImage : MonoBehaviour {

   //Material을 Asign합니다.
    public Material tmpTexture;
	public UITexture textureURL;
		
	void Start () {	
       // StartCoroutine(Init());
		
	}
	// 요청 에의해 서차 트그려.줌.
	public void DrawChart(string inputUrl) {
		
		//Debug.Log ("URL : "+inputUrl);		
        StartCoroutine(Init(inputUrl));		
	}	

	// Update is called once per frame
    IEnumerator Init(string url)
    {   		
		string inputUrl = ServerConnector.GetInstance().strDomainName + url;
        //웹서버를 연결합니다.
        WWW www = new WWW(inputUrl);

        //데이터 수신이 끝날때까지 대기합니다.
	//	Debug.Log("return www");
        yield return www;
		
		Debug.Log ("new URL = "+inputUrl);

        //메터리얼의 메인Texture를 다운로드 받은 Texture로 변경합니다.
        tmpTexture.mainTexture = www.texture;
	//	Debug.Log("Textture www textture");
		//gameObject.Getcomponent<UITexture>().enabled = false;
		//gameObject.Getcomponent<UITexture>().enabled = true;
		textureURL.GetComponent<loadURLImage>().gameObject.SetActive(false);		//수정. 자기 자신 좀더 간략화.
		textureURL.GetComponent<loadURLImage>().gameObject.SetActive(true);		
    }
}

	