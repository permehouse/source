using UnityEngine;
using System.Collections;

public class GameOutput : MonoBehaviour {
	static public int nScore;
	static public int nDistance;
	
	static public bool bEndGame = false;
	static public bool bEscapeGame = false;
}
