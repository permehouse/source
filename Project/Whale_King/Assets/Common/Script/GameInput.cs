using UnityEngine;
using System.Collections;

public class GameInput : MonoBehaviour {
	public struct PlayerProperty {
		public Player.PlayerType nPlayerType;
	}
	
	public struct PetProperty {
		public Pet.PetType nPetType;
	}
	
	static public PlayerProperty _Player;
	static public PetProperty _Pet;
//	static public PetProperty _Pet2;
}
