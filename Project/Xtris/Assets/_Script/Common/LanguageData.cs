﻿using UnityEngine;
using System.Collections;

public class LanguageData {
    public static string LANGUAGE_ENGLISH = "English";
    public static string LANGUAGE_RUSSIAN = "Russian";
    public static string LANGUAGE_FRANCH = "French";
    public static string LANGUAGE_SPANISH = "Spanish";
    public static string LANGUAGE_GERMAN = "German";
    public static string LANGUAGE_SIMPLIFIED_CHINESE = "SimplifiedChinese";
    public static string LANGUAGE_TRADITIONAL_CHINESE = "TraditionalChinese";
    public static string LANGUAGE_KOREAN = "Korean";
    public static string LANGUAGE_JAPANESE = "Japanese";
    public static string LANGUAGE_DEFAULT = LANGUAGE_ENGLISH;

    public static string DISPLAY_ENGLISH = "ENGLISH";
    public static string DISPLAY_RUSSIAN = "РУССКИЙ";
    public static string DISPLAY_FRANCH = "FRANÇAIS";
    public static string DISPLAY_SPANISH = "ESPAÑOL";
    public static string DISPLAY_GERMAN = "DEUTSCH";
    public static string DISPLAY_SIMPLIFIED_CHINESE = "正体字";
    public static string DISPLAY_TRADITIONAL_CHINESE = "正體字";
    public static string DISPLAY_KOREAN = "한국어";
    public static string DISPLAY_JAPANESE = "日本語";

    public static string strLanguage = LANGUAGE_DEFAULT;

    // Set Default Language : English
    public static string strTitleX = "P";
    public static string strTitle = "lanetris";
    public static string strSubTitle = "";

    public static string strContinue = "CONTINUE";
    public static string strNewPlay = "START NEW GAME";

    public static string strPlayEarth = "PLAY EARTH";
    public static string strPlayMars = "PLAY MARS";
    public static string strPlayJupiter = "PLAY JUPITER";

    public static string strLanguagePanelTitle = "LANGUAGE";

    public static string strBest = "BEST";
    public static string strNewBest = "NEW !!";
    public static string strGameMode_Normal = "NORMAL";
    public static string strGameMode_Challenge = "CHALLENGE";

    public static string strPlanetName_Earth = "EARTH";
    public static string strPlanetName_Mars = "MARS";
    public static string strPlanetName_Jupiter = "JUPITER";

    public static string strRound = "ROUND";
    public static string strFinalRound = "FINAL ROUND";

    public static string strHowToPlay = "HOW TO PLAY";
    public static string strHowToPlay_Image1_Title = "BASIC PLAY";
    public static string strHowToPlay_Image1_SubTitle1 = "1. Rotate";
    public static string strHowToPlay_Image1_SubTitle2 = "2. Drop";
    public static string strHowToPlay_Image1_SubTitle3 = "3. Remove More\n    Than 2 Lines";
    public static string strHowToPlay_Image2_Title = "MOVE CONTROLS";
    public static string strHowToPlay_Image2_SubTitle1 = "1. Move";
    public static string strHowToPlay_Image2_SubTitle2 = "2. Avoid Blocks";
    public static string strHowToPlay_Image2_SubTitle3 = "3. Catch Stars";
    public static string strHowToPlay_Image3_SubTitle1 = "1. Find\n   Unsuitable Places";
    public static string strHowToPlay_Image3_SubTitle2 = "2. Drop";
    public static string strHowToPlay_Image3_SubTitle3 = "3. Break !!";
    public static string strHowToPlay_Touch = "Touch";
    public static string strHowToPlay_Drag = "Drag";
    public static string strHowToPlay_NextPage = "Next Page";
    public static string strHowToPlay_Close= "Close";

    // GamePlay
    public static string strSpeed = "SPEED";
    public static string strScore = "SCORE";
    public static string strStart = "START";
    public static string strComplete = "COMPLETE";
    public static string strRoundClear = "ROUND\nCLEAR";
    public static string strTimeOver = "TIME\nOVER";
    public static string strCombo = "COMBO";
    public static string strGood = "GOOD";
    public static string strGreat = "GREAT";
    public static string strPerfect = "PERFECT";
    public static string strBreakThemAll = "BREAK ALL";
    public static string strRoundScore = "ROUND SCORE";
    public static string strBonusScore = "TIME BONUS";
    public static string strTotalScore = "TOTAL SCORE";
    public static string strNextPlanet_Earth = "GO TO EARTH";
    public static string strNextPlanet_Mars = "GO TO MARS";
    public static string strNextPlanet_Jupiter = "GO TO JUPITER";
    public static string strNextRound = "NEXT ROUND";
    public static string strRestart = "RESTART";
    public static string strResume = "RESUME";
    public static string strQuit = "QUIT";
    public static string strReplayRecord = "START RECORDING REPLAY";
    public static string strReplayShare = "WATCH AND SHARE REPLAY";

    public static bool IsSupportedLanguage(string strLanguage)
    {
        if (strLanguage == LANGUAGE_ENGLISH) return true;
        else if (strLanguage == LANGUAGE_FRANCH) return true;
        else if (strLanguage == LANGUAGE_GERMAN) return true;
        else if (strLanguage == LANGUAGE_SPANISH) return true;
        else if (strLanguage == LANGUAGE_RUSSIAN) return true;
        else if (strLanguage == LANGUAGE_SIMPLIFIED_CHINESE) return true;
        else if (strLanguage == LANGUAGE_TRADITIONAL_CHINESE) return true;
        else if (strLanguage == LANGUAGE_KOREAN) return true;
        else if (strLanguage == LANGUAGE_JAPANESE) return true;

        return false;
    }

    public static void SetLanguage(string strNewLanguage)
    {
        strLanguage = strNewLanguage;

        if (strLanguage == LANGUAGE_FRANCH) SetLanguage_French();
        else if (strLanguage == LANGUAGE_GERMAN) SetLanguage_German();
        else if (strLanguage == LANGUAGE_SPANISH) SetLanguage_Spanish();
        else if (strLanguage == LANGUAGE_RUSSIAN) SetLanguage_Russian();
        else if (strLanguage == LANGUAGE_SIMPLIFIED_CHINESE) SetLanguage_SimplifiedChinese();
        else if (strLanguage == LANGUAGE_TRADITIONAL_CHINESE) SetLanguage_TraditionalChinese();
        else if (strLanguage == LANGUAGE_KOREAN) SetLanguage_Korean();
        else if (strLanguage == LANGUAGE_JAPANESE) SetLanguage_Japanese();
        else SetLanguage_English(); // Default = English
    }

    public static void SetLanguage_English()
    {
        // Main
        strTitleX = "P";
        strTitle = "lanetris";
        strSubTitle = "";

        strContinue = "CONTINUE";
        strNewPlay = "START NEW GAME";

        strPlayEarth = "PLAY EARTH";
        strPlayMars = "PLAY MARS";
        strPlayJupiter = "PLAY JUPITER";

        strLanguagePanelTitle = "LANGUAGE";

        strBest = "BEST";
        strNewBest = "NEW !!";
        strGameMode_Normal = "NORMAL";
        strGameMode_Challenge = "CHALLENGE";

        strPlanetName_Earth = "EARTH";
        strPlanetName_Mars = "MARS";
        strPlanetName_Jupiter = "JUPITER";

        strRound = "ROUND";
        strFinalRound = "FINAL ROUND";

        strHowToPlay = "HOW TO PLAY";
        strHowToPlay_Image1_Title = "BASIC PLAY";
        strHowToPlay_Image1_SubTitle1 = "1. Rotate";
        strHowToPlay_Image1_SubTitle2 = "2. Drop";
        strHowToPlay_Image1_SubTitle3 = "3. Remove More\n    Than 2 Lines";
        strHowToPlay_Image2_Title = "MOVE CONTROLS";
        strHowToPlay_Image2_SubTitle1 = "1. Move";
        strHowToPlay_Image2_SubTitle2 = "2. Avoid Blocks";
        strHowToPlay_Image2_SubTitle3 = "3. Catch Stars";
        strHowToPlay_Image3_SubTitle1 = "1. Find\n   Unsuitable Places";
        strHowToPlay_Image3_SubTitle2 = "2. Drop";
        strHowToPlay_Image3_SubTitle3 = "3. Break !!";
        strHowToPlay_Touch = "Touch";
        strHowToPlay_Drag = "Drag";
        strHowToPlay_NextPage = "Next Page";
        strHowToPlay_Close= "Close";

        // GamePlay
        strSpeed = "SPEED";
        strScore = "SCORE";
        strStart = "START";
        strComplete = "COMPLETE";
        strRoundClear = "ROUND\nCLEAR";
        strTimeOver = "TIME\nOVER";
        strCombo = "COMBO";
        strGood = "GOOD";
        strGreat = "GREAT";
        strPerfect = "PERFECT";
        strBreakThemAll = "BREAK ALL";
        strRoundScore = "ROUND SCORE";
        strBonusScore = "TIME BONUS";
        strTotalScore = "TOTAL SCORE";
        strNextPlanet_Earth = "GO TO EARTH";
        strNextPlanet_Mars = "GO TO MARS";
        strNextPlanet_Jupiter = "GO TO JUPITER";
        strNextRound = "NEXT ROUND";
        strRestart = "RESTART";
        strResume = "RESUME";
        strQuit = "QUIT";
        strReplayRecord = "START RECORDING REPLAY";
        strReplayShare = "WATCH AND SHARE REPLAY";
    }

    public static void SetLanguage_French()
    {
        strTitleX = "P";
        strTitle = "lanètris";
        strSubTitle = "";

        strContinue = "CONTINUER";
        strNewPlay = "NOUVEAU JEU";

        strPlayEarth = "JOUER TERRE";
        strPlayMars = "JOUER MARS";
        strPlayJupiter = "JOUER JUPITER";

        strLanguagePanelTitle = "LANGUE";

        strBest = "MEILLEURS";
        strNewBest = "NOUVEAU !!";
        strGameMode_Normal = "NORMAL";
        strGameMode_Challenge = "DÉFI";

        strPlanetName_Earth = "TERRE";
        strPlanetName_Mars = "MARS";
        strPlanetName_Jupiter = "JUPITER";

        strRound = "ROUND";
        strFinalRound = "FINAL ROUND";

        strHowToPlay = "CONTRÔLE DE JEU";
        strHowToPlay_Image1_Title = "JEU DE BASE";
        strHowToPlay_Image1_SubTitle1 = "1. Tourner";
        strHowToPlay_Image1_SubTitle2 = "2. Goutte";
        strHowToPlay_Image1_SubTitle3 = "3. Retirer Plus\n    De 2 Lignes";
        strHowToPlay_Image2_Title = "CONTRÔLE DE MOUVEMENT";
        strHowToPlay_Image2_SubTitle1 = "1. Mouvement";
        strHowToPlay_Image2_SubTitle2 = "2. Évitez Les Blocs";
        strHowToPlay_Image2_SubTitle3 = "3. Étoiles De Capture";
        strHowToPlay_Image3_SubTitle1 = "1. Trouver Des Endroits\n   Inappropriés";
        strHowToPlay_Image3_SubTitle2 = "  2. Goutte";
        strHowToPlay_Image3_SubTitle3 = "3. Briser !!";
        strHowToPlay_Touch = "Toucher";
        strHowToPlay_Drag = "Traîner";
        strHowToPlay_NextPage = "Page Suivante";
        strHowToPlay_Close = "Fermer";

        // GamePlay
        strSpeed = "SPEED";
        strScore = "SCORE";
        strStart = "START";
        strComplete = "ACHÈVEMENT";
        strRoundClear = "ROUND\nCLEAR";
        strTimeOver = "TIME\nOVER";
        strCombo = "COMBO";
        strGood = "BIEN";
        strGreat = "GRAND";
        strPerfect = "PARFAIT";
        strBreakThemAll = "TOUT CASSER";
        strRoundScore = "ROUND SCORE";
        strBonusScore = "TIME BONUS";
        strTotalScore = "TOTAL SCORE";
        strNextPlanet_Earth = "ALLER À LA TERRE";
        strNextPlanet_Mars = "ALLER VERS MARS";
        strNextPlanet_Jupiter = "ALLER À JUPITER";
        strNextRound = "PROCHAIN ROUND";
        strRestart = "REDÉMARRAGE";
        strResume = "REPRENDRE";
        strQuit = "QUITTER";
        strReplayRecord = "COMMENCER L'ENREGISTREMENT VIDÉO";
        strReplayShare = "REGARDER ET PARTAGER DES VIDÉO";
    }
    public static void SetLanguage_German()
    {
        // Main
        strTitleX = "P";
        strTitle = "lanetris";
        strSubTitle = "";

        strContinue = "FORTSETZEN";
        strNewPlay = "STARTET NEUES SPIEL";

        strPlayEarth = "SPIELEN ERDE";
        strPlayMars = "SPIELEN MARS";
        strPlayJupiter = "SPIELEN JUPITER";

        strLanguagePanelTitle = "SPRACHE";

        strBest = "BESTE";
        strNewBest = "NEUE !!";
        strGameMode_Normal = "NORMAL";
        strGameMode_Challenge = "MISSION";

        strPlanetName_Earth = "ERDE";
        strPlanetName_Mars = "MARS";
        strPlanetName_Jupiter = "JUPITER";

        strRound = "ROUND";
        strFinalRound = "FINAL ROUND";

        strHowToPlay = "SPIELSTEUERUNG";
        strHowToPlay_Image1_Title = "GRUNDSPIEL";
        strHowToPlay_Image1_SubTitle1 = "1. Rotieren";
        strHowToPlay_Image1_SubTitle2 = "2. Fallen";
        strHowToPlay_Image1_SubTitle3 = "3. Zu Entfernen mehr\n    Als 2 Zeilen";
        strHowToPlay_Image2_Title = "BEWEGUNGSSTEUERUNG";
        strHowToPlay_Image2_SubTitle1 = "1. Umzug";
        strHowToPlay_Image2_SubTitle2 = "2. Vermeiden Blöcke";
        strHowToPlay_Image2_SubTitle3 = "3. Fang Sternen";
        strHowToPlay_Image3_SubTitle1 = "1. Finden Unpassende\n   OrteFind";
        strHowToPlay_Image3_SubTitle2 = "2. Fallen";
        strHowToPlay_Image3_SubTitle3 = "3. Brechen !!";
        strHowToPlay_Touch = "Berühren";
        strHowToPlay_Drag = "Ziehen";
        strHowToPlay_NextPage = "Folgeseite";
        strHowToPlay_Close = "Verlassen";

        // GamePlay
        strSpeed = "SPEED";
        strScore = "SCORE";
        strStart = "START";
        strComplete = "ABSCHLUSS";
        strRoundClear = "ROUND\nCLEAR";
        strTimeOver = "TIME\nOVER";
        strCombo = "COMBO";
        strGood = "GUT";
        strGreat = "GROß";
        strPerfect = "PERFEKT";
        strBreakThemAll = "BRECHEN ALLES";
        strRoundScore = "ROUND SCORE";
        strBonusScore = "TIME BONUS";
        strTotalScore = "TOTAL SCORE";
        strNextPlanet_Earth = "ZUM ERDE";
        strNextPlanet_Mars = "ZUM MARS";
        strNextPlanet_Jupiter = "ZUM JUPITER";
        strNextRound = "NÄCHSTE ROUND";
        strRestart = "NEUSTART";
        strResume = "FORTSETZEN";
        strQuit = "VERLASSEN";
        strReplayRecord = "STARTEN DER VIDEOAUFNAHME";
        strReplayShare = "BEOBACHTEN UND TEILEN VIDEO";
    }
    public static void SetLanguage_Spanish()
    {
        strTitleX = "P";
        strTitle = "lanetris";
        strSubTitle = "";

        strContinue = "CONTINUAR";
        strNewPlay = "INICIAR NUEVO JUEGO";

        strPlayEarth = "JUGAR TIERRA";
        strPlayMars = "JUGAR MARS";
        strPlayJupiter = "JUGAR JÚPITER";

        strLanguagePanelTitle = "IDIOMA";

        strBest = "MEJOR";
        strNewBest = "NUEVO !!";
        strGameMode_Normal = "NORMAL";
        strGameMode_Challenge = "DESAFÍO";

        strPlanetName_Earth = "TIERRA";
        strPlanetName_Mars = "MARS";
        strPlanetName_Jupiter = "JÚPITER";

        strRound = "ROUND";
        strFinalRound = "FINAL ROUND";

        strHowToPlay = "CONTROLES DE JUEGO";
        strHowToPlay_Image1_Title = "JUEGO BÁSICO";
        strHowToPlay_Image1_SubTitle1 = "1. Rotar";
        strHowToPlay_Image1_SubTitle2 = "2. Soltar";
        strHowToPlay_Image1_SubTitle3 = "3. Eliminar Más\n    De 2 Líneas";
        strHowToPlay_Image2_Title = "CONTROL DE MOVIMIENTO";
        strHowToPlay_Image2_SubTitle1 = "1. Movida";
        strHowToPlay_Image2_SubTitle2 = "2. Evite Bloques";
        strHowToPlay_Image2_SubTitle3 = "3. Tomar Estrellas";
        strHowToPlay_Image3_SubTitle1 = "1. Encuentra Llugares\n   No Adecuados";
        strHowToPlay_Image3_SubTitle2 = "2. Soltar";
        strHowToPlay_Image3_SubTitle3 = "3. Romper !!";
        strHowToPlay_Touch = "Tocar";
        strHowToPlay_Drag = "Arrastrar";
        strHowToPlay_NextPage = "Pagina Siguiente";
        strHowToPlay_Close = "Dejar";

        // GamePlay
        strSpeed = "SPEED";
        strScore = "SCORE";
        strStart = "START";
        strComplete = "nTERMINACIÓN";
        strRoundClear = "ROUND\nCLEAR";
        strTimeOver = "TIME\nOVER";
        strCombo = "COMBO";
        strGood = "BIEN";
        strGreat = "GRAN";
        strPerfect = "PERFECTO";
        strBreakThemAll = "ROMPER TODO";
        strRoundScore = "ROUND SCORE";
        strBonusScore = "TIME BONUS";
        strTotalScore = "TOTAL SCORE";
        strNextPlanet_Earth = "IR A TIERRA";
        strNextPlanet_Mars = "IR A MARS";
        strNextPlanet_Jupiter = "IR A JÚPITER";
        strNextRound = "PROXIMA ROUND";
        strRestart = "EMPEZAR DE NUEVO";
        strResume = "REANUDAR";
        strQuit = "DEJAR";
        strReplayRecord = "INICIAR LA GRABACIÓN DE VÍDEO";
        strReplayShare = "VER Y COMPARTIR VÍDEO";
    }
    public static void SetLanguage_Russian()
    {
        strTitleX = "P";
        strTitle = "lanetris";
        strSubTitle = "ПЛАНЕТРИС";

        strContinue = "ПРОДОЛЖАТЬ";
        strNewPlay = "НАЧАТЬ НОВУЮ ИГРУ";

        strPlayEarth = "ИГРАТЬ ЗЕМЛЯ";
        strPlayMars = "ИГРАТЬ МАРС";
        strPlayJupiter = "ИГРАТЬ ЮПИТЕР";

        strLanguagePanelTitle = "ЯЗЫК";

        strBest = "ЛУЧШАЯ";
        strNewBest = "НОВЫЙ !!";
        strGameMode_Normal = "НОРМАЛЬНЫЙ";
        strGameMode_Challenge = "ВЫЗОВ";

        strPlanetName_Earth = "ЗЕМЛЯ";
        strPlanetName_Mars = "МАРС";
        strPlanetName_Jupiter = "ЮПИТЕР";

        strRound = "ROUND";
        strFinalRound = "FINAL ROUND";

        strHowToPlay = "ИГРА УПРАВЛЕНИЯ";
        strHowToPlay_Image1_Title = "ОСНОВНАЯ ИГРА";
        strHowToPlay_Image1_SubTitle1 = "1. Вращать";
        strHowToPlay_Image1_SubTitle2 = "2. Падение";
        strHowToPlay_Image1_SubTitle3 = "3. Удалить Более \n    2 Линии";
        strHowToPlay_Image2_Title = "ДВИЖЕНИЕ";
        strHowToPlay_Image2_SubTitle1 = "1. Шаг";
        strHowToPlay_Image2_SubTitle2 = "2. Избежать Блоков";
        strHowToPlay_Image2_SubTitle3 = "3. Поймать Звезды";
        strHowToPlay_Image3_SubTitle1 = "1. Найти\n   Неподходящих Местах";
        strHowToPlay_Image3_SubTitle2 = "2. Падение";
        strHowToPlay_Image3_SubTitle3 = "3. Перерыв !!";
        strHowToPlay_Touch = "Коснуться";
        strHowToPlay_Drag = "Тянуть";
        strHowToPlay_NextPage = "Следущая Страница";
        strHowToPlay_Close = "Оставлять";

        // GamePlay
        strSpeed = "SPEED";
        strScore = "SCORE";
        strStart = "START";
        strComplete = "ЗАВЕРШЕНИЕ";
        strRoundClear = "ROUND\nCLEAR";
        strTimeOver = "TIME\nOVER";
        strCombo = "COMBO";
        strGood = "GOOD";
        strGreat = "GREAT";
        strPerfect = "PERFECT";
        strBreakThemAll = "ПЕРЕРЫВ ВСЕ";
        strRoundScore = "ROUND SCORE";
        strBonusScore = "TIME BONUS";
        strTotalScore = "TOTAL SCORE";
        strNextPlanet_Earth = "ПЕРЕЙТИ К ЗЕМЛЕ";
        strNextPlanet_Mars = "ПЕРЕЙТИ К МАРС";
        strNextPlanet_Jupiter = "ПЕРЕЙТИ К ЮПИТЕРУ";
        strNextRound = "СЛЕДУЮЩИЙ ROUND";
        strRestart = "ПЕРЕЗАПУСК";
        strResume = "ПРОДОЛЖИТЬ";
        strQuit = "ОСТАВЛЯТЬ";
        strReplayRecord = "НАЧАТЬ ЗАПИСЬ ВИДЕО";
        strReplayShare = "СМОТРЕТЬ И ДЕЛИТЬСЯ ВИДЕО";
    }
    public static void SetLanguage_SimplifiedChinese()
    {
        strTitleX = "P";
        strTitle = "lanetris";
        strSubTitle = "";

        strContinue = "继续";
        strNewPlay = "开始一个新游戏";

        strPlayEarth = "地球挑战";
        strPlayMars = "火星挑战";
        strPlayJupiter = "木星挑战";

        strLanguagePanelTitle = "语言";

        strBest = "最高纪录";
        strNewBest = "新纪录 !!";
        strGameMode_Normal = "一般";
        strGameMode_Challenge = "星球挑战";

        strPlanetName_Earth = "地球";
        strPlanetName_Mars = "火星";
        strPlanetName_Jupiter = "木星";

        strRound = "ROUND";
        strFinalRound = "FINAL ROUND";

        strHowToPlay = "游戏控制";
        strHowToPlay_Image1_Title = "基本方法";
        strHowToPlay_Image1_SubTitle1 = "1. 旋转";
        strHowToPlay_Image1_SubTitle2 = "2. 降";
        strHowToPlay_Image1_SubTitle3 = "3. 除去超过2行";
        strHowToPlay_Image2_Title = "移动控制";
        strHowToPlay_Image2_SubTitle1 = "1. 移动";
        strHowToPlay_Image2_SubTitle2 = "2. 避免块";
        strHowToPlay_Image2_SubTitle3 = "3. 抓住一个明星";
        strHowToPlay_Image3_SubTitle1 = "1. 查找不匹配的地方";
        strHowToPlay_Image3_SubTitle2 = "2. 降";
        strHowToPlay_Image3_SubTitle3 = "3. 打碎 !!";
        strHowToPlay_Touch = "触摸";
        strHowToPlay_Drag = "拖累";
        strHowToPlay_NextPage = "下一页";
        strHowToPlay_Close = "终了";

        // GamePlay
        strSpeed = "SPEED";
        strScore = "SCORE";
        strStart = "START";
        strComplete = "完了";
        strRoundClear = "ROUND\nCLEAR";
        strTimeOver = "TIME\nOVER";
        strCombo = "COMBO";
        strGood = "GOOD";
        strGreat = "GREAT";
        strPerfect = "PERFECT";
        strBreakThemAll = "打破一切";
        strRoundScore = "ROUND SCORE";
        strBonusScore = "TIME BONUS";
        strTotalScore = "TOTAL SCORE";
        strNextPlanet_Earth = "转到地球";
        strNextPlanet_Mars = "转到火星";
        strNextPlanet_Jupiter = "转到木星";
        strNextRound = "接下来 ROUND";
        strRestart = "重新启动";
        strResume = "继续";
        strQuit = "终了";
        strReplayRecord = "开始录制视频";
        strReplayShare = "观看和分享视频";
    }
    public static void SetLanguage_TraditionalChinese()
    {
        strTitleX = "P";
        strTitle = "lanetris";
        strSubTitle = "";

        strContinue = "繼續";
        strNewPlay = "開始一個新遊戲";

        strPlayEarth = "地球挑戰";
        strPlayMars = "火星挑戰";
        strPlayJupiter = "木星挑戰";

        strLanguagePanelTitle = "語言";

        strBest = "最高紀錄";
        strNewBest = "新紀錄 !!";
        strGameMode_Normal = "一般";
        strGameMode_Challenge = "星球挑戰";

        strPlanetName_Earth = "地球";
        strPlanetName_Mars = "火星";
        strPlanetName_Jupiter = "木星";

        strRound = "ROUND";
        strFinalRound = "FINAL ROUND";

        strHowToPlay = "遊戲控制";
        strHowToPlay_Image1_Title = "基本方法";
        strHowToPlay_Image1_SubTitle1 = "1. 旋轉";
        strHowToPlay_Image1_SubTitle2 = "2. 降";
        strHowToPlay_Image1_SubTitle3 = "3. 除去超過2行";
        strHowToPlay_Image2_Title = "移動控制";
        strHowToPlay_Image2_SubTitle1 = "1. 移動";
        strHowToPlay_Image2_SubTitle2 = "2. 避免塊";
        strHowToPlay_Image2_SubTitle3 = "3. 抓住一個明星";
        strHowToPlay_Image3_SubTitle1 = "1. 查找不匹配的地方";
        strHowToPlay_Image3_SubTitle2 = "2. 降";
        strHowToPlay_Image3_SubTitle3 = "3. 打碎 !!";
        strHowToPlay_Touch = "觸摸";
        strHowToPlay_Drag = "拖累";
        strHowToPlay_NextPage = "下一頁";
        strHowToPlay_Close = "终了";

        // GamePlay
        strSpeed = "SPEED";
        strScore = "SCORE";
        strStart = "START";
        strComplete = "完了";
        strRoundClear = "ROUND\nCLEAR";
        strTimeOver = "TIME\nOVER";
        strCombo = "COMBO";
        strGood = "GOOD";
        strGreat = "GREAT";
        strPerfect = "PERFECT";
        strBreakThemAll = "打破一切";
        strRoundScore = "ROUND SCORE";
        strBonusScore = "TIME BONUS";
        strTotalScore = "TOTAL SCORE";
        strNextPlanet_Earth = "轉到地球";
        strNextPlanet_Mars = "轉到火星";
        strNextPlanet_Jupiter = "轉到木星";
        strNextRound = "接下來 ROUND";
        strRestart = "重新啟動";
        strResume = "繼續";
        strQuit = "终了";
        strReplayRecord = "開始錄製視頻";
        strReplayShare = "觀看和分享視頻";
    }

    public static void SetLanguage_Korean()
    {
        strTitleX = "P";
        strTitle = "lanetris";
        strSubTitle = "플라네트리스";

        strContinue = "이어서 하기";
        strNewPlay = "새로운 게임 시작";

        strPlayEarth = "지구 도전";
        strPlayMars = "화성 도전";
        strPlayJupiter = "목성 도전";

        strLanguagePanelTitle = "언어";

        strBest = "최고기록";
        strNewBest = "신기록 !!";
        strGameMode_Normal = "일반";
        strGameMode_Challenge = "챌린지";

        strPlanetName_Earth = "지구";
        strPlanetName_Mars = "화성";
        strPlanetName_Jupiter = "목성";

        strRound = "ROUND";
        strFinalRound = "FINAL ROUND";

        strHowToPlay = "게임방법";
        strHowToPlay_Image1_Title = "기본 플레이";
        strHowToPlay_Image1_SubTitle1 = "1. 돌리고";
        strHowToPlay_Image1_SubTitle2 = "2. 끼워넣어";
        strHowToPlay_Image1_SubTitle3 = "3. 2개 이상의 라인을 제거";
        strHowToPlay_Image2_Title = "이동 조작";
        strHowToPlay_Image2_SubTitle1 = "1. 이동시켜서";
        strHowToPlay_Image2_SubTitle2 = "2. 블럭을 피하거나";
        strHowToPlay_Image2_SubTitle3 = "3. 별을 획득";
        strHowToPlay_Image3_SubTitle1 = "1. 맞지 않는 곳을 찾아";
        strHowToPlay_Image3_SubTitle2 = "2. 떨어뜨려서";
        strHowToPlay_Image3_SubTitle3 = "3. 격파 !!";
        strHowToPlay_Touch = "터치";
        strHowToPlay_Drag = "드래그";
        strHowToPlay_NextPage = "다음 페이지";
        strHowToPlay_Close = "닫기";

        // GamePlay
        strSpeed = "SPEED";
        strScore = "SCORE";
        strStart = "START";
        strComplete = "완료";
        strRoundClear = "ROUND\nCLEAR";
        strTimeOver = "TIME\nOVER";
        strCombo = "콤보";
        strGood = "GOOD";
        strGreat = "GREAT";
        strPerfect = "PERFECT";
        strBreakThemAll = "모두 다 격파";
        strRoundScore = "ROUND SCORE";
        strBonusScore = "TIME BONUS";
        strTotalScore = "TOTAL SCORE";
        strNextPlanet_Earth = "지구로 이동";
        strNextPlanet_Mars = "화성으로 이동";
        strNextPlanet_Jupiter = "목성으로 이동";
        strNextRound = "다음 ROUND";
        strRestart = "다시 시작";
        strResume = "계속 플레이";
        strQuit = "나가기";
        strReplayRecord = "비디오 녹화 시작";
        strReplayShare = "비디오 확인 및 공유";
    }

    public static void SetLanguage_Japanese()
    {
        // Main
        strTitleX = "P";
        strTitle = "lanetris";
        strSubTitle = "プラネットリース";

        strContinue = "続行";
        strNewPlay = "新しいゲームの開始";

        strPlayEarth = "地球挑戦";
        strPlayMars = "火星挑戦";
        strPlayJupiter = "木星挑戦";

        strLanguagePanelTitle = "言語";

        strBest = "最高記録";
        strNewBest = "新記録 !!";
        strGameMode_Normal = "一般";
        strGameMode_Challenge = "チャレンジ";

        strPlanetName_Earth = "地球";
        strPlanetName_Mars = "火星";
        strPlanetName_Jupiter = "木星";

        strRound = "ROUND";
        strFinalRound = "FINAL ROUND";

        strHowToPlay = "ゲームコントロール";
        strHowToPlay_Image1_Title = "基本プレイ";
        strHowToPlay_Image1_SubTitle1 = "1. 回転";
        strHowToPlay_Image1_SubTitle2 = "2. ドロップ";
        strHowToPlay_Image1_SubTitle3 = "3. 2つ以上のラインを削除";
        strHowToPlay_Image2_Title = "移動制御";
        strHowToPlay_Image2_SubTitle1 = "1. 移動";
        strHowToPlay_Image2_SubTitle2 = "2. ブロックを避ける";
        strHowToPlay_Image2_SubTitle3 = "3. 星をキャッチ";
        strHowToPlay_Image3_SubTitle1 = "1. 一致していない\n   ところを探して";
        strHowToPlay_Image3_SubTitle2 = "2. ドロップ";
        strHowToPlay_Image3_SubTitle3 = "3. 撃破 !!";
        strHowToPlay_Touch = "タッチ";
        strHowToPlay_Drag = "ドラッグ";
        strHowToPlay_NextPage = "次のページ";
        strHowToPlay_Close = "閉じる";

        // GamePlay
        strSpeed = "SPEED";
        strScore = "SCORE";
        strStart = "START";
        strComplete = "完了";
        strRoundClear = "ROUND\nCLEAR";
        strTimeOver = "TIME\nOVER";
        strCombo = "コンボ";
        strGood = "GOOD";
        strGreat = "GREAT";
        strPerfect = "PERFECT";
        strBreakThemAll = "全て破壊";
        strRoundScore = "ROUND SCORE";
        strBonusScore = "TIME BONUS";
        strTotalScore = "TOTAL SCORE";
        strNextPlanet_Earth = "地球に移動";
        strNextPlanet_Mars = "火星に移動";
        strNextPlanet_Jupiter = "木星に移動";
        strNextRound = "次のROUND";
        strRestart = "リスタート";
        strResume = "続行";
        strQuit = "終了";
        strReplayRecord = "ビデオ録画を開始";
        strReplayShare = "ビデオ確認と共有";
    }
}
