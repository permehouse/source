﻿using UnityEngine;
using System.Collections;

public class ReplayManager : MonoBehaviour {
    bool bReadyForRecording = false;
    bool bSetToRecord = false;
    bool bRecording = false;
    bool bPaused = false;
    bool bRecordingStopped = false;
    bool bRecordingCancel = false;

    bool bShowSharingModal = false;

    bool bWaitingForRecordingReady = false;
    bool bWaitingForRecordingStarted = false;
    bool bWaitingForRecordingStopped = false;

    float nTime_WaitingForRecordingReady = 0f;

    static ReplayManager _this;
    public static ReplayManager GetInstance()
    {
        return _this;
    }

    void Awake()
    {
        _this = this;

        Reset();

        bWaitingForRecordingReady = true;
        nTime_WaitingForRecordingReady = GameData.REPLAY_WAITING_FOR_RECORDING_READY_TIME;

        Everyplay.ReadyForRecording += OnReadyForRecording;
        Everyplay.RecordingStarted += OnRecordingStarted;
        Everyplay.RecordingStopped += OnRecordingStopped;
        Everyplay.UploadDidStart += OnUploadDidStart;
    }

    void OnDestroy()
    {
        Everyplay.ReadyForRecording -= OnReadyForRecording;
        Everyplay.RecordingStarted -= OnRecordingStarted;
        Everyplay.RecordingStopped -= OnRecordingStopped;
        Everyplay.UploadDidStart -= OnUploadDidStart;
    }

    void Update()
    {
        CheckTime_WaitingForRecordingReady();
    }

    void CheckTime_WaitingForRecordingReady()
    {
        if(bWaitingForRecordingReady) {
            nTime_WaitingForRecordingReady -= Time.deltaTime;

            if (nTime_WaitingForRecordingReady <= 0f)
            {
                bWaitingForRecordingReady = false;
                nTime_WaitingForRecordingReady = 0f;
            }
        }
    }
   

    public void Reset()
    {
        bSetToRecord = false;
        bRecording = false;
        bRecordingStopped = false;
        bRecordingCancel = false;
        bPaused = false;
        bShowSharingModal = false;

        bWaitingForRecordingStarted = false;
        bWaitingForRecordingStopped = false;
    }

    public void SetToRecord(bool bSet = true)
    {
        if (bSet && IsReadyForRecording())
        {
            bSetToRecord = true;
        }
        else
        {
            bSetToRecord = false;
        }
    }

    public void OnReadyForRecording(bool enabled)
    {
        bWaitingForRecordingReady = false;
        bReadyForRecording = enabled;
    }

    public void OnRecordingStarted()
    {
        if (bRecordingCancel)
        {
            Reset();
            return;
        }

        bWaitingForRecordingStarted = false;
        bRecording = true;
    }

    public void OnRecordingStopped()
    {
        if (bRecordingCancel)
        {
            Reset();
            return;
        }

        bWaitingForRecordingStopped = false;
        bRecording = false;
        bRecordingStopped = true;

        if (bShowSharingModal)
        {
            ShowSharingModal();
        }
    }

    public void OnUploadDidStart(int videoId)
    {
        // When an upload started, be ready to record again.
        Reset();

        AnalyticsManger.GetInstance().SendAnalyticShareReplay();
    }

    public void StartRecording()
    {
        if (bSetToRecord || IsReadyForRecording())
        {
            bWaitingForRecordingStarted = true;
            Everyplay.StartRecording();
        }
    }
    public void PauseRecording()
    {
        if (bRecording && !bPaused)
        {
            bPaused = true;
            Everyplay.PauseRecording();
        }
    }

    public void ResumeRecording()
    {
        if (bRecording && bPaused)
        {
            bPaused = false;
            Everyplay.ResumeRecording();
        }
    }

    public void StopRecording()
    {
        if (bRecording)
        {
            bRecording = false;
            bWaitingForRecordingStopped = true;
            Everyplay.StopRecording();
        }
    }

    public void CancelRecording()
    {
        if (bRecording)
        {
            bRecordingCancel = true;
            StopRecording();
        }
        else if (bWaitingForRecordingStopped || bWaitingForRecordingStarted)
        {
            bRecordingCancel = true;
        }
        else {
            Reset();
        }
    }

    public void ShareReplay()
    {
        if (bRecordingStopped)
        {
            ShowSharingModal();
        }
        else
        {
            bShowSharingModal = true;
            StopRecording();
        }
    }

    public bool IsReadyForRecording()
    {
        if (bWaitingForRecordingReady || bSetToRecord || bWaitingForRecordingStarted || bRecording || bWaitingForRecordingStopped || bRecordingStopped)
        {
            // don't allow multiple recording
            return false;
        }

        return Everyplay.IsRecordingSupported();
/*
        if (bWaitingForRecordingReady && !bReadyForRecording) { 
            bReadyForRecording = Everyplay.IsRecordingSupported();
        }
        return bReadyForRecording;
*/ 
    }

    public bool IsSetToRecord()
    {
        return bSetToRecord;
    }

    public bool IsRecording()
    {
        return bRecording;
    }

    public bool IsRecordingStopped()
    {
        return bRecordingStopped;
    }

    public bool IsPaused()
    {
        return bPaused;
    }

    void ShowSharingModal()
    {
        string strGameMode = "";
        if (GameData.nGameMode == GameMode.CHALLENGE)
        {
            strGameMode = GameData.GetGameModeName(GameData.nGameMode) + " ";
        }

        string strPlanet = GameData.GetPlanetName(GameData.nPlanetType);
        string strRound = GameData.GetRoundName(GameData.nGameMode, GameData.nPlanetType, GameData.nRound);
        string strScore = GameData.nScore.ToString("N0");

        string strRemark = "";
        if (GameData.nBestComboCount >= GameData.REMARK_COMBO_COUNT)
        {
            strRemark = GameData.nBestComboCount.ToString("0") + " Combo ";
        }
        if (GameData.nBestSpeed >= GameData.REMARK_SPEED)
        {
            strRemark += GameData.nBestSpeed.ToString("0") + " Speed ";
        }

        // Make meta data
        Everyplay.SetMetadata("gamemode", strGameMode);
        Everyplay.SetMetadata("planet", strPlanet);
        Everyplay.SetMetadata("round", strRound);
        Everyplay.SetMetadata("score", strScore);
        Everyplay.SetMetadata("remark", strRemark);

        Everyplay.ShowSharingModal();
    }

    public string GetDebugString()
    {
        string str;
        str = "bReadyForRecording : " + bReadyForRecording + "\n";
        str = "IsReadyForRecording : " + IsReadyForRecording() + "\n";
        str += "Everyplay.IsRecordingSupported() : " + Everyplay.IsRecordingSupported() + "\n";
        str += "bWaitingForRecordingReady : " + bWaitingForRecordingReady + "\n";
        str += "bWaitingForRecordingStarted : " + bWaitingForRecordingStarted + "\n";
        str += "bWaitingForRecordingStopped : " + bWaitingForRecordingStopped + "\n";
        str += "bSetToRecord : " + bSetToRecord + "\n";
        str += "bRecording : " + bRecording + "\n";
        str += "bPaused : " + bPaused + "\n";
        str += "bRecordingStopped : " + bRecordingStopped + "\n";
        str += "bRecordingCancel : " + bRecordingCancel + "\n";
        str += "bShowSharingModal : " + bShowSharingModal;

        return str;
    }
}
