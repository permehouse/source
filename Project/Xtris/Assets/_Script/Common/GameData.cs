﻿using UnityEngine;
using System.Collections;

public enum BrickType
{
    BRICK_TYPE_NORMAL,
    BRICK_TYPE_DUMMY,
    BRICK_TYPE_DUMMY_HOLLOW,
    BRICK_TYPE_COUNT
}

public enum BlockType
{
    BLOCK_TYPE_O = 0,
    BLOCK_TYPE_P,
    BLOCK_TYPE_C,
    BLOCK_TYPE_B,
    BLOCK_TYPE_J,
    BLOCK_TYPE_T,
    BLOCK_TYPE_L,
    BLOCK_TYPE_S,
    BLOCK_TYPE_Z,
    BLOCK_TYPE_COUNT
}

public enum BlockAngle
{
    BLOCK_ANGLE_0 = 0,
    BLOCK_ANGLE_90,
    BLOCK_ANGLE_180,
    BLOCK_ANGLE_270,
    BLOCK_ANGLE_COUNT
}

public enum PlanetType
{
    PLANET_TYPE_NONE = -1,
    PLANET_TYPE_EARTH = 0,
    PLANET_TYPE_MARS,
    PLANET_TYPE_JUPITER,
    PLANET_TYPE_COUNT,
    PLANET_TYPE_FIRST = PLANET_TYPE_EARTH,
    PLANET_TYPE_LAST = PLANET_TYPE_JUPITER
}

public enum GameStatus {
    NONE = -1,
    MAIN,		
    GAME_PLAY,	
	DEFAULT = NONE
}

public enum PlayStatus
{
    NONE = -1,
    INIT_COMPONENT,
    INIT_PLANET,
    START_PLANET,
    START_ROUND,
    READY,
    PLAY,
    ENDING,
    RESULT,
    REWARD,
    AFTER_REWARD,
    END_ROUND,
    END_PLANET,
    QUIT,
    DESTROY_COMPONENT,
    PAUSE,
    RESUME,
    RESTART_ROUND,
    DEFAULT = NONE
}

public enum PlayEvent
{
    NONE = -1,
    BLOCK_PASS,
    BLOCK_DROP,
    BLOCK_DROP_SUCCESS,
    BLOCK_DROP_FAIL,
    BLOCK_FIT_TO_CELL,
    BLOCK_HIT_TO_CAMERA,
    CELL_BREAK,
    CELL_ARRIVE,
    CELL_AWAY,
    STAR_HIT,
    ROUND_COUNT_DOWN,
    ROUND_COUNT_ZERO,
    SCORE_COUNTING,
    BUTTON_CLICK,
    DEFAULT = NONE
}
public enum GameMode
{
    NONE = -1,
    NORMAL,
    CHALLENGE
}

public enum PlayMode
{
    NORMAL,
    BREAK_THEM_ALL
}

public enum BlockMode
{
    EASY = 0,
    MID,
    HARD,
    HOLLOW
}

public enum MatchResult
{
    FAIL,
    GOOD,
    GREAT,
    PERFECT
}

public enum RoundResult
{
    NONE,
    ROUND_CLEAR,
    TIME_OVER
}

public class GameData {
    public static string strVersion = "1.0.04";
    public static GameMode nGameMode = GameMode.NORMAL;
    public static GameStatus nGameStatus = GameStatus.DEFAULT;
    public static PlayStatus nPlayStatus = PlayStatus.DEFAULT;
    public static PlayStatus nPlayStatusOnPause = PlayStatus.DEFAULT;
    public static PlayStatus nPlayStatusAfterReward = PlayStatus.DEFAULT;
    public static PlayEvent nPlayEvent = PlayEvent.DEFAULT;
    public static PlanetType nPlanetType = PlanetType.PLANET_TYPE_EARTH;
    public static MatchResult nMatchResult = MatchResult.FAIL;
    public static RoundResult nRoundResult = RoundResult.NONE;
    public static int nRound = 1;       // should be set 1 in order to run GamePlay for test
    public static int nMaxRound = 0;
    public static float nRoundReadyTime = 0f;
    public static float nPlayTime = 0;
    public static int nScore = 0;
    public static int nRoundScore = 0;
    public static int nBonusScore = 0;
    public static int nComboCount = 0;
    public static int nBestScore = 0;
    public static int nBestSpeed = 0;
    public static int nBestComboCount = 0;
    public static int nPlanetCount = (int)PlanetType.PLANET_TYPE_COUNT;
    public static float nPlanetStartDistance = 0f;
    public static float nPlanetEndDistance = 0f;
    public static Color dPlanetColor;
    public static Vector3 dBackgroundScale;
    public static Vector3 posCameraCloseView;
    public static Vector3 posCameraRewardView;
    public static bool bShowLogo = false;
    public static string strPlatform;

    // Brick
    public static float BRICK_SIZE = 0.8f;
    public static float BRICK_BREAK_TIME = 2f;
    public static float BRICK_DISCOVER_DELAY = 0.1f;
    public static float BRICK_BLINK_MAX_ALPHA = 0.5f;

    // Block
    public const float BLOCK_START_DISTANCE = 50.0f;
    public const float BLOCK_MOVE_LERP_RATE = 0.5f;
    public const float BLOCK_ROTATE_SLERP_RATE = 0.3f;


    // Block Matrix
    public static int MAX_BLOCK_MATRIX_SIZE_X = 3;
    public static int MAX_BLOCK_MATRIX_SIZE_Y = 2;
    public static float BLOCK_PASS_START_DISTANCE = 10f;


    // Cell Matrix
    public static int MAX_CELL_MATRIX_SIZE = 4;
/*
    public static int MAX_CELL_MATRIX_SIZE_X = MAX_CELL_MATRIX_SIZE;
    public static int MAX_CELL_MATRIX_SIZE_Y_FOR_BLOCK = 3;
    public static int MAX_CELL_MATRIX_SIZE_Y = MAX_CELL_MATRIX_SIZE + MAX_CELL_MATRIX_SIZE_Y_FOR_BLOCK;
*/

    // Block Zone
    public static float BLOCK_ZONE_SIZE = BRICK_SIZE * MAX_CELL_MATRIX_SIZE;

    // Cell
    public static float CELL_SIZE = MAX_CELL_MATRIX_SIZE * BRICK_SIZE;
    public static float MAX_CELL_INIT_MOVE_DISTANCE = BLOCK_ZONE_SIZE + BRICK_SIZE;
    public static float MAX_CELL_INIT_MOVE_DELAY = 1f;
    public static float MAX_CELL_MOVE_MIN_DELAY = 1f;
    public static float MAX_CELL_MOVE_MAX_DELAY = 5f;
    public static float MAX_CELL_MOVE_DISTANCE = BLOCK_ZONE_SIZE + 4 * BRICK_SIZE;
    public static float CELL_INIT_MOVE_SPEED = 50f;
    public static float CELL_ENDING_MOVE_SPEED = 100f;
    public static float CELL_BREAK_DELAY = 2f;
    public static float CELL_FLIP_DELAY = 1f;
    public static float CELL_BROKEN_EFFECT_PLAY_TIME = 2f;

    public static int VALID_MATCH_LINES = 2;
    public static int CONFLICT_POS_X = (MAX_CELL_MATRIX_SIZE / 2) - 1;
    public static int CONFLICT_POS_Y = 0;

    // Camera
    public static float CAMERA_CELL_DISTANCE = 8f;
    public static float CAMERA_MOVE_DISTANCE = 2.5f;
    public static float CAMERA_SHAKE_TIME = 1f;
    public static float CAMERA_SHAKE_SPEED = 30f;
    public static float CAMERA_SHAKE_MAX_DISTANCE = 0.5f;
    public static float MAINCAMERA_MOVE_SPEED = 0.2f;
    public static float CAMERA_BASE_FOV = 60f;
    public static float CAMERA_BASE_SCREEN_WIDTH = 1280f;
    public static float CAMERA_BASE_SCREEN_HEIGHT = 720f;
    public static float CAMERA_BASE_SCREEN_RATIO = CAMERA_BASE_SCREEN_HEIGHT / CAMERA_BASE_SCREEN_WIDTH;

    // Game Play
    public static int GAMEPLAY_ROUND_COUNT = 3;

    public static float BACKGROUND_DISTANCE = 1000f;

    public static float GAMEPLAY_SPEED_UP_RATE = 0.1f;
    public static float GAMEPLAY_SPEED_DOWN_RATE = 0.02f;
    public static float GAMEPLAY_SPEED_UP_RATE_ON_MATCH_RESULT_GREAT = 0.12f;
    public static float GAMEPLAY_SPEED_UP_RATE_ON_MATCH_RESULT_PERFECT = 0.15f;
    public static float GAMEPLAY_SPEED_DOWN_RATE_ON_HIT_TO_CAMERA = 0.1f;

    // Playground
    public static float PLAYGROUND_START_PLANET_MOVE_LERP_RATE = 0.15f;
    public static float PLAYGROUND_START_PLANET_SHOW_TIME = 2f;

    // Background
    public static float BACKGROUND_MOVE_SCALE = 10f;
    public static float BACKGROUND_DISTANCE_REDUCE_RATE = 0.25f;

    // Controller
    public static float CONTROLLER_MIN_DRAG_SIZE = 20f;
    public static float CONTROLLER_MAX_DRAG_SIZE = 100f;

    // Camera
    public static float CAMERA_MOVE_SPEED = 10f;

    // Star
    public static float STAR_START_DISTANCE = 400f;
    public static float STAR_CURVE_DISTANCE = 100f;
    public static float STAR_ROTATE_MIN_SPEED = 100f;
    public static float STAR_ROTATE_MAX_SPEED = 500f;
    public static float STAR_MOVE_MIN_SPEED = 15f;
    public static float STAR_MOVE_MAX_SPEED = 30f;
    public static float STAR_MOVE_LERP_RATE = 0.5f;
    public static float STAR_SPAWN_MIN_DELAY = 3f;
    public static float STAR_SPAWN_MAX_DELAY = 6f;
    public static float STAR_PREDICT_DISTANCE_RATE = 10f;
    public static float STAR_DEST_RANGE = CAMERA_MOVE_DISTANCE * 1.5f;
    public static float STAR_MOVE_SPEED_RATE_IN_MAIN_SCENE = 1.2f;
    public static float STAR_SPAWN_DISTANCE_IN_MAIN_SCENE = 200f;
    public static float STAR_SPAWN_TIME_IN_MAIN_SCENE = 3f * 60f;       // spawn every 3 minuites

    // Score
    public static float SCORE_BOOST_TIME = 1f;
    public static float SCORE_BOOST_GOOD = 2f;
    public static float SCORE_BOOST_GREATE = 3f;
    public static float SCORE_BOOST_PERFECT = 4f;
    public static int SCORE_BOOST_MIN_COMBO_COUNT = 3;
    public static float SCORE_BOOST_COMBO_RATE = 0.1f;

    // Indicator Bar
    public static float DISTANCE_INDICATOR_POS_X = -8f;
    public static float DISTANCE_INDICATOR_POS_Y = 5f;
    public static float DISTANCE_INDICATOR_BAR_HEIGHT = 0.3f;
    public static float DISTANCE_INDICATOR_BAR_SIZE = 0.2f;
    public static float DISTANCE_INDICATOR_BAR_GAP = DISTANCE_INDICATOR_BAR_HEIGHT * 6f;
    public static int DISTANCE_INDICATOR_BAR_COUNT = 50;
    public static float DISTANCE_INDICATOR_BAR_COLOR = 0.5f;
    public static float DISTANCE_INDICATOR_BAR_ALPHA = 0.1f;
    public static float DISTANCE_INDICATOR_FLAG_ALPHA = 0.5f;

    // UI
    public static float UI_SPEED_SCALE_TIME = 1f;
    public static float UI_SPEED_SCALE_RATE = 0.2f;
    public static float UI_TEXT_ALPHA = 0.25f;
    public static float UI_TEXT_HALF_ALPHA = 0.5f;
    public static float UI_TEXT_HIGH_ALPHA = 0.8f;
    public static float UI_BUTTON_BLINK_ALPHA = 0.25f;
    public static float UI_BUTTON_BLINK_SPEED = 0.25f;
    public static float UI_BUTTON_BLINK_FAST_SPEED = 0.75f;
    public static Color UI_BUTTON_COLOR = new Color(0f, 0f, 0f, 1f);
    public static Color UI_BUTTON_DISABLE_COLOR = new Color(0.5f, 0.06f, 0.06f, 0.5f);
    public static float UI_ROUND_READY_TIME_DISTANCE_RATE = 30f;
    public static float UI_BREAK_THEM_ALL_SCALE_RATE = 0.05f;
    public static float UI_WARM_HOLE_BUTTON_SPEED = 150f;
    public static float UI_BACK_BUTTON_SPEED = 150f;
    public static Color UI_ROUND_CLEAR_COLOR = new Color(0.8f, 0.7f, 0.08f, 1f);
    public static Color UI_TIME_OVER_COLOR = new Color(0.92f, 0f, 0f, 1f);
    public static float UI_COMBO_SCALE_RATE = 0.08f;
    public static float UI_RESULT_SHOW_TIME = 1f;

    // Screen Image
    public static Color SCREEN_IMAGE_DAMAGE_COLOR = new Color(1f, 0f, 0f, 0.5f);
    public static float SCREEN_IMAGE_DAMAGE_COLOR_LERP_RATE = 0.3f;
    public static Color SCREEN_IMAGE_FADE_IN_COLOR = Color.clear;
    public static float SCREEN_IMAGE_FADE_IN_COLOR_LERP_RATE = 1f;
    public static Color SCREEN_IMAGE_FADE_OUT_COLOR = Color.black;
    public static float SCREEN_IMAGE_FADE_OUT_COLOR_LERP_RATE = 6f;
    public static float SCREEN_IMAGE_FADE_OUT_TIME = 0.7f;
    public static float SCREEN_IMAGE_LOADING_TEXT_FADE_IN_COLOR_LERP_RATE = 1.5f;
 
    // Object Name
    public static string OBJECT_NAME_PLAYGROUND = "Playground";
    public static string OBJECT_NAME_CAMERA = "Camera";
    public static string OBJECT_NAME_UI = "UI";
    public static string OBJECT_NAME_UI3D = "UI3D";
    public static string OBJECT_NAME_BACKGROUND = "Background";
    public static string OBJECT_NAME_DISTANCE_INDICATOR = "DistanceIndicator";
    public static string OBJECT_NAME_CELLS = "Cells";
    public static string OBJECT_NAME_BLOCKS = "Blocks";
    public static string OBJECT_NAME_BRICKS = "Bricks";
    public static string OBJECT_NAME_STARS = "Stars";
    public static string OBJECT_NAME_SCREEN_IMAGE = "ScreenImage";
    public static string OBJECT_NAME_SOUND_MANAGER = "SoundManager";
    public static string OBJECT_NAME_CONTROLLER = "Controller";

    // Round
    public static float ROUND_READY_TIME = 4f;
    public static float ROUND_ENDING_TIME = 3.5f;
    public static float ROUND_ENDING_BROKEN_TIME = 2f;
    public static float ROUND_ENDING_PLAYGROUND_ROTATE_TIME = 2f;
    public static float ROUND_ENDING_PLAYGROUND_ROTATE_RATE = 80f;
    public static float ROUND_ENDING_DISAPPEAR_TIME = 1.5f;
    public static float ROUND_ENDING_SCORE_TIME = 1.5f;

    // Title
    public static float TITLE_LOGO_ALPHA = 0.75f;

    // Resume
    public static float RESUME_DELAY = 0.5f;

    // Result
    public static float RESULT_SHOW_TIME = 4f;
    public static float RESULT_SCORE_OUTSIDE_POS_X = 200f;
    public static float RESULT_SCORE_OUTSIDE_POS_GAP = 300f;
    public static float RESULT_SCORE_MOVE_SPEED = 400f;

    // Reward
    public static float REWARD_STAR_SPEED_RATE = 3f;
    public static float AFTER_REWARD_DELAY = 1.5f;

    // Logo
    public static float LOGO_SHOW_TIME = 3f;
    public static float LOGO_SHOW_TIME_FOR_IOS = 5f;

    // How to play
    public static int HOWTOPLAY_IMAGE_COUNT = 3;

    // StarCoin
    public static int STARCOIN_MAX_COUNT = 100;
    public static int STARCOIN_BONUS_SCORE = 10000;
    public static float STARCOIN_DISPLAY_CHANGE_SPEED = 15f;

    // Adjust Measurement Unit
    public static float ADJUST_UNIT_SCORE_RATE = 10f;
    public static float ADJUST_UNIT_SPEED_RATE = 100f;
    public static float ADJUST_UNIT_DISTANCE_RATE = 100f;

    // Remark
    public static int REMARK_COMBO_COUNT = 20;
    public static int REMARK_SPEED = 8000;

    // Sound
    public static float SOUND_BGM_VOLUMN = 0.6f;

    // Title Logo
    public static int TITLE_LOGO_SHOW_CLICK_COUNT = 7;

    // Replay
    public static float REPLAY_WAITING_FOR_RECORDING_READY_TIME = 3f;

    // For showing first play ads
    public class RoundAds
    {
        public static bool bValidFirstPlay = false;
        public static int nValidNextRound = 0;
        public static int ROUND_COUNT_AFTER_FULL_VIDEO_ADS = 4;
        public static int ROUND_COUNT_AFTER_VIDEO_ADS = 3;
    }

    public class StartCondition
    {
        public static bool bNeedReward = false;
        public static int nReward = 0;
        public static int nStarPrice = 0;
    }

    public class SpawnObjectPool {
        public static int STAR_POOL_SIZE = 50;
        public static int INDICATOR_BAR_POOL_SIZE = DISTANCE_INDICATOR_BAR_COUNT;
        // double of all bricks in 4 cells
        public static int BRICK_POOL_SIZE = 2 * (4 * GameData.MAX_CELL_MATRIX_SIZE * GameData.MAX_CELL_MATRIX_SIZE);
        // double of 2 bricks in 4 cells
        public static int BRICK_HOLLOW_POOL_SIZE =  2 * (4 * 2);
        public static int BLOCK_POOL_SIZE = 4;
        public static int CELL_POOL_SIZE = 4;
    }

    public class StarPrice
    {
        public static int REWARD = 30;
        public static int RESTART = 10;
        public static int CONTINUE = 10;
        public static int PLAY_EARTH = 10;
        public static int PLAY_MARS = 10;
        public static int PLAY_JUPITER = 10;
    }

    // Player Preference Data
    public class Prefs {
        public static bool bLoaded = false;
        public static int nPrefCount = 17;

        public static string strLanguage = "";
        public static int nStarCoin = 30;
        public static int nGamePlayCount = 0;
        public static int nNewPlayCount = 0;
        public static int nRoundPlayCount = 0;
        public static bool bSoundOn = true;
        public static bool bCanContinue = false;
        public static GameMode nLastGameMode = GameMode.NORMAL;
        public static PlanetType nLastPlanetType = PlanetType.PLANET_TYPE_EARTH;
        public static int nLastRound = 1;
        public static int nBestScore = 100000;
        public static int nEarthBestScore = 30000;
        public static int nMarsBestScore = 50000;
        public static int nJupiterBestScore = 100000;
        public static bool bEarthLock = true;
        public static bool bMarsLock = true;
        public static bool bJupiterLock = true;
    }

    // Round Data
    public class RoundData
    {
        public static int nMaxRound;
        public static PlayMode nPlayMode;
        public static BlockMode nBlockMode;

        public static float nStartDistance;
        public static float nEndDistance;
        public static float nBlockStartDistance;
        public static float nPlayTime;
        public static float nSpeed;
        public static float nRotateSpeed;
        public static float nCellMaxMoveDistance;
        public static float nCellMoveSpeed;
        public static float nCellMoveDepth;
    }

    // Game Data for Earth
    public class Earth
    {
        public static Vector3 dSlope = new Vector3(0, 0, -23f);
        public static float nRotateSpeed = 10f;
        public static Vector3 posCameraCloseView = new Vector3(-200f, 0f, -400f);
        public static Vector3 posCameraRewardView = new Vector3(2000f, 1200f, -2000f);
        public static Vector3 dBackgroundScale = new Vector3(1300f, 1300f, 1f);
        public static Color dPlanetColor = new Color(0.18f, 0.92f, 0.99f, 1f);

        public class Normal
        {
            public const int nMaxRound = 3;

            public static PlayMode[] listPlayMode = new PlayMode[nMaxRound] { PlayMode.NORMAL, PlayMode.NORMAL, PlayMode.NORMAL };
            public static BlockMode[] listBlockMode = new BlockMode[nMaxRound] { BlockMode.EASY, BlockMode.MID, BlockMode.HOLLOW };

            public static float[] listDistance = new float[nMaxRound + 1] { 3100f, 2600f, 2000f, 500f };
            public static float[] listPlayTime = new float[nMaxRound] { 60f, 60f, 100f };
            public static float[] listSpeed = new float[nMaxRound] { 8f, 10f, 10f };
            public static float[] listRotateSpeed = new float[nMaxRound] { 2f, 2f, 3f };
            public static float[] listBlockDistance = new float[nMaxRound] { 50f, 50f, 60f };
            public static float[] listCellMoveMaxDistance = new float[nMaxRound] { 0f, 1f, 2f };
            public static float[] listCellMoveSpeed = new float[nMaxRound] { 0f, 2f, 4f };
            public static float[] listCellMoveDepth = new float[nMaxRound] { 0.1f, 0.2f, 0.3f };
        }

        public class Challenge
        {
            public const int nMaxRound = 3;

            public static PlayMode[] listPlayMode = new PlayMode[nMaxRound] { PlayMode.NORMAL, PlayMode.NORMAL, PlayMode.NORMAL };
            public static BlockMode[] listBlockMode = new BlockMode[nMaxRound] { BlockMode.HOLLOW, BlockMode.HARD, BlockMode.HOLLOW };

            public static float[] listDistance = new float[nMaxRound + 1] { 4800f, 4000f, 3000f, 500f };
            public static float[] listPlayTime = new float[nMaxRound] { 60f, 60f, 100f };
            public static float[] listSpeed = new float[nMaxRound] { 10f, 13f, 15f };
            public static float[] listRotateSpeed = new float[nMaxRound] { 5f, 6f, 7f };
            public static float[] listBlockDistance = new float[nMaxRound] { 50f, 60f, 70f };
            public static float[] listCellMoveMaxDistance = new float[nMaxRound] { 2f, 3f, 4f };
            public static float[] listCellMoveSpeed = new float[nMaxRound] { 4f, 5f, 6f };
            public static float[] listCellMoveDepth = new float[nMaxRound] { 0.2f, 0.3f, 0.4f };
        }
    }

    public class Mars
    {
        public static Vector3 dSlope = new Vector3(0, 0, 25f);
        public static float nRotateSpeed = Earth.nRotateSpeed * 0.9f;
        public static Vector3 posCameraCloseView = new Vector3(-200f, 0f, -400f);
        public static Vector3 posCameraRewardView = new Vector3(2000f, 1200f, -2000f);
        public static Vector3 dBackgroundScale = new Vector3(2400f, 2400f, 1f);
        public static Color dPlanetColor = new Color(1f, 0.5f, 0.5f, 1f);

        public class Normal
        {
            public const int nMaxRound = 3;

            public static PlayMode[] listPlayMode = new PlayMode[nMaxRound] { PlayMode.BREAK_THEM_ALL, PlayMode.BREAK_THEM_ALL, PlayMode.BREAK_THEM_ALL };
            public static BlockMode[] listBlockMode = new BlockMode[nMaxRound] { BlockMode.HARD, BlockMode.MID, BlockMode.HOLLOW };

            public static float[] listDistance = new float[nMaxRound + 1] { 6600f, 5600f, 4400f, 400f };
            public static float[] listPlayTime = new float[nMaxRound] { 60f, 60f, 100f };
            public static float[] listSpeed = new float[nMaxRound] { 12f, 16f, 20f };
            public static float[] listRotateSpeed = new float[nMaxRound] { 4f, 5f, 6f };
            public static float[] listBlockDistance = new float[nMaxRound] { 60f, 70f, 80f };
            public static float[] listCellMoveMaxDistance = new float[nMaxRound] { 0f, 1f, 2f };
            public static float[] listCellMoveSpeed = new float[nMaxRound] { 0f, 2f, 4f };
            public static float[] listCellMoveDepth = new float[nMaxRound] { 0.4f, 0.5f, 0.6f };
        }

        public class Challenge
        {
            public const int nMaxRound = 3;

            public static PlayMode[] listPlayMode = new PlayMode[nMaxRound] { PlayMode.BREAK_THEM_ALL, PlayMode.BREAK_THEM_ALL, PlayMode.BREAK_THEM_ALL };
            public static BlockMode[] listBlockMode = new BlockMode[nMaxRound] { BlockMode.HOLLOW, BlockMode.HARD, BlockMode.HOLLOW };

            public static float[] listDistance = new float[nMaxRound + 1] { 9800f, 7800f, 5400f,400f };
            public static float[] listPlayTime = new float[nMaxRound] { 60f, 60f, 100f };
            public static float[] listSpeed = new float[nMaxRound] { 20f, 25f, 20f };
            public static float[] listRotateSpeed = new float[nMaxRound] { 7f, 8f, 9f };
            public static float[] listBlockDistance = new float[nMaxRound] { 70f, 80f, 90f };
            public static float[] listCellMoveMaxDistance = new float[nMaxRound] { 2f, 3f, 4f };
            public static float[] listCellMoveSpeed = new float[nMaxRound] { 4f, 6f, 8f };
            public static float[] listCellMoveDepth = new float[nMaxRound] { 0.5f, 0.6f, 0.7f };
        }
    }

    public class Jupiter
    {
        public static Vector3 dSlope = new Vector3(0, 0, -3f);
        public static float nRotateSpeed = Earth.nRotateSpeed * 1.5f;
        public static Vector3 posCameraCloseView = new Vector3(-800f, 0f, -2400f);
        public static Vector3 posCameraRewardView = new Vector3(10000f, 6000f, -8000f);
        public static Vector3 dBackgroundScale = new Vector3(4400f, 4400f, 1f);
        public static Color dPlanetColor = new Color(0.86f, 0.66f, 0.06f, 1f);

        public class Normal
        {
            public const int nMaxRound = 5;
            public static PlayMode[] listPlayMode = new PlayMode[nMaxRound] { PlayMode.NORMAL, PlayMode.BREAK_THEM_ALL, PlayMode.NORMAL, PlayMode.BREAK_THEM_ALL, PlayMode.NORMAL };
            public static BlockMode[] listBlockMode = new BlockMode[nMaxRound] { BlockMode.MID, BlockMode.MID, BlockMode.HARD, BlockMode.HOLLOW, BlockMode.HOLLOW };

            public static float[] listDistance = new float[nMaxRound + 1] { 13500, 12500f, 10500f, 9000f, 6000f, 2500f };
            public static float[] listPlayTime = new float[nMaxRound] { 60f, 60f, 60f, 60f, 100f };
            public static float[] listSpeed = new float[nMaxRound] { 15f, 20f, 20f, 30f, 20f };
            public static float[] listRotateSpeed = new float[nMaxRound] { 7f, 7f, 8f, 8f, 10f };
            public static float[] listBlockDistance = new float[nMaxRound] { 60f, 80f, 70f, 90f, 80f };
            public static float[] listCellMoveMaxDistance = new float[nMaxRound] { 2f, 2f, 3f, 3f, 4f };
            public static float[] listCellMoveSpeed = new float[nMaxRound] { 5f, 5f, 6f, 6f, 8f };
            public static float[] listCellMoveDepth = new float[nMaxRound] { 0.7f, 0.7f, 0.8f, 0.8f, 0.9f };
        }
        public class Challenge
        {
            public const int nMaxRound = 5;
            public static PlayMode[] listPlayMode = new PlayMode[nMaxRound] { PlayMode.NORMAL, PlayMode.BREAK_THEM_ALL, PlayMode.NORMAL, PlayMode.BREAK_THEM_ALL, PlayMode.NORMAL };
            public static BlockMode[] listBlockMode = new BlockMode[nMaxRound] { BlockMode.HOLLOW, BlockMode.MID, BlockMode.MID, BlockMode.HOLLOW, BlockMode.HOLLOW };

            public static float[] listDistance = new float[nMaxRound + 1] { 19200, 17400f, 14400f, 12000f, 7500f, 2500f };
            public static float[] listPlayTime = new float[nMaxRound] { 60f, 60f, 60f, 60f, 100f };
            public static float[] listSpeed = new float[nMaxRound] { 20f, 28f, 26f, 35f, 22f };
            public static float[] listRotateSpeed = new float[nMaxRound] { 10f, 10f, 11f, 11f, 12f };
            public static float[] listBlockDistance = new float[nMaxRound] { 70f, 90f, 80f, 100f, 90f };
            public static float[] listCellMoveMaxDistance = new float[nMaxRound] { 3f, 3f, 4f, 4f, 4f };
            public static float[] listCellMoveSpeed = new float[nMaxRound] { 9f, 9f, 10f, 10f, 12f };
            public static float[] listCellMoveDepth = new float[nMaxRound] { 0.8f, 0.8f, 0.9f, 0.9f, 1f };
        }
    }

    public static string GetPlayStatusName(PlayStatus nPlayStatus)
    {
        string strPlayStatus = "None";
        switch (nPlayStatus)
        {
        case PlayStatus.INIT_COMPONENT: strPlayStatus = "InitComponent"; break;
        case PlayStatus.INIT_PLANET: strPlayStatus = "InitPlanet"; break;
        case PlayStatus.START_PLANET: strPlayStatus = "StartPlanet"; break;
        case PlayStatus.START_ROUND: strPlayStatus = "StartRound"; break;
        case PlayStatus.READY: strPlayStatus = "Ready"; break;
        case PlayStatus.PLAY: strPlayStatus = "Play"; break;
        case PlayStatus.ENDING: strPlayStatus = "Ending"; break;
        case PlayStatus.RESULT: strPlayStatus = "Result"; break;
        case PlayStatus.END_ROUND: strPlayStatus = "EndRound"; break;
        case PlayStatus.END_PLANET: strPlayStatus = "EndPlanet"; break;
        case PlayStatus.QUIT: strPlayStatus = "Quit"; break;
        case PlayStatus.PAUSE: strPlayStatus = "Pause"; break;
        case PlayStatus.RESUME: strPlayStatus = "Resume"; break;
        case PlayStatus.REWARD: strPlayStatus = "Reward"; break;
        case PlayStatus.AFTER_REWARD: strPlayStatus = "AfterReward"; break;
        case PlayStatus.RESTART_ROUND: strPlayStatus = "RestartRound"; break;
        case PlayStatus.DESTROY_COMPONENT: strPlayStatus = "DestroyComponent"; break;
        }

        return strPlayStatus;
    }

    public static string GetPlayEventName(PlayEvent nPlayEvent)
    {
        string strPlayEvent = "None";
        switch (nPlayEvent)
        {
            case PlayEvent.BLOCK_PASS: strPlayEvent = "BlockPass"; break;
            case PlayEvent.BLOCK_DROP: strPlayEvent = "BlockDrop"; break;
            case PlayEvent.BLOCK_DROP_SUCCESS: strPlayEvent = "BlockDropSuccess"; break;
            case PlayEvent.BLOCK_DROP_FAIL: strPlayEvent = "BlockDropFail"; break;
            case PlayEvent.BLOCK_FIT_TO_CELL: strPlayEvent = "BlockFitToCell"; break;
            case PlayEvent.CELL_BREAK: strPlayEvent = "CellBreak"; break;
            case PlayEvent.CELL_ARRIVE: strPlayEvent = "CellArrive"; break;
            case PlayEvent.CELL_AWAY: strPlayEvent = "CellAway"; break;
            case PlayEvent.BLOCK_HIT_TO_CAMERA: strPlayEvent = "BlockHitToCamera"; break;
            case PlayEvent.STAR_HIT: strPlayEvent = "StarHit"; break;
            case PlayEvent.ROUND_COUNT_DOWN: strPlayEvent = "RoundCountDown"; break;
            case PlayEvent.ROUND_COUNT_ZERO: strPlayEvent = "RoundCountZero"; break;
            case PlayEvent.SCORE_COUNTING: strPlayEvent = "ScoreCounting"; break;
            case PlayEvent.BUTTON_CLICK: strPlayEvent = "ButtonClick"; break;
        }

        return strPlayEvent;
    }

    public static string GetGameModeName(GameMode nGameMode)
    {
        string strGameMode = "None";
        switch (nGameMode)
        {
            case GameMode.NORMAL: strGameMode = "Normal"; break;
            case GameMode.CHALLENGE: strGameMode = "Challenge"; break;
        }

        return strGameMode;
    }

    public static string GetPlanetName(PlanetType nPlanetType)
    {
        string strPlanet = "Planet";
        switch (nPlanetType)
        {
            case PlanetType.PLANET_TYPE_EARTH: strPlanet = "Earth"; break;
            case PlanetType.PLANET_TYPE_MARS: strPlanet = "Mars"; break;
            case PlanetType.PLANET_TYPE_JUPITER: strPlanet = "Jupiter"; break;
        }

        return strPlanet;
    }
    public static string GetRoundName(GameMode nGameMode, PlanetType nPlanetType, int nRound, bool bNoSpace = true)
    {
        int nMaxRound = 0;

        if(nGameMode == GameMode.NORMAL) {
            switch (nPlanetType)
            {
                case PlanetType.PLANET_TYPE_EARTH: nMaxRound = GameData.Earth.Normal.nMaxRound; break;
                case PlanetType.PLANET_TYPE_MARS: nMaxRound = GameData.Mars.Normal.nMaxRound; break;
                case PlanetType.PLANET_TYPE_JUPITER: nMaxRound = GameData.Jupiter.Normal.nMaxRound; break;
            }
        }
        else
        {
            switch (nPlanetType)
            {
                case PlanetType.PLANET_TYPE_EARTH: nMaxRound = GameData.Earth.Challenge.nMaxRound; break;
                case PlanetType.PLANET_TYPE_MARS: nMaxRound = GameData.Mars.Challenge.nMaxRound; break;
                case PlanetType.PLANET_TYPE_JUPITER: nMaxRound = GameData.Jupiter.Challenge.nMaxRound; break;
            }
        }

        string strRound = "";
        if(nRound == nMaxRound) {
            if (bNoSpace) strRound = "FinalRound";
            else strRound = "Final Round";
        }
        else {
            strRound = "Round";
            if (!bNoSpace) strRound += " ";
            strRound += nRound.ToString("0");
        }

        return strRound;
    }

    public static string GetGameModeNameInLanguage(GameMode nGameMode)
    {
        string strGameMode = "None";
        switch (nGameMode)
        {
            case GameMode.NORMAL: strGameMode = LanguageData.strGameMode_Normal; break;
            case GameMode.CHALLENGE: strGameMode = LanguageData.strGameMode_Challenge; break;
        }

        return strGameMode;
    }

    public static string GetPlanetNameInLanguage(PlanetType nPlanetType)
    {
        string strPlanet = "Planet";
        switch (nPlanetType)
        {
            case PlanetType.PLANET_TYPE_EARTH: strPlanet = LanguageData.strPlanetName_Earth; break;
            case PlanetType.PLANET_TYPE_MARS: strPlanet = LanguageData.strPlanetName_Mars; break;
            case PlanetType.PLANET_TYPE_JUPITER: strPlanet = LanguageData.strPlanetName_Jupiter; break;
        }

        return strPlanet;
    }
    public static string GetRoundNameInLanguage(GameMode nGameMode, PlanetType nPlanetType, int nRound)
    {
        int nMaxRound = 0;

        if (nGameMode == GameMode.NORMAL)
        {
            switch (nPlanetType)
            {
                case PlanetType.PLANET_TYPE_EARTH: nMaxRound = GameData.Earth.Normal.nMaxRound; break;
                case PlanetType.PLANET_TYPE_MARS: nMaxRound = GameData.Mars.Normal.nMaxRound; break;
                case PlanetType.PLANET_TYPE_JUPITER: nMaxRound = GameData.Jupiter.Normal.nMaxRound; break;
            }
        }
        else
        {
            switch (nPlanetType)
            {
                case PlanetType.PLANET_TYPE_EARTH: nMaxRound = GameData.Earth.Challenge.nMaxRound; break;
                case PlanetType.PLANET_TYPE_MARS: nMaxRound = GameData.Mars.Challenge.nMaxRound; break;
                case PlanetType.PLANET_TYPE_JUPITER: nMaxRound = GameData.Jupiter.Challenge.nMaxRound; break;
            }
        }

        string strRound = "";
        if (nRound == nMaxRound)
        {
            strRound = LanguageData.strFinalRound;
        }
        else
        {
            strRound = LanguageData.strRound + " ";
            strRound += nRound.ToString("0");
        }

        return strRound;
    }
    public class Test
    {
        public static bool bTest = false;
        public static bool bSkipStartPlanet = false;
        public static bool bSkipReady = false;
        public static bool bTurnOffTestOnEndRound = false;
        public static bool bTurnOffTestOnEndPlanet = false;
        public static GameMode nTestGameMode= GameMode.NONE;
        public static PlanetType nTestPlanetType = PlanetType.PLANET_TYPE_NONE;
        public static int nTestRound = 0;
        public static bool nTestQuickClearRound = false;
        public static float nTestStartDistance = 0f;
        public static float nTestStartPlayTime = 0f;
        public static int nTestScore = 0;
        public static int nTestStarCoin = 0;

        public static bool bSkipLoadPrefs = false;

        public static bool bTestChallenge = true;
        public static bool bTestPlayEarth = true;
        public static bool bTestPlayMars = true;
        public static bool bTestPlayJupiter = true;

        public static bool bSkipSendAnalytics = false;
        public static bool bSkipShowAds = false;

        public static bool bShowRecordReplay = false;
        public static bool bRecordingReplay = false;

        public static bool bTurnOffBGM = false;//LanguageData.LANGUAGE_ENGLISH;

        public static string strTestLanguage = "";//LanguageData.LANGUAGE_ENGLISH;
    }
}
