﻿using UnityEngine;
using System.Collections;
using System.IO;

public class PrefsManager : MonoBehaviour {
    int nPassCode = 32;
    string strKey = "Prefs";

/*
    public string strEncValue = "";
    public string strTestText = "";
*/

    static PrefsManager _this;
    public static PrefsManager GetInstance()
    {
        return _this;
    }

    void Awake()
    {
        _this = this;

        // for Test
        if (GameData.Test.bTest && GameData.Test.bSkipLoadPrefs)
        {
            GameData.Prefs.nGamePlayCount = 1;
            LanguageData.SetLanguage(GameData.Test.strTestLanguage);
            return;
        }
        
        LoadPrefs();

        bool bSavePrefs = false;
        if (!GameData.Prefs.bLoaded)
        {
            GameData.Prefs.bLoaded = true;
            GameData.Prefs.nGamePlayCount++;

            GameData.RoundAds.bValidFirstPlay = true;
       }

        if (string.IsNullOrEmpty(GameData.Prefs.strLanguage))
        {
            string strLanguage = LanguageData.LANGUAGE_DEFAULT;
            switch (Application.systemLanguage)
            {
                case SystemLanguage.Russian: strLanguage = LanguageData.LANGUAGE_RUSSIAN; break;
                case SystemLanguage.German: strLanguage = LanguageData.LANGUAGE_GERMAN; break;
                case SystemLanguage.French: strLanguage = LanguageData.LANGUAGE_FRANCH; break;
                case SystemLanguage.Spanish: strLanguage = LanguageData.LANGUAGE_SPANISH; break;
                case SystemLanguage.ChineseSimplified: strLanguage = LanguageData.LANGUAGE_SIMPLIFIED_CHINESE; break;
                case SystemLanguage.ChineseTraditional: strLanguage = LanguageData.LANGUAGE_TRADITIONAL_CHINESE; break;
                case SystemLanguage.Korean: strLanguage = LanguageData.LANGUAGE_KOREAN; break;
                case SystemLanguage.Japanese: strLanguage = LanguageData.LANGUAGE_JAPANESE; break;
            }

            // if it's first setting, send analytics data 
            AnalyticsManger.GetInstance().SendAnalyticsSetLanguage(strLanguage);

            GameData.Prefs.strLanguage = strLanguage;
            bSavePrefs = true;
        }
        else if (!LanguageData.IsSupportedLanguage(GameData.Prefs.strLanguage))
        {
            GameData.Prefs.strLanguage = LanguageData.LANGUAGE_DEFAULT;
            bSavePrefs = true;
        }

        if (GameData.Test.bTest && !string.IsNullOrEmpty(GameData.Test.strTestLanguage))
        {
            GameData.Prefs.strLanguage = GameData.Test.strTestLanguage;
        }

        LanguageData.SetLanguage(GameData.Prefs.strLanguage);

        if(bSavePrefs) {
            SavePrefs();
        }
    }

    public void LoadPrefs()
    {
        string strPrefs = SecurePlayerPrefs.GetString(strKey, nPassCode);
        if (string.IsNullOrEmpty(strPrefs))
        {
            return;
        }

        JSONObject obPrefs = new JSONObject(strPrefs);

        if (obPrefs["Language"]) GameData.Prefs.strLanguage = obPrefs["Language"].str;
        if (obPrefs["StarCoin"]) GameData.Prefs.nStarCoin = (int)obPrefs["StarCoin"].n;
        if (obPrefs["GamePlayCount"]) GameData.Prefs.nGamePlayCount = (int)obPrefs["GamePlayCount"].n;
        if (obPrefs["NewPlayCount"]) GameData.Prefs.nNewPlayCount = (int)obPrefs["NewPlayCount"].n;
        if (obPrefs["RoundPlayCount"]) GameData.Prefs.nRoundPlayCount = (int)obPrefs["RoundPlayCount"].n;
        if (obPrefs["SoundOn"]) GameData.Prefs.bSoundOn = obPrefs["SoundOn"].b;
        if (obPrefs["CanContinue"]) GameData.Prefs.bCanContinue = obPrefs["CanContinue"].b;
        if (obPrefs["LastGameMode"]) GameData.Prefs.nLastGameMode = (GameMode)obPrefs["LastGameMode"].n;
        if (obPrefs["LastPlanetType"]) GameData.Prefs.nLastPlanetType = (PlanetType)obPrefs["LastPlanetType"].n;
        if (obPrefs["LastRound"]) GameData.Prefs.nLastRound = (int)obPrefs["LastRound"].n;
        if (obPrefs["BestScore"]) GameData.Prefs.nBestScore = (int)obPrefs["BestScore"].n;
        if (obPrefs["EarthBestScore"]) GameData.Prefs.nEarthBestScore = (int)obPrefs["EarthBestScore"].n;
        if (obPrefs["MarsBestScore"]) GameData.Prefs.nMarsBestScore = (int)obPrefs["MarsBestScore"].n;
        if (obPrefs["JupiterBestScore"]) GameData.Prefs.nJupiterBestScore = (int)obPrefs["JupiterBestScore"].n;
        if (obPrefs["EarthLock"]) GameData.Prefs.bEarthLock = obPrefs["EarthLock"].b;
        if (obPrefs["MarsLock"]) GameData.Prefs.bMarsLock = obPrefs["MarsLock"].b;
        if (obPrefs["JupiterLock"]) GameData.Prefs.bJupiterLock = obPrefs["JupiterLock"].b;

//        if (obPrefs["TestText"]) strTestText = obPrefs["TestText"].str;

        CheckException();
    }

    public void SavePrefs()
    {
        CheckException();

        JSONObject obPrefs = new JSONObject(JSONObject.Type.OBJECT);

        obPrefs.AddField("Language", GameData.Prefs.strLanguage);
        obPrefs.AddField("StarCoin", GameData.Prefs.nStarCoin);
        obPrefs.AddField("GamePlayCount", GameData.Prefs.nGamePlayCount);
        obPrefs.AddField("NewPlayCount", GameData.Prefs.nNewPlayCount);
        obPrefs.AddField("RoundPlayCount", GameData.Prefs.nRoundPlayCount);
        obPrefs.AddField("SoundOn", GameData.Prefs.bSoundOn);
        obPrefs.AddField("CanContinue", GameData.Prefs.bCanContinue);
        obPrefs.AddField("LastGameMode", (int)GameData.Prefs.nLastGameMode);
        obPrefs.AddField("LastPlanetType", (int)GameData.Prefs.nLastPlanetType);
        obPrefs.AddField("LastRound", GameData.Prefs.nLastRound);
        obPrefs.AddField("BestScore", GameData.Prefs.nBestScore);
        obPrefs.AddField("EarthBestScore", GameData.Prefs.nEarthBestScore);
        obPrefs.AddField("MarsBestScore", GameData.Prefs.nMarsBestScore);
        obPrefs.AddField("JupiterBestScore", GameData.Prefs.nJupiterBestScore);
        obPrefs.AddField("EarthLock", GameData.Prefs.bEarthLock);
        obPrefs.AddField("MarsLock", GameData.Prefs.bMarsLock);
        obPrefs.AddField("JupiterLock", GameData.Prefs.bJupiterLock);
        
/*
        obPrefs.AddField("TestText", "+-,.0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
        strEncValue = SecurePlayerPrefs.Encrypt("+-,.0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", nPassCode);
*/

        string strPrefs = obPrefs.ToString();
        SecurePlayerPrefs.SetString(strKey, strPrefs, nPassCode);
        SecurePlayerPrefs.Save();
    }

    void CheckException()
    {
        if (GameData.Prefs.nStarCoin > GameData.STARCOIN_MAX_COUNT)
        {
            GameData.Prefs.nStarCoin = GameData.STARCOIN_MAX_COUNT;
        }
    }
}
