﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;
//using GoogleMobileAds.Api;

public class AdsManager : MonoBehaviour {
    string strAdsTitle = "";
    bool bFullVideoAds = false;

    string strVideoAdsZone = "defaultZone";
    string strFullVideoAdsZone = "rewardedVideoZone";
    bool bRewardFullVideoAds = false;

//    InterstitialAd iPictureAds = null;
//    string strAdMobId_Android = "ca-app-pub-9792527385262722/6905216496";
//    string strAdMobId_iOS = "";

    MonoBehaviour objCallbackFullVideoAds;
    string funcCallBackFullVideoAds;

    static AdsManager _this;
    public static AdsManager GetInstance()
    {
        return _this;
    }

    void Awake()
    {
        _this = this;
    }

	// Use this for initialization
	void Start () {
        // Default for Android
/*
        string strAdMobId = strAdMobId_Android;
        if(Application.platform == RuntimePlatform.IPhonePlayer) {
            strAdMobId = strAdMobId_iOS;
        }
        iPictureAds = new InterstitialAd(strAdMobId);
*/
    }
	
	// Update is called once per frame
	void Update () {
	}

    public bool IsReadyVideoAds()
    {
        return Advertisement.IsReady(strVideoAdsZone);
    }
    public bool IsReadyFullVideoAds()
    {
        return Advertisement.IsReady(strFullVideoAdsZone);
    }

    public bool ShowVideoAds(string strTitle)
    {
        if (GameData.Test.bSkipShowAds)
        {
            return true;
        }

        if (IsReadyVideoAds())
        {
            GameData.RoundAds.bValidFirstPlay = false;
            GameData.RoundAds.nValidNextRound = GameData.Prefs.nRoundPlayCount + GameData.RoundAds.ROUND_COUNT_AFTER_VIDEO_ADS;

            bFullVideoAds = false;
            strAdsTitle = strTitle;
            objCallbackFullVideoAds = null;
            funcCallBackFullVideoAds = null;
            bRewardFullVideoAds = false;

            var options = new ShowOptions { resultCallback = OnFinishAds };
            Advertisement.Show(strVideoAdsZone, options);
            return true;
        }

        return false;
    }

    public void ShowFullVideoAds(string strTitle, bool bReward = false, MonoBehaviour objCallback = null, string funcCallback = "")
    {
        if (GameData.Test.bSkipShowAds)
        {
            if (objCallback != null && !string.IsNullOrEmpty(funcCallback))
            {
                objCallback.SendMessage(funcCallback);
            }

            return;
        }

        if (IsReadyFullVideoAds())
        {
            GameData.RoundAds.bValidFirstPlay = false;
            GameData.RoundAds.nValidNextRound = GameData.Prefs.nRoundPlayCount + GameData.RoundAds.ROUND_COUNT_AFTER_FULL_VIDEO_ADS;

            bFullVideoAds = true;
            strAdsTitle = strTitle;
            bRewardFullVideoAds = bReward;
            objCallbackFullVideoAds = objCallback;
            funcCallBackFullVideoAds = funcCallback;

            var options = new ShowOptions { resultCallback = OnFinishAds };
            Advertisement.Show(strFullVideoAdsZone, options);
        }
    }

    void OnFinishAds(ShowResult result)
    {
        string strResult = "Finished";
        if(result == ShowResult.Skipped) strResult = "Skipped";
        else if (result == ShowResult.Failed)  strResult = "Failed";

        string strAdsType = bFullVideoAds ? "FullVideo" : "Video";

        AnalyticsManger.GetInstance().SendAnalyticShowAds(strAdsTitle, strAdsType, bRewardFullVideoAds, strResult);

        if (result == ShowResult.Finished && objCallbackFullVideoAds != null && !string.IsNullOrEmpty(funcCallBackFullVideoAds))
        {
            objCallbackFullVideoAds.SendMessage(funcCallBackFullVideoAds);
        }
    }

    public bool ShowEndRoundAds(bool bFirstPlayOnly = false)
    {
        if (GameData.RoundAds.bValidFirstPlay)
        {
            ShowVideoAds("FirstRound");
            return true;
        }
        else if (!bFirstPlayOnly)
        {
            if (GameData.Prefs.nRoundPlayCount >= GameData.RoundAds.nValidNextRound) {
                ShowVideoAds("EndRound");
                return true;
            }
        }

        return false;
    }

/*
    public bool IsReadyPictureAds()
    {
        return iPictureAds.IsLoaded();
    }

    public bool ShowPictureAds()
    {
        if (IsReadyPictureAds())
        {
            // Initialize an InterstitialAd.
            // Create an empty ad request.
            AdRequest request = new AdRequest.Builder().Build();
            // Load the interstitial with the request.
            iPictureAds.LoadAd(request);

            return true;
        }

        return false;
    }
*/
}
