﻿using UnityEngine;
using System.Collections;

public class StarCoinManager : MonoBehaviour {

    static StarCoinManager _this;

    public static StarCoinManager GetInstance()
    {
        return _this;
    }

    void Awake()
    {
        _this = this;
    }

    public bool CanGetReward()
    {
        return AdsManager.GetInstance().IsReadyFullVideoAds();
    }

    public bool CanContinue()
    {
        return (GameData.Prefs.nStarCoin >= GameData.StarPrice.CONTINUE);
    }

    public bool CanRestart()
    {
        return (GameData.Prefs.nStarCoin >= GameData.StarPrice.RESTART);
    }

    public bool CanPlay(PlanetType nPlanetType)
    {
        switch (nPlanetType)
        {
            case PlanetType.PLANET_TYPE_EARTH : 
                return (GameData.Prefs.nStarCoin >= GameData.StarPrice.PLAY_EARTH);
            case PlanetType.PLANET_TYPE_MARS: 
                return (GameData.Prefs.nStarCoin >= GameData.StarPrice.PLAY_MARS);
            case PlanetType.PLANET_TYPE_JUPITER: 
                return (GameData.Prefs.nStarCoin >= GameData.StarPrice.PLAY_JUPITER);
        }

        return false;
    }

    public bool CanPlayEarth()
    {
        return CanPlay(PlanetType.PLANET_TYPE_EARTH);
    }

    public bool CanPlayMars()
    {
        return CanPlay(PlanetType.PLANET_TYPE_MARS);
    }

    public bool CanPlayJupiter()
    {
        return CanPlay(PlanetType.PLANET_TYPE_JUPITER);
    }

    public int GetReward()
    {
        return Random.Range(GameData.StarPrice.REWARD, GameData.StarPrice.REWARD + 11);
    }

    public bool PayContinue()
    {
        return Pay(GameData.StarPrice.CONTINUE);
    }

    public bool PayRestart()
    {
        return Pay(GameData.StarPrice.RESTART);
    }

    public bool PayEarth()
    {
        return Pay(GameData.StarPrice.PLAY_EARTH);
    }

    public bool PayMars()
    {
        return Pay(GameData.StarPrice.PLAY_MARS);
    }

    public bool PayJupiter()
    {
        return Pay(GameData.StarPrice.PLAY_JUPITER);
    }

    public bool Add(int nStarCoin)
    {
        if (nStarCoin < 0)
        {
            return false;
        }

        GameData.Prefs.nStarCoin += nStarCoin;
        if (GameData.Prefs.nStarCoin > GameData.STARCOIN_MAX_COUNT)
        {
            GameData.Prefs.nStarCoin = GameData.STARCOIN_MAX_COUNT;
        }

        PrefsManager.GetInstance().SavePrefs();

        return true;
    }

    public bool Pay(int nStarCoin)
    {
        if (nStarCoin < 0)
        {
            return false;
        }

        if (GameData.Prefs.nStarCoin < nStarCoin)
        {
            return false;
        }

        GameData.Prefs.nStarCoin -= nStarCoin;
        PrefsManager.GetInstance().SavePrefs();

        return true;
    }
}
