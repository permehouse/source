﻿using UnityEngine;
using UnityEngine.Analytics;
using System.Collections;
using System.Collections.Generic;

public class AnalyticsManger : MonoBehaviour {
	// Use this for initialization
    static AnalyticsManger _this;
    public static AnalyticsManger GetInstance()
    {
        return _this;
    }
    void Awake()
    {
        _this = this;
    }

    public void SendAnalyticsStartPlay(string strPlayName)
    {
        string strVersion = GameData.strVersion;
        string strPlatform = Application.platform.ToString();

        Analytics.CustomEvent("StartPlay", new Dictionary<string, object>
        {
            { "PlayName", strPlayName },

            { "Version", strVersion},
            { "Platform", strPlatform}
        });
    }

    public void SendAnalyticsPlayRound()
    {
        if (GameData.Test.bTest && GameData.Test.bSkipSendAnalytics)
        {
            return;
        }

        // Round Information
        string strStatus = GameData.GetPlayStatusName(GameData.nPlayStatus);
        string strGameMode = GameData.GetGameModeName(GameData.nGameMode);
        string strPlanet = GameData.GetPlanetName(GameData.nPlanetType);
        string strRound = GameData.GetRoundName(GameData.nGameMode, GameData.nPlanetType, GameData.nRound);
        int nScore = GameData.nScore;
        int nRoundScore = GameData.nRoundScore + GameData.nBonusScore;
        int nBestSpeed = GameData.nBestSpeed;
        int nPlayTime = (int)Mathf.Max(Mathf.Floor(GameData.nPlayTime), 0f);

        // Player Information
        int nGamePlayCount = GameData.Prefs.nGamePlayCount;
        int nRoundPlayCount = GameData.Prefs.nRoundPlayCount;

        Analytics.CustomEvent("PlayRound", new Dictionary<string, object>
        {
            { "Status", strStatus },
            { "GameMode", strGameMode },
            { "Planet", strPlanet },
            { "Round", strRound },
            { "Score", nScore },
            { "RoundScore", nRoundScore },
            { "BestSpeed", nBestSpeed },
            { "PlayTime", nPlayTime},

            { "GamePlayCount", nGamePlayCount },
            { "RoundPlayCount", nRoundPlayCount }
        });
    }

    public void SendAnalyticShareReplay()
    {
        if (GameData.Test.bTest && GameData.Test.bSkipSendAnalytics)
        {
            return;
        }

        // Round Information
        string strGameMode = GameData.GetGameModeName(GameData.nGameMode);
        string strPlanet = GameData.GetPlanetName(GameData.nPlanetType);
        string strRound = GameData.GetRoundName(GameData.nGameMode, GameData.nPlanetType, GameData.nRound);
        int nScore = GameData.nScore;
        int nRoundScore = GameData.nRoundScore + GameData.nBonusScore;
        int nBestSpeed = GameData.nBestSpeed;
        int nBestComboCount = GameData.nBestComboCount;

        string strVersion = GameData.strVersion;
        string strPlatform = Application.platform.ToString();

        Analytics.CustomEvent("ShareReplay", new Dictionary<string, object>
        {
            { "GameMode", strGameMode },
            { "Planet", strPlanet },
            { "Round", strRound },
            { "Score", nScore },
            { "RoundScore", nRoundScore },
            { "BestSpeed", nBestSpeed },
            { "BestComboCount", nBestComboCount },

            { "Version", strVersion},
            { "Platform", strPlatform}
        });
    }

    public void SendAnalyticShowAds(string strAdsTitle, string strAdsType, bool bReward, string strResult)
    {
        if (GameData.Test.bTest && GameData.Test.bSkipSendAnalytics)
        {
            return;
        }

        // Round Information
        string strGameMode = GameData.GetGameModeName(GameData.nGameMode);
        string strPlanet = GameData.GetPlanetName(GameData.nPlanetType);
        string strRound = GameData.GetRoundName(GameData.nGameMode, GameData.nPlanetType, GameData.nRound);

        string strVersion = GameData.strVersion;
        string strPlatform = Application.platform.ToString();

        Analytics.CustomEvent("ShowAds", new Dictionary<string, object>
        {
            { "Title", strAdsTitle },
            { "Type", strAdsType },
            { "Reward", bReward },
            { "Result", strResult },

            { "GameMode", strGameMode },
            { "Planet", strPlanet },
            { "Round", strRound },

            { "Version", strVersion},
            { "Platform", strPlatform}
        });
    }
    public void SendAnalyticsSetLanguage(string strLanguage)
    {
        string strOSLanguage = Application.systemLanguage.ToString();

        string strVersion = GameData.strVersion;
        string strPlatform = Application.platform.ToString();

        Analytics.CustomEvent("SetLanguage", new Dictionary<string, object>
        {
            { "Language", strLanguage },
            { "OSLanguage", strOSLanguage },

            { "Version", strVersion},
            { "Platform", strPlatform}
        });
    }


/*
    public void SendAnalyticsViewHowToPlay()
    {
        Analytics.Transaction("HowToPlay", 1 , "View", null, null);
        Analytics.CustomEvent("HowToPlay", null);
    }
    public void SendAnalyticsShareReplay()
    {
        Analytics.Transaction("ShareReplay", 1, "Clip", null, null);
        Analytics.CustomEvent("ShareReplay", null);
    }
 */
/*
        Analytics.Transaction("GetReward", test, "StarCoin", null, null);

        Gender gender = Gender.Female;
        Analytics.SetUserGender(gender);

        int birthYear = 2014;
        Analytics.SetUserBirthYear(birthYear);
*/
}
