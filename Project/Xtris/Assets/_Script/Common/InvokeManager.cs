﻿using UnityEngine;
using System.Collections;

public class InvokeManager : MonoBehaviour {
    public class InvokeData {
        public MonoBehaviour obj;
        public string func;
        public object param;
        public float time;

        public InvokeData(MonoBehaviour obj, string func, object param, float time)
        {
            this.obj = obj;
            this.func = func;
            this.param = param;
            this.time = time;
        }
    }

    ArrayList listInvoke = new ArrayList();

    static InvokeManager _this;
    public static InvokeManager GetInstance()
    {
        return _this;
    }

    void Awake()
    {
        _this = this;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (GameData.nPlayStatus == PlayStatus.PAUSE)
        {
            return;
        }

	    CheckInvoke();
	}

    void CheckInvoke()
    {
        int nIndex = listInvoke.Count - 1;
        while (nIndex >= 0)     // should check the list count because the invoked function may clear the list
        {
            InvokeData dData = (InvokeData)listInvoke[nIndex];
            dData.time -= Time.deltaTime;

            if (dData.time <= 0f)
            {
                listInvoke.RemoveAt(nIndex);

                if (dData.obj != null)
                {
                    if (dData.param != null)
                    {
//                        Debug.Log("Invoke : (" + nIndex + " / " + listInvoke.Count + ") -> " + dData.obj + ", " + dData.func + ", " + dData.param);
                        dData.obj.SendMessage(dData.func, dData.param);
                    }
                    else
                    {
//                        Debug.Log("Invoke : (" + nIndex + " / " + listInvoke.Count + ") -> " +dData.obj + ", " + dData.func);
                        dData.obj.SendMessage(dData.func);
                    }

                    if (listInvoke.Count == 0)
                    {
                        // It's possible that the invoked object above called list clear function and there's no list item now
                        return;
                    }
                }
            }

            nIndex--;
        }
    }


    public void SetInvoke(MonoBehaviour obj, string func, float time)
    {
        SetInvoke(obj, func, null, time);
    }

    public void SetInvoke(MonoBehaviour obj, string func, object param, float time)
    {
//        Debug.Log("Set Invoke : " + obj + ", " + func + " : " + ", " + param + ", " + time);
        InvokeData dData = new InvokeData(obj, func, param, time);
        listInvoke.Add(dData);
    }

    public void Clear()
    {
//        Debug.Log("Clear Invoke");
        listInvoke.Clear();
    }
}
