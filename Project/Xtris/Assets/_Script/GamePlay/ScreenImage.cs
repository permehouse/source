﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScreenImage : MonoBehaviour {
    Image obImage;
    Color dColorTo;
    float nColorLerpRate = 0f;

    void Awake()
    {
        obImage = GetComponent<Image>();
    }

	// Use this for initialization
	void Start () {
        gameObject.SetActive(true);
        obImage.color = GameData.SCREEN_IMAGE_FADE_OUT_COLOR;
	}
	
	// Update is called once per frame
	void Update () {
        if (GameData.nPlayStatus < PlayStatus.START_PLANET)
        {
            return;
        }

        UpdateEffect();
    }

    void UpdateEffect() {
        if (obImage.color != dColorTo)
        {
            obImage.color = Color.Lerp(obImage.color, dColorTo, nColorLerpRate * Time.deltaTime);
        }
	}

    public void ShowFadeInEffect()
    {
        gameObject.SetActive(true);
        dColorTo = GameData.SCREEN_IMAGE_FADE_IN_COLOR;
        nColorLerpRate = GameData.SCREEN_IMAGE_FADE_IN_COLOR_LERP_RATE;
    }

    public void ShowFadeOutEffect()
    {
        gameObject.SetActive(true);
        dColorTo = GameData.SCREEN_IMAGE_FADE_OUT_COLOR;
        nColorLerpRate = GameData.SCREEN_IMAGE_FADE_OUT_COLOR_LERP_RATE;
    }

    void ShowDamageEffect()
    {
        gameObject.SetActive(true);
        obImage.color = GameData.SCREEN_IMAGE_DAMAGE_COLOR;
        dColorTo = Color.clear;
        nColorLerpRate = GameData.SCREEN_IMAGE_DAMAGE_COLOR_LERP_RATE;
    }

    void OnPlayStatusStartPlanet()
    {
        ShowFadeInEffect();
    }
    void OnPlayStatusStartRound()
    {
        ShowFadeInEffect();
    }

    void OnPlayStatusRestartRound()
    {
        ShowFadeOutEffect();
    }
    void OnPlayStatusQuit()
    {
        ShowFadeOutEffect();
    }

    void OnPlayStatusReward()
    {
        ShowFadeInEffect();
    }

    void OnPlayStatusAfterReward()
    {
        ShowFadeOutEffect();
    }
    public void OnPlayEventBlockHitToCamera()
    {
        ShowDamageEffect();
    }
}
