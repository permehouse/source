﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour {
    public Camera obCamera = null;

//    public GameObject obPauseButton = null;
//    Vector2 posPauseButton;
//    Vector2 sizeHalfPauseButton;

    bool bTouch = false;
    bool bRelease = false;
    bool bDrag = false;

    Vector2 sizeDrag;

    Vector2 posInput;
    Vector2 posInputBefore;

	// Use this for initialization
	void Start () {
        Reset();
	}

    void Reset()
    {
        bTouch = false;
        bRelease = false;
        bDrag = false;
        posInput = new Vector2(0f, 0f);
        posInputBefore = posInput;
    }
	
	// Update is called once per frame
	void Update () {
        if (GameData.nPlayStatus < PlayStatus.START_ROUND || GameData.nPlayStatus > PlayStatus.END_ROUND)
        {
            return;
        }

        CheckInput();
	}

    public void CheckInput()
    {
        bTouch = false;
        bRelease = false;
        posInput = new Vector2(0f, 0f);

        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        {
            CheckTouchInput();
        }
        else
        {
            CheckMouseInput();
            CheckKeyboardInput();
        }

        ProcessEvent();
    }

    void CheckMouseInput()
    {
        posInput = Input.mousePosition;

        if (Input.GetMouseButtonDown(0))
        {
            bTouch = true;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            bRelease = true;
        }
    }

    public void CheckTouchInput()
    {
        int nCount = Input.touchCount;
        for (int i = 0; i < nCount; i++)
        {
            posInput = Input.GetTouch(i).position;
            TouchPhase phaseTouch = Input.GetTouch(i).phase;

            if (i == 0) {         // first touch
                if (phaseTouch == TouchPhase.Began) {
                    bTouch = true;
                    posInputBefore = posInput;
                }
                else if (phaseTouch == TouchPhase.Ended || phaseTouch == TouchPhase.Canceled)
                {
                    if (bDrag) {
                        bDrag = false;
                    }
                    else
                    {
                        bRelease = true;
                    }
                }
                else if (phaseTouch == TouchPhase.Moved)
                {
                    sizeDrag = posInput - posInputBefore;
                    if (sizeDrag.magnitude >= GameData.CONTROLLER_MIN_DRAG_SIZE)
                    {
                        bDrag = true;
                    }
                }
            }
            else {          // second or else touch
                if (phaseTouch == TouchPhase.Began)
                {
                    bTouch = true;
                }
                else if (phaseTouch == TouchPhase.Ended || phaseTouch == TouchPhase.Canceled)
                {
                    bRelease = true;
                }
            }
        }
    }

    public void CheckKeyboardInput()
    {
        sizeDrag.x = Input.GetAxis("Horizontal") * GameData.CONTROLLER_MAX_DRAG_SIZE;
        sizeDrag.y = Input.GetAxis("Vertical") * GameData.CONTROLLER_MAX_DRAG_SIZE;

        bDrag = (sizeDrag.magnitude > 0f);
    }

    public void ProcessEvent()
    {
        if (CheckPosInMenuButton())
        {
            return;
        }

        if (bTouch)
        {
            GamePlay.GetInstance().OnControllerTouch(posInput);
        }
        else if (bRelease)
        {
            GamePlay.GetInstance().OnControllerRelease(posInput);
        }
        else {
            float nHoriz = 0f;
            float nVert = 0f;
            if (bDrag) {
                nHoriz = Mathf.Clamp(sizeDrag.x, -GameData.CONTROLLER_MAX_DRAG_SIZE, GameData.CONTROLLER_MAX_DRAG_SIZE) / GameData.CONTROLLER_MAX_DRAG_SIZE;
                nVert = Mathf.Clamp(sizeDrag.y, -GameData.CONTROLLER_MAX_DRAG_SIZE, GameData.CONTROLLER_MAX_DRAG_SIZE) / GameData.CONTROLLER_MAX_DRAG_SIZE;
            }

            GamePlay.GetInstance().OnControllerAxes(nHoriz, nVert);
        }
    }

    bool CheckPosInMenuButton()
    {
/*
        if (posInput.x >= posPauseButton.x - sizeHalfPauseButton.x && posInput.x <= posPauseButton.x + sizeHalfPauseButton.x
            && posInput.y >= posPauseButton.y - sizeHalfPauseButton.y && posInput.y <= posPauseButton.y + sizeHalfPauseButton.y)
        {
            return true;
        }
*/

        return false;
    }
    public void OnPlayStatusStartRound()
    {
        Reset();
    }
}
