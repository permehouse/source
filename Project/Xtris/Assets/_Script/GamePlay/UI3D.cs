﻿using UnityEngine;
using System.Collections;

public class UI3D : MonoBehaviour {
    // Left Panel
    GameObject obLeftPanel = null;
    TextMesh txtSpeed = null;
    TextMesh txtSpeedTitle = null;
    TextMesh txtDistance = null;
    TextMesh txtStarCoin = null;
    TextMesh txtStarCoinBack = null;

    // Right Panel
    GameObject obRightPanel = null;
    TextMesh txtRound = null;
    TextMesh txtPlayTime = null;
    TextMesh txtPlayTimeBack = null;
    TextMesh txtGameMode = null;
    TextMesh txtPlanetInRightPanel = null;
    TextMesh txtBestScore = null;
    TextMesh txtBestScoreTitle = null;
    TextMesh txtScore = null;
    TextMesh txtScoreTitle = null;

    // Center Panel
    GameObject obCenterPanel = null;
    TextMesh txtPlanet = null;
    TextMesh txtPlanetBack = null;
    TextMesh txtPlanetComplete = null;
    TextMesh txtPlanetCompleteBack = null;
    TextMesh txtRoundReadyTime = null;
    TextMesh txtRoundOver = null;
    TextMesh txtRoundOverBack = null;
    TextMesh txtResult = null;
    TextMesh txtCombo= null;
    TextMesh txtBreakThemAll = null;

    float nResultDisplayTime = 0f;
    string strBlockDropResult;

    float nStartSpeedScale = 0f;
    float nSpeedUpTime = 0f;
    float nSpeedDownTime = 0f;

    float nDisplayStarCoin = 0f;

    CameraManager obCamera = null;
    Playground obPlayground = null;

    void Awake()
    {
        gameObject.name = GameData.OBJECT_NAME_UI3D;
    }

	// Use this for initialization

    void MakeTextTransparent(ref TextMesh txt, float nAlpha)
    {
        Color color = txt.color;
        color.a = nAlpha;
        SetTextColor(ref txt, color);
    }

    void SetTextColor(ref TextMesh txt, Color color)
    {
        txt.color = color;
    }
	
    void LateUpdate()
    {
        UpdatePos();
        if (GameData.nPlayStatus < PlayStatus.START_PLANET || GameData.nPlayStatus > PlayStatus.END_PLANET) {
            return;
        }

        DisplayStarCoin();
        UpdateSpeedScale();
        UpdateRoundReadyPos();

//        DisplayPlanet();
//        DisplayPlanetComplete();
        DisplayRound();
        DisplaySpeed();
        DisplayDistance();
        DisplayTime();
        DisplayGameMode();
        DisplayScore();
        DisplayResuilt();
        DisplayRoundReadyTime();
        DisplayBreakThemAll();
    }

    void UpdatePos()
    {
        if (GameData.nPlayStatus < PlayStatus.START_PLANET)
        {
            return;
        }

        Vector3 posCamera = obCamera.transform.position;
        transform.position = posCamera;
    }

    void DisplaySpeed()
    {
        int nMoveSpeed = (int)(obPlayground.GetSpeed() * GameData.ADJUST_UNIT_SPEED_RATE);
        txtSpeed.text = nMoveSpeed.ToString("0");
    }

    void DisplayDistance()
    {
        int nDistance = (int)(obPlayground.GetDistance() * GameData.ADJUST_UNIT_DISTANCE_RATE);
        if (nDistance <= 0)
        {
            txtDistance.gameObject.SetActive(false);
        }
        txtDistance.text = nDistance.ToString("0");
    }

    public void DisplayTime()
    {
        float nTime = Mathf.Max((int)GameData.nPlayTime, 0f);
        // to avoid showing time as 0 when a round is clear

        txtPlayTime.text = nTime.ToString("0");
        txtPlayTimeBack.text = nTime.ToString("0");
    }

    void DisplayRound()
    {
        if (GameData.nPlayStatus <= PlayStatus.PLAY)
        {
            txtRound.text = GameData.GetRoundNameInLanguage(GameData.nGameMode, GameData.nPlanetType, GameData.nRound);
        }
    }

    public void DisplayStarCoin()
    {
        if (GameData.nPlayStatus <= PlayStatus.START_ROUND || GameData.nPlayStatus == PlayStatus.REWARD)
        {
            nDisplayStarCoin = GameData.Prefs.nStarCoin;
        }
        else
        {
            if ((int)nDisplayStarCoin == GameData.Prefs.nStarCoin)
            {
                nDisplayStarCoin = GameData.Prefs.nStarCoin;
            }
            else
            {
                nDisplayStarCoin += Mathf.Sign(GameData.Prefs.nStarCoin - nDisplayStarCoin) * GameData.STARCOIN_DISPLAY_CHANGE_SPEED * Time.deltaTime;
            }
        }

        txtStarCoin.text = nDisplayStarCoin.ToString("0");
        txtStarCoinBack.text = nDisplayStarCoin.ToString("0");
    }

    public void DisplayGameMode()
    {
        if (GameData.nGameMode == GameMode.NORMAL)
        {
            txtGameMode.gameObject.SetActive(false);
        }
        else
        {
            txtGameMode.gameObject.SetActive(true);
        }
    }

    public void DisplayScore()
    {
        int nScore = (int)GameData.nScore;
        txtScore.text = nScore.ToString("N0");

        int nBestScore = (int)GameData.nBestScore;
        txtBestScore.text = nBestScore.ToString("N0");

        if (nBestScore == nScore)
        {
            txtBestScoreTitle.text = LanguageData.strNewBest;
        }
        else
        {
            txtBestScoreTitle.text = LanguageData.strBest;
        }
    }

    void DisplayResuilt()
    {
        nResultDisplayTime -= Time.deltaTime;
        if (nResultDisplayTime <= 0)
        {
            strBlockDropResult = "";
            txtResult.text = "";
            txtCombo.text = "";
            return;
        }

        txtResult.text = strBlockDropResult;
        float nAlpha = Mathf.Abs(Mathf.Sin(Mathf.PI * nResultDisplayTime));
        Color color = new Color(GameData.dPlanetColor.r, GameData.dPlanetColor.g, GameData.dPlanetColor.b, nAlpha);
        SetTextColor(ref txtResult, color);


        if (GameData.nComboCount >= GameData.SCORE_BOOST_MIN_COMBO_COUNT) {
            txtCombo.text = GameData.nComboCount.ToString("0") + " " + LanguageData.strCombo;

            float nScale = 1f + Mathf.Max(Mathf.Sin(Mathf.PI * nResultDisplayTime) * GameData.UI_COMBO_SCALE_RATE, 0f);
            txtCombo.transform.localScale = new Vector3(nScale, nScale, 1f);

            MakeTextTransparent(ref txtCombo, nAlpha);
        }
        else
        {
            txtCombo.text = "";
        }
    }

/*
    void DisplayPlanet()
    {
        txtPlanet.text = GameData.strPlanetName;
        txtPlanetBack.text = GameData.strPlanetName;

        txtPlanetInRightPanel.text = GameData.strPlanetName;
    }
    void DisplayPlanetComplete()
    {
        txtPlanetComplete.text = GameData.strPlanetName + " COMPLETE!!";
        txtPlanetCompleteBack.text = GameData.strPlanetName + " COMPLETE!!";
    }
*/

    void UpdatePlanetTextColor()
    {
        Color color = new Color(GameData.dPlanetColor.r, GameData.dPlanetColor.g, GameData.dPlanetColor.b, GameData.UI_TEXT_HIGH_ALPHA);
        SetTextColor(ref txtPlanet, color);
        SetTextColor(ref txtPlanetInRightPanel, color);
    }

    void DisplayRoundReadyTime()
    {
        if (GameData.nRoundReadyTime <= 0f)
        {
            return;
        }

        float nTime = Mathf.Floor(GameData.nRoundReadyTime);
        if (nTime >= 4f) {
            return;
        }

        if (nTime >= 1f)
        {
            txtRoundReadyTime.text = nTime.ToString("0");
        }
        else
        {
            txtRoundReadyTime.text = LanguageData.strStart;
        }

        float nAlpha = GameData.nRoundReadyTime - Mathf.Floor(GameData.nRoundReadyTime);
        float nLevel = GameData.nRoundReadyTime / (GameData.ROUND_READY_TIME + 0.5f);

        Color color = Color.red;
        color.g = nLevel;
        color.b = nLevel;
        color.a = nAlpha;

        SetTextColor(ref txtRoundReadyTime, color);
    }

    void DisplayBreakThemAll()
    {
        float nScale = 1f + Mathf.Min(Mathf.Cos(Mathf.PI * Time.time) * GameData.UI_BREAK_THEM_ALL_SCALE_RATE, 0f);
        txtBreakThemAll.transform.localScale = new Vector3(nScale, nScale, 1f);

        float nAlpha = Mathf.Abs(Mathf.Cos(Mathf.PI * Time.time));
        MakeTextTransparent(ref txtBreakThemAll, nAlpha);
    }

    void ShowBlockDropResult()
    {
        strBlockDropResult = "";
        switch (GameData.nMatchResult)
        {
            case MatchResult.GOOD: strBlockDropResult = LanguageData.strGood; break;
            case MatchResult.GREAT: strBlockDropResult = LanguageData.strGreat; break;
            case MatchResult.PERFECT: strBlockDropResult = LanguageData.strPerfect; break;
        }

        nResultDisplayTime = GameData.UI_RESULT_SHOW_TIME;
    }

    public void ShowRounOver(bool bShow = true)
    {
        txtRoundOver.gameObject.SetActive(bShow);

        if (bShow)
        {
            string strText = "";
            Color dColor = GameData.UI_ROUND_CLEAR_COLOR;
            if (GameData.nRoundResult == RoundResult.ROUND_CLEAR) {
                strText = LanguageData.strRoundClear;
                dColor = GameData.UI_ROUND_CLEAR_COLOR;
            }
            else if(GameData.nRoundResult == RoundResult.TIME_OVER) {
                strText = LanguageData.strTimeOver;
                dColor = GameData.UI_TIME_OVER_COLOR;
            }

            txtRoundOver.text = strText;
            txtRoundOverBack.text = strText;

            SetTextColor(ref txtRoundOver, dColor);

        }
    }

    public void ShowPanels(bool bShowLeftPanel, bool bShowCenterPanel, bool bShowRightPanel)
    {
        obLeftPanel.SetActive(bShowLeftPanel);
        obCenterPanel.SetActive(bShowCenterPanel);
        obRightPanel.SetActive(bShowRightPanel);
    }

    public void ShowPlanet(bool bShow = true)
    {
        txtPlanet.gameObject.SetActive(bShow);
        txtPlanetBack.gameObject.SetActive(bShow);
    }
    public void ShowPlanetComplete(bool bShow = true)
    {
        txtPlanetComplete.gameObject.SetActive(bShow);
        txtPlanetCompleteBack.gameObject.SetActive(bShow);
    }

    public void ShowResult(bool bShow = true)
    {
        txtResult.gameObject.SetActive(bShow);
        txtCombo.gameObject.SetActive(bShow);
    }
    
    public void ShowRoundReadyTime(bool bShow = true)
    {
        txtRoundReadyTime.gameObject.SetActive(bShow);
    }

    public void ShowBreakThemAll(bool bShow = true)
    {
        txtBreakThemAll.gameObject.SetActive(bShow);
    }

    void ShowStarCoinOnly(bool bShow)
    {
        if (bShow)
        {
            ShowPanels(true, false, false);
            txtSpeed.gameObject.SetActive(false);
            txtSpeedTitle.gameObject.SetActive(false);
            txtDistance.gameObject.SetActive(false);
            txtStarCoin.gameObject.SetActive(true);
        }
        else
        {
            ShowPanels(false, false, false);
            txtSpeed.gameObject.SetActive(true);
            txtSpeedTitle.gameObject.SetActive(true);
            txtDistance.gameObject.SetActive(true);
            txtStarCoin.gameObject.SetActive(true);
        }
    }

    public void SpeedUp()
    {
        nStartSpeedScale = txtSpeed.transform.localScale.x;
        nSpeedUpTime = GameData.UI_SPEED_SCALE_TIME;
        nSpeedDownTime = 0f;
    }

    public void SpeedDown()
    {
        nStartSpeedScale = txtSpeed.transform.localScale.x;
        nSpeedUpTime = 0f;
        nSpeedDownTime = GameData.UI_SPEED_SCALE_TIME;
    }
    void UpdateSpeedScale()
    {
        if (GameData.nPlayStatus != PlayStatus.PLAY) {
            return;
        }

        float nScale = txtSpeed.transform.localScale.x;
        if (nSpeedUpTime > 0f)
        {
            nScale = nStartSpeedScale * (1f + GameData.UI_SPEED_SCALE_RATE * (GameData.UI_SPEED_SCALE_TIME - nSpeedUpTime) / GameData.UI_SPEED_SCALE_TIME);
            nScale = Mathf.Min(nScale, 1f + GameData.UI_SPEED_SCALE_RATE);
            nSpeedUpTime -= Time.deltaTime;
        }
        else if (nSpeedDownTime > 0f)
        {
            nScale = nStartSpeedScale * (1f - GameData.UI_SPEED_SCALE_RATE * (GameData.UI_SPEED_SCALE_TIME - nSpeedDownTime) / GameData.UI_SPEED_SCALE_TIME);
            nScale = Mathf.Max(nScale, 1f - GameData.UI_SPEED_SCALE_RATE);
            nSpeedDownTime -= Time.deltaTime;
        }

        txtSpeed.transform.localScale = new Vector3(nScale, nScale, 1f);
    }

    void UpdateRoundReadyPos()
    {
        if (GameData.nPlayStatus != PlayStatus.READY)
        {
            return;
        }

        float nDistance = (GameData.nRoundReadyTime - Mathf.Floor(GameData.nRoundReadyTime)) * GameData.UI_ROUND_READY_TIME_DISTANCE_RATE;
        txtRoundReadyTime.gameObject.transform.localPosition = new Vector3(0f, 0f, nDistance);
    }

    public void OnPlayStatusInitComponent()
    {
        obCamera = GamePlay.GetInstance().obCamera;
        obPlayground = GamePlay.GetInstance().obPlayground;

        obLeftPanel = transform.Find("LeftPanel").gameObject;
        txtSpeed = obLeftPanel.transform.Find("SpeedText").GetComponent<TextMesh>();
        txtSpeedTitle = obLeftPanel.transform.Find("SpeedTitleText").GetComponent<TextMesh>();
        txtDistance = obLeftPanel.transform.Find("DistanceText").GetComponent<TextMesh>();
        txtStarCoin = obLeftPanel.transform.Find("StarCoinText").GetComponent<TextMesh>();
        txtStarCoinBack = obLeftPanel.transform.Find("StarCoinText/StarCoinBackText").GetComponent<TextMesh>();

        obRightPanel = transform.Find("RightPanel").gameObject;
        txtRound = obRightPanel.transform.Find("RoundText").GetComponent<TextMesh>();
        txtPlayTime = obRightPanel.transform.Find("PlayTimeText").GetComponent<TextMesh>();
        txtPlayTimeBack = obRightPanel.transform.Find("PlayTimeText/PlayTimeBackText").GetComponent<TextMesh>();
        txtScore = obRightPanel.transform.Find("ScoreText").GetComponent<TextMesh>();
        txtScoreTitle = obRightPanel.transform.Find("ScoreText/ScoreTitleText").GetComponent<TextMesh>();
        txtGameMode = obRightPanel.transform.Find("GameModeText").GetComponent<TextMesh>();
        txtPlanetInRightPanel = obRightPanel.transform.Find("PlanetText").GetComponent<TextMesh>();
        txtBestScore = obRightPanel.transform.Find("BestScoreText").GetComponent<TextMesh>();
        txtBestScoreTitle = obRightPanel.transform.Find("BestScoreText/BestScoreTitleText").GetComponent<TextMesh>();

        obCenterPanel = transform.Find("CenterPanel").gameObject;
        txtPlanet = obCenterPanel.transform.Find("PlanetText").GetComponent<TextMesh>();
        txtPlanetBack = obCenterPanel.transform.Find("PlanetText/PlanetBackText").GetComponent<TextMesh>();
        txtPlanetComplete = obCenterPanel.transform.Find("PlanetCompleteText").GetComponent<TextMesh>();
        txtPlanetCompleteBack = obCenterPanel.transform.Find("PlanetCompleteText/PlanetCompleteBackText").GetComponent<TextMesh>();
        txtRoundReadyTime = obCenterPanel.transform.Find("RoundReadyTimeText").GetComponent<TextMesh>();
        txtRoundOver = obCenterPanel.transform.Find("RoundOverText").GetComponent<TextMesh>();
        txtRoundOverBack = obCenterPanel.transform.Find("RoundOverText/RoundOverBackText").GetComponent<TextMesh>();
        txtResult = obCenterPanel.transform.Find("ResultText").GetComponent<TextMesh>();
        txtCombo = obCenterPanel.transform.Find("ComboText").GetComponent<TextMesh>();
        txtBreakThemAll = obCenterPanel.transform.Find("BreakThemAllText").GetComponent<TextMesh>();
    }

    public void OnPlayStatusInitPlanet()
    {
        SetTextLanguage();
    }

    public void OnPlayStatusStartPlanet()
    {
        MakeTextTransparent(ref txtSpeed, GameData.UI_TEXT_HALF_ALPHA);
        MakeTextTransparent(ref txtSpeedTitle, GameData.UI_TEXT_HALF_ALPHA);
        MakeTextTransparent(ref txtDistance, GameData.UI_TEXT_ALPHA);
        MakeTextTransparent(ref txtBestScore, GameData.UI_TEXT_HIGH_ALPHA);
        MakeTextTransparent(ref txtBestScoreTitle, GameData.UI_TEXT_HIGH_ALPHA);
        MakeTextTransparent(ref txtScore, GameData.UI_TEXT_HIGH_ALPHA);
        MakeTextTransparent(ref txtGameMode, GameData.UI_TEXT_HIGH_ALPHA);
        MakeTextTransparent(ref txtPlanetInRightPanel, GameData.UI_TEXT_HIGH_ALPHA);
        MakeTextTransparent(ref txtScoreTitle, GameData.UI_TEXT_HIGH_ALPHA);
        MakeTextTransparent(ref txtPlayTimeBack, GameData.UI_TEXT_ALPHA);
        MakeTextTransparent(ref txtPlanetBack, GameData.UI_TEXT_ALPHA);
        MakeTextTransparent(ref txtPlanetCompleteBack, GameData.UI_TEXT_ALPHA);
        MakeTextTransparent(ref txtRoundOverBack, GameData.UI_TEXT_ALPHA);

        UpdatePlanetTextColor();

        ShowStarCoinOnly(false);
        ShowPanels(false, true, false);
        ShowRounOver(false);
        ShowPlanetComplete(false);
        ShowPlanet(true);
    }

    public void OnPlayStatusStartRound()
    {
        ShowStarCoinOnly(false);
        ShowPanels(true, true, true);
        ShowPlanet(false);
        ShowRounOver(false);

        ShowResult(true);
    }
    public void OnPlayStatusReady()
    {
        ShowRoundReadyTime(true);
        if (GameData.RoundData.nPlayMode == PlayMode.BREAK_THEM_ALL)
        {
            ShowBreakThemAll(true);
        }
    }

    public void OnPlayStatusPlay()
    {
        ShowRoundReadyTime(false);
    }

    public void OnPlayStatusEnding()
    {
        ShowRounOver(true);

        if (GameData.RoundData.nPlayMode == PlayMode.BREAK_THEM_ALL)
        {
            ShowBreakThemAll(false);
        }
    }

    public void OnPlayStatusResult()
    {
        ShowRounOver(false);
    }

    public void OnPlayStatusReward()
    {
        ShowStarCoinOnly(true);
    }

    public void OnPlayStatusEndRound()
    {

    }

    public void OnPlayStatusEndPlanet()
    {
        ShowPanels(false, true, false);
        ShowPlanetComplete(true);
    }

    public void OnPlayEventBlockDropSuccess()
    {
        SpeedUp();
        ShowBlockDropResult();
    }

    public void OnPlayEventBlockDropFail()
    {
        SpeedDown();
    }

    void SetTextLanguage()
    {
        txtSpeedTitle.text = LanguageData.strSpeed;
        txtGameMode.text = GameData.GetGameModeNameInLanguage(GameData.nGameMode);
        string strPlanet = GameData.GetPlanetNameInLanguage(GameData.nPlanetType);
        txtPlanet.text = strPlanet;
        txtPlanetBack.text = strPlanet;
        txtPlanetInRightPanel.text = strPlanet;
        txtScoreTitle.text = LanguageData.strScore;
        txtBestScoreTitle.text = LanguageData.strBest;
        txtPlanetComplete.text = strPlanet + " " + LanguageData.strComplete;
        txtPlanetCompleteBack.text = strPlanet + " " + LanguageData.strComplete;
        txtBreakThemAll.text = LanguageData.strBreakThemAll;
    }
}
