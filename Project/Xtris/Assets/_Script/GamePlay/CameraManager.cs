﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour {
    Vector3 posTo;

    float nMoveSpeed = GameData.CAMERA_MOVE_SPEED;
    bool bFollowPlayground = false;

    bool bShake = true;
    float nShakeTime = 0f;
    float nShakeSpeed = GameData.CAMERA_SHAKE_SPEED;
    float nShakeDistanceX = 0f;
    float nShakeDistanceY = 0f;

    Playground obPlayground = null;
    Planet obPlanet = null;
    GameObject obLogoImage;

    void Awake()
    {
        gameObject.name = GameData.OBJECT_NAME_CAMERA;
    }

    void Start()
    {
        CheckScreenResolution();
    }

    void CheckScreenResolution()
    {
        float nScreenRatio = ((float)Screen.currentResolution.height) / Screen.currentResolution.width;
        float nCompRatio = nScreenRatio / GameData.CAMERA_BASE_SCREEN_RATIO;
        float nFOV = GameData.CAMERA_BASE_FOV;
        if (nCompRatio > 1f)
        {
            nFOV = (int)(45.701f * nCompRatio + 14.424);
        }
        Camera obCamera = GetComponent<Camera>();
        obCamera.fieldOfView = nFOV;
    }
    
    void FixedUpdate()
    {
        if (GameData.nPlayStatus < PlayStatus.START_PLANET || GameData.nPlayStatus > PlayStatus.END_PLANET)
        {
            return;
        }

        UpdatePos();
        UpdateShake();
    }

    void UpdatePos()
    {
        // Move size
        posTo = new Vector3(posTo.x, posTo.y, transform.position.z);

        float nStep = nMoveSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, posTo, nStep);
        if ((posTo - transform.position).magnitude < 0.01f)
        {
            transform.position = posTo;
        }

        Vector3 posPlayground = obPlayground.transform.position;
        if (GameData.nPlayStatus == PlayStatus.START_PLANET)
        {
            // force to set playground pos
            transform.position = new Vector3(posPlayground.x, posPlayground.y, posPlayground.z);
        }
        else {
            if (bFollowPlayground) {
                // force to set z position
                transform.position = new Vector3(transform.position.x, transform.position.y, posPlayground.z);
            }

            if (!(GameData.nPlayStatus == PlayStatus.PAUSE && GameData.nPlayStatusOnPause == PlayStatus.START_PLANET))
            {
                // look at a planet
                transform.LookAt(obPlanet.transform);
            }
        }
    }

    void UpdateShake()
    {
        if (bShake && nShakeTime > 0)
        {
            nShakeTime -= Time.deltaTime;

            float nDistanceX = nShakeDistanceX * Mathf.Sin(Time.time * nShakeSpeed);
            float nDistanceY = nShakeDistanceY * Mathf.Sin(Time.time * nShakeSpeed);

            transform.position = new Vector3(posTo.x + nDistanceX, posTo.y + nDistanceY, transform.position.z);
        }
    }

    public void MovePos(float nHoriz, float nVert)
    {
        Vector2 pos = new Vector2(GameData.CAMERA_MOVE_DISTANCE * -nHoriz, GameData.CAMERA_MOVE_DISTANCE * -nVert);
        if (pos.magnitude > GameData.CAMERA_MOVE_DISTANCE)
        {
            pos = GameData.CAMERA_MOVE_DISTANCE * pos.normalized;
        }

        posTo = new Vector3(pos.x, pos.y, transform.position.z);
    }

    void Shake()
    {
        float nRate = obPlayground.nSpeedDownRate;

        bShake = true;
        nShakeTime = GameData.CAMERA_SHAKE_TIME;

        nRate -= (1f - GameData.GAMEPLAY_SPEED_DOWN_RATE_ON_HIT_TO_CAMERA);  // 0 ~ 0.09
        nRate += 0.01f;    // 0.01 ~ 0.1
        nRate *= 10;       // 0.1 ~ 1

        int nSignX = Random.Range(-1, 2);
        int nSignY = -1;
        if (nSignX == 0)    // prevent both sing x and y from being zero
        {
            if (Random.Range(0, 2) == 0) nSignY = 1;
        }
        else
        {
            nSignY = Random.Range(-1, 2);
        }

        nShakeDistanceX = nSignX * GameData.CAMERA_SHAKE_MAX_DISTANCE * nRate;
        nShakeDistanceY = nSignY * GameData.CAMERA_SHAKE_MAX_DISTANCE * nRate;

//        Debug.Log("Rate : " + nRate);
//        Debug.Log("Distance : " + nShakeDistanceX + ", " + nShakeDistanceY);
    }

    public void OnPlayStatusInitComponent()
    {
        obPlayground = GamePlay.GetInstance().obPlayground;

        obLogoImage = transform.Find("LogoImage").gameObject;
    }

    public void OnPlayStatusInitPlanet()
    {
        // Planet should be initialed after INIT_PLANET
        obPlanet = GamePlay.GetInstance().obPlanet;
        obLogoImage.SetActive(false);
    }

    public void OnPlayStatusStartPlanet()
    {
        bFollowPlayground = true;
    }

    public void OnPlayStatusStartRound()
    {
        bFollowPlayground = true;
    }

    public void OnPlayStatusEnding()
    {
        bFollowPlayground = false;
    }

    public void OnPlayStatusReward()
    {
        bFollowPlayground = false;
        transform.position = new Vector3(0f, 0f, GameData.posCameraRewardView.z);
    }

    public void OnPlayEventBlockHitToCamera()
    {
        Shake();
    }
}
