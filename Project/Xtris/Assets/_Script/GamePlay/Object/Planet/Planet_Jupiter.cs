﻿using UnityEngine;
using System.Collections;

public class Planet_Jupiter : Planet
{
    void Awake()
    {
        InitData();
    }

    void InitData()
    {
        dSlope = GameData.Jupiter.dSlope;
        nRotateSpeed = GameData.Jupiter.nRotateSpeed;
        GameData.dPlanetColor = GameData.Jupiter.dPlanetColor;
        GameData.dBackgroundScale = GameData.Jupiter.dBackgroundScale;
        GameData.posCameraCloseView = GameData.Jupiter.posCameraCloseView;
        GameData.posCameraRewardView = GameData.Jupiter.posCameraRewardView;

        if (GameData.nGameMode == GameMode.NORMAL)
        {
            GameData.nPlanetStartDistance = GameData.Jupiter.Normal.listDistance[0];
            GameData.nPlanetEndDistance = GameData.Jupiter.Normal.listDistance[GameData.Jupiter.Normal.nMaxRound];
        }
        else 
        {
            GameData.nPlanetStartDistance = GameData.Jupiter.Challenge.listDistance[0];
            GameData.nPlanetEndDistance = GameData.Jupiter.Challenge.listDistance[GameData.Jupiter.Challenge.nMaxRound];
        }
    }

    public override void UpdateRoundData()
    {
        int nMaxRound = (GameData.nGameMode == GameMode.NORMAL) ? GameData.Jupiter.Normal.nMaxRound : GameData.Jupiter.Challenge.nMaxRound;
        if (GameData.nRound <= 0 || GameData.nRound > nMaxRound)
        {
            return;
        }

        if (GameData.Test.bTest && GameData.Test.nTestRound != 0)
        {
            GameData.nRound = GameData.Test.nTestRound;
        }

        int nIndex = GameData.nRound - 1;

        if (GameData.nGameMode == GameMode.NORMAL)
        {
            GameData.RoundData.nMaxRound = GameData.Jupiter.Normal.nMaxRound;

            GameData.RoundData.nPlayMode = GameData.Jupiter.Normal.listPlayMode[nIndex];
            GameData.RoundData.nBlockMode = GameData.Jupiter.Normal.listBlockMode[nIndex];

            GameData.RoundData.nStartDistance = GameData.Jupiter.Normal.listDistance[nIndex];
            GameData.RoundData.nEndDistance = GameData.Jupiter.Normal.listDistance[nIndex + 1];
            GameData.RoundData.nBlockStartDistance = GameData.Jupiter.Normal.listBlockDistance[nIndex];
            GameData.RoundData.nPlayTime = GameData.Jupiter.Normal.listPlayTime[nIndex];
            GameData.RoundData.nSpeed = GameData.Jupiter.Normal.listSpeed[nIndex];
            GameData.RoundData.nRotateSpeed = GameData.Jupiter.Normal.listRotateSpeed[nIndex];
            GameData.RoundData.nCellMaxMoveDistance = GameData.Jupiter.Normal.listCellMoveMaxDistance[nIndex];
            GameData.RoundData.nCellMoveSpeed = GameData.Jupiter.Normal.listCellMoveSpeed[nIndex];
            GameData.RoundData.nCellMoveDepth = GameData.Jupiter.Normal.listCellMoveDepth[nIndex];
        }
        else
        {
            GameData.RoundData.nMaxRound = GameData.Jupiter.Challenge.nMaxRound;

            GameData.RoundData.nPlayMode = GameData.Jupiter.Challenge.listPlayMode[nIndex];
            GameData.RoundData.nBlockMode = GameData.Jupiter.Challenge.listBlockMode[nIndex];

            GameData.RoundData.nStartDistance = GameData.Jupiter.Challenge.listDistance[nIndex];
            GameData.RoundData.nEndDistance = GameData.Jupiter.Challenge.listDistance[nIndex + 1];
            GameData.RoundData.nBlockStartDistance = GameData.Jupiter.Challenge.listBlockDistance[nIndex];
            GameData.RoundData.nPlayTime = GameData.Jupiter.Challenge.listPlayTime[nIndex];
            GameData.RoundData.nSpeed = GameData.Jupiter.Challenge.listSpeed[nIndex];
            GameData.RoundData.nRotateSpeed = GameData.Jupiter.Challenge.listRotateSpeed[nIndex];
            GameData.RoundData.nCellMaxMoveDistance = GameData.Jupiter.Challenge.listCellMoveMaxDistance[nIndex];
            GameData.RoundData.nCellMoveSpeed = GameData.Jupiter.Challenge.listCellMoveSpeed[nIndex];
            GameData.RoundData.nCellMoveDepth = GameData.Jupiter.Challenge.listCellMoveDepth[nIndex];
        }

        if (GameData.Test.bTest && GameData.Test.nTestStartDistance != 0f)
        {
            GameData.RoundData.nStartDistance = GameData.Test.nTestStartDistance;
        }

        if (GameData.Test.bTest && GameData.Test.nTestQuickClearRound)
        {
            GameData.RoundData.nStartDistance = GameData.RoundData.nEndDistance + 50f;
        }
    }
}
