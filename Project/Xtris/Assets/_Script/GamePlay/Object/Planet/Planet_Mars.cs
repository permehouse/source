﻿using UnityEngine;
using System.Collections;

public class Planet_Mars : Planet
{
    void Awake()
    {
        InitData();
    }

    void InitData()
    {
        dSlope = GameData.Mars.dSlope;
        nRotateSpeed = GameData.Mars.nRotateSpeed;
        GameData.dBackgroundScale = GameData.Mars.dBackgroundScale;
        GameData.dPlanetColor = GameData.Mars.dPlanetColor;
        GameData.posCameraCloseView = GameData.Mars.posCameraCloseView;
        GameData.posCameraRewardView = GameData.Mars.posCameraRewardView;

        if (GameData.nGameMode == GameMode.NORMAL)
        {
            GameData.nPlanetStartDistance = GameData.Mars.Normal.listDistance[0];
            GameData.nPlanetEndDistance = GameData.Mars.Normal.listDistance[GameData.Mars.Normal.nMaxRound];
        }
        else
        {
            GameData.nPlanetStartDistance = GameData.Mars.Challenge.listDistance[0];
            GameData.nPlanetEndDistance = GameData.Mars.Challenge.listDistance[GameData.Mars.Challenge.nMaxRound];
        }
    }

    public override void UpdateRoundData()
    {
        int nMaxRound = (GameData.nGameMode == GameMode.NORMAL) ? GameData.Mars.Normal.nMaxRound : GameData.Mars.Challenge.nMaxRound;
        if (GameData.nRound <= 0 || GameData.nRound > nMaxRound)
        {
            return;
        }

        if (GameData.Test.bTest && GameData.Test.nTestRound != 0)
        {
            GameData.nRound = GameData.Test.nTestRound;
        }

        int nIndex = GameData.nRound - 1;
        if (GameData.nGameMode == GameMode.NORMAL)
        {
            GameData.RoundData.nMaxRound = GameData.Mars.Normal.nMaxRound;
            GameData.RoundData.nPlayMode = GameData.Mars.Normal.listPlayMode[nIndex];
            GameData.RoundData.nBlockMode = GameData.Mars.Normal.listBlockMode[nIndex];

            GameData.RoundData.nStartDistance = GameData.Mars.Normal.listDistance[nIndex];
            GameData.RoundData.nEndDistance = GameData.Mars.Normal.listDistance[nIndex + 1];
            GameData.RoundData.nBlockStartDistance = GameData.Mars.Normal.listBlockDistance[nIndex];
            GameData.RoundData.nPlayTime = GameData.Mars.Normal.listPlayTime[nIndex];
            GameData.RoundData.nSpeed = GameData.Mars.Normal.listSpeed[nIndex];
            GameData.RoundData.nRotateSpeed = GameData.Mars.Normal.listRotateSpeed[nIndex];
            GameData.RoundData.nCellMaxMoveDistance = GameData.Mars.Normal.listCellMoveMaxDistance[nIndex];
            GameData.RoundData.nCellMoveSpeed = GameData.Mars.Normal.listCellMoveSpeed[nIndex];
            GameData.RoundData.nCellMoveDepth = GameData.Mars.Normal.listCellMoveDepth[nIndex];
        }
        else
        {
            GameData.RoundData.nMaxRound = GameData.Mars.Challenge.nMaxRound;
            GameData.RoundData.nPlayMode = GameData.Mars.Challenge.listPlayMode[nIndex];
            GameData.RoundData.nBlockMode = GameData.Mars.Challenge.listBlockMode[nIndex];

            GameData.RoundData.nStartDistance = GameData.Mars.Challenge.listDistance[nIndex];
            GameData.RoundData.nEndDistance = GameData.Mars.Challenge.listDistance[nIndex + 1];
            GameData.RoundData.nBlockStartDistance = GameData.Mars.Challenge.listBlockDistance[nIndex];
            GameData.RoundData.nPlayTime = GameData.Mars.Challenge.listPlayTime[nIndex];
            GameData.RoundData.nSpeed = GameData.Mars.Challenge.listSpeed[nIndex];
            GameData.RoundData.nRotateSpeed = GameData.Mars.Challenge.listRotateSpeed[nIndex];
            GameData.RoundData.nCellMaxMoveDistance = GameData.Mars.Challenge.listCellMoveMaxDistance[nIndex];
            GameData.RoundData.nCellMoveSpeed = GameData.Mars.Challenge.listCellMoveSpeed[nIndex];
            GameData.RoundData.nCellMoveDepth = GameData.Mars.Challenge.listCellMoveDepth[nIndex];
        }
        
        if (GameData.Test.bTest && GameData.Test.nTestStartDistance != 0f)
        {
            GameData.RoundData.nStartDistance = GameData.Test.nTestStartDistance;
        }

        if (GameData.Test.bTest && GameData.Test.nTestQuickClearRound)
        {
            GameData.RoundData.nStartDistance = GameData.RoundData.nEndDistance + 50f;
        }
    }
}
