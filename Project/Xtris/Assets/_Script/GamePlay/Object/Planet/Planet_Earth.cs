﻿using UnityEngine;
using System.Collections;

public class Planet_Earth : Planet
{
    void Awake()
    {
        InitData();
    }

    void InitData()
    {
        dSlope = GameData.Earth.dSlope;
        nRotateSpeed = GameData.Earth.nRotateSpeed;
        GameData.dPlanetColor = GameData.Earth.dPlanetColor;
        GameData.dBackgroundScale = GameData.Earth.dBackgroundScale;
        GameData.posCameraCloseView = GameData.Earth.posCameraCloseView;
        GameData.posCameraRewardView = GameData.Earth.posCameraRewardView;

        if (GameData.nGameMode == GameMode.NORMAL)
        {
            GameData.nPlanetStartDistance = GameData.Earth.Normal.listDistance[0];
            GameData.nPlanetEndDistance = GameData.Earth.Normal.listDistance[GameData.Earth.Normal.nMaxRound];
        }
        else
        {
            GameData.nPlanetStartDistance = GameData.Earth.Challenge.listDistance[0];
            GameData.nPlanetEndDistance = GameData.Earth.Challenge.listDistance[GameData.Earth.Challenge.nMaxRound];
        }
    }

    public override void UpdateRoundData()
    {
        int nMaxRound = (GameData.nGameMode == GameMode.NORMAL) ? GameData.Earth.Normal.nMaxRound : GameData.Earth.Challenge.nMaxRound;
        if (GameData.nRound <= 0 || GameData.nRound > nMaxRound)
        {
            return;
        }

        if (GameData.Test.bTest && GameData.Test.nTestRound != 0)
        {
            GameData.nRound = GameData.Test.nTestRound;
        }

        int nIndex = GameData.nRound - 1;

        if (GameData.nGameMode == GameMode.NORMAL) {
            GameData.RoundData.nMaxRound = GameData.Earth.Normal.nMaxRound;
            GameData.RoundData.nPlayMode= GameData.Earth.Normal.listPlayMode[nIndex];
            GameData.RoundData.nBlockMode = GameData.Earth.Normal.listBlockMode[nIndex];

            GameData.RoundData.nStartDistance = GameData.Earth.Normal.listDistance[nIndex];
            GameData.RoundData.nEndDistance = GameData.Earth.Normal.listDistance[nIndex + 1];
            GameData.RoundData.nBlockStartDistance = GameData.Earth.Normal.listBlockDistance[nIndex];
            GameData.RoundData.nPlayTime = GameData.Earth.Normal.listPlayTime[nIndex];
            GameData.RoundData.nSpeed = GameData.Earth.Normal.listSpeed[nIndex];
            GameData.RoundData.nRotateSpeed = GameData.Earth.Normal.listRotateSpeed[nIndex];
            GameData.RoundData.nCellMaxMoveDistance = GameData.Earth.Normal.listCellMoveMaxDistance[nIndex];
            GameData.RoundData.nCellMoveSpeed = GameData.Earth.Normal.listCellMoveSpeed[nIndex];
            GameData.RoundData.nCellMoveDepth = GameData.Earth.Normal.listCellMoveDepth[nIndex];
        }
        else
        {
            GameData.RoundData.nMaxRound = GameData.Earth.Challenge.nMaxRound;
            GameData.RoundData.nPlayMode= GameData.Earth.Challenge.listPlayMode[nIndex];
            GameData.RoundData.nBlockMode = GameData.Earth.Challenge.listBlockMode[nIndex];

            GameData.RoundData.nStartDistance = GameData.Earth.Challenge.listDistance[nIndex];
            GameData.RoundData.nEndDistance = GameData.Earth.Challenge.listDistance[nIndex + 1];
            GameData.RoundData.nBlockStartDistance = GameData.Earth.Challenge.listBlockDistance[nIndex];
            GameData.RoundData.nPlayTime = GameData.Earth.Challenge.listPlayTime[nIndex];
            GameData.RoundData.nSpeed = GameData.Earth.Challenge.listSpeed[nIndex];
            GameData.RoundData.nRotateSpeed = GameData.Earth.Challenge.listRotateSpeed[nIndex];
            GameData.RoundData.nCellMaxMoveDistance = GameData.Earth.Challenge.listCellMoveMaxDistance[nIndex];
            GameData.RoundData.nCellMoveSpeed = GameData.Earth.Challenge.listCellMoveSpeed[nIndex];
            GameData.RoundData.nCellMoveDepth = GameData.Earth.Challenge.listCellMoveDepth[nIndex];
        }

        if (GameData.Test.bTest && GameData.Test.nTestStartDistance != 0f)
        {
            GameData.RoundData.nStartDistance = GameData.Test.nTestStartDistance;
        }

        if (GameData.Test.bTest && GameData.Test.nTestQuickClearRound)
        {
            GameData.RoundData.nStartDistance = GameData.RoundData.nEndDistance + 50f;
        }
    }
}
