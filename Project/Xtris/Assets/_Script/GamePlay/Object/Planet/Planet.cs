﻿using UnityEngine;
using System.Collections;

public abstract class Planet : MonoBehaviour {
    protected float nRotateSpeed = 10f;
    protected Vector3 dSlope;

    Quaternion rotTo;

	// Use this for initialization
	void Start () {
        transform.eulerAngles = dSlope;
        rotTo = transform.rotation;
	}
	
	// Update is called once per frame
    void FixedUpdate()
    {
        UpdateRotation();
    }

    void UpdateRotation()
    {
        rotTo.eulerAngles += Vector3.up * nRotateSpeed * Time.deltaTime;
        transform.localRotation = rotTo;
    }

    public abstract void UpdateRoundData();
}
