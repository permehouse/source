﻿using UnityEngine;
using System.Collections;

public class SpawnObjectPool {
    SpawnObject[] listObject = null;
    int nCountObject = 0;
    int nIndexObject = 0;

    public void CreatePool(int nCount, SpawnObject pfObject, Transform trParent)
    {
        nCountObject = nCount;
        listObject = new SpawnObject[nCountObject];
        for (int i = 0; i < nCountObject; i++)
        {
            Vector3 pos = new Vector3(0f, 0f, 0f);
            SpawnObject obj = MonoBehaviour.Instantiate(pfObject, pos, Quaternion.identity) as SpawnObject;
            obj.trParent = trParent;
            obj.Free();

            listObject[i] = obj;
        }
    }

    public void DestroyPool()
    {
        for (int i = 0; i < nCountObject; i++)
        {
            SpawnObject obj = listObject[i];
            if (obj)
            {
                obj.Destroy();
                listObject[i] = null;
            }
        }
    }

    public void FreeAll()
    {
        for (int i = 0; i < nCountObject; i++)
        {
            SpawnObject obj = listObject[i];
            if (obj)
            {
                obj.Free();
            }
        }
    }

    public SpawnObject GetFreeObject()
    {
        int nIndexObjectFirst = nIndexObject;
        do
        {
            nIndexObject++;
            if (nIndexObject >= nCountObject)
            {
                nIndexObject = 0;
            }

            SpawnObject obj = listObject[nIndexObject];
            if (obj && !obj.bSpawn)
            {
                return obj;
            }

        } while (nIndexObject != nIndexObjectFirst);

        return null;
    }

    public SpawnObject GetObject(int nIndex)
    {
        if (nIndex < 0 || nIndex >= nCountObject)
        {
            return null;
        }

        return listObject[nIndex];
    }
}
