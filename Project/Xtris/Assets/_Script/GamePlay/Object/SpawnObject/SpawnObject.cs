﻿using UnityEngine;
using System.Collections;

public class SpawnObject : MonoBehaviour {
    public bool bSpawn = false;
    public Transform trParent = null;

    public virtual void Free()
    {
        bSpawn = false;
        transform.parent = trParent;
        gameObject.SetActive(false);
    }
    public void Free(float nDelay)
    {
        // this invoke should be call unless the scene is over.
        Invoke("Free", nDelay);
    }

    public void Destroy()
    {
        DestroyObject(gameObject);
    }

    public virtual void Spawn(Vector3 pos)
    {
        bSpawn = true;
        transform.position = pos;
        gameObject.SetActive(true);
    }
}
