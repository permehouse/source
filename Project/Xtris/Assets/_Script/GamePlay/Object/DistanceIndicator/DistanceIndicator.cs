﻿using UnityEngine;
using System.Collections;

public class DistanceIndicator : MonoBehaviour {
    GameObject obIndicatorBars = null;
    GameObject obHalfFlag = null;
    GameObject obEndFlag = null;

    SpawnObjectPool iIndicatorBarPool = new SpawnObjectPool();

    CameraManager obCamera = null;

    void Awake()
    {
        gameObject.name = GameData.OBJECT_NAME_DISTANCE_INDICATOR;
    }

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void LateUpdate()
    {
        UpdatePos();
    }

    void UpdatePos()
    {
        if (GameData.nPlayStatus < PlayStatus.START_ROUND || GameData.nPlayStatus > PlayStatus.END_ROUND)
        {
            return;
        }

        Vector3 posCamera = obCamera.transform.position;
        transform.localPosition = new Vector3(GameData.DISTANCE_INDICATOR_POS_X + posCamera.x, GameData.DISTANCE_INDICATOR_POS_Y + posCamera.y, 0f);
    }

    void SpawnIndicatorBars()
    {
        float nStartDistance = -GameData.RoundData.nStartDistance;
        float nEndDistance = -GameData.RoundData.nEndDistance;;
        float nDistance = nStartDistance;
        for (int i = 0; i < GameData.SpawnObjectPool.INDICATOR_BAR_POOL_SIZE; i++)
        {
            if (nDistance > nEndDistance) {
                return;
            }

            IndicatorBar obIndicatorBar = (IndicatorBar)iIndicatorBarPool.GetObject(i);
            if (obIndicatorBar == null) {
                return;
            }
            obIndicatorBar.Spawn(new Vector3(transform.position.x, transform.position.y, nDistance));

            nDistance += GameData.DISTANCE_INDICATOR_BAR_GAP;
        }
    }

    void SetFlagPos()
    {
        float nStartDistance = -GameData.RoundData.nStartDistance;
        float nEndDistance = -GameData.RoundData.nEndDistance;
        float nHalfDistance = (nStartDistance + nEndDistance) / 2f;

        obHalfFlag.transform.position = new Vector3(transform.position.x, transform.position.y, nHalfDistance);
        SpriteRenderer obSpriteRenderer = obHalfFlag.GetComponent<SpriteRenderer>();
        obSpriteRenderer.color = new Color(1f, 1f, 1f, GameData.DISTANCE_INDICATOR_FLAG_ALPHA);

        obEndFlag.transform.position = new Vector3(transform.position.x, transform.position.y, nEndDistance);
        obSpriteRenderer = obEndFlag.GetComponent<SpriteRenderer>();
        obSpriteRenderer.color = new Color(1f, 1f, 1f, GameData.DISTANCE_INDICATOR_FLAG_ALPHA);
    }

    void Show(bool bShow)
    {
        obIndicatorBars.SetActive(bShow);
        obHalfFlag.SetActive(bShow);
        obEndFlag.SetActive(bShow);
    }

    public void OnPlayStatusInitComponent()
    {
        obCamera = GamePlay.GetInstance().obCamera;

        obIndicatorBars = transform.Find("IndicatorBars").gameObject;
        obHalfFlag = transform.Find("HalfFlag").gameObject;
        obEndFlag = transform.Find("EndFlag").gameObject;

        iIndicatorBarPool.CreatePool(GameData.SpawnObjectPool.INDICATOR_BAR_POOL_SIZE, GamePlay.GetInstance().pfIndicatorBar, obIndicatorBars.transform);
    }

    public void OnPlayStatusStartRound()
    {
        transform.localPosition = new Vector3(GameData.DISTANCE_INDICATOR_POS_X, GameData.DISTANCE_INDICATOR_POS_Y, 0);
    }

    public void OnPlayStatusReady()
    {
        Show(true);
        SpawnIndicatorBars();
        SetFlagPos();
    }
    public void OnPlayStatusReward()
    {
        Show(false);
    }

    public void OnPlayStatusEnding()
    {
        iIndicatorBarPool.FreeAll();
    }

    public void OnPlayStatusDestoryComponent()
    {
        iIndicatorBarPool.DestroyPool();
    }
}
