﻿using UnityEngine;
using System.Collections;

public class IndicatorBar : SpawnObject {
    CameraManager obCamera = null;

    public override void Spawn(Vector3 pos) {
        obCamera = GamePlay.GetInstance().obCamera;

        transform.localScale = new Vector3(GameData.DISTANCE_INDICATOR_BAR_SIZE, GameData.DISTANCE_INDICATOR_BAR_HEIGHT, GameData.DISTANCE_INDICATOR_BAR_SIZE);

        Quaternion rot = transform.localRotation; 
        rot.eulerAngles = new Vector3(90f, 0f, 0f);
        transform.localRotation = rot;

        MeshRenderer renderer = GetComponent<MeshRenderer>();
        renderer.material.color = new Color(GameData.dPlanetColor.r, GameData.dPlanetColor.g, GameData.dPlanetColor.b, GameData.DISTANCE_INDICATOR_BAR_ALPHA);

        base.Spawn(pos);
	}

    void LateUpdate()
    {
        if (GameData.nPlayStatus != PlayStatus.PLAY)
        {
            return;
        }

        CheckPassed();
    }

    void CheckPassed()
    {
        if (transform.position.z <= obCamera.transform.position.z)
        {
            float nEndDistance = -GameData.RoundData.nEndDistance;
            float nDistance = transform.position.z;
            nDistance += GameData.DISTANCE_INDICATOR_BAR_GAP * GameData.SpawnObjectPool.INDICATOR_BAR_POOL_SIZE;

            if (nDistance > nEndDistance)
            {
                Free();
            }
            else
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, nDistance);
            }
        }
    }
}
