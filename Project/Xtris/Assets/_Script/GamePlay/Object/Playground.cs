﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class Playground : MonoBehaviour
{
    float nMoveSpeed = 0f;
    float nMoveSpeedTo = 20f;

    float nRotateSpeed = 0f;
    float nRotateSlerpRate = 0.5f;
    Quaternion rotTo;

    float nStartDistance = 0f;
    float nEndDistance = 0f;
    float nEndingDistance = 0f;

    Vector3 posTo;
    float nMoveLerpRate = 0f;

    float nPlanetShowTime = 0f;

    [HideInInspector]
    public float nSpeedDownRate = 0f;


    void Awake()
    {
        gameObject.name = GameData.OBJECT_NAME_PLAYGROUND;
    }

    void Start()
    {
    }

    void FixedUpdate()
    {
        UpdatePos();

        if (GameData.nPlayStatus < PlayStatus.START_PLANET || GameData.nPlayStatus > PlayStatus.END_PLANET)
        {
            return;
        }

        UpdateRotation();
    }

    void UpdatePos()
    {
        if (GameData.nPlayStatus == PlayStatus.START_PLANET) {
            nPlanetShowTime -= Time.deltaTime;
            if (nPlanetShowTime <= 0f)
            {
                if ((transform.position - posTo).magnitude < 0.01f)
                {
                    transform.position = posTo;
                    GamePlay.GetInstance().SetPlayStatus(PlayStatus.START_ROUND);
                }
                else
                {
                    transform.position = Vector3.Lerp(transform.position, posTo, nMoveLerpRate);
                }
            }
        }
        else if (GameData.nPlayStatus >= PlayStatus.READY && GameData.nPlayStatus <= PlayStatus.ENDING)
        {
            nMoveSpeed += (nMoveSpeedTo - nMoveSpeed) * Time.deltaTime;

            float nNextDistance = transform.position.z + nMoveSpeed * Time.deltaTime;
            transform.localPosition = new Vector3(transform.position.x, transform.position.y, nNextDistance);
        }
    }

    void UpdateRotation() {
        if (GameData.nPlayStatus != PlayStatus.PLAY && GameData.nPlayStatus != PlayStatus.ENDING)
        {
            return;
        }

        rotTo.eulerAngles += transform.forward * nRotateSpeed * Time.deltaTime;
        if ((rotTo.eulerAngles - transform.rotation.eulerAngles).magnitude < 0.01f)
        {
            transform.rotation = rotTo;
        }
        else
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, rotTo, nRotateSlerpRate);
        }
    }

    public void SpeedUp()
    {
        float nMaxRate;
        switch (GameData.nMatchResult)
        {
            case MatchResult.PERFECT : 
                nMaxRate = GameData.GAMEPLAY_SPEED_UP_RATE_ON_MATCH_RESULT_PERFECT; 
                break;
            case MatchResult.GREAT : 
                nMaxRate = GameData.GAMEPLAY_SPEED_UP_RATE_ON_MATCH_RESULT_GREAT; 
                break;
            default :
                nMaxRate = GameData.GAMEPLAY_SPEED_UP_RATE;
                break;
        }

        float nRate = Random.Range(1.01f, 1f + nMaxRate);
        nMoveSpeedTo = nMoveSpeed * nRate;
    }

    public void SpeedDown(bool bHitToCamera = false)
    {
        float nRate = 0f;
        if (bHitToCamera)
        {
            nRate = Random.Range(1f - GameData.GAMEPLAY_SPEED_DOWN_RATE_ON_HIT_TO_CAMERA, 0.99f);
        }
        else
        {
            nRate = Random.Range(1f - GameData.GAMEPLAY_SPEED_DOWN_RATE, 0.99f);
        }

        nMoveSpeedTo = nMoveSpeedTo * nRate;

        // for using camera shake
        nSpeedDownRate = nRate;
    }

    public float GetDistance()
    {
        float nDistance;
        if (GameData.nPlayStatus == PlayStatus.READY)
        {
            nDistance = nEndDistance - nStartDistance;
        }
        else if (GameData.nPlayStatus == PlayStatus.ENDING || GameData.nPlayStatus == PlayStatus.RESULT
            || GameData.nPlayStatus == PlayStatus.END_ROUND || GameData.nPlayStatus == PlayStatus.END_PLANET)
        {
            nDistance = Mathf.Max(0f, nEndDistance - nEndingDistance);
        }
        else
        {
            nDistance = Mathf.Max(0f, nEndDistance - transform.position.z);
        }

        return nDistance;
    }

    public float GetSpeed()
    {
        return nMoveSpeed;
    }

    public void OnPlayStatusStartPlanet()
    {
        transform.position = GameData.posCameraCloseView;

        float nReadyTime = GameData.ROUND_READY_TIME + 0.5f;
        float nDistance = -GameData.RoundData.nStartDistance - nReadyTime * nMoveSpeed;
        posTo = new Vector3(0f, 0f, nDistance);

        nMoveLerpRate = GameData.PLAYGROUND_START_PLANET_MOVE_LERP_RATE;

        nPlanetShowTime = GameData.PLAYGROUND_START_PLANET_SHOW_TIME;
    }

    public void OnPlayStatusStartRound()
    {
        // Move
        nMoveSpeed = GameData.RoundData.nSpeed;
        nMoveSpeedTo = nMoveSpeed;

        // Rotation
        rotTo = transform.rotation;
        nRotateSpeed = GameData.RoundData.nRotateSpeed;

        // Set Distance
        nStartDistance = -GameData.RoundData.nStartDistance;
        nEndDistance = -GameData.RoundData.nEndDistance;

        // Set Ready Place
        float nReadyTime = GameData.ROUND_READY_TIME + 0.5f;
        float nDistance = nStartDistance - nReadyTime * nMoveSpeed;

        if (GameData.Test.bTest && GameData.Test.bSkipReady)
        {
            nDistance = nStartDistance;
        }

        transform.position = new Vector3(0f, 0f, nDistance);
    }

    public void OnPlayStatusReady()
    {
    }

    public void OnPlayStatusEnding()
    {
        nEndingDistance = transform.position.z;

        if (GameData.nRoundResult == RoundResult.ROUND_CLEAR)
        {
            nRotateSpeed *= GameData.ROUND_ENDING_PLAYGROUND_ROTATE_RATE;
            InvokeManager.GetInstance().SetInvoke(this, "FinishEndingRotate", GameData.ROUND_ENDING_PLAYGROUND_ROTATE_TIME);
        }
    }

    void FinishEndingRotate()
    {
        // restore original rotation speed
        nRotateSpeed /= GameData.ROUND_ENDING_PLAYGROUND_ROTATE_RATE;
    }

    public void OnPlayStatusEndRound()
    {
    }

    public void OnPlayEventBlockDropSuccess()
    {
        SpeedUp();
    }

    public void OnPlayEventBlockDropFail()
    {
        SpeedDown();
    }

    public void OnPlayEventBlockHitToCamera()
    {
        SpeedDown(true);
    }
}
