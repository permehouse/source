﻿using UnityEngine;
using System.Collections;

public class Star : SpawnObject {
    CameraManager obCamera = null;
    float nRotateSpeedY = 0f;

    Vector3 posTo;
    float nMoveSpeed = 0f;
    float nMoveLerpRate = GameData.STAR_MOVE_LERP_RATE;

    public bool bReward = false;
    public override void Spawn(Vector3 pos) {
        obCamera = GamePlay.GetInstance().obCamera;

        float nRotateZ = Random.Range(0f, 359f);
        Quaternion rot = transform.localRotation;
        rot.eulerAngles = new Vector3(rot.z, rot.y, nRotateZ);
        transform.localRotation = rot;

        int nSign = (Random.Range(0, 2) == 0) ? -1 : 1;
        nRotateSpeedY = nSign * Random.Range(GameData.STAR_ROTATE_MIN_SPEED, GameData.STAR_ROTATE_MAX_SPEED);

        nMoveSpeed = Random.Range(GameData.STAR_MOVE_MIN_SPEED, GameData.STAR_MOVE_MAX_SPEED);

        float nDistanceX = Random.Range(-GameData.STAR_DEST_RANGE, GameData.STAR_DEST_RANGE);
        float nDistanceY = Random.Range(-GameData.STAR_DEST_RANGE, GameData.STAR_DEST_RANGE);

        if (GameData.nPlayStatus == PlayStatus.REWARD)
        {
            posTo = obCamera.transform.position;
            nMoveSpeed *= GameData.REWARD_STAR_SPEED_RATE;
        }
        else
        {
            posTo = new Vector3(nDistanceX, nDistanceY, obCamera.transform.position.z + GameData.STAR_START_DISTANCE);
        }

        base.Spawn(pos);
    }

    // Update is called once per frame
    void Update()
    {
        if (!bSpawn)
        {
            return;
        }

        if (GameData.nPlayStatus < PlayStatus.START_PLANET || GameData.nPlayStatus > PlayStatus.END_PLANET)
        {
            return;
        }

        UpdatePos();
        UpdateRotation();
    }

    void LateUpdate()
    {
        if (GameData.nPlayStatus < PlayStatus.START_PLANET || GameData.nPlayStatus > PlayStatus.END_PLANET)
        {
            return;
        }

        CheckPassed();
    }

    void UpdatePos()
    {
        if (Mathf.Abs(transform.position.z - obCamera.transform.position.z) >= GameData.STAR_START_DISTANCE)
        {
            posTo = new Vector3(posTo.x, posTo.y, obCamera.transform.position.z);
            if ((transform.position - posTo).magnitude < 0.01f)
            {
                transform.position = posTo;
            }
            else
            {
                transform.position = Vector3.Lerp(transform.position, posTo, nMoveLerpRate);
            }
        }
        else {
            if (GameData.nPlayStatus == PlayStatus.REWARD)
            {
                float nStep = nMoveSpeed * Time.deltaTime;
                transform.position = Vector3.MoveTowards(transform.position, posTo, nStep);
            }
            else
            {
                float nPlaygroundMoveSpeed = 0f;
                if (GameData.nPlayStatus == PlayStatus.PLAY) nPlaygroundMoveSpeed = GamePlay.GetInstance().obPlayground.GetSpeed();

                float nPlaygroundMoveDistance = nPlaygroundMoveSpeed * Time.deltaTime;
                float nPredictZ = obCamera.transform.position.z + GameData.STAR_PREDICT_DISTANCE_RATE * nPlaygroundMoveDistance;

                if (transform.position.z <= nPredictZ)
                {
                    posTo = new Vector3(posTo.x, posTo.y, obCamera.transform.position.z - 0.1f);
                }
                else
                {
                    posTo = new Vector3(posTo.x, posTo.y, nPredictZ);
                }

                float nStep = nMoveSpeed * Time.deltaTime;
                transform.position = Vector3.MoveTowards(transform.position, posTo, nStep);
            }
        }
    }

    void UpdateRotation()
    {
        Vector3 rot = new Vector3(0, nRotateSpeedY, 0);
        rot *= Time.deltaTime;

        Quaternion rotTo = transform.localRotation;
        rotTo.eulerAngles += rot;
        transform.localRotation = rotTo;
    }

    void CheckPassed()
    {
        if (transform.position.z <= obCamera.transform.position.z) {
            Free();
            if (bReward)
            {
                GamePlay.GetInstance().SetPlayEvent(PlayEvent.STAR_HIT);
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Free();
        GamePlay.GetInstance().SetPlayEvent(PlayEvent.STAR_HIT);
    }
}
