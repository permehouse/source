﻿using UnityEngine;
using System.Collections;

public class Stars : MonoBehaviour {
    CameraManager obCamera = null;

    SpawnObjectPool iStarPool = new SpawnObjectPool();

    bool bFinishRewardStars = false;

    bool bSpawnNewStar = false;
    float nSpawnNewStarDelay = 0f;

    void Awake()
    {
        gameObject.name = GameData.OBJECT_NAME_STARS;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        CheckFinishRewardStars();
        CheckSpawnNewStar();
	}

    void CheckFinishRewardStars()
    {
        if (GameData.nPlayStatus == PlayStatus.REWARD && !bFinishRewardStars)
        {
            for (int i = 0; i < GameData.SpawnObjectPool.STAR_POOL_SIZE; i++)
            {
                Star obStar = (Star)iStarPool.GetObject(i);
                if (obStar && obStar.bSpawn && obStar.bReward)
                {
                    return;
                }
            }

            bFinishRewardStars = true;
            InvokeManager.GetInstance().SetInvoke(GamePlay.GetInstance(), "SetPlayStatus", PlayStatus.AFTER_REWARD, GameData.AFTER_REWARD_DELAY);
        }
    }

    void CheckSpawnNewStar()
    {
        if (!bSpawnNewStar)
        {
            return;
        }

        if (GameData.nPlayStatus != PlayStatus.PLAY)
        {
            return;
        }

        nSpawnNewStarDelay -= Time.deltaTime;
        if (nSpawnNewStarDelay < 0f)
        {
            bSpawnNewStar = false;
            SpawnNewStar();
        }
    }

    void SpawnNewStar()
    {
        Star obStar = (Star)iStarPool.GetFreeObject();
        if (obStar == null)
        {
            return;
        }

        float nDistanceX = Mathf.Abs(obCamera.transform.position.z * 0.5f);
        float nDistanceY = Mathf.Abs(obCamera.transform.position.z * 0.6f * 0.5f);
        float nX = Random.Range(-nDistanceX, nDistanceX);
        float nY = Random.Range(-nDistanceY, nDistanceY);

        obStar.bReward = false;
        obStar.Spawn(new Vector3(nX, nY, 0));

        bSpawnNewStar = true;
        nSpawnNewStarDelay = Random.Range(GameData.STAR_SPAWN_MIN_DELAY, GameData.STAR_SPAWN_MAX_DELAY);
    }

    void SpawnRewardStars()
    {
        if (GameData.nPlayStatus == PlayStatus.REWARD)
        {
            int nCount = GameData.StartCondition.nReward;
            for (int i = 0; i < nCount; i++)
            {
                Star obStar = (Star)iStarPool.GetFreeObject();
                if (obStar == null)
                {
                    return;
                }

                float nDistanceX = GameData.posCameraRewardView.x;
                float nDistanceY = GameData.posCameraRewardView.y;
                float nX = Random.Range(-nDistanceX, nDistanceX);
                float nY = Random.Range(-nDistanceY, nDistanceY);

                obStar.bReward = true;
                obStar.Spawn(new Vector3(nX, nY, 0));
            }
        }
    }

    public void OnPlayStatusInitComponent()
    {
        obCamera = GamePlay.GetInstance().obCamera;

        iStarPool.CreatePool(GameData.SpawnObjectPool.STAR_POOL_SIZE, GamePlay.GetInstance().pfStar, transform);
    }

    public void OnPlayStatusStartPlanet()
    {
        bSpawnNewStar = false;
        iStarPool.FreeAll();
    }

    public void OnPlayStatusStartRound()
    {
        bSpawnNewStar = false;
        iStarPool.FreeAll();
    }

    public void OnPlayStatusPlay()
    {
        SpawnNewStar();
    }

    public void OnPlayStatusEndRound()
    {

    }
    public void OnPlayStatusRestartRound()
    {
        iStarPool.FreeAll();
    }

    public void OnPlayStatusReward()
    {
        iStarPool.FreeAll();

        bSpawnNewStar = false;
        bFinishRewardStars = false;
        SpawnRewardStars();
    }

    public void OnPlayStatusDestoryComponent()
    {
        iStarPool.DestroyPool();
    }
}
