﻿using UnityEngine;
using System.Collections;

public class Cells : MonoBehaviour {
    public enum CellDir
    {
        CELL_DIR_SOUTH,
        CELL_DIR_EAST,
        CELL_DIR_NORTH,
        CELL_DIR_WEST
    }

    SpawnObjectPool iCellPool = new SpawnObjectPool();

    void Awake()
    {
        gameObject.name = GameData.OBJECT_NAME_CELLS;
    }

	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void LateUpdate () {
        if (GameData.nPlayStatus >= PlayStatus.START_PLANET)
        {
            // let it move even if paused for visual scene
            UpdatePos();
        }

        if (GameData.nPlayStatus == PlayStatus.PLAY)
        {
            CheckExistCells();
        }
	}

    void CheckExistCells()
    {
        for (int i = 0; i < GameData.SpawnObjectPool.CELL_POOL_SIZE; i++)
        {
            CellFrame obCellFrame = (CellFrame)iCellPool.GetObject(i);
            if (obCellFrame && !obCellFrame.bSpawn)
            {
                SpawnCell(obCellFrame, (CellDir)i);
            }
        }
    }

    void SpawnCell(CellFrame obCellFrame, CellDir nDir)
    {
        CellFrame.Location nLocation = CellFrame.Location.SOUTH;
        string strName = "Cell(South)"; 
        switch (nDir)
        {
            case CellDir.CELL_DIR_SOUTH: nLocation = CellFrame.Location.SOUTH; strName = "CellFrame(South)";  break;
            case CellDir.CELL_DIR_EAST: nLocation = CellFrame.Location.EAST; strName = "CellFrame(East)"; break;
            case CellDir.CELL_DIR_NORTH: nLocation = CellFrame.Location.NORTH; strName = "CellFrame(North)"; break;
            case CellDir.CELL_DIR_WEST: nLocation = CellFrame.Location.WEST; strName = "CellFrame(West)"; break;
        }
        obCellFrame.nLocation = nLocation;
        obCellFrame.name = strName;
        obCellFrame.transform.parent = transform;

        obCellFrame.Spawn(new Vector3(0, 0, transform.position.z));
    }

    void ShowEnding()
    {
        for (int i = 0; i < GameData.SpawnObjectPool.CELL_POOL_SIZE; i++)
        {
            CellFrame obCellFrame = (CellFrame)iCellPool.GetObject(i);
            if (obCellFrame && obCellFrame.bSpawn)
            {
                obCellFrame.ShowEnding();
            }
        }
    }

    void UpdatePos()
    {
        float nDistance = GameData.CAMERA_CELL_DISTANCE + GameData.RoundData.nCellMoveDepth * Mathf.Cos(Time.time);
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, nDistance);
    }

    public void OnPlayStatusInitComponent()
    {
        iCellPool.CreatePool(GameData.SpawnObjectPool.CELL_POOL_SIZE, GamePlay.GetInstance().pfCellFrame, transform);
    }

    public void OnPlayStatusStartPlanet()
    {
    }

    public void OnPlayStatusStartRound()
    {
        iCellPool.FreeAll();
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, GameData.CAMERA_CELL_DISTANCE);
    }

    public void OnPlayStatusEnding()
    {
        ShowEnding();
    }

    public void OnPlayStatusEndRound()
    {
        iCellPool.FreeAll();
    }
}
