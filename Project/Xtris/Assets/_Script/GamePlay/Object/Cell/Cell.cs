﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Advertisements;

public class Cell : MonoBehaviour {
    public bool bAvailable = false;

    public CellFrame obCellFrame = null;
    public Effect obCellBrokenEffect = null;
    public Effect obCellMatchEffect = null;

    Brick[,] dCellMatrix;
    public Block obDropBlock = null;

    int nMatchedX = 0;
    int nMatchedY = 0;
    ArrayList listConflicPosX = new ArrayList();

    bool bHitToCell = false;

    // Use this for initialization
    public void Spawn()
    {
        listConflicPosX.Clear();
        obDropBlock = null;
        bHitToCell = false;
        bAvailable = true;

        Vector3 dParentScale = transform.parent.localScale;
        BoxCollider iBoxCollider = GetComponent<BoxCollider>();
        iBoxCollider.size = dParentScale;

        transform.localScale = new Vector3(1 / dParentScale.x, 1 / dParentScale.y, 1 / dParentScale.z);

        CreateRandomCell();
    }

    public void CreateRandomCell()
    {
        BlockGenerator iBlockGenerator = BlockGenerator.GetInstance();
        dCellMatrix = iBlockGenerator.CreateRandomCell();
        //iBlockGenerator.DebugPrintCell(dCellMatrix);

        ArrangeBricks();
    }

    void ArrangeBricks()
    {
        for (int nY = 0; nY < GameData.MAX_CELL_MATRIX_SIZE; nY++)
        {
            for (int nX = 0; nX < GameData.MAX_CELL_MATRIX_SIZE; nX++)
            {
                Brick obBrick = dCellMatrix[nY, nX];
                if (obBrick == null)
                {
                    continue;
                }

                obBrick.transform.parent = transform;
                obBrick.transform.localPosition = GetLocalCellPos(nX, nY);
                obBrick.transform.localRotation = transform.localRotation;
            }
        }
    }

    private Vector3 GetLocalCellPos(int nX, int nY)
    {
        Vector3 pos = new Vector3(nX * GameData.BRICK_SIZE, nY * GameData.BRICK_SIZE, 0);
        pos.x -= (GameData.BRICK_SIZE * ((float)GameData.MAX_CELL_MATRIX_SIZE / 2 - 0.5f));
        pos.y -= (GameData.BRICK_SIZE * ((float)GameData.MAX_CELL_MATRIX_SIZE / 2 - 0.5f));
        return pos;
    }

    private Vector3 GetLocalBlockPos(int nX, int nY, BlockAngle nAngle)
    {
        int nBlockSizeX = GameData.MAX_BLOCK_MATRIX_SIZE_X;
        int nBlockSizeY = GameData.MAX_BLOCK_MATRIX_SIZE_Y;
        if (nAngle == BlockAngle.BLOCK_ANGLE_90 || nAngle == BlockAngle.BLOCK_ANGLE_270)
        {
            nBlockSizeX = GameData.MAX_BLOCK_MATRIX_SIZE_Y;
            nBlockSizeY = GameData.MAX_BLOCK_MATRIX_SIZE_X;
        }

        Vector3 pos = new Vector3(nX * GameData.BRICK_SIZE, nY * GameData.BRICK_SIZE, 0);
        pos.x -= (GameData.BRICK_SIZE * ((float)GameData.MAX_CELL_MATRIX_SIZE - nBlockSizeX) / 2);
        pos.y -= (GameData.BRICK_SIZE * ((float)GameData.MAX_CELL_MATRIX_SIZE - nBlockSizeY) / 2);
        return pos;
    }

    public void DropBlock(Block obBlock)
    {
        bAvailable = false;

        obDropBlock = obBlock;
        obDropBlock.transform.parent = transform;

        int nAngle = (((int)obDropBlock.nAngle * 90 - (int)obCellFrame.nAngle) + 360) % 360;
        //Debug.Log("Angle, Block : " + obDropBlock.nAngle + ", CellFrame : " + obCellFrame.nAngle);
        obDropBlock.nAngle = (BlockAngle)((int)nAngle / 90);
        //Debug.Log("       --> new Angle : " + nAngle + ", " + obDropBlock.nAngle);

        Vector3 pos;
        CheckMatchedDropPos(out pos);

        if (bHitToCell) {
            EnableCollider(true);
        }

        obDropBlock.Drop(pos, gameObject);
        GamePlay.GetInstance().SetPlayEvent(PlayEvent.BLOCK_DROP);
    }

    public void OnBlockDropped()
    {
//        Debug.Log("Dropped!");
        obCellFrame.Stop();

        bool bDroppedFail = (GameData.nMatchResult == MatchResult.FAIL);
        if (GameData.RoundData.nPlayMode == PlayMode.BREAK_THEM_ALL)
        {
            if (!bDroppedFail)
            {
                obCellBrokenEffect.PlaceInPlayground();
                obCellBrokenEffect.Play(GameData.CELL_BROKEN_EFFECT_PLAY_TIME);

                BreakBricks(true);
                obCellFrame.Free(GameData.CELL_BREAK_DELAY);

                GamePlay.GetInstance().SetPlayEvent(PlayEvent.CELL_BREAK);
                GamePlay.GetInstance().SetPlayEvent(PlayEvent.BLOCK_DROP_SUCCESS);
            }
            else
            {
                float nTime = FlipBricks();
                obCellFrame.Free(nTime);

                GamePlay.GetInstance().SetPlayEvent(PlayEvent.BLOCK_FIT_TO_CELL);
                GamePlay.GetInstance().SetPlayEvent(PlayEvent.BLOCK_DROP_FAIL);
            }
        }
        else {      // normal mode
            if (!bDroppedFail)
            {
                obCellMatchEffect.PlaceInPlayground();
                obCellMatchEffect.Play(GameData.CELL_BROKEN_EFFECT_PLAY_TIME);

                float nTime = FlipBricks();
                obCellFrame.Free(nTime);

                GamePlay.GetInstance().SetPlayEvent(PlayEvent.BLOCK_FIT_TO_CELL);
                GamePlay.GetInstance().SetPlayEvent(PlayEvent.BLOCK_DROP_SUCCESS);
            }
            else
            {
                BreakBricks();
                obCellFrame.Free(GameData.CELL_BREAK_DELAY);

                GamePlay.GetInstance().SetPlayEvent(PlayEvent.CELL_BREAK);
                GamePlay.GetInstance().SetPlayEvent(PlayEvent.BLOCK_DROP_FAIL);
            }
        }
    }

    void EnableCollider(bool bEnable)
    {
        // Cell Bricks Collider
        for (int nY = 0; nY < GameData.MAX_CELL_MATRIX_SIZE; nY++)
        {
            for (int nX = 0; nX < GameData.MAX_CELL_MATRIX_SIZE; nX++)
            {
                Brick obBrick = dCellMatrix[nY, nX];
                if (obBrick == null) {
                    continue;
                }

                obBrick.EnableCollider(bEnable);
            }
        }

        // Brick Collider in Block
        if (obDropBlock)
        {
            obDropBlock.EnableBrickCollider(bEnable);
        }
    }

    void BreakBricks(bool bShowEffect = false)
    {
        if (obDropBlock)
        {
            Brick[,] dBlockMatrix = obDropBlock.GetBlockMatrix();
            BlockAngle nAngle = obDropBlock.nAngle;
            MergeMatrix(ref dCellMatrix, dBlockMatrix, nMatchedX, nMatchedY, nAngle, true);
        }

        for (int nY = 0; nY < GameData.MAX_CELL_MATRIX_SIZE; nY++)
        {
            for (int nX = 0; nX < GameData.MAX_CELL_MATRIX_SIZE; nX++)
            {
                Brick obBrick = dCellMatrix[nY, nX];
                if (obBrick == null)
                {
                    continue;
                }

                obBrick.Break(bShowEffect);
            }
        }
    }

    float FlipBricks()
    {
        Brick[,] dBlockMatrix = obDropBlock.GetBlockMatrix();
        BlockAngle nAngle = obDropBlock.nAngle;
        MergeMatrix(ref dCellMatrix, dBlockMatrix, nMatchedX, nMatchedY, nAngle, true);

        int nMatchCount = 0;
        for (int nY = 0; nY < GameData.MAX_CELL_MATRIX_SIZE; nY++)
        {
            bool bValid;
            bool bMatched = CheckMatchedLine(dCellMatrix, nY, out bValid);
            if (bMatched)
            {
                for (int nX = 0; nX < GameData.MAX_CELL_MATRIX_SIZE; nX++)
                {
                    Brick obBrick = dCellMatrix[nY, nX];
                    obBrick.Flip(bValid, (nMatchCount + nX) * GameData.BRICK_DISCOVER_DELAY);
                }
                nMatchCount++;
            }
        }

        float nTime = (GameData.MAX_CELL_MATRIX_SIZE + nMatchCount) * GameData.BRICK_DISCOVER_DELAY + GameData.CELL_FLIP_DELAY;
        return nTime;
    }

    public void FreeBricks()
    {
        if (obDropBlock != null)
        {
            obDropBlock.Free();
        }

        if (dCellMatrix != null)
        {
            for (int nY = 0; nY < GameData.MAX_CELL_MATRIX_SIZE; nY++)
            {
                for (int nX = 0; nX < GameData.MAX_CELL_MATRIX_SIZE; nX++)
                {
                    Brick obBrick = dCellMatrix[nY, nX];
                    if (obBrick == null)
                    {
                        continue;
                    }

                    obBrick.Free();
                }
            }
        }
    }


    void CheckMatchedDropPos(out Vector3 pos)
    {
        pos = Vector3.zero;

//        BlockType nType = obDropBlock.nType;
        BlockAngle nAngle = obDropBlock.nAngle;
        Brick[,] dBlockMatrix = obDropBlock.GetBlockMatrix();

        int nBlockSizeX = GameData.MAX_BLOCK_MATRIX_SIZE_X;
        int nBlockSizeY = GameData.MAX_BLOCK_MATRIX_SIZE_Y;
        if (nAngle == BlockAngle.BLOCK_ANGLE_90 || nAngle == BlockAngle.BLOCK_ANGLE_270)
        {
            nBlockSizeX = GameData.MAX_BLOCK_MATRIX_SIZE_Y;
            nBlockSizeY = GameData.MAX_BLOCK_MATRIX_SIZE_X;
        }

//      Debug.Log("Check Matched Drop : " + nType + ", " + nAngle);
//      Debug.Log("BlockSizeX : " + nBlockSizeX + ", BlockSizeY : " + nBlockSizeY);

        // for test
        BlockGenerator iBlockGenerator = BlockGenerator.GetInstance();
//        iBlockGenerator.DebugPrintBlock(dBlockMatrix, nAngle);

        bool bPrintLog = false;
        int nMatchedLines = 0;
        int nLines = 0;
        /*add -1, 1 index for checking partial block*/
        for (int nY = 0; nY < GameData.MAX_CELL_MATRIX_SIZE - nBlockSizeY + 1 + 1; nY++)
        {
            for (int nX = -1; nX < GameData.MAX_CELL_MATRIX_SIZE - nBlockSizeX + 1 + 1; nX++)
            {
                if (bPrintLog)
                {
                    Debug.Log("=======================");
                    Debug.Log("nX : " + nX + ", nY : " + nY);
                }

                Brick[,] dMergeMatrix = iBlockGenerator.CopyCell(dCellMatrix);
                if (!MergeMatrix(ref dMergeMatrix, dBlockMatrix, nX, nY, nAngle))
                {
                    if (bPrintLog)
                    {
                        iBlockGenerator.DebugPrintCell(dMergeMatrix);
                    }
                    continue;
                }

                if (bPrintLog)
                {
                    iBlockGenerator.DebugPrintCell(dMergeMatrix);
                }

                nLines = CheckMatchedLines(dMergeMatrix);
                if (bPrintLog)
                {
                    Debug.Log("Lines : " + nLines);
                }

                if (nLines > nMatchedLines)
                {
                    nMatchedX = nX;
                    nMatchedY = nY;
                    nMatchedLines = nLines;
//                    Debug.Log("New MatchedLine : " + nMatchedLines);
                }
            }
        }

        GameData.nMatchResult = MatchResult.FAIL;
        if (GameData.RoundData.nPlayMode == PlayMode.BREAK_THEM_ALL)
        {
            if (nMatchedLines == 0) {
                bHitToCell = true;

                int nBrickCount = GetBrickCount() + obDropBlock.GetBrickCount();
                if (nBrickCount >= GameData.MAX_CELL_MATRIX_SIZE * GameData.MAX_CELL_MATRIX_SIZE) GameData.nMatchResult = MatchResult.PERFECT;
                else if (nBrickCount >= GameData.MAX_CELL_MATRIX_SIZE * (GameData.MAX_CELL_MATRIX_SIZE - 1)) GameData.nMatchResult = MatchResult.GREAT;
                else GameData.nMatchResult = MatchResult.GOOD;

                nMatchedX = GetConflictPosX();
                nMatchedY = GameData.CONFLICT_POS_Y;
            }
        }
        else {      // normal mode
            switch (nMatchedLines)
            {
                case 2: GameData.nMatchResult = MatchResult.GOOD; break;
                case 3: GameData.nMatchResult = MatchResult.GREAT; break;
                case 4: GameData.nMatchResult = MatchResult.PERFECT; break;
            }

            if (GameData.nMatchResult == MatchResult.FAIL)
            {
                bHitToCell = true;

                nMatchedX = GetConflictPosX();
                nMatchedY = GameData.CONFLICT_POS_Y;
            }
        }

//        Debug.Log("Match Result : " + GameData.nMatchResult);

        pos = GetLocalBlockPos(nMatchedX, nMatchedY, nAngle);
    }

    private bool MergeMatrix(ref Brick[,] dMergeMatrix, Brick[,] dBlockMatrix, int nX, int nY, BlockAngle nAngle, bool bFreeHollowBlock = false)
    {
        int nBlockSizeX = GameData.MAX_BLOCK_MATRIX_SIZE_X;
        int nBlockSizeY = GameData.MAX_BLOCK_MATRIX_SIZE_Y;
        if (nAngle == BlockAngle.BLOCK_ANGLE_90 || nAngle == BlockAngle.BLOCK_ANGLE_270)
        {
            nBlockSizeX = GameData.MAX_BLOCK_MATRIX_SIZE_Y;
            nBlockSizeY = GameData.MAX_BLOCK_MATRIX_SIZE_X;
        }


//        Debug.Log("MergeMatrix : " + nX + ", " + nY);

        bool isFloating = true;
        for (int nBlockY = nBlockSizeY - 1; nBlockY >= 0; nBlockY--)    // start from top due to checking floating
        {
            for (int nBlockX = 0; nBlockX < nBlockSizeX; nBlockX++)
            {
                BlockGenerator iBlockGenerator = BlockGenerator.GetInstance();
                Brick obBrick = iBlockGenerator.GetBrick(dBlockMatrix, nBlockX, nBlockY, nAngle);

                int nCellX = nX + nBlockX;
                int nCellY = nY + nBlockY;

                if (nCellX <0 || nCellX >= GameData.MAX_CELL_MATRIX_SIZE)
                {
                    continue;
                }
                if (nCellY >= GameData.MAX_CELL_MATRIX_SIZE)
                {
                    continue;
                }

                if (obBrick != null)
                {
                    if (dMergeMatrix[nCellY, nCellX] != null)
                    {
                        if (dMergeMatrix[nCellY, nCellX].nType != BrickType.BRICK_TYPE_DUMMY_HOLLOW)
                        {
                            // conflict
                            if(nY == 0) {
                                listConflicPosX.Add(nX);
                            }
                            return false;
                        }

                        // if it's hollow block, replace it to real one.
                        if (bFreeHollowBlock)
                        {
                            dMergeMatrix[nCellY, nCellX].Free();
                        }
                    }

                    if (nCellY == 0 || dMergeMatrix[nCellY - 1, nCellX] != null)
                    {
                        isFloating = false;
                    }

                    dMergeMatrix[nCellY, nCellX] = obBrick;
                }
            }
        }

        if (isFloating)
        {
            return false;
        }

        return true;
    }

    private int CheckMatchedLines(Brick[,] dMergeMatrix)
    {
        int nMatchedLines = 0;
        int nValidLines = 0;
        for (int nY = 0; nY < GameData.MAX_CELL_MATRIX_SIZE; nY++)
        {
            bool bValid;
            bool bMatched = CheckMatchedLine(dMergeMatrix, nY, out bValid);
            if (bMatched) {
                if (bValid) {
                    nValidLines++;
                }

                nMatchedLines++;
            }
        }

        if (nValidLines >= GameData.VALID_MATCH_LINES)
        {
            return nMatchedLines;
        }

        return 0;
    }

    bool CheckMatchedLine(Brick[,] dMatrix, int nY, out bool bValid)
    {
        bValid = false;
        for (int nX = 0; nX < GameData.MAX_CELL_MATRIX_SIZE; nX++)
        {
            Brick obBrick = dMatrix[nY, nX];
            if (obBrick == null)
            {
                return false;
            }
            else
            {
                if (obBrick.nType != BrickType.BRICK_TYPE_DUMMY && obBrick.nType != BrickType.BRICK_TYPE_DUMMY_HOLLOW)
                {
                    bValid = true;
                    continue;
                }
            }
        }

        return true;
    }

    int GetConflictPosX()
    {
        int nIndex = Random.Range(0, listConflicPosX.Count);
        return (int)listConflicPosX[nIndex];
    }

    int GetBrickCount()
    {
        // Cell Bricks Collider
        int nCount = 0;
        for (int nY = 0; nY < GameData.MAX_CELL_MATRIX_SIZE; nY++)
        {
            for (int nX = 0; nX < GameData.MAX_CELL_MATRIX_SIZE; nX++)
            {
                Brick obBrick = dCellMatrix[nY, nX];
                if (obBrick == null)
                {
                    continue;
                }

                nCount++;
            }
        }

        return nCount;
    }

    public void Break()
    {
        obCellBrokenEffect.PlaceInPlayground();
        obCellBrokenEffect.Play(GameData.ROUND_ENDING_BROKEN_TIME);
        BreakBricks(true);
        obCellFrame.Free(GameData.ROUND_ENDING_BROKEN_TIME);
        if (obDropBlock)
        {
            obDropBlock.Free();
        }

        GamePlay.GetInstance().SetPlayEvent(PlayEvent.CELL_BREAK);
    }
}
