﻿using UnityEngine;
using System.Collections;

public class CellFrame : SpawnObject {
    public enum Location
    {
        SOUTH = 0,
        EAST,
        NORTH,
        WEST,
    };

    public Location nLocation;
    Vector2 posLocation;
    public float nAngle;

    //    public static int MAX_CELL_MATRIX_SIZE_X = 4;
    //public static int MAX_CELL_MATRIX_SIZE_Y = MAX_CELL_MATRIX_SIZE_X + BlockGenerator.;

    bool bMove = true;
    Vector3 posTo;
    float nMoveSpeed = 0f;
    bool bInitPos = false;

    bool bShowRoundClearEnding = false;

    bool bDistanceChange = false;
    float nDistanceChangeDelay = 0f;

    Cell obCell;

    // Use this for initialization
    void Awake()
    {
        obCell = transform.Find("Cell").GetComponent<Cell>();
    }

    public override void Spawn(Vector3 pos)
    {
        base.Spawn(pos);

        bShowRoundClearEnding = false;

        InitPos();
        obCell.Spawn();

        bDistanceChange = true;
        nDistanceChangeDelay = GameData.MAX_CELL_INIT_MOVE_DELAY;
    }

    public override void Free()
    {
        if(obCell) {
            obCell.FreeBricks();
        }

        bDistanceChange = false;

        base.Free();
    }
    void InitPos()
    {
        switch (nLocation)
        {
            case Location.SOUTH:
                posLocation = new Vector2(0, -1);
                nAngle = 0f;
                break;
            case Location.EAST:
                posLocation = new Vector2(1, 0);
                nAngle = 90f;
                break;
            case Location.NORTH:
                posLocation = new Vector2(0, 1);
                nAngle = 180f;
                break;
            case Location.WEST:
                posLocation = new Vector2(-1, 0);
                nAngle = 270f;
                break;
        }

        /*
                if (nLocation != LOCATION.SOUTH)
                {
                    gameObject.SetActive(false);
                    return;
                }
         */

        float nDistance = Random.Range(2 * GameData.MAX_CELL_MOVE_DISTANCE, 10 * GameData.MAX_CELL_MOVE_DISTANCE);
        transform.localPosition = new Vector3(posLocation.x * nDistance, posLocation.y * nDistance, transform.localPosition.z);

        float nCellSize = GameData.CELL_SIZE - 0.1f;
        transform.localScale = new Vector3(nCellSize, nCellSize, GameData.BRICK_SIZE - 0.1f);

        Quaternion rot = Quaternion.identity;
        rot.eulerAngles = transform.forward * nAngle;
        transform.localRotation = rot;

        bInitPos = true;

        float nDelay = nDistance * 0.01f;
        InvokeManager.GetInstance().SetInvoke(this, "SetPlayEventCellArrive", nDelay);
    }

    void SetPlayEventCellArrive()
    {
        GamePlay.GetInstance().SetPlayEvent(PlayEvent.CELL_ARRIVE);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!bSpawn)
        {
            return;
        }

        if (GameData.nPlayStatus < PlayStatus.START_ROUND || GameData.nPlayStatus > PlayStatus.END_ROUND)
        {
            return;
        }

        CheckDistanceChange();
        UpdatePos();
    }

    void CheckDistanceChange()
    {
        if (!bDistanceChange)
        {
            return;
        }

        nDistanceChangeDelay -= Time.deltaTime;
        if (nDistanceChangeDelay < 0f)
        {
            bDistanceChange = false;
            SetNextDistance();
        }
    }

    void UpdatePos()
    {
        float nStep = nMoveSpeed * Time.deltaTime;
        if (bShowRoundClearEnding)
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, posTo, nStep);
            return;
        }

        if (!bMove)
        {
            return;
        }

        transform.localPosition = Vector3.MoveTowards(transform.localPosition, posTo, nStep);
        if ((posTo - transform.localPosition).magnitude < 0.01f)
        {
            bMove = false;

            bDistanceChange = true;
            nDistanceChangeDelay = Random.Range(GameData.MAX_CELL_MOVE_MIN_DELAY, GameData.MAX_CELL_MOVE_MAX_DELAY); 
        }
    }

    void SetNextDistance()
    {
        if (GameData.nPlayStatus != PlayStatus.PLAY)
        {
            return;
        }

        float nDistance = 0f;
        if (bInitPos)
        {
            bInitPos = false;

            nDistance = Random.Range(GameData.BLOCK_ZONE_SIZE, GameData.MAX_CELL_INIT_MOVE_DISTANCE);
            nMoveSpeed = GameData.CELL_INIT_MOVE_SPEED;
        }
        else
        {
            float nMaxDistance = GameData.BLOCK_ZONE_SIZE + GameData.RoundData.nCellMaxMoveDistance * GameData.BRICK_SIZE;
            nDistance = Random.Range(GameData.BLOCK_ZONE_SIZE, nMaxDistance);
            nMoveSpeed = Random.Range(0.1f, GameData.RoundData.nCellMoveSpeed);
        }

        posTo = new Vector3(posLocation.x * nDistance, posLocation.y * nDistance, transform.localPosition.z);
        bMove = true;
    }

    public void ShowEnding()
    {
        if (GameData.nRoundResult == RoundResult.ROUND_CLEAR)
        {
            bShowRoundClearEnding = true;
            float nDistance = GameData.BLOCK_ZONE_SIZE;
            posTo = new Vector3(posLocation.x * nDistance, posLocation.y * nDistance, transform.localPosition.z);
            nMoveSpeed = GameData.CELL_INIT_MOVE_SPEED;

            if (obCell.bAvailable)
            {
                InvokeManager.GetInstance().SetInvoke(this, "GetAway", GameData.ROUND_ENDING_PLAYGROUND_ROTATE_TIME);
            }
        }
        else
        {
            if (obCell.bAvailable)
            {
                obCell.Break();
            }
        }
    }

    public void GetAway()
    {
        GamePlay.GetInstance().SetPlayEvent(PlayEvent.CELL_AWAY);

        float nDistance = 10000f * GameData.MAX_CELL_MOVE_DISTANCE;
        posTo = new Vector3(posLocation.x * nDistance, posLocation.y * nDistance, transform.localPosition.z);
        nMoveSpeed = GameData.CELL_ENDING_MOVE_SPEED;
    }

    public void Stop()
    {
        bMove = false;
        bDistanceChange = false;
    }
}
