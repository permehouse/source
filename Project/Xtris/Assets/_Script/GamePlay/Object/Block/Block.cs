﻿using UnityEngine;
using System.Collections;

public class Block : SpawnObject
{
    public bool bAvailable = false;

    public BlockType nType = BlockType.BLOCK_TYPE_O;
    public BlockAngle nAngle = BlockAngle.BLOCK_ANGLE_0;
    public Brick[,] dBlockMatrix = null;

    float nDistanceTo = 0;
    float nMoveLerpRate = GameData.BLOCK_MOVE_LERP_RATE;
    float nRotateSlerpRate = GameData.BLOCK_ROTATE_SLERP_RATE;
    Quaternion rotTo;
    Vector3 posTo;

    public Effect obBlockDisappearEffect = null;

    bool bMoveToStartDistance = false;
    bool bMove = false;
    bool bRotate = false;
    bool bDrop = false;
    bool bDropped = false;   
    float nDropSpeed = 50.0f;

    Cell obDropCell = null;

    public override void Spawn(Vector3 pos)
    {
        bMove = false;
        bRotate = false;
        bDrop = false;
        bDropped = false;   

        BoxCollider obBoxCollider = GetComponent<BoxCollider>();
        obBoxCollider.size = new Vector3(GameData.BRICK_SIZE * GameData.MAX_BLOCK_MATRIX_SIZE_X, GameData.BRICK_SIZE * GameData.MAX_BLOCK_MATRIX_SIZE_Y, 1f);

        SpawnRandomBlock();

        base.Spawn(pos);
	}

    public override void Free()
    {
        FreeBricks();

        base.Free();
    }

    void FixedUpdate() {
        if (!bSpawn)
        {
            return;
        }

        if (GameData.nPlayStatus < PlayStatus.START_ROUND || GameData.nPlayStatus > PlayStatus.END_ROUND)
        {
            return;
        }

        UpdatePos();
        UpdateRotation();

        CheckPassed();
	}

    public void SpawnRandomBlock() {
        BlockGenerator iBlockGenerator = BlockGenerator.GetInstance();
        dBlockMatrix = iBlockGenerator.SpawnRandomBlock(out nType, out nAngle);
        nDistanceTo = GameData.RoundData.nBlockStartDistance;
        bMoveToStartDistance = true;
        bAvailable = false;

//        Debug.Log("Block Create Rotate : " + nAngle);

        ArrangeBricks();
    }

    public Brick[,] GetBlockMatrix()
    {
        return dBlockMatrix;
    }

    void ArrangeBricks()
    {
        if (dBlockMatrix == null)
        {
            return;
        }

        Quaternion rot = Quaternion.identity;
        rot.eulerAngles = Vector3.zero;

        for (int nY = 0; nY < GameData.MAX_BLOCK_MATRIX_SIZE_Y; nY++)
        {
            for (int nX = 0; nX < GameData.MAX_BLOCK_MATRIX_SIZE_X; nX++)
            {
                Brick obBrick = dBlockMatrix[nY, nX];
                if (obBrick == null)
                {
                    continue;
                }

                obBrick.transform.parent = transform;
                obBrick.transform.localPosition = GetLocalBlockPos(nX, nY);
                obBrick.transform.localRotation = rot;
            }
        }

        rotTo.eulerAngles = transform.forward * (int)nAngle * 90;
        //rotTo.eulerAngles = transform.parent.localRotation.eulerAngles + transform.forward * (int)nAngle * 90;
        transform.localRotation = rotTo;
    }

    void BreakBricks(bool bShowEffect = false)
    {
        if (dBlockMatrix == null)
        {
            return;
        }

        for (int nY = 0; nY < GameData.MAX_BLOCK_MATRIX_SIZE_Y; nY++)
        {
            for (int nX = 0; nX < GameData.MAX_BLOCK_MATRIX_SIZE_X; nX++)
            {
                Brick obBrick = dBlockMatrix[nY, nX];
                if (obBrick == null)
                {
                    continue;
                }

                obBrick.Break(bShowEffect);
            }
        }
    }

    void FreeBricks()
    {
        if (dBlockMatrix == null)
        {
            return;
        }

        for (int nY = 0; nY < GameData.MAX_BLOCK_MATRIX_SIZE_Y; nY++)
        {
            for (int nX = 0; nX < GameData.MAX_BLOCK_MATRIX_SIZE_X; nX++)
            {
                Brick obBrick = dBlockMatrix[nY, nX];
                if (obBrick == null)
                {
                    continue;
                }

                obBrick.Free();
            }
        }
    }

    public void EnableBrickCollider(bool bEnable)
    {
        if (dBlockMatrix == null)
        {
            return;
        }

        for (int nY = 0; nY < GameData.MAX_BLOCK_MATRIX_SIZE_Y; nY++)
        {
            for (int nX = 0; nX < GameData.MAX_BLOCK_MATRIX_SIZE_X; nX++)
            {
                Brick obBrick = dBlockMatrix[nY, nX];
                if (obBrick == null)
                {
                    continue;
                }

                obBrick.EnableCollider(bEnable);
            }
        }
    }

    private Vector3 GetLocalBlockPos(int nX, int nY)
    {
        Vector3 pos = new Vector3(nX * GameData.BRICK_SIZE, nY * GameData.BRICK_SIZE, 0);
        pos.x -= (GameData.BRICK_SIZE * 1.0f);
        pos.y -= (GameData.BRICK_SIZE * 0.5f);
        return pos;
    }


    void UpdatePos()
    {
        if (bDropped)
        {
            return;
        }

        if (bDrop)
        {
            float nStep = nDropSpeed * Time.deltaTime;

            if (Mathf.Abs(posTo.z - transform.localPosition.z) > 2f)
            {
                Vector3 posToCenter = new Vector3(posTo.x, GameData.BLOCK_ZONE_SIZE, posTo.z);
                transform.localPosition = Vector3.MoveTowards(transform.localPosition, posToCenter, nStep);
            }
            else
            {
                transform.localPosition = Vector3.MoveTowards(transform.localPosition, posTo, nStep);
            }

            if ((posTo - transform.localPosition).magnitude < 0.01f)
            {
                transform.localPosition = posTo;
                bDropped = true;

                if (obDropCell)
                {
                    obDropCell.OnBlockDropped();
                }
            }
        }
        else if (bMoveToStartDistance)
        {
            posTo = new Vector3(transform.localPosition.x, transform.localPosition.y, nDistanceTo);
            if ((transform.localPosition - posTo).magnitude < 0.01f)
            {
                transform.localPosition = posTo;
                bMoveToStartDistance = false;
                bMove = true;
                bAvailable = true;
            }
            else
            {
                transform.localPosition = Vector3.Slerp(transform.localPosition, posTo, nMoveLerpRate);
            }
        }
        else if (bMove) {
            if (GameData.nPlayStatus != PlayStatus.PLAY)
            {
                return;
            }

            float nPlaygroundMoveSpeed = GamePlay.GetInstance().obPlayground.GetSpeed();
            nDistanceTo -= nPlaygroundMoveSpeed * Time.deltaTime;

            if (nDistanceTo < 0f) {
                // to avoid the case that block can skip over the camera
                nDistanceTo = 0f;
            }

            posTo = new Vector3(transform.localPosition.x, transform.localPosition.y, nDistanceTo);
            if ((transform.localPosition - posTo).magnitude < 0.01f)
            {
                transform.localPosition = posTo;
            }
            else
            {
                transform.localPosition = Vector3.Lerp(transform.localPosition, posTo, nMoveLerpRate);
            }
        }
    }

    void UpdateRotation()
    {
        if (bRotate)
        {
//            Debug.Log("Rotate : " + (transform.localRotation.eulerAngles - rotTo.eulerAngles).magnitude);
            if ((transform.localRotation.eulerAngles - rotTo.eulerAngles).magnitude < 0.01f)
            {
                transform.localRotation = rotTo;
                bRotate = false;
            }
            else
            {
                transform.localRotation = Quaternion.Slerp(transform.localRotation, rotTo, nRotateSlerpRate);
            }
        }
    }

    void CheckPassed()
    {
        if(bAvailable && bMove)
        {
            if (transform.localPosition.z <= 0f)
            {
                Free();
                GamePlay.GetInstance().SetPlayEvent(PlayEvent.BLOCK_PASS);
            }
        }
    }
            
    public void Rotate()
    {
        if (bAvailable && bMove)
        {
            nAngle = (BlockAngle)(((int)nAngle + 1) % (int)BlockAngle.BLOCK_ANGLE_COUNT);
            rotTo.eulerAngles = transform.forward * (int)nAngle * 90;
            bRotate = true;
        }
    }

    public void Drop(Vector3 pos, GameObject obCell)
    {
        if (bAvailable && bMove)
        {
            bAvailable = false;
            bMove = false;

            bDrop = true;
            posTo = pos;
            rotTo.eulerAngles = transform.forward * (int)nAngle * 90;
            obDropCell = obCell.GetComponent<Cell>();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Free();
        GamePlay.GetInstance().SetPlayEvent(PlayEvent.BLOCK_HIT_TO_CAMERA);
    }

    public int GetBrickCount()
    {
        if (dBlockMatrix == null)
        {
            return 0;
        }

        int nCount = 0;
        for (int nY = 0; nY < GameData.MAX_BLOCK_MATRIX_SIZE_Y; nY++)
        {
            for (int nX = 0; nX < GameData.MAX_BLOCK_MATRIX_SIZE_X; nX++)
            {
                Brick obBrick = dBlockMatrix[nY, nX];
                if (obBrick == null)
                {
                    continue;
                }

                nCount++;
            }
        }

        return nCount;
    }

    public void Break()
    {
        if (bDrop || bDropped)
        {
            return;
        }

        if (GameData.nRoundResult == RoundResult.ROUND_CLEAR)
        {
            obBlockDisappearEffect.Play();
            Invoke("FreeBricks", GameData.ROUND_ENDING_DISAPPEAR_TIME / 2f);
            Free(GameData.ROUND_ENDING_DISAPPEAR_TIME);
        }
        else
        {
            BreakBricks(true);
            Free(GameData.ROUND_ENDING_BROKEN_TIME);
        }
    }
}
