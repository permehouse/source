﻿using UnityEngine;
using System.Collections;

public class Brick : SpawnObject {
    public BrickType nType;
    bool bBounce = false;
    float nBounceDeflateSize = 0.5f;
    float bBounceDelay = 0f;
    float nBounceSpeed = 2.5f;

    bool bBlink = false;
    float nBlinkMaxAlpha = GameData.BRICK_BLINK_MAX_ALPHA;
    float nBounceTime = 0f;
    float nBlinkTime = 0f;
    float nBlinkSpeed = 1.5f;

    bool bBreak = false;

    public Effect obBrickBrokenEffect = null;

    public override void Spawn(Vector3 pos)
    {
        bBounce = false;
        bBreak = false;

        float nSize = GameData.BRICK_SIZE;
        transform.localScale = new Vector3(nSize, nSize, nSize);

        base.Spawn(pos);
    }

    public override void Free()
    {
        Rigidbody obRigidBody = GetComponent<Rigidbody>();
        obRigidBody.velocity = Vector3.zero;
        obRigidBody.angularVelocity = Vector3.zero;
        obRigidBody.Sleep();

        EnableCollider(false);

        base.Free();
    }

    public void SetBlink(bool bSet)
    {
        bBlink = bSet;
    }

    void SetWorldScale(Vector3 scaleWorld)
    {
        Vector3 scaleParents = new Vector3(1f, 1f, 1f);
        Transform parent = transform.parent;
        while(parent) {
            scaleParents = Vector3.Scale(scaleParents, parent.localScale);
            parent = parent.parent;
        }

        transform.localScale = new Vector3(scaleWorld.x / scaleParents.x, scaleWorld.y / scaleParents.y, scaleWorld.z / scaleParents.z);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (!bSpawn)
        {
            return;
        }

        if (GameData.nPlayStatus < PlayStatus.START_ROUND || GameData.nPlayStatus > PlayStatus.END_ROUND)
        {
            return;
        }

        UpdateParent();
        UpdateBounce();
        UpdateBlink();
	}

    void UpdateParent()
    {
        if (GameData.nPlayStatus == PlayStatus.ENDING && GameData.nRoundResult == RoundResult.ROUND_CLEAR)
        {
            if (bBreak) {
                transform.parent = GamePlay.GetInstance().transform;
            }
        }
    }

    void UpdateBounce()
    {
        if (bBounce)
        {
            bBounceDelay -= Time.deltaTime;
            if (bBounceDelay <= 0f)
            {
                nBounceTime += (nBounceSpeed * Time.deltaTime);
                if (nBounceTime > Mathf.PI) nBounceTime = Mathf.PI;
                float nSize = GameData.BRICK_SIZE * (nBounceDeflateSize + Mathf.Abs(Mathf.Cos(nBounceTime) * (1 - nBounceDeflateSize)));
                transform.localScale = new Vector3(nSize, nSize, nSize);
            }
        }
    }

    void UpdateBlink()
    {
        float nAlpha = 1f;
        if (bBlink)
        {
            nBlinkTime += (nBlinkSpeed * Time.deltaTime);
            nAlpha = nBlinkMaxAlpha * Mathf.Abs(Mathf.Cos(nBlinkTime));
        }

        MeshRenderer renderer = GetComponent<MeshRenderer>();
        Color color = renderer.material.color;
        color.a = nAlpha;
        renderer.material.color = color;
    }

    public void EnableCollider(bool bEnable)
    {
        BoxCollider iCollider = GetComponent<BoxCollider>();
        iCollider.enabled = bEnable;
    }

    public void Break(bool bShowEffect = false)
    {
        if (bBlink)
        {
            Free();
        }
        else
        {
            if (bShowEffect)
            {
                obBrickBrokenEffect.Play();
            }

            transform.parent = GamePlay.GetInstance().obPlayground.transform;
            Free(GameData.BRICK_BREAK_TIME);

            bBreak = true;
        }
    }

    public void Flip(bool bValid, float nDelay)
    {
        bBlink = false;
        bBounce = true;
        nBounceTime = 0f;
        bBounceDelay = nDelay;

        if (bValid)
        {
            nBounceDeflateSize = 0.5f;
        }
        else
        {
            nBounceDeflateSize = 0.5f;
        }
    }
}
