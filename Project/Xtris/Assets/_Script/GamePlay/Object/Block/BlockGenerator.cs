﻿using UnityEngine;
using System.Collections;

public class BlockGenerator
{
    bool bTestBlock = false;
    BlockType nTestBlockType = BlockType.BLOCK_TYPE_J;
    BlockAngle nTestBlockAngle = BlockAngle.BLOCK_ANGLE_0;

    bool bTestCell = false;
    BlockType nTestCellBlockType = BlockType.BLOCK_TYPE_J;
    BlockAngle nTestCellBlockAngle = BlockAngle.BLOCK_ANGLE_270;
    int nTestCellBlockStartX = 0;
    int nTestCellBlockStartY = 0;

    Brick[,] dCopyMatrix = new Brick[GameData.MAX_CELL_MATRIX_SIZE, GameData.MAX_CELL_MATRIX_SIZE];

    static BlockGenerator _this = null;

    public static BlockGenerator GetInstance()
    {
        if(_this == null) {
            _this = new BlockGenerator();
        }

        return _this;
    }

    public Brick[,] SpawnRandomBlock()
    {
        BlockType nType;
        BlockAngle nAngle;

        return SpawnRandomBlock(out nType, out nAngle);
    }

    public Brick[,] SpawnRandomBlock(out BlockType nType, out BlockAngle nAngle)
    {
        bool bCanDrop = true;

        do {
            nType = (BlockType)Random.Range(0, (int)BlockType.BLOCK_TYPE_COUNT);
            nAngle = (BlockAngle)Random.Range(0, (int)BlockAngle.BLOCK_ANGLE_COUNT);

            if (GameData.RoundData.nBlockMode == BlockMode.EASY)
            {
                bCanDrop = CheckSimpleBlock(nType, nAngle);
            }
            else if(GameData.RoundData.nBlockMode == BlockMode.MID)
            {
                bCanDrop = CheckValidBlock(nType, nAngle);
            }

        } while (!bCanDrop);

        if (bTestBlock)
        {
            nType = nTestBlockType;
            nAngle = nTestBlockAngle;
        }

        return SpawnBlock(nType, nAngle);
    }

    private bool CheckSimpleBlock(BlockType nType, BlockAngle nAngle)
    {
        bool bSimple = false;
        switch (nType)
        {
            case BlockType.BLOCK_TYPE_O:                // OOO
                                                        // OOO
                if (nAngle == BlockAngle.BLOCK_ANGLE_90 || nAngle == BlockAngle.BLOCK_ANGLE_270)
                {
                    bSimple = true;
                }
                break;
            case BlockType.BLOCK_TYPE_P:                // OOO
                                                        //  OO
                // All NOT Simple
                break;
            case BlockType.BLOCK_TYPE_C:                // OOO
                                                        // O O
                // All NOT Simple
                break;
            case BlockType.BLOCK_TYPE_B:                // OOO
                                                        // OO
                // All NOT Simple
                break;
            case BlockType.BLOCK_TYPE_J:                // OOO
                                                        //   O
                if (nAngle == BlockAngle.BLOCK_ANGLE_0 || nAngle == BlockAngle.BLOCK_ANGLE_90)
                {
                    bSimple = true;
                }
                break;
            case BlockType.BLOCK_TYPE_T:                // OOO
                                                        //  O
                if (nAngle == BlockAngle.BLOCK_ANGLE_0)
                {
                    bSimple = true;
                }
                break;
            case BlockType.BLOCK_TYPE_L:                // OOO
                                                        // O
                if (nAngle == BlockAngle.BLOCK_ANGLE_0 || nAngle == BlockAngle.BLOCK_ANGLE_270)
                {
                    bSimple = true;
                }
                break;
            case BlockType.BLOCK_TYPE_S:                //  OO
                                                        // OO
                // All NOT Simple
                break;
            case BlockType.BLOCK_TYPE_Z:                // OO
                                                        //  OO
                // All NOT Simple
                break;
        }

        return bSimple;
    }

    private bool CheckValidBlock(BlockType nType, BlockAngle nAngle)
    {
        bool bValid = true;
        switch (nType)
        {
            case BlockType.BLOCK_TYPE_O:                // OOO
                                                        // OOO
                // All Valid
                break;
            case BlockType.BLOCK_TYPE_P:                // OOO
                                                        //  OO
                if (nAngle == BlockAngle.BLOCK_ANGLE_180 || nAngle == BlockAngle.BLOCK_ANGLE_270)
                {      
                    bValid = false;
                }
                break;
            case BlockType.BLOCK_TYPE_C:                // OOO
                                                        // O O
                if (nAngle == BlockAngle.BLOCK_ANGLE_90 || nAngle == BlockAngle.BLOCK_ANGLE_180 || nAngle == BlockAngle.BLOCK_ANGLE_270)
                {      
                    bValid = false;
                }
                break;
            case BlockType.BLOCK_TYPE_B:                // OOO
                                                        // OO
                if (nAngle == BlockAngle.BLOCK_ANGLE_90 || nAngle == BlockAngle.BLOCK_ANGLE_180)
                {
                    bValid = false;
                }
                break;
            case BlockType.BLOCK_TYPE_J:                // OOO
                                                        //   O
                if (nAngle == BlockAngle.BLOCK_ANGLE_180 || nAngle == BlockAngle.BLOCK_ANGLE_270)
                {
                    bValid = false;
                }
                break;
            case BlockType.BLOCK_TYPE_T:                // OOO
                                                        //  O
                if (nAngle == BlockAngle.BLOCK_ANGLE_90 || nAngle == BlockAngle.BLOCK_ANGLE_180 || nAngle == BlockAngle.BLOCK_ANGLE_270)
                {
                    bValid = false;
                }
                break;
            case BlockType.BLOCK_TYPE_L:                // OOO
                                                        // O
                if (nAngle == BlockAngle.BLOCK_ANGLE_90 || nAngle == BlockAngle.BLOCK_ANGLE_180)
                {
                    bValid = false;
                }
                break;
            case BlockType.BLOCK_TYPE_S:                //  OO
                                                        // OO
                bValid = false; // All NOt Valid
                break;
           case BlockType.BLOCK_TYPE_Z:                 // OO
                                                        //  OO
                bValid = false; // All NOT Valid
                break;
        }

        return bValid;
    }

    public Brick[,] SpawnBlock(BlockType nType, BlockAngle nAngle)
    {
        Brick[,] dBlockMatrix = new Brick[GameData.MAX_BLOCK_MATRIX_SIZE_Y, GameData.MAX_BLOCK_MATRIX_SIZE_X];

        for (int nY = 0; nY < GameData.MAX_BLOCK_MATRIX_SIZE_Y; nY++)
        {
            for (int nX = 0; nX < GameData.MAX_BLOCK_MATRIX_SIZE_X; nX++)
            {
                dBlockMatrix[nY, nX] = null;
            }
        }

        switch (nType)
        {
            case BlockType.BLOCK_TYPE_O :               // OOO
                                                        // OOO 
                dBlockMatrix[0, 0] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[0, 1] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[0, 2] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 0] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 1] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 2] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                break;
            case BlockType.BLOCK_TYPE_P :               // OOO
                                                        //  OO
                dBlockMatrix[0, 1] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[0, 2] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 0] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 1] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 2] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                break;
            case BlockType.BLOCK_TYPE_C:                // OOO
                                                        // O O
                dBlockMatrix[0, 0] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[0, 2] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 0] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 1] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 2] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                break;
            case BlockType.BLOCK_TYPE_B:                // OOO
                                                        // OO
                dBlockMatrix[0, 0] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[0, 1] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 0] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 1] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 2] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                break;
            case BlockType.BLOCK_TYPE_J:                // OOO
                                                        //   O
                dBlockMatrix[0, 2] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 0] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 1] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 2] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                break;
            case BlockType.BLOCK_TYPE_T:                // OOO
                                                        //  O
                dBlockMatrix[0, 1] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 0] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 1] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 2] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                break;
            case BlockType.BLOCK_TYPE_L:                // OOO
                                                        // O
                dBlockMatrix[0, 0] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 0] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 1] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 2] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                break;
            case BlockType.BLOCK_TYPE_S:                //  OO
                                                        // OO
                dBlockMatrix[0, 0] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[0, 1] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 1] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 2] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                break;
            case BlockType.BLOCK_TYPE_Z:                // OO
                                                        //  OO
                dBlockMatrix[0, 1] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[0, 2] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 0] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                dBlockMatrix[1, 1] = SpawnBrick(BrickType.BRICK_TYPE_NORMAL);
                break;
        }

        return dBlockMatrix;
    }

    public Brick[,] CreateRandomCell()
    {
        BlockType nType;
        BlockAngle nAngle;
        // for test
/*
        nType = BlockType.BLOCK_TYPE_C;
        nAngle = BlockAngle.BLOCK_ANGLE_0;
        Brick[,] dBlockMatrix = SpawnBlock(nType, nAngle, true);*/
        Brick[,] dBlockMatrix = SpawnRandomBlock(out nType, out nAngle);
        Brick[,] dCellMatrix = new Brick[GameData.MAX_CELL_MATRIX_SIZE, GameData.MAX_CELL_MATRIX_SIZE];

        if (bTestCell)
        {
            nType = nTestCellBlockType;
            nAngle = nTestCellBlockAngle;
            dBlockMatrix = SpawnBlock(nType, nAngle);
        }

        int nMaxStartX = 1;
        int nMaxStartY = 2;
        int nBlockSizeX = GameData.MAX_BLOCK_MATRIX_SIZE_X;
        int nBlockSizeY = GameData.MAX_BLOCK_MATRIX_SIZE_Y;
        if (nAngle == BlockAngle.BLOCK_ANGLE_90 || nAngle == BlockAngle.BLOCK_ANGLE_270)
        {
            nMaxStartX = 2;
            nMaxStartY = 1;
            nBlockSizeX = GameData.MAX_BLOCK_MATRIX_SIZE_Y;
            nBlockSizeY = GameData.MAX_BLOCK_MATRIX_SIZE_X;
        }

        int nStartX = Random.Range(0, nMaxStartX + 1);
        int nStartY = Random.Range(0, nMaxStartY + 1);

        if (bTestCell)
        {
            nStartX = nTestCellBlockStartX;
            nStartY = nTestCellBlockStartY;
        }
        int nEndX = nStartX + nBlockSizeX - 1;
        int nEndY = nStartY + nBlockSizeY - 1;

/*
        Debug.Log("Random Cell, Type : " + nType + ", Angle : " + nAngle);
        Debug.Log("Block Size : (" + nBlockSizeY + ", " + nBlockSizeX + ")");
        Debug.Log("Block Area : (" + nStartY + ", " + nStartX + ") ~ (" + nEndY + ", " + nEndX + ")");
        DebugPrintBlock(dBlockMatrix, nAngle);
*/ 
        int nBlockX = 0;
        int nBlockY = 0;
        for (int nY = 0; nY < GameData.MAX_CELL_MATRIX_SIZE; nY++)
        {
            for (int nX = 0; nX < GameData.MAX_CELL_MATRIX_SIZE; nX++)
            {
                if (nX >= nStartX && nX <= nEndX && nY >= nStartY && nY <= nEndY)
                {
                    dCellMatrix[nY, nX] = null;

                    nBlockX = nX - nStartX;
                    nBlockY = nY - nStartY;
                    Brick obBrick = GetBrick(dBlockMatrix, nBlockX, nBlockY, nAngle);
                    if (obBrick == null)
                    {
//                        bool bCreateDummyBlock = true;
                        if (nY > 0 && (dCellMatrix[nY - 1, nX] == null || dCellMatrix[nY - 1, nX].nType == BrickType.BRICK_TYPE_DUMMY_HOLLOW))
                        {
                                // if the lower block is empty or hollow
                            if (GameData.RoundData.nBlockMode == BlockMode.HOLLOW)
                            {
                                dCellMatrix[nY, nX] = SpawnBrick(BrickType.BRICK_TYPE_DUMMY_HOLLOW);
                            }

                            continue;

                                //                              bCreateDummyBlock = false;
                        }
//                        }

//                        if (bCreateDummyBlock) { 
                        dCellMatrix[nY, nX] = SpawnBrick(BrickType.BRICK_TYPE_DUMMY);
//                        }
                    }
                }
                else
                {
                    if (nX < nStartX || nX > nEndX) {
                        dCellMatrix[nY, nX] = SpawnBrick(BrickType.BRICK_TYPE_DUMMY);
                    }
                    else if (nY < nStartY) {
                        dCellMatrix[nY, nX] = SpawnBrick(BrickType.BRICK_TYPE_DUMMY);
                    }
                }
            }
        }

        FreeBlock(dBlockMatrix);

        return dCellMatrix;
    }

    Brick SpawnBrick(BrickType nType)
    {
        Bricks obBricks = GamePlay.GetInstance().obBricks;
        
        Brick obBrick;
        if (nType == BrickType.BRICK_TYPE_DUMMY_HOLLOW)
        {
            obBrick = obBricks.GetFreeBrickHollow();
        }
        else
        {
            obBrick = obBricks.GetFreeBrick();
        }

        if (obBrick == null)
        {
            Debug.Log(">>>>>>>> No Brick", obBrick);
            return null;
        }

        obBrick.nType = nType;
        obBrick.SetBlink(nType == BrickType.BRICK_TYPE_DUMMY_HOLLOW);
        obBrick.Spawn(Vector3.zero);

        return obBrick;
    }

    public Brick[,] CopyCell(Brick[,] dCellMatrix)
    {
        for (int nY = 0; nY < GameData.MAX_CELL_MATRIX_SIZE; nY++)
        {
            for (int nX = 0; nX < GameData.MAX_CELL_MATRIX_SIZE; nX++)
            {
                dCopyMatrix[nY, nX] = dCellMatrix[nY, nX];
            }
        }

        return dCopyMatrix;
    }

    public Brick GetBrick(Brick[,] dBlockMatrix, int nBlockX, int nBlockY, BlockAngle nAngle)
    {
        int nBlockSizeX = GameData.MAX_BLOCK_MATRIX_SIZE_X;
        int nBlockSizeY = GameData.MAX_BLOCK_MATRIX_SIZE_Y;
        if (nAngle == BlockAngle.BLOCK_ANGLE_90 || nAngle == BlockAngle.BLOCK_ANGLE_270)
        {
            nBlockSizeX = GameData.MAX_BLOCK_MATRIX_SIZE_Y;
            nBlockSizeY = GameData.MAX_BLOCK_MATRIX_SIZE_X;
        }

        int nRotateBlockX = 0;
        int nRotateBlockY = 0;

        switch (nAngle)
        {
            case BlockAngle.BLOCK_ANGLE_0:
                // 1,0 1,1 1,2
                // 0,0 0,1 0,2
                nRotateBlockX = nBlockX;
                nRotateBlockY = nBlockY;
                break;

            case BlockAngle.BLOCK_ANGLE_90:
                // 2,0 2,1  ->  1,2 0,2
                // 1,0 1,1  ->  1,1 0,1
                // 0,0 0,1  ->  1,0 0,0
                nRotateBlockX = nBlockY;
                nRotateBlockY = nBlockSizeX - 1 - nBlockX;
                break;

            case BlockAngle.BLOCK_ANGLE_180:
                // 1,0 1,1 1,2  ->  0,2 0,1 0,0
                // 0,0 0,1 0,2  ->  1,2 1,1 1,0
                nRotateBlockX = nBlockSizeX - 1 - nBlockX;
                nRotateBlockY = nBlockSizeY - 1 - nBlockY;
                break;
            case BlockAngle.BLOCK_ANGLE_270:
                // 2,0 2,1  ->  0,0 1,0
                // 1,0 1,1  ->  0,1 1,1
                // 0,0 0,1  ->  0,2 1,2
                nRotateBlockX = nBlockSizeY - 1 - nBlockY;
                nRotateBlockY = nBlockX;
                break;
        }

        //        Debug.Log("Convert : (" + nBlockX + ", " + nBlockY + ") -> (" + nRotateBlockX + ", " + nRotateBlockY + ")");
        //        Debug.Log("      ==> " + dBlockMatrix[nRotateBlockY, nRotateBlockX]);

        return dBlockMatrix[nRotateBlockY, nRotateBlockX];
    }

    public void FreeBlock(Brick[,] dBlockMatrix)
    {
        for (int nY = 0; nY < GameData.MAX_BLOCK_MATRIX_SIZE_Y; nY++)
        {
            for (int nX = 0; nX < GameData.MAX_BLOCK_MATRIX_SIZE_X; nX++)
            {
                Brick obBrick = dBlockMatrix[nY, nX];
                if (obBrick)
                {
                    obBrick.Free();
                }
            }
        }
    }

    public void DebugPrintCell(Brick[,] dCellMatrix)
    {
        for (int nY = GameData.MAX_CELL_MATRIX_SIZE - 1; nY >= 0; nY--)
        {
            string strLog = "";
            for (int nX = 0; nX < GameData.MAX_CELL_MATRIX_SIZE; nX++)
            {
                if (dCellMatrix[nY, nX] != null)
                {
                    strLog += "1 ";
                }
                else {
                    strLog += "0 ";
                }
            }
            Debug.Log(strLog);
        }
    }

    public void DebugPrintBlock(Brick[,] dBlockMatrix, BlockAngle nAngle)
    {
        int nBlockSizeX = GameData.MAX_BLOCK_MATRIX_SIZE_X;
        int nBlockSizeY = GameData.MAX_BLOCK_MATRIX_SIZE_Y;
        if (nAngle == BlockAngle.BLOCK_ANGLE_90 || nAngle == BlockAngle.BLOCK_ANGLE_270)
        {
            nBlockSizeX = GameData.MAX_BLOCK_MATRIX_SIZE_Y;
            nBlockSizeY = GameData.MAX_BLOCK_MATRIX_SIZE_X;
        }

        for (int nY = nBlockSizeY - 1; nY >= 0; nY--)
        {
            string strLog = "";
            for (int nX = 0; nX < nBlockSizeX; nX++)
            {
                if (GetBrick(dBlockMatrix, nX, nY, nAngle) != null)
                {
                    strLog += "1 ";
                }
                else
                {
                    strLog += "0 ";
                }
            }
            Debug.Log(strLog);
        }
    }
}
