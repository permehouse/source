﻿using UnityEngine;
using System.Collections;

public class Bricks : MonoBehaviour {
    SpawnObjectPool iBrickPool = new SpawnObjectPool();
    SpawnObjectPool iBrickHollowPool = new SpawnObjectPool();
    void Awake()
    {
        gameObject.name = GameData.OBJECT_NAME_BRICKS;
    }

    public Brick GetFreeBrick()
    {
        return (Brick)iBrickPool.GetFreeObject();
    }

    public Brick GetFreeBrickHollow()
    {
        return (Brick)iBrickHollowPool.GetFreeObject();
    }

    public void OnPlayStatusInitComponent()
    {
        iBrickPool.CreatePool(GameData.SpawnObjectPool.BRICK_POOL_SIZE, GamePlay.GetInstance().pfBrick, transform);
        iBrickHollowPool.CreatePool(GameData.SpawnObjectPool.BRICK_HOLLOW_POOL_SIZE, GamePlay.GetInstance().pfBrick_Hollow, transform);
    }

    public void OnPlayStatusDestoryComponent()
    {
        iBrickPool.DestroyPool();
        iBrickHollowPool.DestroyPool();
    }
}
