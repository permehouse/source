﻿using UnityEngine;
using System.Collections;

public class Blocks : MonoBehaviour {
    public Block obCurBlock = null;

    SpawnObjectPool iBlockPool = new SpawnObjectPool();

    void Awake()
    {
        gameObject.name = GameData.OBJECT_NAME_BLOCKS;
    }

    public void SpawnNewBlock()
    {
        if (GameData.nPlayStatus == PlayStatus.PLAY)
        {
            obCurBlock = (Block)iBlockPool.GetFreeObject();
            if (obCurBlock == null)
            {
                return;
            }

            obCurBlock.Spawn(Vector3.zero);
        }
    }

    public void OnPlayStatusInitComponent()
    {
        iBlockPool.CreatePool(GameData.SpawnObjectPool.BLOCK_POOL_SIZE, GamePlay.GetInstance().pfBlock, transform);
    }
    
    public void OnPlayStatusStartPlanet()
    {
        //obPlayground = GamePlay.GetInstance().obPlayground;
        //        obPlanet = GamePlay.GetInstance().obPlanet;
    }

    public void OnPlayStatusStartRound()
    {
        if (obCurBlock) {
            obCurBlock.Free();
            obCurBlock = null;
        }
    }

    public void OnPlayStatusPlay()
    {
        SpawnNewBlock();
    }

    public void OnPlayStatusEnding()
    {
        if (obCurBlock)
        {
            obCurBlock.Break();
            obCurBlock = null;
        }
    }

    public void OnPlayStatusDestoryComponent()
    {
        iBlockPool.DestroyPool();
    }

    public void OnPlayEventBlockPass()
    {
        SpawnNewBlock();
    }

    public void OnPlayEventBlockFitToCell()
    {
        SpawnNewBlock();
    }

    public void OnPlayEventCellBreak()
    {
        SpawnNewBlock();
    }
    public void OnPlayEventBlockHitToCamera()
    {
        SpawnNewBlock();
    }

}
