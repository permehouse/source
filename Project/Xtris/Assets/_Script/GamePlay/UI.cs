﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UI : MonoBehaviour {
    GameObject obResultPanel = null;
    GameObject obRoundScore = null;
    GameObject obBonusScore = null;
    GameObject obTotalScore= null;
    Text txtRoundScore = null;
    Text txtBonusScore = null;
    Text txtTotalScore = null;
    Text txtBestScore = null;

    GameObject obEndPanel = null;
    Button btnWormHole = null;
    Button btnGoToNextRound = null;
    Button btnGoToNextPlanet = null;
    Button btnRestart = null;
    Button btnQuit = null;
    Text txtRestart = null;
    Text txtGoToNextRound = null;
    Text txtGoToNextPlanet = null;
    Text txtQuit = null;
    Image imgGoToNextPlanet = null;

    GameObject obWormHolePanel = null;
    Button btnBack = null;
    Button btnResume = null;
    Button btnRestartInWormHolePanel = null;
    Button btnQuitInWormHolePanel = null;
    Text txtResume = null;
    Text txtRestartInWormHolePanel= null;
    Text txtQuitInWormHolePanel = null;

    GameObject obReplayPanel = null;
    Button btnReplayRecord = null;
    Button btnReplayShare = null;
    Image imgReplayRecording = null;
    Text txtReplayRecord = null;
    Text txtReplayShare = null;

    Text txtDebugging = null;

    bool bShowWormHolePanel = false;
    bool bShowResultPanel = false;
    bool bPlayScoreCountingSound = false;

    public Sprite[] obPlanetIcon = new Sprite[GameData.nPlanetCount];

    public Sprite obAdsIcon = null;
    public Sprite obStarIcon = null;

    float nResultRoundScore = 0f;
    float nResultBonusScore = 0f;
    float nResultTotalScore = 0f;

    void Awake()
    {
        gameObject.name = GameData.OBJECT_NAME_UI;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
    }

    void LateUpdate()
    {
        if (GameData.nPlayStatus == PlayStatus.NONE) {
            return;
        }

        UpdateButtonStatus();
        UpdateButtonIcon();

        UpdateWormHoleButtonRotation();
        UpdateBackButtonRotation();

        UpdateResultScorePos();
        UpdateResultScore();
        UpdateResultBestText();

        UpdateReplayButtons();

        DisplayDebuggingText();
    }

    void DisplayDebuggingText()
    {
        if (!GameData.Test.bTest)
        {
            txtDebugging.gameObject.SetActive(false);
            return;
        }
        txtDebugging.gameObject.SetActive(true);

/*
        string str = "\nScreen : " + Screen.width + ", " + Screen.height + "\n";
        str += "\nResolution : " + Screen.currentResolution.width + ", " + Screen.currentResolution.height;

        Camera obCamera = GamePlay.GetInstance().obCamera.GetComponent<Camera>();
        str += "\nFOV : " + obCamera.fieldOfView;

        ShowDebuggingText(str);
 */
//        string str = ReplayManager.GetInstance().GetDebugString();
//        ShowDebuggingText(str);
    }

    public void ShowDebuggingText(string str)
    {
        if (!GameData.Test.bTest)
        {
            return;
        }

        txtDebugging.text = str;
    }

    void UpdateButtonStatus()
    {
        float nAlpha = Mathf.Abs(Mathf.Cos(Mathf.PI * Time.time)) * (1f - GameData.UI_BUTTON_BLINK_ALPHA) + GameData.UI_BUTTON_BLINK_ALPHA;

        bool bDisable = !StarCoinManager.GetInstance().CanRestart() && !StarCoinManager.GetInstance().CanGetReward();
        SetButtonStatus(ref btnRestart, bDisable, nAlpha);
        SetButtonStatus(ref btnRestartInWormHolePanel, bDisable, nAlpha);

        SetButtonStatus(ref btnGoToNextPlanet, false, nAlpha);
        SetButtonStatus(ref btnGoToNextRound, false, nAlpha);

        SetButtonStatus(ref btnResume, false, nAlpha);

        SetButtonStatus(ref btnQuit, false, nAlpha);
        SetButtonStatus(ref btnQuitInWormHolePanel, false, nAlpha);
    }

    void SetButtonStatus(ref Button btn, bool bDisable, float nAlpha)
    {
        if (btn.IsActive())
        {
            if (!bDisable)
            {
                SetButtonColor(ref btn, GameData.UI_BUTTON_COLOR);
                MakeButtonTransparent(ref btn, nAlpha);
            }
            else
            {
                SetButtonColor(ref btn, GameData.UI_BUTTON_DISABLE_COLOR);
            }
        }
    }

    void SetButtonColor(ref Button btn, Color color)
    {
        Image obImage = btn.GetComponent<Image>();
        obImage.color = color;
    }

    void MakeButtonTransparent(ref Button btn, float nAlpha)
    {
        Image obImage = btn.GetComponent<Image>();
        Color color = obImage.color;
        color.a = nAlpha;
        obImage.color = color;

        Transform trText = btn.transform.Find("Text");
        if (trText)
        {
            Text obText = trText.GetComponent<Text>();
            if (obText)
            {
                color = obText.color;
                color.a = nAlpha;
                obText.color = color;
            }
        }

        Transform trIcon = btn.transform.Find("Icon");
        if (trIcon)
        {
            Image obIcon = trIcon.GetComponent<Image>();
            if (obIcon)
            {
                color = obIcon.color;
                color.a = nAlpha;
                obIcon.color = color;
            }
        }
    }

    void UpdateButtonIcon()
    {
        if (btnRestart.IsActive())
        {
            ShowButtonIcon(ref btnRestart, StarCoinManager.GetInstance().CanRestart());
        }

        if (btnRestartInWormHolePanel.IsActive())
        {
            ShowButtonIcon(ref btnRestartInWormHolePanel, StarCoinManager.GetInstance().CanRestart());
        }
    }

    void ShowButtonIcon(ref Button btn, bool bCanPlay)
    {
        Transform trIcon = btn.transform.Find("Icon");
        if (trIcon)
        {
            Image obIcon = trIcon.GetComponent<Image>();
            if (bCanPlay)
            {
                obIcon.sprite = obStarIcon;
            }
            else
            {
                obIcon.sprite = obAdsIcon;
            }
        }
    }

    void UpdateWormHoleButtonRotation()
    {
        if (bShowWormHolePanel)
        {
            return;
        }

        RectTransform obRectTransform = btnWormHole.transform.GetComponent<RectTransform>();
        obRectTransform.Rotate(new Vector3(0f, 0f, -GameData.UI_WARM_HOLE_BUTTON_SPEED * Time.deltaTime));

        // Sync Angle to Back button
        RectTransform obRectTransformBack = btnBack.transform.GetComponent<RectTransform>();
        obRectTransformBack.localRotation = obRectTransform.localRotation;
    }

    void UpdateBackButtonRotation()
    {
        if (!bShowWormHolePanel)
        {
            return;
        }

        RectTransform obRectTransform = btnBack.transform.GetComponent<RectTransform>();
        obRectTransform.Rotate(new Vector3(0f, 0f, GameData.UI_BACK_BUTTON_SPEED * Time.deltaTime));

        // Sync Angle to Warm Hole button
        RectTransform obRectTransformWormHole = btnWormHole.transform.GetComponent<RectTransform>();
        obRectTransformWormHole.localRotation = obRectTransform.localRotation;
    }

    void UpdateResultScorePos()
    {
        if (bShowResultPanel)
        {
            float nStep = GameData.RESULT_SCORE_MOVE_SPEED * Time.deltaTime;
            MoveResultScorePos(ref obRoundScore, nStep);
            MoveResultScorePos(ref obBonusScore, nStep);
            MoveResultScorePos(ref obTotalScore, nStep);
        }
    }

    void SetResultScorePos(ref GameObject obScore, float nX)
    {
        RectTransform obRectTransform = obScore.transform.GetComponent<RectTransform>();
        Vector3 pos = new Vector3(nX, obRectTransform.localPosition.y, obRectTransform.localPosition.z);
        obRectTransform.localPosition = pos;
    }

    void MoveResultScorePos(ref GameObject obScore, float nStep)
    {
        RectTransform obRectTransform = obScore.transform.GetComponent<RectTransform>();
        Vector3 pos = new Vector3(0, obRectTransform.localPosition.y, obRectTransform.localPosition.z);
        if ((pos - obRectTransform.position).magnitude < 0.01f)
        {
            obRectTransform.localPosition = pos;
        }
        else {
            obRectTransform.localPosition = Vector3.MoveTowards(obRectTransform.localPosition, pos, nStep);
        }
    }

    void UpdateResultScore()
    {
        if (bShowResultPanel)
        {
            SetResultScore(ref obRoundScore, ref nResultRoundScore, GameData.nRoundScore);
            SetResultScore(ref obBonusScore, ref nResultBonusScore, GameData.nBonusScore);
            SetResultScore(ref obTotalScore, ref nResultTotalScore, GameData.nScore);
        }
    }

    void SetResultScore(ref GameObject obScore, ref float nScore, int nScoreTo)
    {
        RectTransform obRectTransform = obScore.transform.GetComponent<RectTransform>();
        if (obRectTransform.localPosition.x == 0f)
        {
            if (!bPlayScoreCountingSound)
            {
                bPlayScoreCountingSound = true;
                GamePlay.GetInstance().SetPlayEvent(PlayEvent.SCORE_COUNTING);
            }

            Text obText = obScore.transform.Find("ScoreText").GetComponent<Text>();
            if (obText)
            {
                nScore += (((float)nScoreTo / GameData.ROUND_ENDING_SCORE_TIME) * Time.deltaTime);
                nScore = Mathf.Min(nScore, nScoreTo);

                obText.text = nScore.ToString("N0");
            }
        }
    }

    void UpdateResultBestText()
    {
        if (bShowResultPanel)
        {
            txtBestScore.text = LanguageData.strNewBest;
            txtBestScore.gameObject.SetActive(nResultTotalScore == GameData.nScore && GameData.nScore >= GameData.nBestScore);
        }
    }

    void UpdateReplayButtons()
    {
        bool bTestShowRecordReplay = (GameData.Test.bTest && GameData.Test.bShowRecordReplay && !GameData.Test.bRecordingReplay);
        bool bTestRecordingReplay = (GameData.Test.bTest && GameData.Test.bShowRecordReplay && GameData.Test.bRecordingReplay);

        obReplayPanel.gameObject.SetActive(true);
        if (ReplayManager.GetInstance().IsReadyForRecording() || bTestShowRecordReplay)
        {
            // Record Replay
            btnReplayRecord.gameObject.SetActive(false);
            btnReplayShare.gameObject.SetActive(false);
            imgReplayRecording.gameObject.SetActive(false);

            if (GameData.nPlayStatus == PlayStatus.READY || 
                (GameData.nPlayStatus == PlayStatus.PAUSE && (GameData.nPlayStatusOnPause == PlayStatus.READY || GameData.nPlayStatusOnPause == PlayStatus.PLAY)))
            {
                btnReplayRecord.gameObject.SetActive(true);
                float nAlpha = Mathf.Abs(Mathf.Cos(Mathf.PI * Time.time)) * (1f - GameData.UI_BUTTON_BLINK_ALPHA) + GameData.UI_BUTTON_BLINK_ALPHA;
                MakeButtonTransparent(ref btnReplayRecord, nAlpha);
            }
        }
        else if (ReplayManager.GetInstance().IsRecording() || ReplayManager.GetInstance().IsRecordingStopped() || bTestRecordingReplay)
        {
            // Share Replay
            btnReplayRecord.gameObject.SetActive(false);
            btnReplayShare.gameObject.SetActive(false);
            imgReplayRecording.gameObject.SetActive(false);

            float nAlpha = Mathf.Abs(Mathf.Cos(Mathf.PI * Time.time)) * (1f - GameData.UI_BUTTON_BLINK_ALPHA) + GameData.UI_BUTTON_BLINK_ALPHA;
            if (GameData.nPlayStatus == PlayStatus.END_ROUND || GameData.nPlayStatus == PlayStatus.END_PLANET || GameData.nPlayStatus == PlayStatus.PAUSE)
            {
                btnReplayShare.gameObject.SetActive(true);
                MakeButtonTransparent(ref btnReplayShare, nAlpha);
            }

            if (GameData.nPlayStatus == PlayStatus.READY || GameData.nPlayStatus == PlayStatus.PLAY || GameData.nPlayStatus == PlayStatus.ENDING || GameData.nPlayStatus == PlayStatus.RESULT)
            { 
                imgReplayRecording.gameObject.SetActive(true);
                Color color = imgReplayRecording.color;
                color.a = nAlpha;
                imgReplayRecording.color = color;
            }
        }
    }

    public void ShowGoToNextRoundButton(bool bShow = true)
    {
        btnGoToNextRound.gameObject.SetActive(bShow);
    }

    public void ShowRestartButton(bool bShow = true)
    {
        btnRestart.gameObject.SetActive(bShow);
    }

    public void ShowGoToNextPlanetButton(bool bShow, PlanetType nPlanetType = PlanetType.PLANET_TYPE_EARTH)
    {
        btnGoToNextPlanet.gameObject.SetActive(bShow);

        if (bShow)
        {
            string strText = "";
            switch (nPlanetType)
            {
                case PlanetType.PLANET_TYPE_EARTH: strText = LanguageData.strNextPlanet_Earth; break;
                case PlanetType.PLANET_TYPE_MARS : strText = LanguageData.strNextPlanet_Mars; break;
                case PlanetType.PLANET_TYPE_JUPITER: strText = LanguageData.strNextPlanet_Jupiter; break;
            }

            txtGoToNextPlanet.text = strText;
            imgGoToNextPlanet.sprite = obPlanetIcon[(int)nPlanetType];
        }
    }
    public void ShowQuitButton(bool bShow = true)
    {
        btnQuit.gameObject.SetActive(bShow);
    }

    public void OnPlayStatusInitComponent()
    {
        btnWormHole = transform.Find("WormHoleButton").GetComponent<Button>();
        btnBack = transform.Find("BackButton").GetComponent<Button>();

        obResultPanel = transform.Find("ResultPanel").gameObject;
        obRoundScore = obResultPanel.transform.Find("RoundScore").gameObject;
        txtRoundScore = obRoundScore.transform.Find("Text").GetComponent<Text>();
        obBonusScore = obResultPanel.transform.Find("BonusScore").gameObject;
        txtBonusScore = obBonusScore.transform.Find("Text").GetComponent<Text>();
        obTotalScore= obResultPanel.transform.Find("TotalScore").gameObject;
        txtTotalScore = obTotalScore.transform.Find("Text").GetComponent<Text>();
        txtBestScore = obTotalScore.transform.Find("BestText").GetComponent<Text>();

        obEndPanel = transform.Find("EndPanel").gameObject;
        btnRestart = obEndPanel.transform.Find("RestartButton").GetComponent<Button>();
        txtRestart = btnRestart.transform.Find("Text").GetComponent<Text>();
        btnGoToNextRound = obEndPanel.transform.Find("GoToNextRoundButton").GetComponent<Button>();
        txtGoToNextRound = btnGoToNextRound.transform.Find("Text").GetComponent<Text>();
        btnGoToNextPlanet = obEndPanel.transform.Find("GoToNextPlanetButton").GetComponent<Button>();
        txtGoToNextPlanet = btnGoToNextPlanet.transform.Find("Text").GetComponent<Text>();
        imgGoToNextPlanet = btnGoToNextPlanet.transform.Find("PlanetIcon").GetComponent<Image>();
        btnQuit = obEndPanel.transform.Find("QuitButton").GetComponent<Button>();
        txtQuit = btnQuit.transform.Find("Text").GetComponent<Text>();

        obWormHolePanel = transform.Find("WormHolePanel").gameObject;
        btnResume = obWormHolePanel.transform.Find("ResumeButton").GetComponent<Button>();
        txtResume = btnResume.transform.Find("Text").GetComponent<Text>();
        btnRestartInWormHolePanel = obWormHolePanel.transform.Find("RestartButton").GetComponent<Button>();
        txtRestartInWormHolePanel = btnRestartInWormHolePanel.transform.Find("Text").GetComponent<Text>();
        btnQuitInWormHolePanel = obWormHolePanel.transform.Find("QuitButton").GetComponent<Button>();
        txtQuitInWormHolePanel = btnQuitInWormHolePanel.transform.Find("Text").GetComponent<Text>();

        obReplayPanel = transform.Find("ReplayPanel").gameObject;
        btnReplayRecord = obReplayPanel.transform.Find("ReplayRecordButton").GetComponent<Button>();
        txtReplayRecord = btnReplayRecord.transform.Find("Text").GetComponent<Text>();
        btnReplayShare = obReplayPanel.transform.Find("ReplayShareButton").GetComponent<Button>();
        txtReplayShare = btnReplayShare.transform.Find("Text").GetComponent<Text>();
        imgReplayRecording = obReplayPanel.transform.Find("ReplayRecordingImage").GetComponent<Image>();

        txtDebugging = transform.Find("DebuggingText").GetComponent<Text>();
    }

    public void OnPlayStatusInitPlanet()
    {
        SetTextLanguage();
    }

    public void OnPlayStatusStartPlanet()
    {
        ShowEndPanel(false);
        ShowResultPanel(false);
        ShowWormHolePanel(false);
    }

    public void OnPlayStatusStartRound()
    {
        ShowEndPanel(false);
        ShowResultPanel(false);
        ShowWormHolePanel(false);
    }

    public void OnPlayStatusResult()
    {
        nResultRoundScore = 0f;
        nResultBonusScore = 0f;
        nResultTotalScore = 0f;

        ShowResultPanel(true);
    }

    public void OnPlayStatusEndRound()
    {
        ShowEndPanel(true);
    }
    public void OnPlayStatusEndPlanet()
    {
        ShowEndPanel(false);
        if(GameData.nGameMode == GameMode.NORMAL) {
            if (GameData.nPlanetType != PlanetType.PLANET_TYPE_LAST) {
                PlanetType nNextPlanetType = GameData.nPlanetType + 1;
                ShowGoToNextPlanetButton(true, nNextPlanetType);
            }
            else {
                ShowWormHolePanel(false, false);
                ShowQuitButton();
            }
        }
        else {
            ShowWormHolePanel(false, false);
            ShowQuitButton();
        }
    }

    public void OnPlayStatusPause()
    {
        ShowWormHolePanel(true);
    }

    public void OnPlayStatusResume()
    {
        ShowWormHolePanel(false);
    }

    public void OnPlayStatusRestartRound()
    {
        ShowWormHolePanel(false);
        ShowEndPanel(false);
        ShowResultPanel(false);
    }

    public void OnPlayStatusQuit()
    {
        ShowWormHolePanel(false, false);
        ShowEndPanel(false);
        ShowResultPanel(false);
    }

    public void OnPlayStatusReward()
    {
        ShowWormHolePanel(false, false);
        ShowEndPanel(false);
        ShowResultPanel(false);
    }

    public void ShowResultPanel(bool bShow)
    {
        bShowResultPanel = bShow;

        if (bShowResultPanel)
        {
            bPlayScoreCountingSound = false;

            obResultPanel.SetActive(true);
            SetResultScorePos(ref obRoundScore, GameData.RESULT_SCORE_OUTSIDE_POS_X);
            SetResultScorePos(ref obBonusScore, GameData.RESULT_SCORE_OUTSIDE_POS_X + GameData.RESULT_SCORE_OUTSIDE_POS_GAP);
            SetResultScorePos(ref obTotalScore, GameData.RESULT_SCORE_OUTSIDE_POS_X + 2 * GameData.RESULT_SCORE_OUTSIDE_POS_GAP);
        }
        else {
            obResultPanel.SetActive(false);
        }
    }

    public void ShowEndPanel(bool bShow)
    {
        if (bShow)
        {
            obEndPanel.SetActive(true);
            if (GameData.nRoundResult == RoundResult.ROUND_CLEAR)
            {
                ShowGoToNextRoundButton(true);
            }
            else if (GameData.nRoundResult == RoundResult.TIME_OVER)
            {
                ShowRestartButton(true);
            }
        }
        else
        {
            ShowRestartButton(false);
            ShowGoToNextRoundButton(false);
            ShowGoToNextPlanetButton(false);
            ShowQuitButton(false);
        }
    }

    public void ShowWormHolePanel(bool bShow, bool bToggleWormHoleButton = true)
    {
        bShowWormHolePanel = bShow;

        btnWormHole.gameObject.SetActive(false);
        btnBack.gameObject.SetActive(false);

        if (bShowWormHolePanel)
        {
            if (bToggleWormHoleButton)
            {
                btnWormHole.gameObject.SetActive(false);
                btnBack.gameObject.SetActive(true);
            }

            obWormHolePanel.SetActive(true);

            if(GameData.nPlayStatusOnPause == PlayStatus.PLAY || 
                ((GameData.nPlayStatusOnPause == PlayStatus.ENDING || GameData.nPlayStatusOnPause == PlayStatus.RESULT) && GameData.nRoundResult != RoundResult.ROUND_CLEAR)) {
                btnRestartInWormHolePanel.gameObject.SetActive(true);
            }
            else {
                btnRestartInWormHolePanel.gameObject.SetActive(false);
            }
        }
        else
        {
            if (bToggleWormHoleButton)
            {
                btnWormHole.gameObject.SetActive(true);
                btnBack.gameObject.SetActive(false);
            }

            obWormHolePanel.SetActive(false);
        }
    }

    void SetTextLanguage()
    {
        txtRoundScore.text = LanguageData.strRoundScore;
        txtBonusScore.text = LanguageData.strBonusScore;
        txtTotalScore.text = LanguageData.strTotalScore;

        txtRestart.text = LanguageData.strRestart;
        txtGoToNextRound.text = LanguageData.strNextRound;
        txtQuit.text = LanguageData.strQuit;

        txtResume.text = LanguageData.strResume;
        txtRestartInWormHolePanel.text = LanguageData.strRestart;
        txtQuitInWormHolePanel.text = LanguageData.strQuit;

        txtReplayRecord.text = LanguageData.strReplayRecord;
        txtReplayShare.text = LanguageData.strReplayShare;
    }

}
