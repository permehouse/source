﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GamePlay : MonoBehaviour
{
    public SpawnObject pfBlock = null;
    public SpawnObject pfBrick = null;
    public SpawnObject pfBrick_Hollow = null;
    public SpawnObject pfStar = null;
    public SpawnObject pfIndicatorBar = null;
    public SpawnObject pfCellFrame = null;

    public Planet[] pfPlanet = new Planet[GameData.nPlanetCount];
    public AdsManager obAdsManager = null;
    public GameObject obFrontLight = null;


    [HideInInspector] 
    public CameraManager obCamera = null;
    [HideInInspector]
    public UI obUI = null;
    [HideInInspector]
    public UI3D obUI3D = null;
    [HideInInspector]
    public Playground obPlayground = null;
    [HideInInspector]
    public Planet obPlanet = null;
    [HideInInspector]
    public Background obBackground = null;
    [HideInInspector]
    public DistanceIndicator obDistanceIndicator = null;
    [HideInInspector]
    public Cells obCells = null;
    [HideInInspector]
    public Blocks obBlocks = null;
    [HideInInspector]
    public Bricks obBricks = null;
    [HideInInspector]
    public Stars obStars = null;
    [HideInInspector]
    public ScreenImage obScreenImage = null;
    [HideInInspector]
    public SoundManager obSoundManager = null;
    [HideInInspector]
    public Controller obController = null;

    ArrayList listEventReceiver = new ArrayList();

    float nScoreBoostTime = 0f;
    float nScoreBoost = 0;
    int nPrevRoundReadyTime = 0;

    static GamePlay _this;
    public static GamePlay GetInstance()
    {
        return _this;
    }

    void Awake()
    {
        _this = this;
    }

    void InitComponent()
    {
        obPlayground = transform.Find(GameData.OBJECT_NAME_PLAYGROUND).GetComponent<Playground>();
        obCells = obPlayground.transform.Find(GameData.OBJECT_NAME_CELLS).GetComponent<Cells>();
        obBlocks = obPlayground.transform.Find(GameData.OBJECT_NAME_BLOCKS).GetComponent<Blocks>();
        obBricks = obPlayground.transform.Find(GameData.OBJECT_NAME_BRICKS).GetComponent<Bricks>();
        obStars = transform.Find(GameData.OBJECT_NAME_STARS).GetComponent<Stars>();
        obCamera = transform.Find(GameData.OBJECT_NAME_CAMERA).GetComponent<CameraManager>();
        obSoundManager = obCamera.transform.Find(GameData.OBJECT_NAME_SOUND_MANAGER).GetComponent<SoundManager>();
        obBackground = transform.Find(GameData.OBJECT_NAME_BACKGROUND).GetComponent<Background>();
        obDistanceIndicator = transform.Find(GameData.OBJECT_NAME_DISTANCE_INDICATOR).GetComponent<DistanceIndicator>();
        obUI = transform.Find(GameData.OBJECT_NAME_UI).GetComponent<UI>();
        obScreenImage = obUI.transform.Find(GameData.OBJECT_NAME_SCREEN_IMAGE).GetComponent<ScreenImage>();
        obUI3D = transform.Find(GameData.OBJECT_NAME_UI3D).GetComponent<UI3D>();
        obController = transform.Find(GameData.OBJECT_NAME_CONTROLLER).GetComponent<Controller>();

        listEventReceiver.Add(_this);       
        listEventReceiver.Add(obPlayground);
        listEventReceiver.Add(obCells);
        listEventReceiver.Add(obBlocks);
        listEventReceiver.Add(obBricks);
        listEventReceiver.Add(obStars);
        listEventReceiver.Add(obCamera);
        listEventReceiver.Add(obBackground);
        listEventReceiver.Add(obDistanceIndicator);
        listEventReceiver.Add(obUI);
        listEventReceiver.Add(obScreenImage);
        listEventReceiver.Add(obUI3D);
        listEventReceiver.Add(obSoundManager);
        listEventReceiver.Add(obController);
    }

    void Start()
    {
        GameData.nGameStatus = GameStatus.GAME_PLAY;

        InitComponent();

        // late start gameplay for finishing 'InitComponent' of other objects
        InvokeManager.GetInstance().SetInvoke(this, "StartGamePlay", 0.1f);
    }

    void Update()
    {
        CheckRoundReadyTime();

        if (GameData.nPlayStatus == PlayStatus.PLAY)
        {
            if (!CheckRoundClear()) {
                CheckTimeOver();
            }
        }
    }

    void FixedUpdate()
    {
        UpdatePlayTime();
        UpdateScore();
        UpdateBestRecord();
    }

    public void SetPlayEvent(PlayEvent nPlayEvent)
    {
        GameData.nPlayEvent = nPlayEvent;
        SendMessagePlayEvent();
    }

    public void SetPlayStatus(PlayStatus nPlayStatus)
    {
        GameData.nPlayStatus = nPlayStatus;
        SendMessagePlayStatus();
    }

    void CheckRoundReadyTime()
    {
        if (GameData.nPlayStatus == PlayStatus.READY)
        {
            GameData.nRoundReadyTime -= Time.deltaTime;

            if ((int)GameData.nRoundReadyTime != nPrevRoundReadyTime && nPrevRoundReadyTime != 0)
            {
                nPrevRoundReadyTime = (int)GameData.nRoundReadyTime;

                if ((int)GameData.nRoundReadyTime == 0)
                {
                    SetPlayEvent(PlayEvent.ROUND_COUNT_ZERO);
                }
                else
                {
                    SetPlayEvent(PlayEvent.ROUND_COUNT_DOWN);
                }
            }

            if (GameData.nRoundReadyTime <= 0f)
            {
                SetPlayStatus(PlayStatus.PLAY);
            }
        }
    }

    void StartGamePlay()
    {
        SetPlayStatus(PlayStatus.INIT_COMPONENT);
    }
    void OnPlayStatusInitComponent()
    {
    }
    void OnPostPlayStatusInitComponent()
    {
        SetPlayStatus(PlayStatus.INIT_PLANET);
    }

    void OnPlayStatusInitPlanet()
    {
        if (GameData.Test.bTest && GameData.Test.nTestGameMode != GameMode.NONE)
        {
            GameData.nGameMode = GameData.Test.nTestGameMode;
        }

        if (GameData.Test.bTest && GameData.Test.nTestPlanetType != PlanetType.PLANET_TYPE_NONE)
        {
            GameData.nPlanetType = GameData.Test.nTestPlanetType;
        }

        if (GameData.Test.bTest && GameData.Test.nTestStarCoin != 0) {
            GameData.Prefs.nStarCoin = GameData.Test.nTestStarCoin;
        }

//        Debug.Log("Init Planet : " + GameData.nPlanetType);

        // clear previous planet object if exist
        ClearPlanet();

        Vector3 pos = new Vector3(0, 0, 0);
        obPlanet = Instantiate(pfPlanet[(int)GameData.nPlanetType], pos, Quaternion.identity) as Planet;
        obPlanet.transform.parent = transform;
    }

    void OnPostPlayStatusInitPlanet()
    {
        if (GameData.StartCondition.bNeedReward)
        {
            // reset flag
            GameData.StartCondition.bNeedReward = false;

            GameData.nPlayStatusAfterReward = PlayStatus.START_PLANET;
            SetPlayStatus(PlayStatus.REWARD);
        }
        else
        {
            SetPlayStatus(PlayStatus.START_PLANET);
        }
    }

    void OnPlayStatusStartPlanet()
    {
        // Unlock Planet
        UnlockPlanet(GameData.nPlanetType);

        obPlanet.UpdateRoundData();

        GameData.nMaxRound = GameData.RoundData.nMaxRound;

        GameData.Prefs.bCanContinue = true;
        GameData.Prefs.nLastGameMode = GameData.nGameMode;
        GameData.Prefs.nLastPlanetType = GameData.nPlanetType;
        GameData.Prefs.nLastRound = GameData.nRound;
        PrefsManager.GetInstance().SavePrefs();
    }
    void OnPostPlayStatusStartPlanet()
    {
        if (GameData.Test.bTest && GameData.Test.bSkipStartPlanet)
        {
            SetPlayStatus(PlayStatus.START_ROUND);
        }
    }

    void UnlockPlanet(PlanetType nPlanetType)
    {
        bool bUnlocked = false;
        switch (nPlanetType)
        {
        case PlanetType.PLANET_TYPE_EARTH: 
            if(GameData.Prefs.bEarthLock) {
                bUnlocked = true;
                GameData.Prefs.bEarthLock = false; 
            }
            break;
        case PlanetType.PLANET_TYPE_MARS: 
            if(GameData.Prefs.bMarsLock) {
                bUnlocked = true;
                GameData.Prefs.bMarsLock = false;
            }
            break;
        case PlanetType.PLANET_TYPE_JUPITER: 
            if(GameData.Prefs.bJupiterLock) {
                bUnlocked = true;
                GameData.Prefs.bJupiterLock = false; 
            }
            break;
        }

        if(bUnlocked) {
            PrefsManager.GetInstance().SavePrefs();
        }
    }

    void OnPlayStatusStartRound()
    {
        // Save record data
        GameData.Prefs.nLastRound = GameData.nRound;
        GameData.Prefs.nRoundPlayCount++;
        PrefsManager.GetInstance().SavePrefs();

        // init data
        // Do not init GameData.nScore because it should be effective when previous round clear
        GameData.nRoundScore = 0;
        GameData.nBonusScore = 0;
        GameData.nComboCount = 0;
        GameData.nBestSpeed = 0;
        GameData.nBestComboCount = 0;
        GameData.nRoundResult = RoundResult.NONE;
        GameData.nRoundReadyTime = GameData.ROUND_READY_TIME + 0.5f;
        nPrevRoundReadyTime = (int)GameData.nRoundReadyTime;

        obPlanet.UpdateRoundData();

        GameData.nPlayTime = GameData.RoundData.nPlayTime + 0.5f;

        if (GameData.Test.bTest && GameData.Test.nTestStartPlayTime != 0f)
        {
            GameData.nPlayTime = GameData.Test.nTestStartPlayTime;
        }

        AnalyticsManger.GetInstance().SendAnalyticsPlayRound();
    }

    void OnPostPlayStatusStartRound()
    {
        SetPlayStatus(PlayStatus.READY);
    }

    void OnPlayStatusReady()
    {
    }

    void OnPostPlayStatusReady()
    {
        if (GameData.Test.bTest && GameData.Test.bSkipReady)
        {
            SetPlayStatus(PlayStatus.PLAY);
        }
    }

    void OnPlayStatusEnding()
    {
    }

    void OnPostPlayStatusEnding()
    {
        InvokeManager.GetInstance().SetInvoke(this, "SetPlayStatus", PlayStatus.RESULT, GameData.ROUND_ENDING_TIME);
    }
    void OnPostPlayStatusResult()
    {
        InvokeManager.GetInstance().SetInvoke(this, "SetPlayStatus", PlayStatus.END_ROUND, GameData.RESULT_SHOW_TIME);
    }
    void OnPlayStatusEndRound()
    {
        ReplayManager.GetInstance().StopRecording();
        PrefsManager.GetInstance().SavePrefs();
        AnalyticsManger.GetInstance().SendAnalyticsPlayRound();

        if (GameData.Test.bTest && GameData.Test.bTurnOffTestOnEndRound)
        {
            GameData.Test.bTest = false;
        }
    }
    void OnPostPlayStatusEndRound()
    {
        if (GameData.nRoundResult == RoundResult.ROUND_CLEAR)
        {
            if (GameData.nRound == GameData.nMaxRound)
            {
                SetPlayStatus(PlayStatus.END_PLANET);
            }
        }
    }

    void OnPlayStatusEndPlanet()
    {
        bool bLastPlanet = true;
        if (GameData.nGameMode == GameMode.NORMAL && GameData.nPlanetType != PlanetType.PLANET_TYPE_LAST) {
            bLastPlanet = false;
        }

        if (bLastPlanet)
        {
            GameData.Prefs.bCanContinue = false;
            PrefsManager.GetInstance().SavePrefs();
        }

        if (GameData.nPlanetType != PlanetType.PLANET_TYPE_LAST)
        {
            PlanetType nNextPlanetType = (PlanetType)(GameData.nPlanetType + 1);
            UnlockPlanet(nNextPlanetType);
        }

        if (GameData.Test.bTest && GameData.Test.bTurnOffTestOnEndPlanet)
        {
            GameData.Test.bTest = false;
        }
    }

    void OnPostPlayStatusQuit()
    {
        InvokeManager.GetInstance().SetInvoke(this, "SetPlayStatus", PlayStatus.DESTROY_COMPONENT, GameData.SCREEN_IMAGE_FADE_OUT_TIME);
    }

    void OnPlayStatusDestroyComponent()
    {
        ClearPlanet();
    }

    void OnPostPlayStatusDestroyComponent()
    {
        GoToMain();
    }

    void OnPlayStatusPause()
    {
        ReplayManager.GetInstance().PauseRecording();
    }

    void OnPostPlayStatusResume()
    {
        // give a little delay for preventing touching input on clicking resume button
        InvokeManager.GetInstance().SetInvoke(this, "DelayedGameResume", GameData.RESUME_DELAY);
    }

    void DelayedGameResume()
    {
        // Should not call SetPlayStatus because it try to start things in status from beginning
        GameData.nPlayStatus = GameData.nPlayStatusOnPause;
        if (ReplayManager.GetInstance().IsSetToRecord())
        {
            ReplayManager.GetInstance().SetToRecord(false);
            ReplayManager.GetInstance().StartRecording();
        }
        else if (ReplayManager.GetInstance().IsRecordingStopped())
        {
            ReplayManager.GetInstance().Reset();
        }
        else
        {
            ReplayManager.GetInstance().ResumeRecording();
        }
    }

    void OnPlayStatusRestartRound()
    {
        // clear invoke list
        InvokeManager.GetInstance().Clear();

        // Reset scores
        GameData.nScore = 0;

        AnalyticsManger.GetInstance().SendAnalyticsStartPlay("Restart");
    }

    void OnPostPlayStatusRestartRound()
    {
        InvokeManager.GetInstance().SetInvoke(this, "SetPlayStatus", PlayStatus.START_ROUND, GameData.SCREEN_IMAGE_FADE_OUT_TIME);
    }

    void OnPlayStatusReward()
    {
        InvokeManager.GetInstance().Clear();
    }

    void OnPostPlayStatusAfterReward()
    {
        InvokeManager.GetInstance().SetInvoke(this, "DelayedAfterReward", GameData.SCREEN_IMAGE_FADE_OUT_TIME);
    }
    
    void DelayedAfterReward()
    {
        if (!StarCoinManager.GetInstance().Pay(GameData.StartCondition.nStarPrice))
        {
            SetPlayStatus(PlayStatus.QUIT);
            return;
        }

        SetPlayStatus(GameData.nPlayStatusAfterReward);
    }

    void SendMessagePlayEvent()
    {
        string strPlayEvent = GameData.GetPlayEventName(GameData.nPlayEvent);
        string strEvent = "OnPlayEvent" + strPlayEvent;

        int nCount = listEventReceiver.Count;
        for (int i = 0; i < nCount; i++)
        {
            Component obComp = (Component)listEventReceiver[i];
            obComp.SendMessage(strEvent, SendMessageOptions.DontRequireReceiver);
        }
    }

    void SendMessagePlayStatus()
    {
        string strPlayStatus = GameData.GetPlayStatusName(GameData.nPlayStatus);
        string strEvent = "OnPlayStatus" + strPlayStatus;

        int nCount = listEventReceiver.Count;
        for (int i = 0; i < nCount; i++)
        {
            Component obComp = (Component)listEventReceiver[i];
            obComp.SendMessage(strEvent, SendMessageOptions.DontRequireReceiver);
        }

        // Send Post Event to GamePlay object
        strEvent = "OnPostPlayStatus" + strPlayStatus;
        SendMessage(strEvent, SendMessageOptions.DontRequireReceiver);
	}

    void ClearPlanet()
    {
        if (obPlanet)
        {
            DestroyObject(obPlanet.gameObject);
            obPlanet = null;
        }
    }

    void UpdatePlayTime()
    {
        if (GameData.nPlayStatus == PlayStatus.PLAY)
        {
            GameData.nPlayTime -= Time.deltaTime;
        }
        else if(GameData.nPlayStatus == PlayStatus.ENDING)
        {
            if (GameData.nRoundResult == RoundResult.ROUND_CLEAR && GameData.nPlayTime < 1f)
            {
                GameData.nPlayTime = 1f;
            }
        }
    }

    void UpdateScore()
    {
        if (GameData.nPlayStatus == PlayStatus.PLAY)
        {
            int nScore = (int)(obPlayground.GetSpeed() * GameData.ADJUST_UNIT_SCORE_RATE * Time.deltaTime);

            nScoreBoostTime -= Time.deltaTime;
            if (nScoreBoostTime > 0)
            {
                nScore = (int)(nScore * nScoreBoost);
            }

            GameData.nRoundScore += nScore;
            GameData.nScore += nScore;

            if (GameData.Test.bTest && GameData.Test.nTestScore != 0)
            {
                GameData.nScore = GameData.Test.nTestScore;
            }
        }
    }

    void UpdateBestRecord()
    {
        if (GameData.nPlayStatus == PlayStatus.PLAY)
        {
            if (GameData.nScore > GameData.nBestScore)
            {
                GameData.nBestScore = GameData.nScore;

                if (GameData.nGameMode == GameMode.NORMAL)
                {
                    GameData.Prefs.nBestScore = GameData.nScore;
                }
                else
                {
                    switch (GameData.nPlanetType)
                    {
                        case PlanetType.PLANET_TYPE_EARTH: GameData.Prefs.nEarthBestScore = GameData.nScore; break;
                        case PlanetType.PLANET_TYPE_MARS: GameData.Prefs.nMarsBestScore = GameData.nScore; break;
                        case PlanetType.PLANET_TYPE_JUPITER: GameData.Prefs.nJupiterBestScore = GameData.nScore; break;
                    }
                }
            }

            int nSpeed = (int)(obPlayground.GetSpeed() * GameData.ADJUST_UNIT_SPEED_RATE);
            if (nSpeed > GameData.nBestSpeed)
            {
                GameData.nBestSpeed = nSpeed;
            }

            if (GameData.nComboCount > GameData.nBestComboCount)
            {
                GameData.nBestComboCount = GameData.nComboCount;
            }
        }
    }


    bool CheckRoundClear()
    {
        if (GameData.nPlayStatus == PlayStatus.PLAY)
        {
            if (obPlayground.GetDistance() == 0)
            {
                GameData.nBonusScore = (int)(obPlayground.GetSpeed() * GameData.ADJUST_UNIT_SCORE_RATE * GameData.nPlayTime);
                GameData.nScore += GameData.nBonusScore;

                if (GameData.Test.bTest && GameData.Test.nTestScore != 0) {
                    GameData.nScore = GameData.Test.nTestScore;
                }

                UpdateBestRecord();

                GameData.nRoundResult = RoundResult.ROUND_CLEAR;
                SetPlayStatus(PlayStatus.ENDING);
                return true;
            }
        }

        return false;
    }
    
    bool CheckTimeOver()
    {
        if (GameData.nPlayStatus == PlayStatus.PLAY)
        {
            if (GameData.nPlayTime < 1f)
            {
                GameData.nRoundResult = RoundResult.TIME_OVER;
                SetPlayStatus(PlayStatus.ENDING);
                return true;
            }
        }

        return false;
    }

    public void OnPlayEventBlockDropSuccess()
    {
        switch (GameData.nMatchResult)
        {
            case MatchResult.GOOD: nScoreBoost = GameData.SCORE_BOOST_GOOD; nScoreBoostTime = GameData.SCORE_BOOST_TIME; break;
            case MatchResult.GREAT: nScoreBoost = GameData.SCORE_BOOST_GREATE; nScoreBoostTime = GameData.SCORE_BOOST_TIME; break;
            case MatchResult.PERFECT: nScoreBoost = GameData.SCORE_BOOST_PERFECT; nScoreBoostTime = GameData.SCORE_BOOST_TIME; break;
        }

        GameData.nComboCount++;
        if (GameData.nComboCount >= GameData.SCORE_BOOST_MIN_COMBO_COUNT)
        {
            float nComboBost = ((float)GameData.nComboCount) * GameData.SCORE_BOOST_COMBO_RATE;
            nScoreBoost += nComboBost;
        }
    }

    public void OnPlayEventBlockDropFail()
    {
        GameData.nComboCount = 0;
    }

    public void OnPlayEventBlockHitToCamera()
    {
        GameData.nComboCount = 0;
    }

    public void OnPlayEventStarHit()
    {
        if (GameData.Prefs.nStarCoin < GameData.STARCOIN_MAX_COUNT)
        {
            StarCoinManager.GetInstance().Add(1);
        }
        else
        {
            // if the count of star is over the maximum, give bonus score
            GameData.nRoundScore += GameData.STARCOIN_BONUS_SCORE;
            GameData.nScore += GameData.STARCOIN_BONUS_SCORE;

            if (GameData.Test.bTest && GameData.Test.nTestScore != 0)
            {
                GameData.nScore = GameData.Test.nTestScore;
            }

            UpdateBestRecord();
        }
    }

    bool CheckTouchCell(Vector2 pos)
    {
        if (GameData.nPlayStatus != PlayStatus.PLAY)
        {
            return false;
        }

        Block obCurBlock = obBlocks.obCurBlock;
        if (obCurBlock && obCurBlock.bAvailable)
        {
            RaycastHit obHit;
            float nDistance = GameData.CAMERA_CELL_DISTANCE * 2;    // Check enough distance as twice of distance between camera and cell
            Ray iRay = Camera.main.ScreenPointToRay(pos);
            int nLayerMask = (1 << LayerMask.NameToLayer("Cell")); // Check Cell Layer

            if (Physics.Raycast(iRay, out obHit, nDistance, nLayerMask))
            {
                Cell obCell = obHit.collider.gameObject.GetComponent("Cell") as Cell;
                if (obCell && obCell.bAvailable)
                {
                    obCell.DropBlock(obCurBlock);
                    obCurBlock = null;
                    return true;
                }
            }
        }
        
        return false;
    }

    public void OnControllerTouch(Vector2 pos)
    {
    }

    public void OnControllerRelease(Vector2 pos)
    {
        if (!CheckTouchCell(pos)) {
            Block obCurBlock = obBlocks.obCurBlock;
            if (obCurBlock && obCurBlock.bAvailable)
            {
                obCurBlock.Rotate();
            }
        }
    }

    public void OnControllerAxes(float nHoriz, float nVert)
    {
        obCamera.MovePos(nHoriz, nVert);
    }

    public void OnButtonClickGoToNextRound()
    {
        ReplayManager.GetInstance().Reset();

        if (!AdsManager.GetInstance().ShowEndRoundAds())
        {
            SetPlayEvent(PlayEvent.BUTTON_CLICK);
        }

        if (GameData.nRound < GameData.nMaxRound)
        {
            GameData.nRound++;
        }

        obScreenImage.ShowFadeOutEffect();
        InvokeManager.GetInstance().SetInvoke(this, "SetPlayStatus", PlayStatus.START_ROUND, GameData.SCREEN_IMAGE_FADE_OUT_TIME);
    }

    public void OnButtonClickGoToNextPlanet()
    {
        ReplayManager.GetInstance().CancelRecording();

        if (!AdsManager.GetInstance().ShowEndRoundAds())
        {
            SetPlayEvent(PlayEvent.BUTTON_CLICK);
        }

        GameData.nRound = 1;
        GameData.nPlanetType++;

        obScreenImage.ShowFadeOutEffect();
        InvokeManager.GetInstance().SetInvoke(this, "SetPlayStatus", PlayStatus.INIT_PLANET, GameData.SCREEN_IMAGE_FADE_OUT_TIME);
    }
 
    public void OnButtonClickRestart()
    {
        if (!StarCoinManager.GetInstance().CanRestart() && !StarCoinManager.GetInstance().CanGetReward())
        {
            return;
        }

        ReplayManager.GetInstance().CancelRecording();

        if (StarCoinManager.GetInstance().CanRestart())
        {
            // Show first play ads
            if (!AdsManager.GetInstance().ShowEndRoundAds(true))
            {
                SetPlayEvent(PlayEvent.BUTTON_CLICK);
            }

            StarCoinManager.GetInstance().PayRestart();
            SetPlayStatus(PlayStatus.RESTART_ROUND);
        }
        else
        {
            GameData.StartCondition.nStarPrice = GameData.StarPrice.RESTART;
            AdsManager.GetInstance().ShowFullVideoAds("Restart", true, this, "OnFinishFullVideoAds");
        }
    }

    public void OnFinishFullVideoAds()
    {
        GameData.StartCondition.nReward = StarCoinManager.GetInstance().GetReward();
        GameData.nPlayStatusAfterReward = PlayStatus.RESTART_ROUND;
        SetPlayStatus(PlayStatus.REWARD);
    }

    public void OnButtonClickQuit()
    {
        ReplayManager.GetInstance().CancelRecording();

        if (GameData.nPlayStatus == PlayStatus.END_PLANET)
        {
            if (!AdsManager.GetInstance().ShowEndRoundAds())
            {
                SetPlayEvent(PlayEvent.BUTTON_CLICK);
            }
        }
        else
        {
            // Show first play ads
            if (!AdsManager.GetInstance().ShowEndRoundAds(true))
            {
                SetPlayEvent(PlayEvent.BUTTON_CLICK);
            }
        }

        SetPlayStatus(PlayStatus.QUIT);
    }

    public void OnButtonClickResume()
    {
        SetPlayEvent(PlayEvent.BUTTON_CLICK);

        SetPlayStatus(PlayStatus.RESUME);
    }

    public void OnButtonClickWormHole()
    {
        if (GameData.nPlayStatus == PlayStatus.PAUSE || GameData.nPlayStatus == PlayStatus.RESUME)
        {
            return;
        }

        SetPlayEvent(PlayEvent.BUTTON_CLICK);

        GameData.nPlayStatusOnPause = GameData.nPlayStatus;
        SetPlayStatus(PlayStatus.PAUSE);
    }

    public void OnButtonClickBack()
    {
        SetPlayEvent(PlayEvent.BUTTON_CLICK);

        OnButtonClickResume();
    }

    public void OnButtonClickReplayRecord()
    {
        SetPlayEvent(PlayEvent.BUTTON_CLICK);

        if (GameData.nPlayStatus == PlayStatus.PAUSE)
        {
            ReplayManager.GetInstance().SetToRecord(true);
            OnButtonClickResume();
        }
        else
        {
            ReplayManager.GetInstance().StartRecording();
        }

        if (GameData.Test.bTest && GameData.Test.bShowRecordReplay)
        {
            GameData.Test.bRecordingReplay = true;
        }
    }

    public void OnButtonClickReplayShare()
    {
        SetPlayEvent(PlayEvent.BUTTON_CLICK);

        ReplayManager.GetInstance().ShareReplay();

        if (GameData.Test.bTest && GameData.Test.bShowRecordReplay)
        {
            GameData.Test.bRecordingReplay = false;
        }
    }

    void GoToMain()
    {
        Application.LoadLevel("Main");
    }
}
