﻿using UnityEngine;
using System.Collections;

public class Effect : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void PlaceInPlayground()
    {
        transform.parent = GamePlay.GetInstance().obPlayground.transform;
    }

    public void Play(float nPlayTime = 0f)
    {
        gameObject.SetActive(true);

        ParticleSystem obPS = GetComponent<ParticleSystem>();
        obPS.Play();
        obPS.enableEmission = true;

        if (nPlayTime != 0f)
        {
            InvokeManager.GetInstance().SetInvoke(this, "Hide", nPlayTime);
        }
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

}
