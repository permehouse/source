﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {
    public AudioSource audBGM_Earth = null;
    public AudioSource audBGM_Mars = null;
    public AudioSource audBGM_Jupiter = null;
    public AudioSource audBGM_Jupiter_BreakThemAll = null;
    public AudioSource audBGM_Planet_Intro = null;
    public AudioSource audBGM_Reward_Intro = null;
    public AudioClip sndCellArrive = null;
    public AudioClip sndBlockDrop = null;
    public AudioClip sndBlockDropped = null;
    public AudioClip sndCellBreak = null;
    public AudioClip sndBlockHitToCamera = null;
    public AudioClip sndBlockFlip = null;
    public AudioClip sndStarHitToCamera = null;
    public AudioClip sndCellAway = null;
    public AudioClip sndCellRotation = null;
    public AudioClip sndRoundCountDown = null;
    public AudioClip sndRoundStart = null;
    public AudioClip sndRoundEnd = null;
    public AudioClip sndRoundTimeOver = null;
    public AudioClip sndScoreCounting = null;
    public AudioClip sndButtonClick = null;

    AudioSource audCurBGM = null;


    static SoundManager _this;
    public static SoundManager GetInstance()
    {
        return _this;
    }
    void Awake()
    {
        _this = this;
        gameObject.name = GameData.OBJECT_NAME_SOUND_MANAGER;
    }

	// Use this for initialization

/*
    public void ResetSound()
    {
        AudioSource audBGM = GetCurrentBGM();

        if(GameData.Prefs.bSoundOn) {
            PlayBGM(audBGM);
        }
        else {
            MuteBGM(audBGM);
        }
    }

    public void MuteBGM(AudioSource aud)
    {
        if (GameData.Prefs.bSoundOn)
        {
            return;
        }

        aud.mute = true;
    }
*/

    public void PlayBGM(AudioSource aud, float nVolume = 1f)
    {
        if (!GameData.Prefs.bSoundOn)
        {
            return;
        }

        if (GameData.Test.bTest && GameData.Test.bTurnOffBGM)
        {
            return;
        }


        if (audCurBGM != null)
        {
            audCurBGM.Stop();
        }

        audCurBGM = aud;

        aud.mute = false;
        aud.volume = nVolume;
        aud.Play();
    }

    public void PlaySound(AudioClip snd, float nVolume = 1f)
    {
        if (!GameData.Prefs.bSoundOn)
        {
            return;
        }

        AudioSource.PlayClipAtPoint(snd, transform.position, nVolume);
    }

    public void OnPlayStatusStartPlanet()
    {
        PlayBGM(audBGM_Planet_Intro, GameData.SOUND_BGM_VOLUMN);
    }

    public void OnPlayStatusReward()
    {
        PlayBGM(audBGM_Reward_Intro);
    }

    public void OnPlayStatusStartRound()
    {
        AudioSource audBGM = null;
        if (GameData.nPlanetType == PlanetType.PLANET_TYPE_MARS)
        {
            audBGM = audBGM_Mars;
        }
        else if (GameData.nPlanetType == PlanetType.PLANET_TYPE_JUPITER)
        {
            if (GameData.RoundData.nPlayMode == PlayMode.BREAK_THEM_ALL)
            {
                audBGM = audBGM_Jupiter_BreakThemAll;
            }
            else
            {
                audBGM = audBGM_Jupiter;
            }
        }
        else
        {
            audBGM = audBGM_Earth;
        }


        PlayBGM(audBGM, GameData.SOUND_BGM_VOLUMN);
    }

    public void OnPlayStatusEnding()
    {
        if (GameData.nRoundResult == RoundResult.ROUND_CLEAR)
        {
            PlaySound(sndCellRotation, 0.8f);
        }
        else
        {
            PlaySound(sndRoundTimeOver);
        }
    }

    public void OnPlayStatusEndRound()
    {
        PlaySound(sndRoundEnd);
    }

    public void OnPlayEventButtonClick()
    {
        PlaySound(sndButtonClick, 0.5f);
    }

    public void OnPlayEventRoundCountDown()
    {
        PlaySound(sndRoundCountDown, 0.5f);
    }
    public void OnPlayEventRoundCountZero()
    {
        PlaySound(sndRoundStart);
    }

    public void OnPlayEventCellAway()
    {
        PlaySound(sndCellAway, 0.5f);
    }

    public void OnPlayEventStarHit()
    {
        PlaySound(sndStarHitToCamera);
    }

    public void OnPlayEventBlockFitToCell()
    {
        if (GameData.nMatchResult == MatchResult.FAIL)
        {
            PlaySound(sndBlockDropped);
        }
        else
        {
            PlaySound(sndBlockFlip);
        }
    }

    public void OnPlayEventBlockHitToCamera()
    {
        PlaySound(sndBlockHitToCamera);
    }

    public void OnPlayEventCellBreak()
    {
        PlaySound(sndCellBreak);
    }
    public void OnPlayEventCellArrive()
    {
        PlaySound(sndCellArrive);
    }

    public void OnPlayEventBlockDrop()
    {
        PlaySound(sndBlockDrop);
    }

    public void OnPlayEventScoreCounting()
    {
        PlaySound(sndScoreCounting, 0.5f);
    }
}

