﻿using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour {
    public Sprite[] obSprite = new Sprite[GameData.nPlanetCount];
    float nStartDistance = 0f;
    float nBackgroundDistance = 0f;

    CameraManager obCamera = null;

    void Awake()
    {
        gameObject.name = GameData.OBJECT_NAME_BACKGROUND;
    }

    void FixedUpdate()
    {
        if (GameData.nPlayStatus < PlayStatus.START_PLANET || GameData.nPlayStatus > PlayStatus.END_PLANET)
        {
            return;
        }

        UpdatePos();
    }

    void UpdatePos() {
        Vector3 posCamera = obCamera.transform.position;
        float nDistance = nBackgroundDistance - (nStartDistance - Mathf.Abs(posCamera.z)) * GameData.BACKGROUND_DISTANCE_REDUCE_RATE;

        float nX = 0;
        float nY = 0;
        if (GameData.nPlayStatus == PlayStatus.PLAY)
        {
            nX = -posCamera.x * GameData.BACKGROUND_MOVE_SCALE;
            nY = -posCamera.y * GameData.BACKGROUND_MOVE_SCALE;
        }

        transform.position = new Vector3(nX, nY, posCamera.z + nDistance);
    }

    public void OnPlayStatusInitComponent()
    {
        obCamera = GamePlay.GetInstance().obCamera;
    }

    public void OnPlayStatusInitPlanet()
    {
        SpriteRenderer obSpriteRenderer = GetComponent<SpriteRenderer>();
        obSpriteRenderer.sprite = obSprite[(int)GameData.nPlanetType];

        SetBackgroundScale();
        transform.position = new Vector3(0f, 0f, GameData.BACKGROUND_DISTANCE);

        nStartDistance = GameData.nPlanetStartDistance;
        nBackgroundDistance = transform.position.z + nStartDistance;
    }



    public void OnPlayStatusStartPlanet()
    {
    }

    public void OnPlayStatusStartRound()
    {
        SetBackgroundScale();
        transform.position = new Vector3(0f, 0f, GameData.BACKGROUND_DISTANCE);

        nStartDistance = GameData.RoundData.nStartDistance;
        nBackgroundDistance = transform.position.z + nStartDistance;
    }

    public void OnPlayStatusEndRound()
    {
    }

    void SetBackgroundScale()
    {
        float nScreenRatio = ((float)Screen.currentResolution.height) / Screen.currentResolution.width;
        float nCompRatio = Mathf.Max(1f, nScreenRatio / GameData.CAMERA_BASE_SCREEN_RATIO);
        float nScaleRatio = 0.667f * (nCompRatio - 1f) + 1f;
        transform.localScale = GameData.dBackgroundScale * nScaleRatio;
    }
}
