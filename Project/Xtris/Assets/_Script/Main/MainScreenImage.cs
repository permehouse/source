﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainScreenImage : MonoBehaviour {
    Image obImage;
    Color dColorTo;
    float nColorLerpRate = 0f;
    Text txtLoading;
    Color dTextColorTo;

    void Awake()
    {
        obImage = GetComponent<Image>();
        txtLoading = transform.Find("LoadingText").GetComponent<Text>();
    }

	// Use this for initialization
	void Start () {
        txtLoading.gameObject.SetActive(false);
        ShowFadeInEffect();
	}

    void Update()
    {
        UpdateEffect();
    }

    void UpdateEffect() {
        if (obImage != null && obImage.color != dColorTo)
        {
            obImage.color = Color.Lerp(obImage.color, dColorTo, nColorLerpRate * Time.deltaTime);
        }

        if (txtLoading.IsActive())
        {
            txtLoading.color = Color.Lerp(txtLoading.color, dTextColorTo, GameData.SCREEN_IMAGE_LOADING_TEXT_FADE_IN_COLOR_LERP_RATE * Time.deltaTime);
        }
	}

    void ShowFadeInEffect()
    {
        obImage.color = GameData.SCREEN_IMAGE_FADE_OUT_COLOR;
        dColorTo = GameData.SCREEN_IMAGE_FADE_IN_COLOR;
        nColorLerpRate = GameData.SCREEN_IMAGE_FADE_IN_COLOR_LERP_RATE;
        gameObject.SetActive(true);
    }

    public void ShowFadeOutEffect(bool bFadeOutNow = false)
    {
        if (bFadeOutNow)
        {
            obImage.color = GameData.SCREEN_IMAGE_FADE_OUT_COLOR;
            gameObject.SetActive(true);

            // Show Loading Text
            txtLoading.gameObject.SetActive(true);
        }
        else
        {
            dColorTo = GameData.SCREEN_IMAGE_FADE_OUT_COLOR;
            nColorLerpRate = GameData.SCREEN_IMAGE_FADE_OUT_COLOR_LERP_RATE;
            gameObject.SetActive(true);


            // Setting Fade In Loading Text
            txtLoading.gameObject.SetActive(true);
            Color color = txtLoading.color;
            color.a = 0f;
            txtLoading.color = color;

            color.a = 1f;
            dTextColorTo = color;
        }
    }
}
