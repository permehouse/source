﻿using UnityEngine;
using System.Collections;

public class MainManager : MonoBehaviour {
    public MainUI obUI = null;
    public MainScreenImage obScreenImage = null;
    public Planet[] pfPlanet = new Planet[GameData.nPlanetCount];
    public Sprite[] obBackgroundSprite = new Sprite[GameData.nPlanetCount];
    public MainCamera obCamera = null;
    public GameObject obBackground = null;

    bool bNewPlay = false;
    bool bContinue = false;

    void Awake()
    {
    }

	// Use this for initialization
	void Start () {
        if (GameData.Test.bTest && GameData.Test.bTestChallenge)
        {
            GameData.Prefs.bEarthLock = !GameData.Test.bTestPlayEarth;
        }
        if (GameData.Test.bTest && GameData.Test.bTestChallenge)
        {
            GameData.Prefs.bMarsLock = !GameData.Test.bTestPlayMars;
        }
        if (GameData.Test.bTest && GameData.Test.bTestChallenge)
        {
            GameData.Prefs.bJupiterLock = !GameData.Test.bTestPlayMars;
        }

        ShowPlanet();

        GameData.nGameStatus = GameStatus.MAIN;
	}
	
    void ShowPlanet()
    {
        Vector3 pos = new Vector3(0, 0, 0);
        Planet obPlanet = Instantiate(pfPlanet[(int)GameData.Prefs.nLastPlanetType], pos, Quaternion.identity) as Planet;
        obPlanet.transform.parent = transform;

        Vector3 posCamera = GameData.Earth.posCameraCloseView;
        Vector3 dBackgroundScale = GameData.Earth.dBackgroundScale;
        float nStartDistance = 0f;
        switch (GameData.Prefs.nLastPlanetType)
        {
        case PlanetType.PLANET_TYPE_EARTH: 
            posCamera = GameData.Earth.posCameraCloseView;
            nStartDistance = GameData.Earth.Challenge.listDistance[0]; 
            dBackgroundScale = GameData.Earth.dBackgroundScale;
            break;
        case PlanetType.PLANET_TYPE_MARS: 
            posCamera = GameData.Mars.posCameraCloseView;
            nStartDistance = GameData.Mars.Challenge.listDistance[0]; 
            dBackgroundScale = GameData.Mars.dBackgroundScale;
            break;
        case PlanetType.PLANET_TYPE_JUPITER: 
            posCamera = GameData.Jupiter.posCameraCloseView;
            nStartDistance = GameData.Jupiter.Challenge.listDistance[0]; 
            dBackgroundScale = GameData.Jupiter.dBackgroundScale;
            break;
        }

        obCamera.SetPosition(posCamera);

        // Change Background Image
        SpriteRenderer obSpriteRenderer = obBackground.GetComponent<SpriteRenderer>();
        obSpriteRenderer.sprite = obBackgroundSprite[(int)GameData.Prefs.nLastPlanetType];

        // Change Background Scale and Pos
        obBackground.transform.localScale = dBackgroundScale;
        obBackground.transform.position = new Vector3(0f, 0f, GameData.BACKGROUND_DISTANCE);

        // Adjust Background Distance
        float nBackgroundDistance = obBackground.transform.position.z + nStartDistance;
        float nDistance = nBackgroundDistance - (nStartDistance - Mathf.Abs(posCamera.z)) * GameData.BACKGROUND_DISTANCE_REDUCE_RATE;

        obBackground.transform.position = new Vector3(0f, 0f, posCamera.z + nDistance);
    }

    public void OnButtonClickPlayEarth()
    {
//        Debug.Log("Click OnButtonClickPlayEarth");
        if (GameData.Prefs.bEarthLock)
        {
            return;
        }

        if (!StarCoinManager.GetInstance().CanPlayEarth() && !StarCoinManager.GetInstance().CanGetReward())
        {
            return;
        }

        bContinue = false;
        bNewPlay = false;

        GameData.nGameMode = GameMode.CHALLENGE;
        GameData.nPlanetType = PlanetType.PLANET_TYPE_EARTH;
        GameData.nBestScore = GameData.Prefs.nEarthBestScore;
        GameData.nRound = 1;

        if (StarCoinManager.GetInstance().CanPlayEarth())
        {
            MainSoundManager.GetInstance().PlaySoundButtonClick();
            StarCoinManager.GetInstance().PayEarth();
            StartGamePlay();
        }
        else
        {
            GameData.StartCondition.nStarPrice = GameData.StarPrice.PLAY_EARTH;
            AdsManager.GetInstance().ShowFullVideoAds("PlayEarth", true, this, "OnFinishFullVideoAds");
        }
    }

    public void OnButtonClickPlayMars()
    {
//        Debug.Log("Click OnButtonClickPlayMars");
        if (GameData.Prefs.bMarsLock)
        {
            return;
        }

        if (!StarCoinManager.GetInstance().CanPlayMars() && !StarCoinManager.GetInstance().CanGetReward())
        {
            return;
        }

        bContinue = false;
        bNewPlay = false;

        GameData.nGameMode = GameMode.CHALLENGE;
        GameData.nPlanetType = PlanetType.PLANET_TYPE_MARS;
        GameData.nBestScore = GameData.Prefs.nMarsBestScore;
        GameData.nRound = 1;

        if (StarCoinManager.GetInstance().CanPlayMars())
        {
            MainSoundManager.GetInstance().PlaySoundButtonClick();
            StarCoinManager.GetInstance().PayMars();
            StartGamePlay();
        }
        else
        {
            GameData.StartCondition.nStarPrice = GameData.StarPrice.PLAY_MARS;
            AdsManager.GetInstance().ShowFullVideoAds("PlayMars", true, this, "OnFinishFullVideoAds");
        }
    }

    public void OnButtonClickPlayJupiter()
    {
//        Debug.Log("Click OnButtonClickPlayJupiter");
        if (GameData.Prefs.bJupiterLock)
        {
            return;
        }

        if (!StarCoinManager.GetInstance().CanPlayJupiter() && !StarCoinManager.GetInstance().CanGetReward())
        {
            return;
        }

        bContinue = false;
        bNewPlay = false;

        GameData.nGameMode = GameMode.CHALLENGE;
        GameData.nPlanetType = PlanetType.PLANET_TYPE_JUPITER;
        GameData.nBestScore = GameData.Prefs.nJupiterBestScore;
        GameData.nRound = 1;

        if (StarCoinManager.GetInstance().CanPlayJupiter())
        {
            MainSoundManager.GetInstance().PlaySoundButtonClick();
            StarCoinManager.GetInstance().PayJupiter();
            StartGamePlay();
        }
        else
        {
            GameData.StartCondition.nStarPrice = GameData.StarPrice.PLAY_JUPITER;
            AdsManager.GetInstance().ShowFullVideoAds("PlayJupiter", true, this, "OnFinishFullVideoAds");
        }
    }
    
    public void OnButtonClickContinue()
    {
        if (!StarCoinManager.GetInstance().CanContinue() && !StarCoinManager.GetInstance().CanGetReward())
        {
            return;
        }

        GameData.nGameMode = GameData.Prefs.nLastGameMode;
        GameData.nPlanetType = GameData.Prefs.nLastPlanetType;
        GameData.nRound = GameData.Prefs.nLastRound;

        if (GameData.Prefs.nLastGameMode == GameMode.NORMAL)
        {
            GameData.nBestScore = GameData.Prefs.nBestScore;
        }
        else
        {
            switch (GameData.Prefs.nLastPlanetType)
            {
                case PlanetType.PLANET_TYPE_EARTH: GameData.nBestScore = GameData.Prefs.nEarthBestScore; break;
                case PlanetType.PLANET_TYPE_MARS: GameData.nBestScore = GameData.Prefs.nMarsBestScore; break;
                case PlanetType.PLANET_TYPE_JUPITER: GameData.nBestScore = GameData.Prefs.nJupiterBestScore; break;
            }
        }

        bContinue = true;
        bNewPlay = false;

        if (StarCoinManager.GetInstance().CanContinue())
        {
            MainSoundManager.GetInstance().PlaySoundButtonClick();
            StarCoinManager.GetInstance().PayContinue();
            StartGamePlay();
        }
        else
        {
            GameData.StartCondition.nStarPrice = GameData.StarPrice.CONTINUE;
            AdsManager.GetInstance().ShowFullVideoAds("Continue", true, this, "OnFinishFullVideoAds");
        }
    }
    public void OnFinishFullVideoAds()
    {
        GameData.StartCondition.nReward = StarCoinManager.GetInstance().GetReward();
        GameData.StartCondition.bNeedReward = true;
        StartGamePlay(false);
    }

    public void OnButtonClickNewPlay()
    {
        MainSoundManager.GetInstance().PlaySoundButtonClick();

        bNewPlay = true;
        bContinue = false;

        GameData.nGameMode = GameMode.NORMAL;
        GameData.nPlanetType = PlanetType.PLANET_TYPE_EARTH;
        GameData.nBestScore = GameData.Prefs.nBestScore;
        GameData.nRound = 1;

        GameData.Prefs.nNewPlayCount++;
        PrefsManager.GetInstance().SavePrefs();

        StartGamePlay();
    }

    void StartGamePlay(bool bFadeOutScreen = true)
    {
        obUI.OnStartGamePlay();

        SendAnalytics();

        GameData.nPlayStatus = PlayStatus.NONE;
        GameData.nScore = 0;

        if (bFadeOutScreen)
        {
            obScreenImage.ShowFadeOutEffect();
            Invoke("LoadGamePlay", GameData.SCREEN_IMAGE_FADE_OUT_TIME);
        }
        else
        {
            obScreenImage.ShowFadeOutEffect(true);
            LoadGamePlay();
        }
    }

    void LoadGamePlay()
    {
        Application.LoadLevel("GamePlay");
    }

    void SendAnalytics()
    {
        string strPlayName = "";
        if (bNewPlay)
        {
            strPlayName = "NewPlay";
        }
        else if (bContinue)
        {
            strPlayName = "Continue";
        }
        else
        {
            strPlayName = "Play" + GameData.GetPlanetName(GameData.nPlanetType);
        }

        AnalyticsManger.GetInstance().SendAnalyticsStartPlay(strPlayName);
    }
}
