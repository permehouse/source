﻿using UnityEngine;
using System.Collections;

public class MainStar : MonoBehaviour {
    float nRotateSpeedY = 0f;
    float nMoveSpeed = 0f;

    float nSpawnTime = 0f;
    bool bSpawn = false;

    void Start()
    {
        gameObject.SetActive(true);
        nSpawnTime = GameData.STAR_SPAWN_TIME_IN_MAIN_SCENE;
        Hide();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        CheckSpawn();

        if (bSpawn)
        {
            UpdatePos();
            UpdateRotation();

            CheckPassed();
        }
    }

    void UpdatePos()
    {
        float nStep = nMoveSpeed * Time.deltaTime;
        transform.localPosition = Vector3.MoveTowards(transform.localPosition, Vector3.zero, nStep);
    }

    void UpdateRotation()
    {
        Vector3 rot = new Vector3(0, nRotateSpeedY, 0);
        rot *= Time.deltaTime;

        Quaternion rotTo = transform.localRotation;
        rotTo.eulerAngles += rot;
        transform.localRotation = rotTo;
    }

    void CheckPassed()
    {
        if (transform.localPosition.z <= 0) {
            MainSoundManager.GetInstance().PlayStarHit();

            StarCoinManager.GetInstance().Add(1);
            Hide();
        }
    }

    void CheckSpawn()
    {
        nSpawnTime -= Time.deltaTime;
        if (!bSpawn && nSpawnTime <= 0f)
        {
            nSpawnTime = GameData.STAR_SPAWN_TIME_IN_MAIN_SCENE;
            Spawn();
        }
    }

    void Spawn()
    {
        bSpawn = true;

        float nRotateZ = Random.Range(0f, 359f);
        Quaternion rot = transform.localRotation;
        rot.eulerAngles = new Vector3(rot.z, rot.y, nRotateZ);
        transform.localRotation = rot;

        int nSign = (Random.Range(0, 2) == 0) ? -1 : 1;
        nRotateSpeedY = nSign * Random.Range(GameData.STAR_ROTATE_MIN_SPEED, GameData.STAR_ROTATE_MAX_SPEED);

        nMoveSpeed = Random.Range(GameData.STAR_MOVE_MIN_SPEED, GameData.STAR_MOVE_MAX_SPEED);
        nMoveSpeed *= GameData.STAR_MOVE_SPEED_RATE_IN_MAIN_SCENE;

        float nDistance = GameData.STAR_SPAWN_DISTANCE_IN_MAIN_SCENE;
        float nX = Random.Range(-nDistance, nDistance);
        float nY = Random.Range(-nDistance, nDistance);

        transform.localPosition = new Vector3(nX, nY, GameData.STAR_START_DISTANCE);//obBackground.transform.position.z);
    }

    void Hide()
    {
        bSpawn = false;
        transform.position = new Vector3(0f, 0f, 0f);
    }
}
