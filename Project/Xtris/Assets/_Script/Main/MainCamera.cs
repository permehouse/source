﻿using UnityEngine;
using System.Collections;

public class MainCamera : MonoBehaviour {
    float nPosZ = 0f;

    void Start()
    {
        CheckScreenResolution();
    }

    void CheckScreenResolution()
    {
        float nScreenRatio = ((float)Screen.currentResolution.height) / Screen.currentResolution.width;
        float nCompRatio = nScreenRatio / GameData.CAMERA_BASE_SCREEN_RATIO;
        float nFOV = GameData.CAMERA_BASE_FOV;
        if (nCompRatio > 1f)
        {
            nFOV = (int)(45.701f * nCompRatio + 14.424);
        }
        Camera obCamera = GetComponent<Camera>();
        obCamera.fieldOfView = nFOV;
    }

	// Update is called once per frame
	void FixedUpdate () {
        UpdatePos();
	}

    void UpdatePos()
    {
        float nDistance = nPosZ + (nPosZ / 4f) * (1f - Mathf.Cos(GameData.MAINCAMERA_MOVE_SPEED * Time.time));
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, nDistance);
    }

    public void SetPosition(Vector3 pos)
    {
        transform.position = pos;
        nPosZ = transform.localPosition.z;
    }
}
