﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainUI : MonoBehaviour {
    Image imgLogo = null;
    RectTransform trImgLogo = null;

    Button btnHowToPlay = null;
    Button btnWormHole = null;
    Button btnBlock = null;
    Button btnSound = null;
    Text txtHowToPlay = null;

    GameObject obMainPanel = null;
    Button btnContinue = null;
    Text txtContinue = null;
    Button btnNewPlay = null;
    Text txtNewPlay = null;

    GameObject obWormHolePanel = null;
    Button btnPlayEarth = null;
    Button btnPlayMars = null;
    Button btnPlayJupiter = null;
    Text txtWormHolePanelTitleBack = null;
    Text txtWormHolePanelTitle = null;
    Text txtContinueRound = null;
    Text txtBestScore = null;
    Text txtPlayEarth = null;
    Text txtPlayEarthBestScore = null;
    Text txtPlayMars = null;
    Text txtPlayMarsBestScore = null;
    Text txtPlayJupiter = null;
    Text txtPlayJupiterBestScore = null;

    GameObject obLanguagePanel = null;
    Text txtLanguagePanelTitleBack = null;
    Text txtLanguagePanelTitle = null;
    Button btnEnglish = null;
    Button btnRussian = null;
    Button btnGerman = null;
    Button btnFrench = null;
    Button btnSpanish = null;
    Button btnSimplifiedChinese = null;
    Button btnTraditionalChinese = null;
    Button btnKorean = null;
    Button btnJapanese = null;
    Text txtEnglish = null;
    Text txtRussian = null;
    Text txtGerman = null;
    Text txtFrench = null;
    Text txtSpanish = null;
    Text txtSimplifiedChinese = null;
    Text txtTraditionalChinese = null;
    Text txtKorean = null;
    Text txtJapanese = null;

    GameObject obBlockPanel = null;
    GameObject obHowToPlayImage1 = null;
    GameObject obHowToPlayImage2 = null;
    GameObject obHowToPlayImage3 = null;
    Text txtBlockPanelTitleBack = null;
    Text txtBlockPanelTitle = null;
    Text txtHowToImage1_Title = null;
    Text txtHowToImage1_SubTitle1 = null;
    Text txtHowToImage1_SubTitle2 = null;
    Text txtHowToImage1_SubTitle3 = null;
    Text txtHowToImage1_Touch1 = null;
    Text txtHowToImage1_Touch2 = null;
    Text txtHowToImage1_Next = null;
    Text txtHowToImage2_Title = null;
    Text txtHowToImage2_SubTitle1 = null;
    Text txtHowToImage2_SubTitle2 = null;
    Text txtHowToImage2_SubTitle3 = null;
    Text txtHowToImage2_Drag = null;
    Text txtHowToImage2_Next = null;
    Text txtHowToImage3_Title = null;
    Text txtHowToImage3_SubTitle1 = null;
    Text txtHowToImage3_SubTitle2 = null;
    Text txtHowToImage3_SubTitle3 = null;
    Text txtHowToImage3_Touch = null;
    Text txtHowToImage3_Close = null;

    GameObject obTitleLogoImage = null;
    RectTransform trTitleLogoImage = null;
    Text txtVersion = null;
    int nCountStarBackClick = 0;

    int nHowToPlayImageIndex = 0;

    public TextMesh txtTitleLogo = null;
    public TextMesh txtTitleLogoBack = null;
    public TextMesh txtTitleLogoX = null;
    public TextMesh txtTitleLogoXBack = null;
    public TextMesh txtSubTitleLogo = null;
    public TextMesh txtSubTitleLogoBack = null;
    public TextMesh txtStarCoin = null;
    public TextMesh txtStarCoinBack = null;

    public Sprite obAdsIcon = null;
    public Sprite obStarIcon = null;
    public Sprite obLockIcon = null;

    public Sprite obSoundOn = null;
    public Sprite obSoundOff = null;

    float nLogoShowTime = 0f;

    public bool bWormHoleButtonPushed;
    public bool bBlockButtonPushed;
    public bool bLanguageButtonPushed;

    bool bFirstGamePlay = false;

    Text txtDebugging = null;

    void Awake()
    {
        imgLogo = transform.Find("LogoImage").GetComponent<Image>();
        trImgLogo = transform.Find("LogoImage").GetComponent<RectTransform>();

        btnHowToPlay = transform.Find("HowToPlayButton").GetComponent<Button>();
        txtHowToPlay = btnHowToPlay.transform.Find("Text").GetComponent<Text>();
        btnWormHole = transform.Find("WormHoleButton").GetComponent<Button>();
        btnBlock = transform.Find("BlockButton").GetComponent<Button>();
        btnSound = transform.Find("SoundButton").GetComponent<Button>();

        obMainPanel = transform.Find("MainPanel").gameObject;
        btnContinue = obMainPanel.transform.Find("ContinueButton").GetComponent<Button>();
        txtContinue = btnContinue.transform.Find("Text").GetComponent<Text>();
        txtContinueRound = btnContinue.transform.Find("RoundText").GetComponent<Text>();
        btnNewPlay = obMainPanel.transform.Find("NewPlayButton").GetComponent<Button>();
        txtNewPlay = btnNewPlay.transform.Find("Text").GetComponent<Text>();
        txtBestScore = btnNewPlay.transform.Find("BestScoreText").GetComponent<Text>();

        obWormHolePanel = transform.Find("WormHolePanel").gameObject;
        txtWormHolePanelTitleBack = obWormHolePanel.transform.Find("WormHoleTitleBackText").GetComponent<Text>();
        txtWormHolePanelTitle = txtWormHolePanelTitleBack.transform.Find("WormHoleTitleText").GetComponent<Text>();
        btnPlayEarth = obWormHolePanel.transform.Find("EarthButton").GetComponent<Button>();
        txtPlayEarth = btnPlayEarth.transform.Find("Text").GetComponent<Text>();
        txtPlayEarthBestScore = btnPlayEarth.transform.Find("BestScoreText").GetComponent<Text>();
        btnPlayMars = obWormHolePanel.transform.Find("MarsButton").GetComponent<Button>();
        txtPlayMars = btnPlayMars.transform.Find("Text").GetComponent<Text>();
        txtPlayMarsBestScore = btnPlayMars.transform.Find("BestScoreText").GetComponent<Text>();
        btnPlayJupiter = obWormHolePanel.transform.Find("JupiterButton").GetComponent<Button>();
        txtPlayJupiter = btnPlayJupiter.transform.Find("Text").GetComponent<Text>();
        txtPlayJupiterBestScore = btnPlayJupiter.transform.Find("BestScoreText").GetComponent<Text>();

        obBlockPanel = transform.Find("BlockPanel").gameObject;
        txtBlockPanelTitleBack = obBlockPanel.transform.Find("BlockTitleBackText").GetComponent<Text>();
        txtBlockPanelTitle = txtBlockPanelTitleBack.transform.Find("BlockTitleText").GetComponent<Text>();

        obHowToPlayImage1 = obBlockPanel.transform.Find("HowToPlayImage1").gameObject;
        txtHowToImage1_Title = obHowToPlayImage1.transform.Find("TitleText").GetComponent<Text>();
        txtHowToImage1_SubTitle1 = obHowToPlayImage1.transform.Find("SubTitle1Text").GetComponent<Text>();
        txtHowToImage1_SubTitle2 = obHowToPlayImage1.transform.Find("SubTitle2Text").GetComponent<Text>();
        txtHowToImage1_SubTitle3 = obHowToPlayImage1.transform.Find("SubTitle3Text").GetComponent<Text>();
        txtHowToImage1_Touch1 = obHowToPlayImage1.transform.Find("Touch1Text").GetComponent<Text>();
        txtHowToImage1_Touch2 = obHowToPlayImage1.transform.Find("Touch2Text").GetComponent<Text>();
        txtHowToImage1_Next = obHowToPlayImage1.transform.Find("NextText").GetComponent<Text>();

        obHowToPlayImage2 = obBlockPanel.transform.Find("HowToPlayImage2").gameObject;
        txtHowToImage2_Title = obHowToPlayImage2.transform.Find("TitleText").GetComponent<Text>();
        txtHowToImage2_SubTitle1 = obHowToPlayImage2.transform.Find("SubTitle1Text").GetComponent<Text>();
        txtHowToImage2_SubTitle2 = obHowToPlayImage2.transform.Find("SubTitle2Text").GetComponent<Text>();
        txtHowToImage2_SubTitle3 = obHowToPlayImage2.transform.Find("SubTitle3Text").GetComponent<Text>();
        txtHowToImage2_Drag = obHowToPlayImage2.transform.Find("DragText").GetComponent<Text>();
        txtHowToImage2_Next = obHowToPlayImage2.transform.Find("NextText").GetComponent<Text>();

        obHowToPlayImage3 = obBlockPanel.transform.Find("HowToPlayImage3").gameObject;
        txtHowToImage3_Title = obHowToPlayImage3.transform.Find("TitleText").GetComponent<Text>();
        txtHowToImage3_SubTitle1 = obHowToPlayImage3.transform.Find("SubTitle1Text").GetComponent<Text>();
        txtHowToImage3_SubTitle2 = obHowToPlayImage3.transform.Find("SubTitle2Text").GetComponent<Text>();
        txtHowToImage3_SubTitle3 = obHowToPlayImage3.transform.Find("SubTitle3Text").GetComponent<Text>();
        txtHowToImage3_Touch = obHowToPlayImage3.transform.Find("TouchText").GetComponent<Text>();
        txtHowToImage3_Close = obHowToPlayImage3.transform.Find("CloseText").GetComponent<Text>();

        obLanguagePanel = transform.Find("LanguagePanel").gameObject;
        txtLanguagePanelTitleBack = obLanguagePanel.transform.Find("LanguageTitleBackText").GetComponent<Text>();
        txtLanguagePanelTitle = txtLanguagePanelTitleBack.transform.Find("LanguageTitleText").GetComponent<Text>();
        btnEnglish = obLanguagePanel.transform.Find("EnglishButton").GetComponent<Button>();
        txtEnglish = btnEnglish.transform.Find("Text").GetComponent<Text>();
        btnRussian = obLanguagePanel.transform.Find("RussianButton").GetComponent<Button>();
        txtRussian = btnRussian.transform.Find("Text").GetComponent<Text>();
        btnGerman = obLanguagePanel.transform.Find("GermanButton").GetComponent<Button>();
        txtGerman = btnGerman.transform.Find("Text").GetComponent<Text>();
        btnFrench = obLanguagePanel.transform.Find("FrenchButton").GetComponent<Button>();
        txtFrench = btnFrench.transform.Find("Text").GetComponent<Text>();
        btnSpanish = obLanguagePanel.transform.Find("SpanishButton").GetComponent<Button>();
        txtSpanish = btnSpanish.transform.Find("Text").GetComponent<Text>();
        btnSimplifiedChinese = obLanguagePanel.transform.Find("SimplifiedChineseButton").GetComponent<Button>();
        txtSimplifiedChinese = btnSimplifiedChinese.transform.Find("Text").GetComponent<Text>();
        btnTraditionalChinese = obLanguagePanel.transform.Find("TraditionalChineseButton").GetComponent<Button>();
        txtTraditionalChinese = btnTraditionalChinese.transform.Find("Text").GetComponent<Text>();
        btnKorean = obLanguagePanel.transform.Find("KoreanButton").GetComponent<Button>();
        txtKorean = btnKorean.transform.Find("Text").GetComponent<Text>();
        btnJapanese = obLanguagePanel.transform.Find("JapaneseButton").GetComponent<Button>();
        txtJapanese = btnJapanese.transform.Find("Text").GetComponent<Text>();

        obTitleLogoImage = transform.Find("TitleLogoImage").gameObject;
        trTitleLogoImage = transform.Find("TitleLogoImage").GetComponent<RectTransform>();
        txtVersion = obTitleLogoImage.transform.Find("VersionText").GetComponent<Text>();


        txtDebugging = transform.Find("DebuggingText").GetComponent<Text>();
    }

	// Use this for initialization
	void Start () {
        if (GameData.Prefs.nGamePlayCount == 1)
        {
            bFirstGamePlay = true;
        }

        SetTextLanguage();

        ShowLogo();
        ShowPanels(true, false, false, false);

        SetTitleLogoColor();
        ShowSoundButton();
    }
    void SetTextLanguage()
    {
        txtEnglish.text = LanguageData.DISPLAY_ENGLISH;
        txtRussian.text = LanguageData.DISPLAY_RUSSIAN;
        txtGerman.text = LanguageData.DISPLAY_GERMAN;
        txtFrench.text = LanguageData.DISPLAY_FRANCH;
        txtSpanish.text = LanguageData.DISPLAY_SPANISH;
        txtSimplifiedChinese.text = LanguageData.DISPLAY_SIMPLIFIED_CHINESE;
        txtTraditionalChinese.text = LanguageData.DISPLAY_TRADITIONAL_CHINESE;
        txtKorean.text = LanguageData.DISPLAY_KOREAN;
        txtJapanese.text = LanguageData.DISPLAY_JAPANESE;

        txtTitleLogoX.text = LanguageData.strTitleX;
        txtTitleLogoXBack.text = LanguageData.strTitleX;
        txtTitleLogo.text = LanguageData.strTitle;
        txtTitleLogoBack.text = LanguageData.strTitle;

        txtSubTitleLogo.text = LanguageData.strSubTitle;
        txtSubTitleLogoBack.text = LanguageData.strSubTitle;

        txtContinue.text = LanguageData.strContinue;
        txtNewPlay.text = LanguageData.strNewPlay;

        txtWormHolePanelTitleBack.text = LanguageData.strGameMode_Challenge;
        txtWormHolePanelTitle.text = LanguageData.strGameMode_Challenge;

        txtPlayEarth.text = LanguageData.strPlayEarth;
        txtPlayMars.text = LanguageData.strPlayMars;
        txtPlayJupiter.text = LanguageData.strPlayJupiter;

        txtLanguagePanelTitleBack.text = LanguageData.strLanguagePanelTitle;
        txtLanguagePanelTitle.text = LanguageData.strLanguagePanelTitle;

        txtHowToPlay.text = LanguageData.strHowToPlay + " >";

        // Block Panel - How To Play Images
        txtBlockPanelTitleBack.text = LanguageData.strHowToPlay;
        txtBlockPanelTitle.text = LanguageData.strHowToPlay;

        txtHowToImage1_Title.text = LanguageData.strHowToPlay_Image1_Title;
        txtHowToImage1_SubTitle1.text = LanguageData.strHowToPlay_Image1_SubTitle1;
        txtHowToImage1_SubTitle2.text = LanguageData.strHowToPlay_Image1_SubTitle2;
        txtHowToImage1_SubTitle3.text = LanguageData.strHowToPlay_Image1_SubTitle3;
        txtHowToImage1_Touch1.text = LanguageData.strHowToPlay_Touch;
        txtHowToImage1_Touch2.text = LanguageData.strHowToPlay_Touch;
        txtHowToImage1_Next.text = LanguageData.strHowToPlay_NextPage;

        txtHowToImage2_Title.text = LanguageData.strHowToPlay_Image2_Title;
        txtHowToImage2_SubTitle1.text = LanguageData.strHowToPlay_Image2_SubTitle1;
        txtHowToImage2_SubTitle2.text = LanguageData.strHowToPlay_Image2_SubTitle2;
        txtHowToImage2_SubTitle3.text = LanguageData.strHowToPlay_Image2_SubTitle3;
        txtHowToImage2_Drag.text = LanguageData.strHowToPlay_Drag;
        txtHowToImage2_Next.text = LanguageData.strHowToPlay_NextPage;

        txtHowToImage3_Title.text = LanguageData.strBreakThemAll;
        txtHowToImage3_SubTitle1.text = LanguageData.strHowToPlay_Image3_SubTitle1;
        txtHowToImage3_SubTitle2.text = LanguageData.strHowToPlay_Image3_SubTitle2;
        txtHowToImage3_SubTitle3.text = LanguageData.strHowToPlay_Image3_SubTitle3;
        txtHowToImage3_Touch.text = LanguageData.strHowToPlay_Touch;
        txtHowToImage3_Close.text = LanguageData.strHowToPlay_Close;

        SetContinueRound();
        SetBestScore();
    }

    void ShowLogo(bool bShow = true)
    {
        if (bShow)
        {
            if (!GameData.bShowLogo)
            {
                GameData.bShowLogo = true;

                imgLogo.gameObject.SetActive(true);
                SetLogoImageScale(ref trImgLogo);

                if (Application.platform == RuntimePlatform.IPhonePlayer)
                {
                    nLogoShowTime = GameData.LOGO_SHOW_TIME_FOR_IOS;
                }
                else
                {
                    nLogoShowTime = GameData.LOGO_SHOW_TIME;
                }
            }
        }
        else
        {
            imgLogo.gameObject.SetActive(false);
        }
    }

    void SetLogoImageScale(ref RectTransform tr)
    {
        float nScreenRatio = ((float)Screen.currentResolution.height) / Screen.currentResolution.width;
        float nCompRatio = nScreenRatio / GameData.CAMERA_BASE_SCREEN_RATIO;
        tr.localScale = new Vector3(nCompRatio, 1f, 1f);
    }

    void SetContinueRound()
    {
        string strGameMode = GameData.GetGameModeNameInLanguage(GameData.Prefs.nLastGameMode);
        string strPlanetName = GameData.GetPlanetNameInLanguage(GameData.Prefs.nLastPlanetType);
        string strRound = GameData.GetRoundNameInLanguage(GameData.Prefs.nLastGameMode, GameData.Prefs.nLastPlanetType, GameData.Prefs.nLastRound);

        string strText = "";
        if (GameData.Prefs.nLastGameMode != GameMode.NORMAL)
        {
            strText = strGameMode + " ";
        }
        strText += strPlanetName + "\n";
        strText += strRound;

        txtContinueRound.text = strText;
    }

    void SetBestScore()
    {
        string strBest = LanguageData.strBest;

        txtBestScore.text = strBest + "\n" + GameData.Prefs.nBestScore.ToString("N0");
        txtPlayEarthBestScore.text = strBest + "\n" + GameData.Prefs.nEarthBestScore.ToString("N0");
        txtPlayMarsBestScore.text = strBest + "\n" + GameData.Prefs.nMarsBestScore.ToString("N0");
        txtPlayJupiterBestScore.text = strBest + "\n" + GameData.Prefs.nJupiterBestScore.ToString("N0");
    }

    void SetTitleLogoColor() {
/*
        Color dPlanetColor = GameData.Earth.dPlanetColor;
        switch (GameData.Prefs.nLastPlanetType)
        {
        case PlanetType.PLANET_TYPE_EARTH: dPlanetColor = GameData.Earth.dPlanetColor; break;
        case PlanetType.PLANET_TYPE_MARS: dPlanetColor = GameData.Mars.dPlanetColor; break;
        case PlanetType.PLANET_TYPE_JUPITER: dPlanetColor = GameData.Jupiter.dPlanetColor; break;
        }
        txtTitleLogo.color = dPlanetColor;
        txtTitleLogoX.color = dPlanetColor;
*/
    }
	
    void LateUpdate()
    {
        UpdateShowLogo();
        UpdateHowToPlayButton();

        UpdateButtonStatus();
        UpdateButtonIcon();

        UpdateWormHoleButtonRotation();
        UpdateBlockButtonRotation();

        DisplayStarCoin();

        DisplayDebuggingText();
    }

    void DisplayDebuggingText()
    {
        if (!GameData.Test.bTest)
        {
            txtDebugging.gameObject.SetActive(false);
            return;
        }
        txtDebugging.gameObject.SetActive(true);


/*
        string str = "\nScreen : " + Screen.width + ", " + Screen.height + "\n";
        str += "\nResolution : " + Screen.currentResolution.width + ", " + Screen.currentResolution.height;

        ShowDebuggingText(str);
 */
/*
        string str = "\nOriginal : " + PrefsManager.GetInstance().strTestText.Length + ", " + PrefsManager.GetInstance().strTestText;
        str += "\nEncrypt : " + PrefsManager.GetInstance().strEncValue.Length + ", " + PrefsManager.GetInstance().strEncValue;
        ShowDebuggingText(str);
*/
    }

    public void ShowDebuggingText(string str)
    {
        if (!GameData.Test.bTest)
        {
            return;
        }

        txtDebugging.text = str;
    }

    void UpdateShowLogo()
    {
        nLogoShowTime -= Time.deltaTime;
        if (nLogoShowTime <= 0)
        {
            ShowLogo(false);
        }
        else
        {
            if (nLogoShowTime <= 1f)
            {
                Color color = imgLogo.color;
                color.a = nLogoShowTime;
                imgLogo.color = color;
            }
        }
    }

    void UpdateHowToPlayButton()
    {
        if (!bFirstGamePlay)
        {
            btnHowToPlay.gameObject.SetActive(false);
            return;
        }

        btnHowToPlay.gameObject.SetActive(true);

        float nAlpha = Mathf.Abs(Mathf.Cos(Mathf.PI * GameData.UI_BUTTON_BLINK_FAST_SPEED * Time.time)) * (1f - GameData.UI_BUTTON_BLINK_ALPHA) + GameData.UI_BUTTON_BLINK_ALPHA;
        Transform trText = btnHowToPlay.transform.Find("Text");
        if (trText)
        {
            Text obText = trText.GetComponent<Text>();
            if (obText)
            {
                Color color = obText.color;
                color.a = nAlpha;
                obText.color = color;
            }
        }
    }

    void UpdateButtonStatus()
    {
        float nAlpha = Mathf.Abs(Mathf.Cos(Mathf.PI * GameData.UI_BUTTON_BLINK_SPEED * Time.time)) * (1f - GameData.UI_BUTTON_BLINK_ALPHA) + GameData.UI_BUTTON_BLINK_ALPHA;
        bool bDisable = !StarCoinManager.GetInstance().CanContinue() && !StarCoinManager.GetInstance().CanGetReward();
        SetButtonStatus(ref btnContinue, bDisable, nAlpha);

        bDisable = (GameData.Prefs.bEarthLock || (!StarCoinManager.GetInstance().CanPlayEarth() && !StarCoinManager.GetInstance().CanGetReward()));
        SetButtonStatus(ref btnPlayEarth, bDisable, nAlpha);

        bDisable = (GameData.Prefs.bMarsLock || (!StarCoinManager.GetInstance().CanPlayMars() && !StarCoinManager.GetInstance().CanGetReward()));
        SetButtonStatus(ref btnPlayMars, bDisable, nAlpha);

        bDisable = (GameData.Prefs.bJupiterLock || (!StarCoinManager.GetInstance().CanPlayJupiter() && !StarCoinManager.GetInstance().CanGetReward()));
        SetButtonStatus(ref btnPlayJupiter, bDisable, nAlpha);

        SetButtonStatus(ref btnNewPlay, false, nAlpha);
    }

    public void DisplayStarCoin()
    {
        txtStarCoin.text = GameData.Prefs.nStarCoin.ToString("N0");
        txtStarCoinBack.text = GameData.Prefs.nStarCoin.ToString("N0");
    }

    void SetButtonStatus(ref Button btn, bool bDisable, float nAlpha)
    {
        if (btn.IsActive())
        {
            if (!bDisable)
            {
                SetButtonColor(ref btn, GameData.UI_BUTTON_COLOR);
                MakeButtonTransparent(ref btn, nAlpha);
            }
            else
            {
                SetButtonColor(ref btn, GameData.UI_BUTTON_DISABLE_COLOR);
            }
        }
    }

    void SetButtonColor(ref Button btn, Color color)
    {
        Image obImage = btn.GetComponent<Image>();
        obImage.color = color;
    }

    void MakeButtonTransparent(ref Button btn, float nAlpha)
    {
        Image obImage = btn.GetComponent<Image>();
        Color color = obImage.color;
        color.a = nAlpha;
        obImage.color = color;

        Transform trText = btn.transform.Find("Text");
        if (trText)
        {
            Text obText = trText.GetComponent<Text>();
            if (obText)
            {
                color = obText.color;
                color.a = nAlpha;
                obText.color = color;
            }
        }

        Transform trIcon = btn.transform.Find("Icon");
        if (trIcon)
        {
            Image obIcon = trIcon.GetComponent<Image>();
            if (obIcon)
            {
                color = obIcon.color;
                color.a = nAlpha;
                obIcon.color = color;
            }
        }
    }

    void UpdateButtonIcon()
    {
        if (btnContinue.IsActive())
        {
            ShowButtonIcon(ref btnContinue, StarCoinManager.GetInstance().CanContinue());
        }

        if (btnPlayEarth.IsActive())
        {
            ShowButtonIcon(ref btnPlayEarth, StarCoinManager.GetInstance().CanPlayEarth(), GameData.Prefs.bEarthLock);
        }
        if (btnPlayMars.IsActive())
        {
            ShowButtonIcon(ref btnPlayMars, StarCoinManager.GetInstance().CanPlayMars(), GameData.Prefs.bMarsLock);
        }
        if (btnPlayJupiter.IsActive())
        {
            ShowButtonIcon(ref btnPlayJupiter, StarCoinManager.GetInstance().CanPlayJupiter(), GameData.Prefs.bJupiterLock);
        }
    }

    void ShowButtonIcon(ref Button btn, bool bCanPlay, bool bLock = false)
    {
        Transform trIcon = btn.transform.Find("Icon");
        if (trIcon)
        {
            Image obIcon = trIcon.GetComponent<Image>();
            if(bLock) {
                obIcon.sprite = obLockIcon;
            }
            else {
                if (bCanPlay)
                {
                    obIcon.sprite = obStarIcon;
                }
                else
                {
                    obIcon.sprite = obAdsIcon;
                }
            }
        }
    }

    void ShowHowToPlayImage()
    {
        obHowToPlayImage1.SetActive(false);
        obHowToPlayImage2.SetActive(false);
        obHowToPlayImage3.SetActive(false);
        switch (nHowToPlayImageIndex)
        {
            case 0: obHowToPlayImage1.SetActive(true); break;
            case 1: obHowToPlayImage2.SetActive(true); break;
            case 2: obHowToPlayImage3.SetActive(true); break;
        }
    }

    void ShowSoundButton()
    {
        Image obImage = btnSound.GetComponent<Image>();
        if(GameData.Prefs.bSoundOn) {
            obImage.sprite = obSoundOn;
        }
        else {
            obImage.sprite = obSoundOff;
        }
    }

    void UpdateWormHoleButtonRotation()
    {
        float nRoate = 0f;
        if (bWormHoleButtonPushed)
        {
            nRoate = GameData.UI_WARM_HOLE_BUTTON_SPEED * Time.deltaTime;
        }
        else
        {
            nRoate = -GameData.UI_WARM_HOLE_BUTTON_SPEED * Time.deltaTime;
        }

        RectTransform obRectTransform = btnWormHole.transform.GetComponent<RectTransform>();
        obRectTransform.Rotate(new Vector3(0f, 0f, nRoate));
    }

    void UpdateBlockButtonRotation()
    {
        float nRoate = 0f;
        if (bBlockButtonPushed)
        {
            nRoate = GameData.UI_WARM_HOLE_BUTTON_SPEED * Time.deltaTime;
        }
        else
        {
            nRoate = -GameData.UI_WARM_HOLE_BUTTON_SPEED * Time.deltaTime;
        }

        RectTransform obRectTransform = btnBlock.transform.GetComponent<RectTransform>();
        obRectTransform.Rotate(new Vector3(0f, 0f, nRoate));
    }

/*
    public void ShowWormHolePanel(bool bShow)
    {
        btnWormHole.gameObject.SetActive(true);

        if (bShow)
        {
            obMainPanel.SetActive(false);
            txtTitleLogo.gameObject.SetActive(false);

            obWormHolePanel.SetActive(true);
        }
        else
        {
            obMainPanel.SetActive(true);
            txtTitleLogo.gameObject.SetActive(true);

            obWormHolePanel.SetActive(false);

            btnContinue.gameObject.SetActive(GameData.Prefs.bCanContinue);
        }
    }
*/

    public void ShowPanels(bool bShowMainPalen, bool bShowWarmHolePanel, bool bShowBlockPanel, bool bShowLanguagePanel)
    {
        obMainPanel.SetActive(bShowMainPalen);
        txtTitleLogo.gameObject.SetActive(bShowMainPalen);
        if (bShowMainPalen) btnContinue.gameObject.SetActive(GameData.Prefs.bCanContinue);

        obWormHolePanel.SetActive(bShowWarmHolePanel);
        obBlockPanel.SetActive(bShowBlockPanel);
        obLanguagePanel.SetActive(bShowLanguagePanel);
    }

    public void ShowBlockPanel(bool bShow)
    {
        btnBlock.gameObject.SetActive(true);

        if (bShow)
        {
            obBlockPanel.SetActive(false);
            txtTitleLogo.gameObject.SetActive(false);

            obWormHolePanel.SetActive(true);
        }
        else
        {
            obMainPanel.SetActive(true);
            txtTitleLogo.gameObject.SetActive(true);

            obWormHolePanel.SetActive(false);

            btnContinue.gameObject.SetActive(GameData.Prefs.bCanContinue);
        }
    }

    public void OnButtonClickWormHole()
    {
        MainSoundManager.GetInstance().PlaySoundButtonClick();

        // release block button first
        bBlockButtonPushed = false;
        bLanguageButtonPushed = false;

        bWormHoleButtonPushed = !bWormHoleButtonPushed;

        ShowPanels(!bWormHoleButtonPushed, bWormHoleButtonPushed, false, false);
//        ShowWormHolePanel(bWormHoleButtonPushed);
    }

    public void OnButtonClickBlock()
    {
        MainSoundManager.GetInstance().PlaySoundButtonClick();

        if (GameData.Prefs.nGamePlayCount == 1)
        {
            bFirstGamePlay = false;
        }

        bLanguageButtonPushed = false;
        bBlockButtonPushed = !bBlockButtonPushed;

        if (bBlockButtonPushed)
        {
            ShowPanels(false, false, true, false);

            nHowToPlayImageIndex = 0;
            ShowHowToPlayImage();
        }
        else
        {
            ShowPanels(!bWormHoleButtonPushed, bWormHoleButtonPushed, false, false);
        }
    }

    public void OnButtonClickLanguage()
    {
        MainSoundManager.GetInstance().PlaySoundButtonClick();

        bLanguageButtonPushed = !bLanguageButtonPushed;

        if (bLanguageButtonPushed)
        {
            ShowPanels(false, false, false, true);
        }
        else
        {
            ShowPanels(!bWormHoleButtonPushed, bWormHoleButtonPushed, false, false);
        }
    }

    public void OnButtonClickSound()
    {
        MainSoundManager.GetInstance().PlaySoundButtonClick();

        GameData.Prefs.bSoundOn = !GameData.Prefs.bSoundOn;
        PrefsManager.GetInstance().SavePrefs();

        ShowSoundButton();
        MainSoundManager.GetInstance().ResetSound();
    }

    public void OnImageClickHowToPlay()
    {
        nHowToPlayImageIndex++;
        if(nHowToPlayImageIndex == GameData.HOWTOPLAY_IMAGE_COUNT) {
            OnButtonClickBlock();
        }
        else {
            MainSoundManager.GetInstance().PlaySoundButtonClick();
            ShowHowToPlayImage();
        }
    }

    public void OnStartGamePlay()
    {
        ShowPanels(false, false, false, false);

        btnHowToPlay.gameObject.SetActive(false);
        btnWormHole.gameObject.SetActive(false);
        btnBlock.gameObject.SetActive(false);
        btnSound.gameObject.SetActive(false);
    }

    public void OnButtonClickLanguage_English()
    {
        ChangeLanguage(LanguageData.LANGUAGE_ENGLISH);
    }

    public void OnButtonClickLanguage_French()
    {
        ChangeLanguage(LanguageData.LANGUAGE_FRANCH);
    }
    public void OnButtonClickLanguage_German()
    {
        ChangeLanguage(LanguageData.LANGUAGE_GERMAN);
    }
    public void OnButtonClickLanguage_Spanish()
    {
        ChangeLanguage(LanguageData.LANGUAGE_SPANISH);
    }
    public void OnButtonClickLanguage_Russian()
    {
        ChangeLanguage(LanguageData.LANGUAGE_RUSSIAN);
    }
    public void OnButtonClickLanguage_SimplifiedChinese()
    {
        ChangeLanguage(LanguageData.LANGUAGE_SIMPLIFIED_CHINESE);
    }
    public void OnButtonClickLanguage_TraditionalChinese()
    {
        ChangeLanguage(LanguageData.LANGUAGE_TRADITIONAL_CHINESE);
    }

    public void OnButtonClickLanguage_Korean()
    {
        ChangeLanguage(LanguageData.LANGUAGE_KOREAN);
    }

    public void OnButtonClickLanguage_Japanese()
    {
        ChangeLanguage(LanguageData.LANGUAGE_JAPANESE);
    }

    void ChangeLanguage(string strLanguage)
    {
        LanguageData.SetLanguage(strLanguage);

        // if change the language, send analytics data 
        AnalyticsManger.GetInstance().SendAnalyticsSetLanguage(strLanguage);

        GameData.Prefs.strLanguage = strLanguage;
        PrefsManager.GetInstance().SavePrefs();

        SetTextLanguage();
        OnButtonClickLanguage();
    }

    public void OnButtonClickStarBack()
    {
        MainSoundManager.GetInstance().PlaySoundButtonClick();

        nCountStarBackClick++;
        if (nCountStarBackClick >= GameData.TITLE_LOGO_SHOW_CLICK_COUNT)
        {
            nCountStarBackClick = 0;

            obTitleLogoImage.SetActive(true);
            txtVersion.gameObject.SetActive(true);
            txtVersion.text = "Ver " + GameData.strVersion;

            SetLogoImageScale(ref trTitleLogoImage);
        }
    }

    public void OnButtonClickTitleLogoImage()
    {
        nCountStarBackClick = 0;

        MainSoundManager.GetInstance().PlaySoundButtonClick();
        obTitleLogoImage.SetActive(false);
    }
}
