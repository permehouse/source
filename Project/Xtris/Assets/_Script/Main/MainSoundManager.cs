﻿using UnityEngine;
using System.Collections;

public class MainSoundManager : MonoBehaviour {
    public AudioSource audBGM_Main = null;
    public AudioClip sndButtonClick = null;
    public AudioClip sndStarHitToCamera = null;

    static MainSoundManager _this;
    public static MainSoundManager GetInstance()
    {
        return _this;
    }
    void Awake()
    {
        _this = this;
    }

	// Use this for initialization
	void Start () {
        ResetSound();
	}

    public void ResetSound()
    {
        if(GameData.Prefs.bSoundOn) {
            PlayBGM(audBGM_Main, GameData.SOUND_BGM_VOLUMN);
        }
        else {
            MuteBGM(audBGM_Main);
        }
    }

    public void MuteBGM(AudioSource aud)
    {
        if (GameData.Prefs.bSoundOn)
        {
            return;
        }

        aud.mute = true;
    }

    public void PlayBGM(AudioSource aud, float nVolume = 1f)
    {
        if (!GameData.Prefs.bSoundOn)
        {
            return;
        }

        if (GameData.Test.bTest && GameData.Test.bTurnOffBGM)
        {
            return;
        }

        aud.mute = false;
        aud.volume = nVolume;
        aud.Play();
    }

    public void PlayStarHit()
    {
        PlaySound(sndStarHitToCamera);
    }

    public void PlaySoundButtonClick()
    {
        PlaySound(sndButtonClick, 0.5f);
    }

    public void PlaySound(AudioClip snd, float nVolume = 1f)
    {
        if (!GameData.Prefs.bSoundOn)
        {
            return;
        }

        AudioSource.PlayClipAtPoint(snd, transform.position, nVolume);
    }
}
