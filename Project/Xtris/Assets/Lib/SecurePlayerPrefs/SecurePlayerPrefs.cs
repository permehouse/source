﻿using UnityEngine;

public static class SecurePlayerPrefs
{
    public static void Save()
    {
        PlayerPrefs.Save();
    }

    public static string SetString(string key, string value, int nPassCode)
    {
        string strEncKey = Encrypt(key, nPassCode);
        string strEncValue = Encrypt(value, nPassCode);

        PlayerPrefs.SetString(strEncKey, strEncValue);
        return strEncValue;
    }

    public static string GetString(string key, int nPassCode)
    {
        string strEncKey = Encrypt(key, nPassCode);
        string strEncValue = PlayerPrefs.GetString(strEncKey);

        return Decrypt(strEncValue, nPassCode);
    }

    public static string GetString(string key, string defaultValue, int nPassCode)
    {
        string strValue = GetString(key, nPassCode);
        if (string.IsNullOrEmpty(strValue))
        {
            strValue = defaultValue;
        }

        return strValue;
    }

    public static string Encrypt(string strText, int nPassCode)
    {
        char[] str = strText.ToCharArray();
        for (int i = 0; i < strText.Length; i++)
        {
            str[i] += (char)nPassCode;
        }

        string strEncrpyt = new string(str);
        return strEncrpyt;
    }

    public static string Decrypt(string strText, int nPassCode)
    {
        char[] str = strText.ToCharArray();
        for (int i = 0; i < strText.Length; i++)
        {
            str[i] -= (char)nPassCode;
        }

        string strDecypt = new string(str);
        return strDecypt;
    }
}